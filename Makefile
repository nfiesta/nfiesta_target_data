#nfiesta_target_data/Makefile

EXTENSION = nfiesta_target_data # name of extension
MODULEDIR = nfiesta_target_data_dir
DATA = nfiesta_target_data--1.0.0.sql \
	nfiesta_target_data--1.0.0--1.0.1.sql \
	nfiesta_target_data--1.0.1--1.0.2.sql \
	nfiesta_target_data--1.0.2--1.0.3.sql \
	nfiesta_target_data--1.0.3--1.0.4.sql \
	nfiesta_target_data--1.0.4--1.0.5.sql \
	nfiesta_target_data--1.0.5--1.0.6.sql \
	nfiesta_target_data--1.0.6--1.0.7.sql \
	nfiesta_target_data--1.0.7--1.0.8.sql \
	nfiesta_target_data--1.0.8--1.0.9.sql \
	nfiesta_target_data--1.0.9--1.0.10.sql \
	nfiesta_target_data--1.0.10--1.0.11.sql \
	nfiesta_target_data--1.0.11--1.0.12.sql \
	nfiesta_target_data--1.0.12--1.0.13.sql \
	nfiesta_target_data--1.0.13--1.0.14.sql \
	nfiesta_target_data--1.0.14--1.0.15.sql \
	nfiesta_target_data--1.0.15--1.0.16.sql \
	nfiesta_target_data--1.0.16--1.0.17.sql \
	nfiesta_target_data--1.0.17--1.0.18.sql \
	nfiesta_target_data--1.0.18--1.0.19.sql \
	nfiesta_target_data--1.0.19--1.0.20.sql \
	nfiesta_target_data--1.0.20--1.0.21.sql \
	nfiesta_target_data--1.0.21--1.0.22.sql \
	nfiesta_target_data--1.0.22--1.0.23.sql \
	nfiesta_target_data--1.0.23--1.0.24.sql \
	nfiesta_target_data--1.0.24--1.0.25.sql \
	nfiesta_target_data--1.0.25--1.0.26.sql \
	nfiesta_target_data--1.0.26--1.0.27.sql	\
	nfiesta_target_data--1.0.27--1.0.28.sql \
	nfiesta_target_data--1.0.28--1.0.29.sql \
	nfiesta_target_data--1.0.29--1.0.30.sql \
	nfiesta_target_data--1.0.30--1.0.31.sql \
	nfiesta_target_data--1.0.31--1.0.32.sql \
	nfiesta_target_data--1.0.32--1.0.33.sql \
	nfiesta_target_data--1.0.33--1.0.34.sql \
	nfiesta_target_data--1.0.34--1.0.35.sql \
	nfiesta_target_data--1.0.35--1.0.36.sql \
	nfiesta_target_data--1.0.36--1.0.37.sql \
	nfiesta_target_data--1.0.37--1.0.38.sql \
	nfiesta_target_data--1.0.38--1.0.39.sql \
	nfiesta_target_data--1.0.39--1.0.40.sql \
	nfiesta_target_data--1.0.40--1.0.41.sql \
	nfiesta_target_data--1.0.41--1.0.42.sql \
	nfiesta_target_data--1.0.42--1.0.43.sql \
	nfiesta_target_data--1.0.43--1.0.44.sql \
	nfiesta_target_data--1.0.44--1.0.45.sql \
	nfiesta_target_data--1.0.45--1.0.46.sql \
	nfiesta_target_data--1.0.46--1.0.47.sql \
	nfiesta_target_data--1.0.47--1.0.48.sql \
	nfiesta_target_data--1.0.48--1.0.49.sql \
	nfiesta_target_data--1.0.49--1.0.50.sql \
	nfiesta_target_data--1.0.50--1.0.51.sql \
	nfiesta_target_data--1.0.51--1.0.52.sql \
	nfiesta_target_data--1.0.52--1.0.53.sql \
	nfiesta_target_data--1.0.53--1.0.54.sql \
	nfiesta_target_data--1.0.54--1.0.55.sql \
	nfiesta_target_data--1.0.55--1.0.56.sql \
	nfiesta_target_data--1.0.56--1.0.57.sql \
	nfiesta_target_data--1.0.57--1.0.58.sql \
	nfiesta_target_data--1.0.58--1.0.59.sql \
	nfiesta_target_data--1.0.59--1.0.60.sql \
	nfiesta_target_data--1.0.60--1.0.61.sql \
	nfiesta_target_data--1.0.61--1.0.62.sql \
	nfiesta_target_data--1.0.62--1.0.63.sql \
	nfiesta_target_data--1.0.63--1.0.64.sql \
	nfiesta_target_data--1.0.64--1.0.65.sql \
	nfiesta_target_data--1.0.65--1.0.66.sql \
	nfiesta_target_data--1.0.66--2.0.0.sql \
	nfiesta_target_data--2.0.0--2.0.1.sql \
	nfiesta_target_data--2.0.1--2.0.2.sql \
	nfiesta_target_data--2.0.2--2.0.3.sql \
	nfiesta_target_data--2.0.3--2.0.4.sql \
	nfiesta_target_data--2.0.4--2.0.5.sql \
	nfiesta_target_data--2.0.5--2.0.6.sql \
	nfiesta_target_data--2.0.6--2.0.7.sql \
	nfiesta_target_data--2.0.7--2.0.8.sql \
	nfiesta_target_data--2.0.8--2.0.9.sql \
	nfiesta_target_data--2.0.9--2.0.10.sql \
	nfiesta_target_data--2.0.10--2.1.0.sql \
	nfiesta_target_data--2.1.0--2.1.1.sql \
	nfiesta_target_data--2.1.1--2.1.2.sql \
	nfiesta_target_data--2.1.2--2.1.3.sql \
	nfiesta_target_data--2.1.3--2.2.0.sql \
	nfiesta_target_data--2.2.0--2.3.0.sql \
	nfiesta_target_data--2.3.0--2.3.1.sql \
	nfiesta_target_data--2.3.1--2.3.2.sql \
	nfiesta_target_data--2.3.2--2.4.0.sql \
	nfiesta_target_data--2.4.0--2.5.0.sql \
	nfiesta_target_data--2.5.0--2.5.1.sql \
	nfiesta_target_data--2.5.1--2.5.2.sql \
	nfiesta_target_data--2.5.2--2.5.3.sql \
	nfiesta_target_data--2.5.3--2.5.4.sql \
	nfiesta_target_data--2.5.4--2.5.5.sql \
	nfiesta_target_data--2.5.5--2.6.0.sql \
	nfiesta_target_data--2.6.0--2.6.1.sql \
	nfiesta_target_data--2.6.1--2.7.0.sql \
	nfiesta_target_data--2.7.0--2.7.1.sql \
	nfiesta_target_data--2.7.1--2.7.2.sql \
	nfiesta_target_data--2.7.2--2.7.3.sql \
	nfiesta_target_data--2.7.3--2.7.4.sql \
	nfiesta_target_data--2.7.4--2.7.5.sql \
	nfiesta_target_data--2.7.5--2.7.6.sql \
	nfiesta_target_data--2.7.6--2.7.7.sql \
	nfiesta_target_data--2.7.7--2.7.8.sql \
	nfiesta_target_data--2.7.8--2.7.9.sql \
	nfiesta_target_data--2.7.9--2.7.10.sql \
	nfiesta_target_data--2.7.10--2.7.11.sql \
	nfiesta_target_data--2.7.11--2.7.12.sql \
	nfiesta_target_data--2.7.12--2.7.13.sql \
	nfiesta_target_data--2.7.13--2.7.14.sql \
	nfiesta_target_data--2.7.14--2.7.15.sql \
	nfiesta_target_data--2.7.15--2.7.16.sql \
	nfiesta_target_data--2.7.16--2.7.17.sql \
	nfiesta_target_data--2.7.17--2.7.18.sql \
	nfiesta_target_data--2.7.18--2.7.19.sql \
	nfiesta_target_data--2.7.19--2.7.20.sql \
	nfiesta_target_data--2.7.20--2.7.21.sql \
	nfiesta_target_data--2.7.21--2.7.22.sql \
	nfiesta_target_data--2.7.22--2.7.23.sql \
	nfiesta_target_data--2.7.23--2.8.0.sql \
	nfiesta_target_data--2.8.0--2.8.1.sql \
	nfiesta_target_data--2.8.1--2.9.0.sql \
	nfiesta_target_data--2.9.0--2.9.1.sql \
	nfiesta_target_data--2.9.1--2.9.2.sql \
	nfiesta_target_data--2.9.2--2.9.3.sql \
	nfiesta_target_data--2.9.3--2.9.4.sql \
	nfiesta_target_data--2.9.4--2.10.0.sql \
	nfiesta_target_data--2.10.0--2.11.0.sql \
	nfiesta_target_data--2.11.0--2.12.0.sql \
	nfiesta_target_data--2.12.0--2.12.1.sql \
	nfiesta_target_data--2.12.1--2.13.0.sql \
	nfiesta_target_data--2.13.0--2.13.1.sql \
	nfiesta_target_data--2.13.1--2.13.2.sql \
	nfiesta_target_data--2.13.2--2.13.3.sql \
	nfiesta_target_data--2.13.3--2.13.4.sql \
	nfiesta_target_data--2.13.4--2.13.5.sql \
	nfiesta_target_data--2.13.5--2.13.6.sql \
	nfiesta_target_data--2.13.6--2.13.7.sql \
	nfiesta_target_data--2.13.7--2.13.8.sql \
	nfiesta_target_data--2.13.8--2.13.9.sql \
	nfiesta_target_data--2.13.9--2.13.10.sql \
	nfiesta_target_data--2.13.10--2.13.11.sql \
	nfiesta_target_data--2.13.11--2.13.12.sql \
	nfiesta_target_data--2.13.12--2.13.13.sql \
	nfiesta_target_data--2.13.13--2.14.0.sql \
	nfiesta_target_data--2.14.0--2.14.1.sql \
	nfiesta_target_data--2.14.1--2.14.2.sql \
	nfiesta_target_data--2.14.2--2.14.3.sql \
	nfiesta_target_data--2.14.3--2.14.4.sql \
	nfiesta_target_data--2.14.4--2.14.5.sql \
	nfiesta_target_data--2.14.5--2.14.6.sql \
	nfiesta_target_data--2.14.6--2.14.7.sql \
	nfiesta_target_data--2.14.7--2.15.0.sql \
	nfiesta_target_data--2.15.0--2.16.0.sql \
	nfiesta_target_data--2.16.0--2.17.0.sql \
	nfiesta_target_data--2.17.0--2.18.0.sql \
	nfiesta_target_data--2.18.0--2.18.1.sql \
	nfiesta_target_data--2.18.1--2.19.0.sql \
	nfiesta_target_data--2.19.0--2.19.1.sql \
	nfiesta_target_data--2.19.1--2.20.0.sql \
	nfiesta_target_data--2.20.0--2.21.0.sql \
	nfiesta_target_data--2.21.0--2.22.0.sql \
	nfiesta_target_data--2.22.0--2.23.0.sql \
	nfiesta_target_data--2.23.0--2.23.1.sql \
	nfiesta_target_data--2.23.1--2.24.0.sql \
	nfiesta_target_data--2.24.0--2.24.1.sql \
	nfiesta_target_data--2.24.1--2.24.2.sql \
	nfiesta_target_data--2.24.2--2.24.3.sql \
	nfiesta_target_data--2.24.3--2.24.4.sql \
	nfiesta_target_data--2.24.4--2.24.5.sql \
	nfiesta_target_data--2.24.5--2.24.6.sql \
	nfiesta_target_data--2.24.6--2.24.7.sql \
	nfiesta_target_data--2.24.7--2.24.8.sql \
	nfiesta_target_data--2.24.8--2.24.9.sql \
	nfiesta_target_data--2.24.9--2.24.10.sql \
	nfiesta_target_data--2.24.10--2.24.11.sql \
	nfiesta_target_data--2.24.11--2.24.12.sql \
	nfiesta_target_data--2.24.12--2.24.13.sql \
	nfiesta_target_data--2.24.13--2.24.14.sql

REGRESS = install fn_system_utilization

export SRC_DIR := $(shell pwd)

all:

installcheck-all:
	make installcheck REGRESS_OPTS="--dbname=contrib_regression_target_data"
	make installcheck-sdesign
	make installcheck-data
	make installcheck-user
	make installcheck-add
	make installcheck-etl

installcheck-sdesign:
	psql -d postgres  -c "drop database if exists contrib_regression_target_data_sdesign;" -c "create database contrib_regression_target_data_sdesign template contrib_regression_target_data;"
	cd deps/nfiesta_sdesign && make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_target_data_sdesign" REGRESS="sdesign4nfiesta/csv sdesign4nfiesta/fn_import_data sdesign4nfiesta/check_data sdesign4nfiesta/check_triggers"

installcheck-data:
	psql -d postgres  -c "drop database if exists contrib_regression_target_data_data;" -c "create database contrib_regression_target_data_data template contrib_regression_target_data_sdesign;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_target_data_data" REGRESS="nfi_analytical nfi_analytical_data fn_import_data t_plots f_p_stems t_regeneration t_regenerations analyze_data"

installcheck-user:
	psql -d postgres  -c "drop database if exists contrib_regression_target_data_user;" -c "create database contrib_regression_target_data_user template contrib_regression_target_data_data;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_target_data_user" REGRESS="user_data get_data attr_conf_input attr_conf_input_error attr_conf attr_conf_error panel_refyearset analyze_data ldsity_values ldsity_values_history fn_get_attribute_domain user_after_ldsity_values"

installcheck-add:
	psql -d postgres  -c "drop database if exists contrib_regression_target_data_add;" -c "create database contrib_regression_target_data_add template contrib_regression_target_data_user;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_target_data_add" REGRESS="add_plot_level add_error trg_error"

installcheck-etl:
	psql -d postgres  -c "drop database if exists contrib_regression_target_data_etl;" -c "create database contrib_regression_target_data_etl template contrib_regression_target_data_user;"
	make installcheck REGRESS_OPTS="--use-existing --dbname=contrib_regression_target_data_etl" REGRESS="etl_fce import_fce"

purge-db:
	psql -d postgres -c "drop database if exists contrib_regression_target_data;"
	psql -d postgres -c "drop database if exists contrib_regression_target_data_sdesign;"
	psql -d postgres -c "drop database if exists contrib_regression_target_data_data;"
	psql -d postgres -c "drop database if exists contrib_regression_target_data_user;"
	psql -d postgres -c "drop database if exists contrib_regression_target_data_add;"
	psql -d postgres -c "drop database if exists contrib_regression_target_data_etl;"

PG_MAJOR_VERSION := $(shell pg_config --version | sed -E 's/PostgreSQL ([0-9]+).*/\1/')
purge:
	rm -rf /usr/share/postgresql/$(PG_MAJOR_VERSION)/extension/nfiesta_target_data.control /usr/share/postgresql/$(PG_MAJOR_VERSION)/$(MODULEDIR)

# postgres build
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
