--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------
alter table target_data.t_ldsity_values disable trigger trg__ldsity_values__ins;
alter table target_data.t_ldsity_values disable trigger trg__ldsity_values__upd;
--------------------------------------------------------------------;
-- change of area of forests => test save zero value
--------------------------------------------------------------------;
update nfi_analytical.t_plots set is_latest = false where plot = 14937 and reference_year_set = 4;
insert into nfi_analytical.t_plots(id,plot,reference_year_set,fao_fra_forest,land_register,ld_forest,version,is_latest) values
((select max(id) + 1 from nfi_analytical.t_plots),14937,4,200,200,0,100,true);
select * from target_data.fn_save_ldsity_values
(
	(select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
	where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
	and reference_year_set in (select id from sdesign.t_reference_year_set where label = 'NFI2-3_change (2011-2020)')),
		(SELECT t1.target_variable
		FROM
			(SELECT t1.id AS target_variable, array_agg(t2.id) AS cm_ids, array_agg(t3.label_en ORDER BY t3.label_en) AS labels, count(*) AS total
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			WHERE t1.label = 'změna plochy lesa'
			GROUP BY t1.id) AS t1
		WHERE t1.total = 2
		)
);
NOTICE:  _refyearset2panel_mapping_without_zero: {7,11,15}
NOTICE:  doing 1 from 1 arrays
NOTICE:  \nThe 2 new local densities were prepared for 1 plots.
             fn_save_ldsity_values             
-----------------------------------------------
 The ldsity values were prepared successfully.
(1 row)

select id, plot, available_datasets, value, is_latest
from target_data.t_ldsity_values
where available_datasets in
(
select id from target_data.t_available_datasets
	where categorization_setup in (select unnest(
		(select array_agg(t.categorization_setup order by t.categorization_setup) from
			(select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup where ldsity2target_variable in
			(select id from target_data.cm_ldsity2target_variable where target_variable =
				(SELECT t1.target_variable
				FROM
					(SELECT t1.id AS target_variable, array_agg(t2.id) AS cm_ids, array_agg(t3.label_en ORDER BY t3.label_en) AS labels, count(*) AS total
					FROM target_data.c_target_variable AS t1
					INNER JOIN target_data.cm_ldsity2target_variable AS t2
					ON t1.id = t2.target_variable
					INNER JOIN target_data.c_ldsity AS t3
					ON t2.ldsity = t3.id
					WHERE t1.label = 'změna plochy lesa'
					GROUP BY t1.id) AS t1
				WHERE t1.total = 2
				)
			)) as t)))
	and panel in (select unnest((select array_agg(id order by id) from sdesign.t_panel
			  	  where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))))
	and reference_year_set in (select unnest((select array_agg(id order by id) from sdesign.t_reference_year_set
							   where label = 'NFI2-3_change (2011-2020)')))
) order by id;
  id   | plot  | available_datasets | value | is_latest 
-------+-------+--------------------+-------+-----------
 68669 |  8177 |                658 |     1 | t
 68670 |  8177 |                661 |     1 | t
 68671 |  8250 |                658 |     1 | t
 68672 |  8250 |                664 |     1 | t
 68673 |  8316 |                658 |     1 | t
 68674 |  8316 |                664 |     1 | t
 68675 |  8399 |                658 |     1 | t
 68676 |  8399 |                664 |     1 | t
 68677 |  8515 |                658 |     1 | t
 68678 |  8515 |                664 |     1 | t
 68679 |  8547 |                658 |    -1 | t
 68680 |  8547 |                661 |    -1 | t
 68681 |  8628 |                658 |     1 | t
 68682 |  8628 |                664 |     1 | t
 68683 |  8701 |                658 |     1 | t
 68684 |  8701 |                664 |     1 | t
 68685 |  8812 |                658 |     1 | t
 68686 |  8812 |                664 |     1 | t
 68687 |  8899 |                658 |     1 | t
 68688 |  8899 |                661 |     1 | t
 68689 |  9010 |                658 |     1 | t
 68690 |  9010 |                664 |     1 | t
 68691 |  9058 |                658 |     1 | t
 68692 |  9058 |                664 |     1 | t
 68693 |  9131 |                658 |     1 | t
 68694 |  9131 |                664 |     1 | t
 68695 |  9208 |                658 |     1 | t
 68696 |  9208 |                664 |     1 | t
 68697 |  9234 |                658 |    -1 | t
 68698 |  9234 |                664 |    -1 | t
 68699 |  9388 |                658 |     1 | t
 68700 |  9388 |                664 |     1 | t
 68701 |  9526 |                658 |     1 | t
 68702 |  9526 |                664 |     1 | t
 68703 |  9589 |                658 |     1 | t
 68704 |  9589 |                664 |     1 | t
 68705 | 11060 |                659 |     1 | t
 68706 | 11060 |                665 |     1 | t
 68707 | 11078 |                659 |     1 | t
 68708 | 11078 |                665 |     1 | t
 68709 | 11395 |                659 |     1 | t
 68710 | 11395 |                665 |     1 | t
 68711 | 11464 |                659 |     1 | t
 68712 | 11464 |                665 |     1 | t
 68713 | 11568 |                659 |     1 | t
 68714 | 11568 |                665 |     1 | t
 68715 | 11652 |                659 |     1 | t
 68716 | 11652 |                665 |     1 | t
 68717 | 11823 |                659 |     1 | t
 68718 | 11823 |                665 |     1 | t
 68719 | 11961 |                659 |     1 | t
 68720 | 11961 |                662 |     1 | t
 68721 | 12020 |                659 |     1 | t
 68722 | 12020 |                662 |     1 | t
 68723 | 12164 |                659 |    -1 | t
 68724 | 12164 |                662 |    -1 | t
 68725 | 12197 |                659 |    -1 | t
 68726 | 12197 |                665 |    -1 | t
 68727 | 13496 |                660 |     1 | t
 68728 | 13496 |                666 |     1 | t
 68729 | 13630 |                660 |    -1 | t
 68730 | 13630 |                666 |    -1 | t
 68731 | 13812 |                660 |     1 | t
 68732 | 13812 |                666 |     1 | t
 68733 | 13837 |                660 |     1 | t
 68734 | 13837 |                666 |     1 | t
 68735 | 13841 |                660 |     1 | t
 68736 | 13841 |                666 |     1 | t
 68737 | 14008 |                660 |     1 | t
 68738 | 14008 |                666 |     1 | t
 68739 | 14034 |                660 |    -1 | t
 68740 | 14034 |                666 |    -1 | t
 68741 | 14043 |                660 |     1 | t
 68742 | 14043 |                666 |     1 | t
 68743 | 14113 |                660 |     1 | t
 68744 | 14113 |                666 |     1 | t
 68745 | 14247 |                660 |     1 | t
 68746 | 14247 |                666 |     1 | t
 68747 | 14269 |                660 |     1 | t
 68748 | 14269 |                666 |     1 | t
 68749 | 14380 |                660 |     1 | t
 68750 | 14380 |                666 |     1 | t
 68751 | 14398 |                660 |     1 | t
 68752 | 14398 |                666 |     1 | t
 68753 | 14401 |                660 |     1 | t
 68754 | 14401 |                663 |     1 | t
 68755 | 14443 |                660 |     1 | t
 68756 | 14443 |                666 |     1 | t
 68757 | 14502 |                660 |     1 | t
 68758 | 14502 |                666 |     1 | t
 68759 | 14530 |                660 |     1 | t
 68760 | 14530 |                666 |     1 | t
 68761 | 14613 |                660 |     1 | t
 68762 | 14613 |                666 |     1 | t
 68763 | 14637 |                660 |     1 | t
 68764 | 14637 |                666 |     1 | t
 68765 | 14651 |                660 |     1 | t
 68766 | 14651 |                666 |     1 | t
 68767 | 14678 |                660 |     1 | t
 68768 | 14678 |                666 |     1 | t
 68769 | 14753 |                660 |     1 | t
 68770 | 14753 |                666 |     1 | t
 68771 | 14816 |                660 |     1 | t
 68772 | 14816 |                666 |     1 | t
 68773 | 14851 |                660 |    -1 | t
 68774 | 14851 |                663 |    -1 | t
 68775 | 14934 |                660 |     1 | t
 68776 | 14934 |                666 |     1 | t
 68777 | 14935 |                660 |     1 | t
 68778 | 14935 |                666 |     1 | t
 68779 | 14937 |                660 |     1 | f
 68780 | 14937 |                666 |     1 | f
 68781 | 14981 |                660 |    -1 | t
 68782 | 14981 |                663 |    -1 | t
 68783 | 14937 |                660 |     0 | t
 68784 | 14937 |                666 |     0 | t
(116 rows)

--------------------------------------------------------------------;
--------------------------------------------------------------------;
--------------------------------------------------------------------;
-- change of area of forests => add new local density
--------------------------------------------------------------------;
update nfi_analytical.t_plots set is_latest = false where plot = 8156 and reference_year_set = 4;
insert into nfi_analytical.t_plots(id,plot,reference_year_set,fao_fra_forest,land_register,ld_forest,version,is_latest) values
((select max(id) + 1 from nfi_analytical.t_plots),8156,4,100,100,1,100,true);
select * from target_data.fn_save_ldsity_values
(
	(select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
	where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
	and reference_year_set in (select id from sdesign.t_reference_year_set where label = 'NFI2-3_change (2011-2020)')),
		(SELECT t1.target_variable
		FROM
			(SELECT t1.id AS target_variable, array_agg(t2.id) AS cm_ids, array_agg(t3.label_en ORDER BY t3.label_en) AS labels, count(*) AS total
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			WHERE t1.label = 'změna plochy lesa'
			GROUP BY t1.id) AS t1
		WHERE t1.total = 2
		)
);
NOTICE:  _refyearset2panel_mapping_without_zero: {7,11,15}
NOTICE:  doing 1 from 1 arrays
NOTICE:  \nThe 2 new local densities were prepared for 1 plots.
             fn_save_ldsity_values             
-----------------------------------------------
 The ldsity values were prepared successfully.
(1 row)

select id, plot, available_datasets, value, is_latest
from target_data.t_ldsity_values
where available_datasets in
(
select id from target_data.t_available_datasets
	where categorization_setup in (select unnest(
		(select array_agg(t.categorization_setup order by t.categorization_setup) from
			(select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup where ldsity2target_variable in
			(select id from target_data.cm_ldsity2target_variable where target_variable =
				(SELECT t1.target_variable
				FROM
					(SELECT t1.id AS target_variable, array_agg(t2.id) AS cm_ids, array_agg(t3.label_en ORDER BY t3.label_en) AS labels, count(*) AS total
					FROM target_data.c_target_variable AS t1
					INNER JOIN target_data.cm_ldsity2target_variable AS t2
					ON t1.id = t2.target_variable
					INNER JOIN target_data.c_ldsity AS t3
					ON t2.ldsity = t3.id
					WHERE t1.label = 'změna plochy lesa'
					GROUP BY t1.id) AS t1
				WHERE t1.total = 2
				)
		)) as t)))
	and panel in (select unnest((select array_agg(id order by id) from sdesign.t_panel
			  	  where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))))
	and reference_year_set in (select unnest((select array_agg(id order by id) from sdesign.t_reference_year_set
							   where label = 'NFI2-3_change (2011-2020)')))
) order by id;
  id   | plot  | available_datasets | value | is_latest 
-------+-------+--------------------+-------+-----------
 68669 |  8177 |                658 |     1 | t
 68670 |  8177 |                661 |     1 | t
 68671 |  8250 |                658 |     1 | t
 68672 |  8250 |                664 |     1 | t
 68673 |  8316 |                658 |     1 | t
 68674 |  8316 |                664 |     1 | t
 68675 |  8399 |                658 |     1 | t
 68676 |  8399 |                664 |     1 | t
 68677 |  8515 |                658 |     1 | t
 68678 |  8515 |                664 |     1 | t
 68679 |  8547 |                658 |    -1 | t
 68680 |  8547 |                661 |    -1 | t
 68681 |  8628 |                658 |     1 | t
 68682 |  8628 |                664 |     1 | t
 68683 |  8701 |                658 |     1 | t
 68684 |  8701 |                664 |     1 | t
 68685 |  8812 |                658 |     1 | t
 68686 |  8812 |                664 |     1 | t
 68687 |  8899 |                658 |     1 | t
 68688 |  8899 |                661 |     1 | t
 68689 |  9010 |                658 |     1 | t
 68690 |  9010 |                664 |     1 | t
 68691 |  9058 |                658 |     1 | t
 68692 |  9058 |                664 |     1 | t
 68693 |  9131 |                658 |     1 | t
 68694 |  9131 |                664 |     1 | t
 68695 |  9208 |                658 |     1 | t
 68696 |  9208 |                664 |     1 | t
 68697 |  9234 |                658 |    -1 | t
 68698 |  9234 |                664 |    -1 | t
 68699 |  9388 |                658 |     1 | t
 68700 |  9388 |                664 |     1 | t
 68701 |  9526 |                658 |     1 | t
 68702 |  9526 |                664 |     1 | t
 68703 |  9589 |                658 |     1 | t
 68704 |  9589 |                664 |     1 | t
 68705 | 11060 |                659 |     1 | t
 68706 | 11060 |                665 |     1 | t
 68707 | 11078 |                659 |     1 | t
 68708 | 11078 |                665 |     1 | t
 68709 | 11395 |                659 |     1 | t
 68710 | 11395 |                665 |     1 | t
 68711 | 11464 |                659 |     1 | t
 68712 | 11464 |                665 |     1 | t
 68713 | 11568 |                659 |     1 | t
 68714 | 11568 |                665 |     1 | t
 68715 | 11652 |                659 |     1 | t
 68716 | 11652 |                665 |     1 | t
 68717 | 11823 |                659 |     1 | t
 68718 | 11823 |                665 |     1 | t
 68719 | 11961 |                659 |     1 | t
 68720 | 11961 |                662 |     1 | t
 68721 | 12020 |                659 |     1 | t
 68722 | 12020 |                662 |     1 | t
 68723 | 12164 |                659 |    -1 | t
 68724 | 12164 |                662 |    -1 | t
 68725 | 12197 |                659 |    -1 | t
 68726 | 12197 |                665 |    -1 | t
 68727 | 13496 |                660 |     1 | t
 68728 | 13496 |                666 |     1 | t
 68729 | 13630 |                660 |    -1 | t
 68730 | 13630 |                666 |    -1 | t
 68731 | 13812 |                660 |     1 | t
 68732 | 13812 |                666 |     1 | t
 68733 | 13837 |                660 |     1 | t
 68734 | 13837 |                666 |     1 | t
 68735 | 13841 |                660 |     1 | t
 68736 | 13841 |                666 |     1 | t
 68737 | 14008 |                660 |     1 | t
 68738 | 14008 |                666 |     1 | t
 68739 | 14034 |                660 |    -1 | t
 68740 | 14034 |                666 |    -1 | t
 68741 | 14043 |                660 |     1 | t
 68742 | 14043 |                666 |     1 | t
 68743 | 14113 |                660 |     1 | t
 68744 | 14113 |                666 |     1 | t
 68745 | 14247 |                660 |     1 | t
 68746 | 14247 |                666 |     1 | t
 68747 | 14269 |                660 |     1 | t
 68748 | 14269 |                666 |     1 | t
 68749 | 14380 |                660 |     1 | t
 68750 | 14380 |                666 |     1 | t
 68751 | 14398 |                660 |     1 | t
 68752 | 14398 |                666 |     1 | t
 68753 | 14401 |                660 |     1 | t
 68754 | 14401 |                663 |     1 | t
 68755 | 14443 |                660 |     1 | t
 68756 | 14443 |                666 |     1 | t
 68757 | 14502 |                660 |     1 | t
 68758 | 14502 |                666 |     1 | t
 68759 | 14530 |                660 |     1 | t
 68760 | 14530 |                666 |     1 | t
 68761 | 14613 |                660 |     1 | t
 68762 | 14613 |                666 |     1 | t
 68763 | 14637 |                660 |     1 | t
 68764 | 14637 |                666 |     1 | t
 68765 | 14651 |                660 |     1 | t
 68766 | 14651 |                666 |     1 | t
 68767 | 14678 |                660 |     1 | t
 68768 | 14678 |                666 |     1 | t
 68769 | 14753 |                660 |     1 | t
 68770 | 14753 |                666 |     1 | t
 68771 | 14816 |                660 |     1 | t
 68772 | 14816 |                666 |     1 | t
 68773 | 14851 |                660 |    -1 | t
 68774 | 14851 |                663 |    -1 | t
 68775 | 14934 |                660 |     1 | t
 68776 | 14934 |                666 |     1 | t
 68777 | 14935 |                660 |     1 | t
 68778 | 14935 |                666 |     1 | t
 68779 | 14937 |                660 |     1 | f
 68780 | 14937 |                666 |     1 | f
 68781 | 14981 |                660 |    -1 | t
 68782 | 14981 |                663 |    -1 | t
 68783 | 14937 |                660 |     0 | t
 68784 | 14937 |                666 |     0 | t
 68785 |  8156 |                658 |     1 | t
 68786 |  8156 |                661 |     1 | t
(118 rows)

--------------------------------------------------------------------;
--------------------------------------------------------------------;
alter table target_data.t_ldsity_values enable trigger trg__ldsity_values__ins;
alter table target_data.t_ldsity_values enable trigger trg__ldsity_values__upd;
