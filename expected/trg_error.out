--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
-- simulation using triggers in cm_adc2classrule2panel_refyearset table
-- ok
delete from target_data.cm_adc2classrule2panel_refyearset 
where adc2classification_rule = (SELECT id FROM target_data.cm_adc2classification_rule WHERE ldsity_object = 100 AND area_domain_category = 
					(SELECT id FROM target_data.c_area_domain_category WHERE label_en = 'entire area of geographical domain')) and refyearset2panel in (4,5,6,8,9,10,12,13,14);
-- error - at least 1 record
delete from target_data.cm_adc2classrule2panel_refyearset 
where adc2classification_rule = (SELECT id FROM target_data.cm_adc2classification_rule WHERE ldsity_object = 100 AND area_domain_category = 
					(SELECT id FROM target_data.c_area_domain_category WHERE label_en = 'entire area of geographical domain')) and refyearset2panel = 2;
ERROR:  Error: 01: fn_trg_check_cm_adc2classrule2panel_refyearset: This record [adc2classification_rule = 1, refyearset2panel = 2] cannot be deleted from the cm_adc2classrule2panel_refyearset table, becouse at least one record for a given classification rule must remain in the table.
CONTEXT:  PL/pgSQL function target_data.fn_trg_check_cm_adc2classrule2panel_refyearset() line 8 at RAISE
-- ukey
update target_data.cm_adc2classrule2panel_refyearset 
set adc2classification_rule = (SELECT id FROM target_data.cm_adc2classification_rule WHERE ldsity_object = 100 AND area_domain_category = 
					(SELECT id FROM target_data.c_area_domain_category WHERE label_en = 'forest')) 
where adc2classification_rule = (SELECT id FROM target_data.cm_adc2classification_rule WHERE ldsity_object = 100 AND area_domain_category = 
					(SELECT id FROM target_data.c_area_domain_category WHERE label_en = 'non-forest')) and refyearset2panel = 2;
ERROR:  duplicate key value violates unique constraint "ukey__cm_adc2classrule2panel_refyearset"
DETAIL:  Key (adc2classification_rule, refyearset2panel)=(2, 2) already exists.
-- simulation using triggers in cm_spc2classrule2panel_refyearset table
-- ok
delete from target_data.cm_spc2classrule2panel_refyearset 
where spc2classification_rule = (SELECT id FROM target_data.cm_spc2classification_rule WHERE ldsity_object = 200 AND sub_population_category = 
					(SELECT id FROM target_data.c_sub_population_category WHERE label_en = 'dead stem')) and refyearset2panel in (4,5,9,13);
-- error - at least 1 record
delete from target_data.cm_spc2classrule2panel_refyearset 
where spc2classification_rule = (SELECT id FROM target_data.cm_spc2classification_rule WHERE ldsity_object = 200 AND sub_population_category = 
					(SELECT id FROM target_data.c_sub_population_category WHERE label_en = 'dead stem')) and refyearset2panel = 2;
ERROR:  Error: 01: fn_trg_check_cm_spc2classrule2panel_refyearset: This record [spc2classification_rule = 1, refyearset2panel = 2] cannot be deleted from the cm_spc2classrule2panel_refyearset table, becouse at least one record for a given classification rule must remain in the table.
CONTEXT:  PL/pgSQL function target_data.fn_trg_check_cm_spc2classrule2panel_refyearset() line 8 at RAISE
-- ukey
update target_data.cm_spc2classrule2panel_refyearset 
set spc2classification_rule = (SELECT id FROM target_data.cm_spc2classification_rule WHERE ldsity_object = 200 AND sub_population_category = 
					(SELECT id FROM target_data.c_sub_population_category WHERE label_en = 'live stem')) 
where spc2classification_rule = (SELECT id FROM target_data.cm_spc2classification_rule WHERE ldsity_object = 200 AND sub_population_category = 
					(SELECT id FROM target_data.c_sub_population_category WHERE label_en = 'conifers')) and refyearset2panel = 2;
ERROR:  duplicate key value violates unique constraint "ukey__cm_spc2classrule2panel_refyearset"
DETAIL:  Key (spc2classification_rule, refyearset2panel)=(2, 2) already exists.
-- simulation using trigger in cm_adc2classification_rule (non-logical combination of category and object but for the principle of the error check it doesn't matter)
insert into target_data.cm_adc2classification_rule(area_domain_category,ldsity_object,classification_rule) values (101,200,'plot is null');
ERROR:  Error: 01: fn_trg_check_cm_adc2classification_rule: For new inserted classification rule: [area_domain_category = 101, ldsity_object = 200, classification_rule = "plot is null", use_negative = f], not exists any record in table cm_adc2classrule2panel_refyearset!
CONTEXT:  PL/pgSQL function target_data.fn_trg_check_cm_adc2classification_rule() line 11 at RAISE
-- simulation using trigger in cm_spc2classification_rule (non-logical combination of category and object but for the principle of the error check it doesn't matter)
insert into target_data.cm_spc2classification_rule(sub_population_category,ldsity_object,classification_rule) values (101,100,'dead_stem in (100,200)');
ERROR:  Error: 01: fn_trg_check_cm_spc2classification_rule: For new inserted classification rule: [sub_population_category = 101, ldsity_object = 100, classification_rule = "dead_stem in (100,200)", use_negative = f], not exists any record in table cm_adc2classrule2panel_refyearset!
CONTEXT:  PL/pgSQL function target_data.fn_trg_check_cm_spc2classification_rule() line 12 at RAISE
-- simulation using trigger in t_available_datasets = update column ldsity_threshold [KO]
update target_data.t_available_datasets
set ldsity_threshold = 0.0
where panel = (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a'))
and reference_year_set = (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
and categorization_setup =	(
							WITH w_all AS (
							SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
							FROM target_data.c_target_variable AS t1
							INNER JOIN target_data.cm_ldsity2target_variable AS t2
							ON t1.id = t2.target_variable
							WHERE t1.label_en = 'forest area' AND t2.ldsity_object_type = 100 AND t2.area_domain_category IS NULL
							),
							w_tv AS (
								SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
								FROM w_all AS t1
							),
							w_fn AS (
								SELECT t2.target_variable
								FROM 	w_tv AS t1,
									target_data.fn_get_ldsity(t1.variables,100) AS t2
							), w_agg AS (
								SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
								FROM w_fn AS t1
								INNER JOIN w_all AS t2
								ON t1.target_variable = t2.target_variable
								GROUP BY t1.target_variable
							)
							select cltcs.categorization_setup from target_data.cm_ldsity2target2categorization_setup as cltcs
							where cltcs.ldsity2target_variable in (select unnest(cm_ids) from w_agg)
							and cltcs.adc2classification_rule is null and cltcs.spc2classification_rule is null
							);
-- simulation using trigger in t_available_datasets = update column last_change [KO]
update target_data.t_available_datasets
set last_change = now()
where panel = (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a'))
and reference_year_set = (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
and categorization_setup =	(
							WITH w_all AS (
							SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
							FROM target_data.c_target_variable AS t1
							INNER JOIN target_data.cm_ldsity2target_variable AS t2
							ON t1.id = t2.target_variable
							WHERE t1.label_en = 'forest area' AND t2.ldsity_object_type = 100 AND t2.area_domain_category IS NULL
							),
							w_tv AS (
								SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
								FROM w_all AS t1
							),
							w_fn AS (
								SELECT t2.target_variable
								FROM 	w_tv AS t1,
									target_data.fn_get_ldsity(t1.variables,100) AS t2
							), w_agg AS (
								SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
								FROM w_fn AS t1
								INNER JOIN w_all AS t2
								ON t1.target_variable = t2.target_variable
								GROUP BY t1.target_variable
							)
							select cltcs.categorization_setup from target_data.cm_ldsity2target2categorization_setup as cltcs
							where cltcs.ldsity2target_variable in (select unnest(cm_ids) from w_agg)
							and cltcs.adc2classification_rule is null and cltcs.spc2classification_rule is null
							);
-- simulation using trigger in t_available_datasets = update column ldsity_threshold [OK]
update target_data.t_available_datasets
set ldsity_threshold = 0.0000000001
where panel = (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a'))
and reference_year_set = (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
and categorization_setup =	(
							WITH w_all AS (
							SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
							FROM target_data.c_target_variable AS t1
							INNER JOIN target_data.cm_ldsity2target_variable AS t2
							ON t1.id = t2.target_variable
							WHERE t1.label_en = 'forest area' AND t2.ldsity_object_type = 100 AND t2.area_domain_category IS NULL
							),
							w_tv AS (
								SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
								FROM w_all AS t1
							),
							w_fn AS (
								SELECT t2.target_variable
								FROM 	w_tv AS t1,
									target_data.fn_get_ldsity(t1.variables,100) AS t2
							), w_agg AS (
								SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
								FROM w_fn AS t1
								INNER JOIN w_all AS t2
								ON t1.target_variable = t2.target_variable
								GROUP BY t1.target_variable
							)
							select cltcs.categorization_setup from target_data.cm_ldsity2target2categorization_setup as cltcs
							where cltcs.ldsity2target_variable in (select unnest(cm_ids) from w_agg)
							and cltcs.adc2classification_rule is null and cltcs.spc2classification_rule is null
							);
