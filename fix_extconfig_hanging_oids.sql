--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--run with psql

-- remove hanging oids due to bug (https://www.postgresql.org/message-id/flat/15616-260dc9cb3bec7e7e%40postgresql.org)
with w_deps as (
	select
		unnest(extconfig) as extconfig,
		unnest(extcondition) as extcondition
	from pg_extension
	where extname = 'nfiesta_target_data'
)
, w_deps_agg as (
	select
		array_agg(extconfig order by extconfig) as extconfig_arr,
		array_agg(extcondition order by extconfig) as extcondition_arr
	from w_deps
	inner join pg_class on w_deps.extconfig = pg_class.oid
)
update pg_extension set
	extconfig = w_deps_agg.extconfig_arr,
	extcondition = w_deps_agg.extcondition_arr
from w_deps_agg
where pg_extension.extname = 'nfiesta_target_data'
;
