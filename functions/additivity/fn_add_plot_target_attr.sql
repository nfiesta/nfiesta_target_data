--
-- Copyright 2017, 2025 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_add_plot_target_attr
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_add_plot_target_attr(integer[], integer[], integer[], integer[], double precision, boolean);

CREATE OR REPLACE FUNCTION target_data.fn_add_plot_target_attr
(
	IN variables integer[],
	IN plots integer[] default NULL,
	IN panels integer[] default NULL,
	IN refyearsets integer[] default NULL,
	IN min_diff double precision default 0.0,
	IN include_null_diff boolean default true
)
  RETURNS TABLE
(
	plot			integer,
	panel			integer,
	reference_year_set	integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision	
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text_v text;
	_array_text_p text;
	_array_text_r text;
	_array_text_pan text;

BEGIN
	--------------------------------QUERY--------------------------------
	--_array_text_v := concat(quote_literal(variables::text), '::integer[]');

	if plots is not null then
		_array_text_p := concat('AND t8.gid = ANY (', quote_literal(plots::text), '::integer[])');
	else
		_array_text_p := '';
	end if;

	if panels is not null then
		_array_text_pan := concat('AND t3.panel = ANY (', quote_literal(panels::text), '::integer[])');
	else
		_array_text_pan := '';
	end if;

	if refyearsets is not null then
		_array_text_r := concat(' AND t3.reference_year_set = ANY (', quote_literal(refyearsets::text), '::integer[])');
	else
		_array_text_r := '';
	end if;

	_complete_query :=
	'
	with
	w_cat_setups AS (
		SELECT DISTINCT cs_id_sup, ad_id_inf, sp_id_inf --, cs_id_inf
		FROM target_data.v_variable_hierarchy_internal
		WHERE 
			(array[cs_id_sup] <@ $1 OR
			array[cs_id_inf] <@ $1)
	),
	w_plot_var as materialized
					(
		SELECT
			t2.cs_id_sup AS node, 
			t2.cs_id_inf AS edge_def,
			t2.ad_id_inf, t2.sp_id_inf,
			t3.panel, 
			t3.reference_year_set,
			t8.gid AS plot,
			coalesce(t10.value,0) AS value_sup,
			coalesce(t9.value,0) AS value_inf
		FROM
				w_cat_setups AS t1
		INNER JOIN	target_data.v_variable_hierarchy_internal AS t2 	ON t1.cs_id_sup = t2.cs_id_sup AND t1.ad_id_inf = t2.ad_id_inf AND t1.sp_id_inf = t2.sp_id_inf
		INNER JOIN	target_data.t_available_datasets AS t3			ON t2.cs_id_inf = t3.categorization_setup 
		INNER JOIN 	target_data.t_available_datasets AS t4			ON t2.cs_id_sup = t4.categorization_setup AND t3.panel = t4.panel AND t3.reference_year_set = t4.reference_year_set
		INNER JOIN 	sdesign.t_panel AS t5 					ON t4.panel = t5.id
		INNER JOIN 	sdesign.cm_cluster2panel_mapping AS t6 			ON t5.id = t6.panel
		INNER JOIN 	sdesign.t_cluster AS t7					ON t6.cluster = t7.id
		INNER JOIN 	sdesign.f_p_plot AS t8					ON t7.id = t8.cluster
		LEFT JOIN
			target_data.t_ldsity_values AS t9				ON t3.id = t9.available_datasets AND t8.gid = t9.plot AND t9.is_latest
		LEFT JOIN
			target_data.t_ldsity_values AS t10				ON t4.id = t10.available_datasets AND t8.gid = t10.plot AND t10.is_latest
		WHERE true
			' || _array_text_p || '
			' || _array_text_r || '
			' || _array_text_pan || '
		--GROUP BY t2.cs_id_sup, t3.panel, t3.reference_year_set, t6.value
	)
	,w_edge_sum as	(
		SELECT
			plot,
			panel,
			reference_year_set,
			node,
			value_sup AS node_sum,
			array_agg(edge_def ORDER BY edge_def) AS edges_def,
			array_agg(edge_def ORDER BY edge_def) AS edges_found,
			sum(value_inf) AS edges_sum
		FROM w_plot_var
		GROUP BY plot, panel, reference_year_set, node, ad_id_inf, sp_id_inf, value_sup
	)
	,w_diff as		(
					select
						plot,
						panel,
						reference_year_set,
						node		as variable,
						node_sum	as ldsity,
						edges_sum	as ldsity_sum,
						edges_def	as variables_def,
						edges_found	as variables_found,
						case
							when (abs(edges_sum) >  1e-12) and (abs(node_sum) <= 1e-12) then 1.0
							when (abs(edges_sum) <= 1e-12) and (abs(node_sum) <= 1e-12) then 0
							else abs( (node_sum - edges_sum) / node_sum )
						end as diff
					from w_edge_sum
					)
	select * from w_diff
	where
		(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
		or
		(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
	order by diff desc
	;
	';
	--raise notice '%		fn_add_plot_target_attr ', TimeOfDay(); --, _complete_query;
	RETURN QUERY EXECUTE _complete_query USING variables;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
  
comment on function target_data.fn_add_plot_target_attr(integer[], integer[], integer[], integer[], double precision, boolean) is
'Function showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Array of plots -- plots (FKEY to f_p_plot.gid). If NULL all available plots are checked.
 * Array of reference year sets -- reference year sets (FKEY to t_reference_year_set.id). If NULL all available reference year sets are checked.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Reference year set. FKEY to t_reference_year_set.id.
 * Target total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION target_data.fn_add_plot_target_attr(integer[], integer[], integer[], integer[], double precision, boolean) TO PUBLIC;
