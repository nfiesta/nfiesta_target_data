#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse

from etl_db import db_conn
from etl_etl import etl_complete

def main():
    parser = argparse.ArgumentParser(description="Utility for ETL nfiesta_target_data -> nfiesta_pg.",
            formatter_class=argparse.RawDescriptionHelpFormatter,
            epilog='''\
Example:
    template -> changescript
        Keyword/Value Connection Strings
            python3 etl.py -cs='host=localhost dbname=postgres user=vagrant'
            python3 etl.py -cs 'host=localhost dbname=postgres user=vagrant'
            python3 etl.py --connectionstring='host=localhost dbname=postgres user=vagrant'
            python3 etl.py --connectionstring 'host=localhost dbname=postgres user=vagrant'
        Connection URIs
            python3 etl.py -cs='postgresql://vagrant@localhost/postgres'
            ''')
    parser.add_argument('-cs', '--connectionstring', required=True, help='libpq connection string [SOURCE DB]: https://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-CONNSTRING . For password use ~/.pgpass file.')
    parser.add_argument('-ni', '--noninteractive', required=False, help='script for non-interactive run (to be done)')

    args = parser.parse_args()
    
    conn_src = db_conn(args.connectionstring)
    etl_complete(conn_src)
    conn_src.close()

if __name__ == '__main__':
    main()
