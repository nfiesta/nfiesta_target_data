#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import psycopg2
import psycopg2.extras
import json
from psycopg2 import sql

###########################################################     DB CONN

connstr = input("Define a label of the new import connection:\nexample (without quotes): 'dbname=MyDB host=CanBeOmmitedForLinuxBasedAuthentication user=MyUser' for password use ~/.pgpass file\n")

try:
    # alter database contrib_regression_user rename to nfi_analytical;
    conn_src = psycopg2.connect(connstr)
except psycopg2.DatabaseError as e:
    print("nfiesta_parallel.single_query(), not possible to connect DB")
    print(e)
    sys.exit(1)

def run_sql(_conn, _sql_cmd, _data):
    cur = _conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    try:
        cur.execute(_sql_cmd, _data)
    except psycopg2.Error as e:
        print("etl_py, SQL error:")
        print(e)
        sys.exit(1)

    if cur.description != None:
        path_row_list = cur.fetchall()
    else:
        path_row_list = None

    return path_row_list

    cur.close()

###########################################################     ETL
print('--------------------------------------------------------------')
print('ETL')
print('--------------------------------------------------------------')

###############################################################################
#####################################################     Set National Language
print('--------------------------------------------------------------')
print('ETL - Set National Language')
print('--------------------------------------------------------------')
while True:
    national_language = input("Set national language (cs/en):\n")
    if not (national_language == 'cs' or national_language == 'en'):
        print("Sorry, not known.")
        continue
    else:
        break
print('National language was set to: %s' % (national_language))
print('--------------------------------------------------------------')
###############################################################################



###############################################################################
#####################################################     Connections
print('ETL - Export connections')
print('--------------------------------------------------------------')
###############################################     Node 1
print('Node 1 - get export connections')
sql_cmd = '''select count(*) as res_conn_node_1 from target_data.fn_etl_get_export_connections();'''
data = (None, )
res_conn_node1 = run_sql(conn_src, sql_cmd, data)

if res_conn_node1[0]['res_conn_node_1'] == 0:
    ###############################################     Node 2
    print('Node 2 - Exists any record? -> NO -> next step: Node 3')
    ###############################################     Node 3
    print('Node 3 - Define a label and description of the new export connection -> next step: Node 4')
    node3_conn_label = input("Node 3 - Define a label of the new export connection:\nexample (without quotes): 'dbname=MyDB host=CanBeOmmitedForLinuxBasedAuthentication'\n")
    node3_conn_description = input("Node 3 - Define a description of the new export connection:\n")
    ###############################################     Node 4
    print('Node 4 - save export connection -> next step: Node 6')
    sql_cmd = '''select target_data.fn_etl_save_export_connection(%s,%s,%s,%s) as res_conn_node_4;'''
    data = (node3_conn_label,node3_conn_description,node3_conn_label,node3_conn_description)
    res_conn_node4 = run_sql(conn_src, sql_cmd, data)[0]['res_conn_node_4']
else:
    ###############################################     Node 2
    print('Node 2 - Exists any record? -> YES -> Display list of export connections -> next step: Node 5')
    sql_cmd = '''select id, label from target_data.fn_etl_get_export_connections();'''
    print('List of export connections:')
    data = (None, )
    res_conn_node2 = run_sql(conn_src, sql_cmd, data)
    print('-------')
    for i, r in enumerate(res_conn_node2):
        print('ID: %s   LABEL: %s' % (i+1, r['label']))
    print('-------')

    while True:
        ###############################################     Node 5
        node5_conn = input("Node 5 - Is the current list of connections sufficient? (y/n):\n")
        if not (node5_conn == 'y' or node5_conn == 'n'):
            print("Sorry, not known.")
            continue
        else:
            break

    if node5_conn == 'y':
        print('YES -> next step: Node 6')
    elif node5_conn == 'n':
        ###############################################     Node 3
        print('Node 3 - Define a label and description of then new export connection -> next step: Node 4')
        node3_conn_label = input("Node 3 - 1. Define a label of the new export connection:\nexample (without quotes): 'dbname=MyDB host=CanBeOmmitedForLinuxBasedAuthentication'\n")
        node3_conn_description = input("Node 3 - 2. Define a description of the new export connection:\n")
        ###############################################     Node 4
        print('Node 4 - save export connection')
        sql_cmd = '''select target_data.fn_etl_save_export_connection(%s,%s,%s,%s) as res_conn_node_4;'''
        data = (node3_conn_label,node3_conn_description,node3_conn_label,node3_conn_description)
        res_conn_node4 = run_sql(conn_src, sql_cmd, data)[0]['res_conn_node_4']

###############################################     Node 6
print('Node 6 - Informative list of export connections:')
sql_cmd = '''select id, label from target_data.fn_etl_get_export_connections();'''
data = (None, )
res_conn_node6 = run_sql(conn_src, sql_cmd, data)
print('-------')
for i, r in enumerate(res_conn_node6):
    print('ID: %s   LABEL: %s' % (i+1, r['label']))
print('-------')
###############################################     Node 7
node7_conn_pyarray = input("Node 7 - Set ID of export connection:\n")
node7_conn = res_conn_node6[int(node7_conn_pyarray)-1]['id']
###############################################     Node 8
try:
    # alter database contrib_regression_add rename to nfi_esta;
    # connstr = 'dbname=nfi_esta user=vagrant' # for local linux based authentication (without password)
    # connstr = dbname=contrib_regression_etl user=vagrant # for testing etl.py
    connstr = res_conn_node6[int(node7_conn_pyarray)-1]['label']
    node8_conn_user = input("Node 8. Define user for selected connection:\nexample (without quotes): 'user=MyUser' for password use ~/.pgpass file\n")
    connstr="%s %s"%(connstr, node8_conn_user)
    conn_dest = psycopg2.connect(connstr)
except psycopg2.DatabaseError as e:
    print("nfiesta_parallel.single_query(), not possible to connect DB")
    print(e)
    sys.exit(1)
conn_src.commit()

print('--------------------------------------------------------------')
###############################################################################



###############################################################################
# get schema name from target DB
###############################################################################
sql_cmd = '''select nspname from pg_namespace where oid = (select extnamespace from pg_extension where extname = 'nfiesta');'''
data = (None, )
schemaname = run_sql(conn_dest, sql_cmd, data)[0][0]
###############################################################################



###############################################################################
#####################################################     Target variable - change METADATA
print('ETL - Target variable - change METADATA')
print('--------------------------------------------------------------')

###############################################     Node 1
print('Node 1 - get target variables [SOURCE DB]')
sql_cmd = '''select id, label, description, label_en, description_en from target_data.c_target_variable order by id;'''
data = (None,)
res_target_variable_node1 = run_sql(conn_src, sql_cmd, data)

print('List of target variables [SOURCE DB]:')
for r in res_target_variable_node1:
    print('ID: %s   LABEL: %s   DESCRIPTION: %s   LABEL_EN: %s    DESRIPTION_EN: %s' % (r['id'], r['label'], r['description'], r['label_en'], r['description_en']))
        
###############################################     Node 2         
print('Node 2 - set ID of target variable for changing METADATA [SOURCE DB]')
node2_target_variable = input("Set ID of target variable for changing METADATA [SOURCE DB]:\n")        
               
###############################################     Node 3        
print('Node 3 - get values from t_et_target_variable for target variable [SOURCE DB]')
sql_cmd = '''select id, export_connection, target_variable, etl_id from target_data.t_etl_target_variable where export_connection = %s and target_variable = %s;'''
data = (node7_conn,node2_target_variable)
res_target_variable_node3 = run_sql(conn_src, sql_cmd, data)

print('List of target variables [SOURCE DB]:')
for r in res_target_variable_node3:
    print('ID: %s   EXPORT_CONNECTION: %s   TARGET_VARIABLE: %s   ETL_ID: %s' % (r['id'], r['export_connection'], r['target_variable'], r['etl_id']))

###############################################     Node 4
print('Node 4 - set ETL_ID of target variable for changing METADATA [SOURCE DB]')
node4_target_variable= input("Set ETL_ID of target variable for changing METADATA [TARGET DB]:\n")

###############################################     Node 5
print('Node 5 - get target variable metadata [SOURCE DB]')
sql_cmd = '''select target_data.fn_etl_get_target_variable_metadata(%s,%s) as res_target_variable_node_5;'''
data = (node2_target_variable, national_language)
res_target_variable_node5 = run_sql(conn_src, sql_cmd, data)[0]['res_target_variable_node_5']
print(res_target_variable_node5)

###############################################     Node 6
print('Node 6 - update target variable metadata [TARGET DB]')
sql_cmd = sql.SQL('''update {}.c_target_variable set metadata = %s where id = %s;''').format(sql.Identifier(schemaname))
metadata = (json.dumps(res_target_variable_node5, ensure_ascii=False),node4_target_variable)
res_target_variable_node6 = run_sql(conn_dest, sql_cmd, metadata)
###############################################################################



###############################################################################
#######################################################   COMMIT CONFIRMATION
while True:
    commit_conn = input("Confirm a commit of ETL process? (y/n):\n")
    if not (commit_conn == 'y' or commit_conn == 'n'):
        print("Sorry, not known.")
        continue
    else:
        break

if commit_conn == 'y':
    print('ETL process was commited.')
    conn_src.commit()
    conn_dest.commit()
elif commit_conn == 'n':
    print('ETL process was canceled !!!')
###############################################################################



###############################################################################
###########################################################     DB CONN CLOSE
conn_src.close()
conn_dest.close()
###############################################################################
