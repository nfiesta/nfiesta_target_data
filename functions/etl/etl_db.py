#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import psycopg2
import psycopg2.extras
from psycopg2 import sql

def db_conn(connstr):
    print(psycopg2.extensions.parse_dsn(connstr))
    try:
        conn_src = psycopg2.connect(connstr)
    except psycopg2.DatabaseError as e:
        print("nfiesta_parallel.single_query(), not possible to connect DB")
        print(e)
        sys.exit(1)
    return conn_src

def run_sql(_conn, _sql_cmd, _data):
    cur = _conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    try:
        cur.execute(_sql_cmd, _data)
    except psycopg2.Error as e:
        print("etl_py, SQL error:")
        print(e)
        sys.exit(1)

    if cur.description != None:
        path_row_list = cur.fetchall()
    else:
        path_row_list = None

    return path_row_list
    cur.close()
