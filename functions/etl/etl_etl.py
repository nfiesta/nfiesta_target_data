#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import datetime
import tabulate

import json
from psycopg2 import sql

from etl_db import run_sql
from etl_db import db_conn
from tabulate import tabulate

def etl_input(inpm):
    inps = None
    while inps is None:
        try:
            inps = input(inpm)
        except Exception as e:
            print('invalid input')
            print(e)
            pass
    return inps

def etl_complete(conn_src_param):
    ###############################################################################
    ###########################################################     ETL
    print('--------------------------------------------------------------')
    print('ETL')
    print('--------------------------------------------------------------')
    ###############################################################################

    # while True:
    #     loop_1 = etl_input("Solve an update labels or descriptions of categories for any selected AREA DOMAIN? (y/n):\n")
    #     if not (loop_1 == 'y' or loop_1 == 'n'):
    #         print("Sorry, not known.")
    #         continue
    #     else:
    #         break 
    # if loop_1 == 'y':
    #     print('Node 1')
    #     value_node_1 = 1 # nastaveni node 1
    #     if value_node_1 == 0:
    #         print('Node 2 - neexistuje zadna ETL-kovana plosna domena -> dalsi krok musi byt CONTINUE ETL')
    #     else:
    #         print('Node 2 - existuje alespon jedna ETL-kovana plosna domena')
    #         print('Node 3')
    #         value_node_3 = 1 # nastaveni node 3
    #         if value_node_3 == 0:
    #             print('Node 4 - neexistuje zadna plosna domena s rozdilem u kategorii')
    #         else:
    #             print('Node 4 - existuje alespon jedna plosna domena s rozdilem u kategorii')
    #             print('vypsany seznam plosnych domen')
    #             while True:
    #                 loop_2 = etl_input("chces resit update kategorii u nektere z techto plosnych domen? (y/n):\n")
    #                 if not (loop_2 == 'y' or loop_2 == 'n'):
    #                     print("Sorry, not known.")
    #                     continue
    #                 else:
    #                     break 
    #             if loop_2 == 'y':
    #                 print('uzivatel CHCE resit update kategorii u nektere plosne domeny')
    #                 #-------------------------------------------------------------------#
    #                 while True:
    #                     print('vypsany seznam plosnych domen v cyklu')
    #                     print('vyber ID_enumerate plosne domeny k reseni')
    #                     print('vypsany seznam kategorii vybrane plosne domeny')
    #                     while True:
    #                         loop_3 = etl_input("chces resit update nektere z kategorii? (y/n):\n")
    #                         if not (loop_3 == 'y' or loop_3 == 'n'):
    #                             print("Sorry, not known.")
    #                             continue
    #                         else:
    #                             break 
    #                     if loop_3 == 'y':
    #                         print('uzivatel CHCE resit update nektere kategoriie')
    #                         #----------------------------------------------------#
    #                         while True:
    #                             print('vypsany seznam kategorii v cyklu')
    #                             print('vyber ID_enumerate kategorie k reseni')
    #                             while True:
    #                                 loop_5 = etl_input("chces resit dalsi kategorii? (y/n):\n")
    #                                 if not (loop_5 == 'y' or loop_5 == 'n'):
    #                                     print("Sorry, not know.")
    #                                     continue
    #                                 else:
    #                                     break
    #                             if loop_5 == 'y':
    #                                 print('ANO, uzivatel chce resit dalsi kategorii')
    #                                 print('zjisteni, jestli existuje jeste nejaka rozdilna kategorie')
    #                                 value_node_x2 = 2 # nastaveni
    #                                 if value_node_x2 == 0:
    #                                     print('uz neexistuje zadna rozdilna kategorie')
    #                                     break
    #                                 else:
    #                                     print('jeste existje alespon jedna rozdilna kategorie')
    #                                     continue
    #                             elif loop_5 == 'n':
    #                                 print('NE, uzivatel uz nechce resit dalsi kategorii')
    #                                 break
    #                     elif loop_3 == 'n':
    #                         print('uzivatel NECHCE resit update nektere kategoriie')
    #                     while True:
    #                         loop_4 = etl_input("chces resit dalsi plosnou domenu? (y/n):\n")
    #                         if not (loop_4 == 'y' or loop_4 == 'n'):
    #                             print("Sorry, not known.")
    #                             continue
    #                         else:
    #                             break
    #                     if loop_4 == 'y':
    #                         print('ANO, uzivatel chce resit dalsi plosnou domenu')
    #                         print('zjisteni jestli existuje jeste nejaka plosna domena, ktera rozdilne kategorie')
    #                         value_node_x1 = 1 # nastaveni
    #                         if value_node_x1 == 0:
    #                             print('uz neexistuje zadna plosna domena s rozdily u kategorii')
    #                             break
    #                         else:
    #                             print('jeste existuje alespon jedna plosna domena s rozdily u kategorii')
    #                             continue
    #                     elif loop_4 == 'n':
    #                         print('NE, uzivatel uz neche resit dalsi plosnou domenu')
    #                         break     
    #                 #-------------------------------------------------------------------#
    #             elif loop_2 == 'n':
    #                 print('uzivatel NECHCE resit update kategorii u zadne plosne domeny -> dalsi krok musi byt CONTINUE ETL')
    #     ####################################
    #     ####################################
    #     while True:            
    #         check_continue = etl_input("Node 15 - Continue with process ETL? (y/n):\n")
    #         if not (check_continue == 'y' or check_continue == 'n'):
    #             print("Sorry, not known.")
    #             continue
    #         else:
    #             break
    #     if check_continue == 'y':
    #         print('YES -> User wants to continue with process ETL. Next step: Node E')
    #     else:
    #         print('NO -> User does not want to continue with process ETL. Next step: STOP process of ETL.')
    #         conn_src.close()
    #         conn_dest.close()
    #         sys.exit(1)
    # elif loop_1 == 'n':
    #     print('NO -> User does not want to make an update of label and description of categories for any AREA DOMAIN.')    

    # while True:
    #     ###############################################
    #     loop_1 = etl_input("Solve an update labels or descriptions of categories for any selected AREA DOMAIN? (y/n):\n")
    #     if not (loop_1 == 'y' or loop_1 == 'n'):
    #         print("Sorry, not known.")
    #         continue
    #     else:
    #         break 
    # if loop_1 == 'y':
    #     while True:
    #         print('process "select ID_ENUMERATE of AREA DOMAIN"')
    #         #################################
    #         print("list of categories for selected AREA DOMAIN")
    #         while True:
    #             loop_3 = etl_input("Solve an update of any AREA DOMAIN CATEGORY label or description? (y/n):\n")
    #             if not (loop_3 == 'y' or loop_3 == 'n'):
    #                 print("Sorry, not known.")
    #                 continue
    #             else:
    #                 break
    #         if loop_3 == 'y':
    #             while True:
    #                 print('process FROM "select ID_ENUMERATE of AREA DOMAIN CATEGORY for update" TO commit')
    #                 while True:
    #                     loop_4 = etl_input("Make an update for next AREA DOMAIN CATEGORY? (y/n):\n")
    #                     if not (loop_4 == 'y' or loop_4 == 'n'):
    #                         print("Sorry, not known.")
    #                         continue
    #                     else:
    #                         break
    #                 if loop_4 == 'y':
    #                     continue
    #                 elif loop_4 == 'n':
    #                     break           
    #         elif loop_3 == 'n':
    #             print('NO -> User does not want to make an update of any AREA DOMAIN CATEGORY label or description.')                    
    #         #################################            
    #         while True:
    #             loop_2 = etl_input("select next AREA DOMAIN? (y/n):\n")
    #             if not (loop_2 == 'y' or loop_2 == 'n'):
    #                 print("Sorry, not known.")
    #                 continue
    #             else:
    #                 break
    #         if loop_2 == 'y':
    #             continue
    #         elif loop_4 == 'n':
    #             break           
    # elif loop_1 == 'n':
    #     print('NO -> User does not want to make an update of label and description of categories for any AREA DOMAIN.')


    # while True:
    #     ###############################################
    #     loop_1 = etl_input("Solve an update of any AREA DOMAIN label or description? (y/n):\n")
    #     if not (loop_1 == 'y' or loop_1 == 'n'):
    #         print("Sorry, not known.")
    #         continue
    #     else:
    #         break 
    # if loop_1 == 'y':
    #     while True:
    #         print('process FROM "select ID_ENUMERATE of AREA DOMAIN for update" TO commit')
    #         while True:
    #             loop_2 = etl_input("Make an update for next AREA DOMAIN? (y/n):\n")
    #             if not (loop_2 == 'y' or loop_2 == 'n'):
    #                 print("Sorry, not known.")
    #                 continue
    #             else:
    #                 break
    #         if loop_2 == 'y':
    #             continue
    #         elif loop_2 == 'n':
    #             break           
    # elif loop_1 == 'n':
    #     print('NO -> User does not want to make an update of any AREA DOMAIN label or description.')



    ###############################################################################
    #####################################################     ACCESS
    print('ETL - ACCESS')
    print('--------------------------------------------------------------')
    ###############################################
    node_access_module = 'n'
    print('The access for the script is set to "Access from module ETL of field data"')
    print('--------------------------------------------------------------')
    ###############################################################################



    ###############################################################################
    ###########################################################     DB CONN [SOURCE DB]
    print('ETL - DB CONN [SOURCE DB]')
    print('--------------------------------------------------------------')
    if node_access_module == 'n':
        conn_src = conn_src_param
    else:
        print('The access for the script is not set to "Access from module ETL of field data"')
        sys.exit(1)
    print('--------------------------------------------------------------')
    ###############################################################################



    ###############################################################################
    #####################################################     Set National Language
    print('--------------------------------------------------------------')
    print('ETL - Set National Language')
    print('--------------------------------------------------------------')
    while True:
        national_language = etl_input("Set national language (cs/en):\n")
        if not (national_language == 'cs' or national_language == 'en'):
            print("Sorry, not known.")
            continue
        else:
            break
    print('National language was set to: %s' % (national_language))
    print('--------------------------------------------------------------')
    ###############################################################################



    ###############################################################################
    #####################################################     Connections
    print('ETL - Export connections')
    print('--------------------------------------------------------------')
    ###############################################     Node 1
    print('Node 1 - get export connections')
    sql_cmd_conn_node1_count = '''select count(*) as res_conn_node_1 from target_data.fn_etl_get_export_connections();'''
    sql_cmd_conn_node1_list = '''select id, host, dbname, port, comment from target_data.fn_etl_get_export_connections();'''
    data_conn_node1 = (None, )
    res_conn_node1_count = run_sql(conn_src, sql_cmd_conn_node1_count, data_conn_node1)[0]['res_conn_node_1'] 
    res_conn_node1_list = run_sql(conn_src, sql_cmd_conn_node1_list, data_conn_node1)
    ###############################################     Node 2
    print('Node 2 - Display list of export connections -> next step: Node 3')
    if res_conn_node1_count == 0:
        print('-------')
        print('EMPTY')
        print('-------')
    else:
        print('-------')
        for i, r in enumerate(res_conn_node1_list):
            print('ID: %s   HOST: %s    DBNAME: %s  PORT: %s    COMMENT: %s' % (i+1, r['host'], r['dbname'], r['port'], r['comment']))
        print('-------')
    ###############################################     Node 3
    while True:
        node3_conn = etl_input("Node 3 - Is the current list of connections sufficient and correct? (y/n):\n")
        if not (node3_conn == 'y' or node3_conn == 'n'):
            print("Sorry, not known.")
            continue
        else:
            break

    if node3_conn == 'y':
        print('YES -> next step: Node 16')
    else:
        print('NO -> next step: Node 4')
        ###############################################     Node 4
        while True:
            node4_conn_action = etl_input("Node 4 - Set action: (insert/update/delete):\n")
            if not (node4_conn_action == 'insert' or node4_conn_action == 'update' or node4_conn_action == 'delete'):
                print("Sorry, not known.")
                continue
            else:
                break

        if node4_conn_action == 'insert':
            ###############################################     Node 5
            print('Node 5 - Define an attributes of the new export connection -> next step: Node 6')
            node5_conn_host = etl_input("Node 5 - Define a host of the new export connection:\nexample (without quotes): 'localhost'\n")
            node5_conn_dbname = etl_input("Node 5 - Define a dbname of the new export connection:\nexample (without quotes): 'contrib_regression_nfiesta_sdesign'\n")
            node5_conn_port = etl_input("Node 5 - Define a port of the new export connection:\nexample (without quotes): '5432'\n")
            node5_conn_comment = etl_input("Node 5 - Define a comment of the new export connection:\nexample (without quotes): 'test'\n")        
            ###############################################     Node 6
            # TEST SIGN INTO TARGET DB
            print('Node 6 - TEST sign in target database')
            node5_connstr = "host=%s dbname=%s port=%s"%(node5_conn_host,node5_conn_dbname,node5_conn_port)
            node5_conn_user = etl_input("Node 5 - Define user for the new export connection:\nexample (without quotes): 'MyUser' for password use ~/.pgpass file\n")
            node5_connstr="%s user=%s"%(node5_connstr, node5_conn_user)
            conn_dest = db_conn(node5_connstr)
            ###############################################     Node 7
            print('Node 7 - Connection succeeded')
            ###############################################     Node 8
            print('Node 8 - save export connection -> next step: Node 16 [over Nodes 9,1,2,3]')
            sql_cmd_conn_node8 = '''select target_data.fn_etl_save_export_connection(%s,%s,%s,%s) as res_conn_node_8;'''
            data_conn_node8 = (node5_conn_host,node5_conn_dbname,int(node5_conn_port),node5_conn_comment)
            res_conn_node8 = run_sql(conn_src, sql_cmd_conn_node8, data_conn_node8)[0]['res_conn_node_8']
            ###############################################     Node 9
            print('Node 9 - COMMIT [source DB]')
            conn_src.commit()
            conn_dest.close()
            ###############################################
        else:
            if node4_conn_action == 'update':
                node4_conn_pyarray = etl_input("Node 4 - Set ID of export connection for UPDATE:\n")
            else:
                node4_conn_pyarray = etl_input("Node 4 - Set ID of export connection for DELETE:\n")

            node4_conn_id = res_conn_node1_list[int(node4_conn_pyarray)-1]['id']

            #################################################################################### Node 10
            if node4_conn_action == 'update':
                ####################################### SET COMMENT
                node10_conn_comment = etl_input("Node 10 - Define a new comment:\nexample (without quotes): 'test'\n")
                #######################################
                # UPDATE
                print('Node 10 - UPDATE export connection -> next step: Node 14')
                sql_cmd_conn_node10 = '''select * from target_data.fn_etl_update_export_connection(%s,%s);'''
                data_conn_node10 = (node4_conn_id,node10_conn_comment)
                res_conn_node10 = run_sql(conn_src, sql_cmd_conn_node10, data_conn_node10)
                #######################################
            #################################################################################### Node 11,12,13
            if node4_conn_action == 'delete':
                sql_cmd_conn_node11 = '''select count(t.*) from (select fn_etl_try_delete_export_connection from target_data.fn_etl_try_delete_export_connection(%s)) as t where t.fn_etl_try_delete_export_connection = true;'''
                data_conn_node11 = (node4_conn_id,)
                res_conn_node11 = run_sql(conn_src, sql_cmd_conn_node11, data_conn_node11)[0][0]
                #print(res_conn_node11)

                if res_conn_node11 == 0:
                    print('Node 12 - EXPORT CONNECTION IS NOT ALLOWED TO DELETE !!! -> next step: STOP process ETL [In GUI apllication next step will be Node 1]')
                    sys.exit(1)
                else:
                    # DELETE
                    print('Node 13 - DELETE export connection -> next step: Node 14')
                    sql_cmd_conn_node13 = '''select * from target_data.fn_etl_delete_export_connection(%s);'''
                    data_conn_node13 = (node4_conn_id,)
                    res_conn_node13 = run_sql(conn_src, sql_cmd_conn_node13,data_conn_node13)
            ####################################################################################
            ###############################################################################
            #######################################################   CONFIRM CHANGES
            ##############################################     CHM - Node 14
            while True:
                commit_conn_chm = etl_input("Node 14 - Confirm a commit of UPDATE or DELETE connection? (y/n):\n")
                if not (commit_conn_chm == 'y' or commit_conn_chm == 'n'):
                    print("Sorry, not known.")
                    continue
                else:
                    break

            if commit_conn_chm == 'y':
                conn_src.commit()

                if node4_conn_action == 'update':
                    print('Node 14 -> YES -> Process of UPDATE export connection was commited. Next step: Node 15')
                else:
                    print('Node 14 -> YES -> Process of DELETE export connection was commited. Next step: Node 15')

            elif commit_conn_chm == 'n':
                if node4_conn_action == 'update':
                    print('Node 14 -> NO -> Process of UPDATE export connection was NOT commited. Next step: Node 15')
                else:
                    print('Node 14 -> NO -> Process of DELETE export connection was NOT commited. Next step: Node 15')

            while True:
                check_chm_continue = etl_input("CHM - Node 15 - Continue with process ETL? (y/n):\n")
                if not (check_chm_continue == 'y' or check_chm_continue == 'n'):
                    print("Sorry, not known.")
                    continue
                else:
                    break

            if check_chm_continue == 'y':
                print('YES -> User wants to continue with process ETL. Next step: Node 16 [over Nodes 1,2,3]')
            else:
                ###############################################################################
                ###########################################################     DB CONN CLOSE
                print('NO -> User does not want to continue with process ETL. Next step: STOP process of ETL.')
                conn_src.close()
                sys.exit(1)
            ###############################################################################

    ####################################################################################
    # INFORMATIVE LIST OF CONNECTIONS for Node no. 16 if user solved some of actions
    if node3_conn == 'n':
        sql_cmd_conn_node1_count = '''select count(*) from target_data.fn_etl_get_export_connections();'''
        sql_cmd_conn_node1_list = '''select id, host, dbname, port, comment from target_data.fn_etl_get_export_connections();'''
        data_conn_node1 = (None, )
        res_conn_node1_count = run_sql(conn_src, sql_cmd_conn_node1_count, data_conn_node1)[0][0]
        res_conn_node1_list = run_sql(conn_src, sql_cmd_conn_node1_list, data_conn_node1)
        if res_conn_node1_count == 0:
            print('List of export connections is EMPTY -> next step: STOP process ETL')
            sys.exit(1)
        else:
            print('List of export connections:')
            print('-------')
            for i, r in enumerate(res_conn_node1_list):
                print('ID: %s   HOST: %s    DBNAME: %s  PORT: %s    COMMENT: %s' % (i+1, r['host'], r['dbname'], r['port'], r['comment']))
            print('-------')
    ####################################################################################

    ###############################################     Node 16
    node16_conn_pyarray = etl_input("Node 16 - Set ID of export connection:\n")
    node7_conn = res_conn_node1_list[int(node16_conn_pyarray)-1]['id']
    ###############################################     Node 17
    connstr_host = res_conn_node1_list[int(node16_conn_pyarray)-1]['host']
    connstr_dbname = res_conn_node1_list[int(node16_conn_pyarray)-1]['dbname']
    connstr_port = res_conn_node1_list[int(node16_conn_pyarray)-1]['port']
    connstr = "host=%s dbname=%s port=%s"%(connstr_host,connstr_dbname,connstr_port)
    node8_conn_user = etl_input("Node 17. Define user for selected connection:\nexample (without quotes): 'MyUser' for password use ~/.pgpass file\n")
    connstr="%s user=%s"%(connstr, node8_conn_user)
    conn_dest = db_conn(connstr)
    print('--------------------------------------------------------------')
    ###############################################################################



    ###############################################################################
    # get schema name from target DB
    ###############################################################################
    sql_cmd = '''select nspname from pg_namespace where oid = (select extnamespace from pg_extension where extname = 'nfiesta');'''
    data = (None, )
    schemaname = run_sql(conn_dest, sql_cmd, data)[0][0]
    #print(schemaname)
    #sql_cmd = sql.SQL('''select label from {}.c_sub_population where id = %s''').format(sql.Identifier(schemaname))
    #data = (1, )
    #label_1 = run_sql(conn_dest, sql_cmd, data)[0][0]
    #print(label_1)
    ###############################################################################



    ##########################################################################################
    # Node A
    ##########################################################################################
    print('--------------------------------------------------------------')
    print('Node A - Is access from module "Field data"?')
    if node_access_module == 'n':
        # Node B
        print('No - next step: Node B')
        print('--------------------------------------------------------------')
        ##########################################################################################
        ##########################################################################################
        ####### CHECK OF METADATA FOR TARGET VARIABLE that were ETL yet and DATA are CURRENT #####
        ##########################################################################################
        while True:
            ###############################################
            check_chm_tv_ok = etl_input("Node B - Make a check of metadatas of target variable that were ETL yet and theirs DATAs are CURRENT? (y/n):\n")
            if not (check_chm_tv_ok == 'y' or check_chm_tv_ok == 'n'):
                print("Sorry, not known.")
                continue
            else:
                break

        if check_chm_tv_ok == 'y':
            print('YES -> Check of metadatas for target variable that were ETL yet and theirs datas are current.')
            ##########################################################################################
            ##############################################     CHM - Node 1
            print('CHM - Node 1 - get target variable [SOURCE DB]')
            sql_cmd_chm_node_1_list = '''select id, label, id_etl_target_variable, check_target_variable, refyearset2panel_mapping, metadata from target_data.fn_etl_get_target_variable(%s,%s,%s);'''
            sql_cmd_chm_node_1_count = '''select array_agg(id_etl_target_variable order by id_etl_target_variable) as res_chm_node_1_count from target_data.fn_etl_get_target_variable(%s,%s,%s);'''
            data_true = 'true'
            data = (node7_conn,national_language,data_true)
            res_chm_node1_list = run_sql(conn_src, sql_cmd_chm_node_1_list, data)
            res_chm_node1_count = run_sql(conn_src, sql_cmd_chm_node_1_count, data)[0]['res_chm_node_1_count']
            #print(res_chm_node1_count) #[1]
            ##############################################     CHM - Node 2
            if res_chm_node1_count == None:
                print('CHM - Node 2 - Exists any record? -> NO => Not exists any target variable. Next step: Node 12.')

                while True:
                    check_chm_continue = etl_input("CHM - Node 12 - Continue with process ETL? (y/n):\n")
                    if not (check_chm_continue == 'y' or check_chm_continue == 'n'):
                        print("Sorry, not known.")
                        continue
                    else:
                        break

                if check_chm_continue == 'y':
                    print('YES -> User wants to continue with process ETL. Next step: ETL - Sampling Design Synchronization')
                else:
                    print('NO -> User does not want to continue with process ETL. Next step: STOP process of ETL.')
                    sys.exit(1)            
            else:
                print('CHM - Node 2 - Exists any record? -> YES -> next step: Node 3')
                ##############################################     CHM - Node 3
                print('CHM - Node 3 - List of target variables [SOURCE DB]:')
                for r in res_chm_node1_list:
                    print('ID: %s   LABEL: %s   ID_ETL_TV: %s   CHECK_TV: %s    RYS2PM: %s  METADATA: %s' % (r['id'], r['label'], r['id_etl_target_variable'], r['check_target_variable'], r['refyearset2panel_mapping'], r['metadata']))

                while True:
                    ##############################################     CHM - Node 4
                    check_chm_tv_ok_node_4 = etl_input("Make a check of metadatas? (y/n):\n")
                    if not (check_chm_tv_ok_node_4 == 'y' or check_chm_tv_ok_node_4 == 'n'):
                        print("Sorry, not known.")
                        continue
                    else:
                        break

                if check_chm_tv_ok_node_4 == 'y':
                    print('YES -> User wants to make a check of metadatas -> next step: Node 5')
                    ##############################################     CHM - Node 5
                    print('CHM - Node 5 - get target variable metadatas [SOURCE DB]:')
                    sql_cmd_chm_node_5 = '''select target_data.fn_etl_get_target_variable_metadatas(%s,%s) as res_chm_node_5;'''
                    data = (res_chm_node1_count,national_language)
                    res_chm_node5 = run_sql(conn_src, sql_cmd_chm_node_5, data)[0]['res_chm_node_5']
                    #print(res_chm_node5)
                    ##############################################     CHM - Node 6
                    print('CHM - Node 6 - check target variable metadatas [TARGET DB] -> next step: Node 7')
                    sql_cmd_chm_node_6_list = sql.SQL('''select target_variable_source, target_variable_target, metadata_source, metadata_target, metadata_diff, metadata_diff_national_language, metadata_diff_english_language, missing_metadata_national_language from {}.fn_etl_check_target_variable_metadatas(%s,%s,%s);''').format(sql.Identifier(schemaname))
                    sql_cmd_chm_node_6_count = sql.SQL('''select count(*) as res_chm_node_6_count from {}.fn_etl_check_target_variable_metadatas(%s,%s,%s);''').format(sql.Identifier(schemaname))
                    data_true = 'true'
                    metadata = (json.dumps(res_chm_node5, ensure_ascii=False),national_language,data_true)
                    res_chm_node6_list = run_sql(conn_dest, sql_cmd_chm_node_6_list, metadata)
                    res_chm_node6_count = run_sql(conn_dest, sql_cmd_chm_node_6_count, metadata)[0]['res_chm_node_6_count']
                    #print(res_chm_node6_count)
                    ##############################################     CHM - Node 7
                    if res_chm_node6_count == 0:
                        print('CHM - Node 7 - Exists any record? -> NO -> Next step: Node 14.')

                        while True:
                            check_chm_continue = etl_input("CHM - Node 14 - Continue with process ETL? (y/n):\n")
                            if not (check_chm_continue == 'y' or check_chm_continue == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if check_chm_continue == 'y':
                            print('YES -> User wants to continue with process ETL. Next step: ETL - Sampling Design Synchronization')
                        else:
                            print('NO -> User does not want to continue with process ETL. Next step: STOP process of ETL.')
                            conn_src.close()
                            conn_dest.close()
                            sys.exit(1)
                    else:
                        print('CHM - Node 7 - Exists any record? -> YES -> Metadatas by some of target variables are different. Next step: Node 8.')
                        ##############################################     CHM - Node 8
                        print('CHM - Node 8 - List of target variables [SOURCE DB]:')
                        for i,r in enumerate(res_chm_node6_list):
                            print('ID: %s   TVSD: %s   TVTD: %s   MSD: %s   MTD: %s  MDIFF: %s  MDIFFNL: %s MDIFFEL: %s MISSMDNL: %s' % (i+1, r['target_variable_source'], r['target_variable_target'], r['metadata_source'], r['metadata_target'], r['metadata_diff'], r['metadata_diff_national_language'], r['metadata_diff_english_language'], r['missing_metadata_national_language']))
                        ##############################################     CHM - Node 9
                        while True:
                            ##############################################     CHM - Node 9
                            check_chm_tv_ok_node_9 = etl_input("CHM - Node 9 - Solve an update or insert of metadata for selected target variable? (y/n):\n")
                            if not (check_chm_tv_ok_node_9 == 'y' or check_chm_tv_ok_node_9 == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if check_chm_tv_ok_node_9 == 'y':
                            print('YES -> User wants to solve an update or insert of metadata for selected target variable -> next step: Node 10')
                            ##############################################     CHM - Node 10
                            check_chm_tv_ok_node_10_id = etl_input("CHM - Node 10 - Set ID from Node 8 [by the user selected record for update or insert]:\n")

                            while True:
                                check_chm_tv_ok_node_10_action = etl_input("CHM - Node 10 - Set an action for by user selected target varible to change: example (without quotes): 'update' or 'insert'\n")
                                if not (check_chm_tv_ok_node_10_action == 'update' or check_chm_tv_ok_node_10_action == 'insert'):
                                    print("Sorry, not known.")
                                    continue
                                else:
                                    break

                            if check_chm_tv_ok_node_10_action == 'insert':
                                ##############################################     CHM - Node 12
                                print('CHM - Node 12 - INSERT target variable metadata -> next step: Node 13')
                                sql_cmd_chm_node_12_insert = sql.SQL('''select fn_etl_insert_target_variable_metadata as res_chm_node_12_insert from {}.fn_etl_insert_target_variable_metadata(%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                                data_chm_node_12_insert =  (
                                                            res_chm_node6_list[int(check_chm_tv_ok_node_10_id)-1]['target_variable_target'],
                                                            json.dumps((res_chm_node6_list[int(check_chm_tv_ok_node_10_id)-1]['metadata_source']), ensure_ascii=False),
                                                            res_chm_node6_list[int(check_chm_tv_ok_node_10_id)-1]['missing_metadata_national_language'],
                                                            national_language
                                                            )
                                res_chm_node12_insert = run_sql(conn_dest, sql_cmd_chm_node_12_insert, data_chm_node_12_insert)[0]['res_chm_node_12_insert']
                                #print(res_chm_node12_insert)
                            else:
                                ## branch for 'UPDATE'
                                ##############################################     CHM - Node 10 [SET attributes for languages]
                                while True:
                                    check_chm_tv_ok_node_10_el_boolean = etl_input("CHM - Node 10 - Set english language for update (true/false):\n")
                                    if not (check_chm_tv_ok_node_10_el_boolean == 'true' or check_chm_tv_ok_node_10_el_boolean == 'false'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break

                                while True:
                                    check_chm_tv_ok_node_10_nl_boolean = etl_input("CHM - Node 10 - Set national language for update (true/false):\n")
                                    if not (check_chm_tv_ok_node_10_nl_boolean == 'true' or check_chm_tv_ok_node_10_nl_boolean == 'false'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break                           
                                ##############################################     CHM - Node 11
                                print('CHM - Node 11 - UPDATE target variable metadata -> next step: Node 13')
                                sql_cmd_chm_node_11_update = sql.SQL('''select fn_etl_update_target_variable_metadata as res_chm_node_11_update from {}.fn_etl_update_target_variable_metadata(%s,%s,%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                                data_chm_node_11_update =  (
                                                            res_chm_node6_list[int(check_chm_tv_ok_node_10_id)-1]['target_variable_target'],
                                                            json.dumps((res_chm_node6_list[int(check_chm_tv_ok_node_10_id)-1]['metadata_source']), ensure_ascii=False),
                                                            res_chm_node6_list[int(check_chm_tv_ok_node_10_id)-1]['metadata_diff_national_language'],
                                                            res_chm_node6_list[int(check_chm_tv_ok_node_10_id)-1]['metadata_diff_english_language'],
                                                            res_chm_node6_list[int(check_chm_tv_ok_node_10_id)-1]['missing_metadata_national_language'],
                                                            check_chm_tv_ok_node_10_el_boolean,
                                                            check_chm_tv_ok_node_10_nl_boolean,
                                                            national_language
                                                            )
                                res_chm_node11_update = run_sql(conn_dest, sql_cmd_chm_node_11_update, data_chm_node_11_update)[0]['res_chm_node_11_update']
                                #print(res_chm_node11_update)
                            ###############################################################################
                            #######################################################   CONFIRM CHANGES
                            ##############################################     CHM - Node 13
                            while True:
                                commit_conn_chm = etl_input("Node 13 - Confirm a commit of UPDATE or INSERT metadatas? (y/n):\n")
                                if not (commit_conn_chm == 'y' or commit_conn_chm == 'n'):
                                    print("Sorry, not known.")
                                    continue
                                else:
                                    break

                            if commit_conn_chm == 'y':
                                conn_src.commit()
                                conn_dest.commit()

                                if check_chm_tv_ok_node_10_action == 'insert':
                                    print('Node 13 -> YES -> Process of INSERT metadatas was commited. Next step: Node 14')
                                else:
                                    print('Node 13 -> YES ->Process of UPDATE metadatas was commited. Next step: Node 14')

                            elif commit_conn_chm == 'n':
                                if check_chm_tv_ok_node_10_action == 'insert':
                                    print('Node 13 -> NO -> Process of INSERT metadatas was NOT commited. Next step: Node 14')
                                else:
                                    print('Node 13 -> NO -> Process of UPDATE metadatas was NOT commited. Next step: Node 14')

                            while True:
                                check_chm_continue = etl_input("CHM - Node 14 - Continue with process ETL? (y/n):\n")
                                if not (check_chm_continue == 'y' or check_chm_continue == 'n'):
                                    print("Sorry, not known.")
                                    continue
                                else:
                                    break

                            if check_chm_continue == 'y':
                                print('YES -> User wants to continue with process ETL. Next step: Node C')
                            else:
                                ###############################################################################
                                ###########################################################     DB CONN CLOSE
                                print('NO -> User does not want to continue with process ETL. Next step: STOP process of ETL.')
                                conn_src.close()
                                conn_dest.close()
                                sys.exit(1)
                            ###############################################################################

                        elif check_chm_tv_ok_node_9 == 'n':
                            print('NO -> User do not wants to solve an update of metadatas -> next step: Node 14.')

                            while True:                    
                                check_chm_continue = etl_input("CHM - Node 14 - Continue with process ETL? (y/n):\n")
                                if not (check_chm_continue == 'y' or check_chm_continue == 'n'):
                                    print("Sorry, not known.")
                                    continue
                                else:
                                    break

                            if check_chm_continue == 'y':
                                print('YES -> User wants to continue with process ETL. Next step: Node C')
                            else:
                                print('NO -> User does not want to continue with process ETL. Next step: STOP process of CHECK.')
                                conn_src.close()
                                conn_dest.close()
                                sys.exit(1)                  

                elif check_chm_tv_ok_node_4 == 'n':
                    print('NO -> User does not want to make a check of metadatas -> next step: Node 14')

                    while True:            
                        check_chm_continue = etl_input("CHM - Node 14 - Continue with process ETL? (y/n):\n")
                        if not (check_chm_continue == 'y' or check_chm_continue == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break

                    if check_chm_continue == 'y':
                        print('YES -> User wants to continue with process ETL. Next step: Node C')
                    else:
                        print('NO -> User does not want to continue with process ETL. Next step: STOP process of ETL.')
                        conn_src.close()
                        conn_dest.close()
                        sys.exit(1)            

            ##########################################################################################
        elif check_chm_tv_ok == 'n':
            print('NO -> User does not want to make a check of metadatas -> next step: Node C')
        print('--------------------------------------------------------------')
        ##########################################################################################
        ##########################################################################################



        ##########################################################################################
        # Node C
        ##########################################################################################
        while True:
            ###############################################
            check_node_c = etl_input("Node C - Make a check of AREA DOMAIN labels and descriptions that had already been ETL? (y/n):\n")
            if not (check_node_c == 'y' or check_node_c == 'n'):
                print("Sorry, not known.")
                continue
            else:
                break

        if check_node_c == 'y':
            ###############################################     Node 1
            print('Node 1 - get_area_domains4update (SOURCE) -> next step: Node 2')
            sql_cmd_node_c_1 = '''select target_data.fn_etl_get_area_domains4update(%s) as res_node_c1;'''
            data_node_c_1 = (node7_conn,)
            res_node_c_1 = run_sql(conn_src, sql_cmd_node_c_1, data_node_c_1)[0]['res_node_c1']
            ###############################################     Node 2
            if res_node_c_1 == None:
                print('Node 2 - Any record returned? -> NO -> next step: Node 10') # not exists any ETLed area domain
            else:
                print('Node 2 - Any record returned? -> YES -> next step: Node 3')
                ###############################################     Node 3
                print('Node 3 - check_area_domains4update (TARGET) - next step: Node 4')
                sql_cmd_node_c_3_count = sql.SQL('''select count(*) from {}.fn_etl_check_area_domains4update(%s);''').format(sql.Identifier(schemaname))
                sql_cmd_node_c_3_list = sql.SQL('''select area_domain, label_source, description_source, label_en_source, description_en_source, label_target, description_target, label_en_target, description_en_target, check_label, check_description, check_label_en, check_description_en from {}.fn_etl_check_area_domains4update(%s) order by area_domain;''').format(sql.Identifier(schemaname))
                data_node_c_3 = (json.dumps(res_node_c_1, ensure_ascii=False), )
                res_node_c_3_count = run_sql(conn_dest, sql_cmd_node_c_3_count, data_node_c_3)[0][0]
                res_node_c_3_list = run_sql(conn_dest, sql_cmd_node_c_3_list, data_node_c_3)
                ###############################################     Node 4
                if res_node_c_3_count == 0:
                    print('Node 4 - Any record returned? -> NO -> Not exists any AREA DOMAIN with differences.')
                else:
                    print('Node 4 - Any record returned? -> YES -> Exists AREA DOMAIN with differences. -> Next step: Node 5')
                    ###############################################     Node 5
                    print('Node 5 - list of area domains (SOURCE vs TARGET) - next step: Node 6')
                    sql_test = sql.SQL('''
                    select array_agg(t.res order by t.area_domain) from
                    (select
                            area_domain,
                            array[
                            (row_number() over())::text,
                            area_domain::text,
                            label_source::text,
                            case when check_label = true then 'TRUE' else 'FALSE' end,
                            case when check_description = true then 'TRUE' else 'FALSE' end,
                            case when check_label_en = true then 'TRUE' else 'FALSE' end,
                            case when check_description_en = true then 'TRUE' else 'FALSE' end
                            ] as res
                    from {}.fn_etl_check_area_domains4update(%s) order by area_domain
                    ) as t
                    ''').format(sql.Identifier(schemaname))
                    data_test = (json.dumps(res_node_c_1, ensure_ascii=False), )
                    res_test = run_sql(conn_dest, sql_test, data_test)[0][0]
                    print(tabulate(res_test, headers=['ID_ENUMERATE', 'ID_AREA_DOMAIN', 'LABEL_source', 'check_LABEL', 'check_DESCRIPTION', 'check_LABEL_EN', 'check_DESCRIPTION_EN'], tablefmt='psql'))               
                    ###############################################     Node 6
                    while True:
                        loop_1 = etl_input("Node 6 - Do you want to solve an update of label or description for some AREA DOMAIN? (y/n):\n")
                        if not (loop_1 == 'y' or loop_1 == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break 
                    if loop_1 == 'y':
                        while True:
                            check_node_c_7_8_enumerate = etl_input("Set ID_ENUMERATE of area domain:\n")
                            # get res_node_c_3_list in cycle
                            sql_cmd_node_c_3_list = sql.SQL('''select area_domain, label_source, description_source, label_en_source, description_en_source, label_target, description_target, label_en_target, description_en_target, check_label, check_description, check_label_en, check_description_en from {}.fn_etl_check_area_domains4update(%s) order by area_domain;''').format(sql.Identifier(schemaname))
                            data_node_c_3 = (json.dumps(res_node_c_1, ensure_ascii=False), )
                            res_node_c_3_list = run_sql(conn_dest, sql_cmd_node_c_3_list, data_node_c_3)
                            # get checks
                            check_label = res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['check_label']
                            check_description = res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['check_description']
                            check_label_en = res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['check_label_en']
                            check_description_en = res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['check_description_en']
                            # get values
                            source_label = res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['label_source']
                            target_label = res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['label_target']
                            source_label_en = res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['label_en_source']
                            target_label_en = res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['label_en_target']
                            source_description = res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['description_source']
                            target_description = res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['description_target']
                            source_description_en = res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['description_en_source']
                            target_description_en = res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['description_en_target']
                            # get cmds
                            sql_cmd_node_c_7_8_label = sql.SQL('''select * from {}.fn_etl_update_area_domain_label(%s,%s,%s);''').format(sql.Identifier(schemaname))
                            sql_cmd_node_c_7_8_description = sql.SQL('''select * from {}.fn_etl_update_area_domain_description(%s,%s,%s);''').format(sql.Identifier(schemaname))
                            # get source datas
                            data_node_c_7_8_label_source = (res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['area_domain'], national_language, res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['label_source'])
                            data_node_c_7_8_description_source = (res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['area_domain'], national_language, res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['description_source'])
                            data_node_c_7_8_label_en_source = (res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['area_domain'], 'en', res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['label_en_source'])
                            data_node_c_7_8_description_en_source = (res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['area_domain'], 'en', res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['description_en_source'])
                            # get target datas
                            data_node_c_7_8_label_target = (res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['area_domain'], national_language, res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['label_target'])
                            data_node_c_7_8_description_target = (res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['area_domain'], national_language, res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['description_target'])
                            data_node_c_7_8_label_en_target = (res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['area_domain'], 'en', res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['label_en_target'])
                            data_node_c_7_8_description_en_target = (res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['area_domain'], 'en', res_node_c_3_list[int(check_node_c_7_8_enumerate)-1]['description_en_target'])                            

                            if check_label == True and check_description == True:
                                check_national = 'True'
                            else:
                                check_national = 'False'

                            if check_label_en == True and check_description_en == True:
                                check_en = 'True'
                            else:
                                check_en = 'False'

                            if check_national == 'False' and check_en == 'True':
                                variant = '1'
                                print(tabulate([['source', 'label', source_label], ['target', 'label', target_label], ['source', 'description', source_description], ['target', 'description', target_description]], headers=['DATABASE', 'COLUMN','TEXT'], tablefmt='psql'))                      
                            elif check_national == 'True' and check_en == 'False':
                                variant = '2'
                                print(tabulate([['source', 'label_en', source_label_en], ['target', 'label_en', target_label_en], ['source', 'description_en', source_description_en], ['target', 'description_en', target_description_en]], headers=['DATABASE', 'COLUMN','TEXT'], tablefmt='psql'))
                            elif check_national == 'False' and check_en == 'False':
                                variant = '3'
                                print(tabulate([['source', 'label', source_label], ['target', 'label', target_label], ['source', 'description', source_description], ['target', 'description', target_description], ['source', 'label_en', source_label_en], ['target', 'label_en', target_label_en], ['source', 'description_en', source_description_en], ['target', 'description_en', target_description_en]], headers=['DATABASE', 'COLUMN','TEXT'], tablefmt='psql'))                      
                                        
                            if variant == '1' or variant == '2':
                                while True:
                                    make_update_node_c = etl_input("Make an UPDATE? [replace target with source] (y/n):\n")
                                    if not (make_update_node_c == 'y' or make_update_node_c == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break
                                if make_update_node_c == 'y':
                                    ###############################################     Node 7 and 8
                                    print('Node 7 and 8 -> YES -> update_area_domain_label and update_area_domain_description -> Next step: Node 9')
                                    if variant == '1':
                                        res_node_c_7_8_label = run_sql(conn_dest, sql_cmd_node_c_7_8_label, data_node_c_7_8_label_source)[0][0]
                                        res_node_c_7_8_description = run_sql(conn_dest, sql_cmd_node_c_7_8_description, data_node_c_7_8_description_source)[0][0]                    
                                    elif variant == '2':
                                        res_node_c_7_8_label_en = run_sql(conn_dest, sql_cmd_node_c_7_8_label, data_node_c_7_8_label_en_source)[0][0]
                                        res_node_c_7_8_description_en = run_sql(conn_dest, sql_cmd_node_c_7_8_description, data_node_c_7_8_description_en_source)[0][0]
                                    ##############################################    Node 9
                                    while True:
                                        commit_conn_node_c = etl_input("Node 9 - Confirm a commit of UPDATE? (y/n):\n")
                                        if not (commit_conn_node_c == 'y' or commit_conn_node_c == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if commit_conn_node_c == 'y':
                                        conn_dest.commit()
                                        print('Node 9 -> YES -> Process of UPDATE was commited in TARGET DB.')
                                    elif commit_conn_node_c == 'n':
                                        if variant == '1':
                                            res_node_c_7_8_label = run_sql(conn_dest, sql_cmd_node_c_7_8_label, data_node_c_7_8_label_target)[0][0]
                                            res_node_c_7_8_description = run_sql(conn_dest, sql_cmd_node_c_7_8_description, data_node_c_7_8_description_target)[0][0]                       
                                        elif variant == '2':
                                            res_node_c_7_8_label_en = run_sql(conn_dest, sql_cmd_node_c_7_8_label, data_node_c_7_8_label_en_target)[0][0]
                                            res_node_c_7_8_description_en = run_sql(conn_dest, sql_cmd_node_c_7_8_description, data_node_c_7_8_description_en_target)[0][0]                                       
                                        print('Node 9 -> NO -> Process of UPDATE was NOT commited in TARGET DB.')
                                elif make_update_node_c == 'n':
                                     print('Node 7 and 8 -> NO -> User does not want to make an update.')
                            elif variant == '3':
                                while True:
                                    make_update_node_c = etl_input("Make an UPDATE [replace target with source] ? (y/n):\n")
                                    if not (make_update_node_c == 'y' or make_update_node_c == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break
                                if make_update_node_c == 'y': 
                                    print('Node 7 and 8 -> YES -> update_area_domain_label and update_area_domain_description -> Next step: Node 9')                               
                                    print(tabulate([[1, 'national'], [2, 'english'], [3, 'national and english']], headers=['ID_VARIANT', 'LANGUAGE'], tablefmt='psql'))
                                    while True:
                                        ###############################################
                                        check_node_c_7_8_variant = etl_input("Set variant (1-3) for UPDATE differencies:\n")
                                        if not (check_node_c_7_8_variant == '1' or check_node_c_7_8_variant == '2' or check_node_c_7_8_variant == '3'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break
                                    if check_node_c_7_8_variant == '1':
                                        res_node_c_7_8_label = run_sql(conn_dest, sql_cmd_node_c_7_8_label, data_node_c_7_8_label_source)[0][0]
                                        res_node_c_7_8_description = run_sql(conn_dest, sql_cmd_node_c_7_8_description, data_node_c_7_8_description_source)[0][0]                      
                                    elif check_node_c_7_8_variant == '2':
                                        res_node_c_7_8_label_en = run_sql(conn_dest, sql_cmd_node_c_7_8_label, data_node_c_7_8_label_en_source)[0][0]
                                        res_node_c_7_8_description_en = run_sql(conn_dest, sql_cmd_node_c_7_8_description, data_node_c_7_8_description_en_source)[0][0]
                                    elif check_node_c_7_8_variant == '3':
                                        res_node_c_7_8_label = run_sql(conn_dest, sql_cmd_node_c_7_8_label, data_node_c_7_8_label_source)[0][0]
                                        res_node_c_7_8_description = run_sql(conn_dest, sql_cmd_node_c_7_8_description, data_node_c_7_8_description_source)[0][0]
                                        res_node_c_7_8_label_en = run_sql(conn_dest, sql_cmd_node_c_7_8_label, data_node_c_7_8_label_en_source)[0][0]
                                        res_node_c_7_8_description_en = run_sql(conn_dest, sql_cmd_node_c_7_8_description, data_node_c_7_8_description_en_source)[0][0]
                                    ##############################################    Node 8
                                    while True:
                                        commit_conn_node_c = etl_input("Node 9 - Confirm a commit of UPDATE? (y/n):\n")
                                        if not (commit_conn_node_c == 'y' or commit_conn_node_c == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if commit_conn_node_c == 'y':
                                        conn_dest.commit()
                                        print('Node 9 -> YES -> Process of UPDATE was commited in TARGET DB.')
                                    elif commit_conn_node_c == 'n':
                                        if check_node_c_7_8_variant == '1':
                                            res_node_c_7_8_label = run_sql(conn_dest, sql_cmd_node_c_7_8_label, data_node_c_7_8_label_target)[0][0]
                                            res_node_c_7_8_description = run_sql(conn_dest, sql_cmd_node_c_7_8_description, data_node_c_7_8_description_target)[0][0]                
                                        elif check_node_c_7_8_variant == '2':
                                            res_node_c_7_8_label_en = run_sql(conn_dest, sql_cmd_node_c_7_8_label, data_node_c_7_8_label_en_target)[0][0]
                                            res_node_c_7_8_description_en = run_sql(conn_dest, sql_cmd_node_c_7_8_description, data_node_c_7_8_description_en_target)[0][0]
                                        elif check_node_c_7_8_variant == '3':
                                            res_node_c_7_8_label = run_sql(conn_dest, sql_cmd_node_c_7_8_label, data_node_c_7_8_label_target)[0][0]
                                            res_node_c_7_8_description = run_sql(conn_dest, sql_cmd_node_c_7_8_description, data_node_c_7_8_description_target)[0][0]
                                            res_node_c_7_8_label_en = run_sql(conn_dest, sql_cmd_node_c_7_8_label, data_node_c_7_8_label_en_target)[0][0]
                                            res_node_c_7_8_description_en = run_sql(conn_dest, sql_cmd_node_c_7_8_description, data_node_c_7_8_description_en_target)[0][0]                                      
                                        print('Node 9 -> NO -> Process of UPDATE was NOT commited.')
                                elif make_update_node_e == 'n':
                                     print('Node 7 and 8 -> NO -> User does not want to make an update.')                               
                                                                                                                                              
                            # end of process FROM "select ID_ENUMERATE of AREA DOMAIN for update" TO commit'
                            sql_test_count = sql.SQL('''select count(*) from {}.fn_etl_check_area_domains4update(%s);''').format(sql.Identifier(schemaname))
                            data_test_count = (json.dumps(res_node_c_1, ensure_ascii=False), )
                            res_test_count = run_sql(conn_dest, sql_test_count, data_test_count)[0][0]
                            if res_test_count == 0:
                                 print('Not exists any AREA DOMAIN with differences. Next step: Node 10')
                                 break
                            else:
                                print('List of area domains (SOURCE vs TARGET)')
                                sql_test = sql.SQL('''
                                select array_agg(t.res order by t.area_domain) from
                                (select
                                        area_domain,
                                        array[
                                        (row_number() over())::text,
                                        area_domain::text,
                                        label_source::text,
                                        case when check_label = true then 'TRUE' else 'FALSE' end,
                                        case when check_description = true then 'TRUE' else 'FALSE' end,
                                        case when check_label_en = true then 'TRUE' else 'FALSE' end,
                                        case when check_description_en = true then 'TRUE' else 'FALSE' end
                                        ] as res
                                from {}.fn_etl_check_area_domains4update(%s) order by area_domain
                                ) as t
                                ''').format(sql.Identifier(schemaname))
                                data_test = (json.dumps(res_node_c_1, ensure_ascii=False), )
                                res_test = run_sql(conn_dest, sql_test, data_test)[0][0]
                                print(tabulate(res_test, headers=['ID_ENUMERATE', 'ID_AREA_DOMAIN', 'LABEL_source', 'check_LABEL', 'check_DESCRIPTION', 'check_LABEL_EN', 'check_DESCRIPTION_EN'], tablefmt='psql'))
                                while True:
                                    loop_2 = etl_input("Node 6 - Do you want to solve an update of label or description for some AREA DOMAIN? (y/n):\n")
                                    if not (loop_2 == 'y' or loop_2 == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break 
                                if loop_2 == 'y':
                                    continue
                                elif loop_2 == 'n':
                                    break          
                    elif loop_1 == 'n':
                        print('Node 6 -> NO -> User does not want to make an update of any AREA DOMAIN label or description. -> Next step: Node 10')
            ###############################################     Node 10
            while True:
                ###############################################
                check_node_c_10 = etl_input("Node 10 - Continue with process of ETL? (y/n):\n")
                if not (check_node_c_10 == 'y' or check_node_c_10 == 'n'):
                    print("Sorry, not known.")
                    continue
                else:
                    break            
            
            if check_node_c_10 == 'y':
                print('YES -> User wants to continue with process ETL. Next step: Node D')
            else:
                print('NO -> User does not want to continue with process ETL. Next step: STOP process of ETL.')
                conn_src.close()
                conn_dest.close()
                sys.exit(1)
            ###############################################
        elif check_node_c == 'n':
            print('NO -> User does not want to make a check of AREA DOMAIN labels and descriptions -> next step: Node D')
        print('--------------------------------------------------------------')  
        ##########################################################################################
        ##########################################################################################



        ##########################################################################################
        # Node D
        ##########################################################################################
        while True:
            loop_1 = etl_input("Node D - Make a check of AREA DOMAIN CATEGORY labels and descriptions that had already been ETL? (y/n):\n")
            if not (loop_1 == 'y' or loop_1 == 'n'):
                print("Sorry, not known.")
                continue
            else:
                break 
        if loop_1 == 'y':
            print('Node 1 - get_area_domain_categories4area_domains (SOURCE) -> next step: Node 2')
            sql_cmd_node_d_1_count = '''select count(*) from target_data.fn_etl_get_area_domain_categories4area_domains(%s);'''
            sql_cmd_node_d_1_json = '''select target_data.fn_etl_get_area_domain_categories4area_domains(%s) as res_node_d1_json;'''
            data_node_d_1 = (node7_conn,)
            res_node_d_1_count = run_sql(conn_src, sql_cmd_node_d_1_count, data_node_d_1)[0][0]
            res_node_d_1_json = run_sql(conn_src, sql_cmd_node_d_1_json, data_node_d_1)[0]['res_node_d1_json']            
            if res_node_d_1_count == 0:
                print('Node 2 - Not exists any ETLed area domain -> next step: Node 15')
            else:
                print('Node 2 - Exists at least one ETLed area domain -> next step: Node 3')
                print('Node 3 - check_area_domain_categories4area_domains (TARGET) -> next step: Node 4')
                sql_cmd_node_d_3 = sql.SQL('''select {}.fn_etl_check_area_domain_categories4area_domains(%s) as res_node_d3;''').format(sql.Identifier(schemaname))
                data_node_d_3 = (json.dumps(res_node_d_1_json, ensure_ascii=False), )
                res_node_d_3 = run_sql(conn_dest, sql_cmd_node_d_3, data_node_d_3)[0]['res_node_d3']    
                if res_node_d_3 == None:
                    print('Node 4 - not exists any area domain with differencies in categories -> next step: Node 15')
                else:
                    print('Node 4 - exists at least one area domain with differencies in categories -> next step: Node 5')
                    print('Node 5 - get_list_of_area_domains4update (SOURCE) -> next step: Node 6')
                    sql_cmd_node_d_5_list = '''select etl_area_domain, area_domain_target from target_data.fn_etl_get_list_of_area_domains4update(%s,%s) order by etl_area_domain, area_domain_target'''
                    sql_cmd_node_d_5 = '''
                    select array_agg(t.res order by t.etl_area_domain, t.area_domain_target) from
                    (select
                            etl_area_domain,
                            area_domain_target,
                            array[
                            (row_number() over())::text,
                            etl_area_domain::text,
                            area_domain_target::text,
                            label::text,
                            label_en::text
                            ] as res
                    from target_data.fn_etl_get_list_of_area_domains4update(%s,%s) order by etl_area_domain, area_domain_target
                    ) as t                    
                    '''
                    data_node_d_5 = (node7_conn,res_node_d_3)
                    res_node_d_5_list = run_sql(conn_src, sql_cmd_node_d_5_list, data_node_d_5)
                    res_node_d_5 = run_sql(conn_src, sql_cmd_node_d_5, data_node_d_5)[0][0]
                    print('Node 6 - print list of area domains')
                    print(tabulate(res_node_d_5, headers=['ID_ENUMERATE', 'ETL_AREA_DOMAIN', 'AREA_DOMAIN_target', 'LABEL_source', 'LABEL_EN_source'], tablefmt='psql'))
                    while True:
                        loop_2 = etl_input("Do you want to solve an update of categories for some of area domain? (y/n):\n")
                        if not (loop_2 == 'y' or loop_2 == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break 
                    if loop_2 == 'y':
                        print('YES -> user wants to solve an update of categories for some of are domain -> next step: Node 7')
                        #-------------------------------------------------------------------#
                        while True:
                            check_node_d_7_enumerate = etl_input("Node 7 - Set ID_ENUMERATE of area domain:\n")
                            print('Node 8 - get_area_domain_categories4update (SOURCE) -> next step: Node 9')
                            sql_cmd_node_d_8_json = '''select target_data.fn_etl_get_area_domain_categories4update(%s,%s) as res_node_d8_json;'''
                            data_node_d_8 = (res_node_d_5_list[int(check_node_d_7_enumerate)-1]['etl_area_domain'],res_node_d_5_list[int(check_node_d_7_enumerate)-1]['area_domain_target'])
                            res_node_d_8_json = run_sql(conn_src, sql_cmd_node_d_8_json, data_node_d_8)[0]['res_node_d8_json']
                            print('Node 9 - check_area_domain_categories4update (TARGET) -> next step: Node 10')
                            sql_cmd_node_d_9_list = sql.SQL('''select area_domain_category, label_source, description_source, label_en_source, description_en_source, label_target, description_target, label_en_target, description_en_target, check_label, check_description, check_label_en, check_description_en from {}.fn_etl_check_area_domain_categories4update(%s) order by area_domain_category;''').format(sql.Identifier(schemaname))
                            sql_cmd_node_d_9 = sql.SQL('''
                            select array_agg(t.res order by t.area_domain_category) from
                            (select
                                    area_domain_category,
                                    array[
                                    (row_number() over())::text,
                                    area_domain_category::text,
                                    label_source::text,
                                    case when check_label = true then 'TRUE' else 'FALSE' end,
                                    case when check_description = true then 'TRUE' else 'FALSE' end,
                                    case when check_label_en = true then 'TRUE' else 'FALSE' end,
                                    case when check_description_en = true then 'TRUE' else 'FALSE' end
                                    ] as res
                            from {}.fn_etl_check_area_domain_categories4update(%s) order by area_domain_category
                            ) as t
                            ''').format(sql.Identifier(schemaname))
                            data_node_d_9 = (json.dumps(res_node_d_8_json, ensure_ascii=False), )
                            res_node_d_9_list = run_sql(conn_dest, sql_cmd_node_d_9_list, data_node_d_9)
                            res_node_d_9 = run_sql(conn_dest, sql_cmd_node_d_9, data_node_d_9)[0][0]
                            print('Node 10 - print list of area domain categories')
                            print(tabulate(res_node_d_9, headers=['ID_ENUMERATE', 'ID_ADC', 'LABEL_source', 'check_LABEL', 'check_DESCRIPTION', 'check_LABEL_EN', 'check_DESCRIPTION_EN'], tablefmt='psql'))
                            while True:
                                loop_3 = etl_input("Do you want to solve an update of some category? (y/n):\n")
                                if not (loop_3 == 'y' or loop_3 == 'n'):
                                    print("Sorry, not known.")
                                    continue
                                else:
                                    break 
                            if loop_3 == 'y':
                                print('Node 11 - YES -> user wants to solve an update of category')
                                #----------------------------------------------------#
                                while True:
                                    #--------------------------#
                                    check_node_d_12_13_enumerate = etl_input("Node 12 and 13 - Set ID_ENUMERATE of category:\n")
                                    # get checks
                                    check_label = res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['check_label']
                                    check_description = res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['check_description']
                                    check_label_en = res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['check_label_en']
                                    check_description_en = res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['check_description_en']
                                    # get values
                                    source_label = res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['label_source']
                                    target_label = res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['label_target']
                                    source_label_en = res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['label_en_source']
                                    target_label_en = res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['label_en_target']
                                    source_description = res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['description_source']
                                    target_description = res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['description_target']
                                    source_description_en = res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['description_en_source']
                                    target_description_en = res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['description_en_target']
                                    # get cmds
                                    sql_cmd_node_d_12_13_label = sql.SQL('''select * from {}.fn_etl_update_area_domain_category_label(%s,%s,%s);''').format(sql.Identifier(schemaname))
                                    sql_cmd_node_d_12_13_description = sql.SQL('''select * from {}.fn_etl_update_area_domain_category_description(%s,%s,%s);''').format(sql.Identifier(schemaname))
                                    # get source datas
                                    data_node_d_12_13_label_source = (res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['area_domain_category'], national_language, res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['label_source'])
                                    data_node_d_12_13_description_source = (res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['area_domain_category'], national_language, res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['description_source'])
                                    data_node_d_12_13_label_en_source = (res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['area_domain_category'], 'en', res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['label_en_source'])
                                    data_node_d_12_13_description_en_source = (res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['area_domain_category'], 'en', res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['description_en_source'])
                                    # get target datas
                                    data_node_d_12_13_label_target = (res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['area_domain_category'], national_language, res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['label_target'])
                                    data_node_d_12_13_description_target = (res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['area_domain_category'], national_language, res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['description_target'])
                                    data_node_d_12_13_label_en_target = (res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['area_domain_category'], 'en', res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['label_en_target'])
                                    data_node_d_12_13_description_en_target = (res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['area_domain_category'], 'en', res_node_d_9_list[int(check_node_d_12_13_enumerate)-1]['description_en_target'])                            

                                    if check_label == True and check_description == True:
                                        check_national = 'True'
                                    else:
                                        check_national = 'False'

                                    if check_label_en == True and check_description_en == True:
                                        check_en = 'True'
                                    else:
                                        check_en = 'False'

                                    if check_national == 'False' and check_en == 'True':
                                        variant = '1'
                                        print(tabulate([['source', 'label', source_label], ['target', 'label', target_label], ['source', 'description', source_description], ['target', 'description', target_description]], headers=['DATABASE', 'COLUMN','TEXT'], tablefmt='psql'))                      
                                    elif check_national == 'True' and check_en == 'False':
                                        variant = '2'
                                        print(tabulate([['source', 'label_en', source_label_en], ['target', 'label_en', target_label_en], ['source', 'description_en', source_description_en], ['target', 'description_en', target_description_en]], headers=['DATABASE', 'COLUMN','TEXT'], tablefmt='psql'))
                                    elif check_national == 'False' and check_en == 'False':
                                        variant = '3'
                                        print(tabulate([['source', 'label', source_label], ['target', 'label', target_label], ['source', 'description', source_description], ['target', 'description', target_description], ['source', 'label_en', source_label_en], ['target', 'label_en', target_label_en], ['source', 'description_en', source_description_en], ['target', 'description_en', target_description_en]], headers=['DATABASE', 'COLUMN','TEXT'], tablefmt='psql'))                      
                                                
                                    if variant == '1' or variant == '2':
                                        while True:
                                            make_update_node_d = etl_input("Make an UPDATE? [replace target with source] (y/n):\n")
                                            if not (make_update_node_d == 'y' or make_update_node_d == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break
                                        if make_update_node_d == 'y':
                                            ###############################################     Node 12 and 13
                                            print('Node 12 and 13 -> YES -> update_area_domain_category_label and update_area_domain_category_description -> Next step: Node 14')
                                            if variant == '1':
                                                res_node_d_12_13_label = run_sql(conn_dest, sql_cmd_node_d_12_13_label, data_node_d_12_13_label_source)[0][0]
                                                res_node_d_12_13_description = run_sql(conn_dest, sql_cmd_node_d_12_13_description, data_node_d_12_13_description_source)[0][0]                       
                                            elif variant == '2':
                                                res_node_d_12_13_label_en = run_sql(conn_dest, sql_cmd_node_d_12_13_label, data_node_d_12_13_label_en_source)[0][0]
                                                res_node_d_12_13_description_en = run_sql(conn_dest, sql_cmd_node_d_12_13_description, data_node_d_12_13_description_en_source)[0][0]
                                            ##############################################    Node 14
                                            while True:
                                                commit_conn_node_d = etl_input("Node 14 - Confirm a commit of UPDATE? (y/n):\n")
                                                if not (commit_conn_node_d == 'y' or commit_conn_node_d == 'n'):
                                                    print("Sorry, not known.")
                                                    continue
                                                else:
                                                    break

                                            if commit_conn_node_d == 'y':
                                                conn_dest.commit()
                                                print('Node 14 -> YES -> Process of UPDATE was commited in TARGET DB.')
                                            elif commit_conn_node_d == 'n':
                                                if variant == '1':
                                                    res_node_d_12_13_label = run_sql(conn_dest, sql_cmd_node_d_12_13_label, data_node_d_12_13_label_target)[0][0]
                                                    res_node_d_12_13_description = run_sql(conn_dest, sql_cmd_node_d_12_13_description, data_node_d_12_13_description_target)[0][0]                       
                                                elif variant == '2':
                                                    res_node_d_12_13_label_en = run_sql(conn_dest, sql_cmd_node_d_12_13_label, data_node_d_12_13_label_en_target)[0][0]
                                                    res_node_d_12_13_description_en = run_sql(conn_dest, sql_cmd_node_d_12_13_description, data_node_d_12_13_description_en_target)[0][0]                                      
                                                print('Node 14 -> NO -> Process of UPDATE was NOT commited in TARGET DB.')
                                        elif make_update_node_d == 'n':
                                            print('Node 12 and 13 -> NO -> User does not want to make an update.')
                                    elif variant == '3':
                                        while True:
                                            make_update_node_d = etl_input("Make an UPDATE [replace target with source] ? (y/n):\n")
                                            if not (make_update_node_d == 'y' or make_update_node_d == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break
                                        if make_update_node_d == 'y': 
                                            print('Node 12 and 13 -> YES -> update_area_domain_category_label and update_area_domain_category_description -> Next step: Node 14')                               
                                            print(tabulate([[1, 'national'], [2, 'english'], [3, 'national and english']], headers=['ID_VARIANT', 'LANGUAGE'], tablefmt='psql'))
                                            while True:
                                                ###############################################
                                                check_node_d_12_13_variant = etl_input("Set variant (1-3) for UPDATE differencies:\n")
                                                if not (check_node_d_12_13_variant == '1' or check_node_d_12_13_variant == '2' or check_node_d_12_13_variant == '3'):
                                                    print("Sorry, not known.")
                                                    continue
                                                else:
                                                    break
                                            if check_node_d_12_13_variant == '1':
                                                res_node_d_12_13_label = run_sql(conn_dest, sql_cmd_node_d_12_13_label, data_node_d_12_13_label_source)[0][0]
                                                res_node_d_12_13_description = run_sql(conn_dest, sql_cmd_node_d_12_13_description, data_node_d_12_13_description_source)[0][0]                       
                                            elif check_node_d_12_13_variant == '2':
                                                res_node_d_12_13_label_en = run_sql(conn_dest, sql_cmd_node_d_12_13_label, data_node_d_12_13_label_en_source)[0][0]
                                                res_node_d_12_13_description_en = run_sql(conn_dest, sql_cmd_node_d_12_13_description, data_node_d_12_13_description_en_source)[0][0]
                                            elif check_node_d_12_13_variant == '3':
                                                res_node_d_12_13_label = run_sql(conn_dest, sql_cmd_node_d_12_13_label, data_node_d_12_13_label_source)[0][0]
                                                res_node_d_12_13_description = run_sql(conn_dest, sql_cmd_node_d_12_13_description, data_node_d_12_13_description_source)[0][0]
                                                res_node_d_12_13_label_en = run_sql(conn_dest, sql_cmd_node_d_12_13_label, data_node_d_12_13_label_en_source)[0][0]
                                                res_node_d_12_13_description_en = run_sql(conn_dest, sql_cmd_node_d_12_13_description, data_node_d_12_13_description_en_source)[0][0]
                                            ##############################################    Node 14
                                            while True:
                                                commit_conn_node_d = etl_input("Node 14 - Confirm a commit of UPDATE? (y/n):\n")
                                                if not (commit_conn_node_d == 'y' or commit_conn_node_d == 'n'):
                                                    print("Sorry, not known.")
                                                    continue
                                                else:
                                                    break

                                            if commit_conn_node_d == 'y':
                                                conn_dest.commit()
                                                print('Node 14 -> YES -> Process of UPDATE was commited in TARGET DB.')
                                            elif commit_conn_node_d == 'n':
                                                if check_node_d_12_13_variant == '1':
                                                    res_node_d_12_13_label = run_sql(conn_dest, sql_cmd_node_d_12_13_label, data_node_d_12_13_label_target)[0][0]
                                                    res_node_d_12_13_description = run_sql(conn_dest, sql_cmd_node_d_12_13_description, data_node_d_12_13_description_target)[0][0]                     
                                                elif check_node_d_12_13_variant == '2':
                                                    res_node_d_12_13_label_en = run_sql(conn_dest, sql_cmd_node_d_12_13_label, data_node_d_12_13_label_en_target)[0][0]
                                                    res_node_d_12_13_description_en = run_sql(conn_dest, sql_cmd_node_d_12_13_description, data_node_d_12_13_description_en_target)[0][0]
                                                elif check_node_d_12_13_variant == '3':
                                                    res_node_d_12_13_label = run_sql(conn_dest, sql_cmd_node_d_12_13_label, data_node_d_12_13_label_target)[0][0]
                                                    res_node_d_12_13_description = run_sql(conn_dest, sql_cmd_node_d_12_13_description, data_node_d_12_13_description_target)[0][0]
                                                    res_node_d_12_13_label_en = run_sql(conn_dest, sql_cmd_node_d_12_13_label, data_node_d_12_13_label_en_target)[0][0]
                                                    res_node_d_12_13_description_en = run_sql(conn_dest, sql_cmd_node_d_12_13_description, data_node_d_12_13_description_en_target)[0][0]                                    
                                                print('Node 14 -> NO -> Process of UPDATE was NOT commited.')
                                        elif make_update_node_d == 'n':
                                            print('Node 12 and 13 -> NO -> User does not want to make an update.')
                                    #--------------------------#
                                    sql_cmd_node_d_8_json = '''select target_data.fn_etl_get_area_domain_categories4update(%s,%s) as res_node_d8_json;'''
                                    data_node_d_8 = (res_node_d_5_list[int(check_node_d_7_enumerate)-1]['etl_area_domain'],res_node_d_5_list[int(check_node_d_7_enumerate)-1]['area_domain_target'])
                                    res_node_d_8_json = run_sql(conn_src, sql_cmd_node_d_8_json, data_node_d_8)[0]['res_node_d8_json']
                                    sql_cmd_node_d_9_count = sql.SQL('''select count(*) from {}.fn_etl_check_area_domain_categories4update(%s);''').format(sql.Identifier(schemaname))
                                    data_node_d_9_count = (json.dumps(res_node_d_8_json, ensure_ascii=False), )
                                    res_node_d_9_count = run_sql(conn_dest, sql_cmd_node_d_9_count, data_node_d_9_count)[0][0]
                                    if res_node_d_9_count == 0:
                                        print('Not exists any category with differences')
                                        break
                                    else:
                                        sql_cmd_node_d_8_json = '''select target_data.fn_etl_get_area_domain_categories4update(%s,%s) as res_node_d8_json;'''
                                        data_node_d_8 = (res_node_d_5_list[int(check_node_d_7_enumerate)-1]['etl_area_domain'],res_node_d_5_list[int(check_node_d_7_enumerate)-1]['area_domain_target'])
                                        res_node_d_8_json = run_sql(conn_src, sql_cmd_node_d_8_json, data_node_d_8)[0]['res_node_d8_json']
                                        sql_cmd_node_d_9_list = sql.SQL('''select area_domain_category, label_source, description_source, label_en_source, description_en_source, label_target, description_target, label_en_target, description_en_target, check_label, check_description, check_label_en, check_description_en from {}.fn_etl_check_area_domain_categories4update(%s) order by area_domain_category;''').format(sql.Identifier(schemaname))
                                        sql_cmd_node_d_9 = sql.SQL('''
                                        select array_agg(t.res order by t.area_domain_category) from
                                        (select
                                                area_domain_category,
                                                array[
                                                (row_number() over())::text,
                                                area_domain_category::text,
                                                label_source::text,
                                                case when check_label = true then 'TRUE' else 'FALSE' end,
                                                case when check_description = true then 'TRUE' else 'FALSE' end,
                                                case when check_label_en = true then 'TRUE' else 'FALSE' end,
                                                case when check_description_en = true then 'TRUE' else 'FALSE' end
                                                ] as res
                                        from {}.fn_etl_check_area_domain_categories4update(%s) order by area_domain_category
                                        ) as t
                                        ''').format(sql.Identifier(schemaname))
                                        data_node_d_9 = (json.dumps(res_node_d_8_json, ensure_ascii=False), )
                                        res_node_d_9_list = run_sql(conn_dest, sql_cmd_node_d_9_list, data_node_d_9)
                                        res_node_d_9 = run_sql(conn_dest, sql_cmd_node_d_9, data_node_d_9)[0][0]
                                        print(tabulate(res_node_d_9, headers=['ID_ENUMERATE', 'ID_ADC', 'LABEL_source', 'check_LABEL', 'check_DESCRIPTION', 'check_LABEL_EN', 'check_DESCRIPTION_EN'], tablefmt='psql'))         
                                        while True:
                                            loop_5 = etl_input("Do you want to solve an update of some category? (y/n):\n")
                                            if not (loop_5 == 'y' or loop_5 == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break 
                                        if loop_5 == 'y':
                                            continue
                                        elif loop_5 == 'n':
                                            break          
                                    #--------------------------#
                            elif loop_3 == 'n':
                                print('Node 11 - NO -> user does not want to solve an update of category')

                            sql_cmd_node_d_3_count = sql.SQL('''select {}.fn_etl_check_area_domain_categories4area_domains(%s) as res_node_d3;''').format(sql.Identifier(schemaname))
                            data_node_d_3_count = (json.dumps(res_node_d_1_json, ensure_ascii=False), )
                            res_node_d_3_count = run_sql(conn_dest, sql_cmd_node_d_3_count, data_node_d_3_count)[0]['res_node_d3']
                            if res_node_d_3_count == None:
                                print('Not exists any area domain with differencies in categories -> next step: Node 15')
                                break
                            else:
                                sql_cmd_node_d_3 = sql.SQL('''select {}.fn_etl_check_area_domain_categories4area_domains(%s) as res_node_d3;''').format(sql.Identifier(schemaname))
                                data_node_d_3 = (json.dumps(res_node_d_1_json, ensure_ascii=False), )
                                res_node_d_3 = run_sql(conn_dest, sql_cmd_node_d_3, data_node_d_3)[0]['res_node_d3']
                                sql_cmd_node_d_5_list = '''select etl_area_domain, area_domain_target from target_data.fn_etl_get_list_of_area_domains4update(%s,%s) order by etl_area_domain, area_domain_target'''
                                sql_cmd_node_d_5 = '''
                                select array_agg(t.res order by t.etl_area_domain, t.area_domain_target) from
                                (select
                                        etl_area_domain,
                                        area_domain_target,
                                        array[
                                        (row_number() over())::text,
                                        etl_area_domain::text,
                                        area_domain_target::text,
                                        label::text,
                                        label_en::text
                                        ] as res
                                from target_data.fn_etl_get_list_of_area_domains4update(%s,%s) order by etl_area_domain, area_domain_target
                                ) as t                    
                                '''
                                data_node_d_5 = (node7_conn,res_node_d_3)
                                res_node_d_5_list = run_sql(conn_src, sql_cmd_node_d_5_list, data_node_d_5)
                                res_node_d_5 = run_sql(conn_src, sql_cmd_node_d_5, data_node_d_5)[0][0]
                                print(tabulate(res_node_d_5, headers=['ID_ENUMERATE', 'ETL_AREA_DOMAIN', 'AREA_DOMAIN_target', 'LABEL_source', 'LABEL_EN_source'], tablefmt='psql'))                                
                                while True:
                                    loop_2a = etl_input("Do you want to solve an update of categories for some of area domain? (y/n):\n")
                                    if not (loop_2a == 'y' or loop_2a == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break 
                                if loop_2a == 'y':
                                    continue
                                elif loop_2a == 'n':
                                    break    
                        #-------------------------------------------------------------------#
                    elif loop_2 == 'n':
                        print('NO -> user does no want to solve an update of categories for some of area domain -> next step: Node 15')
            ####################################
            ####################################
            while True:            
                check_continue = etl_input("Node 15 - Continue with process ETL? (y/n):\n")
                if not (check_continue == 'y' or check_continue == 'n'):
                    print("Sorry, not known.")
                    continue
                else:
                    break
            if check_continue == 'y':
                print('YES -> User wants to continue with process ETL. Next step: Node E')
            else:
                print('NO -> User does not want to continue with process ETL. Next step: STOP process of ETL.')
                conn_src.close()
                conn_dest.close()
                sys.exit(1)
        elif loop_1 == 'n':
            print('NO -> User does not want to make a check of AREA DOMAIN CATEGORY labels and descriptions -> next step: Node E')
        print('--------------------------------------------------------------')
        ##########################################################################################
        ##########################################################################################



        ##########################################################################################
        # Node E
        ##########################################################################################
        while True:
            ###############################################
            check_node_e = etl_input("Node E - Make a check of SUB POPULATION labels and descriptions that had already been ETL? (y/n):\n")
            if not (check_node_e == 'y' or check_node_e == 'n'):
                print("Sorry, not known.")
                continue
            else:
                break

        if check_node_e == 'y':
            ###############################################     Node 1
            print('Node 1 - get_sub_populations4update (SOURCE) -> next step: Node 2')
            sql_cmd_node_e_1 = '''select target_data.fn_etl_get_sub_populations4update(%s) as res_node_e1;'''
            data_node_e_1 = (node7_conn,)
            res_node_e_1 = run_sql(conn_src, sql_cmd_node_e_1, data_node_e_1)[0]['res_node_e1']
            ###############################################     Node 2
            if res_node_e_1 == None:
                print('Node 2 - Any record returned? -> NO -> next step: Node 10') # not exists any ETLed sub population
            else:
                print('Node 2 - Any record returned? -> YES -> next step: Node 3')
                ###############################################     Node 3
                print('Node 3 - check_sub_populations4update (TARGET) - next step: Node 4')
                sql_cmd_node_e_3_count = sql.SQL('''select count(*) from {}.fn_etl_check_sub_populations4update(%s);''').format(sql.Identifier(schemaname))
                sql_cmd_node_e_3_list = sql.SQL('''select sub_population, label_source, description_source, label_en_source, description_en_source, label_target, description_target, label_en_target, description_en_target, check_label, check_description, check_label_en, check_description_en from {}.fn_etl_check_sub_populations4update(%s) order by sub_population;''').format(sql.Identifier(schemaname))
                data_node_e_3 = (json.dumps(res_node_e_1, ensure_ascii=False), )
                res_node_e_3_count = run_sql(conn_dest, sql_cmd_node_e_3_count, data_node_e_3)[0][0]
                res_node_e_3_list = run_sql(conn_dest, sql_cmd_node_e_3_list, data_node_e_3)
                ###############################################     Node 4
                if res_node_e_3_count == 0:
                    print('Node 4 - Any record returned? -> NO -> Not exists any SUB POPULATION with differences.')
                else:
                    print('Node 4 - Any record returned? -> YES -> Exists SUB POPULATION with differences. -> Next step: Node 5')
                    ###############################################     Node 5
                    print('Node 5 - list of sub populations (SOURCE vs TARGET) - next step: Node 6')
                    sql_test = sql.SQL('''
                    select array_agg(t.res order by t.sub_population) from
                    (select
                            sub_population,
                            array[
                            (row_number() over())::text,
                            sub_population::text,
                            label_source::text,
                            case when check_label = true then 'TRUE' else 'FALSE' end,
                            case when check_description = true then 'TRUE' else 'FALSE' end,
                            case when check_label_en = true then 'TRUE' else 'FALSE' end,
                            case when check_description_en = true then 'TRUE' else 'FALSE' end
                            ] as res
                    from {}.fn_etl_check_sub_populations4update(%s) order by sub_population
                    ) as t
                    ''').format(sql.Identifier(schemaname))
                    data_test = (json.dumps(res_node_e_1, ensure_ascii=False), )
                    res_test = run_sql(conn_dest, sql_test, data_test)[0][0]
                    print(tabulate(res_test, headers=['ID_ENUMERATE', 'ID_SUB_POPULATION', 'LABEL_source', 'check_LABEL', 'check_DESCRIPTION', 'check_LABEL_EN', 'check_DESCRIPTION_EN'], tablefmt='psql'))               
                    ###############################################     Node 6
                    while True:
                        loop_1 = etl_input("Node 6 - Do you want to solve an update of label or description for some SUB POPULATION? (y/n):\n")
                        if not (loop_1 == 'y' or loop_1 == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break 
                    if loop_1 == 'y':
                        while True:
                            check_node_e_7_8_enumerate = etl_input("Set ID_ENUMERATE of sub population:\n")
                            # get res_node_e_3_list in cycle
                            sql_cmd_node_e_3_list = sql.SQL('''select sub_population, label_source, description_source, label_en_source, description_en_source, label_target, description_target, label_en_target, description_en_target, check_label, check_description, check_label_en, check_description_en from {}.fn_etl_check_sub_populations4update(%s) order by sub_population;''').format(sql.Identifier(schemaname))
                            data_node_e_3 = (json.dumps(res_node_e_1, ensure_ascii=False), )
                            res_node_e_3_list = run_sql(conn_dest, sql_cmd_node_e_3_list, data_node_e_3)
                            # get checks
                            check_label = res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['check_label']
                            check_description = res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['check_description']
                            check_label_en = res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['check_label_en']
                            check_description_en = res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['check_description_en']
                            # get values
                            source_label = res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['label_source']
                            target_label = res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['label_target']
                            source_label_en = res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['label_en_source']
                            target_label_en = res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['label_en_target']
                            source_description = res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['description_source']
                            target_description = res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['description_target']
                            source_description_en = res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['description_en_source']
                            target_description_en = res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['description_en_target']
                            # get cmds
                            sql_cmd_node_e_7_8_label = sql.SQL('''select * from {}.fn_etl_update_sub_population_label(%s,%s,%s);''').format(sql.Identifier(schemaname))
                            sql_cmd_node_e_7_8_description = sql.SQL('''select * from {}.fn_etl_update_sub_population_description(%s,%s,%s);''').format(sql.Identifier(schemaname))
                            # get source datas
                            data_node_e_7_8_label_source = (res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['sub_population'], national_language, res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['label_source'])
                            data_node_e_7_8_description_source = (res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['sub_population'], national_language, res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['description_source'])
                            data_node_e_7_8_label_en_source = (res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['sub_population'], 'en', res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['label_en_source'])
                            data_node_e_7_8_description_en_source = (res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['sub_population'], 'en', res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['description_en_source'])
                            # get target datas
                            data_node_e_7_8_label_target = (res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['sub_population'], national_language, res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['label_target'])
                            data_node_e_7_8_description_target = (res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['sub_population'], national_language, res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['description_target'])
                            data_node_e_7_8_label_en_target = (res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['sub_population'], 'en', res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['label_en_target'])
                            data_node_e_7_8_description_en_target = (res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['sub_population'], 'en', res_node_e_3_list[int(check_node_e_7_8_enumerate)-1]['description_en_target'])                            

                            if check_label == True and check_description == True:
                                check_national = 'True'
                            else:
                                check_national = 'False'

                            if check_label_en == True and check_description_en == True:
                                check_en = 'True'
                            else:
                                check_en = 'False'

                            if check_national == 'False' and check_en == 'True':
                                variant = '1'
                                print(tabulate([['source', 'label', source_label], ['target', 'label', target_label], ['source', 'description', source_description], ['target', 'description', target_description]], headers=['DATABASE', 'COLUMN','TEXT'], tablefmt='psql'))                      
                            elif check_national == 'True' and check_en == 'False':
                                variant = '2'
                                print(tabulate([['source', 'label_en', source_label_en], ['target', 'label_en', target_label_en], ['source', 'description_en', source_description_en], ['target', 'description_en', target_description_en]], headers=['DATABASE', 'COLUMN','TEXT'], tablefmt='psql'))
                            elif check_national == 'False' and check_en == 'False':
                                variant = '3'
                                print(tabulate([['source', 'label', source_label], ['target', 'label', target_label], ['source', 'description', source_description], ['target', 'description', target_description], ['source', 'label_en', source_label_en], ['target', 'label_en', target_label_en], ['source', 'description_en', source_description_en], ['target', 'description_en', target_description_en]], headers=['DATABASE', 'COLUMN','TEXT'], tablefmt='psql'))                      
                                        
                            if variant == '1' or variant == '2':
                                while True:
                                    make_update_node_e = etl_input("Make an UPDATE? [replace target with source] (y/n):\n")
                                    if not (make_update_node_e == 'y' or make_update_node_e == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break
                                if make_update_node_e == 'y':
                                    ###############################################     Node 7 and 8
                                    print('Node 7 and 8 -> YES -> update_sub_population_label and update_sub_population_description -> Next step: Node 9')
                                    if variant == '1':
                                        res_node_e_7_8_label = run_sql(conn_dest, sql_cmd_node_e_7_8_label, data_node_e_7_8_label_source)[0][0]
                                        res_node_e_7_8_description = run_sql(conn_dest, sql_cmd_node_e_7_8_description, data_node_e_7_8_description_source)[0][0]                    
                                    elif variant == '2':
                                        res_node_e_7_8_label_en = run_sql(conn_dest, sql_cmd_node_e_7_8_label, data_node_e_7_8_label_en_source)[0][0]
                                        res_node_e_7_8_description_en = run_sql(conn_dest, sql_cmd_node_e_7_8_description, data_node_e_7_8_description_en_source)[0][0]
                                    ##############################################    Node 9
                                    while True:
                                        commit_conn_node_e = etl_input("Node 9 - Confirm a commit of UPDATE? (y/n):\n")
                                        if not (commit_conn_node_e == 'y' or commit_conn_node_e == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if commit_conn_node_e == 'y':
                                        conn_dest.commit()
                                        print('Node 9 -> YES -> Process of UPDATE was commited in TARGET DB.')
                                    elif commit_conn_node_e == 'n':
                                        if variant == '1':
                                            res_node_e_7_8_label = run_sql(conn_dest, sql_cmd_node_e_7_8_label, data_node_e_7_8_label_target)[0][0]
                                            res_node_e_7_8_description = run_sql(conn_dest, sql_cmd_node_e_7_8_description, data_node_e_7_8_description_target)[0][0]                       
                                        elif variant == '2':
                                            res_node_e_7_8_label_en = run_sql(conn_dest, sql_cmd_node_e_7_8_label, data_node_e_7_8_label_en_target)[0][0]
                                            res_node_e_7_8_description_en = run_sql(conn_dest, sql_cmd_node_e_7_8_description, data_node_e_7_8_description_en_target)[0][0]                                       
                                        print('Node 9 -> NO -> Process of UPDATE was NOT commited in TARGET DB.')
                                elif make_update_node_e == 'n':
                                     print('Node 7 and 8 -> NO -> User does not want to make an update.')
                            elif variant == '3':
                                while True:
                                    make_update_node_e = etl_input("Make an UPDATE [replace target with source] ? (y/n):\n")
                                    if not (make_update_node_e == 'y' or make_update_node_e == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break
                                if make_update_node_e == 'y': 
                                    print('Node 7 and 8 -> YES -> update_sub_population_label and update_sub_population_description -> Next step: Node 9')                               
                                    print(tabulate([[1, 'national'], [2, 'english'], [3, 'national and english']], headers=['ID_VARIANT', 'LANGUAGE'], tablefmt='psql'))
                                    while True:
                                        ###############################################
                                        check_node_e_7_8_variant = etl_input("Set variant (1-3) for UPDATE differencies:\n")
                                        if not (check_node_e_7_8_variant == '1' or check_node_e_7_8_variant == '2' or check_node_e_7_8_variant == '3'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break
                                    if check_node_e_7_8_variant == '1':
                                        res_node_e_7_8_label = run_sql(conn_dest, sql_cmd_node_e_7_8_label, data_node_e_7_8_label_source)[0][0]
                                        res_node_e_7_8_description = run_sql(conn_dest, sql_cmd_node_e_7_8_description, data_node_e_7_8_description_source)[0][0]                      
                                    elif check_node_e_7_8_variant == '2':
                                        res_node_e_7_8_label_en = run_sql(conn_dest, sql_cmd_node_e_7_8_label, data_node_e_7_8_label_en_source)[0][0]
                                        res_node_e_7_8_description_en = run_sql(conn_dest, sql_cmd_node_e_7_8_description, data_node_e_7_8_description_en_source)[0][0]
                                    elif check_node_e_7_8_variant == '3':
                                        res_node_e_7_8_label = run_sql(conn_dest, sql_cmd_node_e_7_8_label, data_node_e_7_8_label_source)[0][0]
                                        res_node_e_7_8_description = run_sql(conn_dest, sql_cmd_node_e_7_8_description, data_node_e_7_8_description_source)[0][0]
                                        res_node_e_7_8_label_en = run_sql(conn_dest, sql_cmd_node_e_7_8_label, data_node_e_7_8_label_en_source)[0][0]
                                        res_node_e_7_8_description_en = run_sql(conn_dest, sql_cmd_node_e_7_8_description, data_node_e_7_8_description_en_source)[0][0]
                                    ##############################################    Node 8
                                    while True:
                                        commit_conn_node_e = etl_input("Node 9 - Confirm a commit of UPDATE? (y/n):\n")
                                        if not (commit_conn_node_e == 'y' or commit_conn_node_e == 'n'):
                                            print("Sorry, not known.")
                                            continue
                                        else:
                                            break

                                    if commit_conn_node_e == 'y':
                                        conn_dest.commit()
                                        print('Node 9 -> YES -> Process of UPDATE was commited in TARGET DB.')
                                    elif commit_conn_node_e == 'n':
                                        if check_node_e_7_8_variant == '1':
                                            res_node_e_7_8_label = run_sql(conn_dest, sql_cmd_node_e_7_8_label, data_node_e_7_8_label_target)[0][0]
                                            res_node_e_7_8_description = run_sql(conn_dest, sql_cmd_node_e_7_8_description, data_node_e_7_8_description_target)[0][0]                
                                        elif check_node_e_7_8_variant == '2':
                                            res_node_e_7_8_label_en = run_sql(conn_dest, sql_cmd_node_e_7_8_label, data_node_e_7_8_label_en_target)[0][0]
                                            res_node_e_7_8_description_en = run_sql(conn_dest, sql_cmd_node_e_7_8_description, data_node_e_7_8_description_en_target)[0][0]
                                        elif check_node_e_7_8_variant == '3':
                                            res_node_e_7_8_label = run_sql(conn_dest, sql_cmd_node_e_7_8_label, data_node_e_7_8_label_target)[0][0]
                                            res_node_e_7_8_description = run_sql(conn_dest, sql_cmd_node_e_7_8_description, data_node_e_7_8_description_target)[0][0]
                                            res_node_e_7_8_label_en = run_sql(conn_dest, sql_cmd_node_e_7_8_label, data_node_e_7_8_label_en_target)[0][0]
                                            res_node_e_7_8_description_en = run_sql(conn_dest, sql_cmd_node_e_7_8_description, data_node_e_7_8_description_en_target)[0][0]                                      
                                        print('Node 9 -> NO -> Process of UPDATE was NOT commited.')
                                elif make_update_node_e == 'n':
                                     print('Node 7 and 8 -> NO -> User does not want to make an update.')                               
                                                                                                                                              
                            # end of process FROM "select ID_ENUMERATE of SUB POPULATION for update" TO commit'
                            sql_test_count = sql.SQL('''select count(*) from {}.fn_etl_check_sub_populations4update(%s);''').format(sql.Identifier(schemaname))
                            data_test_count = (json.dumps(res_node_e_1, ensure_ascii=False), )
                            res_test_count = run_sql(conn_dest, sql_test_count, data_test_count)[0][0]
                            if res_test_count == 0:
                                 print('Not exists any SUB POPULATION with differences. Next step: Node 10')
                                 break
                            else:
                                print('List of sub populations (SOURCE vs TARGET)')
                                sql_test = sql.SQL('''
                                select array_agg(t.res order by t.sub_population) from
                                (select
                                        sub_population,
                                        array[
                                        (row_number() over())::text,
                                        sub_population::text,
                                        label_source::text,
                                        case when check_label = true then 'TRUE' else 'FALSE' end,
                                        case when check_description = true then 'TRUE' else 'FALSE' end,
                                        case when check_label_en = true then 'TRUE' else 'FALSE' end,
                                        case when check_description_en = true then 'TRUE' else 'FALSE' end
                                        ] as res
                                from {}.fn_etl_check_sub_populations4update(%s) order by sub_population
                                ) as t
                                ''').format(sql.Identifier(schemaname))
                                data_test = (json.dumps(res_node_e_1, ensure_ascii=False), )
                                res_test = run_sql(conn_dest, sql_test, data_test)[0][0]
                                print(tabulate(res_test, headers=['ID_ENUMERATE', 'ID_SUB_POPULATION', 'LABEL_source', 'check_LABEL', 'check_DESCRIPTION', 'check_LABEL_EN', 'check_DESCRIPTION_EN'], tablefmt='psql'))
                                while True:
                                    loop_2 = etl_input("Node 6 - Do you want to solve an update of label or description for some SUB POPULATION? (y/n):\n")
                                    if not (loop_2 == 'y' or loop_2 == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break 
                                if loop_2 == 'y':
                                    continue
                                elif loop_2 == 'n':
                                    break          
                    elif loop_1 == 'n':
                        print('Node 6 -> NO -> User does not want to make an update of any SUB POPULATION label or description. -> Next step: Node 10')
            ###############################################     Node 10
            while True:
                ###############################################
                check_node_e_10 = etl_input("Node 10 - Continue with process of ETL? (y/n):\n")
                if not (check_node_e_10 == 'y' or check_node_e_10 == 'n'):
                    print("Sorry, not known.")
                    continue
                else:
                    break            
            
            if check_node_e_10 == 'y':
                print('YES -> User wants to continue with process ETL. Next step: Node F')
            else:
                print('NO -> User does not want to continue with process ETL. Next step: STOP process of ETL.')
                conn_src.close()
                conn_dest.close()
                sys.exit(1)
            ###############################################
        elif check_node_e == 'n':
            print('NO -> User does not want to make a check of SUB POPULATION labels and descriptions -> next step: Node F')
        print('--------------------------------------------------------------')  
        ##########################################################################################
        ##########################################################################################



        ##########################################################################################
        # Node F
        ##########################################################################################
        while True:
            loop_1 = etl_input("Node F - Make a check of SUB POPULATION CATEGORY labels and descriptions that had already been ETL? (y/n):\n")
            if not (loop_1 == 'y' or loop_1 == 'n'):
                print("Sorry, not known.")
                continue
            else:
                break 
        if loop_1 == 'y':
            print('Node 1 - get_sub_population_categories4sub_populations (SOURCE) -> next step: Node 2')
            sql_cmd_node_f_1_count = '''select count(*) from target_data.fn_etl_get_sub_population_categories4sub_populations(%s);'''
            sql_cmd_node_f_1_json = '''select target_data.fn_etl_get_sub_population_categories4sub_populations(%s) as res_node_f1_json;'''
            data_node_f_1 = (node7_conn,)
            res_node_f_1_count = run_sql(conn_src, sql_cmd_node_f_1_count, data_node_f_1)[0][0]
            res_node_f_1_json = run_sql(conn_src, sql_cmd_node_f_1_json, data_node_f_1)[0]['res_node_f1_json']            
            if res_node_f_1_count == 0:
                print('Node 2 - Not exists any ETLed sub population -> next step: Node 15')
            else:
                print('Node 2 - Exists at least one ETLed sub population -> next step: Node 3')
                print('Node 3 - check_sub_population_categories4sub_populations (TARGET) -> next step: Node 4')
                sql_cmd_node_f_3 = sql.SQL('''select {}.fn_etl_check_sub_population_categories4sub_populations(%s) as res_node_f3;''').format(sql.Identifier(schemaname))
                data_node_f_3 = (json.dumps(res_node_f_1_json, ensure_ascii=False), )
                res_node_f_3 = run_sql(conn_dest, sql_cmd_node_f_3, data_node_f_3)[0]['res_node_f3']    
                if res_node_f_3 == None:
                    print('Node 4 - not exists any sub population with differences in categories -> next step: Node 15')
                else:
                    print('Node 4 - exists at least one sub population with differences in categories -> next step: Node 5')
                    print('Node 5 - get_list_of_sub_populations4update (SOURCE) -> next step: Node 6')
                    sql_cmd_node_f_5_list = '''select etl_sub_population, sub_population_target from target_data.fn_etl_get_list_of_sub_populations4update(%s,%s) order by etl_sub_population, sub_population_target'''
                    sql_cmd_node_f_5 = '''
                    select array_agg(t.res order by t.etl_sub_population, t.sub_population_target) from
                    (select
                            etl_sub_population,
                            sub_population_target,
                            array[
                            (row_number() over())::text,
                            etl_sub_population::text,
                            sub_population_target::text,
                            label::text,
                            label_en::text
                            ] as res
                    from target_data.fn_etl_get_list_of_sub_populations4update(%s,%s) order by etl_sub_population, sub_population_target
                    ) as t                    
                    '''
                    data_node_f_5 = (node7_conn,res_node_f_3)
                    res_node_f_5_list = run_sql(conn_src, sql_cmd_node_f_5_list, data_node_f_5)
                    res_node_f_5 = run_sql(conn_src, sql_cmd_node_f_5, data_node_f_5)[0][0]
                    print('Node 6 - print list of sub populations')
                    print(tabulate(res_node_f_5, headers=['ID_ENUMERATE', 'ETL_SUB_POPULATION', 'SUB_POPULATION_target', 'LABEL_source', 'LABEL_EN_source'], tablefmt='psql'))
                    while True:
                        loop_2 = etl_input("Do you want to solve an update of categories for some of sub population? (y/n):\n")
                        if not (loop_2 == 'y' or loop_2 == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break 
                    if loop_2 == 'y':
                        print('YES -> user wants to solve an update of categories for some of sub population -> next step: Node 7')
                        #-------------------------------------------------------------------#
                        while True:
                            check_node_f_7_enumerate = etl_input("Node 7 - Set ID_ENUMERATE of sub population:\n")
                            print('Node 8 - get_sub_population_categories4update (SOURCE) -> next step: Node 9')
                            sql_cmd_node_f_8_json = '''select target_data.fn_etl_get_sub_population_categories4update(%s,%s) as res_node_f8_json;'''
                            data_node_f_8 = (res_node_f_5_list[int(check_node_f_7_enumerate)-1]['etl_sub_population'],res_node_f_5_list[int(check_node_f_7_enumerate)-1]['sub_population_target'])
                            res_node_f_8_json = run_sql(conn_src, sql_cmd_node_f_8_json, data_node_f_8)[0]['res_node_f8_json']
                            print('Node 9 - check_sub_population_categories4update (TARGET) -> next step: Node 10')
                            sql_cmd_node_f_9_list = sql.SQL('''select sub_population_category, label_source, description_source, label_en_source, description_en_source, label_target, description_target, label_en_target, description_en_target, check_label, check_description, check_label_en, check_description_en from {}.fn_etl_check_sub_population_categories4update(%s) order by sub_population_category;''').format(sql.Identifier(schemaname))
                            sql_cmd_node_f_9 = sql.SQL('''
                            select array_agg(t.res order by t.sub_population_category) from
                            (select
                                    sub_population_category,
                                    array[
                                    (row_number() over())::text,
                                    sub_population_category::text,
                                    label_source::text,
                                    case when check_label = true then 'TRUE' else 'FALSE' end,
                                    case when check_description = true then 'TRUE' else 'FALSE' end,
                                    case when check_label_en = true then 'TRUE' else 'FALSE' end,
                                    case when check_description_en = true then 'TRUE' else 'FALSE' end
                                    ] as res
                            from {}.fn_etl_check_sub_population_categories4update(%s) order by sub_population_category
                            ) as t
                            ''').format(sql.Identifier(schemaname))
                            data_node_f_9 = (json.dumps(res_node_f_8_json, ensure_ascii=False), )
                            res_node_f_9_list = run_sql(conn_dest, sql_cmd_node_f_9_list, data_node_f_9)
                            res_node_f_9 = run_sql(conn_dest, sql_cmd_node_f_9, data_node_f_9)[0][0]
                            print('Node 10 - print list of sub population categories')
                            print(tabulate(res_node_f_9, headers=['ID_ENUMERATE', 'ID_SPC', 'LABEL_source', 'check_LABEL', 'check_DESCRIPTION', 'check_LABEL_EN', 'check_DESCRIPTION_EN'], tablefmt='psql'))
                            while True:
                                loop_3 = etl_input("Do you want to solve an update of some category? (y/n):\n")
                                if not (loop_3 == 'y' or loop_3 == 'n'):
                                    print("Sorry, not known.")
                                    continue
                                else:
                                    break 
                            if loop_3 == 'y':
                                print('Node 11 - YES -> user wants to solve an update of category')
                                #----------------------------------------------------#
                                while True:
                                    #--------------------------#
                                    check_node_f_12_13_enumerate = etl_input("Node 12 and 13 - Set ID_ENUMERATE of category:\n")
                                    # get checks
                                    check_label = res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['check_label']
                                    check_description = res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['check_description']
                                    check_label_en = res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['check_label_en']
                                    check_description_en = res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['check_description_en']
                                    # get values
                                    source_label = res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['label_source']
                                    target_label = res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['label_target']
                                    source_label_en = res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['label_en_source']
                                    target_label_en = res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['label_en_target']
                                    source_description = res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['description_source']
                                    target_description = res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['description_target']
                                    source_description_en = res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['description_en_source']
                                    target_description_en = res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['description_en_target']
                                    # get cmds
                                    sql_cmd_node_f_12_13_label = sql.SQL('''select * from {}.fn_etl_update_sub_population_category_label(%s,%s,%s);''').format(sql.Identifier(schemaname))
                                    sql_cmd_node_f_12_13_description = sql.SQL('''select * from {}.fn_etl_update_sub_population_category_description(%s,%s,%s);''').format(sql.Identifier(schemaname))
                                    # get source datas
                                    data_node_f_12_13_label_source = (res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['sub_population_category'], national_language, res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['label_source'])
                                    data_node_f_12_13_description_source = (res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['sub_population_category'], national_language, res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['description_source'])
                                    data_node_f_12_13_label_en_source = (res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['sub_population_category'], 'en', res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['label_en_source'])
                                    data_node_f_12_13_description_en_source = (res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['sub_population_category'], 'en', res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['description_en_source'])
                                    # get target datas
                                    data_node_f_12_13_label_target = (res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['sub_population_category'], national_language, res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['label_target'])
                                    data_node_f_12_13_description_target = (res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['sub_population_category'], national_language, res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['description_target'])
                                    data_node_f_12_13_label_en_target = (res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['sub_population_category'], 'en', res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['label_en_target'])
                                    data_node_f_12_13_description_en_target = (res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['sub_population_category'], 'en', res_node_f_9_list[int(check_node_f_12_13_enumerate)-1]['description_en_target'])                            

                                    if check_label == True and check_description == True:
                                        check_national = 'True'
                                    else:
                                        check_national = 'False'

                                    if check_label_en == True and check_description_en == True:
                                        check_en = 'True'
                                    else:
                                        check_en = 'False'

                                    if check_national == 'False' and check_en == 'True':
                                        variant = '1'
                                        print(tabulate([['source', 'label', source_label], ['target', 'label', target_label], ['source', 'description', source_description], ['target', 'description', target_description]], headers=['DATABASE', 'COLUMN','TEXT'], tablefmt='psql'))                      
                                    elif check_national == 'True' and check_en == 'False':
                                        variant = '2'
                                        print(tabulate([['source', 'label_en', source_label_en], ['target', 'label_en', target_label_en], ['source', 'description_en', source_description_en], ['target', 'description_en', target_description_en]], headers=['DATABASE', 'COLUMN','TEXT'], tablefmt='psql'))
                                    elif check_national == 'False' and check_en == 'False':
                                        variant = '3'
                                        print(tabulate([['source', 'label', source_label], ['target', 'label', target_label], ['source', 'description', source_description], ['target', 'description', target_description], ['source', 'label_en', source_label_en], ['target', 'label_en', target_label_en], ['source', 'description_en', source_description_en], ['target', 'description_en', target_description_en]], headers=['DATABASE', 'COLUMN','TEXT'], tablefmt='psql'))                      
                                                
                                    if variant == '1' or variant == '2':
                                        while True:
                                            make_update_node_f = etl_input("Make an UPDATE? [replace target with source] (y/n):\n")
                                            if not (make_update_node_f == 'y' or make_update_node_f == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break
                                        if make_update_node_f == 'y':
                                            ###############################################     Node 12 and 13
                                            print('Node 12 and 13 -> YES -> update_sub_population_category_label and update_sub_population_category_description -> Next step: Node 14')
                                            if variant == '1':
                                                res_node_f_12_13_label = run_sql(conn_dest, sql_cmd_node_f_12_13_label, data_node_f_12_13_label_source)[0][0]
                                                res_node_f_12_13_description = run_sql(conn_dest, sql_cmd_node_f_12_13_description, data_node_f_12_13_description_source)[0][0]                       
                                            elif variant == '2':
                                                res_node_f_12_13_label_en = run_sql(conn_dest, sql_cmd_node_f_12_13_label, data_node_f_12_13_label_en_source)[0][0]
                                                res_node_f_12_13_description_en = run_sql(conn_dest, sql_cmd_node_f_12_13_description, data_node_f_12_13_description_en_source)[0][0]
                                            ##############################################    Node 14
                                            while True:
                                                commit_conn_node_f = etl_input("Node 14 - Confirm a commit of UPDATE? (y/n):\n")
                                                if not (commit_conn_node_f == 'y' or commit_conn_node_f == 'n'):
                                                    print("Sorry, not known.")
                                                    continue
                                                else:
                                                    break

                                            if commit_conn_node_f == 'y':
                                                conn_dest.commit()
                                                print('Node 14 -> YES -> Process of UPDATE was commited in TARGET DB.')
                                            elif commit_conn_node_f == 'n':
                                                if variant == '1':
                                                    res_node_f_12_13_label = run_sql(conn_dest, sql_cmd_node_f_12_13_label, data_node_f_12_13_label_target)[0][0]
                                                    res_node_f_12_13_description = run_sql(conn_dest, sql_cmd_node_f_12_13_description, data_node_f_12_13_description_target)[0][0]                       
                                                elif variant == '2':
                                                    res_node_f_12_13_label_en = run_sql(conn_dest, sql_cmd_node_f_12_13_label, data_node_f_12_13_label_en_target)[0][0]
                                                    res_node_f_12_13_description_en = run_sql(conn_dest, sql_cmd_node_f_12_13_description, data_node_f_12_13_description_en_target)[0][0]                                      
                                                print('Node 14 -> NO -> Process of UPDATE was NOT commited in TARGET DB.')
                                        elif make_update_node_f == 'n':
                                            print('Node 12 and 13 -> NO -> User does not want to make an update.')
                                    elif variant == '3':
                                        while True:
                                            make_update_node_f = etl_input("Make an UPDATE [replace target with source] ? (y/n):\n")
                                            if not (make_update_node_f == 'y' or make_update_node_f == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break
                                        if make_update_node_f == 'y': 
                                            print('Node 12 and 13 -> YES -> update_sub_population_category_label and update_sub_population_category_description -> Next step: Node 14')                               
                                            print(tabulate([[1, 'national'], [2, 'english'], [3, 'national and english']], headers=['ID_VARIANT', 'LANGUAGE'], tablefmt='psql'))
                                            while True:
                                                ###############################################
                                                check_node_f_12_13_variant = etl_input("Set variant (1-3) for UPDATE differencies:\n")
                                                if not (check_node_f_12_13_variant == '1' or check_node_f_12_13_variant == '2' or check_node_f_12_13_variant == '3'):
                                                    print("Sorry, not known.")
                                                    continue
                                                else:
                                                    break
                                            if check_node_f_12_13_variant == '1':
                                                res_node_f_12_13_label = run_sql(conn_dest, sql_cmd_node_f_12_13_label, data_node_f_12_13_label_source)[0][0]
                                                res_node_f_12_13_description = run_sql(conn_dest, sql_cmd_node_f_12_13_description, data_node_f_12_13_description_source)[0][0]                       
                                            elif check_node_f_12_13_variant == '2':
                                                res_node_f_12_13_label_en = run_sql(conn_dest, sql_cmd_node_f_12_13_label, data_node_f_12_13_label_en_source)[0][0]
                                                res_node_f_12_13_description_en = run_sql(conn_dest, sql_cmd_node_f_12_13_description, data_node_f_12_13_description_en_source)[0][0]
                                            elif check_node_f_12_13_variant == '3':
                                                res_node_f_12_13_label = run_sql(conn_dest, sql_cmd_node_f_12_13_label, data_node_f_12_13_label_source)[0][0]
                                                res_node_f_12_13_description = run_sql(conn_dest, sql_cmd_node_f_12_13_description, data_node_f_12_13_description_source)[0][0]
                                                res_node_f_12_13_label_en = run_sql(conn_dest, sql_cmd_node_f_12_13_label, data_node_f_12_13_label_en_source)[0][0]
                                                res_node_f_12_13_description_en = run_sql(conn_dest, sql_cmd_node_f_12_13_description, data_node_f_12_13_description_en_source)[0][0]
                                            ##############################################    Node 14
                                            while True:
                                                commit_conn_node_f = etl_input("Node 14 - Confirm a commit of UPDATE? (y/n):\n")
                                                if not (commit_conn_node_f == 'y' or commit_conn_node_f == 'n'):
                                                    print("Sorry, not known.")
                                                    continue
                                                else:
                                                    break

                                            if commit_conn_node_f == 'y':
                                                conn_dest.commit()
                                                print('Node 14 -> YES -> Process of UPDATE was commited in TARGET DB.')
                                            elif commit_conn_node_f == 'n':
                                                if check_node_f_12_13_variant == '1':
                                                    res_node_f_12_13_label = run_sql(conn_dest, sql_cmd_node_f_12_13_label, data_node_f_12_13_label_target)[0][0]
                                                    res_node_f_12_13_description = run_sql(conn_dest, sql_cmd_node_f_12_13_description, data_node_f_12_13_description_target)[0][0]                     
                                                elif check_node_f_12_13_variant == '2':
                                                    res_node_f_12_13_label_en = run_sql(conn_dest, sql_cmd_node_f_12_13_label, data_node_f_12_13_label_en_target)[0][0]
                                                    res_node_f_12_13_description_en = run_sql(conn_dest, sql_cmd_node_f_12_13_description, data_node_f_12_13_description_en_target)[0][0]
                                                elif check_node_f_12_13_variant == '3':
                                                    res_node_f_12_13_label = run_sql(conn_dest, sql_cmd_node_f_12_13_label, data_node_f_12_13_label_target)[0][0]
                                                    res_node_f_12_13_description = run_sql(conn_dest, sql_cmd_node_f_12_13_description, data_node_f_12_13_description_target)[0][0]
                                                    res_node_f_12_13_label_en = run_sql(conn_dest, sql_cmd_node_f_12_13_label, data_node_f_12_13_label_en_target)[0][0]
                                                    res_node_f_12_13_description_en = run_sql(conn_dest, sql_cmd_node_f_12_13_description, data_node_f_12_13_description_en_target)[0][0]                                    
                                                print('Node 14 -> NO -> Process of UPDATE was NOT commited.')
                                        elif make_update_node_f == 'n':
                                            print('Node 12 and 13 -> NO -> User does not want to make an update.')
                                    #--------------------------#
                                    sql_cmd_node_f_8_json = '''select target_data.fn_etl_get_sub_population_categories4update(%s,%s) as res_node_f8_json;'''
                                    data_node_f_8 = (res_node_f_5_list[int(check_node_f_7_enumerate)-1]['etl_sub_population'],res_node_f_5_list[int(check_node_f_7_enumerate)-1]['sub_population_target'])
                                    res_node_f_8_json = run_sql(conn_src, sql_cmd_node_f_8_json, data_node_f_8)[0]['res_node_f8_json']
                                    sql_cmd_node_f_9_count = sql.SQL('''select count(*) from {}.fn_etl_check_sub_population_categories4update(%s);''').format(sql.Identifier(schemaname))
                                    data_node_f_9_count = (json.dumps(res_node_f_8_json, ensure_ascii=False), )
                                    res_node_f_9_count = run_sql(conn_dest, sql_cmd_node_f_9_count, data_node_f_9_count)[0][0]
                                    if res_node_f_9_count == 0:
                                        print('Not exists any category with differences')
                                        break
                                    else:
                                        sql_cmd_node_f_8_json = '''select target_data.fn_etl_get_sub_population_categories4update(%s,%s) as res_node_f8_json;'''
                                        data_node_f_8 = (res_node_f_5_list[int(check_node_f_7_enumerate)-1]['etl_sub_population'],res_node_f_5_list[int(check_node_f_7_enumerate)-1]['sub_population_target'])
                                        res_node_f_8_json = run_sql(conn_src, sql_cmd_node_f_8_json, data_node_f_8)[0]['res_node_f8_json']
                                        sql_cmd_node_f_9_list = sql.SQL('''select sub_population_category, label_source, description_source, label_en_source, description_en_source, label_target, description_target, label_en_target, description_en_target, check_label, check_description, check_label_en, check_description_en from {}.fn_etl_check_sub_population_categories4update(%s) order by sub_population_category;''').format(sql.Identifier(schemaname))
                                        sql_cmd_node_f_9 = sql.SQL('''
                                        select array_agg(t.res order by t.sub_population_category) from
                                        (select
                                                sub_population_category,
                                                array[
                                                (row_number() over())::text,
                                                sub_population_category::text,
                                                label_source::text,
                                                case when check_label = true then 'TRUE' else 'FALSE' end,
                                                case when check_description = true then 'TRUE' else 'FALSE' end,
                                                case when check_label_en = true then 'TRUE' else 'FALSE' end,
                                                case when check_description_en = true then 'TRUE' else 'FALSE' end
                                                ] as res
                                        from {}.fn_etl_check_sub_population_categories4update(%s) order by sub_population_category
                                        ) as t
                                        ''').format(sql.Identifier(schemaname))
                                        data_node_f_9 = (json.dumps(res_node_f_8_json, ensure_ascii=False), )
                                        res_node_f_9_list = run_sql(conn_dest, sql_cmd_node_f_9_list, data_node_f_9)
                                        res_node_f_9 = run_sql(conn_dest, sql_cmd_node_f_9, data_node_f_9)[0][0]
                                        print(tabulate(res_node_f_9, headers=['ID_ENUMERATE', 'ID_SPC', 'LABEL_source', 'check_LABEL', 'check_DESCRIPTION', 'check_LABEL_EN', 'check_DESCRIPTION_EN'], tablefmt='psql'))         
                                        while True:
                                            loop_5 = etl_input("Do you want to solve an update of some category? (y/n):\n")
                                            if not (loop_5 == 'y' or loop_5 == 'n'):
                                                print("Sorry, not known.")
                                                continue
                                            else:
                                                break 
                                        if loop_5 == 'y':
                                            continue
                                        elif loop_5 == 'n':
                                            break          
                                    #--------------------------#
                            elif loop_3 == 'n':
                                print('Node 11 - NO -> user does not want to solve an update of category')

                            sql_cmd_node_f_3_count = sql.SQL('''select {}.fn_etl_check_sub_population_categories4sub_populations(%s) as res_node_f3;''').format(sql.Identifier(schemaname))
                            data_node_f_3_count = (json.dumps(res_node_f_1_json, ensure_ascii=False), )
                            res_node_f_3_count = run_sql(conn_dest, sql_cmd_node_f_3_count, data_node_f_3_count)[0]['res_node_f3']
                            if res_node_f_3_count == None:
                                print('Not exists any sub population with differences in categories -> next step: Node 15')
                                break
                            else:
                                sql_cmd_node_f_3 = sql.SQL('''select {}.fn_etl_check_sub_population_categories4sub_populations(%s) as res_node_f3;''').format(sql.Identifier(schemaname))
                                data_node_f_3 = (json.dumps(res_node_f_1_json, ensure_ascii=False), )
                                res_node_f_3 = run_sql(conn_dest, sql_cmd_node_f_3, data_node_f_3)[0]['res_node_f3']
                                sql_cmd_node_f_5_list = '''select etl_sub_population, sub_population_target from target_data.fn_etl_get_list_of_sub_populations4update(%s,%s) order by etl_sub_population, sub_population_target'''
                                sql_cmd_node_f_5 = '''
                                select array_agg(t.res order by t.etl_sub_population, t.sub_population_target) from
                                (select
                                        etl_sub_population,
                                        sub_population_target,
                                        array[
                                        (row_number() over())::text,
                                        etl_sub_population::text,
                                        sub_population_target::text,
                                        label::text,
                                        label_en::text
                                        ] as res
                                from target_data.fn_etl_get_list_of_sub_populations4update(%s,%s) order by etl_sub_population, sub_population_target
                                ) as t                    
                                '''
                                data_node_f_5 = (node7_conn,res_node_f_3)
                                res_node_f_5_list = run_sql(conn_src, sql_cmd_node_f_5_list, data_node_f_5)
                                res_node_f_5 = run_sql(conn_src, sql_cmd_node_f_5, data_node_f_5)[0][0]
                                print(tabulate(res_node_f_5, headers=['ID_ENUMERATE', 'ETL_SUB_POPULATION', 'SUB_POPULATION_target', 'LABEL_source', 'LABEL_EN_source'], tablefmt='psql'))                                
                                while True:
                                    loop_2a = etl_input("Do you want to solve an update of categories for some of sub population? (y/n):\n")
                                    if not (loop_2a == 'y' or loop_2a == 'n'):
                                        print("Sorry, not known.")
                                        continue
                                    else:
                                        break 
                                if loop_2a == 'y':
                                    continue
                                elif loop_2a == 'n':
                                    break    
                        #-------------------------------------------------------------------#
                    elif loop_2 == 'n':
                        print('NO -> user does no want to solve an update of categories for some of sub population -> next step: Node 15')
            ####################################
            ####################################
            while True:            
                check_continue = etl_input("Node 15 - Continue with process ETL? (y/n):\n")
                if not (check_continue == 'y' or check_continue == 'n'):
                    print("Sorry, not known.")
                    continue
                else:
                    break
            if check_continue == 'y':
                print('YES -> User wants to continue with process ETL. Next step: ETL - Sampling Design Synchronization')
            else:
                print('NO -> User does not want to continue with process ETL. Next step: STOP process of ETL.')
                conn_src.close()
                conn_dest.close()
                sys.exit(1)
        elif loop_1 == 'n':
            print('NO -> User does not want to make a check of SUB POPULATION CATEGORY labels and descriptions -> next step: ETL - Sampling Design Synchronization')
        print('--------------------------------------------------------------')
        ##########################################################################################
        ##########################################################################################



    ###############################################################################
    #####################################################     Sampling Design Synchronization
    print('ETL - Sampling Design Synchronization')
    print('--------------------------------------------------------------')
    print('Next step: ETL - Target variable')
    print('--------------------------------------------------------------')
    ###############################################################################
    ###############################################################################



    ###############################################################################
    #####################################################     Target variable
    print('ETL - Target variable')
    print('--------------------------------------------------------------')
    ###############################################     Node 1
    print('Node 1 - Informative list of target variables for ETL [SOURCE DB] -> next step: Node 2')
    sql_cmd_tv_node_1_count = '''select count(*) as res_tv_node_1_count from target_data.fn_etl_get_target_variable(%s,%s,%s);'''
    sql_cmd = '''select id, label, id_etl_target_variable, check_target_variable, refyearset2panel_mapping, check_atomic_area_domains, check_atomic_sub_populations, metadata from target_data.fn_etl_get_target_variable(%s,%s,%s) order by id;'''
    data_false = 'false'
    data = (node7_conn,national_language,data_false)
    res_tv_node1_count = run_sql(conn_src, sql_cmd_tv_node_1_count, data)[0]['res_tv_node_1_count']
    res_target_variable_node1_tv = run_sql(conn_src, sql_cmd, data)

    ###############################################     Node 2
    if res_tv_node1_count == 0:
        print('NO -> STOP process ETL => Not exists any target variable to ETL.')
        sys.exit(1)
    else:
        print('Node 2 - Exists any record? -> YES -> next step: Node 3')
        print('List of target variables [SOURCE DB]:')
        for i, r in enumerate(res_target_variable_node1_tv):
            print('ID_ENUMERATE: %s ID_TV: %s   LABEL_TV: %s   ID_ETL_TV: %s   CHECK_TV: %s    RYS2PM: %s  CHECK_A_AD: %s  CHECK_A_SP: %s  METADATA: %s' % (i+1, r['id'], r['label'], r['id_etl_target_variable'], r['check_target_variable'], r['refyearset2panel_mapping'], r['check_atomic_area_domains'], r['check_atomic_sub_populations'], r['metadata']))
        ###############################################     Node 3
        print('Node 3 - set ID_ENUMERATE of target variable for ETL [SOURCE DB] -> next step: Node 4')
        node3_target_variable_enumerate = etl_input("Set ID_ENUMERATE of target variable for ETL [SOURCE DB]:\n")
        #user set ID_ENUMERATE of target variable from list res_target_variable_node1_tv and automaticly is founded value ID_ETL_TV
        #sql_cmd_tv_node3 = '''select id_etl_target_variable as res_target_variable_node_3, refyearset2panel_mapping as res_target_variable_node_3_rys2pm, check_atomic_area_domains as res_target_variable_node_3_chaad, check_atomic_sub_populations as res_target_variable_node_3_chasp from target_data.fn_etl_get_target_variable(%s,%s,%s) where id = %s::integer;'''
        #data_false_tv_node3 = 'false'
        #data_tv_node3 = (node7_conn,national_language,data_false_tv_node3,node3_target_variable)
        #res_target_variable_node3 = run_sql(conn_src, sql_cmd_tv_node3, data_tv_node3)[0]['res_target_variable_node_3']
        #res_target_variable_node3_rys2pm = run_sql(conn_src, sql_cmd_tv_node3, data_tv_node3)[0]['res_target_variable_node_3_rys2pm']
        #res_target_variable_node3_chaad = run_sql(conn_src, sql_cmd_tv_node3, data_tv_node3)[0]['res_target_variable_node_3_chaad']
        #res_target_variable_node3_chasp = run_sql(conn_src, sql_cmd_tv_node3, data_tv_node3)[0]['res_target_variable_node_3_chasp']
        node3_target_variable = res_target_variable_node1_tv[int(node3_target_variable_enumerate)-1]['id']
        res_target_variable_node3 = res_target_variable_node1_tv[int(node3_target_variable_enumerate)-1]['id_etl_target_variable']
        res_target_variable_node3_rys2pm = res_target_variable_node1_tv[int(node3_target_variable_enumerate)-1]['refyearset2panel_mapping']
        res_target_variable_node3_chaad = res_target_variable_node1_tv[int(node3_target_variable_enumerate)-1]['check_atomic_area_domains']
        res_target_variable_node3_chasp = res_target_variable_node1_tv[int(node3_target_variable_enumerate)-1]['check_atomic_sub_populations']
        #print(node3_target_variable)
        #print(res_target_variable_node3)
        #print(res_target_variable_node3_rys2pm)
        #print(res_target_variable_node3_chaad)
        #print(res_target_variable_node3_chasp)
        ###############################################     Node 4
        if res_target_variable_node3 == None:
            print('Node 4 - Exists id_etl_target_variable? -> NO -> next step: Node 5')
            _variables = 0
            _atomic_problem = 0
            ###############################################     Node 5
            print('Node 5 - get target variable metadata [SOURCE DB] -> next step: Node 6')
            sql_cmd = '''select target_data.fn_etl_get_target_variable_metadata(%s,%s) as res_target_variable_node_5;'''
            data = (node3_target_variable, national_language)
            res_target_variable_node5 = run_sql(conn_src, sql_cmd, data)[0]['res_target_variable_node_5']
            ###############################################     Node 6
            #######################################
            ## preparing data for Node6 and Node 8
            sql_cmd_tv_for_node6_8 = '''select target_data.fn_etl_get_etl_ids_of_target_variable(%s) as res_tv_for_node6_8'''
            data_tv_for_node6_8 = (node7_conn,)
            res_tv_for_node6_8 = run_sql(conn_src, sql_cmd_tv_for_node6_8, data_tv_for_node6_8)[0]['res_tv_for_node6_8']
            #######################################
            print('Node 6 - check target variable [TARGET DB] -> next step: Node 7')
            if res_tv_for_node6_8 == None:
                sql_cmd = sql.SQL('''select fn_etl_check_target_variable as res_target_variable_node_6 from {}.fn_etl_check_target_variable(%s);''').format(sql.Identifier(schemaname))
                metadata = (json.dumps(res_target_variable_node5, ensure_ascii=False), )
            else:
                sql_cmd = sql.SQL('''select fn_etl_check_target_variable as res_target_variable_node_6 from {}.fn_etl_check_target_variable(%s,%s);''').format(sql.Identifier(schemaname))
                metadata = (json.dumps(res_target_variable_node5, ensure_ascii=False),res_tv_for_node6_8)            

            res_target_variable_node6 = run_sql(conn_dest, sql_cmd, metadata)[0]['res_target_variable_node_6']
            ###############################################     Node 7
            if res_target_variable_node6 == None:
                print('Node 7, Exists id? -> NO -> next step: Node 8')
                ###############################################     Node 8
                print('Node 8 - get list of target variables [TARGET DB] -> next step: Node 9')
                if res_tv_for_node6_8 == None:
                    sql_cmd = sql.SQL('''select count(*) as res_target_variable_8 from {}.fn_etl_get_target_variable(%s);''').format(sql.Identifier(schemaname))
                    data = (national_language, )
                else:
                    sql_cmd = sql.SQL('''select count(*) as res_target_variable_8 from {}.fn_etl_get_target_variable(%s,%s);''').format(sql.Identifier(schemaname))
                    data = (national_language,res_tv_for_node6_8)

                res_target_variable_node8 = run_sql(conn_dest, sql_cmd, data)

                if res_target_variable_node8[0]['res_target_variable_8'] == 0:
                    print('List of target variables [TARGET DB]:')
                    print('No target variable')
                else:
                    #sql_cmd = '''select id, jsonb_pretty(metadata::jsonb) as metadata from nfiesta_test.fn_etl_get_target_variable(%s);'''
                    sql_cmd = sql.SQL('''select id, jsonb_pretty(metadata::jsonb) as metadata from {}.fn_etl_get_target_variable(%s);''').format(sql.Identifier(schemaname))
                    print('List of target variables [TARGET DB]:')
                    data = (national_language, )
                    res_target_variable_node8_list = run_sql(conn_dest, sql_cmd, data)
                    for r in res_target_variable_node8_list:
                        print('ID: %s   METADATA: %s' % (r['id'], r['metadata']))

                if res_target_variable_node8[0]['res_target_variable_8'] == 0:
                    ###############################################     Node 9
                    print('Node 9, Exists any record? -> NO -> next step: Node 12')
                    ###############################################     Node 12
                    print('Node 12, import taget variable [TARGET DB] -> next step: Node 14')
                    #sql_cmd = '''select fn_etl_import_target_variable as res_target_variable_node_12 from nfiesta_test.fn_etl_import_target_variable(%s);'''
                    sql_cmd = sql.SQL('''select fn_etl_import_target_variable as res_target_variable_node_12 from {}.fn_etl_import_target_variable(%s);''').format(sql.Identifier(schemaname))
                    metadata = (json.dumps(res_target_variable_node5, ensure_ascii=False), )
                    res_target_variable_node12 = run_sql(conn_dest, sql_cmd, metadata)[0]['res_target_variable_node_12']
                    ###############################################     Node 14
                    print('Node 14 - save target variable [SOURCE DB] -> next step: ETL - Panel and reference year set combinations')
                    sql_cmd = '''select target_data.fn_etl_save_target_variable(%s,%s,%s) as res_target_variable_node_14;'''
                    data = (node7_conn,node3_target_variable,res_target_variable_node12)
                    res_target_variable_node14 = run_sql(conn_src, sql_cmd, data)[0]['res_target_variable_node_14']
                else:
                    print('Node 9, Exists any record? -> YES -> next step: Node 10')
                    ###############################################     Node 10
                    print('Node 10, Using metadatas, manually step by step search and assign the target variable [TARGET DB] -> next step: Node 11')
                    while True:
                        ###############################################     Node 11
                        node11_target_variable_yes_or_no = etl_input("Node 11 - Exists ID of target variable? (y/n):\n")
                        if not (node11_target_variable_yes_or_no == 'y' or node11_target_variable_yes_or_no == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break

                    if node11_target_variable_yes_or_no == 'y':
                        print('YES -> next step: Manually choose and set ID of target variable for ETL [TARGET DB] -> next step: Node 13')
                        target_variable4node13or14 = etl_input("Node 11 - Set ID of target variable [TARGET DB]:\n")
                        ###############################################     Node 13
                        print('Node 13, import target variable metadata [TARGET DB] -> next step: Node 14')
                        #sql_cmd = '''select fn_etl_import_target_variable_metadata as res_target_variable_node_13 from nfiesta_test.fn_etl_import_target_variable_metadata(%s,%s);'''
                        sql_cmd = sql.SQL('''select fn_etl_import_target_variable_metadata as res_target_variable_node_13 from {}.fn_etl_import_target_variable_metadata(%s,%s);''').format(sql.Identifier(schemaname))
                        data = (target_variable4node13or14,json.dumps(res_target_variable_node5, ensure_ascii=False))
                        res_target_variable_node13 = run_sql(conn_dest, sql_cmd, data)[0]['res_target_variable_node_13']
                        print(res_target_variable_node13)
                        ###############################################     Node 14
                        print('Node 14 - save target variable [SOURCE DB] -> next step: ETL - Panel and reference year set combinations')
                        sql_cmd = '''select target_data.fn_etl_save_target_variable(%s,%s,%s) as res_target_variable_node_14;'''
                        data = (node7_conn,node3_target_variable,target_variable4node13or14)
                        res_target_variable_node14 = run_sql(conn_src, sql_cmd, data)[0]['res_target_variable_node_14']
                    elif node11_target_variable_yes_or_no == 'n':
                        print('NO -> next step: Node 12')
                        ###############################################     Node 12
                        print('Node 12, import target variable [TARGET DB] -> next step: Node 14')
                        #sql_cmd = '''select fn_etl_import_target_variable as res_target_variable_node_12 from nfiesta_test.fn_etl_import_target_variable(%s);'''
                        sql_cmd = sql.SQL('''select fn_etl_import_target_variable as res_target_variable_node_12 from {}.fn_etl_import_target_variable(%s);''').format(sql.Identifier(schemaname))
                        metadata = (json.dumps(res_target_variable_node5, ensure_ascii=False), )
                        res_target_variable_node12 = run_sql(conn_dest, sql_cmd, metadata)[0]['res_target_variable_node_12']
                        ###############################################     Node 14
                        print('Node 14 - save target variable [SOURCE DB] -> next step: ETL - Panel and reference year set combinations')
                        sql_cmd = '''select target_data.fn_etl_save_target_variable(%s,%s,%s) as res_target_variable_node_14;'''
                        data = (node7_conn,node3_target_variable,res_target_variable_node12)
                        res_target_variable_node14 = run_sql(conn_src, sql_cmd, data)[0]['res_target_variable_node_14']
            else:
                ###############################################     Node 7
                print('Node 7, Exists id? -> YES -> next step: Node 13')
                ###############################################     Node 13
                print('Node 13, import target variable metadata [TARGET DB] -> next step: Node 14')
                #sql_cmd = '''select fn_etl_import_target_variable_metadata as res_target_variable_node_13 from nfiesta_test.fn_etl_import_target_variable_metadata(%s,%s);'''
                sql_cmd = sql.SQL('''select fn_etl_import_target_variable_metadata as res_target_variable_node_13 from {}.fn_etl_import_target_variable_metadata(%s,%s);''').format(sql.Identifier(schemaname))
                data = (res_target_variable_node6,json.dumps(res_target_variable_node5, ensure_ascii=False))
                res_target_variable_node13 = run_sql(conn_dest, sql_cmd, data)[0]['res_target_variable_node_13']
                print(res_target_variable_node13)
                ###############################################     Node 14
                print('Node 14 - save target variable [SOURCE DB] -> next step: ETL - Panel and reference year set combinations')
                sql_cmd = '''select target_data.fn_etl_save_target_variable(%s,%s,%s) as res_target_variable_node_14;'''
                data = (node7_conn,node3_target_variable,res_target_variable_node6)
                res_target_variable_node14 = run_sql(conn_src, sql_cmd, data)[0]['res_target_variable_node_14']
        else:
            _variables = 1

            if res_target_variable_node3_chaad == False or res_target_variable_node3_chasp == False:
                _atomic_problem = 1
            else:
                _atomic_problem = 0   

            #print('_atomic_problem')
            #print(_atomic_problem)     

            print('Node 4 - Exists id_etl_target_variable? -> YES -> next step: Node 15')
            while True:
                ###############################################     Node 15
                node15_target_variable = etl_input("Node 15 - Make a check of target variable metadatas? (y/n):\n")
                if not (node15_target_variable == 'y' or node15_target_variable == 'n'):
                    print("Sorry, not known.")
                    continue
                else:
                    break

            if node15_target_variable == 'y':
                print('YES -> next step: Check of metadata for target variable => user wants to make a check of metadata for choosen target variable.')
                ##########################################################################################
                ####### CHECK OF METADATA FOR TARGET VARIABLE ############################################
                ##########################################################################################
                ###############################################   CHM - Node 1
                print('CHM - Node 1 - get target variable metadatas [SOURCE DB]:')
                sql_cmd_chm_node_1 = '''select target_data.fn_etl_get_target_variable_metadatas(%s,%s) as res_chm_node_1;'''
                #print(res_target_variable_node3)
                #print(national_language)
                data = ([res_target_variable_node3],national_language)
                res_chm_node1 = run_sql(conn_src, sql_cmd_chm_node_1, data)[0]['res_chm_node_1']
                print(res_chm_node1)
                ##############################################     CHM - Node 2
                print('CHM - Node 2 - check target variable metadatas [TARGET DB] -> next step: Node 3')
                sql_cmd_chm_node_2_list = sql.SQL('''select target_variable_source, target_variable_target, metadata_source, metadata_target, metadata_diff, metadata_diff_national_language, metadata_diff_english_language, missing_metadata_national_language from {}.fn_etl_check_target_variable_metadatas(%s,%s,%s);''').format(sql.Identifier(schemaname))
                sql_cmd_chm_node_2_count = sql.SQL('''select count(*) as res_chm_node_2_count from {}.fn_etl_check_target_variable_metadatas(%s,%s,%s);''').format(sql.Identifier(schemaname))
                data_true = 'true'
                metadata = (json.dumps(res_chm_node1, ensure_ascii=False),national_language,data_true)
                res_chm_node2_list = run_sql(conn_dest, sql_cmd_chm_node_2_list, metadata)
                res_chm_node2_count = run_sql(conn_dest, sql_cmd_chm_node_2_count, metadata)[0]['res_chm_node_2_count']
                #print(res_chm_node2_count)
                ##############################################     CHM - Node 3
                if res_chm_node2_count == 0:
                    print('CHM - Node 3 - Exists any record? -> NO -> Metadatas for given target variable are the same -> next step: Node 10')
                    # NODE - Continue with process ETL!
                    while True:
                        node_continue_after_node3 = etl_input("CHM - Node 10 - Continue with process ETL? (y/n):\n")
                        if not (node_continue_after_node3 == 'y' or node_continue_after_node3 == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break

                    if node_continue_after_node3 == 'y':
                        print('YES -> User wants to continue with process ETL -> next step: ETL - Attribute variables')
                    elif node_continue_after_node3 == 'n':
                        print('NO -> User does not want to continue with process ETL -> next step: STOP process ETL!')
                        sys.exit(1)
                else:
                    print('CHM - Node 3 - Exists any record? -> YES -> Metadatas for given target variable are different -> next step: Node 4')
                    ##############################################     CHM - Node 4
                    print('CHM - Node 4 - List of target variable [SOURCE DB]:')
                    for i,r in enumerate(res_chm_node2_list):
                        print('ID: %s   TVSD: %s   TVTD: %s   MSD: %s   MTD: %s  MDIFF: %s  MDIFFNL: %s MDIFFEL: %s MISSMDNL: %s' % (i+1, r['target_variable_source'], r['target_variable_target'], r['metadata_source'], r['metadata_target'], r['metadata_diff'], r['metadata_diff_national_language'], r['metadata_diff_english_language'], r['missing_metadata_national_language']))
                    while True:
                        ##############################################     CHM - Node 5
                        check_chm_tv_ko_node_5 = etl_input("CHM - Node 5 - Solve an update or insert of metadata for this target variable? (y/n):\n")
                        if not (check_chm_tv_ko_node_5 == 'y' or check_chm_tv_ko_node_5 == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break

                    if check_chm_tv_ko_node_5 == 'y':
                        print('YES -> User wants to solve an update or insert of metadata -> next step: Node 6')
                        ##############################################     CHM - Node 6
                        check_chm_tv_ko_node_6_id = 1

                        while True:
                            check_chm_tv_ko_node_6_action = etl_input("CHM - Node 6 - Set an action for target varible to change: example (without quotes): 'update' or 'insert'\n")
                            if not (check_chm_tv_ko_node_6_action == 'update' or check_chm_tv_ko_node_6_action == 'insert'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if check_chm_tv_ko_node_6_action == 'insert':
                            ##############################################     CHM - Node 8
                            print('Node 8 - INSERT target variable metadata -> next step: Node 9')
                            sql_cmd_chm_node_8_insert = sql.SQL('''select fn_etl_insert_target_variable_metadata as res_chm_node_8_insert from {}.fn_etl_insert_target_variable_metadata(%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                            data_chm_node_8_insert =  (
                                                        res_chm_node2_list[int(check_chm_tv_ko_node_6_id)-1]['target_variable_target'],
                                                        json.dumps((res_chm_node2_list[int(check_chm_tv_ko_node_6_id)-1]['metadata_source']), ensure_ascii=False),
                                                        res_chm_node2_list[int(check_chm_tv_ko_node_6_id)-1]['missing_metadata_national_language'],
                                                        national_language
                                                        )
                            res_chm_node8_insert = run_sql(conn_dest, sql_cmd_chm_node_8_insert, data_chm_node_8_insert)[0]['res_chm_node_8_insert']
                            #print(res_chm_node8_insert)
                        else:
                            ## branch for 'UPDATE'
                            ##############################################     CHM - Node 6 [SET attributes for languages]
                            while True:
                                check_chm_tv_ko_node_6_el_boolean = etl_input("CHM - Node 6 - Set english language for update (true/false):\n")
                                if not (check_chm_tv_ko_node_6_el_boolean == 'true' or check_chm_tv_ko_node_6_el_boolean == 'false'):
                                    print("Sorry, not known.")
                                    continue
                                else:
                                    break

                            while True:
                                check_chm_tv_ko_node_6_nl_boolean = etl_input("CHM - Node 6 - Set national language for update (true/false):\n")
                                if not (check_chm_tv_ko_node_6_nl_boolean == 'true' or check_chm_tv_ko_node_6_nl_boolean == 'false'):
                                    print("Sorry, not known.")
                                    continue
                                else:
                                    break                           
                            ##############################################     CHM - Node 7
                            print('CHM - Node 7 - UPDATE target variable metadata -> next step: Node 9')
                            sql_cmd_chm_node_7_update = sql.SQL('''select fn_etl_update_target_variable_metadata as res_chm_node_7_update from {}.fn_etl_update_target_variable_metadata(%s,%s,%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                            data_chm_node_7_update =  (
                                                        res_chm_node2_list[int(check_chm_tv_ko_node_6_id)-1]['target_variable_target'],
                                                        json.dumps((res_chm_node2_list[int(check_chm_tv_ko_node_6_id)-1]['metadata_source']), ensure_ascii=False),
                                                        res_chm_node2_list[int(check_chm_tv_ko_node_6_id)-1]['metadata_diff_national_language'],
                                                        res_chm_node2_list[int(check_chm_tv_ko_node_6_id)-1]['metadata_diff_english_language'],
                                                        res_chm_node2_list[int(check_chm_tv_ko_node_6_id)-1]['missing_metadata_national_language'],
                                                        check_chm_tv_ko_node_6_el_boolean,
                                                        check_chm_tv_ko_node_6_nl_boolean,
                                                        national_language
                                                        )
                            res_chm_node7_update = run_sql(conn_dest, sql_cmd_chm_node_7_update, data_chm_node_7_update)[0]['res_chm_node_7_update']
                            #print(res_chm_node7_update)
                        ###############################################################################
                        #######################################################   CONFIRM CHANGES
                        ##############################################     CHM - Node 9
                        while True:
                            commit_conn_chm = etl_input("Node 9 - Confirm a commit of UPDATE or INSERT metadatas? (y/n):\n")
                            if not (commit_conn_chm == 'y' or commit_conn_chm == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if commit_conn_chm == 'y':
                            conn_src.commit()
                            conn_dest.commit()

                            if check_chm_tv_ko_node_6_action == 'insert':
                                print('Node 9 -> YES -> Process of INSERT metadatas was commited. Next step: Node 10')
                            else:
                                print('Node 9 -> YES -> Process of UPDATE metadatas was commited. Next step: Node 10')

                        elif commit_conn_chm == 'n':
                            if check_chm_tv_ko_node_6_action == 'insert':
                                print('Node 9 -> NO -> Process of INSERT metadatas was NOT commited. Next step: Node 10')
                            else:
                                print('Node 9 -> NO -> Process of UPDATE metadatas was NOT commited. Next step: Node 10')

                        while True:
                            check_chm_continue = etl_input("CHM - Node 10 - Continue with process ETL? (y/n):\n")
                            if not (check_chm_continue == 'y' or check_chm_continue == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if check_chm_continue == 'y':
                            print('YES -> User wants to continue with process ETL. Next step: ETL - Attribute variables')
                        else:
                            ###############################################################################
                            ###########################################################     DB CONN CLOSE
                            print('NO -> User does not want to continue with process ETL. Next step: STOP process of ETL.')
                            conn_src.close()
                            conn_dest.close()
                            sys.exit(1)
                        ###############################################################################
                    elif check_chm_tv_ko_node_5 == 'n':
                        print('NO -> User does not want to solve an update of metadatas -> next step: Node 10')
                        ##############################################     CHM - Node 10
                        while True:
                            node_continue_after_node5 = etl_input("CHM - Node 10- Continue with process ETL? (y/n):\n")
                            if not (node_continue_after_node5 == 'y' or node_continue_after_node5 == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if node_continue_after_node5 == 'y':
                            print('YES -> User wants to continue with process ETL -> next step: ETL - Attribute variables')
                        elif node_continue_after_node5 == 'n':
                            print('NO -> User does not want to continue with process ETL -> next step: STOP process ETL!')
                            conn_src.close()
                            conn_dest.close()
                            sys.exit(1)
                ##########################################################################################
                ##########################################################################################
            elif node15_target_variable == 'n':
                print('NO -> User does not want to make a check of metadata for choosen target variable -> next step: ETL - Attribute variables')
        print('--------------------------------------------------------------')
    ###############################################################################
    ###############################################################################



    ###############################################################################
    #####################################################   LABEL Target Variable
    sql_cmd_target_variable_label_nl_en = '''select label as target_variable_label_nl, label_en as target_variable_label_en from target_data.c_target_variable where id = (select target_variable from target_data.t_etl_target_variable where id = %s)'''

    if _variables == 1:
        data_target_variable_label_nl_en = (res_target_variable_node3,)
    else:
        data_target_variable_label_nl_en = (res_target_variable_node14,)

    if national_language == 'en':
        res_target_variable_label_nl_en = run_sql(conn_src,sql_cmd_target_variable_label_nl_en,data_target_variable_label_nl_en)[0]['target_variable_label_en']
    else:
        res_target_variable_label_nl_en = run_sql(conn_src,sql_cmd_target_variable_label_nl_en,data_target_variable_label_nl_en)[0]['target_variable_label_nl']
    ###############################################################################
    ###############################################################################



    ###############################################################################
    ###############################################################################
    if _variables == 0: # not exists ETL_ID and _atomic_problem is always value 0 => next step will be always ETL - Panel and reference year set combinations
        ###############################################################################
        ###############################     Panel and reference year set combinations
        print('ETL - Panel and reference year set combinations')
        print('--------------------------------------------------------------')
        ###############################################     Node 1
        print('Node 1 - get available list of combinations of panels and reference year sets for target variable "%s" [SOURCE DB] -> next step: Node 2' % (res_target_variable_label_nl_en))
        sql_cmd = '''select refyearset2panel, panel, reference_year_set, passed_by_module from target_data.fn_etl_get_panel_refyearset_combinations(%s,%s) order by refyearset2panel;'''
        data = (res_target_variable_node14,res_target_variable_node3_rys2pm)
        res_comb_panels_rysets = run_sql(conn_src, sql_cmd, data)
        ###############################################     Node 2
        lst = []
        print('List of combinations [SOURCE DB]:')
        for r in res_comb_panels_rysets:
            lst.append(r['refyearset2panel']) # adding the element
            print('ID: %s   PANEL: %s   REFERENCE_YEAR_SET: %s  PASSED_BY_MODULE:   %s' % (r['refyearset2panel'], r['panel'], r['reference_year_set'], r['passed_by_module']))
        ###############################################     Node 3
        while True:
            node3_comb_panels_rysets = etl_input("Node 3 - Continue with process ETL? (y/n):\n")
            if not (node3_comb_panels_rysets == 'y' or node3_comb_panels_rysets == 'n'):
                print("Sorry, not known.")
                continue
            else:
                break

        if node3_comb_panels_rysets == 'y':
            print('YES -> user wants to continue with ETL process -> next step: Node 4')
        else:
            print('NO -> user does not want to continue with ETL process -> next step: STOP process ETL')
            sys.exit(1)

        ###############################################     Node 4
        print('Node 4 - choose and set IDs of combinations of panels and referece year sets for ETL [SOURCE DB] -> next step: ETL - Area domain and area domain category')
        print(lst)
        lst_input = etl_input("Insert array of IDs (you can copy - paste from line above):\n")
        lst = list(map(int, lst_input.lstrip('[').rstrip(']').replace(' ', '').split(',')))
        print('--------------------------------------------------------------')
    else:
        # branch that ETL_ID always exist => _variables == 1
        ##########################################################################################
        # Node C
        ##########################################################################################
        print('--------------------------------------------------------------')
        print('Node C - Is the choosen target variable a target variable with missing atomic ADC or SPC?')
        if _atomic_problem == 0:
            print('No - next step: ETL - Panel and reference year set combinations')
            print('--------------------------------------------------------------')
            ###############################################################################
            ###############################     Panel and reference year set combinations
            print('ETL - Panel and reference year set combinations')
            print('--------------------------------------------------------------')
            ###############################################     Node 1
            print('Node 1 - get available list of combinations of panels and reference year sets for target variable "%s" [SOURCE DB] -> next step: Node 2' % (res_target_variable_label_nl_en))
            sql_cmd = '''select refyearset2panel, panel, reference_year_set, passed_by_module from target_data.fn_etl_get_panel_refyearset_combinations(%s,%s) order by refyearset2panel;'''
            data = (res_target_variable_node3,res_target_variable_node3_rys2pm)
            res_comb_panels_rysets = run_sql(conn_src, sql_cmd, data)
            ###############################################     Node 2
            lst = []
            print('List of combinations [SOURCE DB]:')
            for r in res_comb_panels_rysets:
                lst.append(r['refyearset2panel']) # adding the element
                print('ID: %s   PANEL: %s   REFERENCE_YEAR_SET: %s  PASSED_BY_MODULE:   %s' % (r['refyearset2panel'], r['panel'], r['reference_year_set'], r['passed_by_module']))
            ###############################################     Node 3
            while True:
                node3_comb_panels_rysets = etl_input("Node 3 - Continue with process ETL? (y/n):\n")
                if not (node3_comb_panels_rysets == 'y' or node3_comb_panels_rysets == 'n'):
                    print("Sorry, not known.")
                    continue
                else:
                    break

            if node3_comb_panels_rysets == 'y':
                print('YES -> user wants to continue with ETL process -> next step: Node 4')
            else:
                print('NO -> user does not want to continue with ETL process -> next step: STOP process ETL')
                sys.exit(1)

            ###############################################     Node 4
            print('Node 4 - choose and set IDs of combinations of panels and referece year sets for ETL [SOURCE DB] -> next step: ETL - Area domain and area domain category')
            print(lst)
            lst_input = etl_input("Insert array of IDs (you can copy - paste from line above):\n")
            lst = list(map(int, lst_input.lstrip('[').rstrip(']').split(', ')))
            print('--------------------------------------------------------------')
        else:
            print('YES - next step: ETL - Area domain and Area domain category')
            print('--------------------------------------------------------------')
    ###############################################################################
    ###############################################################################



    ###############################################################################
    # internal argument lst
    ###############################################################################
    if _atomic_problem == 0:
        lst = lst
    else:
        lst = res_target_variable_node3_rys2pm
    ###############################################################################
    ###############################################################################



    ###############################################################################
    ###############################################################################
    #####################################################     Area domain and Area domain category
    print('ETL - Area domain and Area domain category')
    print('--------------------------------------------------------------')
    ###############################################     Node 1
    print('Node 1 - check area domain [SOURCE DB]')

    # sql_cmds AD
    sql_cmd_ad_check_records = '''select count(*) as res_ad_node_1_check_records from target_data.fn_etl_check_area_domain(%s,%s);'''
    sql_cmd_ad_list = '''select * from target_data.fn_etl_check_area_domain(%s,%s);'''
    sql_cmd_ad_check_filled = '''select count(*) as res_ad_node_4_check_filled from target_data.fn_etl_check_area_domain(%s,%s) where etl_id is null;''' # zero rows => it means that etl_id is filled for all rows
    sql_cmd_ad_list_cycle = '''select * from target_data.fn_etl_check_area_domain(%s,%s) where etl_id is null;'''
    sql_cmd_ad_etl_id = '''select array_agg(etl_id) as res_ad_node_1_etl_id from target_data.fn_etl_check_area_domain(%s,%s) where etl_id is not null;'''

    if _variables == 1:
        data_id_t_etl_tv = (lst,res_target_variable_node3)
    else:
        data_id_t_etl_tv = (lst,res_target_variable_node14)

    res_area_domain_node1_check_records = run_sql(conn_src, sql_cmd_ad_check_records, data_id_t_etl_tv)[0]['res_ad_node_1_check_records']

    if res_area_domain_node1_check_records > 0:
        ###############################################     Node 2
        print('Node 2 - Any record returned? -> YES -> next step: Node 3')
        ###############################################     Node 3
        print('Node 3 - Display of selected columns from Node 1 [SOURCE DB] -> next step: Node 4')
        res_area_domain_node1_list = run_sql(conn_src, sql_cmd_ad_list, data_id_t_etl_tv) # TABLE from NODE 1
        for row in res_area_domain_node1_list:
            print('ID: %s   LABEL: %s   ETL_ID: %s' % (row[0], row[5], row[7]))

        res_area_domain_node4_check_filled = run_sql(conn_src, sql_cmd_ad_check_filled, data_id_t_etl_tv)[0]['res_ad_node_4_check_filled']
        if res_area_domain_node4_check_filled > 0:
            ###############################################     Node 4
            print('Node 4 - Is etl_id not null for all records [SOURCE DB]? -> NO -> next step: Node 5')
            res_area_domain_node4_list_cycle = run_sql(conn_src, sql_cmd_ad_list_cycle, data_id_t_etl_tv) # LIST for cycle
            print('List of area domains where etl_id is null [SOURCE DB]:')
            for row in res_area_domain_node4_list_cycle:
                print('ID: %s   LABEL: %s   ETL_ID: %s' % (row[0], row[5], row[7]))

            #---- cycle for area domains -------#
            print('BEGIN cycle for area domains where etl_id is null [SOURCE DB]')
            for r in res_area_domain_node4_list_cycle:
                ###############################################     Node 5
                print('--------------')
                if national_language == 'en':
                    print('!!! CYCLE for area domain: "%s"' % (r[5]))
                else:
                    print('!!! CYCLE for area domain: "%s"' % (r[3]))
                print('--------------')
                print('Node 5 - get area domains [TARGET DB] -> next step: Node 6')
                #res_ad_node1_i_etl_id = run_sql(conn_src, sql_cmd_ad_etl_id, data_id_t_etl_tv)[0]['res_ad_node_1_etl_id'] #zjisteni seznamu etl_id pro except !!!
                #preparing ETL_IDS for node 5
                sql_cmd_get_etl_ids_of_area_domain = '''select * from target_data.fn_etl_get_etl_ids_of_area_domain(%s);'''
                data_etl_ids_of_area_domain = (node7_conn,)
                res_etl_ids_of_area_domain = run_sql(conn_src,sql_cmd_get_etl_ids_of_area_domain,data_etl_ids_of_area_domain)[0][0]
                #print(res_etl_ids_of_area_domain)

                if res_etl_ids_of_area_domain == None:
                    etl_id_node5_i = [0]
                else:
                    etl_id_node5_i = res_etl_ids_of_area_domain

                data_ad_node5_i = (r[0],etl_id_node5_i)
                #sql_cmd_ad_node5_i_check_records = '''select count(*) as res_ad_node_5_check_records from nfiesta_test.fn_etl_get_area_domains(%s,%s);'''
                sql_cmd_ad_node5_i_check_records = sql.SQL('''select count(*) as res_ad_node_5_check_records from {}.fn_etl_get_area_domains(%s,%s);''').format(sql.Identifier(schemaname))
                #sql_cmd_ad_node5_i_list = '''select * from nfiesta_test.fn_etl_get_area_domains(%s,%s);'''
                sql_cmd_ad_node5_i_list = sql.SQL('''select * from {}.fn_etl_get_area_domains(%s,%s);''').format(sql.Identifier(schemaname))

                res_ad_node5_i_check_records = run_sql(conn_dest, sql_cmd_ad_node5_i_check_records, data_ad_node5_i)[0]['res_ad_node_5_check_records']
                res_ad_node5_i_list = run_sql(conn_dest, sql_cmd_ad_node5_i_list, data_ad_node5_i)

                if res_ad_node5_i_check_records == 0:
                    ###############################################     Node 6
                    print('Node 6 - Any record returned? -> NO -> next step: Node 11')
                    ###############################################     Node 11
                    print('Node 11 - import area domain [TARGET DB] -> next step: Node 13')
                    #sql_cmd_ad_node11_i_import = '''select etl_id as res_ad_node_11_import from nfiesta_test.fn_etl_import_area_domain(%s,%s,%s,%s,%s);'''
                    sql_cmd_ad_node11_i_import = sql.SQL('''select etl_id as res_ad_node_11_import from {}.fn_etl_import_area_domain(%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                    data_ad_node11_i = (r[0],r[3],r[4],r[5],r[6],r[9])
                    res_ad_node11_i_import = run_sql(conn_dest, sql_cmd_ad_node11_i_import, data_ad_node11_i)[0]['res_ad_node_11_import']
                    ###############################################     Node 13
                    print('Node 13 - save area domain [SOURCE DB] -> next step: Node 14')
                    sql_cmd_ad_node13_i_save = '''select target_data.fn_etl_save_area_domain(%s,%s,%s) as res_ad_node_13_save'''
                    data_ad_node13_i = (r[1],r[2],res_ad_node11_i_import)
                    res_ad_node13_i_save = run_sql(conn_src, sql_cmd_ad_node13_i_save, data_ad_node13_i)
                    ###############################################     Node 14
                    print('Node 14 is not needed in python skript [SOURCE DB]')
                else:
                    ###############################################     Node 6
                    print('Node 6 - Any record returned? -> YES -> next step: Node 7')
                    ###############################################     Node 7
                    print('Node 7 - check area domain [TARGET DB] -> next step: Node 8')
                    #sql_cmd_ad_node7_i_check_records = '''select count(*) as res_ad_node_7_check_records from nfiesta_test.fn_etl_check_area_domain(%s,%s) where etl_id is not null;'''
                    sql_cmd_ad_node7_i_check_records = sql.SQL('''select count(*) as res_ad_node_7_check_records from {}.fn_etl_check_area_domain(%s,%s) where etl_id is not null;''').format(sql.Identifier(schemaname))
                    #sql_cmd_ad_node7_i_list = '''select * from nfiesta_test.fn_etl_check_area_domain(%s,%s);'''
                    sql_cmd_ad_node7_i_list = sql.SQL('''select * from {}.fn_etl_check_area_domain(%s,%s);''').format(sql.Identifier(schemaname))
                    #print(r[0])
                    #print(r[5])
                    data_ad_node7_i = (r[0],r[5])
                    res_ad_node7_i_check_records = run_sql(conn_dest, sql_cmd_ad_node7_i_check_records, data_ad_node7_i)
                    res_ad_node7_i_list = run_sql(conn_dest, sql_cmd_ad_node7_i_list, data_ad_node7_i)

                    if res_ad_node7_i_check_records[0]['res_ad_node_7_check_records'] == 0:
                        ###############################################     Node 8
                        print('Node 8 - Exists etl_id? -> NO -> next step: Node 9')
                        ###############################################     Node 9
                        print('Node 9 - Using labels and descriptions, manually search and assign the area domain (respectively etl_id) [TARGET DB] -> next step: Node 10')
                        print('List of area domains [TARGET DB]:')
                        for row in res_ad_node5_i_list:
                            print('CYCLE_ID: %s   ETL_ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s   ATOMIC: %s' % (row[0], row[1], row[2], row[3], row[4], row[5], row[6]))
                        ###############################################     Node 10
                        while True:
                            node10_ad = etl_input("Node 10 - Exists etl_id? (y/n):\n")
                            if not (node10_ad == 'y' or node10_ad == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if node10_ad == 'y':
                            print('YES -> next step: Node 13')
                            ###############################################     Node 13
                            print('Node 13 - save area domain [SOURCE DB] -> next step: Node 14')
                            sql_cmd_ad_node13_i_save = '''select target_data.fn_etl_save_area_domain(%s,%s,%s) as res_ad_node_13_save'''
                            node13_ad_save = etl_input("Set ETL_ID of area domain [choosing from TARGET DB and saving into SOURCE DB]:\n")
                            data_ad_node13_i = (r[1],r[2],node13_ad_save)
                            res_ad_node13_i_save = run_sql(conn_src, sql_cmd_ad_node13_i_save, data_ad_node13_i)
                            ###############################################     Node 14
                            print('Node 14 is not needed in python skript [SOURCE DB]')
                        elif node10_ad == 'n':
                            print('NO -> next step: Node 11')
                            ###############################################     Node 11
                            print('Node 11 - import area domain [TARGET DB] -> next step: Node 13')
                            #sql_cmd_ad_node11_i_import = '''select etl_id as res_ad_node_11_import from nfiesta_test.fn_etl_import_area_domain(%s,%s,%s,%s,%s);'''
                            sql_cmd_ad_node11_i_import = sql.SQL('''select etl_id as res_ad_node_11_import from {}.fn_etl_import_area_domain(%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                            data_ad_node11_i = (r[0],r[3],r[4],r[5],r[6],r[9])
                            res_ad_node11_i_import = run_sql(conn_dest, sql_cmd_ad_node11_i_import, data_ad_node11_i)[0]['res_ad_node_11_import']
                            ###############################################     Node 13
                            print('Node 13 - save area domain [SOURCE DB] -> next step: Node 14')
                            sql_cmd_ad_node13_i_save = '''select target_data.fn_etl_save_area_domain(%s,%s,%s) as res_ad_node_13_save'''
                            data_ad_node13_i = (r[1],r[2],res_ad_node11_i_import)
                            res_ad_node13_i_save = run_sql(conn_src, sql_cmd_ad_node13_i_save, data_ad_node13_i)
                            ###############################################     Node 14
                            print('Node 14 is not needed in python skript [SOURCE DB]')
                    else:
                        ###############################################     Node 8
                        print('Node 8 - Exists etl_id? -> YES -> next step: Node 12')
                        ###############################################     Node 12
                        print('Information label and description of area domain [TARGET DB]:')
                        for row in res_ad_node7_i_list:
                            print('CYCLE_ID: %s   ETL_ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s   ATOMIC: %s' % (row[0], row[1], row[2], row[3], row[4], row[5], row[6]))

                        while True:
                            node12_ad = etl_input("Node 12 - Is etl_id correct? (y/n):\n")
                            if not (node12_ad == 'y' or node12_ad == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if node12_ad == 'y':
                            print('YES -> next step: Node 13')
                            ###############################################     Node 13
                            print('Node 13 - save area domain [SOURCE DB] -> next step: Node 14')
                            sql_cmd_ad_node13_i_save = '''select target_data.fn_etl_save_area_domain(%s,%s,%s) as res_ad_node_13_save'''
                            data_ad_node13_i = (r[1],r[2],res_ad_node7_i_list[0][1])
                            res_ad_node13_i_save = run_sql(conn_src, sql_cmd_ad_node13_i_save, data_ad_node13_i)
                            ###############################################     Node 14
                            print('Node 14 is not needed in python skript [SOURCE DB]')
                        else:
                            print('NO -> next step: STOP process ETL -> [Report a mismatch in the attribute type found between the source and target database.]')
                            sys.exit(1)
                            # print('NO -> next step: Node 9')
                            # ###############################################     Node 9
                            # print('Node 9 - Using labels and descriptions, manually search and assign the area domain (respectively etl_id) [TARGET DB] -> next step: Node 10')
                            # print('List of area domains [TARGET DB]:')
                            # for row in res_ad_node5_i_list:
                            #     print('CYCLE_ID: %s   ETL_ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s   ATOMIC: %s' % (row[0], row[1], row[2], row[3], row[4], row[5], row[6]))
                            # ###############################################     Node 10
                            # while True:
                            #     node10_ad = etl_input("Node 10 - Exists etl_id? (y/n):\n")
                            #     if not (node10_ad == 'y' or node10_ad == 'n'):
                            #         print("Sorry, not known.")
                            #         continue
                            #     else:
                            #         break

                            # if node10_ad == 'y':
                            #     print('YES -> next step: Node 13')
                            #     ###############################################     Node 13
                            #     print('Node 13 - save area domain [SOURCE DB] -> next step: Node 14')
                            #     sql_cmd_ad_node13_i_save = '''select target_data.fn_etl_save_area_domain(%s,%s,%s) as res_ad_node_13_save'''
                            #     node13_ad_save = etl_input("Set ETL_ID of area domain [choosing from TARGET DB and saving into SOURCE DB]:\n")
                            #     data_ad_node13_i = (r[1],r[2],node13_ad_save)
                            #     res_ad_node13_i_save = run_sql(conn_src, sql_cmd_ad_node13_i_save, data_ad_node13_i)
                            #     ###############################################     Node 14
                            #     print('Node 14 is not needed in python skript [SOURCE DB]')
                            # elif node10_ad == 'n':
                            #     print('NO -> next step: Node 11')
                            #     ###############################################     Node 11
                            #     print('Node 11 - import area domain [TARGET DB] -> next step: Node 13')
                            #     #sql_cmd_ad_node11_i_import = '''select etl_id as res_ad_node_11_import from nfiesta_test.fn_etl_import_area_domain(%s,%s,%s,%s,%s);'''
                            #     sql_cmd_ad_node11_i_import = sql.SQL('''select etl_id as res_ad_node_11_import from {}.fn_etl_import_area_domain(%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                            #     data_ad_node11_i = (r[0],r[3],r[4],r[5],r[6],r[9])
                            #     res_ad_node11_i_import = run_sql(conn_dest, sql_cmd_ad_node11_i_import, data_ad_node11_i)[0]['res_ad_node_11_import']
                            #     ###############################################     Node 13
                            #     print('Node 13 - save area domain [SOURCE DB] -> next step: Node 14')
                            #     sql_cmd_ad_node13_i_save = '''select target_data.fn_etl_save_area_domain(%s,%s,%s) as res_ad_node_13_save'''
                            #     data_ad_node13_i = (r[1],r[2],res_ad_node11_i_import)
                            #     res_ad_node13_i_save = run_sql(conn_src, sql_cmd_ad_node13_i_save, data_ad_node13_i)
                            #     ###############################################     Node 14
                            #     print('Node 14 is not needed in python skript [SOURCE DB]')

            #---- end cycle for area domains -------#
            print('END cycle for area domains where etl_id is null -> next step: ETL area domain category')
            print('--------------------------------------------------------------')
        else:
            ###############################################     Node 4
            print('Node 4 - Is etl_id not null for all records -> YES -> next step: ETL area domain category')
            print('--------------------------------------------------------------')

        #-------------------------#
        # ETL area domain category
        #-------------------------#
        ###############################################     Node 15
        print('Node 15 - check area domain category [SOURCE DB] -> next step: Node 16')
        # sql_cmds ADC
        sql_cmd_adc_list = '''select * from target_data.fn_etl_check_area_domain_category(%s,%s);'''
        sql_cmd_adc_check_filled = '''select count(*) as res_adc_node_17_check_filled from target_data.fn_etl_check_area_domain_category(%s,%s) where etl_id is null;''' # zero rows => it means that etl_id is filled for all rows
        sql_cmd_adc_list_cycle = '''select * from target_data.fn_etl_check_area_domain_category(%s,%s) where etl_id is null;'''
        sql_cmd_adc_etl_id = '''select array_agg(etl_id) as res_adc_node_15_etl_id from target_data.fn_etl_check_area_domain_category(%s,%s) where etl_id is not null;'''
        ###############################################     Node 16
        print('Node 16 - Display of selected columns from Node 15 [SOURCE DB] -> next step: Node 17')
        res_area_domain_category_node15_list = run_sql(conn_src, sql_cmd_adc_list, data_id_t_etl_tv) # TABLE from NODE 15
        for row in res_area_domain_category_node15_list:
            print('ID: %s   LABEL_AD: %s    LABEL: %s   ETL_ID: %s' % (row[0], row[4], row[8], row[10]))

        res_area_domain_category_node17_check_filled = run_sql(conn_src, sql_cmd_adc_check_filled, data_id_t_etl_tv)[0]['res_adc_node_17_check_filled']
        if res_area_domain_category_node17_check_filled > 0:
            ###############################################     Node 17
            print('Node 17 - Is etl_id not null for all records? -> NO -> next step: Node 18')
            res_area_domain_category_node17_list_cycle = run_sql(conn_src, sql_cmd_adc_list_cycle, data_id_t_etl_tv) # LIST for cycle
            print('List of area domain categories where etl_id is null [SOURCE DB]:')
            for row in res_area_domain_category_node17_list_cycle:
                print('ID: %s   LABEL_AD: %s    LABEL: %s   ETL_ID: %s' % (row[0], row[4], row[8], row[10]))

            #---- cycle for area domain categories -------#
            print('BEGIN cycle for area domain categories where etl_id is null')
            for r in res_area_domain_category_node17_list_cycle:
                ###############################################     Node 18
                print('--------------')
                if national_language == 'en':
                    print('!!! CYCLE for area domain: "%s" and area domain category: "%s"' % (r[4], r[8]))
                else:
                    print('!!! CYCLE for area domain: "%s" and area domain category: "%s"' % (r[3], r[6]))
                print('--------------')
                print('Node 18 - get area domain categories [TARGET DB] -> next step: Node 19')
                res_adc_node15_i_etl_id = run_sql(conn_src, sql_cmd_adc_etl_id, data_id_t_etl_tv)[0]['res_adc_node_15_etl_id'] #zjisteni seznamu etl_id pro except !!!

                if res_adc_node15_i_etl_id == None:
                    etl_id_node18_i = [0]
                else:
                    etl_id_node18_i = res_adc_node15_i_etl_id

                data_adc_node18_i = (r[0],r[2],etl_id_node18_i)
                #sql_cmd_adc_node18_i_check_records = '''select count(*) as res_adc_node_18_check_records from nfiesta_test.fn_etl_get_area_domain_categories(%s,%s,%s);'''
                sql_cmd_adc_node18_i_check_records = sql.SQL('''select count(*) as res_adc_node_18_check_records from {}.fn_etl_get_area_domain_categories(%s,%s,%s);''').format(sql.Identifier(schemaname))
                #sql_cmd_adc_node18_i_list = '''select * from nfiesta_test.fn_etl_get_area_domain_categories(%s,%s,%s);'''
                sql_cmd_adc_node18_i_list = sql.SQL('''select * from {}.fn_etl_get_area_domain_categories(%s,%s,%s);''').format(sql.Identifier(schemaname))

                res_adc_node18_i_check_records = run_sql(conn_dest, sql_cmd_adc_node18_i_check_records, data_adc_node18_i)[0]['res_adc_node_18_check_records']
                res_adc_node18_i_list = run_sql(conn_dest, sql_cmd_adc_node18_i_list, data_adc_node18_i)

                if res_adc_node18_i_check_records == 0:
                    ###############################################     Node 19
                    print('Node 19 - Any record returned? -> NO -> next step: Node 24')
                    ###############################################     Node 24
                    print('Node 24 - import area domain category [TARGET DB] -> next step: Node 26')
                    #sql_cmd_adc_node24_i_import = '''select etl_id as res_adc_node_24_import from nfiesta_test.fn_etl_import_area_domain_category(%s,%s,%s,%s,%s,%s);'''
                    sql_cmd_adc_node24_i_import = sql.SQL('''select etl_id as res_adc_node_24_import from {}.fn_etl_import_area_domain_category(%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                    data_adc_node24_i = (r[0],r[2],r[6],r[7],r[8],r[9])
                    res_adc_node24_i_import = run_sql(conn_dest, sql_cmd_adc_node24_i_import, data_adc_node24_i)[0]['res_adc_node_24_import']
                    ###############################################     Node 26
                    print('Node 26 - save area domain category [SOURCE DB] -> next step: Node 27')
                    sql_cmd_adc_node26_i_save = '''select target_data.fn_etl_save_area_domain_category(%s,%s,%s) as res_adc_node_26_save'''
                    data_adc_node26_i = (r[5],res_adc_node24_i_import,r[12])
                    res_adc_node26_i_save = run_sql(conn_src, sql_cmd_adc_node26_i_save, data_adc_node26_i)
                    ###############################################     Node 27
                    print('Node 27 is not needed in python skript [SOURCE DB]')
                else:
                    ###############################################     Node 19
                    print('Node 19 - Any record returned? -> YES -> next step: Node 20')
                    ###############################################     Node 20
                    print('Node 20 - check area domain category [TARGET DB] -> next step: Node 21')
                    #sql_cmd_adc_node20_i_check_records = '''select count(*) as res_adc_node_20_check_records from nfiesta_test.fn_etl_check_area_domain_category(%s,%s,%s) where etl_id is not null;'''
                    sql_cmd_adc_node20_i_check_records = sql.SQL('''select count(*) as res_adc_node_20_check_records from {}.fn_etl_check_area_domain_category(%s,%s,%s) where etl_id is not null;''').format(sql.Identifier(schemaname))
                    #sql_cmd_adc_node20_i_list = '''select * from nfiesta_test.fn_etl_check_area_domain_category(%s,%s,%s);'''
                    sql_cmd_adc_node20_i_list = sql.SQL('''select * from {}.fn_etl_check_area_domain_category(%s,%s,%s);''').format(sql.Identifier(schemaname))
                    #print(r[0])
                    #print(r[2])
                    #print(r[8])
                    data_adc_node20_i = (r[0],r[2],r[8])
                    res_adc_node20_i_check_records = run_sql(conn_dest, sql_cmd_adc_node20_i_check_records, data_adc_node20_i)
                    res_adc_node20_i_list = run_sql(conn_dest, sql_cmd_adc_node20_i_list, data_adc_node20_i)

                    if res_adc_node20_i_check_records[0]['res_adc_node_20_check_records'] == 0:
                        ###############################################     Node 21
                        print('Node 21 - Exists etl_id? -> NO -> next step: Node 22')
                        ###############################################     Node 22
                        print('Node 22 - Using labels and descriptions, manually search and assign the area domain category (respectively etl_id) [TARGET DB] -> next step: Node 23')
                        print('List of area domain categories [TARGET DB]:')
                        for row in res_adc_node18_i_list:
                            print('CYCLE_ID: %s   ETL_ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (row[0], row[1], row[2], row[3], row[4], row[5]))
                        ###############################################     Node 23
                        while True:
                            node23_adc = etl_input("Node 23 - Exists etl_id? (y/n):\n")
                            if not (node23_adc == 'y' or node23_adc == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if node23_adc == 'y':
                            print('YES -> next step: Node 26')
                            ###############################################     Node 26
                            print('Node 26 - save area domain category [SOURCE DB] -> next step: Node 27')
                            sql_cmd_adc_node26_i_save = '''select target_data.fn_etl_save_area_domain_category(%s,%s,%s) as res_adc_node_26_save'''
                            node26_adc_save = etl_input("Set ETL_ID of area domain category [choosing from TARGET DB and saving into SOURCE DB]:\n")
                            data_adc_node26_i = (r[5],node26_adc_save,r[12])
                            res_adc_node26_i_save = run_sql(conn_src, sql_cmd_adc_node26_i_save, data_adc_node26_i)
                            ###############################################     Node 27
                            print('Node 27 is not needed in python skript [SOURCE DB]')
                        elif node23_adc == 'n':
                            print('NO -> next step: Node 24')
                            ###############################################     Node 24
                            print('Node 24 - import area domain category [TARGET DB] -> next step: Node 26')
                            #sql_cmd_adc_node24_i_import = '''select etl_id as res_adc_node_24_import from nfiesta_test.fn_etl_import_area_domain_category(%s,%s,%s,%s,%s,%s);'''
                            sql_cmd_adc_node24_i_import = sql.SQL('''select etl_id as res_adc_node_24_import from {}.fn_etl_import_area_domain_category(%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                            data_adc_node24_i = (r[0],r[2],r[6],r[7],r[8],r[9])
                            res_adc_node24_i_import = run_sql(conn_dest, sql_cmd_adc_node24_i_import, data_adc_node24_i)[0]['res_adc_node_24_import']
                            ###############################################     Node 26
                            print('Node 26 - save area domain category [SOURCE DB] -> next step: Node 27')
                            sql_cmd_adc_node26_i_save = '''select target_data.fn_etl_save_area_domain_category(%s,%s,%s) as res_adc_node_26_save'''
                            data_adc_node26_i = (r[5],res_adc_node24_i_import,r[12])
                            res_adc_node26_i_save = run_sql(conn_src, sql_cmd_adc_node26_i_save, data_adc_node26_i)
                            ###############################################     Node 27
                            print('Node 27 is not needed in python skript [SOURCE DB]')
                    else:
                        ###############################################     Node 21
                        print('Node 21 - Exists etl_id? -> YES -> next step: Node 25')
                        ###############################################     Node 25
                        print('Information label and description of area domain category [TARGET DB]:')
                        for row in res_adc_node20_i_list:
                            print('CYCLE_ID: %s   ETL_ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (row[0], row[1], row[2], row[3], row[4], row[5]))

                        while True:
                            node25_adc = etl_input("Node 25 - Is etl_id correct? (y/n):\n")
                            if not (node25_adc == 'y' or node25_adc == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if node25_adc == 'y':
                            print('YES -> next step: Node 26')
                            ###############################################     Node 26
                            print('Node 26 - save area domain category [SOURCE DB] -> next step: Node 27')
                            sql_cmd_adc_node26_i_save = '''select target_data.fn_etl_save_area_domain_category(%s,%s,%s) as res_adc_node_26_save'''
                            data_adc_node26_i = (r[5],res_adc_node20_i_list[0][1],r[12])
                            res_adc_node26_i_save = run_sql(conn_src, sql_cmd_adc_node26_i_save, data_adc_node26_i)
                            ###############################################     Node 27
                            print('Node 27 is not needed in python skript [SOURCE DB]')
                        else:
                            print('NO -> next step: STOP process ETL -> [Report a mismatch in the attribute category found between the source and target database.]')
                            sys.exit(1)
                            # print('NO -> next step: Node 22')
                            # ###############################################     Node 22
                            # print('Node 22 - Using labels and descriptions, manually search and assign the area domain category (respectively etl_id) [TARGET DB] -> next step: Node 23')
                            # print('List of area domain categories [TARGET DB]:')
                            # for row in res_adc_node18_i_list:
                            #     print('CYCLE_ID: %s   ETL_ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (row[0], row[1], row[2], row[3], row[4], row[5]))
                            # ###############################################     Node 23
                            # while True:
                            #     node23_adc = etl_input("Node 23 - Exists etl_id? (y/n):\n")
                            #     if not (node23_adc == 'y' or node23_adc == 'n'):
                            #         print("Sorry, not known.")
                            #         continue
                            #     else:
                            #         break

                            # if node23_adc == 'y':
                            #     print('YES -> next step: Node 26')
                            #     ###############################################     Node 26
                            #     print('Node 26 - save area domain category [SOURCE DB] -> next step: Node 27')
                            #     sql_cmd_adc_node26_i_save = '''select target_data.fn_etl_save_area_domain_category(%s,%s,%s) as res_adc_node_26_save'''
                            #     node26_adc_save = etl_input("Set ETL_ID of area domain [choosing from TARGET DB and saving into SOURCE DB]:\n")
                            #     data_adc_node26_i = (r[5],node26_adc_save,r[12])
                            #     res_adc_node26_i_save = run_sql(conn_src, sql_cmd_adc_node26_i_save, data_adc_node26_i)
                            #     ###############################################     Node 27
                            #     print('Node 27 is not needed in python skript [SOURCE DB]')
                            # elif node23_adc == 'n':
                            #     print('NO -> next step: Node 24')
                            #     ###############################################     Node 24
                            #     print('Node 24 - import area domain category [TARGET DB] -> next step: Node 26')
                            #     #sql_cmd_adc_node24_i_import = '''select etl_id as res_adc_node_24_import from nfiesta_test.fn_etl_import_area_domain_category(%s,%s,%s,%s,%s,%s);'''
                            #     sql_cmd_adc_node24_i_import = sql.SQL('''select etl_id as res_adc_node_24_import from {}.fn_etl_import_area_domain_category(%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                            #     data_adc_node24_i = (r[0],r[2],r[6],r[7],r[8],r[9])
                            #     res_adc_node24_i_import = run_sql(conn_dest, sql_cmd_adc_node24_i_import, data_adc_node24_i)[0]['res_adc_node_24_import']
                            #     ###############################################     Node 26
                            #     print('Node 26 - save area domain category [SOURCE DB] -> next step: Node 27')
                            #     sql_cmd_adc_node26_i_save = '''select target_data.fn_etl_save_area_domain_category(%s,%s,%s) as res_adc_node_26_save'''
                            #     data_adc_node26_i = (r[5],res_adc_node24_i_import,r[12])
                            #     res_adc_node26_i_save = run_sql(conn_src, sql_cmd_adc_node26_i_save, data_adc_node26_i)
                            #     ###############################################     Node 27
                            #     print('Node 27 is not needed in python skript [SOURCE DB]')

            #---- end cycle for area domain categories -------#
            print('END cycle for area domain categories where etl_id is null -> next step: ETL sub population')
            print('--------------------------------------------------------------')
        else:
            ###############################################     Node 17
            print('Node 17 - Is etl_id not null for all records -> YES -> next step: ETL sub population')
            print('--------------------------------------------------------------')
    else:
        print('Node 2 - Any record returned? -> NO -> next step: ETL - Sub population and Sub population category')
    ###############################################################################
    ###############################################################################



    ###############################################################################
    ###############################################################################
    #####################################################     Sub population and Sub population category
    print('ETL - Sub population and Sub population category')
    print('--------------------------------------------------------------')
    ###############################################     Node 1
    print('Node 1 - check sub population [SOURCE DB]')

    # sql_cmds SP
    sql_cmd_sp_check_records = '''select count(*) as res_sp_node_1_check_records from target_data.fn_etl_check_sub_population(%s,%s);'''
    sql_cmd_sp_list = '''select * from target_data.fn_etl_check_sub_population(%s,%s);'''
    sql_cmd_sp_check_filled = '''select count(*) as res_sp_node_4_check_filled from target_data.fn_etl_check_sub_population(%s,%s) where etl_id is null;''' # zero rows => it means that etl_id is filled for all rows
    sql_cmd_sp_list_cycle = '''select * from target_data.fn_etl_check_sub_population(%s,%s) where etl_id is null;'''
    sql_cmd_sp_etl_id = '''select array_agg(etl_id) as res_sp_node_1_etl_id from target_data.fn_etl_check_sub_population(%s,%s) where etl_id is not null;'''

    if _variables == 1:
        data_id_t_etl_tv = (lst,res_target_variable_node3)
    else:
        data_id_t_etl_tv = (lst,res_target_variable_node14)

    res_sub_population_node1_check_records = run_sql(conn_src, sql_cmd_sp_check_records, data_id_t_etl_tv)[0]['res_sp_node_1_check_records']

    if res_sub_population_node1_check_records > 0:
        ###############################################     Node 2
        print('Node 2 - Any record returned? -> YES -> next step: Node 3')
        ###############################################     Node 3
        print('Node 3 - Display of selected columns from Node 1 [SOURCE DB] -> next step: Node 4')
        res_sub_population_node1_list = run_sql(conn_src, sql_cmd_sp_list, data_id_t_etl_tv) # TABLE from NODE 1
        for row in res_sub_population_node1_list:
            print('ID: %s   LABEL: %s   ETL_ID: %s' % (row[0], row[5], row[7]))

        res_sub_population_node4_check_filled = run_sql(conn_src, sql_cmd_sp_check_filled, data_id_t_etl_tv)[0]['res_sp_node_4_check_filled']
        if res_sub_population_node4_check_filled > 0:
            ###############################################     Node 4
            print('Node 4 - Is etl_id not null for all records [SOURCE DB]? -> NO -> next step: Node 5')
            res_sub_population_node4_list_cycle = run_sql(conn_src, sql_cmd_sp_list_cycle, data_id_t_etl_tv) # LIST for cycle
            print('List of sub populations where etl_id is null [SOURCE DB]:')
            for row in res_sub_population_node4_list_cycle:
                print('ID: %s   LABEL: %s   ETL_ID: %s' % (row[0], row[5], row[7]))

            #---- cycle for sub populations -------#
            print('BEGIN cycle for sub populations where etl_id is null [SOURCE DB]')
            for r in res_sub_population_node4_list_cycle:
                ###############################################     Node 5
                print('--------------')
                if national_language == 'en':
                    print('!!! CYCLE for sub population: "%s"' % (r[5]))
                else:
                    print('!!! CYCLE for sub population: "%s"' % (r[3]))
                print('--------------')
                print('Node 5 - get sub populations [TARGET DB] -> next step: Node 6')
                #res_sp_node1_i_etl_id = run_sql(conn_src, sql_cmd_sp_etl_id, data_id_t_etl_tv)[0]['res_sp_node_1_etl_id'] #zjisteni seznamu etl_id pro except !!!
                #preparing ETL_IDS for node 5
                sql_cmd_get_etl_ids_of_sub_population = '''select * from target_data.fn_etl_get_etl_ids_of_sub_population(%s);'''
                data_etl_ids_of_sub_population = (node7_conn,)
                res_etl_ids_of_sub_population = run_sql(conn_src,sql_cmd_get_etl_ids_of_sub_population,data_etl_ids_of_sub_population)[0][0]
                #print(res_etl_ids_of_sub_population)

                if res_etl_ids_of_sub_population == None:
                    etl_id_node5_i = [0]
                else:
                    etl_id_node5_i = res_etl_ids_of_sub_population

                data_sp_node5_i = (r[0],etl_id_node5_i)
                #sql_cmd_sp_node5_i_check_records = '''select count(*) as res_sp_node_5_check_records from nfiesta_test.fn_etl_get_sub_populations(%s,%s);'''
                sql_cmd_sp_node5_i_check_records = sql.SQL('''select count(*) as res_sp_node_5_check_records from {}.fn_etl_get_sub_populations(%s,%s);''').format(sql.Identifier(schemaname))
                #sql_cmd_sp_node5_i_list = '''select * from nfiesta_test.fn_etl_get_sub_populations(%s,%s);'''
                sql_cmd_sp_node5_i_list = sql.SQL('''select * from {}.fn_etl_get_sub_populations(%s,%s);''').format(sql.Identifier(schemaname))

                res_sp_node5_i_check_records = run_sql(conn_dest, sql_cmd_sp_node5_i_check_records, data_sp_node5_i)[0]['res_sp_node_5_check_records']
                res_sp_node5_i_list = run_sql(conn_dest, sql_cmd_sp_node5_i_list, data_sp_node5_i)

                if res_sp_node5_i_check_records == 0:
                    ###############################################     Node 6
                    print('Node 6 - Any record returned? -> NO -> next step: Node 11')
                    ###############################################     Node 11
                    print('Node 11 - import sub population [TARGET DB] -> next step: Node 13')
                    #sql_cmd_sp_node11_i_import = '''select etl_id as res_sp_node_11_import from nfiesta_test.fn_etl_import_sub_population(%s,%s,%s,%s,%s);'''
                    sql_cmd_sp_node11_i_import = sql.SQL('''select etl_id as res_sp_node_11_import from {}.fn_etl_import_sub_population(%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                    data_sp_node11_i = (r[0],r[3],r[4],r[5],r[6],r[9])
                    res_sp_node11_i_import = run_sql(conn_dest, sql_cmd_sp_node11_i_import, data_sp_node11_i)[0]['res_sp_node_11_import']
                    ###############################################     Node 13
                    print('Node 13 - save sub population [SOURCE DB] -> next step: Node 14')
                    sql_cmd_sp_node13_i_save = '''select target_data.fn_etl_save_sub_population(%s,%s,%s) as res_sp_node_13_save'''
                    data_sp_node13_i = (r[1],r[2],res_sp_node11_i_import)
                    res_sp_node13_i_save = run_sql(conn_src, sql_cmd_sp_node13_i_save, data_sp_node13_i)
                    ###############################################     Node 14
                    print('Node 14 is not needed in python skript [SOURCE DB]')
                else:
                    ###############################################     Node 6
                    print('Node 6 - Any record returned? -> YES -> next step: Node 7')
                    ###############################################     Node 7
                    print('Node 7 - check sub population [TARGET DB] -> next step: Node 8')
                    #sql_cmd_sp_node7_i_check_records = '''select count(*) as res_sp_node_7_check_records from nfiesta_test.fn_etl_check_sub_population(%s,%s) where etl_id is not null;'''
                    sql_cmd_sp_node7_i_check_records = sql.SQL('''select count(*) as res_sp_node_7_check_records from {}.fn_etl_check_sub_population(%s,%s) where etl_id is not null;''').format(sql.Identifier(schemaname))
                    #sql_cmd_sp_node7_i_list = '''select * from nfiesta_test.fn_etl_check_sub_population(%s,%s);'''
                    sql_cmd_sp_node7_i_list = sql.SQL('''select * from {}.fn_etl_check_sub_population(%s,%s);''').format(sql.Identifier(schemaname))
                    #print(r[0])
                    #print(r[5])
                    data_sp_node7_i = (r[0],r[5])
                    res_sp_node7_i_check_records = run_sql(conn_dest, sql_cmd_sp_node7_i_check_records, data_sp_node7_i)
                    res_sp_node7_i_list = run_sql(conn_dest, sql_cmd_sp_node7_i_list, data_sp_node7_i)

                    if res_sp_node7_i_check_records[0]['res_sp_node_7_check_records'] == 0:
                        ###############################################     Node 8
                        print('Node 8 - Exists etl_id? -> NO -> next step: Node 9')
                        ###############################################     Node 9
                        print('Node 9 - Using labels and descriptions, manually search and assign the sub population (respectively etl_id) [TARGET DB] -> next step: Node 10')
                        print('List of sub populations [TARGET DB]:')
                        for row in res_sp_node5_i_list:
                            print('CYCLE_ID: %s   ETL_ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s   ATOMIC: %s' % (row[0], row[1], row[2], row[3], row[4], row[5], row[6]))
                        ###############################################     Node 10
                        while True:
                            node10_sp = etl_input("Node 10 - Exists etl_id? (y/n):\n")
                            if not (node10_sp == 'y' or node10_sp == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if node10_sp == 'y':
                            print('YES -> next step: Node 13')
                            ###############################################     Node 13
                            print('Node 13 - save sub population [SOURCE DB] -> next step: Node 14')
                            sql_cmd_sp_node13_i_save = '''select target_data.fn_etl_save_sub_population(%s,%s,%s) as res_sp_node_13_save'''
                            node13_sp_save = etl_input("Set ETL_ID of sub population [choosing from TARGET DB and saving into SOURCE DB]:\n")
                            data_sp_node13_i = (r[1],r[2],node13_sp_save)
                            res_sp_node13_i_save = run_sql(conn_src, sql_cmd_sp_node13_i_save, data_sp_node13_i)
                            ###############################################     Node 14
                            print('Node 14 is not needed in python skript [SOURCE DB]')
                        elif node10_sp == 'n':
                            print('NO -> next step: Node 11')
                            ###############################################     Node 11
                            print('Node 11 - import sub population [TARGET DB] -> next step: Node 13')
                            #sql_cmd_sp_node11_i_import = '''select etl_id as res_sp_node_11_import from nfiesta_test.fn_etl_import_sub_population(%s,%s,%s,%s,%s);'''
                            sql_cmd_sp_node11_i_import = sql.SQL('''select etl_id as res_sp_node_11_import from {}.fn_etl_import_sub_population(%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                            data_sp_node11_i = (r[0],r[3],r[4],r[5],r[6],r[9])
                            res_sp_node11_i_import = run_sql(conn_dest, sql_cmd_sp_node11_i_import, data_sp_node11_i)[0]['res_sp_node_11_import']
                            ###############################################     Node 13
                            print('Node 13 - save sub population [SOURCE DB] -> next step: Node 14')
                            sql_cmd_sp_node13_i_save = '''select target_data.fn_etl_save_sub_population(%s,%s,%s) as res_sp_node_13_save'''
                            data_sp_node13_i = (r[1],r[2],res_sp_node11_i_import)
                            res_sp_node13_i_save = run_sql(conn_src, sql_cmd_sp_node13_i_save, data_sp_node13_i)
                            ###############################################     Node 14
                            print('Node 14 is not needed in python skript [SOURCE DB]')
                    else:
                        ###############################################     Node 8
                        print('Node 8 - Exists etl_id? -> YES -> next step: Node 12')
                        ###############################################     Node 12
                        print('Information label and description of sub population [TARGET DB]:')
                        for row in res_sp_node7_i_list:
                            print('CYCLE_ID: %s   ETL_ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s   ATOMIC: %s' % (row[0], row[1], row[2], row[3], row[4], row[5], row[6]))

                        while True:
                            node12_sp = etl_input("Node 12 - Is etl_id correct? (y/n):\n")
                            if not (node12_sp == 'y' or node12_sp == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if node12_sp == 'y':
                            print('YES -> next step: Node 13')
                            ###############################################     Node 13
                            print('Node 13 - save sub population [SOURCE DB] -> next step: Node 14')
                            sql_cmd_sp_node13_i_save = '''select target_data.fn_etl_save_sub_population(%s,%s,%s) as res_sp_node_13_save'''
                            data_sp_node13_i = (r[1],r[2],res_sp_node7_i_list[0][1])
                            res_sp_node13_i_save = run_sql(conn_src, sql_cmd_sp_node13_i_save, data_sp_node13_i)
                            ###############################################     Node 14
                            print('Node 14 is not needed in python skript [SOURCE DB]')
                        else:
                            print('NO -> next step: STOP process ETL -> [Report a mismatch in the attribute type found between the source and target database.]')
                            sys.exit(1)
                            # print('NO -> next step: Node 9')
                            # ###############################################     Node 9
                            # print('Node 9 - Using labels and descriptions, manually search and assign the sub population (respectively etl_id) [TARGET DB] -> next step: Node 10')
                            # print('List of sub populations [TARGET DB]:')
                            # for row in res_sp_node5_i_list:
                            #     print('CYCLE_ID: %s   ETL_ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s   ATOMIC: %s' % (row[0], row[1], row[2], row[3], row[4], row[5], row[6]))
                            # ###############################################     Node 10
                            # while True:
                            #     node10_sp = etl_input("Node 10 - Exists etl_id? (y/n):\n")
                            #     if not (node10_sp == 'y' or node10_sp == 'n'):
                            #         print("Sorry, not known.")
                            #         continue
                            #     else:
                            #         break

                            # if node10_sp == 'y':
                            #     print('YES -> next step: Node 13')
                            #     ###############################################     Node 13
                            #     print('Node 13 - save sub population [SOURCE DB] -> next step: Node 14')
                            #     sql_cmd_sp_node13_i_save = '''select target_data.fn_etl_save_sub_population(%s,%s,%s) as res_sp_node_13_save'''
                            #     node13_sp_save = etl_input("Set ETL_ID of sub population [choosing from TARGET DB and saving into SOURCE DB]:\n")
                            #     data_sp_node13_i = (r[1],r[2],node13_sp_save)
                            #     res_sp_node13_i_save = run_sql(conn_src, sql_cmd_sp_node13_i_save, data_sp_node13_i)
                            #     ###############################################     Node 14
                            #     print('Node 14 is not needed in python skript [SOURCE DB]')
                            # elif node10_sp == 'n':
                            #     print('NO -> next step: Node 11')
                            #     ###############################################     Node 11
                            #     print('Node 11 - import sub population [TARGET DB] -> next step: Node 13')
                            #     #sql_cmd_sp_node11_i_import = '''select etl_id as res_sp_node_11_import from nfiesta_test.fn_etl_import_sub_population(%s,%s,%s,%s,%s);'''
                            #     sql_cmd_sp_node11_i_import = sql.SQL('''select etl_id as res_sp_node_11_import from {}.fn_etl_import_sub_population(%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                            #     data_sp_node11_i = (r[0],r[3],r[4],r[5],r[6],r[9])
                            #     res_sp_node11_i_import = run_sql(conn_dest, sql_cmd_sp_node11_i_import, data_sp_node11_i)[0]['res_sp_node_11_import']
                            #     ###############################################     Node 13
                            #     print('Node 13 - save sub population [SOURCE DB] -> next step: Node 14')
                            #     sql_cmd_sp_node13_i_save = '''select target_data.fn_etl_save_sub_population(%s,%s,%s) as res_sp_node_13_save'''
                            #     data_sp_node13_i = (r[1],r[2],res_sp_node11_i_import)
                            #     res_sp_node13_i_save = run_sql(conn_src, sql_cmd_sp_node13_i_save, data_sp_node13_i)
                            #     ###############################################     Node 14
                            #     print('Node 14 is not needed in python skript [SOURCE DB]')

            #---- end cycle for sub populations -------#
            print('END cycle for sub populations where etl_id is null -> next step: ETL sub population category')
            print('--------------------------------------------------------------')
        else:
            ###############################################     Node 4
            print('Node 4 - Is etl_id not null for all records -> YES -> next step: ETL sub population category')
            print('--------------------------------------------------------------')

        #-------------------------#
        # ETL sub population category
        #-------------------------#
        ###############################################     Node 15
        print('Node 15 - check sub population category [SOURCE DB] -> next step: Node 16')
        # sql_cmds SPC
        sql_cmd_spc_list = '''select * from target_data.fn_etl_check_sub_population_category(%s,%s);'''
        sql_cmd_spc_check_filled = '''select count(*) as res_spc_node_17_check_filled from target_data.fn_etl_check_sub_population_category(%s,%s) where etl_id is null;''' # zero rows => it means that etl_id is filled for all rows
        sql_cmd_spc_list_cycle = '''select * from target_data.fn_etl_check_sub_population_category(%s,%s) where etl_id is null;'''
        sql_cmd_spc_etl_id = '''select array_agg(etl_id) as res_spc_node_15_etl_id from target_data.fn_etl_check_sub_population_category(%s,%s) where etl_id is not null;'''
        ###############################################     Node 16
        print('Node 16 - Display of selected columns from Node 15 [SOURCE DB] -> next step: Node 17')
        res_sub_population_category_node15_list = run_sql(conn_src, sql_cmd_spc_list, data_id_t_etl_tv) # TABLE from NODE 15
        for row in res_sub_population_category_node15_list:
            print('ID: %s   LABEL_AD: %s    LABEL: %s   ETL_ID: %s' % (row[0], row[4], row[8], row[10]))

        res_sub_population_category_node17_check_filled = run_sql(conn_src, sql_cmd_spc_check_filled, data_id_t_etl_tv)[0]['res_spc_node_17_check_filled']
        if res_sub_population_category_node17_check_filled > 0:
            ###############################################     Node 17
            print('Node 17 - Is etl_id not null for all records? -> NO -> next step: Node 18')
            res_sub_population_category_node17_list_cycle = run_sql(conn_src, sql_cmd_spc_list_cycle, data_id_t_etl_tv) # LIST for cycle
            print('List of sub population categories where etl_id is null [SOURCE DB]:')
            for row in res_sub_population_category_node17_list_cycle:
                print('ID: %s   LABEL_AD: %s    LABEL: %s   ETL_ID: %s' % (row[0], row[4], row[8], row[10]))

            #---- cycle for sub population categories -------#
            print('BEGIN cycle for sub population categories where etl_id is null')
            for r in res_sub_population_category_node17_list_cycle:
                ###############################################     Node 18
                print('--------------')
                if national_language == 'en':
                    print('!!! CYCLE for sub population: "%s" and sub population category: "%s"' % (r[4], r[8]))
                else:
                    print('!!! CYCLE for sub population: "%s" and sub population category: "%s"' % (r[3], r[6]))
                print('--------------')
                print('Node 18 - get sub population categories [TARGET DB] -> next step: Node 19')
                res_spc_node15_i_etl_id = run_sql(conn_src, sql_cmd_spc_etl_id, data_id_t_etl_tv)[0]['res_spc_node_15_etl_id'] #zjisteni seznamu etl_id pro except !!!

                if res_spc_node15_i_etl_id == None:
                    etl_id_node18_i = [0]
                else:
                    etl_id_node18_i = res_spc_node15_i_etl_id

                data_spc_node18_i = (r[0],r[2],etl_id_node18_i)
                #sql_cmd_spc_node18_i_check_records = '''select count(*) as res_spc_node_18_check_records from nfiesta_test.fn_etl_get_sub_population_categories(%s,%s,%s);'''
                sql_cmd_spc_node18_i_check_records = sql.SQL('''select count(*) as res_spc_node_18_check_records from {}.fn_etl_get_sub_population_categories(%s,%s,%s);''').format(sql.Identifier(schemaname))
                #sql_cmd_spc_node18_i_list = '''select * from nfiesta_test.fn_etl_get_sub_population_categories(%s,%s,%s);'''
                sql_cmd_spc_node18_i_list = sql.SQL('''select * from {}.fn_etl_get_sub_population_categories(%s,%s,%s);''').format(sql.Identifier(schemaname))

                res_spc_node18_i_check_records = run_sql(conn_dest, sql_cmd_spc_node18_i_check_records, data_spc_node18_i)[0]['res_spc_node_18_check_records']
                res_spc_node18_i_list = run_sql(conn_dest, sql_cmd_spc_node18_i_list, data_spc_node18_i)

                if res_spc_node18_i_check_records == 0:
                    ###############################################     Node 19
                    print('Node 19 - Any record returned? -> NO -> next step: Node 24')
                    ###############################################     Node 24
                    print('Node 24 - import sub population category [TARGET DB] -> next step: Node 26')
                    #sql_cmd_spc_node24_i_import = '''select etl_id as res_spc_node_24_import from nfiesta_test.fn_etl_import_sub_population_category(%s,%s,%s,%s,%s,%s);'''
                    sql_cmd_spc_node24_i_import = sql.SQL('''select etl_id as res_spc_node_24_import from {}.fn_etl_import_sub_population_category(%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                    data_spc_node24_i = (r[0],r[2],r[6],r[7],r[8],r[9])
                    res_spc_node24_i_import = run_sql(conn_dest, sql_cmd_spc_node24_i_import, data_spc_node24_i)[0]['res_spc_node_24_import']
                    ###############################################     Node 26
                    print('Node 26 - save sub population category [SOURCE DB] -> next step: Node 27')
                    sql_cmd_spc_node26_i_save = '''select target_data.fn_etl_save_sub_population_category(%s,%s,%s) as res_spc_node_26_save'''
                    data_spc_node26_i = (r[5],res_spc_node24_i_import,r[12])
                    res_spc_node26_i_save = run_sql(conn_src, sql_cmd_spc_node26_i_save, data_spc_node26_i)
                    ###############################################     Node 27
                    print('Node 27 is not needed in python skript [SOURCE DB]')
                else:
                    ###############################################     Node 19
                    print('Node 19 - Any record returned? -> YES -> next step: Node 20')
                    ###############################################     Node 20
                    print('Node 20 - check sub population category [TARGET DB] -> next step: Node 21')
                    #sql_cmd_spc_node20_i_check_records = '''select count(*) as res_spc_node_20_check_records from nfiesta_test.fn_etl_check_sub_population_category(%s,%s,%s) where etl_id is not null;'''
                    sql_cmd_spc_node20_i_check_records = sql.SQL('''select count(*) as res_spc_node_20_check_records from {}.fn_etl_check_sub_population_category(%s,%s,%s) where etl_id is not null;''').format(sql.Identifier(schemaname))
                    #sql_cmd_spc_node20_i_list = '''select * from nfiesta_test.fn_etl_check_sub_population_category(%s,%s,%s);'''
                    sql_cmd_spc_node20_i_list = sql.SQL('''select * from {}.fn_etl_check_sub_population_category(%s,%s,%s);''').format(sql.Identifier(schemaname))
                    #print(r[0])
                    #print(r[2])
                    #print(r[8])
                    data_spc_node20_i = (r[0],r[2],r[8])
                    res_spc_node20_i_check_records = run_sql(conn_dest, sql_cmd_spc_node20_i_check_records, data_spc_node20_i)
                    res_spc_node20_i_list = run_sql(conn_dest, sql_cmd_spc_node20_i_list, data_spc_node20_i)

                    if res_spc_node20_i_check_records[0]['res_spc_node_20_check_records'] == 0:
                        ###############################################     Node 21
                        print('Node 21 - Exists etl_id? -> NO -> next step: Node 22')
                        ###############################################     Node 22
                        print('Node 22 - Using labels and descriptions, manually search and assign the sub population category (respectively etl_id) [TARGET DB] -> next step: Node 23')
                        print('List of sub population categories [TARGET DB]:')
                        for row in res_spc_node18_i_list:
                            print('CYCLE_ID: %s   ETL_ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (row[0], row[1], row[2], row[3], row[4], row[5]))
                        ###############################################     Node 23
                        while True:
                            node23_spc = etl_input("Node 23 - Exists etl_id? (y/n):\n")
                            if not (node23_spc == 'y' or node23_spc == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if node23_spc == 'y':
                            print('YES -> next step: Node 26')
                            ###############################################     Node 26
                            print('Node 26 - save sub population category [SOURCE DB] -> next step: Node 27')
                            sql_cmd_spc_node26_i_save = '''select target_data.fn_etl_save_sub_population_category(%s,%s,%s) as res_spc_node_26_save'''
                            node26_spc_save = etl_input("Set ETL_ID of sub population category [choosing from TARGET DB and saving into SOURCE DB]:\n")
                            data_spc_node26_i = (r[5],node26_spc_save,r[12])
                            res_spc_node26_i_save = run_sql(conn_src, sql_cmd_spc_node26_i_save, data_spc_node26_i)
                            ###############################################     Node 27
                            print('Node 27 is not needed in python skript [SOURCE DB]')
                        elif node23_spc == 'n':
                            print('NO -> next step: Node 24')
                            ###############################################     Node 24
                            print('Node 24 - import sub population category [TARGET DB] -> next step: Node 26')
                            #sql_cmd_spc_node24_i_import = '''select etl_id as res_spc_node_24_import from nfiesta_test.fn_etl_import_sub_population_category(%s,%s,%s,%s,%s,%s);'''
                            sql_cmd_spc_node24_i_import = sql.SQL('''select etl_id as res_spc_node_24_import from {}.fn_etl_import_sub_population_category(%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                            data_spc_node24_i = (r[0],r[2],r[6],r[7],r[8],r[9])
                            res_spc_node24_i_import = run_sql(conn_dest, sql_cmd_spc_node24_i_import, data_spc_node24_i)[0]['res_spc_node_24_import']
                            ###############################################     Node 26
                            print('Node 26 - save sub population category [SOURCE DB] -> next step: Node 27')
                            sql_cmd_spc_node26_i_save = '''select target_data.fn_etl_save_sub_population_category(%s,%s,%s) as res_spc_node_26_save'''
                            data_spc_node26_i = (r[5],res_spc_node24_i_import,r[12])
                            res_spc_node26_i_save = run_sql(conn_src, sql_cmd_spc_node26_i_save, data_spc_node26_i)
                            ###############################################     Node 27
                            print('Node 27 is not needed in python skript [SOURCE DB]')
                    else:
                        ###############################################     Node 21
                        print('Node 21 - Exists etl_id? -> YES -> next step: Node 25')
                        ###############################################     Node 25
                        print('Information label and description of sub population category [TARGET DB]:')
                        for row in res_spc_node20_i_list:
                            print('CYCLE_ID: %s   ETL_ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (row[0], row[1], row[2], row[3], row[4], row[5]))

                        while True:
                            node25_spc = etl_input("Node 25 - Is etl_id correct? (y/n):\n")
                            if not (node25_spc == 'y' or node25_spc == 'n'):
                                print("Sorry, not known.")
                                continue
                            else:
                                break

                        if node25_spc == 'y':
                            print('YES -> next step: Node 26')
                            ###############################################     Node 26
                            print('Node 26 - save sub population category [SOURCE DB] -> next step: Node 27')
                            sql_cmd_spc_node26_i_save = '''select target_data.fn_etl_save_sub_population_category(%s,%s,%s) as res_spc_node_26_save'''
                            data_spc_node26_i = (r[5],res_spc_node20_i_list[0][1],r[12])
                            res_spc_node26_i_save = run_sql(conn_src, sql_cmd_spc_node26_i_save, data_spc_node26_i)
                            ###############################################     Node 27
                            print('Node 27 is not needed in python skript [SOURCE DB]')
                        else:
                            print('NO -> next step: STOP process ETL -> [Report a mismatch in the attribute category found between the source and target database.]')
                            sys.exit(1)
                            # print('NO -> next step: Node 22')
                            # ###############################################     Node 22
                            # print('Node 22 - Using labels and descriptions, manually search and assign the sub population category (respectively etl_id) [TARGET DB] -> next step: Node 23')
                            # print('List of sub population categories [TARGET DB]:')
                            # for row in res_spc_node18_i_list:
                            #     print('CYCLE_ID: %s   ETL_ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (row[0], row[1], row[2], row[3], row[4], row[5]))
                            # ###############################################     Node 23
                            # while True:
                            #     node23_spc = etl_input("Node 23 - Exists etl_id? (y/n):\n")
                            #     if not (node23_spc == 'y' or node23_spc == 'n'):
                            #         print("Sorry, not known.")
                            #         continue
                            #     else:
                            #         break

                            # if node23_spc == 'y':
                            #     print('YES -> next step: Node 26')
                            #     ###############################################     Node 26
                            #     print('Node 26 - save sub population category [SOURCE DB] -> next step: Node 27')
                            #     sql_cmd_spc_node26_i_save = '''select target_data.fn_etl_save_sub_population_category(%s,%s,%s) as res_spc_node_26_save'''
                            #     node26_spc_save = etl_input("Set ETL_ID of sub population category [choosing from TARGET DB and saving into SOURCE DB]:\n")
                            #     data_spc_node26_i = (r[5],node26_spc_save,r[12])
                            #     res_spc_node26_i_save = run_sql(conn_src, sql_cmd_spc_node26_i_save, data_spc_node26_i)
                            #     ###############################################     Node 27
                            #     print('Node 27 is not needed in python skript [SOURCE DB]')
                            # elif node23_spc == 'n':
                            #     print('NO -> next step: Node 24')
                            #     ###############################################     Node 24
                            #     print('Node 24 - import sub population category [TARGET DB] -> next step: Node 26')
                            #     #sql_cmd_spc_node24_i_import = '''select etl_id as res_spc_node_24_import from nfiesta_test.fn_etl_import_sub_population_category(%s,%s,%s,%s,%s,%s);'''
                            #     sql_cmd_spc_node24_i_import = sql.SQL('''select etl_id as res_spc_node_24_import from {}.fn_etl_import_sub_population_category(%s,%s,%s,%s,%s,%s);''').format(sql.Identifier(schemaname))
                            #     data_spc_node24_i = (r[0],r[2],r[6],r[7],r[8],r[9])
                            #     res_spc_node24_i_import = run_sql(conn_dest, sql_cmd_spc_node24_i_import, data_spc_node24_i)[0]['res_spc_node_24_import']
                            #     ###############################################     Node 26
                            #     print('Node 26 - save sub population category [SOURCE DB] -> next step: Node 27')
                            #     sql_cmd_spc_node26_i_save = '''select target_data.fn_etl_save_sub_population_category(%s,%s,%s) as res_spc_node_26_save'''
                            #     data_spc_node26_i = (r[5],res_spc_node24_i_import,r[12])
                            #     res_spc_node26_i_save = run_sql(conn_src, sql_cmd_spc_node26_i_save, data_spc_node26_i)
                            #     ###############################################     Node 27
                            #     print('Node 27 is not needed in python skript [SOURCE DB]')

            #---- end cycle for sub population categories -------#
            print('END cycle for sub population categories where etl_id is null -> next step: ETL -  ADC and SPC MAPPING')
            print('--------------------------------------------------------------')
        else:
            ###############################################     Node 17
            print('Node 17 - Is etl_id not null for all records -> YES -> next step: ETL -  ADC and SPC MAPPING')
            print('--------------------------------------------------------------')
    else:
        print('Node 2 - Any record returned? -> NO -> next step: ETL -  ADC and SPC MAPPING')
        print('--------------------------------------------------------------')
    ###############################################################################
    ###############################################################################



    ###############################################################################
    #####################################################    ADC and SPC MAPPING
    print('ETL -  ADC and SPC MAPPING')
    print('--------------------------------------------------------------')
    ###############################################     Node 1
    print('Node 1 - ADC MAPPING - export area domain category_mapping [TARGET DB] -> next step: Node 2')
    sql_cmd_adc_mapping_node_1 = '''select target_data.fn_etl_export_area_domain_category_mapping(%s,%s) as res_adc_mapping_node_1;'''

    if _variables == 1:
        data_adc_mapping_node_1 = (lst,res_target_variable_node3)
    else:
        data_adc_mapping_node_1 = (lst,res_target_variable_node14)

    res_adc_mapping_node1 = run_sql(conn_src, sql_cmd_adc_mapping_node_1, data_adc_mapping_node_1)[0]['res_adc_mapping_node_1']
    #print(res_adc_mapping_node1)

    if res_adc_mapping_node1 == None:
        ###############################################     Node 2
        print('Node 2 - Any record returned? -> NO -> next step: Node 4')
    else:
        ###############################################     Node 2
        print('Node 2 - Any record returned? -> YES -> next step: Node 3')
        ###############################################     Node 3
        print('Node 3 - import area domain category mapping -> next step: Node 4')
        sql_cmd_adc_mapping_node_3 = sql.SQL('''select * from {}.fn_etl_import_area_domain_category_mapping(%s) as res_adc_mapping_node_3;''').format(sql.Identifier(schemaname))
        data_adc_mapping_node_3 = (json.dumps(res_adc_mapping_node1, ensure_ascii=False),)
        res_adc_mapping_node3 = run_sql(conn_dest, sql_cmd_adc_mapping_node_3, data_adc_mapping_node_3)[0]['res_adc_mapping_node_3']
        print(res_adc_mapping_node3)

    ###############################################     Node 4
    print('Node 4 - SPC MAPPING - export sub population category mapping [TARGET DB] -> next step: Node 5')
    sql_cmd_spc_mapping_node_4 = '''select target_data.fn_etl_export_sub_population_category_mapping(%s,%s) as res_spc_mapping_node_4;'''

    if _variables == 1:
        data_spc_mapping_node_4 = (lst,res_target_variable_node3)
    else:
        data_spc_mapping_node_4 = (lst,res_target_variable_node14)

    res_spc_mapping_node4 = run_sql(conn_src, sql_cmd_spc_mapping_node_4, data_spc_mapping_node_4)[0]['res_spc_mapping_node_4']
    #print(res_spc_mapping_node4)

    if res_spc_mapping_node4 == None:
        ###############################################     Node 5
        print('Node 5 - Any record returned? -> NO -> next step: Node D')
    else:
        ###############################################     Node 5
        print('Node 5 - Any record returned? -> YES -> next step: Node 6')
        ###############################################     Node 6
        print('Node 6 - import sub population category mapping -> next step: Node D')
        sql_cmd_spc_mapping_node_6 = sql.SQL('''select * from {}.fn_etl_import_sub_population_category_mapping(%s) as res_spc_mapping_node_6;''').format(sql.Identifier(schemaname))
        data_spc_mapping_node_6 = (json.dumps(res_spc_mapping_node4, ensure_ascii=False),)
        res_spc_mapping_node6 = run_sql(conn_dest, sql_cmd_spc_mapping_node_6, data_spc_mapping_node_6)[0]['res_spc_mapping_node_6']
        print(res_spc_mapping_node6)
    print('--------------------------------------------------------------')
    ###############################################################################
    ###############################################################################



    ##########################################################################################
    # Node D
    ##########################################################################################
    print('--------------------------------------------------------------')
    print('Node D - Is the choosen target variable a target variable with missing atomic ADC or SPC?')
    if _atomic_problem == 0:
        print('No - next step: ETL -  Attribute variables')
        print('--------------------------------------------------------------')
        ###############################################################################
        #####################################################    Attribute variables
        print('ETL -  Attribute variables')
        print('--------------------------------------------------------------')
        ###############################################     Node 1
        print('Node 1 - get variables [SOURCE DB] -> next step: Node 2')
        sql_cmd = '''select target_data.fn_etl_get_variables(%s,%s) as res_variables_node_1;
        '''
        if _variables == 1:
            data_id_t_etl_tv = (lst,res_target_variable_node3)
        else:
            data_id_t_etl_tv = (lst,res_target_variable_node14)

        res_variables_node1 = run_sql(conn_src, sql_cmd, data_id_t_etl_tv)[0]['res_variables_node_1']
        ###############################################     Node 2
        print('Node 2 - check variables [TARGET DB] -> next step: Node 3')
        #sql_cmd = '''select jsonb_pretty(fn_etl_check_variables::jsonb) as res_variables_node_4 from nfiesta_test.fn_etl_check_variables(%s);'''
        sql_cmd = sql.SQL('''select jsonb_pretty(fn_etl_check_variables::jsonb) as res_variables_node_4 from {}.fn_etl_check_variables(%s);''').format(sql.Identifier(schemaname))
        sql_cmd_json = sql.SQL('''select fn_etl_check_variables as res_variables_node_4_json from {}.fn_etl_check_variables(%s);''').format(sql.Identifier(schemaname))
        metadata = (json.dumps(res_variables_node1, ensure_ascii=False),)
        res_variables_node2 = run_sql(conn_dest, sql_cmd, metadata)[0]['res_variables_node_4']
        res_variables_node2_json = run_sql(conn_dest, sql_cmd_json, metadata)[0]['res_variables_node_4_json']
        #print('List of variables that missing for ETL on side [SOURCE DB] for target variable "%s":' % (res_target_variable_label_nl_en))
        #print(res_variables_node2) # None
        ###############################################     Node 3
        if res_variables_node2 == None:
            print('Node 3 - Any record returned? -> NO -> next step: ETL - Topic and mapping between target variable and topic')
        else:
            print('Node 3 - Any record returned? -> YES -> next step: Node 4')
            ###############################################     Node 4
            print('Node 4 - get_panel_refyearset_combinations_false [SOURCE DB] -> next step: Node 5')
            sql_cmd_variables_node4 = '''select refyearset2panel, panel, reference_year_set, useable4etl from target_data.fn_etl_get_panel_refyearset_combinations_false(%s,%s) order by refyearset2panel;'''
            data_variables_node4 = (json.dumps(res_variables_node2_json, ensure_ascii=False),lst)
            res_variables_node4 = run_sql(conn_src, sql_cmd_variables_node4, data_variables_node4)
            ###############################################     Node 5
            print('Node 5 - List of combinations of panels and reference year sets with information that given combination is or not useable for ETL. If given combination is not useable for ETL than next is display a list of attribute variables that are needed to implemented in SOURCE DB:')
            for r in res_variables_node4:
                print('----------------------------------')
                print('ID_RYS2P: %s PANEL: %s   REFERENCE_YEAR_SET: %s  USEABLE4ETL: %s' % (r['refyearset2panel'], r['panel'], r['reference_year_set'], r['useable4etl']))
                ###############################################
                ## If is by user selected combination of panel and reference year set (only for not useable
                ## combination) the GUI application calls fn_etl_get_variables_false(JSON, panel, reference_year_set,
                ## national_language) and display result (result is a list of attribute variables from result of node 4
                ## for given combination of panel and reference year set)
                sql_cmd_variables_node5 = '''select sub_population, sub_population_category, area_domain, area_domain_category from target_data.fn_etl_get_variables_false(%s,%s,%s,%s)'''
                data_variables_node5 = (json.dumps(res_variables_node2_json, ensure_ascii=False),r['panel'],r['reference_year_set'],national_language)
                res_variables_node5 = run_sql(conn_src, sql_cmd_variables_node5, data_variables_node5)
                #print('Node 5 - List of attribute variables that are needed to impleneted into SOURCE DB:')
                for rr in res_variables_node5:
                    print('ID_RYS2P: %s SP: %s  SPC: %s AD: %s  ADC: %s' % (r['refyearset2panel'], rr['sub_population'], rr['sub_population_category'], rr['area_domain'], rr['area_domain_category']))
                print('----------------------------------')
                ###############################################
            ###############################################     Node 6
            while True:
                node6_variables = etl_input("Node 6 - Continue with process ETL? (y/n):\n")
                if not (node6_variables == 'y' or node6_variables == 'n'):
                    print("Sorry, not known.")
                    continue
                else:
                    break

            if node6_variables == 'y':
                print('YES -> next step: Node 7')
                ###############################################     Node 7
                print('Node 7 - choose and set IDs [ID_RYS2P] of combinations of panels and referece year sets (choose from list in result of Node 5) that are only useable for ETL [SOURCE DB] -> next step: next step: ETL - Topic and mapping between target variable and topic')
                lst4check = []
                # number of elements as input
                n = int(input("Enter number of combinations and IDs [SOURCE DB]: "))
                # iterating till the range
                for i in range(0, n):
                    ele = int(input())
                    lst4check.append(ele) # adding the element
                print(lst4check)
                ###############################################
                # internal_check
                sql_cmd_variables_internal_check = '''select count(*) from target_data.fn_etl_get_panel_refyearset_combinations_false(%s,%s) where refyearset2panel in (select unnest(%s)) and useable4etl = false;'''
                data_variables_internal_check = (json.dumps(res_variables_node2_json, ensure_ascii=False),lst,lst4check)
                res_variables_internal_check = run_sql(conn_src, sql_cmd_variables_internal_check, data_variables_internal_check)[0][0]
                #print(res_variables_internal_check)
                if res_variables_internal_check == 0:
                    lst = lst4check
                else:
                    print('The one of the selected combinations of panel and reference year set is not useable for ETL process! Next step: STOP proces ETL !!!')
                    sys.exit(1)
                ###############################################

            elif node6_variables == 'n':
                print('NO -> next step: STOP proces ETL !!!')
                sys.exit(1)
        print('--------------------------------------------------------------')
        ###############################################################################
        ###############################################################################



        ###############################################################################
        ###############################################################################
        #####################################################    Topic and mapping target variable between topic
        print('ETL -  Topic and mapping target variable between topic')
        print('--------------------------------------------------------------')
        ###############################################     Node 1
        print('Node 1 - get topics [TARGET DB]')
        sql_cmd_topic_records = sql.SQL('''select count(*) as res_topic_node_1_records from {}.fn_etl_get_topics();''').format(sql.Identifier(schemaname))
        sql_cmd_topic_list = sql.SQL('''select * from {}.fn_etl_get_topics();''').format(sql.Identifier(schemaname))
        data_topic_node1 = (None, )
        res_topic_node1_records = run_sql(conn_dest, sql_cmd_topic_records, data_topic_node1)[0]['res_topic_node_1_records']
        res_topic_node1_list = run_sql(conn_dest, sql_cmd_topic_list, data_topic_node1)
        ###############################################     Node 2
        print('Node 2 - Display list of topics -> next step: Node 3')
        if res_topic_node1_records == 0:
            print('List of topics [TARGET DB]:')
            print('-------')
            print('EMPTY')
            print('-------')
        else:
            print('List of topics [TARGET DB]:')
            print('-------')
            for row in res_topic_node1_list:
                print('ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (row[0], row[1], row[2], row[3], row[4]))        
            print('-------')
        ###############################################     Node 3
        while True:
            node3_topic = etl_input("Node 3 - Is the current list of topics sufficient and correct? (y/n):\n")
            if not (node3_topic == 'y' or node3_topic == 'n'):
                print("Sorry, not known.")
                continue
            else:
                break

        if node3_topic == 'y':
            print('YES -> next step: Node 11')
        else:
            print('NO -> next step: Node 4')
            ###############################################     Node 4
            while True:
                node4_topic_action = etl_input("Node 4 - Set action: (insert/update/delete):\n")
                if not (node4_topic_action == 'insert' or node4_topic_action == 'update' or node4_topic_action == 'delete'):
                    print("Sorry, not known.")
                    continue
                else:
                    break

            if node4_topic_action == 'insert':
                ###############################################     Node 5
                print('Node 5 - Define an attributes of the new topic -> next step: Node 6')
                node5_topic_label = etl_input("Node 5 - Define a label of the new topic:\nexample (without quotes): 'plocha lesa'\n")
                node5_topic_description = etl_input("Node 5 - Define a description of the new topic:\nexample (without quotes): 'Plocha lesa.'\n")
                node5_topic_label_en = etl_input("Node 5 - Define a label_en of the new topic:\nexample (without quotes): 'forest area'\n")
                node5_topic_description_en = etl_input("Node 5 - Define a description_en of the new topic:\nexample (without quotes): 'Forest area.'\n")        
                ###############################################     Node 6
                print('Node 6 - save topic [TARGET DB] -> next step: Node 11 [over Nodes 1,2,3]')
                sql_cmd_topic_save = sql.SQL('''select {}.fn_etl_save_topic(%s,%s,%s,%s) as res_topic_node_6_save;''').format(sql.Identifier(schemaname))
                data_topic_node6_save = (node5_topic_label,node5_topic_description,node5_topic_label_en,node5_topic_description_en)
                res_topic_node6_save = run_sql(conn_dest, sql_cmd_topic_save, data_topic_node6_save)[0]['res_topic_node_6_save']
                ###############################################
            else:
                if node4_topic_action == 'update':
                    node4_topic_pyarray = etl_input("Node 4 - Set ID of topic for UPDATE:\n")
                else:
                    node4_topic_pyarray = etl_input("Node 4 - Set ID of topic for DELETE:\n")

                node4_topic_id = res_topic_node1_list[int(node4_topic_pyarray)-1]['id']

                #################################################################################### Node 7
                if node4_topic_action == 'update':
                    ####################################### SET NEW ATTRIBUTES FOR UPDATE
                    ## LABEL ##############################
                    while True:
                        node7_topic_label_y_n = etl_input("Node 7 - Do you want to update LABEL? (y/n):\n")
                        if not (node7_topic_label_y_n == 'y' or node7_topic_label_y_n == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break

                    if node7_topic_label_y_n == 'y':
                        node7_topic_label = etl_input("Node 7 - Define a new LABEL:\nexample (without quotes): 'plosne udaje'\n")
                    else:
                        node7_topic_label = None
                    ## DESCRIPTION ########################
                    while True:
                        node7_topic_description_y_n = etl_input("Node 7 - Do you want to update DESCRIPTION? (y/n):\n")
                        if not (node7_topic_description_y_n == 'y' or node7_topic_description_y_n == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break

                    if node7_topic_description_y_n == 'y':
                        node7_topic_description = etl_input("Node 7 - Define a new DESCRIPTION:\nexample (without quotes): 'Plosne udaje.'\n")
                    else:
                        node7_topic_description = None
                    ## LABEL_EN ###########################
                    while True:
                        node7_topic_label_en_y_n = etl_input("Node 7 - Do you want to update LABEL_EN? (y/n):\n")
                        if not (node7_topic_label_en_y_n == 'y' or node7_topic_label_en_y_n == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break

                    if node7_topic_label_en_y_n == 'y':
                        node7_topic_label_en = etl_input("Node 7 - Define a new LABEL_EN:\nexample (without quotes): 'area information'\n")
                    else:
                        node7_topic_label_en = None
                    ## DESCRIPTION_EN #####################
                    while True:
                        node7_topic_description_en_y_n = etl_input("Node 7 - Do you want to update DESCRIPTION_EN? (y/n):\n")
                        if not (node7_topic_description_en_y_n == 'y' or node7_topic_description_en_y_n == 'n'):
                            print("Sorry, not known.")
                            continue
                        else:
                            break

                    if node7_topic_description_en_y_n == 'y':
                        node7_topic_description_en = etl_input("Node 7 - Define a new DESCRIPTION_EN:\nexample (without quotes): 'Area information.'\n")
                    else:
                        node7_topic_description_en = None
                #######################################
                    # UPDATE
                    print('Node 7 - UPDATE topic -> next step: Node 11 [over Nodes 1,2,3]')
                    sql_cmd_topic_update = sql.SQL('''select {}.fn_etl_update_topic(%s,%s,%s,%s,%s) as res_topic_node_7_update;''').format(sql.Identifier(schemaname))
                    data_topic_update = (node4_topic_id,node7_topic_label,node7_topic_description,node7_topic_label_en,node7_topic_description_en)
                    res_topic_update = run_sql(conn_dest, sql_cmd_topic_update, data_topic_update)
                    #######################################
                #################################################################################### Node 8,9,10
                else:
                    print('Node 8 - TRY DELETE TOPIC')
                    sql_cmd_topic_node8_try_delete = sql.SQL('''select count(t.*) from (select * from {}.fn_etl_try_delete_topic(%s) as res_topic_node_8_try_delete) as t where t.res_topic_node_8_try_delete = true;''').format(sql.Identifier(schemaname))
                    data_topic_node8_try_delete = (node4_topic_id,)
                    res_topic_node8_try_delete = run_sql(conn_dest, sql_cmd_topic_node8_try_delete, data_topic_node8_try_delete)[0][0]
                    #print(res_topic_node8_try_delete)

                    if res_topic_node8_try_delete == 0:
                        print('Node 9 - TOPIC IS NOT ALLOWED TO DELETE !!! -> next step: STOP process ETL [In GUI apllication next step will be Node 1]')
                        sys.exit(1)
                    else:
                        # DELETE
                        print('Node 10 - DELETE TOPIC -> next step: Node 11 [over Nodes 1,2,3]')
                        sql_cmd_topic_node10 = sql.SQL('''select {}.fn_etl_delete_topic(%s) as res_topic_node_10_delete;''').format(sql.Identifier(schemaname))
                        data_topic_node10 = (node4_topic_id,)
                        res_topic_node10 = run_sql(conn_dest, sql_cmd_topic_node10,data_topic_node10)
                ####################################################################################


        ####################################################################################
        # INFORMATIVE LIST OF TOPICS for Node no. 3 if user solved some of actions
        if node3_topic == 'n':
            sql_cmd_topic_records = sql.SQL('''select count(*) as res_topic_node_1_records from {}.fn_etl_get_topics();''').format(sql.Identifier(schemaname))
            sql_cmd_topic_list = sql.SQL('''select * from {}.fn_etl_get_topics();''').format(sql.Identifier(schemaname))
            data_topic_node1 = (None, )
            res_topic_node1_records = run_sql(conn_dest, sql_cmd_topic_records, data_topic_node1)[0]['res_topic_node_1_records']
            res_topic_node1_list = run_sql(conn_dest, sql_cmd_topic_list, data_topic_node1)

            if res_topic_node1_records == 0:
                print('List of topics is EMPTY -> next step: STOP process ETL')
                sys.exit(1)
            else:
                print('List of topics:')
                print('-------')
                for row in res_topic_node1_list:
                    print('ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (row[0], row[1], row[2], row[3], row[4]))  
                print('-------')
        ####################################################################################


        ###############################################     Node 11
        print('Node 11 - get ETL_ID for target variable [SOURCE DB]: "%s"' % (res_target_variable_label_nl_en))
        sql_cmd_etl_id_for_target_variable = '''select target_data.fn_etl_get_etl_id4target_variable(%s) as res_topic_node_11_etl_id;'''
        if _variables == 1:
            data_topic_node11 = (res_target_variable_node3,)
        else:
            data_topic_node11 = (res_target_variable_node14,)
        res_topic_node11_etl_id = run_sql(conn_src, sql_cmd_etl_id_for_target_variable, data_topic_node11)[0]['res_topic_node_11_etl_id'] 
        #print(res_topic_node11_etl_id)
        ###############################################     Node 12
        print('Node 12 - get topics for target variable [TARGET DB]')
        sql_cmd_topic_mapping_records = sql.SQL('''select count(*) as res_topic_node_12_records from {}.fn_etl_get_target_variable2topic(%s);''').format(sql.Identifier(schemaname))
        sql_cmd_topic_mapping_list = sql.SQL('''select * from {}.fn_etl_get_target_variable2topic(%s);''').format(sql.Identifier(schemaname))
        data_topic_node12 = (res_topic_node11_etl_id,)
        res_topic_node12_records = run_sql(conn_dest, sql_cmd_topic_mapping_records, data_topic_node12)[0]['res_topic_node_12_records']
        res_topic_node12_list = run_sql(conn_dest, sql_cmd_topic_mapping_list, data_topic_node12)
        ###############################################     Node 13
        print('Node 13 - Display mapping list of topics -> next step: Node 14')
        if res_topic_node12_records == 0:
            print('Actually mapping list of topics for target variable: "%s" [TARGET DB]:' % (res_target_variable_label_nl_en))
            print('-------')
            print('EMPTY')
            print('-------')
        else:
            print('Actually mapping list of topics for target variable: "%s" [TARGET DB]:' % (res_target_variable_label_nl_en))
            print('-------')
            for row in res_topic_node12_list:
                print('ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (row[0], row[1], row[2], row[3], row[4]))        
            print('-------')
        ###############################################     Node 14
        while True:
            node14_topic = etl_input("Node 14 - Is the current list of mapping sufficient? (y/n):\n")
            if not (node14_topic == 'y' or node14_topic == 'n'):
                print("Sorry, not known.")
                continue
            else:
                break

        if node14_topic == 'y':
            print('YES -> next step: Node 20')
        else:
            print('NO -> next step: Node 15')
            ###############################################     Node 15
            while True:
                node15_topic_action = etl_input("Node 15 - Set action: (insert/delete):\n")
                if not (node15_topic_action == 'insert' or node15_topic_action == 'delete'):
                    print("Sorry, not known.")
                    continue
                else:
                    break

            if node15_topic_action == 'insert':
                ###############################################     Node 16
                print('Node 16 - Define a new mapping between target variable and topic. Select a topic from Node no. 2 -> next step: Node 17')
                node16_topic_id = node16_topic_pyarray = etl_input("Node 16 - Set ID of topic for INSERT to mapping:\n")
                ###############################################     Node 17
                print('Node 17 - save topic for mapping [TARGET DB] -> next step: Node 20 [over Nodes 12,13,14]')
                sql_cmd_topic_save_mapping = sql.SQL('''select {}.fn_etl_save_target_variable2topic(%s,%s) as res_topic_node_17_save_mapping;''').format(sql.Identifier(schemaname))
                data_topic_node17_save_mapping = (res_topic_node11_etl_id,node16_topic_id)
                res_topic_node17_save_mapping = run_sql(conn_dest, sql_cmd_topic_save_mapping, data_topic_node17_save_mapping)[0]['res_topic_node_17_save_mapping']
                ###############################################
            else:
                ###############################################     Node 18
                print('Node 18 - choose and set IDs [ID] of topics (choose from list in Node 13) for DELETE [TARGET DB] -> next step: next step: Node 19')
                lst4delete = []
                # number of elements as input
                n = int(input("Enter number of topics and IDs [SOURCE DB]: "))
                # iterating till the range
                for i in range(0, n):
                    ele = int(input())
                    lst4delete.append(ele) # adding the element
                print(lst4delete)
                ###############################################     Node 19
                print('Node 19 - DELETE TOPIC MAPPING -> next step: Node 20 [over Nodes 12,13,14]')
                sql_cmd_topic_node19_delete_mapping = sql.SQL('''select {}.fn_etl_delete_target_variable2topic(%s,%s) as res_topic_node_19_delete_mapping;''').format(sql.Identifier(schemaname))
                data_topic_node19_delete_mapping = (res_topic_node11_etl_id,lst4delete)
                res_topic_node19_delete_mapping = run_sql(conn_dest, sql_cmd_topic_node19_delete_mapping,data_topic_node19_delete_mapping)
                ####################################################################################


        ####################################################################################
        # INFORMATIVE LIST OF TOPIC MAPPING for Node no. 14 if user solved some of actions
        if node14_topic == 'n':
            sql_cmd_topic_mapping_records = sql.SQL('''select count(*) as res_topic_node_12_records from {}.fn_etl_get_target_variable2topic(%s);''').format(sql.Identifier(schemaname))
            sql_cmd_topic_mapping_list = sql.SQL('''select * from {}.fn_etl_get_target_variable2topic(%s);''').format(sql.Identifier(schemaname))
            data_topic_node12 = (res_topic_node11_etl_id,)
            res_topic_node12_records = run_sql(conn_dest, sql_cmd_topic_mapping_records, data_topic_node12)[0]['res_topic_node_12_records']
            res_topic_node12_list = run_sql(conn_dest, sql_cmd_topic_mapping_list, data_topic_node12)

            if res_topic_node12_records == 0:
                print('List of topic mapping is EMPTY -> next step: STOP process ETL')
                sys.exit(1)
            else:
                print('List of topic mapping:')
                print('-------')
                for row in res_topic_node12_list:
                    print('ID: %s   LABEL: %s  DESC: %s    LABEL_EN: %s    DESC_EN: %s' % (row[0], row[1], row[2], row[3], row[4]))
                print('-------')
        ####################################################################################


        ###############################################     Node 20
        print('Node 20 - Back to topics - this usecase is only implemented in GUI application -> next step: ETL - Variable')
        print('--------------------------------------------------------------')
        ###############################################################################
        ###############################################################################



        ###############################################################################
        ###############################################################################
        #####################################################    Variable
        print('ETL -  Variable')
        print('--------------------------------------------------------------')
        ###############################################     Node 1
        print('Node 1 - export variable [SOURCE DB] -> next step: Node 2')
        sql_cmd_export_variable = '''select target_data.fn_etl_export_variable(%s,%s) as res_variable_node_1;'''

        if _variables == 1:
            data_export_variable = (lst,res_target_variable_node3)
        else:
            data_export_variable = (lst,res_target_variable_node14)

        res_variable_node1 = run_sql(conn_src, sql_cmd_export_variable, data_export_variable)[0]['res_variable_node_1']
        ###############################################     Node 2
        print('Node 2 - import variable [TARGET DB] -> next step: ETL - Variable hierarchy')
        #sql_cmd_import_variable = '''select nfiesta_test.fn_etl_import_variable(%s) as res_variable_node_2;'''
        sql_cmd_import_variable = sql.SQL('''select {}.fn_etl_import_variable(%s) as res_variable_node_2;''').format(sql.Identifier(schemaname))
        data_import_variable = (json.dumps(res_variable_node1, ensure_ascii=False), )
        res_variable_node2 = run_sql(conn_dest, sql_cmd_import_variable, data_import_variable)[0]['res_variable_node_2']
        print(res_variable_node2)
        print('--------------------------------------------------------------')
        ###############################################################################
        ###############################################################################



        ###############################################################################
        ###############################################################################
        #####################################################    Variable hierarchy
        print('ETL -  Variable hierarchy')
        print('--------------------------------------------------------------')
        ###############################################     Node 1
        print('Node 1 - export variable hierarchy [SOURCE DB] -> next step: Node 2')
        sql_cmd_export_variable_hierarchy = '''select target_data.fn_etl_export_variable_hierarchy(%s,%s) as res_variable_hierarchy_node_1;'''

        if _variables == 1:
            data_export_variable_hierarchy = (lst,res_target_variable_node3)
        else:
            data_export_variable_hierarchy = (lst,res_target_variable_node14)

        res_variable_hierarchy_node1 = run_sql(conn_src, sql_cmd_export_variable_hierarchy, data_export_variable_hierarchy)[0]['res_variable_hierarchy_node_1']
        ###############################################     Node 2
        if res_variable_hierarchy_node1 == None:
            print('Node 2 - import variable hierarchy [TARGET DB] -> No record to import! -> next step: ETL - Ldsity values')
        else:
            print('Node 2 - import variable hierarchy [TARGET DB] -> next step: ETL - Ldsity values')
            #sql_cmd_import_variable_hierarchy = '''select nfiesta_test.fn_etl_import_variable_hierarchy(%s) as res_variable_hierarchy_node_2;'''
            sql_cmd_import_variable_hierarchy = sql.SQL('''select {}.fn_etl_import_variable_hierarchy(%s) as res_variable_hierarchy_node_2;''').format(sql.Identifier(schemaname))
            data_import_variable_hierarchy = (json.dumps(res_variable_hierarchy_node1, ensure_ascii=False), )
            res_variable_hierarchy_node2 = run_sql(conn_dest, sql_cmd_import_variable_hierarchy, data_import_variable_hierarchy)[0]['res_variable_hierarchy_node_2']
            print(res_variable_hierarchy_node2)
        print('--------------------------------------------------------------')
        ###############################################################################
        ###############################################################################



        ###############################################################################
        ###############################################################################
        #####################################################    Ldsity values
        print('ETL -  Ldsity values')
        print('--------------------------------------------------------------')
        ###############################################     Node 1
        print('Node 1 - export ldsity values [SOURCE DB] -> next step: Node 2')
        sql_cmd_export_ldsity_values = '''select target_data.fn_etl_export_ldsity_values(%s,%s) as res_ldsity_values_node_1;'''

        if _variables == 1:
            data_export_ldsity_values = (lst,res_target_variable_node3)
        else:
            data_export_ldsity_values = (lst,res_target_variable_node14)

        res_ldsity_values_node1 = run_sql(conn_src, sql_cmd_export_ldsity_values, data_export_ldsity_values)[0]['res_ldsity_values_node_1']
        ###############################################     Node 2
        if res_ldsity_values_node1 == None:
            print('Node 2 - import ldsity values [TARGET DB] -> No record to import! -> next step: ETL process is done.')
        else:
            print('Node 2 - import ldsity values [TARGET DB], start: %s' % (datetime.datetime.now()))
            #sql_cmd_import_ldsity_values = '''select nfiesta_test.fn_etl_import_ldsity_values(%s) as res_ldsity_values_node_2;'''
            sql_cmd_import_ldsity_values = sql.SQL('''select {}.fn_etl_import_ldsity_values(%s) as res_ldsity_values_node_2;''').format(sql.Identifier(schemaname))
            data_import_ldsity_values = (json.dumps(res_ldsity_values_node1, ensure_ascii=False), )
            res_ldsity_values_node2 = run_sql(conn_dest, sql_cmd_import_ldsity_values, data_import_ldsity_values)[0]['res_ldsity_values_node_2']
            print(res_ldsity_values_node2)
            print('Node 2 - import ldsity values [TARGET DB], finish: %s' % (datetime.datetime.now()))
        print('--------------------------------------------------------------')
        ###############################################################################
        ###############################################################################



        ###############################################################################
        #######################################################   ETL LOG
        print('Save records into T_ETL_LOG [SOURCE DB]')
        sql_cmd_save_log = '''select target_data.fn_etl_save_log(%s,%s) as res_save_log;'''

        if _variables == 1:
            data_save_log = (lst,res_target_variable_node3)
        else:
            data_save_log = (lst,res_target_variable_node14)

        res_save_log = run_sql(conn_src, sql_cmd_save_log, data_save_log)[0]['res_save_log']
        print('--------------------------------------------------------------')
        ###############################################################################


        #print('[TARGET DB], gathering statistics on imported data (analyze;) %s' % (datetime.datetime.now()))
        #sql_cmd_analyze = sql.SQL('''select nfiesta.fn_analyze_add_tables();''')
        #run_sql(conn_dest, sql_cmd_analyze, (None, ))
        #print('[TARGET DB], gathering statistics on imported data (analyze;) finished %s' % (datetime.datetime.now()))

        ###############################################################################
        #######################################################   COMMIT CONFIRMATION -> ADDITIVITY CHECK
        while True:
            commit_conn = etl_input("Commit ETL process -- transaction will be finalized after additivity check. (y/n):\n")
            if not (commit_conn == 'y' or commit_conn == 'n'):
                print("Sorry, not known.")
                continue
            else:
                break

        if commit_conn == 'y':
            print('[TARGET DB], additivity check start.: %s' % (datetime.datetime.now()))
            conn_dest.commit()
            print('[TARGET DB], additivity check finished. ETL process commited.: %s' % (datetime.datetime.now()))
            conn_src.commit()
            print('[SOURCE DB], ETL process commited.: %s' % (datetime.datetime.now()))
            print('--------------------------------------------------------------')
            print('ETL process for target variable "%s" is done.' %(res_target_variable_label_nl_en))
            print('--------------------------------------------------------------')    
        elif commit_conn == 'n':
            print('ETL process was canceled !!!')
        ###############################################################################
    else:
        print('YES - next step: ETL -  COMMIT CONFIRMATION and END ETL process')
        print('--------------------------------------------------------------')
        ###############################################################################
        #######################################################   COMMIT CONFIRMATION
        while True:
            commit_conn = etl_input("Commit ETL process (y/n):\n")
            if not (commit_conn == 'y' or commit_conn == 'n'):
                print("Sorry, not known.")
                continue
            else:
                break

        if commit_conn == 'y':
            conn_dest.commit()
            print('[TARGET DB], ETL process of ATOMIC attributes was commited.')
            conn_src.commit()
            print('[SOURCE DB], ETL process of ATOMIC attributes was commited.')
            print('--------------------------------------------------------------')
            print('ETL process of ATOMIC attributes for target variable "%s" is done.' %(res_target_variable_label_nl_en))
            print('--------------------------------------------------------------')    
        elif commit_conn == 'n':
            print('ETL process of ATOMIC attributes for target variable "%s" was canceled.' %(res_target_variable_label_nl_en))
    ###############################################################################
    ###############################################################################



    ###############################################################################
    ###########################################################     DB CONN CLOSE
    conn_src.close()
    conn_dest.close()
    ###############################################################################
