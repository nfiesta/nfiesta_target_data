--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_array_compare
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_array_compare(anyarray, anyarray) CASCADE;

create or replace function target_data.fn_etl_array_compare(anyarray,anyarray) 
returns boolean as $$
declare
		_res	boolean;
begin
	_res := (	
			select
				case
			  		when array_dims($1) <> array_dims($2) then 'f'
			 		when array_length($1,1) <> array_length($2,1) then 'f'
			  	else
				    not exists	(
						        select 1
						        from unnest($1) a 
						        full join unnest($2) b on (a=b) 
						        where a is null or b is null
				   				)
		  		end
		  	);
	
	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_array_compare(anyarray, anyarray) is
'Function returns true (the number of elements and the elements are identical) or false (the number of elements and the elements are not identical).';

grant execute on function target_data.fn_etl_array_compare(anyarray, anyarray) to public;