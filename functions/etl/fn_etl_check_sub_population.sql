--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_sub_population(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_check_sub_population
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns table
(
	id						integer,
	export_connection		integer,
	sub_population			integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_sp				integer,
	atomic					boolean
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
	
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.spc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.spc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			return query
			with w as	(
						select
								0 as id,
								0 as export_connection,
								array[0] as sub_population,
								''::varchar as label,
								''::text as description,
								''::varchar as label_en,
								''::text as description_en,
								0 as etl_id,
								0 as id_t_etl_sp,
								null::boolean as atomic
						)
			select w.id, w.export_connection, w.sub_population, w.label, w.description, w.label_en, w.description_en, w.etl_id, w.id_t_etl_sp, w.atomic
			from w
			where w.id is distinct from 0;		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.spc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.spc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)		
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select array[csp.id] as id_type, csp.label, csp.description, csp.label_en, csp.description_en
					from target_data.c_sub_population as csp where csp.id in (select w4.id_type[1] from w4)
					)
			,w6 as	(
					select w2.id_type, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select
							_export_connection as export_connection,
							w6.id_type as sub_population,
							w6.label,
							w6.description,
							w6.label_en,
							w6.description_en
					from
							w6
					)
			,w8 as	(
					select
							w7.export_connection,
							w7.sub_population,
							w7.label,
							w7.description,
							w7.label_en,
							w7.description_en,
							t.etl_id,
							t.id as id_t_etl_sp
					from
							w7 left join target_data.t_etl_sub_population as t
					on
							w7.export_connection = t.export_connection
					and	
							--target_data.fn_etl_array_compare(w7.sub_population,t.sub_population)
							(w7.sub_population @> t.sub_population and w7.sub_population <@ t.sub_population)
					order
							by w7.sub_population
					)
			select
					(row_number() over ())::integer as id_order,
					w8.export_connection,
					w8.sub_population,
					w8.label,
					w8.description,
					w8.label_en,
					w8.description_en,
					w8.etl_id,
					w8.id_t_etl_sp,
					case when array_length(w8.sub_population,1) = 1 then true else false end as atomic
			from
					w8;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_sub_population(integer[], integer) is
'Function returns records for ETL c_sub_population table.';

grant execute on function target_data.fn_etl_check_sub_population(integer[], integer) to public;