--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_target_variable(integer, integer) CASCADE;

create or replace function target_data.fn_etl_check_target_variable
(
	_export_connection		integer,
	_target_variable		integer
)
returns table
(
	id_etl_target_variable		integer,
	check_target_variable		boolean,
	refyearset2panel_mapping	integer[]
)
as
$$
declare
		_id_etl_target_variable		integer;
		_refyearset2panel_mapping	integer[];
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_target_variable: Input argument _export_connection must not by NULL!';
		end if; 
	
		if _target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_target_variable: Input argument _target_variable must not by NULL!';
		end if;
	
		select id from target_data.t_etl_target_variable
		where export_connection = _export_connection
		and target_variable = _target_variable
		into _id_etl_target_variable;
	
		if _id_etl_target_variable is null
		then
			return query select
								_id_etl_target_variable as etl_target_variable,
								false as check_target_variable,
								null::integer[] as refyearset2panel_mapping;
		else
			with
			w1 as	(
					select * from target_data.t_etl_log
					where etl_target_variable = _id_etl_target_variable
					)
			,w2 as	(
					select w1.refyearset2panel_mapping, max(w1.etl_time) as max_etl_time from w1
					group by w1.refyearset2panel_mapping
					)
			,w3 as	(
					select w1.*, w2.max_etl_time from w1 inner join w2
					on w1.refyearset2panel_mapping = w2.refyearset2panel_mapping
					and w1.etl_time = w2.max_etl_time
					)
			,w4 as	(-- list of panels and reference year sets that was ETL, source is from t_etl_log
					select
							w3.id as id_etl_log,
							w3.etl_target_variable,
							w3.refyearset2panel_mapping as refyearset2panel_mapping_in_log,
							w3.etl_time,
							w3.max_etl_time,
							crpm.panel as panel_in_log,
							crpm.reference_year_set as reference_year_set_in_log
					from w3
					inner join sdesign.cm_refyearset2panel_mapping as crpm on w3.refyearset2panel_mapping = crpm.id
					)
			,w5 as	(-- list of panels and reference year sets that are in configuration for given target variable
					 -- with information about ldsity_threshold and last_change
					select tad.* from target_data.t_available_datasets as tad
					where categorization_setup in
						(
						select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
						where ldsity2target_variable in
							(
							select id from target_data.cm_ldsity2target_variable
							where target_variable = (
													select tetv.target_variable
													from target_data.t_etl_target_variable as tetv
													where tetv.id = _id_etl_target_variable
													)
							)
						)
					)
			,w6 as	(
					select w5.*, w4.* from w5 left join w4
					on w5.panel = w4.panel_in_log and w5.reference_year_set = w4.reference_year_set_in_log
					)
			,w7 as	(-- list of panels and reference year sets that were not ETL yet
					select crpm.id as refyearset2panel_mapping
					from sdesign.cm_refyearset2panel_mapping as crpm
					inner join
								(
								select distinct w6.panel, w6.reference_year_set from w6
								where w6.id_etl_log is null
								) as t
					on crpm.panel = t.panel and crpm.reference_year_set = t.reference_year_set
					)			
			,w8 as	(-- list of panels and reference year sets that were ETL but exists newer local densities
					select distinct w6.refyearset2panel_mapping_in_log as refyearset2panel_mapping
					from w6 where w6.id_etl_log is not null and w6.last_change > w6.max_etl_time
					order by w6.refyearset2panel_mapping_in_log
					)
			select
				array_agg(a.refyearset2panel_mapping order by a.refyearset2panel_mapping) as refyearset2panel_mapping
			from
				(
				select w7.refyearset2panel_mapping from w7 union all
				select w8.refyearset2panel_mapping from w8
				) as a
			into
				_refyearset2panel_mapping;

			if _refyearset2panel_mapping is null
			then
				return query select
									_id_etl_target_variable as etl_target_variable,
									true as check_target_variable,
									null::integer[] as refyearset2panel_mapping;
			else
				return query select
									_id_etl_target_variable as etl_target_variable,
									false as check_target_variable,
									_refyearset2panel_mapping as refyearset2panel_mapping;
			end if;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_target_variable(integer, integer) is
'Function checks that target variable and all theirs current datas was ETL (true) or not (false).';

grant execute on function target_data.fn_etl_check_target_variable(integer, integer) to public;