--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_delete_export_connefn_etl_delete_etl_data4export_connectionction
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_etl_delete_etl_data4export_connection(integer) CASCADE;

create or replace function target_data.fn_etl_delete_etl_data4export_connection
(
	_export_connection integer
)
returns text
as
$$
declare
	_res text;
begin
	-------------------------------------------------------
	if _export_connection is null
	then 
		raise exception 'Error: 01: fn_etl_delete_etl_data4export_connection: Input argument _id must not be NULL!';
	end if;
	-------------------------------------------------------
	if not exists (select t1.* from target_data.t_export_connection as t1 where t1.id = _export_connection)
	then
		raise exception 'Error: 02: fn_etl_delete_etl_data4export_connection: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
	end if;
	-------------------------------------------------------
	delete from target_data.t_etl_area_domain_category where etl_area_domain in (select id from target_data.t_etl_area_domain where export_connection = _export_connection);
	delete from target_data.t_etl_area_domain where export_connection = _export_connection;
	delete from target_data.t_etl_sub_population_category where etl_sub_population in (select id from target_data.t_etl_sub_population where export_connection = _export_connection);
	delete from target_data.t_etl_sub_population where export_connection = _export_connection;
	delete from target_data.t_etl_log where etl_target_variable	in (select id from target_data.t_etl_target_variable where export_connection = _export_connection);
	delete from target_data.t_etl_target_variable where export_connection = _export_connection;	
	delete from target_data.t_export_connection where id = _export_connection;
	-------------------------------------------------------
	_res := concat('The ETL datas for export connection ID = ',_export_connection,' was successfully deleted.');
	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_delete_etl_data4export_connection(integer) is
'The function deletes all records from ETL tables for given export_connection.';

grant execute on function target_data.fn_etl_delete_etl_data4export_connection(integer) to app_nfiesta_mng;