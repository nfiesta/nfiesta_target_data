--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_area_domain_category_mapping
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_area_domain_category_mapping(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_area_domain_category_mapping
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_check_ad		integer;
		_check_adc		integer;
		_res			json;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_area_domain_category_mapping: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_area_domain_category_mapping: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;

		select count(*) from target_data.fn_etl_check_area_domain(_refyearset2panel_mapping,_id_t_etl_target_variable)
		into _check_ad;

		if _check_ad = 0
		then
				with
				w1 as	(
						select
								0 as etl_id_adc_non_atomic,
								0 as etl_id_adc_atomic
						)
				select json_agg(json_build_object(
						'area_domain_category_non_atomic',w1.etl_id_adc_non_atomic,
						'area_domain_category_atomic',w1.etl_id_adc_atomic))
				from
						w1 where w1.etl_id_adc_non_atomic > 0
				into
						_res;
		else
			select count(*) from target_data.fn_etl_check_area_domain_category(_refyearset2panel_mapping,_id_t_etl_target_variable)
			where etl_id is null
			into _check_adc;

			if _check_adc = 0
			then
				with
				w1 as	(
						select
								id,
								etl_id_area_domain,
								area_domain_category,
								etl_id,
								id_t_etl_adc,
								id_t_etl_ad
						from
								target_data.fn_etl_check_area_domain_category(_refyearset2panel_mapping,_id_t_etl_target_variable)
						)
				,w2 as	(
						select area_domain_category, etl_id from w1 where array_length(area_domain_category,1) > 1
						)
				,w3 as	(
						select area_domain_category[1] as adc_atomic, etl_id from w1 where array_length(area_domain_category,1) = 1
						)
				,w4 as	(
						select
								t1.etl_id as etl_id_adc_non_atomic,
								w3.etl_id as etl_id_adc_atomic
						from
								(select etl_id, unnest(area_domain_category) as adc_non_atomic from w2) as t1
								inner join w3 on t1.adc_non_atomic = w3.adc_atomic
						)
				select json_agg(json_build_object(
						'area_domain_category_non_atomic',w4.etl_id_adc_non_atomic,
						'area_domain_category_atomic',w4.etl_id_adc_atomic))
				from
						w4
				into
						_res;					
			else
				raise exception 'Error 03: fn_etl_export_area_domain_category_mapping: For some of area domain category not exists ETL ID!';
			end if;

		end if;
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_area_domain_category_mapping(integer[], integer) is
'Function returns records for ETL cm_area_domain_category table.';

grant execute on function target_data.fn_etl_export_area_domain_category_mapping(integer[], integer) to public;