--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_sub_population_category_mapping
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_sub_population_category_mapping(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_sub_population_category_mapping
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_check_sp		integer;
		_check_spc		integer;
		_res			json;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_sub_population_category_mapping: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_sub_population_category_mapping: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;

		select count(*) from target_data.fn_etl_check_sub_population(_refyearset2panel_mapping,_id_t_etl_target_variable)
		into _check_sp;

		if _check_sp = 0
		then
				with
				w1 as	(
						select
								0 as etl_id_spc_non_atomic,
								0 as etl_id_spc_atomic
						)
				select json_agg(json_build_object(
						'sub_population_category_non_atomic',w1.etl_id_spc_non_atomic,
						'sub_population_category_atomic',w1.etl_id_spc_atomic))
				from
						w1 where w1.etl_id_spc_non_atomic > 0
				into
						_res;
		else
			select count(*) from target_data.fn_etl_check_sub_population_category(_refyearset2panel_mapping,_id_t_etl_target_variable)
			where etl_id is null
			into _check_spc;

			if _check_spc = 0
			then
				with
				w1 as	(
						select
								id,
								etl_id_sub_population,
								sub_population_category,
								etl_id,
								id_t_etl_spc,
								id_t_etl_sp
						from
								target_data.fn_etl_check_sub_population_category(_refyearset2panel_mapping,_id_t_etl_target_variable)
						)
				,w2 as	(
						select sub_population_category, etl_id from w1 where array_length(sub_population_category,1) > 1
						)
				,w3 as	(
						select sub_population_category[1] as spc_atomic, etl_id from w1 where array_length(sub_population_category,1) = 1
						)
				,w4 as	(
						select
								t1.etl_id as etl_id_spc_non_atomic,
								w3.etl_id as etl_id_spc_atomic
						from
								(select etl_id, unnest(sub_population_category) as spc_non_atomic from w2) as t1
								inner join w3 on t1.spc_non_atomic = w3.spc_atomic
						)
				select json_agg(json_build_object(
						'sub_population_category_non_atomic',w4.etl_id_spc_non_atomic,
						'sub_population_category_atomic',w4.etl_id_spc_atomic))
				from
						w4
				into
						_res;					
			else
				raise exception 'Error 03: fn_etl_export_sub_population_category_mapping: For some of sub population category not exists ETL ID!';
			end if;

		end if;
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_sub_population_category_mapping(integer[], integer) is
'Function returns records for ETL cm_sub_population_category table.';

grant execute on function target_data.fn_etl_export_sub_population_category_mapping(integer[], integer) to public;