--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_variable(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_variable
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_categorization_setups		integer[];
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_variable: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_variable: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 04: fn_etl_export_variable: Internal argument _categorization_setups must not by NULL!';
		end if;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);

		with
		w1 as	(
				select
						id,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest(_categorization_setups))
				)
		,w2 as	(
				select
						w1.*,
						cltv.ldsity_object_type as core_or_division
				from
						w1
						inner join target_data.cm_ldsity2target_variable as cltv on w1.ldsity2target_variable = cltv.id
				)
		-------------------------
		,w10a as	(select * from w2 where core_and_division = false)
		,w10b as	(select * from w2 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w2 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w10a
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					core_or_division
					from w12b
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w12a
					)
		-------------------------
		,w4 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w13 order by categorization_setup
				)
		,w5 as	(
				select * from w4 where id_spt is null and id_spc is null and id_adt is null and id_adc is null -- choosing of row without distinction
				)
		,w6 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from (select * from w4 where categorization_setup is distinct from (select categorization_setup from w5)) as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on (t.id_spt @> tesp.sub_population and t.id_spt <@ tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on (t.id_adt @> tead.area_domain and t.id_adt <@ tead.area_domain)
				
				)
		,w7 as	(
				select
						w6.*,
						tespc.etl_id as etl_id_spc,
						teadc.etl_id as etl_id_adc
				from w6
				
				left join	target_data.t_etl_sub_population_category as tespc
				on  w6.id_sub_population = tespc.etl_sub_population
				and (w6.id_spc @> tespc.sub_population_category and w6.id_spc <@ tespc.sub_population_category)
				
				left join	target_data.t_etl_area_domain_category as teadc
				on  w6.id_area_domain = teadc.etl_area_domain
				and (w6.id_adc @> teadc.area_domain_category and w6.id_adc <@ teadc.area_domain_category)
				)
		,w8 as	(
				select categorization_setup, _etl_id as etl_id_tv, 0 as etl_id_spc, 0 as etl_id_adc from w5 -- row without distinction 
				union all
				select
						categorization_setup,
						_etl_id as etl_id_tv,
						case when etl_id_spc is null then 0 else etl_id_spc end as etl_id_spc,
						case when etl_id_adc is null then 0 else etl_id_adc end as etl_id_adc
				from
						w7 order by categorization_setup
				)
		select
			json_agg(json_build_object(
				'target_variable', w8.etl_id_tv,
				'sub_population_category', w8.etl_id_spc, 
				'area_domain_category', w8.etl_id_adc))
		from
				w8 into _res;
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_variable(integer[], integer) is
'Function returns records for ETL t_variable table.';

grant execute on function target_data.fn_etl_export_variable(integer[], integer) to public;