--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_variable_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_variable_hierarchy(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_variable_hierarchy
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_categorization_setups		integer[];
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_variable_hierarchy: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_variable_hierarchy: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_etl_export_variable_hierarchy: Internal argument _categorization_setups must not by NULL!';
		end if;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);

		-------------------------------------------------------------
		refresh materialized view target_data.v_variable_hierarchy_internal;
		-------------------------------------------------------------
		
		with
		w_etl_ad as		(
						select * from target_data.t_etl_area_domain	where export_connection = _export_connection
						)
		,w_etl_adc as	(
						select * from target_data.t_etl_area_domain_category where etl_area_domain in (select id from w_etl_ad)
						)
		,w_etl_sp as	(
						select * from target_data.t_etl_sub_population where export_connection = _export_connection
						)
		,w_etl_spc as	(
						select * from target_data.t_etl_sub_population_category where etl_sub_population in (select id from w_etl_sp)
						)
		,w1 as	(
				select
						id,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest(_categorization_setups))
				)
		,w2 as	(
				select
						w1.*,
						cltv.ldsity_object_type as core_or_division
				from
						w1
						inner join target_data.cm_ldsity2target_variable as cltv on w1.ldsity2target_variable = cltv.id
				)
		-------------------------
		,w10a as	(select * from w2 where core_and_division = false)
		,w10b as	(select * from w2 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w2 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w10a
					union all			
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					core_or_division
					from w12b
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w12a
					)
		-------------------------
		,w4 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w13 order by categorization_setup
				)
		,w5 as	(	
				select
						node as variable_superior,
						panel, reference_year_set,
						unnest(edges) as variable
				from
						target_data.v_variable_hierarchy
				where
						node in (select unnest(_categorization_setups))
				)
		,w6 as	(
				select
						_etl_id as etl_id_target_variable,
						w5.variable,
						w5.panel, w5.reference_year_set,
						w5.variable_superior,
						w4b.id_spt,
						w4a.id_spt as id_spt_sup,
						w4b.id_spc,
						w4a.id_spc as id_spc_sup,
						w4b.id_adt,
						w4a.id_adt as id_adt_sup,
						w4b.id_adc,
						w4a.id_adc as id_adc_sup
				from		w5
				left join	w4 as w4a	on w5.variable_superior = w4a.categorization_setup
				left join	w4 as w4b	on w5.variable = w4b.categorization_setup
				)
		,w15a as	(
					select t1.id_spt, w_etl_sp.id as id_sub_population_podrizene from
					(select distinct id_spt from w6 where id_spt is not null) as t1
					inner join w_etl_sp
					on (t1.id_spt @> w_etl_sp.sub_population and t1.id_spt <@ w_etl_sp.sub_population)
					)
		,w15b as	(
					select t2.id_spt_sup, w_etl_sp.id as id_sub_population_nadrizene from
					(select distinct id_spt_sup from w6 where id_spt_sup is not null) as t2
					inner join w_etl_sp
					on (t2.id_spt_sup @> w_etl_sp.sub_population and t2.id_spt_sup <@ w_etl_sp.sub_population)
					)
		,w15c as	(
					select t3.id_adt, w_etl_ad.id as id_area_domain_podrizene from
					(select distinct id_adt from w6 where id_adt is not null) as t3
					inner join w_etl_ad
					on (t3.id_adt @> w_etl_ad.area_domain and t3.id_adt <@ w_etl_ad.area_domain)
					)
		,w15d as	(
					select t4.id_adt_sup, w_etl_ad.id as id_area_domain_nadrizene from
					(select distinct id_adt_sup from w6 where id_adt_sup is not null) as t4
					inner join w_etl_ad
					on (t4.id_adt_sup @> w_etl_ad.area_domain and t4.id_adt_sup <@ w_etl_ad.area_domain)
					)					
		,w7a as	(
				select
						w6.*,
						w15a.id_sub_population_podrizene,
						w15b.id_sub_population_nadrizene,
						w15c.id_area_domain_podrizene,
						w15d.id_area_domain_nadrizene
				from
						w6
						left join w15a on w6.id_spt = w15a.id_spt
						left join w15b on w6.id_spt_sup = w15b.id_spt_sup
						left join w15c on w6.id_adt = w15c.id_adt
						left join w15d on w6.id_adt_sup = w15d.id_adt_sup
				)
		,w16a as	(
					select t1.id_sub_population_podrizene, t1.id_spc, w_etl_spc.etl_id as etl_id_spc from
					(select distinct id_sub_population_podrizene, id_spc from w7a where id_spc is not null) as t1
					inner join w_etl_spc on t1.id_sub_population_podrizene = w_etl_spc.etl_sub_population
					and (t1.id_spc @> w_etl_spc.sub_population_category and t1.id_spc <@ w_etl_spc.sub_population_category)
					)
		,w16b as	(
					select t2.id_sub_population_nadrizene, t2.id_spc_sup, w_etl_spc.etl_id as etl_id_spc_sup from
					(select distinct id_sub_population_nadrizene, id_spc_sup from w7a where id_spc_sup is not null) as t2
					inner join w_etl_spc on t2.id_sub_population_nadrizene = w_etl_spc.etl_sub_population
					and (t2.id_spc_sup @> w_etl_spc.sub_population_category and t2.id_spc_sup <@ w_etl_spc.sub_population_category)
					)
		,w16c as	(
					select t3.id_area_domain_podrizene, t3.id_adc, w_etl_adc.etl_id as etl_id_adc from
					(select distinct id_area_domain_podrizene, id_adc from w7a where id_adc is not null) as t3
					inner join w_etl_adc on t3.id_area_domain_podrizene = w_etl_adc.etl_area_domain
					and (t3.id_adc @> w_etl_adc.area_domain_category and t3.id_adc <@ w_etl_adc.area_domain_category)
					)
		,w16d as	(
					select t4.id_area_domain_nadrizene, t4.id_adc_sup, w_etl_adc.etl_id as etl_id_adc_sup from
					(select distinct id_area_domain_nadrizene, id_adc_sup from w7a where id_adc_sup is not null) as t4
					inner join w_etl_adc on t4.id_area_domain_nadrizene = w_etl_adc.etl_area_domain
					and (t4.id_adc_sup @> w_etl_adc.area_domain_category and t4.id_adc_sup <@ w_etl_adc.area_domain_category)
					)			
		,w8a as	(
				select
						w7a.*,
						w16a.etl_id_spc,
						w16b.etl_id_spc_sup,
						w16c.etl_id_adc,
						w16d.etl_id_adc_sup
				from
						w7a
						left join w16a on w7a.id_sub_population_podrizene = w16a.id_sub_population_podrizene and w7a.id_spc = w16a.id_spc
						left join w16b on w7a.id_sub_population_nadrizene = w16b.id_sub_population_nadrizene and w7a.id_spc_sup = w16b.id_spc_sup
						left join w16c on w7a.id_area_domain_podrizene = w16c.id_area_domain_podrizene and w7a.id_adc = w16c.id_adc
						left join w16d on w7a.id_area_domain_nadrizene = w16d.id_area_domain_nadrizene and w7a.id_adc_sup = w16d.id_adc_sup
				)			
		,w9 as	(
				select
						etl_id_target_variable as etl_id_tv,
						panel,
						reference_year_set,
						case when etl_id_spc is null then 0 else etl_id_spc end				as etl_id_spc,
						case when etl_id_spc_sup is null then 0 else etl_id_spc_sup end 	as etl_id_spc_sup,
						case when etl_id_adc is null then 0 else etl_id_adc end				as etl_id_adc,
						case when etl_id_adc_sup is null then 0 else etl_id_adc_sup end		as etl_id_adc_sup
				from
						w8a
				)
		select
			json_agg(json_build_object(
				'target_variable',			w9.etl_id_tv,
				'country',					(select c_country.label
										from sdesign.c_country
										inner join sdesign.t_strata_set on (t_strata_set.country = c_country.id)
										inner join sdesign.t_stratum on (t_stratum.strata_set = t_strata_set.id)
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'strata_set',					(select t_strata_set.strata_set
										from sdesign.t_strata_set
										inner join sdesign.t_stratum on (t_stratum.strata_set = t_strata_set.id)
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'stratum',					(select t_stratum.stratum
										from sdesign.t_stratum
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'reference_year_set',				(select reference_year_set from sdesign.t_reference_year_set where t_reference_year_set.id = w9.reference_year_set),
				'panel',					(select panel from sdesign.t_panel where t_panel.id = w9.panel),
				'sub_population_category',		w9.etl_id_spc,
				'sub_population_category_superior',	w9.etl_id_spc_sup,
				'area_domain_category',			w9.etl_id_adc,
				'area_domain_category_superior',	w9.etl_id_adc_sup)
				order by w9.etl_id_tv, w9.panel, w9.reference_year_set, w9.etl_id_spc, w9.etl_id_spc_sup, w9.etl_id_adc, w9.etl_id_adc_sup
			)
		from w9
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_variable_hierarchy(integer[], integer) is
'Function returns records for ETL t_variable_hierarchy table.';

grant execute on function target_data.fn_etl_export_variable_hierarchy(integer[], integer) to public;
