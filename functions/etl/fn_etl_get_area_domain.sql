--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domain(integer[], integer, json, boolean) CASCADE;

create or replace function target_data.fn_etl_get_area_domain
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_auto_information			json,
	_etl						boolean default null::boolean
)
returns table
(
	id							integer,
	export_connection			integer,
	area_domain					integer[],
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	etl_id						integer,
	id_t_etl_ad					integer,
	atomic						boolean,
	auto_transfer				boolean,
	auto_pair					boolean,
	auto						boolean
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
		_cond						text;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;

		if _auto_information is null
		then
			raise exception 'Error 03: fn_etl_get_area_domain: Input argument _auto_information must not be NULL!';
		end if;			

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;		

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
			
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.adc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.adc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			return query
			with w as	(
						select
								0 as id,
								0 as export_connection,
								array[0] as area_domain,
								''::varchar as label,
								''::text as description,
								''::varchar as label_en,
								''::text as description_en,
								0 as etl_id,
								0 as id_t_etl_ad,
								null::boolean as atomic,
								null::boolean as auto_transfer,
								null::boolean as auto_pair,
								null::boolean as auto
						)
			select w.id, w.export_connection, w.area_domain, w.label, w.description, w.label_en,
			w.description_en, w.etl_id, w.id_t_etl_ad, w.atomic, w.auto_transfer, w.auto_pair, w.auto
			from w where w.id is distinct from 0;		
		else
			if _etl is null
			then
				_cond := 'TRUE'::text;
			else
				if _etl = true
				then
					_cond := 'w10.etl_id is not null'::text;
				else
					_cond := 'w10.etl_id is null'::text;
				end if;
			end if;
		
			return query execute
			'
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.adc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).label as type_label,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).description as type_description,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).label_en as type_label_en,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).description_en as type_description_en,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).id_category,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).label as category_label,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).description as category_description,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).label_en as category_label_en,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).description_en as category_description_en
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest($2))								
														)
					and
							t.adc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.type_label, w1.type_description, w1.type_label_en, w1.type_description_en, w1.id_category, w1.category_label, w1.category_description, w1.category_label_en, w1.category_description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select array[cad.id] as id_type, cad.label as type_label, cad.description as type_description, cad.label_en as  type_label_en, cad.description_en as type_description_en
					from target_data.c_area_domain as cad
					where cad.id in (select w4.id_type[1] from w4)
					)
			,w6 as (
					select
							w5.*,
							array[t.id] as id_category,
							t.label as category_label,
							t.description as category_description,
							t.label_en as category_label_en,
							t.description_en as category_description_en
					from
							w5
					inner join
							(
							select cadc.* from target_data.c_area_domain_category as cadc
							where cadc.area_domain in (select w5.id_type[1] from w5)
							) as t 
					on
							w5.id_type[1] = t.area_domain
					)			
			,w7 as	(
					select w2.id_type, w2.type_label, w2.type_description, w2.type_label_en, w2.type_description_en, w2.id_category, w2.category_label, w2.category_description, w2.category_label_en, w2.category_description_en from w2 union all
					select w6.id_type, w6.type_label, w6.type_description, w6.type_label_en, w6.type_description_en, w6.id_category, w6.category_label, w6.category_description, w6.category_label_en, w6.category_description_en from w6
					)
			,w8 as	(
					select
							$1 as export_connection,
							w7.id_type as area_domain,
							w7.type_label,
							w7.type_description,
							w7.type_label_en,
							w7.type_description_en,
							w7.id_category as area_domain_category,
							w7.category_label,
							w7.category_description,
							w7.category_label_en,
							w7.category_description_en							
					from
							w7
					)
			,w9 as	(
					select
							w8.export_connection,
							w8.area_domain,
							w8.type_label,
							w8.type_description,
							w8.type_label_en,
							w8.type_description_en,
							w8.area_domain_category,
							w8.category_label,
							w8.category_description,
							w8.category_label_en,
							w8.category_description_en,								
							t.etl_id,
							t.id as id_t_etl_ad
					from
							w8 left join target_data.t_etl_area_domain as t
					on
							w8.export_connection = t.export_connection
					and	
							target_data.fn_etl_array_compare(w8.area_domain,t.area_domain)
					order
							by w8.area_domain, w8.area_domain_category
					)
			,w10 as (
					select 
							a.*,
							b.category_json
					from
							(select distinct w9.export_connection, w9.area_domain, w9. type_label, w9.type_description, w9.type_label_en, w9.type_description_en, w9.etl_id, w9.id_t_etl_ad from w9) as a
					inner join
							(
							select
									w9.area_domain,
									json_agg(json_build_object(''area_domain_category'',w9.area_domain_category,''label_en_category'',w9.category_label_en)) as category_json
							from
									w9 group by w9.area_domain
							) as b
					on
							a.area_domain = b.area_domain
					order
							by a.area_domain
					)
			,w11 as (
					select
							(row_number() over ())::integer as id_order,
							w10.export_connection,
							w10.area_domain,
							w10.type_label,
							w10.type_description,
							w10.type_label_en,
							w10.type_description_en,							
							w10.etl_id,
							w10.id_t_etl_ad,
							w10.category_json,
							case when array_length(w10.area_domain,1) = 1 then true else false end as atomic
					from
							w10 where '|| _cond ||'
					)
			----------------------------------------
			,w12 as	(
					select $3 as res
					)
			,w13 as	(
					select json_array_elements(w12.res) as auto_information from w12
					)
			,w14 as	(
					select
							(w13.auto_information->>''id'')::integer as id,
							(w13.auto_information->>''auto_transfer'')::boolean as auto_transfer,
							(w13.auto_information->>''auto_pair'')::boolean as auto_pair
					from
							w13
					)
			----------------------------------------
			,w15 as	(
					select
							w11.*,
							w14.auto_transfer,
							w14.auto_pair
					from
							w11 inner join w14 on w11.id_order = w14.id
					)
			select
					w15.id_order,
					w15.export_connection,
					w15.area_domain,
					w15.type_label,
					w15.type_description,
					w15.type_label_en,
					w15.type_description_en,
					w15.etl_id,
					w15.id_t_etl_ad,
					w15.atomic,
					w15.auto_transfer,
					w15.auto_pair,
					case when (w15.auto_transfer = true or w15.auto_pair = true) then true else false end as auto
			from
					w15 order by w15.id_order
			' using _export_connection, _categorization_setups, _auto_information;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domain(integer[], integer, json, boolean) is
'Function returns records for ETL c_area_domain table.';

grant execute on function target_data.fn_etl_get_area_domain(integer[], integer, json, boolean) to public;