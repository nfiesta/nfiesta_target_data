--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_categories4area_domains
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domain_categories4area_domains(integer) CASCADE;

create or replace function target_data.fn_etl_get_area_domain_categories4area_domains
(
	_export_connection	integer
)
returns json
as
$$
declare
		_res	json;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_categories4area_domains: Input argument _export_connection must not be null!';
		end if;
		-------------------------------------------------------------
		with
		w1 as	(
				select
						tead.id as etl_area_domain__id,
						tead.etl_id as etl_area_domain__etl_id
				from
						target_data.t_etl_area_domain as tead
				where
						tead.export_connection = _export_connection
				and
						array_length(tead.area_domain,1) = 1
				)
		,w2 as	(
				select
						w1.*,
						teadc.id as etl_area_domain_category__id,
						teadc.area_domain_category as etl_area_domain_category__adc,
						teadc.etl_id as etl_area_domain_category__etl_id
				from
						w1
						inner join target_data.t_etl_area_domain_category as teadc
						on w1.etl_area_domain__id = teadc.etl_area_domain
				)
		,w3 as	(
				select
						w2.*,
						cadc.label,
						cadc.description,
						cadc.label_en,
						cadc.description_en
				from
						w2 inner join (select * from target_data.c_area_domain_category where id in (select w2.etl_area_domain_category__adc[1] from w2)) as cadc
						on w2.etl_area_domain_category__adc[1] = cadc.id
				)
		,w4 as	(
				select
						json_build_object
							(
							'area_domain',w3.etl_area_domain__etl_id,
							'area_domain_category',w3.etl_area_domain_category__etl_id,
							'label',w3.label,
							'description',w3.description,
							'label_en',w3.label_en,
							'description_en',w3.description_en
							) as res
				from w3
				)
		select json_agg(w4.res) from w4
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domain_categories4area_domains(integer) IS
'The funcions returs JSON of area domain categories that had already been ETLed for given export connection.';

grant execute on function target_data.fn_etl_get_area_domain_categories4area_domains(integer) to public;