--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_categories4update
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domain_categories4update(integer,integer) CASCADE;

create or replace function target_data.fn_etl_get_area_domain_categories4update
(
	_etl_area_domain	integer,
	_area_domain_target	integer
)
returns json
as
$$
declare
		_res	json;
begin
		if _etl_area_domain is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_categories4update: Input argument _etl_area_domain must not be null!';
		end if;

		if _area_domain_target is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_categories4update: Input argument _area_domain_target must not be null!';
		end if;
		-------------------------------------------------------------
		if	(
			select array_length(tead.area_domain,1) != 1
			from target_data.t_etl_area_domain as tead
			where tead.id = _etl_area_domain
			)
		then
			raise exception 'Error 03: fn_etl_get_area_domain_categories4update: For input argument _etl_area_domain = % is not area domain as ATOMIC type in t_etl_area_domain table!',_etl_area_domain;
		end if;
		-------------------------------------------------------------
		with
		w1 as	(
				select
						teadc.id,
						teadc.area_domain_category,
						teadc.etl_id,
						teadc.etl_area_domain
				from
						target_data.t_etl_area_domain_category as teadc
				where
						teadc.etl_area_domain = _etl_area_domain
				)
		,w2 as	(
				select
						w1.*,
						cadc.label,
						cadc.description,
						cadc.label_en,
						cadc.description_en
				from
						w1 inner join target_data.c_area_domain_category as cadc on w1.area_domain_category[1] = cadc.id
				)
		,w3 as	(
				select
						json_build_object
							(
							'area_domain',_area_domain_target,
							'area_domain_category',w2.etl_id,
							'label',w2.label,
							'description',w2.description,
							'label_en',w2.label_en,
							'description_en',w2.description_en
							) as res
				from w2
				)
		select json_agg(w3.res) from w3
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domain_categories4update(integer, integer) IS
'The funcions returs list of area domain categories that had already been ETLed for given area domain.';

grant execute on function target_data.fn_etl_get_area_domain_categories4update(integer, integer) to public;