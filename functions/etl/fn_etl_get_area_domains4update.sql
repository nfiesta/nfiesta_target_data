--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domains4update
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domains4update(integer) CASCADE;

create or replace function target_data.fn_etl_get_area_domains4update
(
	_export_connection	integer
)
returns json
as
$$
declare
		_res	json;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_area_domains4update: Input argument _export_connection must not be null!';
		end if;
		-------------------------------------------------------------
		with
		w1 as	(
				select
						tead.id,
						tead.export_connection,
						tead.area_domain,
						tead.etl_id
				from
						target_data.t_etl_area_domain as tead
				where
						tead.export_connection = _export_connection
				and
						array_length(tead.area_domain,1) = 1
				)
		,w2 as	(
				select
						w1.id,
						w1.area_domain,
						w1.etl_id,
						cad.label,
						cad.description,
						cad.label_en,
						cad.description_en
				from
						w1 inner join target_data.c_area_domain as cad on w1.area_domain[1] = cad.id
				)
		,w3 as	(
				select
						json_build_object
							(
							'area_domain',w2.etl_id,
							'label',w2.label,
							'description',w2.description,
							'label_en',w2.label_en,
							'description_en',w2.description_en
							) as res
				from w2
				)
		select json_agg(w3.res) from w3
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domains4update(integer) IS
'The funcions returs list of area domains that had already been ETLed for given export connection.';

grant execute on function target_data.fn_etl_get_area_domains4update(integer) to public;