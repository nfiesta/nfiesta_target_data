--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_etl_id4target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_etl_id4target_variable(integer) CASCADE;

create or replace function target_data.fn_etl_get_etl_id4target_variable
(
	_id_t_etl_target_variable	integer
)
returns integer
as
$$
declare
		_etl_id		integer;
begin
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_etl_id4target_variable: Input argument _id_t_etl_target_variable must not by NULL!';
		end if; 
		
		select etl_id from target_data.t_etl_target_variable
		where id = _id_t_etl_target_variable
		into _etl_id;
	
		return _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_etl_id4target_variable(integer) is
'Function returns record ETL_ID from table t_etl_target_variable based on given parameters.';

grant execute on function target_data.fn_etl_get_etl_id4target_variable(integer) to public;