--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_etl_ids_of_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_etl_ids_of_area_domain(integer) CASCADE;

create or replace function target_data.fn_etl_get_etl_ids_of_area_domain
(
	_export_connection	integer
)
returns integer[]
as
$$
declare
	_res integer[];
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_etl_ids_of_area_domain: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from target_data.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_etl_ids_of_area_domain: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		select array_agg(tead.etl_id order by tead.etl_id)
		from target_data.t_etl_area_domain as tead 
		where tead.export_connection = _export_connection
		into _res;
	
		return _res;	
end;
$$

language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_etl_ids_of_area_domain(integer) is
'The function gets an array of etl_ids for area domain combinations that was ETLed yet.';

grant execute on function target_data.fn_etl_get_etl_ids_of_area_domain(integer) to public;