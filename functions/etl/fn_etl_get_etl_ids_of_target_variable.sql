--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_etl_ids_of_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_etl_ids_of_target_variable(integer) CASCADE;

create or replace function target_data.fn_etl_get_etl_ids_of_target_variable
(
	_export_connection	integer
)
returns integer[]
as
$$
declare
	_res integer[];
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_etl_ids_of_target_variable: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from target_data.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_etl_ids_of_target_variable: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		select array_agg(tetv.etl_id order by tetv.etl_id)
		from target_data.t_etl_target_variable as tetv
		where tetv.export_connection = _export_connection
		into _res;
	
		return _res;	
end;
$$

language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_etl_ids_of_target_variable(integer) is
'The function gets an array of etl_ids for target variables that was ETLed yet.';

grant execute on function target_data.fn_etl_get_etl_ids_of_target_variable(integer) to public;