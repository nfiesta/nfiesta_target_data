--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_export_connections
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_export_connections() CASCADE;

create or replace function target_data.fn_etl_get_export_connections()
returns table
(
	id				integer,
	host			character varying,
	dbname			character varying,
	port			integer,
	comment			text
)
as
$$
begin
	return query
	select t.id, t.host, t.dbname, t.port, t.comment
	from target_data.t_export_connection as t
	order by t.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_export_connections() is
'The function returns records from t_export_connection table.';

grant execute on function target_data.fn_etl_get_export_connections() to public;