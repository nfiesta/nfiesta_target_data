--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_list_of_area_domains4update
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_list_of_area_domains4update(integer, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_list_of_area_domains4update
(
	_export_connection	integer,
	_area_domain_target	integer[]
)
returns table
(
	etl_area_domain		integer,
	area_domain_target	integer,
	label				varchar,
	description			text,
	label_en			varchar,
	description_en		text
)
as
$$
declare
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_list_of_area_domains4update: Input argument _export_connection must not be null!';
		end if;

		if _area_domain_target is null
		then
			raise exception 'Error 02: fn_etl_get_list_of_area_domains4update: Input argument _area_domain_target must not be null!';
		end if;		
		-------------------------------------------------------------
		return query
		with
		w1 as	(
				select
						tead.id,
						tead.export_connection,
						tead.area_domain,
						tead.etl_id
				from
						target_data.t_etl_area_domain as tead
				where
						tead.export_connection = _export_connection
				and
						array_length(tead.area_domain,1) = 1
				and
						tead.etl_id in (select unnest(_area_domain_target))
				)
		,w2 as	(
				select
						w1.id,
						w1.area_domain,
						w1.etl_id,
						cad.label,
						cad.description,
						cad.label_en,
						cad.description_en
				from
						w1 inner join target_data.c_area_domain as cad on w1.area_domain[1] = cad.id
				)
		select
				w2.id as etl_area_domain,
				w2.etl_id as area_domain_target,
				w2.label::varchar,
				w2.description,
				w2.label_en::varchar,
				w2.description_en
		from
				w2;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_list_of_area_domains4update(integer, integer[]) IS
'The funcions returs list of area domains that had already been ETLed and some of their label or description is difference between source and target DB for given export connection.';

grant execute on function target_data.fn_etl_get_list_of_area_domains4update(integer, integer[]) to public;