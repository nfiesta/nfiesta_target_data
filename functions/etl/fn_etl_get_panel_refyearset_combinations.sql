--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_panel_refyearset_combinations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_panel_refyearset_combinations(integer,integer[],integer[]) CASCADE;

create or replace function target_data.fn_etl_get_panel_refyearset_combinations
(
	_id_t_etl_target_variable			integer,
	_refyearset2panel_mapping			integer[],
	_refyearset2panel_mapping_module	integer[] default null::integer[]
)
returns table
(
	refyearset2panel		integer,
	panel					varchar,	
	reference_year_set		varchar,
	passed_by_module		boolean
)
as
$$
declare
		_res_panels_and_reference_year_sets		json;
		_country_array							integer[];
		_res_country							varchar;
		_target_variable						integer;
		_etl_id									integer;
		_categorization_setups					integer[];
		_res									json;
		_check_rys2pm							integer;
begin		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_panel_refyearset_combinations: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;

		select
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_target_variable;

		if _target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_panel_refyearset_combinations: For input argument _id_t_etl_target_variable = % not exists any target_variable in table t_etl_target_variable!',_id_t_etl_target_variable;
		end if;

		if _refyearset2panel_mapping_module is not null and _refyearset2panel_mapping is null
		then
			raise exception 'Error 03: fn_etl_get_panel_refyearset_combinations: If input argument _refyearset2panel_mapping_module is not null then input argument _refyearset2panel_mapping must be not null!';
		end if;

		if _refyearset2panel_mapping_module is not null
		then
			with
			w1 as	(select unnest(_refyearset2panel_mapping) as rys2pm)
			,w2 as	(select unnest(_refyearset2panel_mapping_module) as rys2pm)
			,w3 as	(
					select w2.rys2pm from w2 except
					select w1.rys2pm from w1
					)
			select count(w3.rys2pm) from w3
			into
				_check_rys2pm;

			if _check_rys2pm is distinct from 0
			then
				raise exception 'Error 04: fn_etl_get_panel_refyearset_combinations: Some of values in input argument _refyearset2panel_mapping_module is not contains in input argument _refyearset2panel_mapping!';
			end if;
		end if;

		if _refyearset2panel_mapping is null and _refyearset2panel_mapping_module is not null
		then
			raise exception 'Error 05: fn_etl_get_panel_refyearset_combinations: If input argument _refyearset2panel_mapping is null then input argument _refyearset2panel_mapping_module must be null!';
		end if;


		if _refyearset2panel_mapping is null
		then
			return query
			with
			w1 as	(
					select tad.* from target_data.t_available_datasets as tad where tad.categorization_setup in
						(
						select distinct cmlcs.categorization_setup
						from target_data.cm_ldsity2target2categorization_setup as cmlcs
						where cmlcs.ldsity2target_variable in
							(
							select cmltv.id from target_data.cm_ldsity2target_variable as cmltv
							where cmltv.target_variable = _target_variable
							)
						)
					)
			,w2 as	(
					select distinct tlv.available_datasets from target_data.t_ldsity_values as tlv
					where tlv.available_datasets in (select w1.id from w1)
					and is_latest = true
					)
			,w3 as	(
					select distinct tad1.panel, tad1.reference_year_set
					from target_data.t_available_datasets as tad1 where tad1.id in
					(select w2.available_datasets from w2)
					)
			,w4 as	(
					select
							w3.panel as panel_id,
							w3.reference_year_set as reference_year_set_id,
							crpm.id as refyearset2panel,
							tp.panel,
							trys.reference_year_set from w3
					inner join sdesign.cm_refyearset2panel_mapping as crpm on w3.panel = crpm.panel and w3.reference_year_set = crpm.reference_year_set
					inner join sdesign.t_panel as tp on w3.panel = tp.id
					inner join sdesign.t_reference_year_set as trys on w3.reference_year_set = trys.id
					)
			select
					w4.refyearset2panel,
					w4.panel,
					w4.reference_year_set,
					false as passed_by_module
			from
					w4 order by w4.refyearset2panel;
		else
			return query
			with
			w1 as	(
					select cm.* from sdesign.cm_refyearset2panel_mapping as cm
					where cm.id in (select unnest(_refyearset2panel_mapping))
					)
			,w2 as	(
					select
							w1.id as refyearset2panel,
							w1.panel as panel_id,
							w1.reference_year_set as reference_year_set_id,
							tp.panel,
							trys.reference_year_set
					from
							w1
							inner join sdesign.t_panel as tp on w1.panel = tp.id
							inner join sdesign.t_reference_year_set as trys on w1.reference_year_set = trys.id
					)
			,w3 as	(
					select unnest(_refyearset2panel_mapping_module) as rys2pm
					)
			select
					w2.refyearset2panel,
					w2.panel,
					w2.reference_year_set,
					case when w3.rys2pm is not null then true else false end as passed_by_module
			from
					w2
					left join w3 on w2.refyearset2panel = w3.rys2pm
			order
					by w2.refyearset2panel;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_panel_refyearset_combinations(integer, integer[], integer[]) is
'Function returns list of combinations of panels and reference year sets for given target variable that are available for ETL.';

grant execute on function target_data.fn_etl_get_panel_refyearset_combinations(integer, integer[], integer[]) to public;