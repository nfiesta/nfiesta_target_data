--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_panel_refyearset_combinations_false
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_panel_refyearset_combinations_false(JSON, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_panel_refyearset_combinations_false
(
	_variables					JSON,
	_refyearset2panel_mapping	integer[]
)
returns table
(
	refyearset2panel		integer,
	panel					varchar,	
	reference_year_set		varchar,
	useable4etl				boolean
)
as
$$
declare
begin		
		if _variables is null
		then
			raise exception 'Error 01: fn_etl_get_panel_refyearset_combinations_false: Input argument _variables must not be NULL!';
		end if;

		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 02: fn_etl_get_panel_refyearset_combinations_false: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;

		return query
		with
		w1 as	(
				select cm.* from sdesign.cm_refyearset2panel_mapping as cm
				where cm.id in (select unnest(_refyearset2panel_mapping))
				)
		,w2 as	(
				select
						w1.id as refyearset2panel,
						w1.panel as panel_id,
						w1.reference_year_set as reference_year_set_id,
						tp.panel,
						trys.reference_year_set
				from
						w1
						inner join sdesign.t_panel as tp on w1.panel = tp.id
						inner join sdesign.t_reference_year_set as trys on w1.reference_year_set = trys.id
				)
		,w3 as	(
				select json_array_elements(_variables) as s
				)
		,w4 as	(
				select
						(s->>'panel')::varchar					as panel,
						(s->>'reference_year_set')::varchar		as reference_year_set
				from w3
				)
		,w5 as	(
				select distinct w4.panel, w4.reference_year_set from w4
				)
		select
				w2.refyearset2panel,
				w2.panel,
				w2.reference_year_set,
				case when w5.panel is not null and w5.reference_year_set is not null then false else true end as useable4etl
		from
				w2
				left join w5 on w2.panel = w5.panel and w2.reference_year_set = w5.reference_year_set
		order
				by w2.refyearset2panel;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_panel_refyearset_combinations_false(JSON, integer[]) is
'Function returns list of combinations of panels and reference year sets that user wants to for ETL and is added information that given combination of panel a reference year set is useable to ETL.';

grant execute on function target_data.fn_etl_get_panel_refyearset_combinations_false(JSON, integer[]) to public;