--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_categories4update
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_sub_population_categories4update(integer,integer) CASCADE;

create or replace function target_data.fn_etl_get_sub_population_categories4update
(
	_etl_sub_population		integer,
	_sub_population_target	integer
)
returns json
as
$$
declare
		_res	json;
begin
		if _etl_sub_population is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_categories4update: Input argument _etl_sub_population must not be null!';
		end if;

		if _sub_population_target is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_categories4update: Input argument _sub_population_target must not be null!';
		end if;
		-------------------------------------------------------------
		if	(
			select array_length(tesp.sub_population,1) != 1
			from target_data.t_etl_sub_population as tesp
			where tesp.id = _etl_sub_population
			)
		then
			raise exception 'Error 03: fn_etl_get_sub_population_categories4update: For input argument _etl_sub_population = % is not area domain as ATOMIC type in t_etl_sub_population table!',_etl_sub_population;
		end if;
		-------------------------------------------------------------		
		with
		w1 as	(
				select
						tespc.id,
						tespc.sub_population_category,
						tespc.etl_id,
						tespc.etl_sub_population
				from
						target_data.t_etl_sub_population_category as tespc
				where
						tespc.etl_sub_population = _etl_sub_population
				)
		,w2 as	(
				select
						w1.*,
						cspc.label,
						cspc.description,
						cspc.label_en,
						cspc.description_en
				from
						w1 inner join target_data.c_sub_population_category as cspc on w1.sub_population_category[1] = cspc.id
				)
		,w3 as	(
				select
						json_build_object
							(
							'sub_population',_sub_population_target,
							'sub_population_category',w2.etl_id,
							'label',w2.label,
							'description',w2.description,
							'label_en',w2.label_en,
							'description_en',w2.description_en
							) as res
				from w2
				)
		select json_agg(w3.res) from w3
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_sub_population_categories4update(integer, integer) IS
'The funcions returs list of sub population categories that had already been ETLed for given sub population.';

grant execute on function target_data.fn_etl_get_sub_population_categories4update(integer, integer) to public;