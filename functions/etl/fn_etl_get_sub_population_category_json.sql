--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_category_json
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_sub_population_category_json(integer[], integer, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_sub_population_category_json
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_sub_population				integer[]
)
returns json
as
$$
declare
		_res json;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_category_json: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_category_json: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;	
	
		if _sub_population is null
		then
			raise exception 'Error 03: fn_etl_get_sub_population_category_json: Input argument _sub_population must not be NULL!';
		end if;
	
		with
		w1 as	(
				select
						id,
						sub_population,
						label_en_type,
						sub_population_category,
						label,
						description,
						label_en,
						description_en			
				from
						target_data.fn_etl_get_sub_population_category(_refyearset2panel_mapping,_id_t_etl_target_variable,_sub_population)				
				)
		select
				json_agg(
				json_build_object
				(
				'id',w1.id,
				'label_en_type',w1.label_en_type,
				'sub_population_category',w1.sub_population_category,
				'label_en',w1.label_en,
				'label',w1.label
				)
				) from w1
		into _res;
	
		if _res is null
		then
			raise exception 'Error 04: fn_etl_get_sub_population_category_json: Output argument _res must not be NULL!';
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_sub_population_category_json(integer[], integer, integer[]) is
'Function returns records for ETL c_sub_population_category table for given sub population types in JSON format.';

grant execute on function target_data.fn_etl_get_sub_population_category_json(integer[], integer, integer[]) to public;