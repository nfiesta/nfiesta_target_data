--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_json
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_sub_population_json(integer[], integer, boolean) CASCADE;

create or replace function target_data.fn_etl_get_sub_population_json
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_etl						boolean default null::boolean
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
		_cond						text;
		_res						json;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_json: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_json: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;		

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
			
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.spc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))
													)
				and
						t.spc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			with
			w1 as	(
					select
							0 as id,
							0 as export_connection,
							array[0] as sub_population,
							''::varchar as type_label,
							''::text as type_description,
							''::varchar as type_label_en,
							''::text as type_description_en,
							array[0] as sub_population_category,
							''::varchar as category_label,
							''::text as category_description,
							''::varchar as category_label_en,
							''::text as category_description_en,							
							0 as etl_id,
							0 as id_t_etl_sp,
							null::boolean as atomic
					)
			,w2 as	(
					select w1.id, w1.export_connection,
					w1.sub_population, w1.type_label, w1.type_description, w1.type_label_en, w1.type_description_en,
					w1.sub_population_category, w1.category_label, w1.category_description, w1.category_label_en, w1.category_description_en,
					w1.etl_id, w1.id_t_etl_sp, w1.atomic
					from w1 where w1.id is distinct from 0
					)
			select json_agg(
			json_build_object
				(
				'id',w2.id,
				'sub_population',w2.sub_population,
				'label_en_type',w2.type_label_en,
				'sub_population_category',w2.sub_population_category,
				'label_en_category',w2.category_label_en
				))
			from w2 into _res;
		else
			if _etl is null
			then
				_cond := 'TRUE'::text;
			else
				if _etl = true
				then
					_cond := 'w10.etl_id is not null'::text;
				else
					_cond := 'w10.etl_id is null'::text;
				end if;
			end if;
		
			execute
			'
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.spc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).label as type_label,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).description as type_description,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).label_en as type_label_en,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).description_en as type_description_en,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).id_category,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).label as category_label,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).description as category_description,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).label_en as category_label_en,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).description_en as category_description_en
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest($2))								
														)
					and
							t.spc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.type_label, w1.type_description, w1.type_label_en, w1.type_description_en, w1.id_category, w1.category_label, w1.category_description, w1.category_label_en, w1.category_description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select array[csp.id] as id_type, csp.label as type_label, csp.description as type_description, csp.label_en as  type_label_en, csp.description_en as type_description_en
					from target_data.c_sub_population as csp
					where csp.id in (select w4.id_type[1] from w4)
					)
			,w6 as (
					select
							w5.*,
							array[t.id] as id_category,
							t.label as category_label,
							t.description as category_description,
							t.label_en as category_label_en,
							t.description_en as category_description_en
					from
							w5
					inner join
							(
							select cspc.* from target_data.c_sub_population_category as cspc
							where cspc.sub_population in (select w5.id_type[1] from w5)
							) as t 
					on
							w5.id_type[1] = t.sub_population
					)			
			,w7 as	(
					select w2.id_type, w2.type_label, w2.type_description, w2.type_label_en, w2.type_description_en, w2.id_category, w2.category_label, w2.category_description, w2.category_label_en, w2.category_description_en from w2 union all
					select w6.id_type, w6.type_label, w6.type_description, w6.type_label_en, w6.type_description_en, w6.id_category, w6.category_label, w6.category_description, w6.category_label_en, w6.category_description_en from w6
					)
			,w8 as	(
					select
							$1 as export_connection,
							w7.id_type as sub_population,
							w7.type_label,
							w7.type_description,
							w7.type_label_en,
							w7.type_description_en,
							w7.id_category as sub_population_category,
							w7.category_label,
							w7.category_description,
							w7.category_label_en,
							w7.category_description_en							
					from
							w7
					)
			,w9 as	(
					select
							w8.export_connection,
							w8.sub_population,
							w8.type_label,
							w8.type_description,
							w8.type_label_en,
							w8.type_description_en,
							w8.sub_population_category,
							w8.category_label,
							w8.category_description,
							w8.category_label_en,
							w8.category_description_en,								
							t.etl_id,
							t.id as id_t_etl_sp
					from
							w8 left join target_data.t_etl_sub_population as t
					on
							w8.export_connection = t.export_connection
					and	
							target_data.fn_etl_array_compare(w8.sub_population,t.sub_population)
					order
							by w8.sub_population, w8.sub_population_category
					)
			,w10 as (
					select 
							a.*,
							b.category_json
					from
							(select distinct w9.export_connection, w9.sub_population, w9. type_label, w9.type_description, w9.type_label_en, w9.type_description_en, w9.etl_id, w9.id_t_etl_sp from w9) as a
					inner join
							(
							select
									w9.sub_population,
									json_agg(json_build_object(''sub_population_category'',w9.sub_population_category,''label_en_category'',w9.category_label_en)) as category_json
							from
									w9 group by w9.sub_population
							) as b
					on
							a.sub_population = b.sub_population
					order
							by a.sub_population
					)
			,w11 as (
					select
							(row_number() over ())::integer as id_order,
							w10.export_connection,
							w10.sub_population,
							w10.type_label,
							w10.type_description,
							w10.type_label_en,
							w10.type_description_en,							
							w10.etl_id,
							w10.id_t_etl_sp,
							w10.category_json,
							case when array_length(w10.sub_population,1) = 1 then true else false end as atomic
					from
							w10 where '|| _cond ||'
					)
			select json_agg(
			json_build_object
				(
				''id'',w11.id_order,
				''sub_population'',w11.sub_population,
				''label_en_type'',w11.type_label_en,
				''category'',w11.category_json
				))
			from w11
			'
			using _export_connection, _categorization_setups
			into _res;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_sub_population_json(integer[], integer, boolean) is
'Function returns records for ETL c_sub_population table in JSON format.';

grant execute on function target_data.fn_etl_get_sub_population_json(integer[], integer, boolean) to public;