--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_target_variable_metadata
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable_metadata(integer, character varying) CASCADE;

create or replace function target_data.fn_etl_get_target_variable_metadata
(
	_target_variable	integer,
	_national_language	character varying(2) default 'en'::character varying(2)
)
returns json
as
$$
declare
		_s_or_ch_label								varchar;
		_s_or_ch_description						text;
		_s_or_ch_label_en							varchar;
		_s_or_ch_description_en						text;
		---------------------------------------------------
		_tv_label									varchar;
		_tv_description								text;
		_tv_label_en								varchar;
		_tv_description_en							text;
		---------------------------------------------------
		_unit_of_measure_array						integer[];
		_u_of_m_label								varchar;
		_u_of_m_description							text;
		_u_of_m_label_en							varchar;
		_u_of_m_description_en						text;
		---------------------------------------------------
		_array_id									integer[];
		---------------------------------------------------
		_i_cm_ldsity								integer;
		_i_cm_ldsity_object_type					integer;
		_i_cm_area_domain_category					integer[];
		_i_cm_sub_population_category				integer[];
		_i_cm_version								integer;
		_i_cm_use_negative							boolean;
		---------------------------------------------------
		_i_clot_label								varchar;
		_i_clot_description							text;
		_i_clot_label_en							varchar;
		_i_clot_description_en						text;
		---------------------------------------------------
		_i_cv_label									varchar;
		_i_cv_description							text;
		_i_cv_label_en								varchar;
		_i_cv_description_en						text;
		---------------------------------------------------
		_i_cl_ldsity_object							integer;
		_i_cl_label									varchar;
		_i_cl_description							text;
		_i_cl_label_en								varchar;
		_i_cl_description_en						text;
		_i_cl_definition_variant					integer[];
		_i_cl_area_domain_category					integer[];
		_i_cl_sub_population_category				integer[];
		---------------------------------------------------
		_i_clo_label								varchar;
		_i_clo_description							text;
		_i_clo_label_en								varchar;
		_i_clo_description_en						text;
		---------------------------------------------------
		_i_definition_variant						integer[];
		_i_area_domain_category						integer[];
		_i_sub_population_category					integer[];
		---------------------------------------------------
		_i_definition_variant_label					varchar[];
		_i_definition_variant_description			text[];
		_i_definition_variant_label_en				varchar[];
		_i_definition_variant_description_en		text[];
		---------------------------------------------------
		_i_area_domain_category_label				varchar[];
		_i_area_domain_category_description			text[];
		_i_area_domain_category_label_en			varchar[];
		_i_area_domain_category_description_en		text[];
		---------------------------------------------------
		_i_sub_population_category_label			varchar[];
		_i_sub_population_category_description		text[];
		_i_sub_population_category_label_en			varchar[];
		_i_sub_population_category_description_en	text[];
		---------------------------------------------------
		_i_object_json_nl							json;
		_objects_json_nl							json;
		_json_nl									json;
		---------------------------------------------------
		_i_object_json_en							json;
		_objects_json_en							json;
		_json_en									json;
		---------------------------------------------------
		_json_res									json;
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable_metadata: Input argument _target_variable must not by NULL!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_get_target_variable_metadata: Input argument _national_language must not by NULL!';
		end if;		
		---------------------------------------------------
		-- state or change
		---------------------------------------------------
		select
				label,
				description,
				label as label_en,
				description as description_en
		from
				target_data.c_state_or_change
		where
				id = (select state_or_change from target_data.c_target_variable where id = _target_variable)
		into
				_s_or_ch_label,
				_s_or_ch_description,
				_s_or_ch_label_en,
				_s_or_ch_description_en;
		---------------------------------------------------
		-- indicator
		---------------------------------------------------
		select
				label,
				description,
				label_en,
				description_en
		from
				target_data.c_target_variable
		where
				id = _target_variable
		into
				_tv_label,
				_tv_description,
				_tv_label_en,
				_tv_description_en;
		---------------------------------------------------
		-- unit of measure
		---------------------------------------------------
		select array_agg(unit_of_measure) from target_data.c_ldsity where id in
		(select ldsity from target_data.cm_ldsity2target_variable where target_variable = _target_variable and ldsity_object_type = 100)
		into _unit_of_measure_array;

		if	(
			select count(t2.res) is distinct from 1
			from (select distinct t1.res from (select unnest(_unit_of_measure_array) as res) as t1) as t2
			)
		then
			raise exception 'Error 02: fn_etl_get_target_variable_metadata: More different unit of measures for target variable = %!',_target_variable;
		end if;
	
		select
				label,
				description,
				label_en,
				description_en
		from
				target_data.c_unit_of_measure where id = _unit_of_measure_array[1]
		into
				_u_of_m_label,
				_u_of_m_description,
				_u_of_m_label_en,
				_u_of_m_description_en;
		---------------------------------------------------
		-- OBJECTS --	
		select array_agg(id order by ldsity_object_type, id)
		from target_data.cm_ldsity2target_variable where target_variable = _target_variable
		into _array_id;
		---------------------------------------------------
		-- cycle for objects [LOCAL DENSITIES]
		---------------------------------------------------
		for i in 1..array_length(_array_id,1)
		loop
			select
					ldsity,
					ldsity_object_type,
					area_domain_category,
					sub_population_category,
					version,
					use_negative
			from
					target_data.cm_ldsity2target_variable where id = _array_id[i]
			into
					_i_cm_ldsity,
					_i_cm_ldsity_object_type,
					_i_cm_area_domain_category,		-- can be NULL
					_i_cm_sub_population_category,	-- can be NULL
					_i_cm_version,					-- can be NULL
					_i_cm_use_negative;
			-------------------------------------
			-- ldsity_object_type
			-------------------------------------
			select
					label,
					description,
					label as label_en,
					description as description_en
			from
					target_data.c_ldsity_object_type
			where
					id = _i_cm_ldsity_object_type
			into
					_i_clot_label,
					_i_clot_description,
					_i_clot_label_en,
					_i_clot_description_en;
			-------------------------------------
			-- version
			-------------------------------------	
			select
					label,
					description,
					label_en,
					description_en
			from
					target_data.c_version where id = _i_cm_version -- case when _i_cm_version is null then zero rows is returned
			into
					_i_cv_label,			-- can be NULL
					_i_cv_description,		-- can be NULL
					_i_cv_label_en,			-- can be NULL
					_i_cv_description_en;	-- can be NULL
			-------------------------------------
			-- contribution
			-------------------------------------				
			select
					ldsity_object,
					label,
					description,
					label_en,
					description_en,
					definition_variant,
					area_domain_category,
					sub_population_category
			from
					target_data.c_ldsity where id = _i_cm_ldsity
			into
					_i_cl_ldsity_object,
					_i_cl_label,
					_i_cl_description,
					_i_cl_label_en,
					_i_cl_description_en,
					_i_cl_definition_variant,		-- can be NULL
					_i_cl_area_domain_category,		-- can be NULL
					_i_cl_sub_population_category;	-- can be NULL
			-------------------------------------
			-- object
			-------------------------------------
			select
					label,
					description,
					label_en,
					description_en
			from
					target_data.c_ldsity_object where id = _i_cl_ldsity_object
			into
					_i_clo_label,
					_i_clo_description,
					_i_clo_label_en,
					_i_clo_description_en;
			-------------------------------------		
			_i_definition_variant := _i_cl_definition_variant;												-- can be NULL
			_i_area_domain_category := _i_cl_area_domain_category || _i_cm_area_domain_category;			-- can be NULL
			_i_sub_population_category := _i_cl_sub_population_category || _i_cm_sub_population_category;	-- can be NULL
			-------------------------------------
			if _i_definition_variant is null
			then
					_i_definition_variant_label := null::varchar[];
					_i_definition_variant_description := null::text[];
					_i_definition_variant_label_en := null::varchar[];
					_i_definition_variant_description_en := null::text[];					
			else
					select
							array_agg(label order by label),
							array_agg(description order by label)
					from
							target_data.c_definition_variant where id in (select unnest(_i_definition_variant))
					into
							_i_definition_variant_label,
							_i_definition_variant_description;
						
					select
							array_agg(label_en order by label_en),
							array_agg(description_en order by label_en)
					from
							target_data.c_definition_variant where id in (select unnest(_i_definition_variant))
					into
							_i_definition_variant_label_en,
							_i_definition_variant_description_en;												
			end if;
			-------------------------------------
			if _i_area_domain_category is null
			then
					_i_area_domain_category_label := null::varchar[];
					_i_area_domain_category_description := null::text[];
					_i_area_domain_category_label_en := null::varchar[];
					_i_area_domain_category_description_en := null::text[];					
			else				
					select
							array_agg(label order by label),
							array_agg(description order by label)
					from
							target_data.c_area_domain_category
					where id in	(
								select area_domain_category from target_data.cm_adc2classification_rule where id in
								(select unnest(_i_area_domain_category))
								)
					into
							_i_area_domain_category_label,
							_i_area_domain_category_description;
						
					select
							array_agg(label_en order by label_en),
							array_agg(description_en order by label_en)
					from
							target_data.c_area_domain_category
					where id in	(
								select area_domain_category from target_data.cm_adc2classification_rule where id in
								(select unnest(_i_area_domain_category))
								)
					into
							_i_area_domain_category_label_en,
							_i_area_domain_category_description_en;				
			end if;
			-------------------------------------
			if _i_sub_population_category is null
			then
					_i_sub_population_category_label := null::varchar[];
					_i_sub_population_category_description := null::text[];
					_i_sub_population_category_label_en := null::varchar[];
					_i_sub_population_category_description_en := null::text[];					
			else				
					select
							array_agg(label order by label),
							array_agg(description order by label)
					from
							target_data.c_sub_population_category
					where id in	(
								select sub_population_category from target_data.cm_spc2classification_rule where id in
								(select unnest(_i_sub_population_category))
								)
					into
							_i_sub_population_category_label,
							_i_sub_population_category_description;
						
					select
							array_agg(label_en order by label_en),
							array_agg(description_en order by label_en)
					from
							target_data.c_sub_population_category
					where id in	(
								select sub_population_category from target_data.cm_spc2classification_rule where id in
								(select unnest(_i_sub_population_category))
								)
					into
							_i_sub_population_category_label_en,
							_i_sub_population_category_description_en;				
			end if;
			-------------------------------------
			-------------------------------------
			-- get JSON for i- local density

			-- national language
			select json_build_object(
									'object type label',_i_clot_label,				-- c_ldsity_object_type
									'object type description',_i_clot_description,	-- c_ldsity_object_type
									'object label',_i_clo_label,					-- c_ldsity_object
									'object description',_i_clo_description,		-- c_ldsity_object
									'label',_i_cl_label,							-- c_ldsity
									'description',_i_cl_description,				-- c_ldsity
									'version',json_build_object	(
																'label',_i_cv_label,
																'description',_i_cv_description
																),
									'definition variant',json_build_object	(
																			'label',_i_definition_variant_label,
																			'description',_i_definition_variant_description
																			),
									'area domain',json_build_object	(
																	'label',_i_area_domain_category_label,
																	'description',_i_area_domain_category_description
																	),
									'population',json_build_object	(
																	'label',_i_sub_population_category_label,
																	'description',_i_sub_population_category_description
																	),
									'use_negative',_i_cm_use_negative
									)
			into _i_object_json_nl;

			-- english language
			select json_build_object(
									'object type label',_i_clot_label_en,				-- c_ldsity_object_type
									'object type description',_i_clot_description_en,	-- c_ldsity_object_type
									'object label',_i_clo_label_en,						-- c_ldsity_object
									'object description',_i_clo_description_en,			-- c_ldsity_object
									'label',_i_cl_label_en,								-- c_ldsity
									'description',_i_cl_description_en,					-- c_ldsity
									'version',json_build_object	(
																'label',_i_cv_label_en,
																'description',_i_cv_description_en
																),
									'definition variant',json_build_object	(
																			'label',_i_definition_variant_label_en,
																			'description',_i_definition_variant_description_en
																			),
									'area domain',json_build_object	(
																	'label',_i_area_domain_category_label_en,
																	'description',_i_area_domain_category_description_en
																	),
									'population',json_build_object	(
																	'label',_i_sub_population_category_label_en,
																	'description',_i_sub_population_category_description_en
																	),
									'use_negative',_i_cm_use_negative
									)
			into _i_object_json_en;
			-------------------------------------
			-------------------------------------
			if i = 1
			then
					select json_agg(_i_object_json_nl)
					into _objects_json_nl;
				
					select json_agg(_i_object_json_en)
					into _objects_json_en;				
			else
					with
					w1 as	(
							select
									row_number() over () as new_id,
									json_array_elements(_objects_json_nl) as res
							)
					,w2 as	(
							select
									(row_number() over ()) + (select max(new_id) from w1) as new_id,
									_i_object_json_nl as res
							)
					,w3 as	(
							select new_id, res from w1 union all
							select new_id, res from w2
							)
					select json_agg(w3.res order by w3.new_id) from w3
					into _objects_json_nl;
				
					with
					w1 as	(
							select
									row_number() over () as new_id,
									json_array_elements(_objects_json_en) as res
							)
					,w2 as	(
							select
									(row_number() over ()) + (select max(new_id) from w1) as new_id,
									_i_object_json_en as res
							)
					,w3 as	(
							select new_id, res from w1 union all
							select new_id, res from w2
							)
					select json_agg(w3.res order by w3.new_id) from w3
					into _objects_json_en;				
			end if;					
		end loop;
		-------------------------------------
		select json_build_object
			(
			'indicator', json_build_object('label',_tv_label,'description',_tv_description),
			'state or change', json_build_object('label',_s_or_ch_label,'description',_s_or_ch_description),
			'unit',json_build_object('label',_u_of_m_label,'description',_u_of_m_description),
			'local densities',_objects_json_nl
			)
		into _json_nl;
		-------------------------------------
		select json_build_object
			(
			'indicator', json_build_object('label',_tv_label_en,'description',_tv_description_en),
			'state or change', json_build_object('label',_s_or_ch_label_en,'description',_s_or_ch_description_en),
			'unit',json_build_object('label',_u_of_m_label_en,'description',_u_of_m_description_en),
			'local densities',_objects_json_en
			)
		into _json_en;
		-------------------------------------
		if _national_language = 'en'
		then
			select json_build_object
				(
				'en',_json_en
				)
			into _json_res;		
		else
			select json_build_object
				(
				_national_language,_json_nl,	-- set national language from input argument
				'en',_json_en
				)
			into _json_res;
		end if;
		-------------------------------------
	return _json_res;
	---------------------------------------------
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_target_variable_metadata(integer, character varying) IS
'The function returs metadata for input target variable.';

grant execute on function target_data.fn_etl_get_target_variable_metadata(integer, character varying) to public;