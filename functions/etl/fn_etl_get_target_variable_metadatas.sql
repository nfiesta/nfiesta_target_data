--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_target_variable_metadatas
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable_metadatas(integer[], character varying) CASCADE;

create or replace function target_data.fn_etl_get_target_variable_metadatas
(
	_id_etl_target_variable	integer[],
	_national_language		character varying(2) default 'en'::character varying(2)
)
returns json
as
$$
declare
		_json_res	json;
begin
		if _id_etl_target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable_metadatas: Input argument _id_etl_target_variable must not by NULL!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_get_target_variable_metadatas: Input argument _national_language must not by NULL!';
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select tetv.* from target_data.t_etl_target_variable as tetv
				where tetv.id in (select unnest(_id_etl_target_variable))
				)
		,w2 as	(
				select
						w1.target_variable,	-- id of target variable from source DB
						w1.etl_id,			-- id of target variable that is saved in target DB
						target_data.fn_etl_get_target_variable_metadata(w1.target_variable,_national_language) as metadata
				from
						w1 order by w1.target_variable
				)
		select
			json_agg(
			json_build_object
			(
			'target_variable',w2.target_variable,
			'etl_id',w2.etl_id,
			'metadata',w2.metadata
			))
		from
			w2
		into
			_json_res;
		-------------------------------------
		return _json_res;
		-------------------------------------
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_target_variable_metadatas(integer[], character varying) IS
'The function returs metadata for input target variables.';

grant execute on function target_data.fn_etl_get_target_variable_metadatas(integer[], character varying) to public;