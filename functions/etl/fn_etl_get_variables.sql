--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_variables
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_variables(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_get_variables
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns json
as
$$
declare
		_country_array							integer[];
		_res_country							varchar;
		_export_connection						integer;
		_target_variable						integer;
		_etl_id									integer;
		_res									json;

		_core_and_division						boolean;
		_check_variables						integer;
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_variables: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_variables: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;

		select array_agg(tss.country order by tss.country) from sdesign.t_strata_set as tss where tss.id in
		(select strata_set from sdesign.t_stratum where id in
		(select stratum from sdesign.t_panel
		where id in ((select panel from sdesign.cm_refyearset2panel_mapping where id in (select unnest(array[_refyearset2panel_mapping]))))))
		into _country_array;
	
		if array_length(_country_array,1) is distinct from 1
		then
			raise exception 'Error 03: fn_etl_get_variables: Input argument _refyearset2panel_mapping contains values for two or more different countries!';
		end if;
	
		select label from sdesign.c_country where id = _country_array[1]
		into _res_country;
	
		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w_etl_ad as		(
						select * from target_data.t_etl_area_domain	where export_connection = _export_connection
						)
		,w_etl_adc as	(
						select * from target_data.t_etl_area_domain_category where etl_area_domain in (select id from w_etl_ad)
						)
		,w_etl_sp as	(
						select * from target_data.t_etl_sub_population where export_connection = _export_connection
						)
		,w_etl_spc as	(
						select * from target_data.t_etl_sub_population_category where etl_sub_population in (select id from w_etl_sp)
						)				
		,w1 as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups for target variable that is reduced by
				 -- combination of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(-- list of IDs of available datasets from t_ldsity_values table
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select w2.* from w2 where w2.id in (select w3.available_datasets from w3)
				)
		,w5 as	(
				select
						w4.panel,
						w4.reference_year_set,
						array_agg(w4.categorization_setup order by w4.categorization_setup) as categorization_setup
				from
						w4 group by w4.panel, w4.reference_year_set
				)
		,w6 as	(
				select
						w5.*,
						target_data.fn_etl_supplement_categorization_setups(w5.categorization_setup) as categorization_setup_supplement
				from
						w5
				)
		,w7 as	(
				select
						w6.panel,
						w6.reference_year_set,
						unnest(w6.categorization_setup_supplement) as categorization_setup_supplement
				from
						w6
				)
		,w8 as	(
				select
					id,
					ldsity2target_variable,
					categorization_setup,
					_core_and_division as core_and_division,				
					(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type as id_spt_orig,
					(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type as id_adt_orig,
					(target_data.fn_get_category4classification_rule_id('spc', t.spc2classification_rule)).id_category as id_spc_orig,
					(target_data.fn_get_category4classification_rule_id('adc', t.adc2classification_rule)).id_category as id_adc_orig,
					(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_spt_orig,
					(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_adt_orig,
					(target_data.fn_get_category4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_spc_orig,
					(target_data.fn_get_category4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_adc_orig
				from
					target_data.cm_ldsity2target2categorization_setup as t
				where
					ldsity2target_variable
				in
						(
						select id from target_data.cm_ldsity2target_variable
						where target_variable = _target_variable
						)
				and categorization_setup in (select distinct w7.categorization_setup_supplement from w7) -- vyber kategorizacnich setupu po supplementu
				)
		,w9 as	(
				select
						w8.*,
						cltv.ldsity_object_type as core_or_division
				from
						w8
						inner join target_data.cm_ldsity2target_variable as cltv on w8.ldsity2target_variable = cltv.id
				)
		,w10a as	(select * from w9 where core_and_division = false)
		,w10b as	(select * from w9 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w9 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, label_spt_orig, label_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, label_adt_orig, label_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni,
							w11a.label_spt_orig as label_spt_orig_doplneni,
							w11a.label_spc_orig as label_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni,
							w11b.label_adt_orig as label_adt_orig_doplneni,
							w11b.label_adc_orig as label_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					label_spt_orig as label_spt, 
					label_adt_orig as label_adt,
					label_spc_orig as label_spc,
					label_adc_orig as label_adc,
					core_or_division
					from w10a
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					label_spt_orig as label_spt, 
					label_adt_orig_doplneni as label_adt,
					label_spc_orig as label_spc,
					label_adc_orig_doplneni as label_adc,
					core_or_division
					from w12b
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					label_spt_orig_doplneni as label_spt,
					label_adt_orig as label_adt,
					label_spc_orig_doplneni as label_spc,
					label_adc_orig as label_adc,
					core_or_division
					from w12a
					)
		,w14 as	(
				select distinct categorization_setup, id_spt, id_adt, id_spc, id_adc, label_spt, label_adt, label_spc, label_adc from w13 order by categorization_setup
				)
		,w15a as	(
					select t1.id_spt, w_etl_sp.etl_id as etl_id_spt from
					(select distinct id_spt from w14 where id_spt is not null) as t1
					inner join w_etl_sp
					on (t1.id_spt @> w_etl_sp.sub_population and t1.id_spt <@ w_etl_sp.sub_population)
					)
		,w15b as	(
					select t2.id_adt, w_etl_ad.etl_id as etl_id_adt from
					(select distinct id_adt from w14 where id_adt is not null) as t2
					inner join w_etl_ad
					on (t2.id_adt @> w_etl_ad.area_domain and t2.id_adt <@ w_etl_ad.area_domain)
					)
		,w15c as	(
					select t3.id_spc, w_etl_spc.etl_id as etl_id_spc from
					(select distinct id_spc from w14 where id_spc is not null) as t3
					inner join w_etl_spc
					on (t3.id_spc @> w_etl_spc.sub_population_category and t3.id_spc <@ w_etl_spc.sub_population_category)
					)
		,w15d as	(
					select t4.id_adc, w_etl_adc.etl_id as etl_id_adc from
					(select distinct id_adc from w14 where id_adc is not null) as t4
					inner join w_etl_adc
					on (t4.id_adc @> w_etl_adc.area_domain_category and t4.id_adc <@ w_etl_adc.area_domain_category)
					)
		,w15 as	(
				select
						w14.*,
						w15a.etl_id_spt,
						w15b.etl_id_adt,
						w15c.etl_id_spc,
						w15d.etl_id_adc
				from
						w14
						left join w15a on w14.id_spt = w15a.id_spt
						left join w15b on w14.id_adt = w15b.id_adt
						left join w15c on w14.id_spc = w15c.id_spc
						left join w15d on w14.id_adc = w15d.id_adc
				)
		,w16 as	(
				select
						categorization_setup,
						label_spt,
						label_spc,
						label_adt,
						label_adc,
						etl_id_spt,
						etl_id_spc,
						etl_id_adt,
						etl_id_adc,		
						case when etl_id_spt is null then 0 else etl_id_spt end as etl_id_spt__not_null,
						case when etl_id_spc is null then 0 else etl_id_spc end as etl_id_spc__not_null,
						case when etl_id_adt is null then 0 else etl_id_adt end as etl_id_adt__not_null,
						case when etl_id_adc is null then 0 else etl_id_adc end as etl_id_adc__not_null
				from
						w15
				)
		,w17 as	(
				select
						w16.*,
						---------------------------------------------------------------------------
						case
							when label_spt is     null and etl_id_spt is     null then true
							when label_spt is     null and etl_id_spt is not null then false -- this variant is impossibe to be
							when label_spt is not null and etl_id_spt is     null then false
							when label_spt is not null and etl_id_spt is not null then true
						end
							as check_spt,
						---------------------------------------------------------------------------
						case
							when label_spc is     null and etl_id_spc is     null then true
							when label_spc is     null and etl_id_spc is not null then false -- this variant is impossibe to be
							when label_spc is not null and etl_id_spc is     null then false
							when label_spc is not null and etl_id_spc is not null then true
						end
							as check_spc,
						---------------------------------------------------------------------------
						case
							when label_adt is     null and etl_id_adt is     null then true
							when label_adt is     null and etl_id_adt is not null then false -- this variant is impossibe to be
							when label_adt is not null and etl_id_adt is     null then false
							when label_adt is not null and etl_id_adt is not null then true
						end
							as check_adt,
						---------------------------------------------------------------------------
						case
							when label_adc is     null and etl_id_adc is     null then true
							when label_adc is     null and etl_id_adc is not null then false -- this variant is impossibe to be
							when label_adc is not null and etl_id_adc is     null then false
							when label_adc is not null and etl_id_adc is not null then true
						end
							as check_adc
				from
						w16
				)
		,w18 as	(
				select
						w17.*,
						case
							when (check_spt = false or check_spc = false or check_adt = false or check_adc = false)
							then
								false
							else
								true
						end
							as check_variables
				from
						w17
				)
		,w19 as	(
				select
						w7.*,
						tp.panel as panel__label,
						trys.reference_year_set as reference_year_set__label
				from
						w7
						inner join sdesign.t_panel as tp on w7.panel = tp.id
						inner join sdesign.t_reference_year_set as trys on w7.reference_year_set = trys.id
				)
		,w20 as	(
				select
						categorization_setup_supplement,
						array_agg(panel__label order by panel, reference_year_set) as panel__label,
						array_agg(reference_year_set__label order by panel, reference_year_set) as reference_year_set__label
				from
						w19 group by categorization_setup_supplement
				)
		,w21 as	(
				select w20.*, w18.* from w20 inner join w18
				on w20.categorization_setup_supplement = w18.categorization_setup
				)
		,w22 as	(
				select
						unnest(panel__label) as panel__label,
						unnest(reference_year_set__label) as reference_year_set__label,
						etl_id_spt__not_null,
						etl_id_spc__not_null,
						etl_id_adt__not_null,
						etl_id_adc__not_null,
						check_variables
				from
						w21
				)
		select
				json_build_object
				(
				'country', _res_country,
				'target_variable', _etl_id,
				'variables',json_agg(json_build_object(
					'panel', w22.panel__label,
					'reference_year_set', w22.reference_year_set__label,
					'sub_population_etl_id', w22.etl_id_spt__not_null,
					'sub_population_category_etl_id', w22.etl_id_spc__not_null,
					'area_domain_etl_id', w22.etl_id_adt__not_null,
					'area_domain_category_etl_id', w22.etl_id_adc__not_null,
					'check_variables',w22.check_variables))
				)
		from w22
		into _res;

		-----------------------------------------------------------------------
		-- check that all records of check_variables in internal argument _res
		-- in element variables are TRUE
		with
		w1 as	(
				select json_array_elements(_res->'variables') as s
				)
		,w2 as	(
				select
						(s->>'check_variables')::boolean as check_variables
				from
						w1
				)
		select count(w2.*) from w2 where w2.check_variables = false
		into _check_variables;

		if _check_variables > 0
		then
			raise exception 'Error 04: fn_etl_get_variables: For input aruguments: _refyearset2panel_mapping = % and _id_t_etl_target_variable = % was not ETL yet some of area domain type, area domain category, sub population type or sub population category into target DB!',_refyearset2panel_mapping, _id_t_etl_target_variable;
		end if;
		-----------------------------------------------------------------------

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_variables(integer[], integer) is
'Function returns variables for given input arguments.';

grant execute on function target_data.fn_etl_get_variables(integer[], integer) to public;