--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_variables_false
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_variables_false(JSON, varchar, varchar, character varying) CASCADE;

create or replace function target_data.fn_etl_get_variables_false
(
	_variables				JSON,
	_panel					varchar,
	_reference_year_set		varchar,
	_national_language		character varying(2) default 'en'::character varying(2)
)
returns table
(
	sub_population				varchar,
	sub_population_category		varchar,	
	area_domain					varchar,
	area_domain_category		varchar,
	sub_population_en			varchar,
	sub_population_category_en	varchar,	
	area_domain_en				varchar,
	area_domain_category_en		varchar
)
as
$$
declare
begin		
		if _variables is null
		then
			raise exception 'Error 01: fn_etl_get_variables_false: Input argument _variables must not be NULL!';
		end if;

		if _panel is null
		then
			raise exception 'Error 02: fn_etl_get_variables_false: Input argument _panel must not be NULL!';
		end if;

		if _reference_year_set is null
		then
			raise exception 'Error 03: fn_etl_get_variables_false: Input argument _reference_year_set must not be NULL!';
		end if;		

		if _national_language = 'en'
		then
			return query
			with
				w1 as	(
						select json_array_elements(_variables) as s
						)
				,w2 as	(
						select
								(s->>'panel')::varchar							as panel,
								(s->>'reference_year_set')::varchar				as reference_year_set,
								(s->>'sub_population_en')::varchar				as sub_population,
								(s->>'sub_population_category_en')::varchar		as sub_population_category,
								(s->>'area_domain_en')::varchar					as area_domain,
								(s->>'area_domain_category_en')::varchar		as area_domain_category,
								(s->>'sub_population_en')::varchar				as sub_population_en,
								(s->>'sub_population_category_en')::varchar		as sub_population_category_en,
								(s->>'area_domain_en')::varchar					as area_domain_en,
								(s->>'area_domain_category_en')::varchar		as area_domain_category_en			
						from w1
						)
				,w3 as	(
						select w2.* from w2
						where w2.panel = _panel
						and w2.reference_year_set = _reference_year_set
						)
				select
						w3.sub_population,
						w3.sub_population_category,
						w3.area_domain,
						w3.area_domain_category,
						w3.sub_population_en,
						w3.sub_population_category_en,
						w3.area_domain_en,
						w3.area_domain_category_en
				from
						w3;
		else
			return query
			with
				w1 as	(
						select json_array_elements(_variables) as s
						)
				,w2 as	(
						select
								(s->>'panel')::varchar							as panel,
								(s->>'reference_year_set')::varchar				as reference_year_set,
								(s->>'sub_population')::varchar					as sub_population,
								(s->>'sub_population_category')::varchar		as sub_population_category,
								(s->>'area_domain')::varchar					as area_domain,
								(s->>'area_domain_category')::varchar			as area_domain_category,
								(s->>'sub_population_en')::varchar				as sub_population_en,
								(s->>'sub_population_category_en')::varchar		as sub_population_category_en,
								(s->>'area_domain_en')::varchar					as area_domain_en,
								(s->>'area_domain_category_en')::varchar		as area_domain_category_en						
						from w1
						)
				,w3 as	(
						select w2.* from w2
						where w2.panel = _panel
						and w2.reference_year_set = _reference_year_set
						)
				select
						w3.sub_population,
						w3.sub_population_category,
						w3.area_domain,
						w3.area_domain_category,
						w3.sub_population_en,
						w3.sub_population_category_en,
						w3.area_domain_en,
						w3.area_domain_category_en
				from
						w3;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_variables_false(JSON, varchar, varchar, character varying) is
'Function returns list of attribute variables that must be still implemented for given combination of panel and reference year set for selected target variable.';

grant execute on function target_data.fn_etl_get_variables_false(JSON, varchar, varchar, character varying) to public;