--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_save_export_connection(varchar, varchar, integer, text) CASCADE;

create or replace function target_data.fn_etl_save_export_connection
(
	_host				varchar,
	_dbname				varchar,
	_port				integer,
	_comment			text
)
returns integer
as
$$
declare
		_id		integer;
begin
	if _host is null or _dbname is null or _port is null or _comment is null
	then
		raise exception 'Error 01: fn_etl_save_export_connection: Mandatory input of host, dbname, port or comment is null (%, %, %, %).', _host, _dbname, _port, _comment;
	end if; 

	insert into target_data.t_export_connection(host, dbname, port, comment)
	select _host, _dbname, _port, _comment
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_save_export_connection(varchar, varchar, integer, text) is
'Function inserts a record into table t_export_connection based on given parameters.';

grant execute on function target_data.fn_etl_save_export_connection(varchar, varchar, integer, text) to public;