--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_log
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_save_log(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_save_log
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns text
as
$$
declare
		_max_id_tel		integer;
		_time_now		timestamptz;
		_res			text;
begin
	if _refyearset2panel_mapping is null
	then
		raise exception 'Error 01: fn_etl_save_log: Input argument _refyearset2panel_mapping must not by NULL!';
	end if; 

	if _id_t_etl_target_variable is null
	then
		raise exception 'Error 02: fn_etl_save_log: Input argument _id_t_etl_target_variable must not by NULL!';
	end if;

	if	(
		select count(tetv.*) is distinct from 1
		from target_data.t_etl_target_variable as tetv where tetv.id = _id_t_etl_target_variable
		)
	then
		raise exception 'Error 03: fn_etl_save_log: For input argument _id_t_etl_target_variable = % not exist any record in table t_etl_target_variable!',_id_t_etl_target_variable;
	end if;

	for i in 1..array_length(_refyearset2panel_mapping,1)
	loop
		if	(
			select count(crpm.*) is distinct from 1
			from sdesign.cm_refyearset2panel_mapping as crpm where crpm.id = _refyearset2panel_mapping[i]
			)
		then
			raise exception 'Error 04: fn_etl_save_log: For input argument _refyearset2panel_mapping[i] = % not exist any record in table sdesign.cm_refyearset2panel_mapping!',_refyearset2panel_mapping[i];
		end if;
	end loop;

	_time_now := (now())::timestamptz;

	_max_id_tel := (select coalesce(max(id),0) from target_data.t_etl_log);

	with
	w1 as	(
			select
					_id_t_etl_target_variable as etl_target_variable,
					unnest(_refyearset2panel_mapping) as refyearset2panel_mapping,
					_time_now as time_now
			)
	insert into target_data.t_etl_log(etl_target_variable, refyearset2panel_mapping, etl_time)
	select
			w1.etl_target_variable,
			w1.refyearset2panel_mapping,
			w1.time_now
	from
			w1 order by w1.refyearset2panel_mapping;

	_res := concat('The ',(select count(*) from target_data.t_etl_log where id > _max_id_tel),' new records were inserted into t_etl_log table.');
	
	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_save_log(integer[], integer) is
'Function inserts a records into table t_etl_log based on given input parameters.';

grant execute on function target_data.fn_etl_save_log(integer[], integer) to public;