--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_supplement_categorization_setups
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_supplement_categorization_setups(integer[]) CASCADE;

create or replace function target_data.fn_etl_supplement_categorization_setups
(
	_categorization_setups	integer[]
)
returns integer[]
as
$$
declare
		_categorization_setups_adc		integer[];
		_categorization_setups_spc		integer[];
		_res							integer[];
begin
		if _categorization_setups is null
		then
			raise exception 'Error 01: fn_etl_supplement_categorization_setups: Input argument _categorization_setups must not by NULL!';
		end if;
		
		with
		w as	(
				select distinct categorization_setup
				from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(	
					select id from target_data.cm_ldsity2target_variable
					where target_variable =
								(
								select distinct target_variable from target_data.cm_ldsity2target_variable
								where id in	(
											select ldsity2target_variable
											from target_data.cm_ldsity2target2categorization_setup
											where categorization_setup in (select unnest(_categorization_setups))
											)
								)
					)
				and (
					adc2classification_rule is null
					or array_length(adc2classification_rule,1) <= 
						(select max(t.res) from
						(select array_length(adc2classification_rule,1) as res
						from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						and adc2classification_rule is not null) as t)
					)
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups_adc;

		with
		w as	(
				select distinct categorization_setup
				from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(	
					select id from target_data.cm_ldsity2target_variable
					where target_variable =
								(
								select distinct target_variable from target_data.cm_ldsity2target_variable
								where id in	(
											select ldsity2target_variable
											from target_data.cm_ldsity2target2categorization_setup
											where categorization_setup in (select unnest(_categorization_setups))
											)
								)
					)
				and (
					spc2classification_rule is null
					or array_length(spc2classification_rule,1) <= 
						(select max(t.res) from
						(select array_length(spc2classification_rule,1) as res
						from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						and spc2classification_rule is not null) as t)
					)
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups_spc;

		with
		w as	(
				select a.categorization_setup from
					(select unnest(_categorization_setups_adc) as categorization_setup) as a
				inner join
					(select unnest(_categorization_setups_spc) as categorization_setup) as b
				on a.categorization_setup = b.categorization_setup
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _res;
		
		return _res;

		/*
		-------------------------------------------------------------
		-------------------------------------------------------------
		-- add missing categorization_setups which are not prepared in t_available_datasets for incomming target variable,
		-- list of panels and list of reference year sets, but are needed to check additivity (especially are needed in
		-- process ETL Variable hierarchy)
		with
		w1 as	(
				select
						ldsity2target_variable,
						case
						when adc2classification_rule is null
							then array[0]
							else (select id_type from target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule))
						end as id_adt,
						case
						when spc2classification_rule is null
							then array[0]
							else (select id_type from target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule))
						end as id_spt
				from target_data.cm_ldsity2target2categorization_setup
				where categorization_setup in (select unnest(_categorization_setups))
				)
		,w2 as	(select distinct ldsity2target_variable, id_adt, id_spt from w1)
		,w3 as	(
				select
						id,
						ldsity2target_variable,
						categorization_setup,
						case
						when adc2classification_rule is null
							then array[0]
							else (select id_type from target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule))
						end as id_adt,
						case
						when spc2classification_rule is null
							then array[0]
							else (select id_type from target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule))
						end as id_spt
				from target_data.cm_ldsity2target2categorization_setup	
				where ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = _target_variable)
				)
		,w4 as	(
				select w3.* from w3 inner join w2
				on	(
					w3.ldsity2target_variable = w2.ldsity2target_variable
					and target_data.fn_etl_array_compare(w3.id_adt,w2.id_adt)
					and target_data.fn_etl_array_compare(w3.id_spt,w2.id_spt)
					)
				)
		select array_agg(t.categorization_setup order by t.categorization_setup) from
		(select distinct categorization_setup from w4) as t
		into _categorization_setups;
		-------------------------------------------------------------
		-------------------------------------------------------------
		*/
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_supplement_categorization_setups(integer[]) is
'Function supplement of missing categorie for input categorization setups.';

grant execute on function target_data.fn_etl_supplement_categorization_setups(integer[]) to public;