--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_try_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_try_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_etl_try_delete_export_connection(_id integer)
RETURNS integer
AS
$$
DECLARE
	_check_etl_data	integer;
	_check_user		integer;
BEGIN
	if _id is null
	then
		RAISE EXCEPTION 'Error 01: fn_etl_try_delete_export_connection: Input argument _id must not be NULL!';
	end if;

	IF NOT EXISTS (SELECT t.* FROM target_data.t_export_connection as t where t.id = _id)
	THEN RAISE EXCEPTION 'Error 02: fn_etl_try_delete_export_connection: Given export connection (_id = %) does not exist in t_export_connection table!', _id;
	END IF;

	with
	w1 as	(
			select tetv.id from target_data.t_etl_target_variable as tetv where tetv.export_connection = _id	union all
			select tead.id from target_data.t_etl_area_domain as tead where tead.export_connection = _id		union all
			select tesp.id from target_data.t_etl_sub_population as tesp where tesp.export_connection = _id
			)
	select count(w1.*) from w1
	into _check_etl_data;

	with
	w1 as	(
			select r.rolname as username,r1.rolname as "role"
			from pg_catalog.pg_roles r join pg_catalog.pg_auth_members m
			on (m.member = r.oid)
			join pg_roles r1 on (m.roleid=r1.oid)                                  
			where r.rolcanlogin and r.rolname = current_user
			order by 1
			)
	select count(*) from w1 where w1.role = 'app_nfiesta_mng'
	into _check_user;

	if _check_etl_data > 0 and _check_user = 0
	then
		raise exception 'Error 03: fn_etl_try_delete_export_connection: NOT ALLOWED delete given export connection. Given export connection (_id = %) contains ETL data and current user is not member in role "app_nfiesta_mng".',_id;
	end if;

	return _check_etl_data;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_etl_try_delete_export_connection(integer) IS
'The function provides test if it is possible to delete record from t_export_connection table and delete ETL datas.';

GRANT EXECUTE ON FUNCTION target_data.fn_etl_try_delete_export_connection(integer) TO public;