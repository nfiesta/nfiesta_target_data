--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rule_syntax
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_check_classification_rule_syntax(integer, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rule_syntax(_ldsity_object integer, _rule text)
RETURNS boolean 
AS
$$
DECLARE
_table_name	varchar;
_test		boolean;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
	THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
	END IF;

	SELECT table_name
	FROM target_data.c_ldsity_object
	WHERE id = _ldsity_object
	INTO _table_name;


	IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
	THEN _test = true;
	ELSE
		BEGIN
		EXECUTE
		'SELECT EXISTS(SELECT *
		FROM '||_table_name||'
		WHERE '||_rule ||'
		)'
		INTO _test;

		IF _test IS NOT NULL 
		THEN _test := true;
		ELSE _test := false; 
		END IF;

		EXCEPTION WHEN OTHERS THEN
			_test := false;
		END;
	END IF;

	RETURN _test;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rule_syntax(integer, text) IS
'Function checks syntax in text field with classification rule.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rule_syntax(integer, text) TO public;
