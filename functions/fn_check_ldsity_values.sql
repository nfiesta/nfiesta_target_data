--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_check_ldsity_values
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_ldsity_values(integer[], integer[]);

CREATE OR REPLACE FUNCTION target_data.fn_check_ldsity_values(_available_datasets integer[], _plots integer[] DEFAULT NULL::int[]) RETURNS void AS $src$
DECLARE
	_cs int[];
	--_vars int[];
	_panels int[];
	_refyearsets int[];
	_available_datasets_dist int[];
	_errp json;
BEGIN
	--raise notice '%', _available_datasets;

	select array_agg(distinct id), array_agg(distinct categorization_setup), array_agg(distinct panel), array_agg(distinct reference_year_set) 
	from target_data.t_available_datasets 
	where t_available_datasets.id = ANY(_available_datasets)
	into _available_datasets_dist, _cs, _panels, _refyearsets;

--raise notice '%, %, %', _cs, _refyearsets, _available_datasets_dist;

	if _cs is not null
	then
		--raise notice '%		fn_check_ldsity_values -- UPDATE CHECK START -- select target_data.fn_add_plot_target_attr(%, %, %, %, %);', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', _vars, _plots, _refyearsets, 1e-6, true;
		with w_err as (
			select target_data.fn_add_plot_target_attr(_cs, _plots, _panels, _refyearsets, 1e-6, true) as fn_err
		)
		, w_err_t as (
			select (fn_err).* from w_err
		)
		, w_err_json as (
			select 
				json_build_object(
					'plot'					, plot,
					'panel'		, panel,
					'reference_year_set'	, reference_year_set,
					'variable'				, variable,
					'ldsity'				, ldsity,
					'ldsity_sum'			, ldsity_sum,
					'variables_def'			, variables_def,
					'variables_found'		, variables_found,
					'diff'					, diff
				) as errj
			from w_err_t
		)
		select json_agg(errj) into _errp from w_err_json;
		--raise notice '%		fn_check_ldsity_values -- UPDATE CHECK STOP', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague';
		IF 
			(json_array_length(_errp) > 0) 
			THEN RAISE EXCEPTION 'fn_check_ldsity_values -- plot level local densities are not additive:
				checked categorization setups: %, checked plots: %, 
				found err plots: 
				%', _cs, _plots, jsonb_pretty(_errp::jsonb);
		END IF;
	else
		RAISE WARNING 'fn_check_ldsity_values -- additivity check was skiped: 
				* 0 rows edited/all rows were deleted (next line displays NULL)
				* corresponding variable hierarchy not found (next line displays array)
				available_datasets: %', _available_datasets_dist;
	end if;
        RETURN;
END;
$src$ LANGUAGE plpgsql;

comment on function target_data.fn_check_ldsity_values IS 'Function for checking additivity. Launched by triggers on either t_ldsity_values or t_available_datasets. Triggers on insert and update on t_available_datasets dropped due to performance problem. Will be replaced by more advanced concept of additivity checks in https://gitlab.com/nfiesta/nfiesta_pg/-/issues/133.';

