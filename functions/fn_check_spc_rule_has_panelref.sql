--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_spc_rule_has_panelref
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_check_spc_rule_has_panelref(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_spc_rule_has_panelref(_id integer)
RETURNS boolean
AS
$$
BEGIN
	RETURN (SELECT
		CASE WHEN EXISTS( SELECT * FROM target_data.cm_spc2classification_rule WHERE id = _id )
		THEN EXISTS( SELECT * FROM target_data.cm_spc2classrule2panel_refyearset WHERE spc2classification_rule = _id)
		ELSE TRUE
		END);		
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_spc_rule_has_panelref(integer) IS
'Function provides test if classification rule has some records in cm_spc2classrule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_spc_rule_has_panelref(integer) TO public;
