--
-- Copyright 2022, 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_stored_classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_stored_classification_rules(integer, integer, integer, integer, integer, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_stored_classification_rules(
	_ldsity integer,						-- id from table c_ldsity, ldsity contribution for which the rules should be tested
	_ldsity_object integer,						-- id from c_ldsity_object, for which object the classification rules are valid (must be within hierarchy of ldsity contribution)
	_area_domain integer DEFAULT NULL::int, 			-- area domain for which the classification rules will be tested
	_sub_population integer DEFAULT NULL::int, 			-- sub population for which the classification rules will be tested
	_panel_refyearset integer DEFAULT NULL::int,			-- an array of panel and reference year set combinations (id from table cm_refyearset2panel_mapping)
									-- in which are classification rules applicable
	_use_negative boolean DEFAULT false
)
RETURNS TABLE (
	result		boolean,
	message_id	integer,
	message		text
) 
AS
$$
DECLARE

BEGIN
	-- test on iput parameters
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	IF NOT array[_ldsity_object] <@ (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]))
	THEN
		RAISE EXCEPTION 'Given ldsity object (%) is not found within the hierarchy of ldsity contribution (%).', _ldsity_object, _ldsity;
	END IF;

	IF _area_domain IS NOT NULL AND NOT EXISTS(SELECT * FROM target_data.c_area_domain WHERE id = _area_domain)
	THEN
		RAISE EXCEPTION 'Given area domain (id = %) does not exist in table c_area_doamin!', _area_domain;
	END IF;

	IF _area_domain IS NOT NULL AND NOT EXISTS(SELECT * FROM target_data.cm_adc2classification_rule WHERE area_domain_category IN 
							(SELECT id FROM target_data.c_area_domain_category WHERE area_domain = _area_domain) AND
							ldsity_object = _ldsity_object)
	THEN
		RAISE EXCEPTION 'For given combination of ldsity object (id = %) and area domain (id = %) do not exist any classification rules!', _ldsity_object, _area_domain;
	END IF;

	IF _sub_population IS NOT NULL AND NOT EXISTS(SELECT * FROM target_data.c_sub_population WHERE id = _sub_population)
	THEN
		RAISE EXCEPTION 'Given sub population (id = %) does not exist in table c_sub_population!', _sub_population;
	END IF;

	IF _sub_population IS NOT NULL AND NOT EXISTS(SELECT * FROM target_data.cm_spc2classification_rule WHERE sub_population_category IN 
							(SELECT id FROM target_data.c_sub_population_category WHERE sub_population = _sub_population) AND
							ldsity_object = _ldsity_object)
	THEN
		RAISE EXCEPTION 'For given combination of ldsity object (id = %) and sub population (id = %) do not exist any classification rules!', _ldsity_object, _sub_population;
	END IF;

	IF _area_domain IS NULL AND _sub_population IS NULL
	THEN
		RAISE EXCEPTION 'Area domain and sub population both are NULL, there has to be one from them NOT NULL.';
	END IF;

	IF _area_domain IS NOT NULL AND _sub_population IS NOT NULL
	THEN
		RAISE EXCEPTION 'Area domain and sub population both are NOT NULL, there has to be only one from them not null.';
	END IF;

	-- variant for area_domain
	IF _area_domain IS NOT NULL
	THEN
		RETURN QUERY
		WITH 
		w_adc AS (
			SELECT
				t1.id AS area_domain_category,
				array_agg(t2.variable_superior ORDER BY t2.variable_superior) AS variable_superior
			FROM
				target_data.c_area_domain_category AS t1
			LEFT JOIN
				target_data.t_adc_hierarchy AS t2
			ON t1.id = t2.variable AND t2.dependent = true
			WHERE t1.area_domain = _area_domain
			GROUP BY t1.id
		),
		w_rules AS (
			SELECT
				t2.ldsity_object,
				array_agg(t2.classification_rule ORDER BY t2.area_domain_category) AS rules,
				array_agg(t1.variable_superior ORDER BY t2.area_domain_category) AS adc
			FROM w_adc AS t1
			INNER JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.area_domain_category = t2.area_domain_category
			WHERE t2.ldsity_object = _ldsity_object AND t2.use_negative = _use_negative
			GROUP BY t2.ldsity_object
		)
		SELECT t2.result, t2.message_id, t2.message
		FROM
			w_rules AS t1,
			target_data.fn_check_classification_rules(
				_ldsity, t1.ldsity_object, t1.rules, _panel_refyearset, adc, NULL::integer[][], _use_negative) AS t2
		;

		-- variant for sub population
	ELSIF _sub_population IS NOT NULL
	THEN
		RETURN QUERY
		WITH 
		w_spc AS (
			SELECT
				t1.id AS sub_population_category,
				array_agg(t2.variable_superior ORDER BY t2.variable_superior) AS variable_superior
			FROM
				target_data.c_sub_population_category AS t1
			LEFT JOIN
				target_data.t_spc_hierarchy AS t2
			ON t1.id = t2.variable AND t2.dependent = true
			WHERE t1.sub_population = _sub_population
			GROUP BY t1.id
		),
		w_rules AS (
			SELECT
				t2.ldsity_object,
				array_agg(t2.classification_rule ORDER BY t2.sub_population_category) AS rules,
				array_agg(t1.variable_superior ORDER BY t2.sub_population_category) AS spc
			FROM w_spc AS t1
			INNER JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.sub_population_category = t2.sub_population_category
			WHERE t2.ldsity_object = _ldsity_object AND t2.use_negative = _use_negative
			GROUP BY t2.ldsity_object
		)
		SELECT t2.result, t2.message_id, t2.message
		FROM
			w_rules AS t1,
			target_data.fn_check_classification_rules(
				_ldsity, t1.ldsity_object, t1.rules, _panel_refyearset, NULL::integer[][], spc, _use_negative) AS t2
		;
	END IF;	

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_stored_classification_rules(integer, integer, integer, integer, integer, boolean) IS
'Function checks already stored classificaiton rules by passing them into the fn_check_classification_rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_stored_classification_rules(integer, integer, integer, integer,  integer, boolean) TO public;
