--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_delete_area_domain(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_area_domain(_id integer)
RETURNS void
AS
$$
BEGIN
	WITH w AS (
		DELETE FROM target_data.cm_ldsity2target2categorization_setup
		WHERE categorization_setup IN
			(SELECT categorization_setup 
			FROM target_data.cm_ldsity2target2categorization_setup AS t1
			INNER JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.adc2classification_rule @> array[t2.id]
			WHERE t2.area_domain_category IN
					(SELECT id FROM target_data.c_area_domain_category 
					WHERE area_domain = _id)
			)
		RETURNING categorization_setup
	)
	DELETE FROM target_data.t_categorization_setup
	WHERE id IN 
		(SELECT categorization_setup
		FROM w);

	WITH w AS (
		SELECT target_variable
		FROM target_data.cm_ldsity2target_variable AS t1
		INNER JOIN target_data.cm_adc2classification_rule AS t2
		ON t1.area_domain_category @> array[t2.id]
		WHERE t2.area_domain_category IN
				(SELECT id FROM target_data.c_area_domain_category 
				WHERE area_domain = _id)
		),
	-- delete all contributions belonging to the target_variable
	w_del AS (
		DELETE FROM target_data.cm_ldsity2target_variable
		WHERE target_variable IN (SELECT target_variable FROM w)
		RETURNING target_variable
	)
	DELETE FROM target_data.c_target_variable
	WHERE id IN (SELECT target_variable FROM w_del);

	DELETE FROM target_data.cm_adc2classrule2panel_refyearset 
	WHERE adc2classification_rule IN 
		(SELECT id FROM target_data.cm_adc2classification_rule 
		WHERE area_domain_category IN
			(SELECT id FROM target_data.c_area_domain_category 
			WHERE area_domain = _id)); 		
				
	DELETE FROM target_data.cm_adc2classification_rule 
	WHERE area_domain_category IN 
		(SELECT id FROM target_data.c_area_domain_category 
		WHERE area_domain = _id);

	IF EXISTS (SELECT * FROM target_data.t_adc_hierarchy
		WHERE variable_superior IN (SELECT id FROM target_data.c_area_domain_category
			WHERE area_domain = _id) AND
			dependent = true
		)
	THEN 
		RAISE EXCEPTION 'Given area domain cannot be deleted, because some of its categories are present in table t_adc_hierarchy in the superior position. It can destroy the dependency between the categories. You have to delete inferior categories (area domain) first.';
	END IF;

	DELETE FROM target_data.t_adc_hierarchy
	WHERE	variable IN (SELECT id FROM target_data.c_area_domain_category
			WHERE area_domain = _id) OR
		variable_superior IN (SELECT id FROM target_data.c_area_domain_category
			WHERE area_domain = _id);
	
	DELETE FROM target_data.c_area_domain_category 
	WHERE area_domain = _id;
	
	DELETE FROM target_data.c_area_domain 
	WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_area_domain(integer) IS
'Function deletes records from c_area_domain, c_area_domain_category, cm_adc2classification_rule and cm_adc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_area_domain(integer) TO public;
