--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_area_domain_and_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_area_domain_and_values(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_area_domain_and_values(_id integer)
RETURNS void
AS
$$
BEGIN
	PERFORM target_data.fn_delete_ldsity_values(NULL::int, _id, NULL::int);

	PERFORM target_data.fn_delete_area_domain(_id);
END;
$$
LANGUAGE plpgsql
VOLATILE
STRICT
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_area_domain_and_values(integer) IS
'Function deletes records from all tables which can be influenced by the given area domain.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_area_domain_and_values(integer) TO public;
