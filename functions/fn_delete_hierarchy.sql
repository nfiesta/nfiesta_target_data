--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_delete_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_delete_hierarchy(integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_hierarchy(_areal_or_pop integer, _superior_cat integer[], _inferior_cat integer[])
RETURNS TABLE (
superior_cat		integer,
inferior_cat		integer
)
AS
$$
DECLARE
_test			integer[];
_superior_cat_test	integer[];
_inferior_cat_test	integer[];
BEGIN
		-- reorder just for equality test
		SELECT array_agg(t1.sup ORDER BY t1.sup) AS test
		FROM unnest(_superior_cat) AS t1(sup)
		INTO _superior_cat_test;

		SELECT array_agg(t1.inf ORDER BY t1.inf) AS test
		FROM unnest(_inferior_cat) AS t1(inf)
		INTO _inferior_cat_test;

	CASE WHEN _areal_or_pop = 100 THEN

		SELECT array_agg(t1.sup ORDER BY t1.sup)
		FROM
			unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
		INNER JOIN
			unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
		ON t1.id = t2.id
		INNER JOIN target_data.t_adc_hierarchy AS t3
		ON t1.sup = t3.variable_superior AND t2.inf = t3.variable
		WHERE t3.dependent = false
		INTO _test;

		IF _test != _superior_cat_test
		THEN RAISE EXCEPTION 'Given arrays of superior and inferior categories contain combinations which are not present in table t_adc_hierarchy. Or the inferior category is dependent on the superior - hierarchy can be deleted only by deleting inferior categories itself.';
		END IF; 

		RETURN QUERY
		DELETE FROM target_data.t_adc_hierarchy AS a
		USING 
			(SELECT t1.sup, t2.inf
			FROM	unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
			INNER JOIN unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
			ON t1.id = t2.id) AS b
		WHERE a.variable_superior = b.sup AND a.variable = b.inf
		RETURNING a.variable_superior, a.variable;

	WHEN _areal_or_pop = 200 THEN

		SELECT array_agg(t1.sup ORDER BY t1.sup)
		FROM
			unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
		INNER JOIN
			unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
		ON t1.id = t2.id
		INNER JOIN target_data.t_spc_hierarchy AS t3
		ON t1.sup = t3.variable_superior AND t2.inf = t3.variable
		INTO _test;

		IF _test != _superior_cat_test
		THEN RAISE EXCEPTION 'Given arrays of superior and inferior categories contain combinations which are not present in table t_spc_hierarchy. Or the inferior category is dependent on the superior - hierarchy can be deleted only by deleting inferior categories itself.';
		END IF; 

		RETURN QUERY
		DELETE FROM target_data.t_spc_hierarchy AS a
		USING 
			(SELECT t1.sup, t2.inf
			FROM	unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
			INNER JOIN unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
			ON t1.id = t2.id) AS b
		WHERE a.variable_superior = b.sup AND a.variable = b.inf
		RETURNING a.variable_superior, a.variable;

	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_hierarchy(integer, integer[], integer[]) IS
'Function deletes records from t_adc_hierarchy/t_spc_hierarchy table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_hierarchy(integer, integer[], integer[]) TO public;
