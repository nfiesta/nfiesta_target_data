--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_ldsity_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_ldsity_object_group(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_ldsity_object_group(_id integer)
RETURNS void
AS
$$
BEGIN

	DELETE FROM target_data.cm_ld_object2ld_object_group WHERE ldsity_object_group = _id;
	DELETE FROM target_data.c_ldsity_object_group WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_ldsity_object_group(integer) IS
'Function deletes records from c_ldsity_object_group and cm_ld_object2ld_object_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_ldsity_object_group(integer) TO public;
