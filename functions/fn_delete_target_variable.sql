--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_target_variable(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_target_variable(_id integer)
RETURNS void
AS
$$
BEGIN

	IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _id)
	THEN RAISE EXCEPTION 'Given target variable does not exist in table c_target_variable (%)', _id;
	END IF;

	WITH w AS (
		DELETE FROM target_data.cm_ldsity2target2categorization_setup
		WHERE categorization_setup IN 
			(SELECT categorization_setup FROM target_data.cm_ldsity2target2categorization_setup
			WHERE ldsity2target_variable IN 
				(SELECT id FROM target_data.cm_ldsity2target_variable
				WHERE target_variable = _id
				)		
			)
		RETURNING categorization_setup
	)
	DELETE FROM target_data.t_categorization_setup
	WHERE id IN 
		(SELECT categorization_setup
		FROM w);

	DELETE FROM target_data.cm_ldsity2target2categorization_setup 
	WHERE ldsity2target_variable IN (SELECT id FROM target_data.cm_ldsity2target_variable
				WHERE target_variable = _id);

	DELETE FROM target_data.cm_ldsity2target_variable WHERE target_variable = _id;
	DELETE FROM target_data.c_target_variable WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_target_variable(integer) IS
'Function deletes records from c_target_variable and cm_ldsity2target_variable table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_target_variable(integer) TO public;
