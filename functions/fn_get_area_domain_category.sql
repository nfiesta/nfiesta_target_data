--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_area_domain_category(integer, integer, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_area_domain_category(
	_target_variable integer DEFAULT NULL::integer, -- target variable (id from c_target_variable)
	_ldsity integer DEFAULT NULL::integer, 		-- ldsity contribution (id from c_ldsity)
	_use_negative boolean DEFAULT NULL::boolean	-- use_negative, only with combination target_variable NOT NULL
	)
RETURNS TABLE (
id		integer,			-- id from table c_area_domain_category
target_variable integer,			-- target variable (id from c_target_variable)
ldsity		integer,			-- local density contribution (id from c_ldsity)
label		character varying(200),		-- label from c_area_domain_category
description	text,				-- description dtto
label_en	character varying(200),
description_en	text,
ldsity_object		integer,
object_label		character varying(200),
object_description	text,
object_label_en		character varying(200),
object_description_en	text,
use_negative	boolean,
restriction	boolean
)
AS
$$
BEGIN
	IF _ldsity IS NULL
	THEN
		IF _target_variable IS NULL 
		THEN
			IF _use_negative IS NOT NULL THEN
				RAISE EXCEPTION 'Use negative can be specified only in combination with not null target variable.';
			END IF;

			RETURN QUERY
			SELECT 	t1.id, NULL::integer, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en, 
				NULL::integer, NULL::varchar(200), NULL::text, NULL::varchar(200), NULL::text,
				NULL::boolean, NULL::boolean
			FROM target_data.c_area_domain_category AS t1
			ORDER BY t1.id;
		ELSE
			RAISE EXCEPTION 'If target_variable is specified, ldsity has to be specified too.';
		END IF;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
		END IF;

		IF _target_variable IS NULL
		THEN
			IF _use_negative IS NOT NULL THEN
				RAISE EXCEPTION 'Use negative can be specified only in combination with not null target variable.';
			END IF;

			RETURN QUERY
			SELECT 	t4.id, NULL::integer, t1.id AS ldsity, t4.label, t4.description, t4.label_en, t4.description_en, 
				t6.id AS ldsity_object, t6.label, t6.description, t6.label_en, t6.description_en,  
				NULL::boolean, false::boolean AS restriction
			FROM target_data.c_ldsity AS t1,
			unnest(t1.area_domain_category) WITH ORDINALITY AS t2(def, id)
			LEFT JOIN target_data.cm_adc2classification_rule AS t3
			ON t2.def = t3.id
			LEFT JOIN target_data.c_area_domain_category AS t4
			ON t3.area_domain_category = t4.id
			LEFT JOIN target_data.c_ldsity_object AS t6
			ON t3.ldsity_object = t6.id
			WHERE t1.id = _ldsity
			ORDER BY t2.id;
		ELSE
			IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
			THEN RAISE EXCEPTION 'Given target_variable does not exist in table c_target_variable (%)', _target_variable;
			END IF;

			IF NOT EXISTS (SELECT * FROM target_data.cm_ldsity2target_variable AS t1 
					WHERE t1.target_variable = _target_variable AND t1.ldsity = _ldsity AND
					CASE WHEN _use_negative IS NOT NULL THEN t1.use_negative = _use_negative ELSE true END)
			THEN RAISE EXCEPTION 'Given ldsity is not part of given target_variable.';
			END IF;

			RETURN QUERY
			SELECT 	t5.id, t2.target_variable, t1.id AS ldsity, t5.label, t5.description, t5.label_en, t5.description_en, 
				t6.id AS ldsity_object, t6.label, t6.description, t6.label_en, t6.description_en,  
				t2.use_negative, false::boolean AS restriction
			FROM target_data.c_ldsity AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.ldsity,
				unnest(t1.area_domain_category) WITH ORDINALITY AS t3(def, id)
			LEFT JOIN target_data.cm_adc2classification_rule AS t4
			ON t3.def = t4.id
			LEFT JOIN target_data.c_area_domain_category AS t5
			ON t4.area_domain_category = t5.id
			LEFT JOIN target_data.c_ldsity_object AS t6
			ON t4.ldsity_object = t6.id
			WHERE t1.id = _ldsity AND t2.target_variable = _target_variable AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			--ORDER BY t2.id
			UNION ALL
			SELECT 	t5.id, t2.target_variable, t1.id AS ldsity, t5.label, t5.description, t5.label_en, t5.description_en,
				t6.id AS ldsity_object, t6.label, t6.description, t6.label_en, t6.description_en,  
				t2.use_negative, true::boolean AS restriction
			FROM target_data.c_ldsity AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.ldsity,
				unnest(t2.area_domain_category) WITH ORDINALITY AS t3(def, id)
			LEFT JOIN target_data.cm_adc2classification_rule AS t4
			ON t3.def = t4.id
			LEFT JOIN target_data.c_area_domain_category AS t5
			ON t4.area_domain_category = t5.id
			LEFT JOIN target_data.c_ldsity_object AS t6
			ON t4.ldsity_object = t6.id
			WHERE t1.id = _ldsity AND t2.target_variable = _target_variable AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			--ORDER BY t2.id
			;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_area_domain_category(integer, integer, boolean) IS
'Function returns records from c_area_domain_category table, optionally for given target variable and ldsity.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_area_domain_category(integer, integer, boolean) TO public;
