--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_area_domain_category4domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_area_domain_category4domain(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_area_domain_category4domain(_area_domain integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
area_domain	integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _area_domain IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.area_domain, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_area_domain_category AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _area_domain)
		THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _area_domain;
		END IF;

		RETURN QUERY
		SELECT t4.id, t4.area_domain, t4.label, t4.description, t4.label_en, t4.description_en
		FROM target_data.c_area_domain_category AS t4
		WHERE t4.area_domain = _area_domain
		ORDER BY t4.id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_area_domain_category4domain(integer) IS
'Function returns records from c_area_domain_category table, optionally for given area domain.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_area_domain_category4domain(integer) TO public;
