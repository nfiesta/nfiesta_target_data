--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_areal_or_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_areal_or_population(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_areal_or_population()
RETURNS TABLE (
id		integer,
label		character varying(200),
description	text
)
AS
$$
BEGIN
	RETURN QUERY
	SELECT t1.id, t1.label, t1.description
	FROM target_data.c_areal_or_population AS t1;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_areal_or_population() IS
'Function returns records from c_areal_or_population table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_areal_or_population() TO public;
