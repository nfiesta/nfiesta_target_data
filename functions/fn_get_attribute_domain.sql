--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_attribute_domain(varchar(2), integer[]);

CREATE OR REPLACE FUNCTION target_data.fn_get_attribute_domain
(
	IN _attr_type varchar(2), -- 'ad' or 'sp'
	IN _attr_types integer[]
)
  RETURNS TABLE(attribute_domain integer[], category integer[], label character varying[], description text[]) AS
$BODY$
	DECLARE
		_at_count					integer;
		_table_name_category		text;
		_column_name				text;
		_table_name_hierarchy		text;
		
		_attr_types_text			text;
		_carthesian_product			text;
		_constraints				text;
		_constraints_text			text;
		_complete_query				text;
	BEGIN

		if _attr_type is null
		then
			raise exception 'Error 01: fn_get_attribute_domain: The input argument _attr_type must not be NULL!';
		end if;
	
		if not(_attr_type = any(array['ad','sp']))
		then
			raise exception 'Error 02: fn_get_attribute_domain: Allowed values for the input argument _attr_type are "ad" or "sp" !';
		end if;
	
		if _attr_type = 'ad'
		then
			_table_name_category := 'target_data.c_area_domain_category';
			_column_name := 'area_domain';
			_table_name_hierarchy := 'target_data.t_adc_hierarchy';
		end if;
	
		if _attr_type = 'sp'
		then
			_table_name_category := 'target_data.c_sub_population_category';
			_column_name := 'sub_population';
			_table_name_hierarchy := 'target_data.t_spc_hierarchy';
		end if;

	CASE	WHEN _attr_types IS NULL THEN

			RAISE EXCEPTION 'Error 03: fn_get_attribute_domain: The function cannot be called without selling at least one attribute type!';

		WHEN array_length(_attr_types,1) IS DISTINCT FROM array_length(array_remove(_attr_types,NULL),1) THEN

			RAISE EXCEPTION 'Error 04: fn_get_attribute_domain: At least one of the passed attribute types is NULL (attr_types = %)!', _attr_types;
	ELSE
		_at_count := array_length(_attr_types,1);
	END CASE;

-----------------------------------------------------------------------------------------------------------------------
		_attr_types_text := concat(quote_literal(_attr_types::text), '::integer[]');
		_carthesian_product := '
			with recursive /*w_groupnums as (
				select 
					distinct ' || _column_name || ' as grp
				from ' || _table_name_category || '
				where ' || _column_name || ' = ANY (' || _attr_types_text || ')
			)
			,*/ w_groupnums as (select domains.item as grp, domains.id as gnum 
						from unnest( ' || _attr_types_text || ' ) with ordinality as domains(item, id) )
			, w_numbering as (
				select 
					/*grp, row_number() over(order by grp) as gnum */
					grp, gnum 
				from w_groupnums
			)
			, w_category as (
				select id, label, description, ' || _column_name || ' as sup_domain from ' || _table_name_category || '
				union all
				select 0 as id, ''bez rozlišení'' as label, ''bez rozlišení'' as description, ' || _column_name || '
				from ' || _table_name_category || ' group by ' || _column_name || '
			)
			, w_numbered as (
				select 
					w_numbering.gnum, 
					w_numbering.grp, 
					w_category.id as c_category__id,
					w_category.label as c_category__label,
					w_category.description as c_category__description
				from w_numbering 
				join w_category on w_numbering.grp = w_category.sup_domain
				order by w_numbering.grp, w_category.id nulls first
			)
			, w_exploded as (
					select gnum, 
						array[grp] as varray,
						array[c_category__id] as parray, 
						array[c_category__label] as larray,
						array[c_category__description] as darray
					from w_numbered
					where gnum = 1
				union all
					select w_numbered.gnum, 
						array_append(w_exploded.varray,w_numbered.grp),
						array_append(w_exploded.parray,w_numbered.c_category__id),
						array_append(w_exploded.larray,w_numbered.c_category__label),
						array_append(w_exploded.darray,w_numbered.c_category__description)
					from w_numbered 
					join w_exploded on w_exploded.gnum = w_numbered.gnum - 1
			)
			, w_carth_prod as (
				select varray as attribute_domain, parray as category, larray as label, darray as description
				from w_exploded
				where gnum = (select max(gnum) from w_numbering)
			)
			select * 
			from w_carth_prod
';
-------------------------------------------------------------
		_constraints := '
		with w_constraints_explicit as (
			select
				category_sup.' || _column_name || ' as sup_domain, 
				category_sup.id as sup_category, 
				category_sup.label as sup_label, 
				category.' || _column_name || ' as domain,
				category.id as category, 
				category.label
			from ' || _table_name_hierarchy || ' as constraints
			join ' || _table_name_category || ' as category_sup on (category_sup.id = constraints.variable_superior)
			join ' || _table_name_category || ' as category on (category.id = constraints.variable)
		)
		, w_constraints_sup_implicit as (
			select
				category_table.' || _column_name || ' as sup_domain, 
				category_table.id as sup_category, 
				category_table.label as sup_label, 
				null::int as domain,
				null::int as category, 
				''not found'' as label 
			from ' || _table_name_category || ' as category_table
			where 
				(category_table.' || _column_name || ' in (select sup_domain from w_constraints_explicit)
				and
				category_table.id not in (select sup_category from w_constraints_explicit))
		)
		, w_constraints_sup_implicit_combinations as (
			select 
				w_constraints_sup_implicit.sup_domain,
				w_constraints_sup_implicit.sup_category,
				w_constraints_sup_implicit.sup_label,
				domain_combination.domain,
				w_constraints_sup_implicit.category,
				w_constraints_sup_implicit.label
			from (select 
					distinct sup_domain, domain 
					from w_constraints_explicit) as domain_combination
			join w_constraints_sup_implicit on (domain_combination.sup_domain = 
												w_constraints_sup_implicit.sup_domain ) 
		)
		, w_constraints_implicit as (
			select 
				null::int as sup_domain, 
				null::int as sup_category, 
				''not found'' as sup_label, 
				category_table.' || _column_name || ',
				category_table.id as category, 
				category_table.label  
			from ' || _table_name_category || ' as category_table
			where 
				(category_table.' || _column_name || ' in (select domain from w_constraints_explicit)
				and
				category_table.id not in (select category from w_constraints_explicit))
		)
		, w_constraints_implicit_combinations as (
			select 
				w_constraints_implicit.sup_domain,
				w_constraints_implicit.sup_category,
				w_constraints_implicit.sup_label,
				domain_combination.domain,
				w_constraints_implicit.category,
				w_constraints_implicit.label
			from (select 
					distinct sup_domain, domain 
					from w_constraints_explicit) as domain_combination
			join w_constraints_implicit on (domain_combination.sup_domain = 
											w_constraints_implicit.sup_domain ) 
		)
		, w_constraints_implicit_nodistinction as (
			select distinct 
				sup_domain, 0 as sup_category, ''bez rozlišení'' as sup_label, 
				domain, 0 as sup_category, ''bez rozlišení'' as sup_label
			from w_constraints_explicit
		)
		, w_constraints_union as (
			select * from w_constraints_explicit
			union all
			select * from w_constraints_sup_implicit_combinations
			union all
			select * from w_constraints_implicit_combinations
			union all
			select * from w_constraints_implicit_nodistinction
		)
		, w_sub_constraints as (
			select 
				sup_domain, sup_category, domain, array_agg(category order by category) as categories,
				format(''category[array_position(attribute_domain, %s)] = %s'', sup_domain, sup_category) as const_1,
				format(''category[array_position(attribute_domain, %s)] in (%s)'', domain, array_to_string(array_agg(category order by category), '','')) as const_2,
				format(''category[array_position(attribute_domain, %s)] = 0'', domain) as const_3
			from w_constraints_union
			group by sup_domain, domain, sup_category
			order by sup_domain, domain, sup_category
		)
		, w_constraints as (
			select
				sup_domain, domain,
				case when categories = ''{NULL}'' or categories = ''{0}''
					then format(E''(%s and %s)'', const_1, const_3) 
					else format(E''(%s and (%s or %s))'', const_1, const_2, const_3) 
				end as const
			from w_sub_constraints
		)
		, w_constraint as (
			select 
				format (E''\n(\n\t(array_position(attribute_domain, %s) is not null and array_position(attribute_domain, %s) is not null)\n\tand (\n\t\t%s\n))'', 
							sup_domain, domain, array_to_string(array_agg(const), E''\n\tor\t ''))
					as const
				from w_constraints group by	sup_domain, domain
				having sup_domain = ANY (' || _attr_types_text || ') 
					and domain = ANY (' || _attr_types_text || ')
		)
		select 
			format (E''%s'', array_to_string(array_agg(const), E''\nand\n''))
			from w_constraint;
		';
		
		EXECUTE _constraints INTO _constraints_text;
		
		if _constraints_text = '' then 
			_complete_query := concat(_carthesian_product, E'\norder by category\n;');
		else
			_complete_query := concat(_carthesian_product, E'where\n', _constraints_text, E'\norder by category\n;');
		end if;

		/*IF _attribute_domain IS NOT NULL
		THEN
			IF _attr_1 IS NULL THEN RAISE EXCEPTION 'Error 06: No combination between attribute types was found for the specified attribute domain (attribute_types = %, attribute_domain = %).', _attr_types, _attribute_domain;
			END IF;
		END IF;*/
		
		--raise notice '%		fn_get_attribute_domain -- %', TimeOfDay(), _complete_query;
		RETURN QUERY EXECUTE _complete_query;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  COMMENT ON FUNCTION target_data.fn_get_attribute_domain(varchar(2), integer[]) IS
'The function returns the attribute domain field table for the specified attribute type combination.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_attribute_domain (varchar(2), integer[]) TO app_nfiesta;
