--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_case_ad4categorization_setups
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_case_ad4categorization_setups(integer[]) CASCADE;

create or replace function target_data.fn_get_case_ad4categorization_setups
(
	_categorization_setups	integer[]
)
returns text
as
$$
declare
		_id_ad_array			integer[];
		_string4case_i			text;
		_string4case			text;
begin
		if _categorization_setups is null
		then
			raise exception 'Error 01: fn_get_case_ad4categorization_setups: Input argument _categorization_setups must not by null !';
		end if;
		
		select array_agg(id order by id) from target_data.t_available_datasets
		where categorization_setup in (select unnest(_categorization_setups))
		into _id_ad_array;
	
		if _id_ad_array is null
		then
			raise exception 'Error 02: fn_get_case_ad4categorization_setups: For values in input argument _categorization_setup not found any record in table t_available_datasets !';
		end if;	

		with
		w1 as	(
				select
						id,
						concat('when (t1.categorization_setup = ',categorization_setup,' and t1.panel = ',panel,' and t1.reference_year_set = ',reference_year_set,') then ',id) as string4case
				from
						target_data.t_available_datasets
				where
						id = any(_id_ad_array)
				)
		,w2 as	(
				select
						w1.id,
						concat('#DELETE#',w1.string4case) as string4case
				from
						w1
				)
		,w3 as	(
				select
						array_agg(w2.id order by w2.id) as id,
						array_agg(w2.string4case order by w2.id) as string4case
				from
						w2
				)
		select
				replace(replace(replace(concat(w3.string4case),'{"#DELETE#',' '),'","#DELETE#',' '),'"}',' ') as string4case
		from
				w3		
		into
				_string4case;
				
		/*
		for i in 1..array_length(_id_ad_array,1)
		loop
			select
					concat('when (t1.categorization_setup = ',categorization_setup,' and t1.panel = ',panel,' and t1.reference_year_set = ',reference_year_set,') then ',id)
			from
					target_data.t_available_datasets where id = _id_ad_array[i]
			into
					_string4case_i;
			
			if i = 1
			then
					_string4case := _string4case_i;
			else
					_string4case := concat(_string4case,' ',_string4case_i);
			end if;
		
		end loop;
		*/

		_string4case := concat('case ',_string4case,' end');

		return _string4case;	
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_case_ad4categorization_setups(integer[]) is
'The function gets string for transforming available dataset ids for input values of categorization setups.';

grant execute on function target_data.fn_get_case_ad4categorization_setups(integer[]) to public;
