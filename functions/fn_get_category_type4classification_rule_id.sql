--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_category_type4classification_rule_id
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_category_type4classification_rule_id(character varying, integer[]) CASCADE;

create or replace function target_data.fn_get_category_type4classification_rule_id
(
	_classification_rule_type	character varying,
	_classification_rule_id		integer[]
)
returns table
(
	id_type integer[],
	label varchar,
	description text,
	label_en varchar,
	description_en text
)
as
$$
declare
		_table_name_1		text;
		_table_name_2		text;
		_table_name_3		text;
		_column_name_1		text;
		_column_name_2		text;
		_column_name_3		text;
		_category_i			integer;
		_category_type_i	integer;
		_id_i				integer;
		_label_i			varchar;
		_description_i		text;
		_label_en_i			varchar;
		_description_en_i	text;
		_res_id				integer[];
		_res_label			varchar;
		_res_description	text;
		_res_label_en		varchar;
		_res_description_en	text;
begin
		if _classification_rule_type is null
		then
			raise exception 'Error 01: fn_get_category_type4classification_rule_id: Input argument _classification_rule_type must not by NULL!';
		end if;
	
		/*
		if _classification_rule_id is null
		then
			raise exception 'Error 02: fn_get_category_type4classification_rule_id: Input argument _classification_rule_id must not by NULL!';
		end if;
		*/
	
		if _classification_rule_id is null
		then
			_res_id := null::integer[];
			_res_label := null::varchar;
			_res_description := null::text;
			_res_label_en := null::varchar;
			_res_description_en := null::text;
		else		
			if _classification_rule_type = 'adc'
			then
				_table_name_1 := 'cm_adc2classification_rule';
				_column_name_1 := 'area_domain_category';
				_table_name_2 := 'c_area_domain_category';
				_column_name_2 := 'area_domain';
				_table_name_3 := 'c_area_domain';
			end if;
		
			if _classification_rule_type = 'spc'
			then
				_table_name_1 := 'cm_spc2classification_rule';
				_column_name_1 := 'sub_population_category';
				_table_name_2 := 'c_sub_population_category';
				_column_name_2 := 'sub_population';
				_table_name_3 := 'c_sub_population';
			end if;
	
		
			for i in 1..array_length(_classification_rule_id,1)
			loop
				execute 'select '||_column_name_1||' from target_data.'||_table_name_1||' where id = $1'
				using _classification_rule_id[i]
				into _category_i;
						
				if _category_i is null
				then
					raise exception 'Error 03: fn_get_category_type4classification_rule_id: For input arguments _classification_rule_type = % and _classifacation_rule_id = % not found record in table % !',_classification_rule_type, _classification_rule_id[i], _table_name_1;
				end if;
			
				execute 'select '||_column_name_2||' from target_data.'||_table_name_2||' where id = $1'
				using _category_i
				into _category_type_i;
						
				if _category_type_i is null
				then
					raise exception 'Error 04: fn_get_category_type4classification_rule_id: For input arguments _classification_rule_type = % and _classifacation_rule_id = % not found record in table % !',_classification_rule_type, _classification_rule_id[i], _table_name_2;
				end if;
			
				execute
				'
				select
						id,
						case when label is null then ''null''::varchar else label end as label,
						case when description is null then ''null''::text else description end as description,
						case when label_en is null then ''null''::varchar else label_en end as label_en,
						case when description_en is null then ''null''::text else description_en end as description_en
				from
						target_data.'||_table_name_3||'
				where
						id = $1
				'
				using	_category_type_i
				into
						_id_i,
						_label_i,
						_description_i,
						_label_en_i,
						_description_en_i;
											
				if i = 1
				then
					_res_id := array[_id_i];
					_res_label := _label_i;
					_res_description := _description_i;
					_res_label_en := _label_en_i;
					_res_description_en := _description_en_i;			
				else
					_res_id := _res_id || array[_id_i];
					_res_label := _res_label || ';' || _label_i;
					_res_description := _res_description || ';' || _description_i;
					_res_label_en := _res_label_en || ';' || _label_en_i;
					_res_description_en := _res_description_en || ';' || _description_en_i;
				end if;
			end loop;
	end if;
	
	return query select _res_id, _res_label, _res_description, _res_label_en, _res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_category_type4classification_rule_id(character varying, integer[]) is
'The function gets category type for input classification rules.';

grant execute on function target_data.fn_get_category_type4classification_rule_id(character varying, integer[]) to public;