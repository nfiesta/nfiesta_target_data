--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4classification_rule_id
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) CASCADE;

create or replace function target_data.fn_get_classification_rule4classification_rule_id
(
	_classification_rule_type	character varying,
	_classification_rule_id		integer[],
	_ldsity_objects				integer[]
)	
returns text
as
$$
declare
		_table_name								text;
		_classification_rule_i					text;	
		_ldsity_object_i						integer;
		_pozice_i								integer;
		_column_name_i_array					text[];
		_classification_rule_i_pozice			text;
		_classification_rule_4case_pozice		text;
		_category								integer[];
begin
		if _classification_rule_type is null
		then
			raise exception 'Error 01: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_type must not by NULL !';
		end if;
	
		if not(_classification_rule_type = any(array['adc','spc']))
		then
			raise exception 'Error 02: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_type must be "adc" or "spc" !';
		end if;	
	
		if _classification_rule_id is null
		then
			raise exception 'Error 03: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_id must not by NULL !';
		end if;
	
		if _ldsity_objects is null
		then
			raise exception 'Error 04: fn_get_classification_rule4classification_rule_id: Input argument _ldsity_objects must not by NULL !';
		end if;	
	
		if _classification_rule_type = 'adc' then _table_name := 'cm_adc2classification_rule'; end if;
		if _classification_rule_type = 'spc' then _table_name := 'cm_spc2classification_rule'; end if;	
	
		for i in 1..array_length(_classification_rule_id,1)
		loop
				execute '
				select
						case when classification_rule is null then ''true'' else classification_rule end as classification_rule,
						ldsity_object
				from
						target_data.'||_table_name||'
				where
						id = $1'
				using	_classification_rule_id[i]
				into	_classification_rule_i,
						_ldsity_object_i;

				-----------------------------------------------------
				if 	_classification_rule_i in ('EXISTS','NOT EXISTS')
				then
					_classification_rule_i := 'TRUE';
					_pozice_i := 1;
				else
					_classification_rule_i := _classification_rule_i;
					_pozice_i := array_position(_ldsity_objects,_ldsity_object_i);
				end if;
				-----------------------------------------------------						
										
				/*
				select array_agg(column_name::text order by ordinal_position) from information_schema.columns
			 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_object_i)
			 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_object_i)
				into _column_name_i_array;
				*/

				with
				w1 as	(
						select column_name::text from information_schema.columns
			 			where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_object_i)
			 			and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_object_i)
			 			)
			 	select array_agg(w1.column_name) from w1
			 	where position(w1.column_name in _classification_rule_i) > 0
				into _column_name_i_array;

				if _column_name_i_array is not null
				then
					for ii in 1..array_length(_column_name_i_array,1)
					loop					
						_classification_rule_i := regexp_replace(_classification_rule_i,concat('(\+|\*|\-|\/| |\A|\(|\)|\=|,|\t)(',_column_name_i_array[ii],')(\+|\*|\-|\/| |\Z|\(|\)|\=|,|\:)'),concat('\1\2','_',_pozice_i,'\3'),'g'); 
					end loop;
				end if;							
								
				_classification_rule_i := concat('(',_classification_rule_i,')');
		
				if i = 1
				then
					_classification_rule_4case_pozice := _classification_rule_i;
				else
					_classification_rule_4case_pozice := concat(_classification_rule_4case_pozice,' and ',_classification_rule_i);
				end if;		
						
		end loop;
	
		_classification_rule_4case_pozice := concat('(',_classification_rule_4case_pozice,')');

		return _classification_rule_4case_pozice;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) is
'The function gets text of classification rules for their classification rule identifiers.';

grant execute on function target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) to public;
