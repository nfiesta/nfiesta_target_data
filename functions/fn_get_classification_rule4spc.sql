--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4spc
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_classification_rule4spc(integer, integer, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_classification_rule4spc(
	_sub_population_category integer DEFAULT NULL::integer, 
	_ldsity_object integer DEFAULT NULL::integer,
	_use_negative boolean DEFAULT NULL::boolean )
RETURNS TABLE (
id			integer,
sub_population_category	integer,
ldsity_object		integer,
classification_rule	text,
use_negative		boolean
)
AS
$$
BEGIN
	IF _sub_population_category IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.sub_population_category, t1.ldsity_object, t1.classification_rule, t1.use_negative
		FROM target_data.cm_spc2classification_rule AS t1
		WHERE
			CASE WHEN _use_negative IS NOT NULL THEN t1.use_negative = _use_negative ELSE true END
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population_category AS t1 WHERE t1.id = _sub_population_category)
		THEN RAISE EXCEPTION 'Given sub_population_category does not exist in table c_sub_population_category (%)', _sub_population_category;
		END IF;

		IF _ldsity_object IS NOT NULL
		THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
			THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
			END IF;

			RETURN QUERY
			SELECT t2.id, t1.id AS sub_population_category, t2.ldsity_object, t2.classification_rule, t2.use_negative
			FROM target_data.c_sub_population_category AS t1
			LEFT JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.id = _sub_population_category AND t2.ldsity_object = _ldsity_object AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			ORDER BY t2.id;
		ELSE
			RETURN QUERY
			SELECT t2.id, t1.id AS sub_population_category, t2.ldsity_object, t2.classification_rule, t2.use_negative
			FROM target_data.c_sub_population_category AS t1
			LEFT JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.id = _sub_population_category AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			ORDER BY t2.id;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_classification_rule4spc(integer, integer, boolean) IS
'Function returns records from cm_spc2classification_rule table, optionally for given sub_population_category and/or ldsity object.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_classification_rule4spc(integer, integer, boolean) TO public;
