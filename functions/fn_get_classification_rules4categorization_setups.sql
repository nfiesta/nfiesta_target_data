--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rules4categorization_setups
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rules4categorization_setups(integer[], integer) CASCADE;

create or replace function target_data.fn_get_classification_rules4categorization_setups
(
	_categorization_setups	integer[],
	_ldsity2target_variable	integer
)
returns text
as
$$
declare
		_ldsity									integer;
		_ldsity_object_type						integer;
		_target_variable						integer;
		_ldsity_object							integer;
		_ldsity_objects							integer[];
		_ldsity2target_variable_object_type_100	integer;
		_string4case_i							text;
		_string4case							text;
begin
		if _categorization_setups is null
		then
			raise exception 'Error 01: fn_get_classification_rules4categorization_setups: Input argument _categorization_setups must not by null !';
		end if;
	
		if _ldsity2target_variable is null
		then
			raise exception 'Error 02: fn_get_classification_rules4categorization_setups: Input argument _ldsity2target_variable must not by null !';
		end if;
	
		select
				ldsity,
				ldsity_object_type,
				target_variable
		from
				target_data.cm_ldsity2target_variable
		where
				id = _ldsity2target_variable
		into
				_ldsity,
				_ldsity_object_type,
				_target_variable;
	
		select ldsity_object from target_data.c_ldsity 
		where id = _ldsity
		into _ldsity_object;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_get_classification_rules4categorization_setups: For input argument _ldsity2target_variable = % not found any ldsity_object in table c_ldsity!',_ldsity2target_variable;
		end if;
	
		_ldsity_objects := target_data.fn_get_ldsity_objects(array[_ldsity_object]);
	
		if _ldsity_object_type = 200
		then
			select id from target_data.cm_ldsity2target_variable
			where target_variable = _target_variable
			and ldsity_object_type = 100
			into _ldsity2target_variable_object_type_100;
		else
			_ldsity2target_variable_object_type_100 := null::integer;
		end if;
	
		-- ordering values in argument _categorization_setups
		with
		w as	(
				select distinct t.res from (select unnest(_categorization_setups) as res) as t
				)
		select array_agg(w.res order by w.res) from w
		into _categorization_setups;
				
		if _ldsity_object_type = 200
		then
			with
			w as	(
					select
							a.categorization_setup,
							a.adc2classification_rule,
							b.spc2classification_rule
					from
					(
					select categorization_setup, adc2classification_rule
					from target_data.cm_ldsity2target2categorization_setup
					where categorization_setup = any(_categorization_setups)
					and	ldsity2target_variable = _ldsity2target_variable_object_type_100
					) as a
					inner join
					(
					select categorization_setup, spc2classification_rule
					from target_data.cm_ldsity2target2categorization_setup
					where categorization_setup = any(_categorization_setups)
					and	ldsity2target_variable = _ldsity2target_variable
					) as b
					on a.categorization_setup = b.categorization_setup
					)
			,w1 as	(
					select
						w.categorization_setup,
						case
							when adc2classification_rule is null and spc2classification_rule is null
							then 'when area_domain_category = array[0] and sub_population_category = array[0] then true'
							
							when adc2classification_rule is not null and spc2classification_rule is null
							then concat('when area_domain_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('adc',adc2classification_rule)),',',''),
										'] and sub_population_category = array[0] then case when '
										,target_data.fn_get_classification_rule4classification_rule_id('adc',adc2classification_rule,_ldsity_objects),
										' then true else false end'
										)
										
							when adc2classification_rule is null and spc2classification_rule is not null
							then concat('when area_domain_category = array[0] and sub_population_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('spc',spc2classification_rule)),',',''),
										'] then case when '
										,target_data.fn_get_classification_rule4classification_rule_id('spc',spc2classification_rule,_ldsity_objects),
										' then true else false end'
										)
							
							when adc2classification_rule is not null and spc2classification_rule is not null
							then concat('when area_domain_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('adc',adc2classification_rule)),',',''),
										'] and sub_population_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('spc',spc2classification_rule)),',',''),
										'] then case when ('
										,target_data.fn_get_classification_rule4classification_rule_id('adc',adc2classification_rule,_ldsity_objects),
										' and '
										,target_data.fn_get_classification_rule4classification_rule_id('spc',spc2classification_rule,_ldsity_objects),
										') then true else false end'
										)		
						end as string4case												
					from
							w
					)
			,w2 as	(
					select
							w1.categorization_setup,
							concat('#DELETE#',w1.string4case) as string4case
					from
							w1
					)
			,w3 as	(
					select
							array_agg(w2.categorization_setup order by w2.categorization_setup) as categorization_setup,
							array_agg(w2.string4case order by w2.categorization_setup) as string4case
					from
							w2
					)
			select
					replace(replace(replace(concat(w3.string4case),'{"#DELETE#',' '),'","#DELETE#',' '),'"}',' ') as string4case
			from
					w3
			into
					_string4case;
		else
			with
			w1 as	(					
					select
						categorization_setup,
						case
							when adc2classification_rule is null and spc2classification_rule is null
							then 'when area_domain_category = array[0] and sub_population_category = array[0] then true'
							
							when adc2classification_rule is not null and spc2classification_rule is null
							then concat('when area_domain_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('adc',adc2classification_rule)),',',''),
										'] and sub_population_category = array[0] then case when '
										,target_data.fn_get_classification_rule4classification_rule_id('adc',adc2classification_rule,_ldsity_objects),
										' then true else false end'
										)
										
							when adc2classification_rule is null and spc2classification_rule is not null
							then concat('when area_domain_category = array[0] and sub_population_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('spc',spc2classification_rule)),',',''),
										'] then case when '
										,target_data.fn_get_classification_rule4classification_rule_id('spc',spc2classification_rule,_ldsity_objects),
										' then true else false end'
										)
							
							when adc2classification_rule is not null and spc2classification_rule is not null
							then concat('when area_domain_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('adc',adc2classification_rule)),',',''),
										'] and sub_population_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('spc',spc2classification_rule)),',',''),
										'] then case when ('
										,target_data.fn_get_classification_rule4classification_rule_id('adc',adc2classification_rule,_ldsity_objects),
										' and '
										,target_data.fn_get_classification_rule4classification_rule_id('spc',spc2classification_rule,_ldsity_objects),
										') then true else false end'
										)		
						end as string4case												
					from
							target_data.cm_ldsity2target2categorization_setup
					where
							categorization_setup = any(_categorization_setups)
					and
							ldsity2target_variable = _ldsity2target_variable
					)
			,w2 as	(
					select
							w1.categorization_setup,
							concat('#DELETE#',w1.string4case) as string4case
					from
							w1
					)
			,w3 as	(
					select
							array_agg(w2.categorization_setup order by w2.categorization_setup) as categorization_setup,
							array_agg(w2.string4case order by w2.categorization_setup) as string4case
					from
							w2
					)
			select
					replace(replace(replace(concat(w3.string4case),'{"#DELETE#',' '),'","#DELETE#',' '),'"}',' ') as string4case
			from
					w3
			into
					_string4case;
		end if;
			
		_string4case := concat('case ',_string4case,' end');

		return _string4case;	
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_classification_rules4categorization_setups(integer[], integer) is
'The function gets text of classification rules for input categorization setups.';

grant execute on function target_data.fn_get_classification_rules4categorization_setups(integer[], integer) to public;