--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rules_id4categories
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[], boolean[], boolean[]) CASCADE;

create or replace function target_data.fn_get_classification_rules_id4categories
(
	_area_domain integer[],
	_area_domain_object integer[],
	_sub_population integer[],
	_sub_population_object integer[],
	_use_negative_ad boolean[],
	_use_negative_sp boolean[]
)
returns table
(
	area_domain_category integer[],
	sub_population_category integer[],
	adc2classification_rule integer[],
	spc2classification_rule integer[]
) as
$$
declare
		_string_ad_w1			text;
		_string_ad_w3			text;
		_string_columns_ad		text;
		_string_columns_ad_w3	text;
	
		_string_sp_w2			text;
		_string_sp_w3			text;
		_string_columns_sp		text;
		_string_columns_sp_w3	text;
	
		_query_res				text;
begin
		-------------------------------
		-------------------------------
		if
			_area_domain is null
		then
			/*
			_string_ad_w1 := 'select 0 as adc_1';
			_string_ad_w3 := 'array[adc_1] as area_domain_category';
			*/

			_string_ad_w1 := 'select array[0] as area_domain, array[0] as area_domain_category';
		else
			/*		
			for i in 1..array_length(_area_domain,1)
			loop
					if i = 1
					then
						_string_columns_ad := concat('unnest(attr_',i,') as adc_',i);
						_string_columns_ad_w3 := concat('adc_',i);
					else
						_string_columns_ad := concat(_string_columns_ad,', ',concat('unnest(attr_',i,') as adc_',i));
						_string_columns_ad_w3 := concat(_string_columns_ad_w3,', adc_',i);
					end if;
			end loop;
		
			_string_ad_w1 := replace(replace(concat('select ',_string_columns_ad,' from target_data.fn_get_attribute_domain(''ad''::varchar,array[',_area_domain,'])'),'{',''),'}','');
			_string_ad_w3 := concat('array[',_string_columns_ad_w3,'] as area_domain_category');
			*/

			_string_ad_w1 := replace(replace(concat('select attribute_domain as area_domain, category as area_domain_category from target_data.fn_get_attribute_domain(''ad''::varchar,array[',_area_domain,'])'),'{',''),'}','');
		end if;
				
		-------------------------------
		-------------------------------
		
		-------------------------------
		-------------------------------
		if
			_sub_population is null
		then
			/*
			_string_sp_w2 := 'select 0 as spc_1';
			_string_sp_w3 := 'array[spc_1] as sub_population_category';
			*/

			_string_sp_w2 := 'select array[0] as sub_population_domain, array[0] as sub_population_category';
		else
			/*
			for i in 1..array_length(_sub_population,1)
			loop
					if i = 1
					then
						_string_columns_sp := concat('unnest(attr_',i,') as spc_',i);
						_string_columns_sp_w3 := concat('spc_',i);
					else
						_string_columns_sp := concat(_string_columns_sp,', ',concat('unnest(attr_',i,') as spc_',i));
						_string_columns_sp_w3 := concat(_string_columns_sp_w3,', spc_',i);
					end if;
			end loop;
		
			_string_sp_w2 := replace(replace(concat('select ',_string_columns_sp,' from target_data.fn_get_attribute_domain(''sp''::varchar,array[',_sub_population,'])'),'{',''),'}','');
			_string_sp_w3 := concat('array[',_string_columns_sp_w3,'] as sub_population_category');
			*/

			_string_sp_w2 := replace(replace(concat('select attribute_domain as sub_population_domain, category as sub_population_category from target_data.fn_get_attribute_domain(''sp''::varchar,array[',_sub_population,'])'),'{',''),'}','');
		end if;
		
		-------------------------------
		-------------------------------	

		_query_res := concat
		('
		with
		w1 as	(',_string_ad_w1,'),
		w2 as	(',_string_sp_w2,'),
		w_data_ad as (
			select
		 		$3 				as domain_orig, 
				w1.area_domain			as domain_reorder,
				$1 				as object_orig
			from w1 limit 1
		)
		, w_unnest_ad as (
			select
				domain_orig.id, domain_orig.item as domain_orig, 
		 		domain_reorder.item as domain_reorder,
		 		object_orig.item as object_orig
			from w_data_ad as w_data,
					unnest(w_data.domain_orig)		with ordinality as domain_orig(item, id)
			join 	unnest(w_data.domain_reorder) 	with ordinality as domain_reorder(item, id) 	on domain_orig.id = domain_reorder.id
			join 	unnest(w_data.object_orig) 		with ordinality as object_orig(item, id) 		on object_orig.id = domain_reorder.id
		)
		, w_sort_ad as (
			select
				w_unnest.object_orig,
				array_position(w_data.domain_reorder, w_unnest.domain_orig)
			from w_data_ad as w_data, w_unnest_ad as w_unnest
		)
		, w_reorder_ad as (
			select 
				array_agg(object_orig order by array_position) as object_reorder 
			from w_sort_ad as w_sort
		),
		w_data_sp as (
			select
		 		$4 				as domain_orig,
		 		w2.sub_population_domain	as domain_reorder,
		 		$2 				as object_orig
			from w2 limit 1
		)
		, w_unnest_sp as (
			select
				domain_orig.id, domain_orig.item as domain_orig, 
		 		domain_reorder.item as domain_reorder, 
		 		object_orig.item as object_orig
			from w_data_sp as w_data,
					unnest(w_data.domain_orig)		with ordinality as domain_orig(item, id)
			join 	unnest(w_data.domain_reorder) 	with ordinality as domain_reorder(item, id) 	on domain_orig.id = domain_reorder.id
			join 	unnest(w_data.object_orig) 		with ordinality as object_orig(item, id) 		on object_orig.id = domain_reorder.id
		)
		, w_sort_sp as (
			select
				w_unnest.object_orig,
				array_position(w_data.domain_reorder, w_unnest.domain_orig)
			from w_data_sp as w_data, w_unnest_sp as w_unnest
		)
		, w_reorder_sp as (
			select 
				array_agg(object_orig order by array_position) as object_reorder 
			from w_sort_sp as w_sort
		),
		w3 as	(select w1.area_domain_category, w2.sub_population_category from w1, w2),
		w4 as	(
				select
					area_domain_category,
					sub_population_category,
					(
					target_data.fn_get_classification_rule_id4category
						(
						area_domain_category,
						w_reorder_ad.object_reorder,
						sub_population_category,
						w_reorder_sp.object_reorder,
						$5,
						$6
						)
					) as res
				from
					w3, w_reorder_ad, w_reorder_sp
				)
		select
				area_domain_category,
				sub_population_category,
				(res).adc2classification_rule,
				(res).spc2classification_rule
		from
				w4 order by area_domain_category, sub_population_category;
		');
		return query execute ''||_query_res||'' using _area_domain_object, _sub_population_object, _area_domain, _sub_population, _use_negative_ad, _use_negative_sp;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[], boolean[], boolean[]) is
'The function for the area domain or sub population returns a combination of attribute classifications and database identifiers of their classifiaction_rules.';

grant execute on function target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[], boolean[], boolean[]) to public;
