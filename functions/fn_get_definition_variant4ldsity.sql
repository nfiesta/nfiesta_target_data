--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_definition_variant4ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_definition_variant4ldsity(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_definition_variant4ldsity(_ldsity integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
ldsity		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _ldsity IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_definition_variant AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
		END IF;

		RETURN QUERY
		SELECT t3.id, t1.id AS ldsity, t3.label, t3.description, t3.label_en, t3.description_en
		FROM target_data.c_ldsity AS t1,
		unnest(t1.definition_variant) WITH ORDINALITY AS t2(def, id)
		LEFT JOIN target_data.c_definition_variant AS t3
		ON t2.def = t3.id
		WHERE t1.id = _ldsity
		ORDER BY t2.id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_definition_variant4ldsity(integer) IS
'Function returns records from c_definition_variant table, optionally for given ldsity.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_definition_variant4ldsity(integer) TO public;
