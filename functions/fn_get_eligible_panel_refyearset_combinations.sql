-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_eligible_panel_refyearset_combinations 
--------------------------------------------------------------------------------
-- ALTER EXTENSION nfiesta_target_data DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT);
-- DROP FUNCTION IF EXISTS target_data.fn_get_eligible_panel_refyearset_combinations(INT, INT[]);

-- API function 
CREATE OR REPLACE FUNCTION 
target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable INT, _categorization_setup INT [])
RETURNS TABLE (
	refyearset2panel_mapping INT, 
	panel_label VARCHAR(20), 
	panel_description VARCHAR(120), 
	refyearset_label VARCHAR(20), 
	refyearset_description VARCHAR(120))  AS 
$$
DECLARE 

	_state_or_change INT; -- 100 state variable, 200 change variable, 300 dynamic variable
	_refyearset2panel_eligible_sum INT[]; -- eligible combinations of panel and reference yearset for positive terms of the local density     
	_refyearset2panel_eligible_subtract INT[]; -- eligible combinations of panel and reference yearset for negative terms of the local density

BEGIN
	
	-- raising an exception on NULL input for _target_variable
	IF _target_variable IS NULL then 
		RAISE EXCEPTION 'Input parameter _target_variable INT not given, but it is required!';
	END IF; 

	-- raising an exception on NULL input for _categorization_setup
	IF _categorization_setup IS NULL then 
		RAISE EXCEPTION 'Input parameter _categorization_setup INT[] not given, but it is required!';
	END IF;

	-- set the state or change variable
	SELECT state_or_change FROM target_data.c_target_variable WHERE id = _target_variable INTO _state_or_change; 	

	SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(_target_variable, _categorization_setup, FALSE) 
	INTO _refyearset2panel_eligible_sum;
	
	IF _refyearset2panel_eligible_sum IS NULL THEN 
		RETURN;
	END IF;


	IF _state_or_change = 200 THEN 
	
		SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(_target_variable, _categorization_setup, TRUE) 
		INTO _refyearset2panel_eligible_subtract;
		
		IF _refyearset2panel_eligible_subtract IS NULL THEN 
			RETURN;
		END IF;
	
		RETURN QUERY
		SELECT 
			t5.id AS refyearset2panel_mapping,
			t6.panel AS panel_label,
			t6."label" AS panel_description,
			t2.reference_year_set AS refyearset_label,
			t2."label" AS refyearset_description
		FROM 
			sdesign.cm_refyearset2panel_mapping AS t1
		INNER JOIN 
			sdesign.t_reference_year_set AS t2
			ON
			ARRAY[t1.id] <@ _refyearset2panel_eligible_sum AND
			t1.reference_year_set = t2.reference_year_set_end
		INNER JOIN
			sdesign.cm_refyearset2panel_mapping AS t3
			ON
			ARRAY[t3.id] <@ _refyearset2panel_eligible_subtract AND
			t1.panel = t3.panel
		INNER JOIN 
			sdesign.t_reference_year_set AS t4
			ON
			t3.reference_year_set = t4.reference_year_set_begin AND 
			t2.id = t4.id
		INNER JOIN
			sdesign.cm_refyearset2panel_mapping AS t5
			ON 
			t5.panel = t1.panel AND 
			t5.reference_year_set = t2.id
		INNER JOIN 
		 	sdesign.t_panel AS t6
		 	ON
		 	t1.panel = t6.id;
	
	ELSE -- state or dynamic variables with no subtraction of local density contributions
	
		RETURN QUERY
		WITH w_refyearset2panel_mapping AS (
			SELECT unnest(_refyearset2panel_eligible_sum) AS refyearset2panel_mapping
		)
		SELECT 
			t1.refyearset2panel_mapping,
			t4.panel AS panel_label,
			t4."label" AS panel_description,
			t3.reference_year_set AS refyerset_label,
			t3."label" AS refyearset_description
		FROM
			w_refyearset2panel_mapping AS t1 
		INNER JOIN	
			sdesign.cm_refyearset2panel_mapping AS t2
			ON t1.refyearset2panel_mapping = t2.id
		INNER JOIN
			sdesign.t_reference_year_set AS t3
			ON t2.reference_year_set = t3.id
		INNER JOIN
			sdesign.t_panel AS t4
			ON t2.panel = t4.id;
	END IF;
	
 RETURN;

END
$$ 
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER; 

COMMENT ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT, INT []) IS 
'Function returns combinations of panel and reference-yearsets (ids of cm_refyearset2panel_mapping), '
'for which local density of the given target variable and for the given set of its categorizations '
'can be calculated.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT, INT []) TO public;

/*
-- tests
-- inspection of the list of target variables 
SELECT * FROM target_data.c_target_variable;

-- inspection of categorization setups for given target variable (adapt the id on line 159)
 WITH w AS ( 
	SELECT 
		t1.target_variable,
		t1.ldsity,
		t2.id,
		t2.categorization_setup, 
		t2.adc2classification_rule,
		t2.spc2classification_rule,
		unnest(coalesce(t2.adc2classification_rule, ARRAY[NULL::integer])) AS adc_rule,
		unnest(coalesce(t2.spc2classification_rule, ARRAY[NULL::integer])) AS spc_rule
	FROM 
		target_data.cm_ldsity2target_variable AS t1
	INNER JOIN
		target_data.cm_ldsity2target2categorization_setup AS t2
	ON
		t1.target_variable IN (6) AND
		t1.id = t2.ldsity2target_variable
-- ORDER BY t1.target_variable, t2.categorization_setup;
),
w_domains AS (
SELECT 
	t1.target_variable,
	t1.ldsity,
	t1.categorization_setup,
	t1.adc2classification_rule,
	t1.spc2classification_rule,
	array_agg(DISTINCT t4.area_domain ORDER BY t4.area_domain ) FILTER (WHERE t4.area_domain IS NOT NULL) AS area_domain,
	array_agg(DISTINCT t5.sub_population ORDER BY t5.sub_population) FILTER (WHERE t5.sub_population IS NOT NULL) AS subpopulation
FROM 
	w AS t1 
LEFT JOIN 
	target_data.cm_adc2classification_rule AS t2
ON
	t1.adc_rule = t2.id
LEFT JOIN 
	target_data.cm_spc2classification_rule AS t3
ON
	t1.spc_rule = t3.id
LEFT JOIN 
	target_data.c_area_domain_category AS t4
ON
	t2.area_domain_category = t4.id
LEFT JOIN 
	target_data.c_sub_population_category AS t5
ON
	t3.sub_population_category = t5.id
GROUP BY t1.target_variable, t1.ldsity, t1.categorization_setup, t1.spc2classification_rule, adc2classification_rule
)
SELECT 
	area_domain,
	subpopulation,
	array_agg(categorization_setup) AS categorization_setup
FROM 
	w_domains
GROUP BY area_domain, subpopulation; 
   

-- checking if the function returns some combinations of panels and reference yearsets for forest area   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(1, ARRAY[2,3]);
-- ten records

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(1, ARRAY[1]);
-- ten records

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(4, ARRAY[109,110,111,112,113,115,116,117,118,119,120,121,122,123,128,129,130,131,132,134,135,136,137,138,139,140,141,142,147,148,149,150,151,153,154,155,156,157,158,159,160,161]);
-- four records

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(5, ARRAY[163,180,182,199,201,218,163,180,182,199,201,218]);
-- two records

-- checking if the function returns some combinations of panels and reference yearsets for change of forest area 
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(6, ARRAY[220,221,222]);
-- three records, all coresponding to change referenece yearsets

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(2, ARRAY[23,24,25,26,27,29,30,31,32,33,34,35,36,37,40,41,42,43,44,46,47,48,49,50,51,52,53,54]);
-- five records

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood and regeneration stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(3, ARRAY[73,79,90,96,73,79,90,96]);
-- three records

 -- checking if the function raises an exception for NULL input
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(NULL, ARRAY[73,79,90,96,73,79,90,96]);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(3, NULL);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(NULL, NULL);

*/

-- ALTER EXTENSION nfiesta_target_data DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT);
-- DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT);

-- overloaded function which is not part of API and should be used from within API function fn_save_ldsity_values 
CREATE OR REPLACE FUNCTION 
target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable INT) -- id of target_data.c_target_variable 
RETURNS TABLE (
	categorization_setup INT, -- id of target_data.t_categorization_setup
	refyearset2panel_mapping INT [])  AS -- maximum set of records from sdesign.cm_refyearset2panel_mapping for which ldsity can be calculated using given categorization setup   
$$
BEGIN
	
	-- raising an exception on NULL input for _target_variable
	IF _target_variable IS NULL THEN 
		RAISE EXCEPTION 'Input parameter _target_variable INT not given, but it is required!';
	END IF; 

	RETURN QUERY
	WITH w_categorization_list AS (
		SELECT 
			DISTINCT t2.categorization_setup AS catsetup
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN
			target_data.cm_ldsity2target2categorization_setup AS t2
		ON
			t1.target_variable = _target_variable AND
			t1.id = t2.ldsity2target_variable 
	),
	w_refyearset2panel_mapping AS ( 
	SELECT 
		t1.catsetup,
		(target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable, ARRAY[t1.catsetup])).refyearset2panel_mapping AS refmap
	FROM
		w_categorization_list AS t1
	)
	SELECT 
		catsetup,
		array_agg(refmap)
	FROM 
		w_refyearset2panel_mapping
	GROUP BY catsetup;	
	
RETURN;

END
$$ 
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER; 

COMMENT ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT) IS 
'Function returns a table with all categorization setups under given target variable.'
'To each categorization setup an array of all panel and reference yearset combination '
' is attached, for which local densities can be calculated ';

GRANT EXECUTE ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT) TO public;

/* tests
  
  SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(1);
  -- 3 records
  
  SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(2);
  -- 51 records
  
    SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(4);
  -- 57 records
  
  -- test of null input
  SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(NULL);
  -- exception raised
  
 */ 
