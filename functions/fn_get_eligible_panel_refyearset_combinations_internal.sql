-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_eligible_panel_refyearset_combinations_internal 
--------------------------------------------------------------------------------
-- ALTER EXTENSION nfiesta_target_data DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations_internal(INT, INT [], BOOL);
-- DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations_internal(INT, INT [], BOOL);

CREATE OR REPLACE FUNCTION 
target_data.fn_get_eligible_panel_refyearset_combinations_internal(_target_variable INT, _categorization_setup INT [],  _use_negative BOOL)
RETURNS INT[] AS 
$$
DECLARE 

	_refyearset2panel4ldsity INT[]; -- set of panel and refrerence yearset combinations for which local densities are available
	_refyearset2panel4ldsity_spc INT[]; -- set of panel and reference yearset combinations for which values local densities are ETL-constrained to the specified subpopulations
	_refyearset2panel4ldsity_adc INT[]; -- set of panel and reference yearset combinations for which values local densities are ETL-constrained to the specified areal domain  
	_refyearset2panel4ldsity2target_variable_spc INT[]; -- set of panel and reference yearset combinations for which values local densities were constrained to the specified subpopulations by the user   
	_refyearset2panel4ldsity2target_variable_adc INT[]; -- set of panel and reference yearset combinations for which values local densities were constrained to the specified area domains by the user
	_refyearset2panel4spc INT[]; -- set of panel and reference yearset combinations for which values local densities can be classified using a particular set of subpopulation categories
	_refyearset2panel4adc INT[]; -- set of panel and reference yearset combinations for which values local densities can be classified using a particular set of area domain categories 
	_refyearset2panel_common INT[]; -- set of panel and reference yearset combinations for which values local densities of a target variable can be calculated
	_rules_present bool; -- logical value coding whether there are some rules defined    

BEGIN
	
	-- raising an exception on NULL input
	IF _target_variable IS NULL then 
		RAISE EXCEPTION 'Input parameter _target_variable INT not given, but it is required!';
	END IF; 

	-- raising an exception on NULL input
	IF _categorization_setup IS NULL then 
		RAISE EXCEPTION 'Input parameter _categorization_setup INT[] not given, but it is required!';
	END IF; 
	
	-- raising an exception on NULL input
	IF _use_negative IS NULL then 
		RAISE EXCEPTION 'Input parameter _use_negative INT not given, but it is required!';
	END IF; 

	-- assigning _refyearset2panel4ldsity
	WITH w_ldsity AS (
		SELECT DISTINCT 
			t2.id AS ldsity, 
			t1."version" 
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN 
			target_data.c_ldsity AS t2
			ON	
		 	t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative AND
			t1.ldsity = t2.id
	),
	w_refyearset2panel AS (
		SELECT 
			t2.refyearset2panel 
		FROM 
			w_ldsity  AS t1
		INNER JOIN 
			target_data.cm_ldsity2panel_refyearset_version AS t2
			ON 
			t2.ldsity = t1.ldsity AND 
			t2."version" = t1."version"
		GROUP BY t2.refyearset2panel, t2."version" HAVING count(*) = (SELECT count(*) FROM w_ldsity)
	)
	SELECT array_agg(DISTINCT refyearset2panel) FROM w_refyearset2panel
	INTO _refyearset2panel4ldsity;
--raise notice 'refyearset2panel4ldsity = %', _refyearset2panel4ldsity;

	-- assigning _refyearset2panel4ldsity_spc
	WITH w_ldsity_spc_rules AS (
		SELECT DISTINCT
			 unnest(t2.sub_population_category) AS spc2classification_rule 
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN 
			target_data.c_ldsity AS t2
			ON	
			t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative AND
			t1.ldsity = t2.id
	),
	w_refyearset2panel AS (
		SELECT 
			t3.refyearset2panel
		FROM
			w_ldsity_spc_rules AS t1	
		INNER JOIN
			target_data.cm_spc2classification_rule AS t2
			ON
			t1.spc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_spc2classrule2panel_refyearset AS t3 
			ON 
			t3.spc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity_spc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_ldsity_spc_rules) FROM w_refyearset2panel 
	INTO _refyearset2panel4ldsity_spc, _rules_present; 
--raise notice 'refyearset2panel4ldsity_spc = %, rules = %', _refyearset2panel4ldsity_spc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity_spc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	-- assigning _refyearset2panel4ldsity_adc
	WITH w_ldsity_adc_rules AS (
		SELECT DISTINCT
			 unnest(t2.area_domain_category) AS adc2classification_rule 
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN 
			target_data.c_ldsity AS t2
			ON	
			t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative AND
			t1.ldsity = t2.id
	),
	w_refyearset2panel AS (
		SELECT 
			t3.refyearset2panel
		FROM
			w_ldsity_adc_rules AS t1	
		INNER JOIN
			target_data.cm_adc2classification_rule AS t2
			ON
			t1.adc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_adc2classrule2panel_refyearset AS t3 
			ON 
			t3.adc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity_adc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_ldsity_adc_rules) FROM w_refyearset2panel 
	INTO _refyearset2panel4ldsity_adc, _rules_present;  
--raise notice 'refyearset2panel4ldsity_adc = %, rules = %', _refyearset2panel4ldsity_adc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity_adc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	-- assigning _refyearset2panel4ldsity2target_variable_spc
	WITH w_ldsity2target_variable_spc_rules AS (
		SELECT DISTINCT
			unnest(t1.sub_population_category) AS spc2classification_rule 
		FROM 
		 	target_data.cm_ldsity2target_variable AS t1
		WHERE
		 	t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative
	),
	w_refyearset2panel AS (
		SELECT 
			t3.refyearset2panel
		FROM
			w_ldsity2target_variable_spc_rules AS t1	
		INNER JOIN
			target_data.cm_spc2classification_rule AS t2
			ON
			t1.spc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_spc2classrule2panel_refyearset AS t3 
			ON 
			t3.spc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity2target_variable_spc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >=1 FROM w_ldsity2target_variable_spc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4ldsity2target_variable_spc, _rules_present; 
--raise notice 'refyearset2panel4ldsity2target_variable_spc = %, rules = %', _refyearset2panel4ldsity2target_variable_spc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity2target_variable_spc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;


	-- assigning _refyearset2panel4ldsity2target_variable_adc
	WITH w_ldsity2target_variable_adc_rules AS (
		SELECT DISTINCT
			unnest(t1.area_domain_category) AS adc2classification_rule
		FROM 
		 	target_data.cm_ldsity2target_variable AS t1
		WHERE
		 	t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative
	),
	w_refyearset2panel AS (
		SELECT
			t3.refyearset2panel
		FROM
			w_ldsity2target_variable_adc_rules AS t1	
		INNER JOIN
			target_data.cm_adc2classification_rule AS t2
			ON
			t1.adc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_adc2classrule2panel_refyearset AS t3
			ON 
			t3.adc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity2target_variable_adc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_ldsity2target_variable_adc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4ldsity2target_variable_adc, _rules_present; 
--raise notice 'refyearset2panel4ldsity2target_variable_adc = %, rules = %', _refyearset2panel4ldsity2target_variable_adc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity2target_variable_adc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	-- assigning _refyearset2panel4spc
	WITH w_spc_rules AS (
	SELECT DISTINCT 
		 unnest(t2.spc2classification_rule) AS spc_classification_rule 
	FROM 
		target_data.cm_ldsity2target_variable AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.target_variable = _target_variable AND 
		t1.use_negative = _use_negative AND
		t1.id = t2.ldsity2target_variable AND 
		ARRAY[t2.categorization_setup] <@ _categorization_setup
	),
	w_refyearset2panel AS (
		SELECT 
			refyearset2panel
		FROM 
			w_spc_rules AS t1
		INNER JOIN 
			target_data.cm_spc2classification_rule AS t2
			ON
			t1.spc_classification_rule = t2.id
		INNER JOIN 
			target_data.cm_spc2classrule2panel_refyearset AS t3
			ON 
			t3.spc2classification_rule = t2.id
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_spc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >=1 FROM w_spc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4spc, _rules_present;
--raise notice 'refyearset2panel4spc = %, rules = %', _refyearset2panel4spc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4spc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	-- assigning _refyearset2panel4adc
	WITH w_adc_rules AS (
	SELECT DISTINCT
		 unnest(t2.adc2classification_rule) AS adc_classification_rule 
	FROM 
		target_data.cm_ldsity2target_variable AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.target_variable = _target_variable AND 
		t1.use_negative = _use_negative AND
		t1.id = t2.ldsity2target_variable AND 
		ARRAY[t2.categorization_setup] <@ _categorization_setup 
	),
	w_refyearset2panel AS (
		SELECT 
			refyearset2panel
		FROM 
			w_adc_rules AS t1
		INNER JOIN 
			target_data.cm_adc2classification_rule AS t2
			ON
			t1.adc_classification_rule = t2.id
		INNER JOIN 
			target_data.cm_adc2classrule2panel_refyearset AS t3
			ON 
			t3.adc2classification_rule = t2.id
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_adc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_adc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4adc, _rules_present;
--raise notice 'refyearset2panel4adc = %, rules = %', _refyearset2panel4adc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4adc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	WITH w_refyearset2panel_intersection AS (
		SELECT unnest(_refyearset2panel4ldsity) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity_spc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity_adc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity2target_variable_spc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity2target_variable_adc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4spc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4adc,_refyearset2panel4ldsity)) AS refyearset2panel
	)
	SELECT array_agg(refyearset2panel) FROM  w_refyearset2panel_intersection INTO _refyearset2panel_common;
	
	RETURN _refyearset2panel_common;
		
END
$$ 
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER; 

COMMENT ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations_internal(INT, INT [], BOOL) IS 
'Function returns combinations of panel and reference-yearsets (ids of cm_refyearset2panel_mapping), '
'for which positive or negative contributions to local density of the given target variable and for the given ' 
'set of its categorizations can be calculated.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations_internal(INT, INT [], BOOL) TO public;

/*
-- tests
-- inspection of the list of target variables 
SELECT * FROM target_data.c_target_variable;

 -- inspection of categorization setups for given target variable (adapt the id on line 361)
 WITH w AS ( 
	SELECT 
		t1.target_variable,
		t1.ldsity,
		t2.id,
		t2.categorization_setup, 
		t2.adc2classification_rule,
		t2.spc2classification_rule,
		unnest(coalesce(t2.adc2classification_rule, ARRAY[NULL::integer])) AS adc_rule,
		unnest(coalesce(t2.spc2classification_rule, ARRAY[NULL::integer])) AS spc_rule
	FROM 
		target_data.cm_ldsity2target_variable AS t1
	INNER JOIN
		target_data.cm_ldsity2target2categorization_setup AS t2
	ON
		t1.target_variable IN (6) AND
		t1.id = t2.ldsity2target_variable
-- ORDER BY t1.target_variable, t2.categorization_setup;
),
w_domains AS (
SELECT 
	t1.target_variable,
	t1.ldsity,
	t1.categorization_setup,
	t1.adc2classification_rule,
	t1.spc2classification_rule,
	array_agg(DISTINCT t4.area_domain ORDER BY t4.area_domain ) FILTER (WHERE t4.area_domain IS NOT NULL) AS area_domain,
	array_agg(DISTINCT t5.sub_population ORDER BY t5.sub_population) FILTER (WHERE t5.sub_population IS NOT NULL) AS subpopulation
FROM 
	w AS t1 
LEFT JOIN 
	target_data.cm_adc2classification_rule AS t2
ON
	t1.adc_rule = t2.id
LEFT JOIN 
	target_data.cm_spc2classification_rule AS t3
ON
	t1.spc_rule = t3.id
LEFT JOIN 
	target_data.c_area_domain_category AS t4
ON
	t2.area_domain_category = t4.id
LEFT JOIN 
	target_data.c_sub_population_category AS t5
ON
	t3.sub_population_category = t5.id
GROUP BY t1.target_variable, t1.ldsity, t1.categorization_setup, t1.spc2classification_rule, adc2classification_rule
)
SELECT 
	area_domain,
	subpopulation,
	array_agg(categorization_setup) AS categorization_setup
FROM 
	w_domains
GROUP BY area_domain, subpopulation;

-- checking if the function returns some combinations of panels and reference yearsets for forest area   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, ARRAY[1,2,3], FALSE); 
-- {8,9,10,13,5,2,4,6,12,14}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, ARRAY[1,2,3], TRUE); 
-- no records, state variable has no negative contributions to local density

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(4, ARRAY[109,110,111,112,113,115,116,117,118,119,120,121,122,123,128,129,130,131,132,134,135,136,137,138,139,140,141,142,147,148,149,150,151,153,154,155,156,157,158,159,160,161], FALSE);
-- {4,2,13,9}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(4, ARRAY[109,110,111,112,113,115,116,117,118,119,120,121,122,123,128,129,130,131,132,134,135,136,137,138,139,140,141,142,147,148,149,150,151,153,154,155,156,157,158,159,160,161], TRUE);
-- no records, state variable has no negative contributions to local density


SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(5, ARRAY[173,179,192,198,211,217,173,179,192,198,211,217], FALSE);
-- {9,13}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(5, ARRAY[173,179,192,198,211,217,173,179,192,198,211,217], TRUE);
-- no records, state variable has no negative contributions to local density

-- checking if the function returns some combinations of panels and reference yearsets for change of forest area 
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(6, ARRAY[221,222], FALSE);
-- {8,9,10,13,5,2,4,6,12,14}

-- the same set is retrieved for negative contributions
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(6, ARRAY[221,222], TRUE);
-- {8,9,10,13,5,2,4,6,12,14}

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(2, ARRAY[23,24,25,26,27,29,30,31,32,33,34,35,36,37,40,41,42,43,44,46,47,48,49,50,51,52,53,54], FALSE);
-- {9,13,5,2,4}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(2, ARRAY[23,24,25,26,27,29,30,31,32,33,34,35,36,37,40,41,42,43,44,46,47,48,49,50,51,52,53,54], TRUE);
-- no records, state variable has no negative contributions to local density

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood and regeneration stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(3, ARRAY[73,79,90,96,73,79,90,96], FALSE);
--{9,13,5}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(3, ARRAY[73,79,90,96,73,79,90,96], TRUE);
-- no records, state variable has no negative contributions to local density

-- checking if the function raises an exception for NULL input
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(NULL, ARRAY[1,2,3], TRUE);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, NULL, TRUE);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, ARRAY[1,2,3], NULL);
*/