-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_eligible_panel_refyearset_groups
--------------------------------------------------------------------------------

-- ALTER EXTENSION nfiesta_target_data DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_groups(INT[])
-- DROP FUNCTION IF EXISTS target_data.fn_get_eligible_panel_refyearset_groups(INT []);

CREATE OR REPLACE FUNCTION 
target_data.fn_get_eligible_panel_refyearset_groups(IN _eligible_refyearset2panel_mappings INT[])
RETURNS TABLE (panel_refyearset_group INT, label VARCHAR(200), description TEXT, label_en VARCHAR(200), description_en TEXT) AS 
$$
DECLARE 
	-- the set of combinations of panel and reference-yearset of an eligible group is a subset of eligible panel and reference-yearset combinations handed over in _eligible_refyearset2panel_mappings INT[] 
	_eligible_groups INT[]; -- eligible groups of panel and reference-yearset combinations      
BEGIN
	
	-- raising an exception on NULL input
	IF _eligible_refyearset2panel_mappings IS NULL then 
		RAISE EXCEPTION 'Input parameter _eligible_refyearset2panel_mappings INT[] is required!';
	END IF; 
	
	WITH w_eligible_groups AS (
		SELECT
			t1.panel_refyearset_group, 
			array_agg(refyearset2panel) -- aggregation of all refyearset and panel combinations for each group    
		FROM
			target_data.t_panel_refyearset_group AS t1
		GROUP BY t1.panel_refyearset_group HAVING array_agg(refyearset2panel) <@ _eligible_refyearset2panel_mappings -- keep only those groups which are subsets of the set eligible combinations
	)
	SELECT array_agg(t1.panel_refyearset_group) FROM w_eligible_groups AS t1 INTO _eligible_groups; 
	
	RETURN QUERY 
	WITH w_eligible_groups AS (
		SELECT
			unnest(_eligible_groups) AS panel_refyearset_group
	)
	SELECT 
		t1.panel_refyearset_group,
		t2."label",
		t2.description,
		t2.label_en,
		t2.description_en
	FROM 
		w_eligible_groups AS t1
	INNER JOIN
		target_data.c_panel_refyearset_group AS t2 
	ON	
		t1.panel_refyearset_group = t2.id;
	
END
$$ 
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER; 

COMMENT ON FUNCTION target_data.fn_get_eligible_panel_refyearset_groups(int[]) IS 
'Function returns combinations of panel and reference-yearsets, for which local densities can be calculated';

GRANT EXECUTE ON FUNCTION target_data.fn_get_eligible_panel_refyearset_groups(int[]) TO public;

/* -- tests
 -- test if an exception is raised 
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_groups(NULL);
 
-- return some groups
 SELECT * FROM target_data.fn_get_eligible_panel_refyearset_groups(ARRAY[1,2,3]);  
 */
