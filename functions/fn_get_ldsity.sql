--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_ldsity(integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity(_target_variable integer[] DEFAULT NULL::integer[], _ldsity_object_type integer DEFAULT 100::integer)
RETURNS TABLE (
id			integer,
target_variable		integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
column_expression	text,
unit_of_measure		integer,
ldsity_object		integer,
object_label		character varying(200),
object_description	text,
object_label_en		character varying(200),
object_description_en	text,
ldsity_object_type	integer,
object_type_label	character varying(200),
object_type_desc	text,
use_negative		boolean,
version			integer
)
AS
$$
DECLARE
_test_total		integer;
BEGIN
	IF _target_variable IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS target_variable,
			t1.label, t1.description, t1.label_en, t1.description_en, 
			t1.column_expression, t1.unit_of_measure, t1.ldsity_object, 
			t6.label, t6.description, t6.label_en, t6.description_en,
			NULL::int, NULL::varchar(200), NULL::text, NULL::boolean, NULL::integer
		FROM target_data.c_ldsity AS t1
		INNER JOIN target_data.c_ldsity_object AS t6
		ON t1.ldsity_object = t6.id
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable[1])
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_target_variable (%)', _target_variable;
		END IF;

		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_type AS t1 WHERE t1.id = _ldsity_object_type)
		THEN RAISE EXCEPTION 'Given ldsity_object_type does not exist in table c_ldsity_object_type (%)', _ldsity_object_type;
		END IF;


		IF array_length(_target_variable,1) > 1
		THEN
			-- test if all given target_variables has the same ldsitys 100 when 1 areal ldsity contribution is devided
			WITH w AS (
				SELECT	t2.target_variable, t3.id AS ldsity, t2.area_domain_category, t2.sub_population_category, t2.use_negative
				FROM target_data.c_target_variable AS t1
				INNER JOIN target_data.cm_ldsity2target_variable AS t2
				ON t1.id = t2.target_variable
				INNER JOIN target_data.c_ldsity AS t3
				ON t2.ldsity = t3.id
				INNER JOIN target_data.c_ldsity_object_type AS t4
				ON t2.ldsity_object_type = t4.id
				WHERE array[t1.id] <@ _target_variable AND t4.id = 100
			),
			w_test1 AS (
				SELECT 	w.target_variable, w.area_domain_category, w.sub_population_category,
					array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) FILTER (WHERE w.use_negative = false) AS ldsity_pozit,
					array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) FILTER (WHERE w.use_negative = true) AS ldsity_negat
				FROM w
				GROUP BY w.target_variable, w.area_domain_category, w.sub_population_category
			),
			w_total AS (
				SELECT ldsity_pozit, ldsity_negat, count(*) AS total
				FROM w_test1
				GROUP BY ldsity_pozit, ldsity_negat, area_domain_category, sub_population_category
			)
			SELECT 	count(*) AS total
			FROM w_total
			INTO _test_total;

			IF _test_total > 1
			THEN	
				RAISE EXCEPTION 'Given target variables (in array) does not have the same ldsitys of ldsity object type 100 (pozitive or negative). There is either different ldsity, or the same ldsity but somehow constrained by area domain category or sub population category.';
			END IF;
		END IF;

		CASE WHEN _ldsity_object_type = 100
		THEN
			-- return distinct objects for given target variables
			RETURN QUERY
			WITH w AS (
				SELECT  t1.id AS target_variable, count(CASE WHEN t2.ldsity_object_type = 200 THEN 1 ELSE NULL END) AS ldsity_200 
				FROM target_data.c_target_variable AS t1
				INNER JOIN target_data.cm_ldsity2target_variable AS t2
				ON t1.id = t2.target_variable
				WHERE array[t1.id] <@ _target_variable
				GROUP BY t1.id
			)
			SELECT  t3.id, t1.id AS target_variable,
				t3.label, t3.description, t3.label_en, t3.description_en,
				t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
				t6.label, t6.description, t6.label_en, t6.description_en,
				t2.ldsity_object_type, t4.label, t4.description, t2.use_negative, t2.version
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			INNER JOIN w AS t5 
			ON t1.id = t5.target_variable
			INNER JOIN target_data.c_ldsity_object AS t6
			ON t3.ldsity_object = t6.id
			WHERE t4.id = 100 AND CASE WHEN array_length(_target_variable,1) > 1 THEN t5.ldsity_200 = 0 ELSE true END
			ORDER BY t1.id, t3.id;

		WHEN _ldsity_object_type = 200
		THEN
			RETURN QUERY
			SELECT  t3.id AS ldsity, t1.id AS target_variable,
				t3.label, t3.description, t3.label_en, t3.description_en,
				t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
				t6.label, t6.description, t6.label_en, t6.description_en,
				t2.ldsity_object_type, t4.label, t4.description, t2.use_negative, t2.version
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			INNER JOIN target_data.c_ldsity_object AS t6
			ON t3.ldsity_object = t6.id
			WHERE array[t1.id] <@ _target_variable AND t4.id = 200
			ORDER BY t1.id, t3.id;
		ELSE
			RAISE EXCEPTION 'Not known value of ldsity object type (%).', _ldsity_object_type;
		END CASE;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity(integer[], integer) IS
'Function returns records from c_ldsity table, optionally for given target_variable.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity(integer[], integer) TO public;
