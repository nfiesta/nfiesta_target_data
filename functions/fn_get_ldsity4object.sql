--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity4object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity4object(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity4object(_ldsity_object integer DEFAULT NULL::integer, _unit_of_measure integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object		integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
column_expression	text,
unit_of_measure		integer,
definition_variant	integer[],
version			integer
)
AS
$$
BEGIN
	RETURN QUERY
	SELECT DISTINCT t1.id, t1.ldsity_object, t1.label, t1.description, t1.label_en, t1.description_en, t1.column_expression, t1.unit_of_measure, t1.definition_variant, t3.version
	FROM target_data.c_ldsity AS t1
	LEFT JOIN target_data.c_ldsity_object AS t2
	ON t1.ldsity_object = t2.id
	LEFT JOIN target_data.cm_ldsity2panel_refyearset_version AS t3
	ON t1.id = t3.ldsity
	WHERE
		CASE WHEN _ldsity_object IS NOT NULL THEN t2.id = _ldsity_object ELSE true END AND
		CASE WHEN _unit_of_measure IS NOT NULL THEN t1.unit_of_measure = _unit_of_measure ELSE true END;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity4object(integer, integer) IS
'Function returns records from c_ldsity table, optionally for given ldsity object and unit_of_measure.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity4object(integer, integer) TO public;
