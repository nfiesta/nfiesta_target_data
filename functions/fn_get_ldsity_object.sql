--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object()
RETURNS TABLE (
id			integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
areal_or_population	integer
)
AS
$$
BEGIN
	RETURN QUERY
	SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.table_name, t1.areal_or_population
	FROM target_data.c_ldsity_object AS t1;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object() IS
'Function returns records from c_ldsity_object table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object() TO public;
