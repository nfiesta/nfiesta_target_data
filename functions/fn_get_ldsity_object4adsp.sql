--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object4adsp
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object4adsp(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object4adsp(_ldsity_object integer, _areal_or_population integer)
RETURNS TABLE (
id			integer,
ldsity_object		integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
areal_or_population	integer
)
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
	THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object_group (%)', _ldsity_object;
	END IF;

	RETURN QUERY
	WITH w_hier AS (
		SELECT _ldsity_object AS ldsity_object, target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	)
	SELECT t3.id, t1.ldsity_object, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, t3.areal_or_population
	FROM w_hier AS t1,
		unnest(t1.ldsity_objects) AS t2(ldsity_object)
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	WHERE t3.areal_or_population = _areal_or_population;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object4adsp(integer, integer) IS
'Function returns records from c_ldsity_object table, optionally for given ldsity object. Returned records belong to the hierarchy of given ldsity object. Result can be subsetted by areal or populational character.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object4adsp(integer, integer) TO public;
