--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object4ld_object_adsp
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(
	_ldsity_object integer, 		-- ldsity object which carries the local density
	_areal_or_population integer, 		-- identificator from c_areal_or_population
	_id integer,				-- id from table c_area_domain or c_sub_population according to areal_or_population id
	_use_negative boolean DEFAULT FALSE	-- boolean value if the local density should consider negative variant of the rule
	)
RETURNS TABLE (
id			integer,			-- ldsity object used for classification, id from table c_ldsity_object
label			character varying(200),		-- label of returned ldsity object
description		text,				-- description dtto
label_en		character varying(200),		-- label_en dtto
description_en		text,				-- description_en dtto
table_name		character varying(200),		-- table of returned ldsity object used for classification
classification_rule	boolean,			-- identification if returned ldsity object has classification rule defined
use_negative		boolean				-- use_negative parameter for returned object
)
AS
$$
BEGIN
	IF _ldsity_object IS NULL
	THEN
		RAISE EXCEPTION 'Parameter _ldsity_object is mandatory and must not be null!';
	END IF;

	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
	THEN
		RAISE EXCEPTION 'Given ldsity object (%) does not exist in table c_ldsity_object!', _ldsity_object;
	END IF;

	CASE
	WHEN _areal_or_population = 100
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
		THEN
			RAISE EXCEPTION 'Given area domain (%) does not exist in table c_area_domain!', _id;
		END IF;

		RETURN QUERY
		WITH w AS (
			SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
		)
		SELECT 	t3.id, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, 
			CASE WHEN t4.ldsity_object IS NOT NULL THEN true ELSE false END AS rule,
			t4.use_negative
		FROM w AS t1,
			unnest(t1.ldsity_objects) AS t2(ldsity_object)
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		LEFT JOIN 
			(SELECT t1.area_domain, t2.ldsity_object, t2.use_negative, count(*) AS total
			FROM target_data.c_area_domain_category AS t1
			INNER JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.id = t2.area_domain_category
			WHERE t1.area_domain = _id AND t2.use_negative = _use_negative
			GROUP BY t1.area_domain, t2.ldsity_object, t2.use_negative
			) AS t4
		ON t3.id = t4.ldsity_object
		WHERE t3.areal_or_population = 100;

	WHEN _areal_or_population = 200
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _id)
		THEN
			RAISE EXCEPTION 'Given population (%) does not exist in table c_sub_population!', _id;
		END IF;

		RETURN QUERY
		WITH w AS (
			SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
		)
		SELECT 	t3.id, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, 
			CASE WHEN t4.ldsity_object IS NOT NULL THEN true ELSE false END AS rule,
			t4.use_negative
		FROM w AS t1,
			unnest(t1.ldsity_objects) AS t2(ldsity_object)
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		LEFT JOIN 
			(SELECT t1.sub_population, t2.ldsity_object, t2.use_negative, count(*) AS total
			FROM target_data.c_sub_population_category AS t1
			INNER JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.sub_population = _id AND t2.use_negative = _use_negative
			GROUP BY t1.sub_population, t2.ldsity_object, t2.use_negative
			) AS t4
		ON t3.id = t4.ldsity_object
		WHERE t3.areal_or_population = 200;
	ELSE
		RAISE EXCEPTION 'Not known value of areal or population (%)!', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer, boolean) IS
'Function returns records from c_ldsity_object table for given area domain or population.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer, boolean) TO public;
