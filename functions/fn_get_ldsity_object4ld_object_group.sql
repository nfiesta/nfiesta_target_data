--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object4ld_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object4ld_object_group(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object4ld_object_group(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
areal_or_population	integer
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t3.id AS ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, t1.table_name, t1.areal_or_population
		FROM target_data.c_ldsity_object AS t1
		INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
		ON t1.id = t2.ldsity_object
		INNER JOIN target_data.c_ldsity_object_group AS t3
		ON t2.ldsity_object_group = t3.id
		ORDER BY t1.id, t3.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		SELECT t1.id, t3.id AS ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, t1.table_name, t1.areal_or_population
		FROM target_data.c_ldsity_object AS t1
		INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
		ON t1.id = t2.ldsity_object
		INNER JOIN target_data.c_ldsity_object_group AS t3
		ON t2.ldsity_object_group = t3.id
		WHERE t3.id = _ldsity_object_group;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object4ld_object_group(integer) IS
'Function returns records from c_ldsity_object table, optionally for given topic.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object4ld_object_group(integer) TO public;
