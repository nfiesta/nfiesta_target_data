--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_objects(integer[]) CASCADE;

create or replace function target_data.fn_get_ldsity_objects(_ldsity_objects integer[])
returns integer[]
as
$$
declare
		_res_1	integer;
		_res	integer[];
begin
		if _ldsity_objects is null
		then
			raise exception 'Error 01: fn_get_ldsity_objects: The input argument _ldsity_objects must not be NULL !';
		end if;	
	
		select upper_object from target_data.c_ldsity_object where id = _ldsity_objects[1]
		into _res_1;	
	
		if _res_1 is null
		then
			_res := _ldsity_objects;
		else
			_res := target_data.fn_get_ldsity_objects(array[_res_1] || _ldsity_objects);
		end if;
	
	return _res;

end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_get_ldsity_objects(integer[]) IS
'The function for the specified ldsity object hierarchically returns a list of objects.';

