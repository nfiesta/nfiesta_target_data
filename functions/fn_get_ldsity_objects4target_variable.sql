--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_objects4target_variable(integer) CASCADE;

create or replace function target_data.fn_get_ldsity_objects4target_variable(_target_variable integer)
returns table
(
	id_cm_ldsity2target_variable	integer,
	ldsity_object			integer,
	use_negative			boolean,
	ldsity_object_type		integer
)
AS
$$
declare
		_check_exist_id		integer[];
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_get_ldsity_objects4target_variable: The input argument _target_variable must not be NULL !';
		end if;	
	
		select array_agg(id order by id) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable
		into _check_exist_id;

		if _check_exist_id is null
		then
			raise exception 'Error 02: fn_get_ldsity_objects4target_variable: For input argument _target_variable = % not exist any record in cm_ldsity2target_variable table!',_target_variable;
		end if;

		RETURN QUERY	
		SELECT
				a.id as id_cm_ldsity2target_variable,
				b.ldsity_object,
				a.use_negative,
				a.ldsity_object_type
		FROM
				(select * from target_data.cm_ldsity2target_variable where id in (select unnest(_check_exist_id))) as a
		INNER JOIN
				target_data.c_ldsity as b on a.ldsity = b.id
		ORDER
				BY a.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_ldsity_objects4target_variable(integer) IS
'The function for the specified target variable returns a list of ldsity objects.';

grant execute on function target_data.fn_get_ldsity_objects4target_variable(integer) TO public;

