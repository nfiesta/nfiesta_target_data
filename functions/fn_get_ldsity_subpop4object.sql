--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity_subpop4object
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_ldsity_subpop4object(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_subpop4object(_ldsity_object integer)
RETURNS TABLE (
id			integer,
ldsity_object		integer,
object_label		character varying(200),
object_description	text,
object_label_en		character varying(200),
object_description_en	text,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
column_expression	text,
unit_of_measure		integer,
definition_variant	integer[],
version			integer
)
AS
$$
BEGIN

	IF (SELECT areal_or_population FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object) != 100
	THEN
		RAISE EXCEPTION 'Given ldsity object (%) is not of areal type.', _ldsity_object;
	END IF;

	RETURN QUERY
	WITH RECURSIVE w_object AS (
		SELECT t1.id AS ldsity_object, t1.areal_or_population
		FROM target_data.c_ldsity_object AS t1
		WHERE t1.id = _ldsity_object
		UNION ALL
		SELECT t2.id AS ldsity_object, t2.areal_or_population
		FROM w_object AS t1
		INNER JOIN target_data.c_ldsity_object AS t2
		ON t1.ldsity_object = t2.upper_object
	)
	SELECT 	DISTINCT t2.id, t1.ldsity_object, 
		t3.label, t3.description, t3.label_en, t3.description_en, 
		t2.label, t2.description, t2.label_en, t2.description_en, 
		t2.column_expression, t2.unit_of_measure, t2.definition_variant,
		t4.version
	FROM w_object AS t1
	INNER JOIN target_data.c_ldsity AS t2
	ON t1.ldsity_object = t2.ldsity_object
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	LEFT JOIN target_data.cm_ldsity2panel_refyearset_version AS t4
	ON t2.id = t4.ldsity
	WHERE t1.areal_or_population = 200
	ORDER BY t1.ldsity_object, t2.label;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_subpop4object(integer) IS
'Function returns records from c_ldsity table, optionally for given ldsity object and unit_of_measure.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_subpop4object(integer) TO public;
