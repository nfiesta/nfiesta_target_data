--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_modified_ldsity_column_expression(text, integer, integer) CASCADE;

create or replace function target_data.fn_get_modified_ldsity_column_expression
(
	_ldsity_column_expression		text,
	_position4ldsity				integer,
	_ldsity_ldsity_object			integer
)
returns text
as
$$
declare
		_column_name_i_array		text[];
begin
		if _ldsity_column_expression is null
		then
			raise exception 'Error 01: fn_get_modified_ldsity_column_expression: Input argument _ldsity_column_expression must not by null !';
		end if;

		if _position4ldsity is null
		then
			raise exception 'Error 02: fn_get_modified_ldsity_column_expression: Input argument _position4ldsity must not by null !';
		end if;

		if _ldsity_ldsity_object is null
		then
			raise exception 'Error 03: fn_get_modified_ldsity_column_expression: Input argument _ldsity_ldsity_object must not by null !';
		end if;

		select array_agg(column_name order by ordinal_position) from information_schema.columns
	 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_ldsity_object)
	 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_ldsity_object)
		into _column_name_i_array;

		for i in 1..array_length(_column_name_i_array,1)
		loop
			--_ldsity_column_expression := replace(_ldsity_column_expression,_column_name_i_array[i],concat(_column_name_i_array[i],'_',_position4ldsity));
			--_ldsity_column_expression := regexp_replace(_ldsity_column_expression,concat('(\+|\*|\-|\/| |^)(',_column_name_i_array[i],')(\+|\*|\-|\/| |$)'),'\1\2_2\3','g');
			--_ldsity_column_expression := regexp_replace(_ldsity_column_expression,concat('(\+|\*|\-|\/| |\A)(',_column_name_i_array[i],')(\+|\*|\-|\/| |\Z)'),'\1\2_2\3','g');
			--_ldsity_column_expression := regexp_replace(_ldsity_column_expression,concat('(\+|\*|\-|\/| |\A|\(|\)|\=)(',_column_name_i_array[i],')(\+|\*|\-|\/| |\Z|\(|\)|\=)'),concat('\1\2','_',_position4ldsity,'\3'),'g');   
			_ldsity_column_expression := regexp_replace(_ldsity_column_expression,concat('(\+|\*|\-|\/| |\A|\(|\)|\=|,)(',_column_name_i_array[i],')(\+|\*|\-|\/| |\Z|\(|\)|\=|,|\:)'),concat('\1\2','_',_position4ldsity,'\3'),'g');
		end loop;
	
		return _ldsity_column_expression;	
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_modified_ldsity_column_expression(text, integer, integer) is
'The function modified input ldsity column exression for given ldsity object and their position in ldsity objects.';

grant execute on function target_data.fn_get_modified_ldsity_column_expression(text, integer, integer) to public;

