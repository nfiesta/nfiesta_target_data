--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_panel_refyearset
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_panel_refyearset(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_panel_refyearset(_panel_refyearset integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
panel_id		integer,
panel			varchar(20),
panel_label		varchar(120),
refyearset_id		integer,
reference_year_set	varchar(20),
refyearset_label	varchar(120)
)
AS
$$
BEGIN
	IF _panel_refyearset IS NULL
	THEN
		RETURN QUERY
		SELECT t2.id, t1.id, t1.panel, t1.label, t3.id, t3.reference_year_set, t3.label
		FROM sdesign.t_panel AS t1
		INNER JOIN sdesign.cm_refyearset2panel_mapping AS t2
		ON t1.id = t2.panel
		INNER JOIN sdesign.t_reference_year_set AS t3
		ON t2.reference_year_set = t3.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM sdesign.cm_refyearset2panel_mapping AS t1 WHERE t1.id = _panel_refyearset)
		THEN RAISE EXCEPTION 'Given combination of panel and reference year set does not exist in table cm_refyearset2panel_mapping (%)', _panel_refyearset;
		END IF;

		RETURN QUERY
		SELECT t2.id, t1.id, t1.panel, t1.label, t3.id, t3.reference_year_set, t3.label
		FROM sdesign.t_panel AS t1
		INNER JOIN sdesign.cm_refyearset2panel_mapping AS t2
		ON t1.id = t2.panel
		INNER JOIN sdesign.t_reference_year_set AS t3
		ON t2.reference_year_set = t3.id
		WHERE t2.id = _panel_refyearset;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_panel_refyearset(integer) IS
'Function returns records from cm_refyearset2panel_mappping table, optionally for given panel_refyearset.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_panel_refyearset(integer) TO public;
