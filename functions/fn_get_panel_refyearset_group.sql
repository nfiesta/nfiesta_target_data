--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_panel_refyearset_group
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_panel_refyearset_group(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_panel_refyearset_group(_id integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_panel_refyearset_group AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_panel_refyearset_group AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given group of panels and refyearsets does not exist in table c_panel_refyearset_group (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_panel_refyearset_group AS t1
		WHERE t1.id = _id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_panel_refyearset_group(integer) IS
'Function returns records from c_panel_refyearset_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_panel_refyearset_group(integer) TO public;
