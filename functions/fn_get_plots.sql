--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_get_plots
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_plots(integer[], integer, boolean) CASCADE;

create or replace function target_data.fn_get_plots
(
	_refyearset2panel_mapping integer[],
	_state_or_change integer,
	_use_negative boolean
)
returns table
(
	gid integer,
	cluster_configuration integer,
	reference_year_set4join integer,
	reference_year_set integer,
	panel integer,
	stratum integer
)
as
$$
declare
		_reference_year_set_from_cm_i		integer;
		_refyearset4join_i					integer;
		_refyearset4avds_i					integer;
		_query_i							text;
		_query_res							text;
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_get_plots: Input argument _refyearset2panel_mapping must not be NULL !';
		end if;
		
		if _state_or_change is null
		then
			raise exception 'Error 02: fn_get_plots: Input argument _state_or_change must not be NULL !';
		end if;
	
		if _use_negative is null
		then
			raise exception 'Error 03: fn_get_plots: Input argument _use_negative must not be NULL !';
		end if;

		select array_agg(t.res order by t.res) from
		(select unnest(_refyearset2panel_mapping) as res) as t
		into _refyearset2panel_mapping;
	
		for i in 1..array_length(_refyearset2panel_mapping,1)
		loop
				select cm.reference_year_set from sdesign.cm_refyearset2panel_mapping as cm where id = _refyearset2panel_mapping[i]
				into _reference_year_set_from_cm_i;
			
				if _reference_year_set_from_cm_i is null
				then
					raise exception 'Error 04: fn_get_plots: For input argument _refyearset2panel_mapping = %  not found any value reference_year_set in table sdesign.cm_refyearset2panel_mapping!',_refyearset2panel_mapping[i];
				end if;
			
				case
					when _state_or_change = 100 and _use_negative = false
							then
								_refyearset4join_i := _reference_year_set_from_cm_i;
								_refyearset4avds_i := _reference_year_set_from_cm_i;
								
							
					when _state_or_change = 200 and _use_negative = true
							then
								_refyearset4join_i :=	(
														select reference_year_set_begin
														from sdesign.t_reference_year_set
														where id = _reference_year_set_from_cm_i
														);
	
								if _refyearset4join_i is null
								then
										raise exception 'Chyba 05: fn_get_plots: For input argument _refyearset2panel_mapping = %  not found any value reference_year_set_begin in table sdesign.t_reference_year_set!',_refyearset2panel_mapping[i];
								end if;
	
								_refyearset4avds_i := _reference_year_set_from_cm_i;
								
							
					when _state_or_change = 200 and _use_negative = false
							then
								_refyearset4join_i :=	(
														select reference_year_set_end
														from sdesign.t_reference_year_set
														where id = _reference_year_set_from_cm_i
														);
	
								if _refyearset4join_i is null
								then
										raise exception 'Chyba 06: fn_get_plots: For input argument _refyearset2panel_mapping = %  not found any value reference_year_set_end in table sdesign.t_reference_year_set!',_refyearset2panel_mapping[i];
								end if;
	
								_refyearset4avds_i := _reference_year_set_from_cm_i;
								

					when _state_or_change = 300
							then
								_refyearset4join_i := _reference_year_set_from_cm_i;
								_refyearset4avds_i := _reference_year_set_from_cm_i;					
						
					else raise exception 'Error 07: fn_get_plots: This variant is not implemented yet !';
				end case;
				
				
					_query_i := concat(					
					'					
					select
							f_p_plot.gid,
							t_cluster_configuration.id as cluster_configuration,
							',_refyearset4join_i,' as reference_year_set4join,
							',_refyearset4avds_i,' as reference_year_set4avds,
							cm_refyearset2panel_mapping.panel,
							t_stratum.id as stratum
					from
								sdesign.cm_refyearset2panel_mapping
					inner join	sdesign.t_panel ON t_panel.id = cm_refyearset2panel_mapping.panel	
					inner join	sdesign.cm_cluster2panel_mapping ON cm_cluster2panel_mapping.panel = t_panel.id
					inner join	sdesign.t_cluster ON t_cluster.id = cm_cluster2panel_mapping."cluster"
					inner join	sdesign.f_p_plot ON f_p_plot."cluster" = t_cluster.id
					inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
					inner join	sdesign.t_cluster_configuration ON (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
					inner join	sdesign.t_stratum ON t_panel.stratum = t_stratum.id
					where
							cm_refyearset2panel_mapping.id = ',_refyearset2panel_mapping[i],'
					');
				
					if i = 1
					then
						_query_res := _query_i;
					else
						_query_res := concat(_query_res,' union all ',_query_i);
					end if;
		end loop;

	---------------------------------------------
	return query execute ''||_query_res||'';
	---------------------------------------------
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_plots(integer[], integer, boolean) IS
'The function for the specified list of panels and reference_year_sets returns a list of points for local densities.';

grant execute on function target_data.fn_get_plots(integer[], integer, boolean) to public;