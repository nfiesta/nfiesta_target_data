--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_unit_of_measure
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_unit_of_measure(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_unit_of_measure(_ldsity integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
ldsity		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _ldsity IS NULL
	THEN
		RETURN QUERY
		SELECT DISTINCT t1.id, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_unit_of_measure AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
		END IF;

		RETURN QUERY
		SELECT DISTINCT t1.id, t2.id AS ldsity, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_unit_of_measure AS t1
		INNER JOIN target_data.c_ldsity AS t2
		ON t1.id = t2.unit_of_measure
		WHERE t2.id = _ldsity;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_unit_of_measure(integer) IS
'Function returns records from c_unit_of_measrue table, optionally for given ldsity.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_unit_of_measure(integer) TO public;
