--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_unit_of_measure4ld_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_unit_of_measure4ld_object_group(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_unit_of_measure4ld_object_group(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_unit_of_measure AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		WITH w_data AS (
			SELECT DISTINCT t1.id AS unit_of_measure, t4.ldsity_object
			FROM target_data.c_unit_of_measure AS t1
			INNER JOIN target_data.c_ldsity AS t2
			ON t1.id = t2.unit_of_measure
			INNER JOIN target_data.c_ldsity_object AS t3
			ON t2.ldsity_object = t3.id
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t4
			ON t3.id = t4.ldsity_object
			WHERE t4.ldsity_object_group = _ldsity_object_group
		), w_count AS (
			SELECT unit_of_measure, count(*) AS total
			FROM w_data
			GROUP BY unit_of_measure
		), w_objects AS (
			SELECT count(DISTINCT ldsity_object) AS total_objects
			FROM w_data
		), w_unit AS (
			SELECT 
				unit_of_measure
			FROM w_count AS t1
			INNER JOIN w_objects AS t2
			ON t1.total = t2.total_objects
		)			
		SELECT t1.id, _ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_unit_of_measure AS t1
		INNER JOIN w_unit AS t2
		ON t1.id = t2.unit_of_measure
		ORDER BY t1.id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_unit_of_measure4ld_object_group(integer) IS
'Function returns records from c_unit_of_measrue table, optionally for given ldsity object group.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_unit_of_measure4ld_object_group(integer) TO public;
