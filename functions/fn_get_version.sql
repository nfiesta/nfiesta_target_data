--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_version
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_version(integer, integer, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_version(
	_target_variable integer DEFAULT NULL::integer, -- target variable (id from c_target_variable)
	_ldsity integer DEFAULT NULL::integer, 		-- ldsity contribution (id from c_ldsity)
	_use_negative boolean DEFAULT NULL::boolean	-- use_negative, only with combination target_variable NOT NULL
	)
RETURNS TABLE (
id		integer,			-- id from table c_version
target_variable integer,			-- target variable (id from c_target_variable)
ldsity		integer,			-- local density contribution (id from c_ldsity)
label		character varying(200),		-- label from c_version
description	text,				-- description dtto
label_en	character varying(200),
description_en	text,
use_negative	boolean
)
AS
$$
BEGIN
	IF _ldsity IS NULL
	THEN
		IF _target_variable IS NULL
		THEN
			IF _use_negative IS NOT NULL THEN
				RAISE EXCEPTION 'Use negative can be specified only in combination with not null target variable.';
			END IF;

			RETURN QUERY
			SELECT DISTINCT t1.id, NULL::integer, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en, NULL::boolean
			FROM target_data.c_version AS t1;
		ELSE
			IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
			THEN RAISE EXCEPTION 'Given target variable does not exist in table c_target_variable (%)', _target_variable;
			END IF;

			IF NOT EXISTS (SELECT * FROM target_data.cm_ldsity2target_variable AS t1
					WHERE t1.target_variable = _target_variable AND
					CASE WHEN _use_negative IS NOT NULL THEN t1.use_negative = _use_negative ELSE true END)
			THEN RAISE EXCEPTION 'Given target_variable and use_negetavie combination does not exist.';
			END IF;

			RETURN QUERY
			SELECT t4.id, t2.target_variable, t2.ldsity, t4.label, t4.description, t4.label_en, t4.description_en, t2.use_negative
			FROM target_data.c_ldsity AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.ldsity
			INNER JOIN target_data.c_version AS t4
			ON t2.version = t4.id
			WHERE t2.target_variable = _target_variable AND 
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			ORDER BY t2.id;
		END IF;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
		END IF;

		IF _target_variable IS NULL 
		THEN
			RAISE EXCEPTION 'If ldsity is specified, target variable must not be null.';
		ELSE
			IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
			THEN RAISE EXCEPTION 'Given target variable does not exist in table c_target_variable (%)', _target_variable;
			END IF;

			IF NOT EXISTS (SELECT * FROM target_data.cm_ldsity2target_variable AS t1
					WHERE t1.target_variable = _target_variable AND t1.ldsity = _ldsity AND
					CASE WHEN _use_negative IS NOT NULL THEN t1.use_negative = _use_negative ELSE true END)
			THEN RAISE EXCEPTION 'Given ldsity is not part of given target_variable.';
			END IF;

			RETURN QUERY
			SELECT t4.id, t2.target_variable, t2.ldsity, t4.label, t4.description, t4.label_en, t4.description_en, t2.use_negative
			FROM target_data.c_ldsity AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.ldsity
			INNER JOIN target_data.c_version AS t4
			ON t2.version = t4.id
			WHERE t1.id = _ldsity AND t2.target_variable = _target_variable AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			ORDER BY t2.id;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_version(integer, integer, boolean) IS
'Function returns records from c_version table, optionally for given target variable and ldsity.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_version(integer, integer, boolean) TO public;
