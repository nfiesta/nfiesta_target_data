--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_areal_or_pop
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_areal_or_pop(integer, varchar(200), text, varchar(200), text, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_areal_or_pop(_areal_or_population integer, _label varchar(200), _description text, _label_en varchar(200), _description_en text, _classification_type integer DEFAULT 100, _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
BEGIN
	IF _label IS NULL OR _description IS NULL OR _label_en IS NULL OR _description_en IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of label or description or label_en or descrioption_en is null (%, %, %, %).', _label, _description, _label_en, _description_en;
	END IF; 

	IF _id IS NULl
	THEN
		CASE 
		WHEN _areal_or_population = 100 THEN
			INSERT INTO target_data.c_area_domain(label, description, label_en, description_en, classification_type)
			SELECT _label, _description, _label_en, _description_en, _classification_type
			RETURNING id
			INTO _id;

		WHEN _areal_or_population = 200 THEN
			INSERT INTO target_data.c_sub_population(label, description, label_en, description_en, classification_type)
			SELECT _label, _description, _label_en, _description_en, _classification_type
			RETURNING id
			INTO _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	ELSE
		CASE WHEN _areal_or_population = 100 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _id;
			END IF;

			UPDATE target_data.c_area_domain
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_area_domain.id = _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain does not exist in table c_sub_population (%)', _id;
			END IF;

			UPDATE target_data.c_sub_population
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_sub_population.id = _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_areal_or_pop(integer, character varying, text, character varying, text, integer, integer) IS
'Functions inserts a record into table c_area_domain or c_sub_population based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_areal_or_pop(integer, character varying, text, character varying, text, integer, integer) TO public;
