--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categories
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_categories(integer, integer, varchar[], text[], varchar[], text[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_categories(_parent integer, _areal_or_population integer, _label varchar(200)[], _description text[], _label_en varchar(200)[], _description_en text[])
RETURNS TABLE (
id		integer,
label		varchar,
description	text,
label_en	varchar,
description_en	text
)
AS
$$
BEGIN
	IF _parent IS NULL OR _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent indicator (%) or label/descriptioin is null (%, %)!', _parent, _label, _description;
	END IF; 

	IF array_length(_label,1) != array_length(_description,1) OR
		array_length(_label_en,1) != array_length(_description_en,1)
	THEN
		RAISE EXCEPTION 'Given arrays of label and description (%,%) or label_en and description_en (%,%) must be of same length!', _label, _description, _label_en, _description_en;
	END IF;

	CASE 
	WHEN _areal_or_population = 100 THEN

		RETURN QUERY
		INSERT INTO target_data.c_area_domain_category(area_domain, label, description, label_en, description_en)
		SELECT 
			_parent,
			t1.label, t2.description, t3.label_en, t4.description_en
		FROM
			unnest(_label) WITH ORDINALITY AS t1(label, id)
		INNER JOIN
			unnest(_description) WITH ORDINALITY AS t2(description, id)
		ON t1.id = t2.id
		LEFT JOIN unnest(_label_en) WITH ORDINALITY AS t3(label_en, id)
		ON t1.id = t3.id
		LEFT JOIN unnest(_description_en) WITH ORDINALITY AS t4(description_en, id)
		ON t1.id  = t4.id
		RETURNING id, label, description, label_en, description_en;

	WHEN _areal_or_population = 200 THEN

		RETURN QUERY
		INSERT INTO target_data.c_sub_population_category(sub_population, label, description, label_en, description_en)
		SELECT 
			_parent,
			t1.label, t2.description, t3.label_en, t4.description_en
		FROM
			unnest(_label) WITH ORDINALITY AS t1(label, id)
		INNER JOIN
			unnest(_description) WITH ORDINALITY AS t2(description, id)
		ON t1.id = t2.id
		LEFT JOIN unnest(_label_en) WITH ORDINALITY AS t3(label_en, id)
		ON t1.id = t3.id
		LEFT JOIN unnest(_description_en) WITH ORDINALITY AS t4(description_en, id)
		ON t1.id  = t4.id
		RETURNING id, label, description, label_en, description_en;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;

	RETURN;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_categories(integer, integer, varchar(200)[], text[], varchar(200)[], text[]) IS
'Functions inserts a record into table c_area_domain_category or c_sub_population_category based on given parameters.';

