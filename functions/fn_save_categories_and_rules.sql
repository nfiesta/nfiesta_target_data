--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categories_and_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], boolean[], integer[], integer[][], integer[][]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_categories_and_rules(
	_ldsity_object integer,						-- id from c_ldsity_object, for which object the classification rules are valid
	_areal_or_pop integer,						-- id from c_areal_or_population
	_classification_type integer,					-- id from c_classification_type
	_label varchar(200), 						-- label of the area domain/population
	_description text, 						-- description of the area domain/population
	_label_en varchar(200), 					-- english label of the area domain/population
	_description_en text,						-- english description of the area domain/population
	_cat_label varchar(200)[], 					-- label of the area domain/population categories
	_cat_description text[],					-- description of the area domain/population categories
	_cat_label_en varchar(200)[], 					-- english label of the area domain/population categories
	_cat_description_en text[],					-- english description of the area domain/population categories
	_rules text[], 							-- text representation of the classification rules which are used to classify an object
	_use_negative boolean[],					-- if there is standard classification_type, all values are false, if 
									-- change type present then each category has to have both rules variants (with use negative true and false)
									-- currently there has to be all values sorted and cannot be mixed (e.g. an array [false, true, false, true] is prohibited)
	_panel_refyearset integer[][], 					-- an array of panel and reference year set combinations (id from table cm_refyearset2panel_mapping)
									-- in which are classification rules applicable, the second dimension is because of use_negative variant, where
									-- there is a different set of combinations for true and false variants
	_adc integer[][] DEFAULT NULL::int[][], 			-- optional area domain categories contraint (only for areal), e.g. accessible land
	_spc integer[][] DEFAULT NULL::int[][]				-- dtto for populational, e.g. living stem for age classification rule, it is an array of arrays hence
									--  more categories from one population can fit the constraint for each rule, 
									-- e.g. for rule which classifies high intensity of damage a prerequisite can be two categories:
									-- damaged by bark stripping, damaged by human etc.
)
RETURNS TABLE (
	areal_or_pop			integer,
	cat_label			varchar(200),
	cat_label_en			varchar(200),
	parent_id			integer,
	category_id			integer,	
	classification_rule		integer,
	refyearset2panel		integer
) 
AS
$$
DECLARE
_total_false integer;
_total_true integer;
BEGIN
	IF array_length(_cat_label,1) != array_length(_cat_description,1) OR
		array_length(_cat_label_en,1) != array_length(_cat_description_en,1)
	THEN
		RAISE EXCEPTION 'Given arrays of label and description (%,%) or label_en and description_en (%,%) must be of same length!', _label, _description, _label_en, _description_en;
	END IF;

	WITH w AS (
		SELECT use_negative, id
		FROM unnest(_use_negative) WITH ORDINALITY AS t(use_negative,id)
	)
	SELECT count(*)::numeric AS total
	FROM w
	WHERE use_negative = false
	INTO _total_false;
	
	WITH w AS (
		SELECT use_negative, id
		FROM unnest(_use_negative) WITH ORDINALITY AS t(use_negative,id)
	)
	SELECT count(*)::numeric AS total
	FROM w
	WHERE use_negative = true
	INTO _total_true;

	IF _classification_type = 100 THEN
		IF array_length(_rules,1) != array_length(_cat_label,1)
		THEN
			RAISE EXCEPTION 'Given arrays of rules (%) must be the same length as array of labels (%)!', _rules, _cat_label;
		END IF;

		IF _total_true > 0
		THEN 
			RAISE EXCEPTION 'If calculation type = 100 then no use_negative = true classification rules can be passed.';
		END IF;
	ELSE
		IF array_length(_rules,1) != (array_length(_cat_label,1) * 2.0) -- each category has 2 rules (use_negative true and false)
		THEN
			RAISE EXCEPTION 'Given arrays of rules (%) must have twice the length of labels array (%)!', _rules, _cat_label;
		END IF;

		IF _total_false != _total_true
		THEN 
			RAISE EXCEPTION 'Both variants of use_negative classification rules (false %, true %) has to have the same totals.', _total_false, _total_true;
		END IF;

		IF EXISTS (
			WITH w AS (
				SELECT 
					use_negative, id, ceil( round((id/(count(*) OVER())::numeric * 2),12) ) AS array_id
				FROM unnest(_use_negative) WITH ORDINALITY AS t1(use_negative, id)
			), w_test AS (
				SELECT array_id, count(DISTINCT use_negative) AS test
				FROM w
				GROUP BY array_id
			) SELECT *
			FROM w_test
			WHERE test != 1)
		THEN 
			RAISE EXCEPTION 'Given array of use_negative values must be ordered. First use_negative true or false and then the other one.';
		END IF;

	END IF;

	RETURN QUERY
	WITH w_parent AS (
		SELECT t1 AS id FROM target_data.fn_save_areal_or_pop(_areal_or_pop, _label, _description, _label_en, _description_en, _classification_type) AS t1
	), w_categories AS (
		SELECT 	t1.id AS parent_id, -- t1.label, t1.label_en,
			t2.id AS array_id,
			t6 AS category_id, t2.label AS label_cat, t4.label_en AS label_en_cat
		FROM 	w_parent AS t1,
			unnest(_cat_label) WITH ORDINALITY AS t2(label, id)
		LEFT JOIN unnest(_cat_description) WITH ORDINALITY AS t3(description, id) ON t2.id = t3.id
		LEFT JOIN unnest(_cat_label_en) WITH ORDINALITY AS t4(label_en, id) ON t3.id = t4.id
		LEFT JOIN unnest(_cat_description_en) WITH ORDINALITY AS t5(description_en, id) ON t4.id = t5.id,
			target_data.fn_save_category(_areal_or_pop, t2.label, t3.description, t4.label_en, t5.description_en, t1.id) AS t6
	), w_rules AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, t2.rule, t4 AS rule_id, t1.label_cat, t1.label_en_cat
		FROM 	w_categories AS t1
		INNER JOIN unnest(_rules) WITH ORDINALITY AS t2(rule,id) 
		ON t1.array_id = t2.id OR t1.array_id = (t2.id - array_length(_cat_label,1)) -- in case of use_negative rules, there will be array overflow (twice length)
		INNER JOIN unnest(_use_negative) WITH ORDINALITY AS t3(use_negative,id) 
		ON t2.id = t3.id,
			target_data.fn_save_classification_rule(t1.category_id, _areal_or_pop, _ldsity_object, t2.rule, t3.use_negative) AS t4
	),
	w_rule_agg AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, t1.label_cat, t1.label_en_cat,
			array_agg(t1.rule_id ORDER BY array_id) AS rule_ids
		FROM w_rules AS t1
		GROUP BY t1.parent_id, t1.array_id, t1.category_id, t1.label_cat, t1.label_en_cat
	), w_hier_adc AS (
		INSERT INTO target_data.t_adc_hierarchy (variable_superior, variable, dependent)
		SELECT t2.adc, t1.category_id, true
		FROM	w_categories AS t1,
			(SELECT 
				adc, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_adc,1)),12) ) AS array_id
			FROM unnest(_adc) WITH ORDINALITY AS t1(adc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id AND t2.adc IS NOT NULL
	), w_hier_spc AS (
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable, dependent)
		SELECT t2.spc, t1.category_id, true
		FROM	w_categories AS t1,
			(SELECT 
				spc, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_spc,1)),12) ) AS array_id
			FROM unnest(_spc) WITH ORDINALITY AS t1(spc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id AND t2.spc IS NOT NULL
	)
	SELECT  _areal_or_pop, t1.label_cat, t1.label_en_cat,
		 t1.parent_id, t1.category_id, t2.classification_rule, t2.refyearset2panel
	FROM 
		w_rule_agg AS t1,
		target_data.fn_save_class_rule_mapping(_areal_or_pop, t1.rule_ids, _use_negative, _panel_refyearset) AS t2
	;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_categories_and_rules(integer, integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], boolean[], integer[], integer[][], integer[][]) IS
'Function inserts all necessary data into lookups/tables with area_domain/sub_population categories and its classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_categories_and_rules(integer, integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], boolean[], integer[], integer[][], integer[][]) TO public;
