--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categorization_setup
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_categorization_setup(integer, integer[], integer[], integer[][], integer[], integer[][]) CASCADE;

create or replace function target_data.fn_save_categorization_setup
(
	_target_variable		integer,
	_target_variable_cm		integer[],
	_area_domain			integer[],
	_area_domain_object		integer[][],
	_sub_population			integer[],
	_sub_population_object	integer[][]
)
returns void
as
$$
declare
		_array_id							integer[];
		_array_id4check						integer[];
		_lot_100							integer;
		_lot_200							integer;
		_variant							integer;	
		_area_domain4input					integer[];
		_area_domain_object4input			integer[];
		_sub_population4input				integer[];
		_sub_population_object4input		integer[];	
		_area_domain4input_text				text;
		_area_domain_object4input_text		text;
		_sub_population4input_text			text;
		_sub_population_object4input_text	text;
		_string4attr						text;
		_query_res							text;
		_s4ij_p1							text;
		_s4ij_p2							text;
		_s4ij_p3							text;
		_s4ij_p4							text;
		_s4ij_p5							text;
		_string4incomming					text;
		_string4inner_join					text;
		_array_id_200						integer[];
		_string_inner_join					text;
		_string_incomming_0					text;
		_string_incomming					text;
		_string_check_1						text;
		_string_check_2						text;
		_string_check_3						text;
		_string_check_4						text;
		_string_insert_1					text;
		_string_insert_2					text;
		/*
		_s4da_p1							text;
		_s4da_p2							text;
		_s4da_p3							text;
		_s4da_p4							text;
		_s4ia_p1							text;
		_s4ia_p2							text;
		_s4ia_p3							text;
		_s4ia_p4							text;
		_s4result_p1						text;
		_s4result_p2						text;
		_string4database_agg				text;
		_string4incomming_agg				text;	
		_string4result						text;
		_string_agg_and_result				text;
		*/
		_res								text;

		_use_negative_id_bc					boolean;
		_use_negative_ad_4input_text		text;
		_classification_type_ad				integer;
		_use_negative_ad_4input_text_i		text;
		_use_negative_ad_4input_text_join	text;
		_use_negative_sp_4input_text		text;
		_classification_type_sp				integer;
		_use_negative_sp_4input_text_i		text;
		_use_negative_sp_4input_text_join	text;

		_target_variable_cm_order			integer[];
		_area_domain_object_i				integer[];
		_area_domain_object_order			integer[][];
		_sub_population_object_i			integer[];
		_sub_population_object_order		integer[];

		_check_input_ad_all					integer;
		_check_input_ad_null				integer;
		_check_input_ad						integer[];
		_check_input_ad_object				integer[];
		_check_input_ad_errors				integer[];

		_check_input_sp_all					integer;
		_check_input_sp_null				integer;
		_check_input_sp						integer[];
		_check_input_sp_object				integer[];
		_check_input_sp_errors				integer[];	

		_ldsity_object_type_ad				integer;
		_ldsity_object_type_sp				integer;
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_save_categorization_setup: The input argument _target_variable must not be NULL !';
		end if;

		if _target_variable_cm is null
		then
			raise exception 'Error 02: fn_save_categorization_setup: The input argument _target_variable_cm must not be NULL !';
		end if;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where target_variable = _target_variable
		into
				_array_id;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where id in (select unnest(_target_variable_cm))
		into
				_array_id4check;			

		if _array_id != _array_id4check
			then raise exception 'Error 03: fn_save_categorization_setup: The internal argument _array_id = % and internal argument _array_id4check = % are not the same!',_array_id, _array_id4check;
		end if;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 04: fn_save_categorization_setup: This variant is not implemented yet!';
		end if;

		------------------------------------------------------------------
		-- ordering input arguments, order by ldsity_object_type and id
		------------------------------------------------------------------	
		with
		w1 as	(
				select
						row_number() over() as id4order,
						t.id as target_variable_cm
				from
						(
						select
								id,
								ldsity_object_type
						from
								target_data.cm_ldsity2target_variable cltv
						where
								id in (select unnest(_target_variable_cm))
						order
								by ldsity_object_type, id
						) as t
				order
						by ldsity_object_type, id
				)
		select
				array_agg(w1.target_variable_cm order by w1.id4order) as target_variable_cm
		from
				w1
		into
				_target_variable_cm_order;
		------------------------------------------------------------------
		-- check passed object for input area domains
		------------------------------------------------------------------
		if _area_domain is not null
		then
			if array_length(_target_variable_cm_order,1) != array_length(_area_domain_object,1)
			then
				raise exception 'Error 05: fn_get_categorization_setup: If the input argument _area_domain IS NOT NULL then the count of elements in the input arguments _target_variable_cm and _area_domain_object must be equal!';
			end if;

			for i in 1..array_length(_target_variable_cm_order,1)
			loop
				with
				w1 as	(
						select
								_area_domain as area_domain,
								_area_domain_object[array_position(_target_variable_cm, _target_variable_cm_order[i]):array_position(_target_variable_cm, _target_variable_cm_order[i])] as area_domain_object
						)
				,w2 as	(
						select
								unnest(w1.area_domain) as area_domain,
								unnest(w1.area_domain_object) as area_domain_object
						from
								w1
						)
				,w3 as (select 1 as id4join, count(w2.*) as count_all from w2)
				,w4 as (select 1 as id4join, count(w2.*) as count_null from w2 where w2.area_domain_object is null)
				,w5 as	(
						select
								1 as id4join,
								array_agg(w2.area_domain order by w2.area_domain) as area_domain,
								array_agg(w2.area_domain_object order by w2.area_domain) as area_domain_object
						from
								w2
						)
				select
						w3.count_all,
						w4.count_null,
						w5.area_domain,
						w5.area_domain_object
				from
						w3
						inner join w4 on w3.id4join = w4.id4join
						inner join w5 on w3.id4join = w5.id4join
				into
					_check_input_ad_all,	-- pocet atributovych cleneni na vstupu
					_check_input_ad_null,	-- pocet nepredanych objektu
					_check_input_ad,		-- pole atributovych cleneni na vstupu
					_check_input_ad_object; -- pole objektu na vstupu			

				select ldsity_object_type from target_data.cm_ldsity2target_variable
				where id = _target_variable_cm_order[i]
				into _ldsity_object_type_ad;

				if _check_input_ad_all = 1 -- pocet atributovych PLOSNYCH cleneni je 1
				then
					if	(
						(_variant in (1,2)) or									-- prispevky jsou jen typu CORE
						(_variant in (3,4) and _ldsity_object_type_ad = 100)	-- prispevek je typu CORE
						)
					then
						if _check_input_ad_null = 1 -- neni predan objekt
						then
							raise exception 'Error 06: fn_get_categorization_setup: For _target_variable_cm [ID] = % and for this area domain element = % in input argument _area_domain WAS NOT SOLD object in input argument _area_domain_object!',_target_variable_cm_order[i],_check_input_ad;
						end if;
					else
						-- _variant is (3,4) and _ldsity_object_type_ad is 200	-- prispevek je typu DIVISION => plosne atributove cleneni nesmi byt predano, respektive pocet nepredanych objektu musi byt 1
						if _check_input_ad_null is distinct from 1
						then
							raise exception 'Error 07: fn_get_categorization_setup: For _target_variable_cm [ID] = % and for this area domain element = % in input argument _area_domain WAS SOLD object in input argument _area_domain_object!',_target_variable_cm_order[i],_check_input_ad;
						end if;
					end if;
				else
					-- pocet atributovych cleneni je 2 a vice
					if	(
						(_variant in (1,2)) or									-- prispevky jsou jen typu CORE
						(_variant in (3,4) and _ldsity_object_type_ad = 100)	-- prispevek je typu CORE
						)
					then
						if _check_input_ad_null > 0 -- existuje alespon jedno null v poli objektu, respektive nebyl predan alespon jeden objekt
						then
							with
							w1 as	(
									select
											unnest(_check_input_ad) as id,
											unnest(_check_input_ad_object) as res
									)
							select array_agg(w1.id order by w1.id) from w1 where res is null
							into _check_input_ad_errors;

							raise exception 'Error 08: fn_get_categorization_setup: For _target_variable_cm [ID] = % and for these area domain elements = % in input argument _area_domain WERE NOT SOLD objects in input argument _area_domain_object!',_target_variable_cm_order[i],_check_input_ad_errors;
						end if;
					else
						-- _variant is (3,4) and _ldsity_object_type_ad is 200 -- prispevek je typu DIVISION => plosna atributova cleneni nesmi byt predana, respektive pocet nepredanych objektu musi shodny s poctem predanych cleneni 
						if _check_input_ad_all is distinct from _check_input_ad_null
						then
							raise exception 'Error 09: fn_get_categorization_setup: For _target_variable_cm [ID] = % and for these area domain elements = % in input argument _area_domain WERE SOLD some of objects in input argument _area_domain_object!',_target_variable_cm_order[i],_check_input_ad;
						end if;
					end if;
				end if;
			end loop;
		end if;
		------------------------------------------------------------------
		-- check passed object for input sub populations
		------------------------------------------------------------------
		if _sub_population is not null
		then
			if array_length(_target_variable_cm_order,1) != array_length(_sub_population_object,1)
			then
				raise exception 'Error 10: fn_get_categorization_setup: If the input argument _sub_population IS NOT NULL then the count of elements in the input arguments _target_variable_cm and _sub_population_object must be equal!';
			end if;

			for i in 1..array_length(_target_variable_cm_order,1)
			loop
				with
				w1 as	(
						select
								_sub_population as sub_population,
								_sub_population_object[array_position(_target_variable_cm, _target_variable_cm_order[i]):array_position(_target_variable_cm, _target_variable_cm_order[i])] as sub_population_object
						)
				,w2 as	(
						select
								unnest(w1.sub_population) as sub_population,
								unnest(w1.sub_population_object) as sub_population_object
						from
								w1
						)
				,w3 as (select 1 as id4join, count(w2.*) as count_all from w2)
				,w4 as (select 1 as id4join, count(w2.*) as count_null from w2 where w2.sub_population_object is null)
				,w5 as	(
						select
								1 as id4join,
								array_agg(w2.sub_population order by w2.sub_population) as sub_population,
								array_agg(w2.sub_population_object order by w2.sub_population) as sub_population_object
						from
								w2
						)
				select
						w3.count_all,
						w4.count_null,
						w5.sub_population,
						w5.sub_population_object
				from
						w3
						inner join w4 on w3.id4join = w4.id4join
						inner join w5 on w3.id4join = w5.id4join
				into
					_check_input_sp_all,	-- pocet atributovych cleneni na vstupu
					_check_input_sp_null,	-- pocet nepredanych objektu
					_check_input_sp,		-- pole atributovych cleneni na vstupu
					_check_input_sp_object; -- pole objektu na vstupu

				select ldsity_object_type from target_data.cm_ldsity2target_variable
				where id = _target_variable_cm_order[i]
				into _ldsity_object_type_sp;

				if _check_input_sp_all = 1 -- pocet atributovych cleneni je 1
				then
					if	(
						(_variant in (1,2)) or									-- prispevky jsou jen typu CORE
						(_variant in (3,4) and _ldsity_object_type_sp = 200)	-- prispevek je typu DIVISION
						)
					then
						if _check_input_sp_null = 1 -- neni predan objekt
						then
							raise exception 'Error 11: fn_get_categorization_setup: For _target_variable_cm [ID] = % and for this sub population element = % in input argument _sub_population WAS NOT SOLD object in input argument _sub_population_object!',_target_variable_cm_order[i],_check_input_sp;
						end if;
					else
						-- _variant is (3,4) and _ldsity_object_type_sp is 100	-- prispevek je typu CORE => pocet nepredanych objektu musi shodny s poctem predanych cleneni, v tomto pripade to musi byt 1 nepredany objekt 
						if _check_input_sp_null is distinct from 1
						then
							raise exception 'Error 12: fn_get_categorization_setup: For _target_variable_cm [ID] = % and for this sub population element = % in input argument _sub_population WAS SOLD object in input argument _sub_population_object!',_target_variable_cm_order[i],_check_input_sp;
						end if;
					end if;
				else
					-- pocet atributovych cleneni je 2 a vice
					if	(
						(_variant in (1,2)) or									-- prispevky jsou jen typu CORE
						(_variant in (3,4) and _ldsity_object_type_sp = 200)	-- prispevek je typu DIVISION
						)
					then
						if _check_input_sp_null > 0 -- existuje alespon jedno null v poli objektu
						then
							with
							w1 as	(
									select
											unnest(_check_input_sp) as id,
											unnest(_check_input_sp_object) as res
									)
							select array_agg(w1.id order by w1.id) from w1 where res is null
							into _check_input_sp_errors;

							raise exception 'Error 13: fn_get_categorization_setup: For _target_variable_cm [ID] = % and for these sub population elements = % in input argument _sub_population WERE NOT SOLD objects in input argument _sub_population_object!',_target_variable_cm_order[i],_check_input_sp_errors;
						end if;
					else
						-- _variant is (3,4) and _ldsity_object_type_sp is 100	-- prispevek je typu CORE => pocet nepredanych objektu musi shodny s poctem predanych cleneni, v tomto pripade to musi byt jako je _check_input_sp_all
						if _check_input_sp_all is distinct from _check_input_sp_null
						then
							raise exception 'Error 14: fn_get_categorization_setup: For _target_variable_cm [ID] = % and for these sub population elements = % in input argument _sub_population WERE SOLD some of objects in input argument _sub_population_object!',_target_variable_cm_order[i],_check_input_sp;
						end if;
					end if;
				end if;
			end loop;
		end if;
		------------------------------------------------------------------
		------------------------------------------------------------------
		------------------------------------------------------------------
		-----------------------------------------	
		if _area_domain is null
		then
			_area_domain_object := _area_domain_object;
		else		
			for i in 1..array_length(_target_variable_cm_order,1)
			loop
				_area_domain_object_i := _area_domain_object[array_position(_target_variable_cm, _target_variable_cm_order[i]):array_position(_target_variable_cm, _target_variable_cm_order[i])];
						
				if i = 1
				then
					_area_domain_object_order := array[_area_domain_object_i];
				else
					_area_domain_object_order := _area_domain_object_order || array[_area_domain_object_i];
				end if;
				
			end loop;
		
			_area_domain_object := _area_domain_object_order;
		end if;
		-----------------------------------------
		if _sub_population is null
		then
			_sub_population_object := _sub_population_object;
		else		
			for i in 1..array_length(_target_variable_cm_order,1)
			loop
				_sub_population_object_i := _sub_population_object[array_position(_target_variable_cm, _target_variable_cm_order[i]):array_position(_target_variable_cm, _target_variable_cm_order[i])];
						
				if i = 1
				then
					_sub_population_object_order := array[_sub_population_object_i];
				else
					_sub_population_object_order := _sub_population_object_order || array[_sub_population_object_i];
				end if;
				
			end loop;
		
			_sub_population_object := _sub_population_object_order;
		end if;
		------------------------------------------------------------------
		------------------------------------------------------------------
		
		-------------------------------------------------------------
		_array_id := _target_variable_cm_order;
		-------------------------------------------------------------

		for bc in 1..array_length(_array_id,1)
		loop	
			-----------------------------------------
			-----------------------------------------
			if _area_domain is null and _sub_population is null
			then
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is null
			then
				if _variant in (3,4)
				then
					with
					w1 as (select unnest(_area_domain_object[1:1]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;				
				else		
					with
					w1 as (select unnest(_area_domain_object[bc:bc]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				end if;			
			
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;
			
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			------
			if _area_domain is null and _sub_population is not null
			then		
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
			
				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is not null
			then
				if _variant in (3,4)
				then
					with
					w1 as (select unnest(_area_domain_object[1:1]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				else
					with
					w1 as (select unnest(_area_domain_object[bc:bc]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				end if;				
			
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
	
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;			

				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			end if;
			-----------------------------------------
			-----------------------------------------
			
			if _area_domain4input is null
			then
				_area_domain4input_text := 'null::integer[]';
			else
				_area_domain4input_text := replace(replace(concat('array[',_area_domain4input,']'),'{',''),'}','');
			end if;
		
			if _area_domain_object4input is null
			then
				_area_domain_object4input_text := 'null::integer[]';
			else
				_area_domain_object4input_text := replace(replace(concat('array[',_area_domain_object4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population4input is null
			then
				_sub_population4input_text := 'null::integer[]';
			else
				_sub_population4input_text := replace(replace(concat('array[',_sub_population4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population_object4input is null
			then
				_sub_population_object4input_text := 'null::integer[]';
			else
				_sub_population_object4input_text := replace(replace(concat('array[',_sub_population_object4input,']'),'{',''),'}','');
			end if;

			-----------------------------------------
			-----------------------------------------
			-----------------------------------------
			_use_negative_id_bc := (select cmlv.use_negative from target_data.cm_ldsity2target_variable as cmlv where cmlv.id = _array_id[bc]);

			if _use_negative_id_bc is null
			then
				raise exception 'Error 15: fn_save_categorization_setup: For target_variable = % and target_variable_cm = % is value use_negative NULL!',_target_variable, _array_id[bc];
			end if;

			if _use_negative_id_bc = FALSE
			then
				if _area_domain4input is null or _area_domain_object4input is null
				then
					_use_negative_ad_4input_text := 'null::boolean[]';
				else
					_use_negative_ad_4input_text := (select replace(replace(concat('array[',(select array_agg(t.res) from (select 'false' as res from generate_series(1,array_length(_area_domain4input,1))) as t),']'),'{',''),'}',''));
				end if;

				if _sub_population4input is null or _sub_population_object4input is null
				then
					_use_negative_sp_4input_text := 'null::boolean[]';
				else
					_use_negative_sp_4input_text := (select replace(replace(concat('array[',(select array_agg(t.res) from (select 'false' as res from generate_series(1,array_length(_sub_population4input,1))) as t),']'),'{',''),'}',''));
				end if;
			else
				-- _use_negative_id_bc = TRUE

				-- AD --
				if _area_domain4input is null
				then
					_use_negative_ad_4input_text := 'null::boolean[]';
				else
					-- get classification_type for AD
					for ict in 1..array_length(_area_domain4input,1)
					loop
						select cad.classification_type from target_data.c_area_domain as cad
						where cad.id = _area_domain4input[ict]
						into _classification_type_ad;

						if _classification_type_ad is null
						then
							raise exception 'Error 16: fn_save_categorization_setup: For target_variable = %, target_variable_cm = % and area_domain = % not exists any value classification_type in c_area_domain table!',_target_variable, _array_id[bc], _area_domain4input[ict];
						end if;

						if _classification_type_ad = 100
						then
							_use_negative_ad_4input_text_i := 'false';
						else
							_use_negative_ad_4input_text_i := 'true';
						end if;

						if ict = 1
						then
							_use_negative_ad_4input_text_join := _use_negative_ad_4input_text_i;
						else
							_use_negative_ad_4input_text_join := concat(_use_negative_ad_4input_text_join,',',_use_negative_ad_4input_text_i);
						end if;
					end loop;

					_use_negative_ad_4input_text := concat('array[',_use_negative_ad_4input_text_join,']');

				end if;
				
				-- SP --
				if _sub_population4input is null
				then
					_use_negative_sp_4input_text := 'null::boolean[]';
				else
					-- get classification_type for SP
					for ict in 1..array_length(_sub_population4input,1)
					loop
						select csp.classification_type from target_data.c_sub_population as csp
						where csp.id = _sub_population4input[ict]
						into _classification_type_sp;

						if _classification_type_sp is null
						then
							raise exception 'Error 17: fn_save_categorization_setup: For target_variable = %, target_variable_cm = % and sub_population = % not exists any value classification_type in c_sub_population table!',_target_variable, _array_id[bc], _sub_population4input[ict];
						end if;

						if _classification_type_sp = 100
						then
							_use_negative_sp_4input_text_i := 'false';
						else
							_use_negative_sp_4input_text_i := 'true';
						end if;

						if ict = 1
						then
							_use_negative_sp_4input_text_join := _use_negative_sp_4input_text_i;
						else
							_use_negative_sp_4input_text_join := concat(_use_negative_sp_4input_text_join,',',_use_negative_sp_4input_text_i);
						end if;
					end loop;

					_use_negative_sp_4input_text := concat('array[',_use_negative_sp_4input_text_join,']');

				end if;
					
			end if;
			-----------------------------------------
			-----------------------------------------
			-----------------------------------------			
			
			_string4attr := concat('
			select
					area_domain_category,
					sub_population_category,
					adc2classification_rule,
					spc2classification_rule
			from
					target_data.fn_get_classification_rules_id4categories(',_area_domain4input_text,',',_area_domain_object4input_text,',',_sub_population4input_text,',',_sub_population_object4input_text,',',_use_negative_ad_4input_text,',',_use_negative_sp_4input_text,')
			');
		
			if bc = 1
			then
				_query_res := concat('with w_',bc,' as (',_string4attr,')');
			else
				_query_res := concat(_query_res,',w_',bc,' as (',_string4attr,')');
			end if;
		end loop;
		
		-------------------------------------------------------------
		-------------------------------------------------------------
		if _variant in (1,2)
		then
		
			_s4ij_p1 := 'select row_number() over() as group_id_poradi';
			_s4ij_p3 := ' from ';
		
			for bc in 1..array_length(_array_id,1)
			loop
				if bc = 1
				then
			
					_s4ij_p2 := concat('
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.area_domain_category
										,t',bc,'.sub_population_category
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'');
				
					_string4incomming := concat(
						'
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'
										on
											t1.area_domain_category = t',bc,'.area_domain_category and
											t1.sub_population_category = t',bc,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				end if;
			end loop;
			
				_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4;
		end if;		
		-------------------------------------------------------------
		if _variant in (3,4)
		then	
			_s4ij_p1 := concat(
				'
				select
					row_number() over() as group_id_poradi,
					a.*,
					b.*
				from
					(
					select
							',_array_id[1],' as ldsity2target_variable_',_array_id[1],',
							area_domain_category as area_domain_category_',_array_id[1],',
							adc2classification_rule as adc2classification_rule_',_array_id[1],',
							spc2classification_rule as spc2classification_rule_',_array_id[1],'
					from w_1
					) as a
				inner
				join
					(select ');
				
			_s4ij_p3 := ' from ';
		
			_s4ij_p5 := concat('
						) as b
					on
						a.area_domain_category_',_array_id[1],' = b.area_domain_category');
					
			_string4incomming := concat(
				'
				select
						1 as group_id,
						group_id_poradi,
						area_domain_category,
						sub_population_category,
						ldsity2target_variable_',_array_id[1],' as ldsity2target_variable,
						adc2classification_rule_',_array_id[1],' as adc2classification_rule,
						spc2classification_rule_',_array_id[1],' as spc2classification_rule
				from
						w_inner_join
				');					
				
			_array_id_200 := array_remove(_array_id, _array_id[1]);
			
			for bc_200 in 1..array_length(_array_id_200,1)
			loop
				if bc_200 = 1
				then				
					_s4ij_p2 := concat('
										t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.area_domain_category
										,t',bc_200,'.sub_population_category
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'');
				
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');				
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'
										on
											t1.area_domain_category = t',bc_200,'.area_domain_category and
											t1.sub_population_category = t',bc_200,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');
					
				end if;
			end loop;
			
			_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4 || _s4ij_p5;
		
		end if;
		-------------------------------------------------------------
		-------------------------------------------------------------
		_string_inner_join := concat(',w_inner_join as (',_string4inner_join,')');
		_string_incomming_0 := concat(',w_incomming_0 as (',_string4incomming,')');
		-------------------------------------------------------------
		if _variant in (3,4)
		then
			_string_incomming := concat(
				'
				,w_incomming as	(
								select
										group_id,
										group_id_poradi,
										------------------------------------
										area_domain_category,
										case
											when (array_remove(area_domain_category,0)) = array[]::integer[]
											then array[0]
											else (array_remove(area_domain_category,0))
										end as area_domain_category_upr,
										------------------------------------
										sub_population_category,
										case
											when (array_remove(sub_population_category,0)) = array[]::integer[]
											then array[0]
											else (array_remove(sub_population_category,0))
										end as sub_population_category_upr,
										------------------------------------
										ldsity2target_variable,
										------------------------------------
										case
											when adc2classification_rule is null
											then array[0]
											else adc2classification_rule
										end as adc2classification_rule,
										case
											when spc2classification_rule is null
											then array[0]
											else spc2classification_rule
										end as spc2classification_rule,
										------------------------------------
										case
											when ldsity2target_variable in (',array_to_string(_array_id_200,','),')
											then null::integer[]
											else adc2classification_rule
										end as adc2classification_rule4insert,
										spc2classification_rule as spc2classification_rule4insert
								from
									w_incomming_0
								)
				');


			_string_check_1 := concat(
				'
				,w_check_1 as		(
									select
											a.*,
											b.id_cm_setup,
											b.categorization_setup		
									from
											w_incomming as a
									left
									join	(								
											select
													tt1.*
											from
														(
														select
																t1.*,
																----------------------
																t2.adc2classification_rule4join
																----------------------
														from
															(
																	select
																		z1.*,
																		-----------------------
																		case
																			when z2.sub_population_category4join is null
																			then array[0]
																			else z2.sub_population_category4join
																		end as sub_population_category4join
																		-----------------------
																	from
																		(
																		select
																				id as id_cm_setup,
																				ldsity2target_variable,
																				-------------------------------------
																				case
																					when adc2classification_rule is null
																					then array[0]
																					else adc2classification_rule
																				end as adc2classification_rule,
																				-------------------------------------
																				spc2classification_rule,
																				categorization_setup
																		from
																				target_data.cm_ldsity2target2categorization_setup
																		where
																				ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
																		) as z1
																	left join
																		(
																		select
																			distinct categorization_setup, sub_population_category4join
																		from
																			(
																			select
																					categorization_setup,
																					case when spc2classification_rule is null then array[0]
																					else (select id_category from target_data.fn_get_category4classification_rule_id(''spc''::varchar,spc2classification_rule))
																					end as sub_population_category4join
																			from
																					target_data.cm_ldsity2target2categorization_setup
																			where
																					ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
																			) as t
																		where
																				t.sub_population_category4join is distinct from array[0]
																		) as z2
																	on
																		z1.categorization_setup = z2.categorization_setup
															) as t1
															inner join
															(
															select
																	categorization_setup,
																	case when adc2classification_rule is null then array[0] else adc2classification_rule end as adc2classification_rule4join
															from
																	target_data.cm_ldsity2target2categorization_setup
															where
																	ldsity2target_variable = ',_array_id[1],'
															) as t2
														on t1.categorization_setup = t2.categorization_setup
														) as tt1
											inner join
														w_incomming as tt2
											on
													tt1.ldsity2target_variable = tt2.ldsity2target_variable

								and tt1.adc2classification_rule4join @> tt2.adc2classification_rule AND 
								tt1.adc2classification_rule4join <@ tt2.adc2classification_rule

								and tt1.sub_population_category4join @> tt2.sub_population_category_upr AND 
								tt1.sub_population_category4join <@ tt2.sub_population_category_upr

								--and	(target_data.fn_array_compare(tt1.adc2classification_rule4join,tt2.adc2classification_rule))
								--and	(target_data.fn_array_compare(tt1.sub_population_category4join,tt2.sub_population_category_upr))
											) as b
									on	
											a.ldsity2target_variable = b.ldsity2target_variable

							and b.adc2classification_rule4join @> a.adc2classification_rule AND 
							b.adc2classification_rule4join <@ a.adc2classification_rule

							and b.sub_population_category4join @> a.sub_population_category_upr AND 
							b.sub_population_category4join <@ a.sub_population_category_upr

						--and	(target_data.fn_array_compare(a.adc2classification_rule,b.adc2classification_rule4join))
						--and	(target_data.fn_array_compare(a.sub_population_category_upr,b.sub_population_category4join))		
									)
			');

			_string_check_3 := concat(
				'
				,w_check_3 as		(
									select * from w_check_2 union all
									select distinct c.group_id, c.group_id_poradi,
									------------------------
									c.area_domain_category,
									c.area_domain_category_upr,
									c.sub_population_category,
									c.sub_population_category_upr,
									--------------------------
									c.ldsity2target_variable,
									c.adc2classification_rule,
									c.spc2classification_rule,
									c.adc2classification_rule4insert,
									c.spc2classification_rule4insert,
									c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
									from
											(
											select
													1 as group_id,
													group_id_poradi,
													--------------------------
													area_domain_category,
													area_domain_category_upr,
													sub_population_category,
													sub_population_category_upr,
													---------------------------
													',_array_id[1],' as ldsity2target_variable,
													adc2classification_rule,
													null::integer[] as spc2classification_rule,
													adc2classification_rule4insert,
													spc2classification_rule4insert,
													null::integer as id_cm_setup,
													null::integer as categorization_setup,
													check_exists_group_id_1
											from
													w_check_2 where check_exists_group_id_1 = 1
											) as c
									)				
			');

			_string_insert_2 :=
			'
			insert into target_data.cm_ldsity2target2categorization_setup
			(ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup)
			select
					w_check_4.ldsity2target_variable,
					w_check_4.adc2classification_rule4insert,
					w_check_4.spc2classification_rule4insert,
					w_check_4.new_categorization_setup
			from
					w_check_4
			order
					by
						w_check_4.new_categorization_setup,
						w_check_4.pozice,
						w_check_4.group_id;
			';
		else
			_string_incomming := concat(
				'
				,w_incomming as	(
								select
										group_id,
										group_id_poradi,
										area_domain_category,
										sub_population_category,
										ldsity2target_variable,
										adc2classification_rule,
										spc2classification_rule
								from
									w_incomming_0
								)
				');

			_string_check_1 := concat(
				'
				,w_check_1 as		(
									select
											a.*,
											b.id_cm_setup,
											b.categorization_setup
											-----------------------
											--,b.adc2classification_rule as adc2classification_rule_original,
											--b.spc2classification_rule as spc2classification_rule_original
									from
											w_incomming as a
									left
									join	(
											select
													t1.*
											from
														(
														select
																id as id_cm_setup,
																ldsity2target_variable,
																adc2classification_rule,
																spc2classification_rule,
																categorization_setup
														from
																target_data.cm_ldsity2target2categorization_setup
														where
																ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
														) as t1
											inner
											join		w_incomming as t2
												
														on	t1.ldsity2target_variable = t2.ldsity2target_variable

								and coalesce(t1.adc2classification_rule,array[0]) @> coalesce(t2.adc2classification_rule,array[0]) AND 
								coalesce(t1.adc2classification_rule,array[0]) <@ coalesce(t2.adc2classification_rule,array[0])

								and coalesce(t1.spc2classification_rule,array[0]) @> coalesce(t2.spc2classification_rule,array[0]) AND 
								coalesce(t1.spc2classification_rule,array[0]) <@ coalesce(t2.spc2classification_rule,array[0])

								--and	(target_data.fn_array_compare
								--		((case when t1.adc2classification_rule is null then array[0] else t1.adc2classification_rule end),
								--		(case when t2.adc2classification_rule is null then array[0] else t2.adc2classification_rule end)
								--		)
								--	)
								--and	(target_data.fn_array_compare
								--		((case when t1.spc2classification_rule is null then array[0] else t1.spc2classification_rule end),
								--		(case when t2.spc2classification_rule is null then array[0] else t2.spc2classification_rule end)
								--		)
								--	)
											) as b
									on
											a.ldsity2target_variable = b.ldsity2target_variable

							and coalesce(a.adc2classification_rule,array[0]) @> coalesce(b.adc2classification_rule,array[0]) AND 
							coalesce(a.adc2classification_rule,array[0]) <@ coalesce(b.adc2classification_rule,array[0])

							and coalesce(a.spc2classification_rule,array[0]) @> coalesce(b.spc2classification_rule,array[0]) AND 
							coalesce(a.spc2classification_rule,array[0]) <@ coalesce(b.spc2classification_rule,array[0])

							--and			
							--(												
							--target_data.fn_array_compare
							--	((case when a.adc2classification_rule is null then array[0] else a.adc2classification_rule end),
							--	(case when b.adc2classification_rule is null then array[0] else b.adc2classification_rule end)
							--	)
							--)
							--and
							--(												
							--target_data.fn_array_compare
							--	((case when a.spc2classification_rule is null then array[0] else a.spc2classification_rule end),
							--	(case when b.spc2classification_rule is null then array[0] else b.spc2classification_rule end)
							--	)
							--)	
									)
				'
			);

			_string_check_3 := concat(
				'
				,w_check_3 as		(
									select * from w_check_2 union all
									select distinct c.group_id, c.group_id_poradi, c.area_domain_category,
									c.sub_population_category, c.ldsity2target_variable,
									c.adc2classification_rule, c.spc2classification_rule,
									c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
									from
											(
											select
													1 as group_id,
													group_id_poradi,
													area_domain_category,
													sub_population_category,
													',_array_id[1],' as ldsity2target_variable,
													adc2classification_rule,
													null::integer[] as spc2classification_rule,
													null::integer as id_cm_setup,
													null::integer as categorization_setup,
													check_exists_group_id_1
											from
													w_check_2 where check_exists_group_id_1 = 1
											) as c
									)				
			');

			_string_insert_2 :=
			'
			insert into target_data.cm_ldsity2target2categorization_setup
			(ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup)
			select
					w_check_4.ldsity2target_variable,
					w_check_4.adc2classification_rule,
					w_check_4.spc2classification_rule,
					w_check_4.new_categorization_setup
			from
					w_check_4
			order
					by
						w_check_4.new_categorization_setup,
						w_check_4.pozice,
						w_check_4.group_id;
			';			
		end if;
		-------------------------------------------------------------
		
		/*
		-------------------------------------------------------------
		-------------------------------------------------------------
		_s4da_p1 := 'select t0.categorization_setup';
		_s4da_p3 := ' from (select distinct categorization_setup from w_database) as t0 ';

		_s4ia_p1 := 'select t0.group_id_poradi';
		_s4ia_p3 := ' from (select distinct group_id_poradi from w_incomming) as t0 ';
	
		_s4result_p1 :=	'
						select
								a.*,
								b.categorization_setup
						from
								w_incomming_agg as a
						inner
						join	w_database_agg as b
						
						on ';	
	

		for i in 1..array_length(_array_id,1)
		loop
			if i = 1
			then
				_s4da_p2 := concat('
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');

				if _variant in (3,4)
				then				
					_s4ia_p2 := concat('
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule4insert as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');
				else
					_s4ia_p2 := concat('
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');				
				end if;								
								
				_s4da_p4 := concat(' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat('
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],'
									and			
									(												
										target_data.fn_array_compare
										(
											(case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end),
											(case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)
										)
									)
									and
									(												
										target_data.fn_array_compare
										(
											(case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end),
											(case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end)
										)
									)
								');
			else
				_s4da_p2 := concat(_s4da_p2,
									'
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');
								
				if _variant in (3,4)
				then				
					_s4ia_p2 := concat(_s4ia_p2,
										'
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule4insert as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');
				else
					_s4ia_p2 := concat(_s4ia_p2,
										'
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');				
				end if;								
								
				_s4da_p4 := concat(_s4da_p4,
									' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(_s4ia_p4,
									' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat(_s4result_p2,' and 
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],'
									and			
									(												
										target_data.fn_array_compare
										(
											(case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end),
											(case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)
										)
									)
									and
									(												
										target_data.fn_array_compare
										(
											(case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end),
											(case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end)
										)
									)									
								');
			end if;
		end loop;
	
		_string4database_agg := _s4da_p1 || _s4da_p2 || _s4da_p3 || _s4da_p4;
		_string4incomming_agg := _s4ia_p1 || _s4ia_p2 || _s4ia_p3 || _s4ia_p4;
		_string4result := _s4result_p1 || _s4result_p2;
		-------------------------------------------------------------
		-------------------------------------------------------------
		*/

		_string_check_2 := 
		'
		,w_check_2 as		(
							select
									w_check_1.*,
									case
										when categorization_setup is null
											then
												case
													when	(
															select min(t.group_id) from w_check_1 as t
															where t.categorization_setup is null
															and t.group_id_poradi = w_check_1.group_id_poradi
															)
															= 1
													then 0
													else 1
												end 
											else 0
									end as check_exists_group_id_1
							from
									w_check_1
							)		
		';


		_string_check_4 :=
		'
		,w_check_4 as		(					
							select
									(select coalesce(max(id),0) from target_data.t_categorization_setup) + 
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as new_categorization_setup,
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as pozice,
									w_check_3.*
							from
									w_check_3
							where
									categorization_setup is null
							order
									by new_categorization_setup, pozice, group_id
							)		
		';

		_string_insert_1 :=
		'
		,w_insert_1 as		(
							insert into target_data.t_categorization_setup(id)
							select distinct new_categorization_setup from w_check_4 order by new_categorization_setup
							returning id
							)		
		';		
		
		/*
		_string_agg_and_result := concat(
		'
		,w_database as		(
							select id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
							from target_data.cm_ldsity2target2categorization_setup where ldsity2target_variable in
							(select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
							union all
							select id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
							from w_insert_2
							)
		,w_database_agg as	('||_string4database_agg||')
		,w_incomming_agg as	('||_string4incomming_agg||')
		,w_result as		('||_string4result||')
		select
			categorization_setup
		from
			w_result order by categorization_setup;
		');
		*/

		_res := _query_res || _string_inner_join || _string_incomming_0 || _string_incomming || _string_check_1 || _string_check_2 || _string_check_3 || _string_check_4 || _string_insert_1 || _string_insert_2 /*|| _string_agg_and_result*/;
		
		execute ''||_res||'';
end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_save_categorization_setup(integer,integer[],integer[],integer[][],integer[],integer[][]) IS
'The function sets attribute configurations.';

grant execute on function target_data.fn_save_categorization_setup(integer,integer[],integer[],integer[][],integer[],integer[][]) to public;