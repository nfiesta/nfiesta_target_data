--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_change_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_change_target_variable(
	_label character varying(200), 
	_description text, 
	_label_en character varying(200) DEFAULT NULL::varchar, 
	_description_en text DEFAULT NULL::text, 
	_ldsity integer[] DEFAULT NULL::integer[], 
	_ldsity_object_type integer[] DEFAULT NULL::integer[], 
	_version integer[] DEFAULT NULL::integer[], 
	_ldsity_negat integer[] DEFAULT NULL::integer[], 
	_ldsity_object_type_negat integer[] DEFAULT NULL::integer[], 
	_version_negat integer[] DEFAULT NULL::integer[], 
	_id integer DEFAULT NULL::integer)
RETURNS integer[]
AS
$$
DECLARE
	_target_variable integer;
	_target_variables integer[];
BEGIN
	IF _id IS NULL
	THEN
		IF _label IS NOT NULL AND _description IS NOT NULL AND (_ldsity IS NOT NULL OR _ldsity_negat IS NOT NULL)
		THEN
			IF array_length(_ldsity,1) != array_length(_ldsity_object_type,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity, _ldsity_object_type;
			END IF;

			IF array_length(_ldsity,1) != array_length(_version,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of versions (%) are not equal!', _ldsity, _version;
			END IF;

			IF array_length(_ldsity_negat,1) != array_length(_ldsity_object_type_negat,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions with negative value (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity_negat, _ldsity_object_type_negat;
			END IF;

			IF array_length(_ldsity_negat,1) != array_length(_version_negat,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions with negative value (%) and corresponding assignment of versions (%) are not equal!', _ldsity_negat, _version_negat;
			END IF;

			INSERT INTO target_data.c_target_variable(label, description, label_en, description_en, state_or_change)
			SELECT _label, _description, _label_en, _description_en, 200
			RETURNING id
			INTO _target_variable;

			INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category, version, use_negative)
			(SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], t3.version, false
			FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
			ON t1.id = t2.id
			INNER JOIN unnest(_version) WITH ORDINALITY AS t3(version, id)
			ON t1.id = t3.id
			UNION ALL
			SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], t3.version, true
			FROM unnest(_ldsity_negat) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type_negat) WITH ORDINALITY AS t2(ldsity_object_type, id)
			ON t1.id = t2.id
			INNER JOIN unnest(_version_negat) WITH ORDINALITY AS t3(version, id)
			ON t1.id = t3.id);
	
			_target_variables := array[_target_variable];
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%), local density contributions of pozitive (%) or negative (%) value must be not null!', _label, _description, _ldsity, _ldsity_negat;
		END IF;
	ELSE
		-- test on the same label and description set
		WITH w AS (
			SELECT 	DISTINCT t3.id AS ldsity, 
				t2.area_domain_category, t2.sub_population_category, t2.use_negative
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _id AND t4.id = 100
		), w_agg_target_variable AS (
			SELECT --w.target_variable,
				w.area_domain_category, w.sub_population_category,
				array_agg(w.ldsity ORDER BY w.ldsity) FILTER (WHERE use_negative = false) AS ldsity_pozit,
				array_agg(w.ldsity ORDER BY w.ldsity) FILTER (WHERE use_negative = true) AS ldsity_negat
			FROM w
			GROUP BY --w.target_variable, 
				w.area_domain_category, w.sub_population_category
		),
		w_all AS (
			SELECT 	t1.id AS target_variable, 
				array_agg(t3.id ORDER BY t3.id) FILTER (WHERE use_negative = false) AS ldsity_pozit, 
				array_agg(t3.id ORDER BY t3.id) FILTER (WHERE use_negative = true) AS ldsity_negat, 
				t2.area_domain_category, t2.sub_population_category
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE t4.id = 100
			GROUP BY t1.id, t2.area_domain_category, t2.sub_population_category
		), w_tv AS (
			SELECT t1.target_variable
			FROM w_all AS t1
			INNER JOIN w_agg_target_variable AS t2
			ON 	coalesce(t1.area_domain_category,array[0]) = coalesce(t2.area_domain_category,array[0]) AND 
				coalesce(t1.sub_population_category,array[0]) = coalesce(t2.sub_population_category,array[0]) AND
				coalesce(t1.ldsity_pozit,array[0]) = coalesce(t2.ldsity_pozit,array[0]) AND 
				coalesce(t1.ldsity_negat,array[0]) = coalesce(t2.ldsity_negat,array[0])
		)
		SELECT array_agg(target_variable)
		FROM w_tv
		INTO _target_variables;

		IF _target_variables != _id
		THEN
			RAISE EXCEPTION 'Given array of target_variables (%) is not compatible with the array of target_variables with the same ldsitys (%). It is either incomplete or containes target variables with different ldsitys.', _id, _target_variables;
		END IF;

		IF _ldsity IS NULL
		THEN
			UPDATE target_data.c_target_variable
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_target_variable.id = _id;

		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing target variable and create new one with given ldsitys (%).', _ldsity;
		END IF;
	END IF;

	RETURN _target_variables;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) TO public;
