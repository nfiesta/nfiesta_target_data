--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_class_rule_mapping
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_class_rule_mapping(integer, integer[], boolean[], integer[][]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_class_rule_mapping(_areal_or_pop integer, _rules integer[], _use_negative boolean[], _panel_refyearset integer[][])
--boolean
RETURNS TABLE (
	classification_rule		integer,
	refyearset2panel		integer
) 
AS
$$
DECLARE
	_use_negative_count integer;
BEGIN
	_use_negative_count := (SELECT count(DISTINCT val) FROM unnest(_use_negative) AS t(val));

	CASE
	WHEN _areal_or_pop = 100
	THEN
		PERFORM pg_catalog.setval('target_data.cm_adc2classrule2panel_refyearset_id_seq', (SELECT max(id) FROM target_data.cm_adc2classrule2panel_refyearset), true); 
		RETURN QUERY

		WITH w_rules AS (
			SELECT
				t1.rule, t1.id, t2.use_negative, t2.array_id
			FROM
				unnest(_rules) WITH ORDINALITY AS t1(rule, id)
			INNER JOIN
				(SELECT 
					use_negative, id, ceil( round((id/(count(*) OVER())::numeric * _use_negative_count),12) ) AS array_id
				FROM unnest(_use_negative) WITH ORDINALITY AS t1(use_negative, id)
				) AS t2
			ON t1.id = t2.id
			)
		INSERT INTO target_data.cm_adc2classrule2panel_refyearset (adc2classification_rule, refyearset2panel)
		SELECT
			t1.rule, t2.panel_refyearset
		FROM
			w_rules AS t1
		INNER JOIN
			(SELECT 
				panel_refyearset, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_panel_refyearset,1)),12) ) AS array_id
			FROM unnest(_panel_refyearset) WITH ORDINALITY AS t1(panel_refyearset, id)
			) AS t2
		ON t1.array_id = t2.array_id
		RETURNING cm_adc2classrule2panel_refyearset.adc2classification_rule, cm_adc2classrule2panel_refyearset.refyearset2panel;

	WHEN _areal_or_pop = 200
	THEN
		PERFORM pg_catalog.setval('target_data.cm_spc2classrule2panel_refyearset_id_seq', (SELECT max(id) FROM target_data.cm_spc2classrule2panel_refyearset), true); 
		RETURN QUERY
		WITH w_rules AS (
			SELECT
				t1.rule, t1.id, t2.use_negative, t2.array_id
			FROM
				unnest(_rules) WITH ORDINALITY AS t1(rule, id)
			INNER JOIN
				(SELECT 
					use_negative, id, ceil( round((id/(count(*) OVER())::numeric * _use_negative_count),12) ) AS array_id
				FROM unnest(_use_negative) WITH ORDINALITY AS t1(use_negative, id)
				) AS t2
			ON t1.id = t2.id
			)
		INSERT INTO target_data.cm_spc2classrule2panel_refyearset (spc2classification_rule, refyearset2panel)
		SELECT
			t1.rule, t2.panel_refyearset
		FROM
			w_rules AS t1
		INNER JOIN
			(SELECT 
				panel_refyearset, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_panel_refyearset,1)),12) ) AS array_id
			FROM unnest(_panel_refyearset) WITH ORDINALITY AS t1(panel_refyearset, id)
			) AS t2
		ON t1.array_id = t2.array_id
		RETURNING cm_spc2classrule2panel_refyearset.spc2classification_rule, cm_spc2classrule2panel_refyearset.refyearset2panel;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_class_rule_mapping(integer, integer[], boolean[], integer[][]) IS
'Function checks syntax in text field with classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_class_rule_mapping(integer, integer[], boolean[], integer[][]) TO public;
