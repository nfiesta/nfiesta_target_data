--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_classification_rule.sql
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_classification_rule(integer, integer, integer, text, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_classification_rule(_parent integer, _areal_or_population integer, _ldsity_object integer, _classification_rule text, _use_negative boolean DEFAULT false)
RETURNS integer
AS
$$
DECLARE
	_id integer;
BEGIN
	IF _parent IS NULL OR _ldsity_object IS NULL OR _classification_rule IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent indicator (%) or ldsity object (%) or classification rule (%) is null!', _parent, _ldsity_object, _classification_rule;
	END IF; 

	CASE 
	WHEN _areal_or_population = 100 THEN
		INSERT INTO target_data.cm_adc2classification_rule(area_domain_category, ldsity_object, classification_rule, use_negative)
		SELECT 
			_parent,
			_ldsity_object, _classification_rule, _use_negative
		RETURNING id
		INTO _id;

	WHEN _areal_or_population = 200 THEN

		INSERT INTO target_data.cm_spc2classification_rule(sub_population_category, ldsity_object, classification_rule, use_negative)
		SELECT 
			_parent,
			_ldsity_object, _classification_rule, _use_negative
		RETURNING id
		INTO _id;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_classification_rule(integer, integer, integer, text, boolean) IS
'Functions inserts a record into table cm_adc2conversion_string or cm_spc2conversion_string based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_classification_rule(integer, integer, integer, text, boolean) TO public;
