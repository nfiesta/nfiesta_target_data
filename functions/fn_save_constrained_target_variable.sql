--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_constrained_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_constrained_target_variable(integer, integer[], integer[][], integer[][], integer[][], integer[][]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_constrained_target_variable
(
	_target_variable		integer,		-- current target variable which should be constrained
	_target_variable_cm		integer[],		-- ldsity objects which the target variable consists off
	_area_domain_category		integer[][],		-- area domain category(ies) used to constrain the object
	_adc_object			integer[][],		-- object on which the category is applied on for each cm id
	_sub_population_category	integer[][],		-- sub population category(ies) used to constrain the object
	_spc_object			integer[][]		-- object on which the category is applied on for each cm id
)
RETURNS integer
AS
$$
DECLARE
_id 		integer;
_mtext		text;
_mcont		text;
BEGIN
	BEGIN
		INSERT INTO target_data.c_target_variable(label, description, label_en, description_en, state_or_change)
		SELECT label, description, label_en, description_en, state_or_change
		FROM target_data.c_target_variable
		WHERE id = _target_variable
		RETURNING id
		INTO _id;

		WITH w_data AS (
			SELECT
				t1.target_variable, t1.ldsity, t1.ldsity_object_type, t1.version, t1.use_negative, t2.id AS array_id,
				t3.id AS adc_id, t7.id AS adc, t5.id AS spc_id, t8.id AS spc
			FROM
				target_data.cm_ldsity2target_variable AS t1
			INNER JOIN
				unnest(_target_variable_cm) WITH ORDINALITY AS t2(cm_id, id)
			ON t1.id = t2.cm_id
			LEFT JOIN 
				(
				SELECT  area_domain_category, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_area_domain_category,1)),12) ) AS array_id
				FROM  unnest(_area_domain_category) WITH ORDINALITY AS t1(area_domain_category, id)
				) AS t3
			ON t2.id = t3.array_id
			LEFT JOIN 
				(
				SELECT  adc_object, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_adc_object,1)),12) ) AS array_id
				FROM  unnest(_adc_object) WITH ORDINALITY AS t1(adc_object, id)
				) AS t4
			ON t3.id = t4.id
			LEFT JOIN 
				(
				SELECT  sub_population_category, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_sub_population_category,1)),12) ) AS array_id
				FROM  unnest(_sub_population_category) WITH ORDINALITY AS t1(sub_population_category, id)
				) AS t5
			ON t2.id = t5.array_id
			LEFT JOIN 
				(
				SELECT  spc_object, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_spc_object,1)),12) ) AS array_id
				FROM  unnest(_spc_object) WITH ORDINALITY AS t1(spc_object, id)
				) AS t6
			ON t5.id = t6.id
			LEFT JOIN
				(SELECT t1.classification_type, t3.id, t3.area_domain_category, t3.ldsity_object, t3.use_negative
				FROM target_data.c_area_domain AS t1
				INNER JOIN target_data.c_area_domain_category AS t2 ON t1.id = t2.area_domain
				INNER JOIN target_data.cm_adc2classification_rule AS t3 ON t2.id = t3.area_domain_category
				) AS t7
			ON t3.area_domain_category = t7.area_domain_category AND t4.adc_object = t7.ldsity_object AND 
				CASE WHEN t7.classification_type = 100 THEN t7.use_negative = false ELSE t1.use_negative = t7.use_negative END
			LEFT JOIN
				(SELECT t1.classification_type, t3.id, t3.sub_population_category, t3.ldsity_object, t3.use_negative
				FROM target_data.c_sub_population AS t1
				INNER JOIN target_data.c_sub_population_category AS t2 ON t1.id = t2.sub_population
				INNER JOIN target_data.cm_spc2classification_rule AS t3 ON t2.id = t3.sub_population_category
				) AS t8
			ON t5.sub_population_category = t8.sub_population_category AND t6.spc_object = t8.ldsity_object AND
				CASE WHEN t8.classification_type = 100 THEN t8.use_negative = false ELSE t1.use_negative = t8.use_negative END
		), w_ldsity AS (
			SELECT	ldsity, ldsity_object_type, version,
				array_agg(adc ORDER BY adc_id) FILTER (WHERE adc IS NOT NULL) AS area_domain_category, 
				array_agg(spc ORDER BY spc_id) FILTER (WHERE spc IS NOT NULL) AS sub_population_category,
				use_negative
			FROM w_data
			GROUP BY ldsity, ldsity_object_type, version, use_negative
		)
		INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category, version, use_negative)
		SELECT _id, ldsity, ldsity_object_type, area_domain_category, sub_population_category, version, use_negative
		FROM w_ldsity;


		EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS _mtext := MESSAGE_TEXT, _mcont := PG_EXCEPTION_CONTEXT;

		PERFORM pg_catalog.setval('target_data.cm_ldsity2target_variable_id_seq', (SELECT max(id) FROM target_data.cm_ldsity2target_variable), true);
		DELETE FROM target_data.c_target_variable WHERE id = _id;
		PERFORM pg_catalog.setval('target_data.c_target_variable_id_seq', (SELECT max(id) FROM target_data.c_target_variable), true);
		RAISE EXCEPTION '%
		CONTEXT: %', _mtext, _mcont;
	END;
	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_constrained_target_variable(integer, integer[], integer[][], integer[][], integer[][], integer[][]) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters. It takes already created target variable, mirrors its ldsity contributions and adds a constraint for given area domain and sub population categories.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_constrained_target_variable(integer, integer[], integer[][], integer[][], integer[][], integer[][]) TO public;
