--
-- Copyright 2021, 2025 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_save_ldsity_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_ldsity_values(integer[], integer, double precision) CASCADE;
	
create or replace function target_data.fn_save_ldsity_values
(
	_refyearset2panel_mapping	integer[],
	_target_variable			integer,
	_threshold					double precision default 0.0000000001
)
returns text
as
$$
declare
		_max_categorization_setup				integer;
		_max_refyearset2panel_mapping			integer;
		_refyearset2panel_mapping_add_zero		integer[];
		_categorization_setup_add_zero			integer[];
		_refyearset2panel_mapping_without_zero	integer[];
		_categorization_setup_without_zero		integer[];
		_res_i									text;
		_res									text;
		_result									text;
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_save_ldsity_values: The input argument _refyearset2panel_mapping must not be NULL !';
		end if;
	
		if _target_variable is null
		then
			raise exception 'Error 02: fn_save_ldsity_values: The input argument _target_variable must not be NULL !';
		end if;

		if not exists (select t.* from target_data.c_target_variable as t where t.id = _target_variable)
			then raise exception 'Error: 03: fn_save_ldsity_values: Given target variable (%) does not exist in c_target_variable table.', _target_variable;
		END IF;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
/*		with
		w1 as	(
				select * from target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable)
				)
		,w2 as	(
				select
						w1.categorization_setup,
						unnest(w1.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select distinct w2.categorization_setup, w2.refyearset2panel_mapping from w2
				)
		,w4 as	(
				select
						w3.refyearset2panel_mapping,
						array_agg(w3.categorization_setup order by w3.categorization_setup) as categorization_setup
				from
						w3 group by w3.refyearset2panel_mapping
				)
		,w5 as	(
				select * from w4 where w4.refyearset2panel_mapping = ANY(_refyearset2panel_mapping)
				)
		select max(array_length(w5.categorization_setup,1)) from w5
		into _max_categorization_setup;
		-----------------------------------------------------------------------
		if _max_categorization_setup is null
		then
			raise exception 'Error 04: fn_save_ldsity_values: For input argument _target_variable not exist any categorization setup!';
		end if;
*/
		-----------------------------------------------------------------------
		with
		w1 as	(
				select * from target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable)
				)
		,w2 as	(
				select
						w1.categorization_setup,
						unnest(w1.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select distinct w2.categorization_setup, w2.refyearset2panel_mapping from w2
				)
		,w4 as	(
				select
						w3.refyearset2panel_mapping,
						array_agg(w3.categorization_setup order by w3.categorization_setup) as categorization_setup
				from
						w3 group by w3.refyearset2panel_mapping
				)
		,w5 as	(
				select * from w4 where w4.refyearset2panel_mapping = ANY(_refyearset2panel_mapping)
				)
		,w6a as (
				select max(array_length(w5.categorization_setup,1)) AS max_cat from w5
			)			
		,w6b as	(
				select
						w5.*,
						------------
						case
							when array_length(w5.categorization_setup,1) = _max_categorization_setup
							then w5.categorization_setup
							else
								w5.categorization_setup ||
								(select array_agg(t.categorization_setup) from (select 0 as categorization_setup, generate_series(1, (_max_categorization_setup - array_length(w5.categorization_setup,1)))) as t)
						end
							as categorization_setup_add_zero
				from
							w5
				)
		,w7b as	(
				select
						w6b.categorization_setup_add_zero,
						array_agg(w6b.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w6b group by w6b.categorization_setup_add_zero
				)
		,w8b as (
			select max(array_length(w7b.refyearset2panel_mapping,1)) AS max_refpanel from w7b
			)
		,w8c as	(
				select
						w7b.*,
						case
							when array_length(w7b.refyearset2panel_mapping,1) = _max_refyearset2panel_mapping
							then w7b.refyearset2panel_mapping
							else
								w7b.refyearset2panel_mapping ||
								(select array_agg(t.refyearset2panel_mapping) from (select 0 as refyearset2panel_mapping, generate_series(1, (_max_refyearset2panel_mapping - array_length(w7b.refyearset2panel_mapping,1)))) as t)
						end
							as refyearset2panel_mapping_add_zero
				from
							w7b
			)
		,w9c as (
			select
					array_agg(w8c.refyearset2panel_mapping_add_zero order by w8c.refyearset2panel_mapping) as refyearset2panel_mapping_add_zero,
					array_agg(w8c.categorization_setup_add_zero order by w8c.refyearset2panel_mapping) as categorization_setup_add_zero
			from
					w8c
			)
		SELECT t1.max_cat, t2.max_refpanel, t3.refyearset2panel_mapping_add_zero, t3.categorization_setup_add_zero
		FROM w6a AS t1, w8b AS t2, w9c AS t3
		into 	_max_categorization_setup, _max_refyearset2panel_mapping,
			_refyearset2panel_mapping_add_zero, _categorization_setup_add_zero;

		-----------------------------------------------------------------------
		if _max_categorization_setup is null
		then
			raise exception 'Error 04: fn_save_ldsity_values: For input argument _target_variable not exist any categorization setup!';
		end if;

		-----------------------------------------------------------------------
		if _max_refyearset2panel_mapping is null
		then
			raise exception 'Error 05: fn_save_ldsity_values: For input argument _target_variable not exist any panel refyearset combination!';
		end if;
		-----------------------------------------------------------------------
	/*	with
		w1 as	(
				select * from target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable)
				)
		,w2 as	(
				select
						w1.categorization_setup,
						unnest(w1.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select distinct w2.categorization_setup, w2.refyearset2panel_mapping from w2
				)
		,w4 as	(
				select
						w3.refyearset2panel_mapping,
						array_agg(w3.categorization_setup order by w3.categorization_setup) as categorization_setup
				from
						w3 group by w3.refyearset2panel_mapping order by w3.refyearset2panel_mapping
				)
		,w5 as	(
				select * from w4 where w4.refyearset2panel_mapping = ANY(_refyearset2panel_mapping)
				)				
		,w6 as	(
				select
						w5.*,
						------------
						case
							when array_length(w5.categorization_setup,1) = _max_categorization_setup
							then w5.categorization_setup
							else
								w5.categorization_setup ||
								(select array_agg(t.categorization_setup) from (select 0 as categorization_setup, generate_series(1, (_max_categorization_setup - array_length(w5.categorization_setup,1)))) as t)
						end
							as categorization_setup_add_zero
				from
							w5
				)
		,w7 as	(
				select
						w6.categorization_setup_add_zero,
						array_agg(w6.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w6 group by w6.categorization_setup_add_zero
				)	
		,w8 as	(
				select
						w7.*,
						case
							when array_length(w7.refyearset2panel_mapping,1) = _max_refyearset2panel_mapping
							then w7.refyearset2panel_mapping
							else
								w7.refyearset2panel_mapping ||
								(select array_agg(t.refyearset2panel_mapping) from (select 0 as refyearset2panel_mapping, generate_series(1, (_max_refyearset2panel_mapping - array_length(w7.refyearset2panel_mapping,1)))) as t)
						end
							as refyearset2panel_mapping_add_zero
				from
							w7
				)
		select
				array_agg(w8.refyearset2panel_mapping_add_zero order by w8.refyearset2panel_mapping) as refyearset2panel_mapping_add_zero,
				array_agg(w8.categorization_setup_add_zero order by w8.refyearset2panel_mapping) as categorization_setup_add_zero
		from
				w8
		into
				_refyearset2panel_mapping_add_zero,
				_categorization_setup_add_zero;*/
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		for i in 1..array_length(_refyearset2panel_mapping_add_zero,1)
		loop
			--raise notice '%', i;
			-- check that values in array _refyearset2panel_mapping_add_zero[i:i] must not be only zeroes !!
			if	(
				select count(t.res) = 0 from
				(select unnest(_refyearset2panel_mapping_add_zero[i:i]) as res) as t
				where t.res is distinct from 0
				)
			then
				raise exception 'Error 06: fn_save_ldsity_values: Array of panel refyearset combinations is an empty array!';
			end if;

			-- check that values in array _categorization_setup_add_zero[i:i] must not be only zeroes !!
			if	(
				select count(t.res) = 0 from
				(select unnest(_categorization_setup_add_zero[i:i]) as res) as t
				where t.res is distinct from 0
				)
			then
				raise exception 'Error 07: fn_save_ldsity_values: Array of categorization setups is an empty array!';
			end if;

			-- remove zeroes from array _refyearset2panel_mapping_add_zero
			with
			w1 as	(
					select unnest(_refyearset2panel_mapping_add_zero[i:i]) as refyearset2panel_mapping
					)
			select
					array_agg(w1.refyearset2panel_mapping)
			from
					w1 where w1.refyearset2panel_mapping is distinct from 0
			into
				_refyearset2panel_mapping_without_zero;

			-- remove zeroes from array _categorization_setup_add_zero
			with
			w1 as	(
					select unnest(_categorization_setup_add_zero[i:i]) as categorization_setup
					)
			select
					array_agg(w1.categorization_setup)
			from
					w1 where w1.categorization_setup is distinct from 0
			into
				_categorization_setup_without_zero;

			raise notice '_refyearset2panel_mapping_without_zero: %',_refyearset2panel_mapping_without_zero;
			--raise notice '_categorization_setup_without_zero: %',_categorization_setup_without_zero;

			_res_i := target_data.fn_save_ldsity_values_internal
						(
							_refyearset2panel_mapping_without_zero,
							_categorization_setup_without_zero,
							_threshold
						);

			if i = 1
			then
				_res := _res_i;
			else
				_res := concat(_res,' ',_res_i);
			end if;

		end loop;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		raise notice '%',_res;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		_result := 'The ldsity values were prepared successfully.'::text;

		return _result;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_save_ldsity_values(integer[], integer, double precision) IS
'The function internally calls function fn_save_ldsity_values_internal.';

grant execute on function target_data.fn_save_ldsity_values(integer[], integer, double precision) to public;
