--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_rules(integer, integer, integer, integer[], text[], boolean[], integer[], integer[][], integer[][]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_rules(
	_ldsity_object integer,						-- id from c_ldsity_object, for which object the classification rules are valid
	_areal_or_pop integer,						-- id from c_areal_or_population
	_classification_type integer,					-- id from c_classification_type
	_category_ids integer[],					-- ids from c_area_domain_category or c_sub_population_category 
	_rules text[], 							-- text representation of the classification rules which are used to classify an object
	_use_negative boolean[],					-- if there is standard classification_type, all values are false, if 
									-- change type present then each category has to have both rules variants (with use negative true and false)
	_panel_refyearset integer[][], 					-- an array of panel and reference year set combinations (id from table cm_refyearset2panel_mapping)
									-- in which are classification rules applicable, the second dimension is because of use_negative variant, where
									-- there is a different set of combinations for true and false variants
	_adc integer[][] DEFAULT NULL::int[][], 			-- optional area domain categories contraint (only for areal), e.g. accessible land
	_spc integer[][] DEFAULT NULL::int[][]				-- dtto for populational, e.g. living stem for age classification rule, it is an array of arrays hence
									--  more categories from one population can fit the constraint for each rule, 
									-- e.g. for rule which classifies high intensity of damage a prerequisite can be two categories:
									-- damaged by bark stripping, damaged by human etc.
)
RETURNS TABLE (
	areal_or_pop			integer,
	parent_id			integer,
	category_id			integer,
	classification_rule		integer,
	refyearset2panel		integer
) 
AS
$$
DECLARE
BEGIN
	IF array_length(_rules,1) != array_length(_category_ids,1)
	THEN
		RAISE EXCEPTION 'Given arrays of rules (%) must be the same length as array of category ids (%)!', _rules, _category_ids;
	END IF;

	RETURN QUERY
	WITH w_categories AS (
		SELECT 
			t1.id AS array_id,
			t1.category_id,
			CASE WHEN _areal_or_pop = 100 THEN t2.area_domain
			WHEN _areal_or_pop = 200 THEN t3.sub_population
			END AS parent_id
		FROM
			unnest(_category_ids) WITH ORDINALITY AS t1(category_id, id)
		LEFT JOIN target_data.c_area_domain_category AS t2
		ON t2.id = t1.category_id AND _areal_or_pop = 100
		LEFT JOIN target_data.c_sub_population_category AS t3
		ON t3.id = t1.category_id AND _areal_or_pop = 200

	), w_rules AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, t2.rule, t4 AS rule_id
		FROM 	w_categories AS t1
		INNER JOIN unnest(_rules) WITH ORDINALITY AS t2(rule,id) 
		ON t1.array_id = t2.id OR t1.array_id = (t2.id - array_length(_category_ids,1)) -- in case of use_negative rules, there will be array overflow (twice length)
		INNER JOIN unnest(_use_negative) WITH ORDINALITY AS t3(use_negative,id) 
		ON t1.array_id = t3.id OR t1.array_id = (t3.id - array_length(_category_ids,1)),
			target_data.fn_save_classification_rule(t1.category_id, _areal_or_pop, _ldsity_object, t2.rule, t3.use_negative) AS t4
	),
	w_rule_agg AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, array_agg(t1.rule_id ORDER BY array_id) AS rule_ids
		FROM w_rules AS t1
		GROUP BY t1.parent_id, t1.array_id, t1.category_id
	), w_hier_adc AS (
		INSERT INTO target_data.t_adc_hierarchy (variable_superior, variable, dependent)
		SELECT t2.adc, t1.category_id, true
		FROM	w_categories AS t1,
			(SELECT 
				adc, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_adc,1)),12) ) AS array_id
			FROM unnest(_adc) WITH ORDINALITY AS t1(adc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id AND t2.adc IS NOT NULL
	), w_hier_spc AS (
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable, dependent)
		SELECT t2.spc, t1.category_id, true
		FROM	w_categories AS t1,
			(SELECT 
				spc, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_spc,1)),12) ) AS array_id
			FROM unnest(_spc) WITH ORDINALITY AS t1(spc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id AND t2.spc IS NOT NULL
	)
	SELECT  _areal_or_pop, t1.parent_id, t1.category_id, t2.classification_rule, t2.refyearset2panel
	FROM 
		w_rule_agg AS t1,
		target_data.fn_save_class_rule_mapping(_areal_or_pop, t1.rule_ids, _use_negative, _panel_refyearset) AS t2
	;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_rules(integer, integer, integer, integer[], text[], boolean[], integer[], integer[][], integer[][]) IS
'Function inserts all necessary data into lookups/tables with area_domain/sub_population categories and its classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_rules(integer, integer, integer, integer[], text[], boolean[], integer[], integer[][], integer[][]) TO public;
