--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_version(integer, character varying, text, character varying, text, integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_version(_id integer, _label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text)
RETURNS integer
AS
$$
BEGIN
	IF _id IS NOT NULL 
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_version AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given version does not exist in table c_version (%)', _id;
		END IF;

		IF _label IS NOT NULL OR _description IS NOT NULL
		THEN
			UPDATE target_data.c_version
			SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
			WHERE c_version.id = _id;
		ELSE
			RAISE EXCEPTION 'At least label or description must be not null when doing an update of c_version (%, %).', _label, _description;
		END IF;
	ELSE
		RAISE EXCEPTION 'Parameter _id must not be null (%)!', _id;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_version(integer, character varying, text, character varying, text) IS
'Function provides update in c_version table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_version(integer, character varying, text, character varying, text) TO public;
