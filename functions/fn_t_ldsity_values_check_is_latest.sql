--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_t_ldsity_values_check_is_latest() CASCADE;

create or replace function target_data.fn_t_ldsity_values_check_is_latest()
returns trigger AS
$$
begin
		if	(
			select count(*) > 1 from target_data.t_ldsity_values
			where plot = new.plot
			and available_datasets = new.available_datasets
			and is_latest = true
			)
		then
			raise exception 'Error 01: fn_t_ldsity_values_check_is_latest: Not allowed to update or insert the value of is_latest in the table t_ldsity_values for records plot = % and available_datasets = %, due to the fact that the value of is_latest = TRUE would be filled more than once!', new.plot, new.available_datasets;
		end if;

	return new;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_t_ldsity_values_check_is_latest() IS
'A trigger function that checks that only one value is_latest = TRUE for the same plot and available_datasets records.';

