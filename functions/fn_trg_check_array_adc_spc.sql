--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_array_adc_spc
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_array_adc_spc();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_array_adc_spc() 
RETURNS TRIGGER AS 
$$
DECLARE
	_cm_adc	integer[];
	_cm_spc	integer[];
	_adc	integer[];
	_spc	integer[];
BEGIN
	

	CASE 	WHEN TG_TABLE_NAME IN ('c_ldsity','cm_ldsity2target_variable')
		THEN _adc := NEW.area_domain_category; _spc := NEW.sub_population_category;

	 	WHEN TG_TABLE_NAME = 'cm_ldsity2target2categorization_setup' 
		THEN _adc := NEW.adc2classification_rule; _spc := NEW.spc2classification_rule;
	ELSE
		RAISE EXCEPTION 'Not known table name (%).', TG_TABLE_NAME;
	END CASE;

	IF _adc = '{}'
	THEN
		RAISE EXCEPTION 'Given array of area domain categories is empty.';
	END IF;

	IF _spc = '{}'
	THEN
		RAISE EXCEPTION 'Given array of sub population categories is empty.';
	END IF;

	IF array_length(_adc,1) != array_length(array_remove(_adc,NULL),1)
	THEN
		RAISE EXCEPTION 'Given array (%) of area domain categories (in fact array of ids from cm_adc2classification_rule) contain null values!', _area_domain_category;
	END IF;

	IF array_length(_spc,1) != array_length(array_remove(_spc,NULL),1)
	THEN
		RAISE EXCEPTION 'Given array (%) of sub population categories (in fact array of ids from cm_spc2classification_rule) contain null values!', _sub_population_category;
	END IF;

	-- adc
	WITH w_adc AS (
		SELECT
			t1.adc, t1.id
		FROM
			unnest(_adc) WITH ORDINALITY AS t1(adc, id)
	)
	SELECT 
		array_agg(t2.id ORDER BY t1.id) AS cm_adc
	FROM
		w_adc AS t1
	INNER JOIN
		target_data.cm_adc2classification_rule AS t2
	ON t1.adc = t2.id
	INTO _cm_adc;
	
	IF array_length(_adc,1) != array_length(_cm_adc,1)
	THEN
		RAISE EXCEPTION 'Not all items from given area domain categories (%) array exist in table cm_adc2classification_rule (%).', _adc, _cm_adc;
	END IF;

	-- spc
	WITH w_spc AS (
		SELECT
			t1.spc, t1.id
		FROM
			unnest(_spc) WITH ORDINALITY AS t1(spc, id)
	)
	SELECT 
		array_agg(t2.id ORDER BY t1.id) AS cm_spc
	FROM
		w_spc AS t1
	INNER JOIN
		target_data.cm_spc2classification_rule AS t2
	ON t1.spc = t2.id
	INTO _cm_spc;
	
	IF array_length(_spc,1) != array_length(_cm_spc,1)
	THEN
		RAISE EXCEPTION 'Not all items from given sub population categories (%) array exist in table cm_spc2classification_rule (%).', _spc, _cm_spc;
	END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_array_adc_spc() IS
'Trigger function which checks the validity of ara_domain_category and sub_population_category arrays.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_array_adc_spc() TO public;

