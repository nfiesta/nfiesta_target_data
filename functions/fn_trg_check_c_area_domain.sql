--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_c_area_domain
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_c_area_domain();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_c_area_domain() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF	(
			SELECT count(*) FROM target_data.c_area_domain_category
			WHERE area_domain = NEW.id
			) = 0
		THEN
			RAISE EXCEPTION 'For newly inserted area domain (area_domain = %), not exists any record in table c_area_domain_category!', NEW.id;
		END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_c_area_domain() IS
'This trigger function checks wheter for newly inserted record in c_area_domain table exists at least one record in c_area_domain_category table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_c_area_domain() TO public;

