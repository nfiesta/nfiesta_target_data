--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_c_area_domain
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_c_area_domain_category();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_c_area_domain_category() 
RETURNS TRIGGER AS 
$$
DECLARE
	_class_type 	integer;
	_total		integer;
	_total_negat	integer;
BEGIN
		_class_type :=  (SELECT classification_type FROM target_data.c_area_domain WHERE id = NEW.area_domain);

		CASE WHEN _class_type = 100
		THEN
		
			SELECT count(*) 
			FROM target_data.cm_adc2classification_rule
			WHERE area_domain_category = NEW.id AND use_negative = false
			INTO _total;

			SELECT count(*) 
			FROM target_data.cm_adc2classification_rule
			WHERE area_domain_category = NEW.id AND use_negative = true
			INTO _total_negat;

			IF _total = 0
			THEN
				RAISE EXCEPTION 'For newly inserted area domain category (area_domain = %), not exists any record in table cm_adc2classification_rule!', NEW.area_domain;
			END IF;

			IF _total != 0 AND _total_negat != 0
			THEN
				RAISE EXCEPTION 'For newly inserted area domain category (area_domain = %) of standard classification type (%), there can''t be any classification rule with use_negative true!', NEW.area_domain, _class_type;
			END IF;

		WHEN _class_type = 200
		THEN
			SELECT count(*) 
			FROM target_data.cm_adc2classification_rule
			WHERE area_domain_category = NEW.id AND use_negative = false
			INTO _total;

			SELECT count(*) 
			FROM target_data.cm_adc2classification_rule
			WHERE area_domain_category = NEW.id AND use_negative = true
			INTO _total_negat;

			IF _total = 0
			THEN
				RAISE EXCEPTION 'For newly inserted area domain category (area_domain = %), not exists any record in table cm_adc2classification_rule!', NEW.area_domain;
			END IF;

			IF _total != _total_negat
			THEN
				RAISE EXCEPTION 'For newly inserted area domain category (area_domain = %) of change classification type (%), there has to be the same number of classification rules with use negative false and true!', NEW.area_domain, _class_type;
			END IF;

		ELSE

		END CASE;
	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_c_area_domain_category() IS
'This trigger function checks wheter for newly inserted record in c_area_domain_category table exists at least one record in cm_adc2classification_rule table. According to classification type there is also a check on the same number of classification rules with each use_negative variant.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_c_area_domain_category() TO public;

