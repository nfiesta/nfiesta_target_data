--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_c_ldsity
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_c_ldsity();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_c_ldsity() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
	IF (SELECT areal_or_population FROM target_data.c_ldsity_object WHERE id = NEW.ldsity_object) = 100
		AND NEW.sub_population_category IS NOT NULL
	THEN
		RAISE EXCEPTION 'Ldsity comes from areal ldsity object thus it has to have the sub population category NULL.';
	END IF;

RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_c_ldsity() IS
'This trigger function controls the newly inserted records into c_ldsity table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_c_ldsity() TO public;

