--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_c_sub_population
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_c_sub_population();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_c_sub_population() 
RETURNS TRIGGER AS 
$$
DECLARE
	_old_sp	integer;
	_new_sp	integer;
BEGIN
		IF	(
			SELECT count(*) FROM target_data.c_sub_population_category
			WHERE sub_population = NEW.id
			) = 0
		THEN
			RAISE EXCEPTION 'For newly inserted sub population (sub_population = %), not exists any record in table c_sub_population_category!', NEW.id;
		END IF;

		-- if the population has EXISTS/NOT EXISTS rules
		-- only one population of this kind can exist

		WITH w_old AS (
			-- find populations which already existed before new one
			SELECT t2.sub_population, t1.ldsity_object, count(t1.*) AS total
			FROM target_data.cm_spc2classification_rule AS t1
			INNER JOIN target_data.c_sub_population_category AS t2
			ON t1.sub_population_category = t2.id
			WHERE 	t2.sub_population != NEW.id AND 
				(t1.classification_rule = 'EXISTS' OR 
				t1.classification_rule = 'NOT EXISTS')
			GROUP BY t2.sub_population, t1.ldsity_object
		)
		SELECT DISTINCT sub_population
		FROM w_old
		WHERE total = 2
		INTO _old_sp;

		WITH w_new AS (
			SELECT t2.sub_population, t1.ldsity_object, count(t1.*) AS total
			FROM target_data.cm_spc2classification_rule AS t1
			INNER JOIN target_data.c_sub_population_category AS t2
			ON t1.sub_population_category = t2.id
			WHERE 	t2.sub_population = NEW.id AND 
				(t1.classification_rule = 'EXISTS' OR 
				t1.classification_rule = 'NOT EXISTS')
			GROUP BY t2.sub_population, t1.ldsity_object
		)
		SELECT DISTINCT sub_population
		FROM w_new
		WHERE total = 2
		INTO _new_sp;

		IF _old_sp IS NOT NULL AND _new_sp IS NOT NULL
		THEN
			RAISE EXCEPTION 'There is already one sub_population (id=%) with EXISTS/NOT EXISTS rules.',_old_sp;
		END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_c_sub_population() IS
'This trigger function checks whether for newly inserted record in c_sub_population table exists at least one record in c_sub_population_category table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_c_sub_population() TO public;

