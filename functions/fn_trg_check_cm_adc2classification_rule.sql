--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_adc2classification_rule
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_adc2classification_rule();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_adc2classification_rule() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF NOT(new.classification_rule = any(array['EXISTS','NOT EXISTS']))
		THEN
			IF	(
				SELECT count(*) FROM target_data.cm_adc2classrule2panel_refyearset
				WHERE adc2classification_rule = new.id
				) = 0
			THEN
				RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_adc2classification_rule: For new inserted classification rule: [area_domain_category = %, ldsity_object = %, classification_rule = "%", use_negative = %], not exists any record in table cm_adc2classrule2panel_refyearset!', new.area_domain_category, new.ldsity_object, new.classification_rule, new.use_negative;
			END IF;
		END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_adc2classification_rule() IS
'This trigger function controls that for new inserted record into cm_adc2classification_rule table exists at least one record in cm_adc2classrule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_adc2classification_rule() TO public;

