--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_c_area_domain
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_adc2classification_rule_del();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_adc2classification_rule_del() 
RETURNS TRIGGER AS 
$$
BEGIN
	IF EXISTS (
		SELECT *
		FROM  target_data.c_ldsity
		WHERE area_domain_category @> array[OLD.id])
	THEN
		RAISE EXCEPTION 'Cannot delete adc2classification_rule (id=%), it is still referenced from c_ldsity table.', OLD.id;
	END IF;

	IF EXISTS (
		SELECT *
		FROM  target_data.cm_ldsity2target_variable
		WHERE area_domain_category @> array[OLD.id])
	THEN
		RAISE EXCEPTION 'Cannot delete adc2classification_rule (id=%), it is still referenced from cm_ldsity2target_variable table.', OLD.id;
	END IF;
	
	IF EXISTS (
		SELECT *
		FROM  target_data.cm_ldsity2target2categorization_setup
		WHERE adc2classification_rule @> array[OLD.id])
	THEN
		RAISE EXCEPTION 'Cannot delete adc2classification_rule (id=%), it is still referenced from cm_ldsity2target2categorization_setup table.', OLD.id;
	END IF;

	RETURN OLD;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_adc2classification_rule_del() IS
'This trigger function checks before the delete of some category wheter its rule is referenced from c_ldsity, cm_ldsity2target_variable or cm_ldsity2target2categorization_setup.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_adc2classification_rule_del() TO public;

