--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_adc2classrule2panel_refyearset
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_adc2classrule2panel_refyearset();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_adc2classrule2panel_refyearset() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF (TG_OP = 'DELETE')
		THEN
			IF NOT target_data.fn_check_adc_rule_has_panelref( OLD.adc2classification_rule )
			THEN
				RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_adc2classrule2panel_refyearset: This record [adc2classification_rule = %, refyearset2panel = %] cannot be deleted from the cm_adc2classrule2panel_refyearset table, becouse at least one record for a given classification rule must remain in the table.', old.adc2classification_rule, old.refyearset2panel;
			END IF;

			RETURN OLD;
		END IF;

		IF (TG_OP = 'UPDATE')
		THEN
			IF NOT target_data.fn_check_adc_rule_has_panelref( OLD.adc2classification_rule )
			THEN
				RAISE EXCEPTION 'Error: 02: fn_trg_check_cm_adc2classrule2panel_refyearset: This record [adc2classification_rule = %, refyearset2panel = %] cannot be updated in the cm_adc2classrule2panel_refyearset table, becouse after update at least one record for a given classification rule must remain in the table.', old.adc2classification_rule, old.refyearset2panel;
			END IF;

			RETURN NEW;
		END IF;

	RETURN NULL;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_adc2classrule2panel_refyearset() IS
'This trigger function controls wether given of record can be deleted or updated. After delete or after update must remain at least one record for a given rule in the table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_adc2classrule2panel_refyearset() TO public;

