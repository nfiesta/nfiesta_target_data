--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_ldsity2target_variable
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() 
RETURNS TRIGGER AS 
$$
DECLARE
_test1		boolean;
_test2		boolean;
_test3		boolean;
_test4		boolean;
_variables 	integer[];
_use_negative	boolean[];
_100100		integer[];
_100200		integer[];
_200100		integer[];
_200200		integer[];
_total_new	integer;
_total_comp	integer;
_id		integer;
_pozitive_objects integer[];
_negative_objects integer[];
BEGIN

	-- check if already exists
	WITH w_new AS (
		SELECT  t1.target_variable,
			t1.ldsity, 
			t1.ldsity_object_type, 
			t1.use_negative, 
			t1.version,
			t1.area_domain_category,
			t1.sub_population_category
		FROM new_table AS t1
	), w_existing AS (
		SELECT  t1.target_variable,
			t1.ldsity, 
			t1.ldsity_object_type, 
			t1.use_negative, 
			t1.version,
			t1.area_domain_category,
			t1.sub_population_category
		FROM (SELECT * FROM target_data.cm_ldsity2target_variable EXCEPT SELECT * FROM new_table) AS t1
	)
	, w_new_agg AS (
		SELECT  t1.target_variable,
			array_agg(t1.ldsity ORDER BY ldsity, use_negative) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY ldsity, use_negative) AS ldsity_object_type, 
			array_agg(t1.use_negative ORDER BY ldsity, use_negative) AS use_negative, 
			array_agg(t1.version ORDER BY ldsity, use_negative) AS version
		FROM w_new AS t1
		GROUP BY target_variable, t1.area_domain_category, t1.sub_population_category
	), w_existing_agg AS (
		SELECT  t1.target_variable,
			array_agg(t1.ldsity ORDER BY ldsity, use_negative) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY ldsity, use_negative) AS ldsity_object_type, 
			array_agg(t1.use_negative ORDER BY ldsity, use_negative) AS use_negative, 
			array_agg(t1.version ORDER BY ldsity, use_negative) AS version
		FROM w_existing AS t1
		GROUP BY target_variable, t1.area_domain_category, t1.sub_population_category
	)
	, w_comp AS (
		SELECT t2.target_variable
		FROM w_new_agg AS t1
		INNER JOIN w_existing_agg AS t2
		ON 	coalesce(t1.ldsity,array[0]) = coalesce(t2.ldsity,array[0]) AND
		 	coalesce(t1.ldsity_object_type,array[0]) = coalesce(t2.ldsity_object_type,array[0]) AND
		 	coalesce(t1.use_negative,array[NULL::boolean]) = coalesce(t2.use_negative,array[NULL::boolean]) AND
		 	coalesce(t1.version,array[0]) = coalesce(t2.version,array[0])
	), w_comp_adsp AS (
		SELECT
			t1.*
		FROM
			w_new AS t1
		INNER JOIN
			w_existing AS t2
		ON t1.ldsity = t2.ldsity AND t1.ldsity_object_type = t2.ldsity_object_type AND t1.use_negative = t2.use_negative AND 
			t1.version = t2.version AND coalesce(t1.area_domain_category,array[0]) = coalesce(t2.area_domain_category,array[0]) AND 
			coalesce(t1.sub_population_category,array[0]) = coalesce(t2.sub_population_category,array[0])
		INNER JOIN w_comp AS t3
		ON t2.target_variable = t3.target_variable
	), w_count_new AS (
		SELECT count(*) AS total_new
		FROM w_new 
	), w_count_comp AS (
		SELECT count(*) AS total_comp
		FROM w_comp_adsp
	)
	SELECT total_new, total_comp
	FROM w_count_new, w_count_comp
	INTO _total_new, _total_comp;

--raise notice '%, %', _total_new, _total_comp;

	--_id := (SELECT max(id) 
	--	FROM 	(SELECT * FROM target_data.cm_ldsity2target_variable EXCEPT SELECT * FROM new_table) AS t1);

	IF _total_new = _total_comp
	THEN 
		--PERFORM pg_catalog.setval('target_data.cm_ldsity2target_variable_id_seq', _id, true);
		RAISE EXCEPTION 'Some of inserted/udated target variables already exists in table target_data.cm_ldsity2target_variable. This means that given combination of ldsitys is already assigned to some target variable.';
	END IF;

	-- after insert on cm_ldsity2target_variable
	WITH w_new AS (
		SELECT 	
			t2.id, t2.label, t2.description, t2.label_en, t2.description_en,
			array_agg(t1.ldsity ORDER BY t1.ldsity) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY t1.ldsity) AS ldsity_object_type,
			array_agg(t1.use_negative ORDER BY t1.ldsity) AS use_negative
		FROM 
			new_table AS t1
		INNER JOIN
			target_data.c_target_variable AS t2
		ON t1.target_variable = t2.id
		WHERE t1.ldsity_object_type = 100
		GROUP BY t2.id, t2.label, t2.description, t2.label_en, t2.description
	),
	w_existing AS (
		SELECT 	
			t2.id, t2.label, t2.description, t2.label_en, t2.description_en,
			array_agg(t1.ldsity ORDER BY t1.ldsity) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY t1.ldsity) AS ldsity_object_type,
			array_agg(t1.use_negative ORDER BY t1.ldsity) AS use_negative
		FROM 
			(SELECT * FROM target_data.cm_ldsity2target_variable EXCEPT SELECT * FROM new_table) AS t1
		INNER JOIN
			target_data.c_target_variable AS t2
		ON t1.target_variable = t2.id
		WHERE t1.ldsity_object_type = 100
		GROUP BY t2.id, t2.label, t2.description, t2.label_en, t2.description
	)
	SELECT
		t1.label = t2.label AS label_test, t1.description = t2.description AS desc_test,
		t1.label_en = t2.label_en AS label_en_test, t1.description_en = t2.description_en AS desc_en_test
	FROM
		w_new AS t1
	INNER JOIN
		w_existing AS t2
	ON t1.ldsity = t2.ldsity AND t1.use_negative = t2.use_negative
	INTO _test1, _test2, _test3, _test4;

	-- if the join on the same ldsity with ldsity_object_type = 100 results in not null output
	-- it means, there already exists target variable with given 100 ldsitys (both constrained and non-constrained)

	IF _test1 IS NOT NULL
	THEN
		-- then we can test the equality
		IF _test1 = false OR _test2 = false OR _test3 = false OR _test4 = false
		THEN
			RAISE EXCEPTION 'Newly inserted target variable has to respect the label (%) and description (%), label_en (%) and description_en (%) respectively, of existing target variables with the same local density contributions (of ldsity_object_type = 100).', _test1, _test2, _test3, _test4;
		END IF;
	END IF; 

	-- check if correct combination of ldsity_object_types is choosen
	WITH 
	w_tv AS (SELECT distinct target_variable, use_negative
		FROM new_table
	),
	w_type AS (
		SELECT t1.target_variable, t1.ldsity_object_type, t3.areal_or_population, t1.use_negative, count(*) AS total
		FROM new_table AS t1
		INNER JOIN target_data.c_ldsity AS t2
		ON t1.ldsity = t2.id
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		GROUP BY t1.target_variable, t1.ldsity_object_type, t3.areal_or_population, t1.use_negative
	),
	w_total AS (
		SELECT 	t1.target_variable AS target_variable, t1.use_negative,
			coalesce(t2.total,0) AS total_100100, coalesce(t3.total,0) AS total_100200,
			coalesce(t4.total,0) AS total_200100, coalesce(t5.total,0) AS total_200200
		FROM
			w_tv AS t1
		LEFT JOIN w_type AS t2
		ON 	t1.target_variable = t2.target_variable AND t1.use_negative = t2.use_negative
			AND t2.ldsity_object_type = 100 AND t2.areal_or_population = 100
		LEFT JOIN w_type AS t3
		ON 	t1.target_variable = t3.target_variable AND t1.use_negative = t3.use_negative
			AND t3.ldsity_object_type = 100 AND t3.areal_or_population = 200
		LEFT JOIN w_type AS t4
		ON 	t1.target_variable = t4.target_variable AND t1.use_negative = t4.use_negative
			AND t4.ldsity_object_type = 200 AND t4.areal_or_population = 100
		LEFT JOIN w_type AS t5
		ON 	t1.target_variable = t5.target_variable AND t1.use_negative = t5.use_negative
			AND t5.ldsity_object_type = 200 AND t5.areal_or_population = 200
			  
	), w_result AS (
		SELECT
			target_variable, use_negative, total_100100, total_100200, total_200100, total_200200,
			CASE
			WHEN total_100100 = 1 AND total_100200 = 0 AND total_200100 = 0 AND total_200200 = 0 THEN true	-- only one areal
			WHEN total_100100 = 0 AND total_100200 >= 1 AND total_200100 = 0 AND total_200200 = 0 THEN true -- one or more populational
			WHEN total_100100 = 1 AND total_100200 = 0 AND total_200100 = 0 AND total_200200 >= 1 THEN true -- one areal divided by one or more populational
			ELSE false -- everything else does not make sense
			END AS validity	
		FROM w_total
	)
	SELECT 	
		array_agg(total_100100 ORDER BY target_variable, use_negative),
		array_agg(total_100200 ORDER BY target_variable, use_negative),
		array_agg(total_200100 ORDER BY target_variable, use_negative),
		array_agg(total_200200 ORDER BY target_variable, use_negative),
		array_agg(use_negative ORDER BY target_variable, use_negative),
		array_agg(target_variable ORDER BY target_variable, use_negative)
	FROM w_result
	WHERE validity = false
	INTO _100100,_100200,_200100,_200200,_use_negative,_variables;

--raise notice '%, %, %, %', _100100,_100200,_200100,_200200;

	IF _variables IS NOT NULL
	THEN
		RAISE EXCEPTION 'Some of the inserted target variables (%) (can be only pozitive or negative part of the variable) has violited ldsity_object_type and areal/population combination constraints. (%, %, %, %, %)', _variables, _100100, _100200, _200100, _200200, _use_negative;
	END IF;


		WITH w AS (
			SELECT 	array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects,
				t1.use_negative
			FROM new_table AS t1
			INNER JOIN target_data.c_ldsity AS t2
			ON t1.ldsity = t2.id
			GROUP BY t1.use_negative
		)
		SELECT t1.ldsity_objects, t2.ldsity_objects
		FROM w AS t1, w AS t2
		WHERE t1.use_negative = false AND t2.use_negative = true
		INTO _pozitive_objects, _negative_objects;

		IF _negative_objects IS NOT NULL AND _pozitive_objects != _negative_objects
		THEN
			RAISE EXCEPTION 'The list of ldsity objects for negative ldsity contributions must be the same as for the pozitive ones.';
		END IF;

	RETURN NULL;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() IS
'This trigger function controls the newly inserted records into c_target_variable table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() TO public;

