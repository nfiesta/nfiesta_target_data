--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_ldsity2target_variable_unit
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable_unit();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable_unit() 
RETURNS TRIGGER AS 
$$
DECLARE
		_array_unit_of_measure	integer[];
BEGIN
		WITH
		w AS	(
				SELECT DISTINCT unit_of_measure FROM target_data.c_ldsity
				WHERE id in	(
							SELECT ldsity FROM target_data.cm_ldsity2target_variable
							WHERE target_variable = NEW.target_variable
							AND ldsity_object_type = NEW.ldsity_object_type
							)
				)
		SELECT array_agg(w.unit_of_measure) FROM w
		INTO _array_unit_of_measure;

		IF array_length(_array_unit_of_measure,1) IS DISTINCT FROM 1
		THEN
			RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_ldsity2target_variable_unit: For the newly inserted value of "target variable = %" are different units for individuals local density contributions!',NEW.target_variable;
		END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable_unit() IS
'This trigger function controls that local density contributions has the same unit.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable_unit() TO public;

