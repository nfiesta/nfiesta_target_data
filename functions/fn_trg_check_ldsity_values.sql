--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_ldsity_values
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_ldsity_values();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_ldsity_values() RETURNS TRIGGER AS $src$
    BEGIN
        IF (TG_OP = 'DELETE' or TG_OP = 'UPDATE' or TG_OP = 'INSERT') THEN
			IF (TG_TABLE_NAME = 't_ldsity_values') THEN
				--raise notice 't_ldsity_values';
				PERFORM target_data.fn_check_ldsity_values(ids, plots)
				FROM (SELECT array_agg(DISTINCT available_datasets) AS ids, array_agg(DISTINCT plot) AS plots FROM trans_table) AS t1;
	
			ELSIF (TG_TABLE_NAME = 't_available_datasets') THEN
				--raise notice 't_available_datasets';
				PERFORM target_data.fn_check_ldsity_values(ids)
				FROM (SELECT array_agg(DISTINCT id) AS ids FROM trans_table) AS t1;
			ELSE
				RAISE EXCEPTION 'fn_check_ldsity_values -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
			END IF;
	ELSE
		RAISE EXCEPTION 'fn_check_ldsity_values -- trigger operation not known: %', TG_OP;
	END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$src$ LANGUAGE plpgsql;

comment on function target_data.fn_trg_check_ldsity_values IS 'Trigger function for checking additivity. Launched by triggers on either t_ldsity_values or t_available_datasets. Triggers on insert and update on t_available_datasets dropped due to performance problem. Will be replaced by more advanced concept of additivity checks in https://gitlab.com/nfiesta/nfiesta_pg/-/issues/133.';

drop trigger if exists trg__ldsity_values__ins ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__ins
    AFTER INSERT ON target_data.t_ldsity_values
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_trg_check_ldsity_values();

drop trigger if exists trg__ldsity_values__upd ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__upd
    AFTER UPDATE ON target_data.t_ldsity_values
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_trg_check_ldsity_values();

drop trigger if exists trg__ldsity_values__del ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__del
    AFTER DELETE ON target_data.t_ldsity_values
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_trg_check_ldsity_values();

/*
triggers dropped due to prefromance problem
drop trigger if exists trg__available_datasets__ins ON target_data.t_available_datasets;
CREATE TRIGGER trg__available_datasets__ins
    AFTER INSERT ON target_data.t_available_datasets
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_trg_check_ldsity_values();

drop trigger if exists trg__available_datasets__upd ON target_data.t_available_datasets;
CREATE TRIGGER trg__available_datasets__upd
    AFTER UPDATE ON target_data.t_available_datasets
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_trg_check_ldsity_values();
*/

drop trigger if exists trg__available_datasets__del ON target_data.t_available_datasets;
CREATE TRIGGER trg__available_datasets__del
    AFTER DELETE ON target_data.t_available_datasets
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_trg_check_ldsity_values();
