--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_delete_ldsity_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_try_delete_ldsity_object_group(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_delete_ldsity_object_group(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _id)
	THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _id;
	END IF;

	RETURN NOT EXISTS (
		WITH w_group AS (
			SELECT array_agg(t3.id ORDER BY t3.id) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			INNER JOIN target_data.c_ldsity_object AS t3
			ON t2.ldsity_object = t3.id
			WHERE t1.id = _id
		), w_tv AS (
			SELECT target_variable, array_agg(t4.ldsity_object ORDER BY t4.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			INNER JOIN target_data.c_target_variable AS t6
			ON t5.target_variable = t6.id
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		)
		SELECT t2.target_variable
		FROM w_group AS t1
		INNER JOIN w_tv AS t2
		ON t1.ldsity_objects = t2.ldsity_objects);
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_delete_ldsity_object_group(integer) IS
'Function provides test if it is possible to delete records from c_ldsity_object_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_delete_ldsity_object_group(integer) TO public;
