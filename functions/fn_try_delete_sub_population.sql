--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_delete_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_try_delete_sub_population(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_delete_sub_population(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_sub_population)
	THEN RAISE EXCEPTION 'Given subpopulation does not exist in table c_sub_population (%)', _id;
	END IF;
	
	RETURN
	-- any categorization setup using given sub population
	NOT (EXISTS (
		SELECT id 
		FROM target_data.t_available_datasets
		WHERE categorization_setup IN
			(
				SELECT DISTINCT t.categorization_setup  
				FROM 
					(SELECT
							categorization_setup,
							unnest(spc2classification_rule) AS spc2classification_rule
					FROM
							target_data.cm_ldsity2target2categorization_setup
					WHERE
							spc2classification_rule IS NOT NULL
					) AS t
				WHERE t.spc2classification_rule IN 
					(SELECT id FROM target_data.cm_spc2classification_rule WHERE sub_population_category IN
						(SELECT id FROM target_data.c_sub_population_category WHERE sub_population = _id))
			)
		)
	OR
	-- any target variable configured with constraints using given sub population
	EXISTS (
		SELECT DISTINCT t.ldsity
		FROM 
			(SELECT
					ldsity,
					unnest(sub_population_category) AS spc2classification_rule
			FROM
					target_data.cm_ldsity2target_variable
			WHERE
					sub_population_category IS NOT NULL
			) AS t
		WHERE t.spc2classification_rule IN 
			(SELECT id FROM target_data.cm_spc2classification_rule WHERE sub_population_category in
				(SELECT id FROM target_data.c_sub_population_category WHERE sub_population = _id))
	)
	);
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_delete_sub_population(integer) IS
'Function provides test if it is possible to delete records from c_sub_population table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_delete_sub_population(integer) TO public;
