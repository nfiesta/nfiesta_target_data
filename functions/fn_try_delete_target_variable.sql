--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_delete_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_try_delete_target_variable(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_delete_target_variable(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _id)
	THEN RAISE EXCEPTION 'Given target variable does not exist in table c_target_variable (%)', _id;
	END IF;

	RETURN NOT EXISTS (
		SELECT t1.id
		FROM target_data.c_target_variable AS t1
		INNER JOIN target_data.cm_ldsity2target_variable AS t2
		ON t1.id = t2.target_variable
		INNER JOIN target_data.cm_ldsity2target2categorization_setup AS t3
		ON t2.id = t3.ldsity2target_variable
		INNER JOIN target_data.t_categorization_setup AS t4
		ON t3.categorization_setup = t4.id
		INNER JOIN target_data.t_available_datasets AS t5
		ON t4.id = t5.categorization_setup
		-- t_ldsity_values does not have to be joined, it does not have to be even filled, when there are 0 values
		-- for some of the available_datasets
		WHERE t1.id = _id);
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_delete_target_variable(integer) IS
'Function provides test if it is possible to delete records from c_target_variable table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_delete_target_variable(integer) TO public;
