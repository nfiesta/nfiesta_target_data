--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_pyrgroup_refyearset2panel_mapping
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_try_pyrgroup_refyearset2panel_mapping(integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_pyrgroup_refyearset2panel_mapping(
	_refyearset2panels integer[])
RETURNS bool
AS
$$
DECLARE 
	_check_result boolean;

BEGIN

	IF _refyearset2panels IS NULL
	THEN RAISE EXCEPTION 'Parameter _refyearset2panels must be provided.';
	END IF;	

	-- array empty
	IF COALESCE(array_length(_refyearset2panels,1),0) = 0
	THEN RAISE EXCEPTION 'The array of _refyearset2panels parameter is empty.';
	END IF;

	-- array contains NULL values
	IF COALESCE(array_length(array_remove(_refyearset2panels,NULL::int),1),0) != array_length(_refyearset2panels,1)
	THEN RAISE EXCEPTION 'The array of _refyearset2panels parameter contains NULL values.';
	END IF;
	
	SELECT EXISTS(
	SELECT 
		array_agg(refyearset2panel) AS refyearset2panel 
	FROM 
	target_data.t_panel_refyearset_group 
	GROUP BY panel_refyearset_group HAVING array_agg(refyearset2panel) <@ _refyearset2panels AND array_agg(refyearset2panel) @> _refyearset2panels) INTO _check_result;

RETURN _check_result; 

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_pyrgroup_refyearset2panel_mapping(integer[]) IS
'Function checks if a combination of panels and reference year sets already exists in t_panel_refyearset_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_pyrgroup_refyearset2panel_mapping(integer[]) TO public;

