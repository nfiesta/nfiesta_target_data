--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_update_pyrgroup
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_update_pyrgroup(character varying, text, character varying, text, integer);

CREATE OR REPLACE FUNCTION target_data.fn_update_pyrgroup(
	_label character varying, 
	_description text, 
	_label_en character varying, 
	_description_en text, 
	_id integer)
RETURNS void
AS
$$
BEGIN
	
	IF _id IS NULL THEN 
		RAISE EXCEPTION 'Parameter _id must not be null.';
	END IF;

	IF NOT EXISTS (SELECT * FROM target_data.c_panel_refyearset_group AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given group of panels and reference year sets (%) does not exist in c_panel_refyearset_group table.', _id;
	END IF;
	
	IF _label IS NULL THEN 
		RAISE EXCEPTION 'Parameter label must be given.';
	END IF;

	IF _description IS NULL THEN 
		RAISE EXCEPTION 'Parameter description must be given.';
	END IF;

	IF _label_en IS NULL THEN 
		RAISE EXCEPTION 'Parameter label_en must be given.';
	END IF;

	IF _description_en IS NULL THEN 
		RAISE EXCEPTION 'Parameter description_en must be given.';
	END IF;

	UPDATE target_data.c_panel_refyearset_group 
	SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
	WHERE c_panel_refyearset_group.id = _id;	

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_update_pyrgroup(character varying, text, character varying, text, integer) IS
'Function provides update in c_panel_refyearset_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_update_pyrgroup(character varying, text, character varying, text, integer) TO public;
