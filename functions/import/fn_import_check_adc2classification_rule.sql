--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_adc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_adc2classification_rule(integer, integer, integer, text) CASCADE;

create or replace function target_data.fn_import_check_adc2classification_rule
(
	_adc2classification_rule			integer,
	_area_domain_category__etl_id		integer,
	_ldsity_object__etl_id				integer,
	_classification_rule				text
)
returns table
(	
	adc2classification_rule					integer,
	ldsity_object__id						integer,
	ldsity_object__label					varchar,
	ldsity_object__description				text,
	ldsity_object__label_en					varchar,
	ldsity_object__description_en			text,
	area_domain_category__id				integer,
	area_domain_category__label				varchar,
	area_domain_category__description		text,
	area_domain_category__label_en			varchar,
	area_domain_category__description_en	text,
	etl_id									integer,
	classification_rule						text
)
as
$$
declare
		_ldsity_object__label					character varying;
		_ldsity_object__description				text;
		_ldsity_object__label_en				character varying;
		_ldsity_object__description_en 			text;
		_area_domain_category__label			character varying;
		_area_domain_category__description		text;
		_area_domain_category__label_en			character varying;
		_area_domain_category__description_en 	text;
		_res_etl_id								integer;
		_res_classification_rule				text;
begin
		if _adc2classification_rule is null
		then
			raise exception 'Error 01: fn_import_check_adc2classification_rule: Input argument _adc2classification_rule must not be NULL!';
		end if;
	
		if _area_domain_category__etl_id is null
		then
			raise exception 'Error 02: fn_import_check_adc2classification_rule: Input argument _area_domain_category__etl_id must not be NULL!';
		end if;
	
		if _ldsity_object__etl_id is null
		then
			raise exception 'Error 03: fn_import_check_adc2classification_rule: Input argument _ldsity_object__etl_id must not be NULL!';
		end if;
	
		if _classification_rule is null
		then
			raise exception 'Error 04: fn_import_check_adc2classification_rule: Input argument _classification_rule must not be NULL!';
		end if;
	
		with
		w1 as	(
				select
						_adc2classification_rule as adc2classification_rule,
						_ldsity_object__etl_id as ldsity_object__id,
						_area_domain_category__etl_id as area_domain_category__id
				)
		,w2 as	(
				select
						w1.adc2classification_rule,
						w1.ldsity_object__id,
						clo.label as ldsity_object__label,
						clo.description as ldsity_object__description,
						clo.label_en as ldsity_object__label_en,
						clo.description_en as ldsity_object__description_en,
						w1.area_domain_category__id,
						cadc.label as area_domain_category__label,
						cadc.description as area_domain_category__description,
						cadc.label_en as area_domain_category__label_en,
						cadc.description_en as area_domain_category__description_en
				from
						w1
						inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
						inner join target_data.c_area_domain_category as cadc on w1.area_domain_category__id = cadc.id
				)
		,w3 as	(
				select cacr.* from target_data.cm_adc2classification_rule as cacr
				where cacr.area_domain_category = _area_domain_category__etl_id
				and cacr.ldsity_object = _ldsity_object__etl_id
				and cacr.classification_rule = _classification_rule
				)
		select
				w2.ldsity_object__label,
				w2.ldsity_object__description,
				w2.ldsity_object__label_en,
				w2.ldsity_object__description_en,
				w2.area_domain_category__label,
				w2.area_domain_category__description,
				w2.area_domain_category__label_en,
				w2.area_domain_category__description_en,
				w3.id as etl_id,
				w3.classification_rule
		from
				w2
				left join w3
				on w2.ldsity_object__id = w3.ldsity_object
				and w2.area_domain_category__id = w3.area_domain_category
		into
				_ldsity_object__label,
				_ldsity_object__description,
				_ldsity_object__label_en,
				_ldsity_object__description_en,
				_area_domain_category__label,
				_area_domain_category__description,
				_area_domain_category__label_en,
				_area_domain_category__description_en,
				_res_etl_id,
				_res_classification_rule;
	
		return query
		select
				_adc2classification_rule,
				_ldsity_object__etl_id,
				_ldsity_object__label,
				_ldsity_object__description,
				_ldsity_object__label_en,
				_ldsity_object__description_en,
				_area_domain_category__etl_id,
				_area_domain_category__label,
				_area_domain_category__description,
				_area_domain_category__label_en,
				_area_domain_category__description_en,
				_res_etl_id,
				_res_classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_adc2classification_rule(integer, integer, integer, text) is
'Function returns record ID from table cm_adc2classification_rule based on given parameters.';

grant execute on function target_data.fn_import_check_adc2classification_rule(integer, integer, integer, text) to public;