--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_area_domain(integer, character varying) CASCADE;

create or replace function target_data.fn_import_check_area_domain
(
	_area_domain		integer,
	_label_en			character varying
)
returns table
(
	area_domain			integer,
	etl_id				integer,
	label				character varying,
	description			text,
	label_en			character varying,
	description_en		text
)
as
$$
declare
		_res_etl_id							integer;
		_res_label							character varying;
		_res_description					text;
		_res_label_en						character varying;
		_res_description_en					text;
begin
		if _area_domain is null
		then
			raise exception 'Error 01: fn_import_check_area_domain: Input argument _area_domain must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_import_check_area_domain: Input argument _label_en must not be NULL!';
		end if;
	
		select
				cad.id as etl_id,
				cad.label,
				cad.description,
				cad.label_en,
				cad.description_en
		from 
				target_data.c_area_domain as cad
		where
				cad.label_en = _label_en
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query
		select
				_area_domain,
				_res_etl_id, 
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_area_domain(integer, character varying) is
'Function returns record ID from table c_area_domain based on given parameters.';


grant execute on function target_data.fn_import_check_area_domain(integer, character varying) to public;