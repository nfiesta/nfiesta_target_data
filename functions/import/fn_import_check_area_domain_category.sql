--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_area_domain_category(integer, integer, character varying) CASCADE;

create or replace function target_data.fn_import_check_area_domain_category
(
	_area_domain_category		integer,
	_area_domain__etl_id		integer,
	_label_en					character varying
)
returns table
(
	area_domain_category		integer,
	area_domain__etl_id			integer,
	area_domain__label			character varying,
	area_domain__description	text,
	area_domain__label_en		character varying,
	area_domain__description_en text,
	etl_id						integer,
	label						character varying,
	description					text,
	label_en					character varying,
	description_en				text
)
as
$$
declare
		_area_domain__label					character varying;
		_area_domain__description			text;
		_area_domain__label_en				character varying;
		_area_domain__description_en 		text;
		_res_etl_id							integer;
		_res_label							character varying;
		_res_description					text;
		_res_label_en						character varying;
		_res_description_en					text;
begin
		if _area_domain_category is null
		then
			raise exception 'Error 01: fn_import_check_area_domain_category: Input argument _area_domain_category must not be NULL!';
		end if;
	
		if _area_domain__etl_id is null
		then
			raise exception 'Error 02: fn_import_check_area_domain_category: Input argument _area_domain__etl_id must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 03: fn_import_check_area_domain_category: Input argument _label_en must not be NULL!';
		end if;
	
		with
		w1 as	(select cad.* from target_data.c_area_domain as cad where cad.id = _area_domain__etl_id)
		,w2 as	(
				select
						cadc.id as etl_id,
						cadc.label,
						cadc.description,
						cadc.label_en,
						cadc.description_en,
						cadc.area_domain
				from 
						target_data.c_area_domain_category as cadc
				where
						cadc.area_domain = _area_domain__etl_id
				and
						cadc.label_en = _label_en
				)
		select
				w1.label as area_domain__label,
				w1.description as area_domain__description,
				w1.label_en as area_domain__label_en,
				w1.description_en as area_domain__description_en,
				w2.etl_id,
				w2.label,
				w2.description,
				w2.label_en,
				w2.description_en
		from
				w1 left join w2 on w1.id = w2.area_domain
		into
				_area_domain__label,
				_area_domain__description,
				_area_domain__label_en,
				_area_domain__description_en,
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query
		select
				_area_domain_category,
				_area_domain__etl_id,
				_area_domain__label,
				_area_domain__description,
				_area_domain__label_en,
				_area_domain__description_en,
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_area_domain_category(integer, integer, character varying) is
'Function returns record ID from table c_area_domain based on given parameters.';

grant execute on function target_data.fn_import_check_area_domain_category(integer, integer, character varying) to public;