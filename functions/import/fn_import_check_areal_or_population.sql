--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_areal_or_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_areal_or_population(integer, varchar) CASCADE;

create or replace function target_data.fn_import_check_areal_or_population
(
	_id			integer,
	_label_en	varchar
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text
)
as
$$
declare
		_res_etl_id				integer;
		_res_label				character varying;
		_res_description		text;
		_res_label_en			character varying;
		_res_description_en		text;
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_check_areal_or_population: Input argument _id must not be NULL!';
		end if; 
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_import_check_areal_or_population: Input argument _label_en must not be NULL!';
		end if;
	
		select
				caop.id,
				caop.label,
				caop.description,
				caop.label as label_en,
				caop.description as description_en
		from
				target_data.c_areal_or_population as caop
		where
				caop.label = _label_en
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query select _id, _res_etl_id, _res_label, _res_description, _res_label_en, _res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_areal_or_population(integer, varchar) is
'Function returns record ID from table c_areal_or_population based on given parameters.';

grant execute on function target_data.fn_import_check_areal_or_population(integer, varchar) to public;