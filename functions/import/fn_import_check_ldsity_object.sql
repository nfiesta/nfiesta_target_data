--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_ldsity_object(integer, character varying) CASCADE;

create or replace function target_data.fn_import_check_ldsity_object
(
	_ldsity_object	integer,
	_label_en		character varying
)
returns table
(
	ldsity_object					integer,
	id								integer,
	label							character varying,
	description						text,
	label_en						character varying,
	description_en					text,
	areal_or_population__label		character varying,
	areal_or_population__label_en	character varying
)
as
$$
declare
		_res_etl_id							integer;
		_res_label							character varying;
		_res_description					text;
		_res_label_en						character varying;
		_res_description_en					text;
		_res_areal_or_population__label		character varying;
		_res_areal_or_population__label_en	character varying;
begin
		if _ldsity_object is null
		then
			raise exception 'Error 01: fn_import_check_ldsity_object: Input argument _ldsity_object must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_import_check_ldsity_object: Input argument _label_en must not be NULL!';
		end if;
	
		with
		w1 as	(
				select
						clo.id,
						clo.label,
						clo.description,
						clo.label_en,
						clo.description_en,
						clo.areal_or_population
				from
						target_data.c_ldsity_object as clo
				where
						clo.label_en = _label_en
				)
		select
				w1.id,
				w1.label,
				w1.description,
				w1.label_en,
				w1.description_en,
				caop.label as areal_or_population__label,
				caop.label as areal_or_population__label_en
		from 
				w1
				inner join target_data.c_areal_or_population as caop
				on w1.areal_or_population = caop.id
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en,
				_res_areal_or_population__label,
				_res_areal_or_population__label_en;

		return query
		select
				_ldsity_object,
				_res_etl_id, 
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en,
				_res_areal_or_population__label,
				_res_areal_or_population__label_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_ldsity_object(integer, character varying) is
'Function returns record ID from table c_ldsity_object based on given parameters.';

grant execute on function target_data.fn_import_check_ldsity_object(integer, character varying) to public;