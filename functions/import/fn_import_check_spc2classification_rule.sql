--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_spc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_spc2classification_rule(integer, integer, integer, text) CASCADE;

create or replace function target_data.fn_import_check_spc2classification_rule
(
	_spc2classification_rule			integer,
	_sub_population_category__etl_id	integer,
	_ldsity_object__etl_id				integer,
	_classification_rule				text
)
returns table
(	
	spc2classification_rule					integer,
	ldsity_object__id						integer,
	ldsity_object__label					varchar,
	ldsity_object__description				text,
	ldsity_object__label_en					varchar,
	ldsity_object__description_en			text,
	sub_population_category__id				integer,
	sub_population_category__label			varchar,
	sub_population_category__description	text,
	sub_population_category__label_en		varchar,
	sub_population_category__description_en	text,
	etl_id									integer,
	classification_rule						text
)
as
$$
declare
		_ldsity_object__label						character varying;
		_ldsity_object__description					text;
		_ldsity_object__label_en					character varying;
		_ldsity_object__description_en 				text;
		_sub_population_category__label				character varying;
		_sub_population_category__description		text;
		_sub_population_category__label_en			character varying;
		_sub_population_category__description_en 	text;
		_res_etl_id									integer;
		_res_classification_rule					text;
begin
		if _spc2classification_rule is null
		then
			raise exception 'Error 01: fn_import_check_spc2classification_rule: Input argument _spc2classification_rule must not be NULL!';
		end if;
	
		if _sub_population_category__etl_id is null
		then
			raise exception 'Error 02: fn_import_check_spc2classification_rule: Input argument _sub_population_category__etl_id must not be NULL!';
		end if;
	
		if _ldsity_object__etl_id is null
		then
			raise exception 'Error 03: fn_import_check_spc2classification_rule: Input argument _ldsity_object__etl_id must not be NULL!';
		end if;
	
		if _classification_rule is null
		then
			raise exception 'Error 04: fn_import_check_spc2classification_rule: Input argument _classification_rule must not be NULL!';
		end if;
	
		with
		w1 as	(
				select
						_spc2classification_rule as spc2classification_rule,
						_ldsity_object__etl_id as ldsity_object__id,
						_sub_population_category__etl_id as sub_population_category__id
				)
		,w2 as	(
				select
						w1.spc2classification_rule,
						w1.ldsity_object__id,
						clo.label as ldsity_object__label,
						clo.description as ldsity_object__description,
						clo.label_en as ldsity_object__label_en,
						clo.description_en as ldsity_object__description_en,
						w1.sub_population_category__id,
						cspc.label as sub_population_category__label,
						cspc.description as sub_population_category__description,
						cspc.label_en as sub_population_category__label_en,
						cspc.description_en as sub_population_category__description_en
				from
						w1
						inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
						inner join target_data.c_sub_population_category as cspc on w1.sub_population_category__id = cspc.id
				)
		,w3 as	(
				select cacr.* from target_data.cm_spc2classification_rule as cacr
				where cacr.sub_population_category = _sub_population_category__etl_id
				and cacr.ldsity_object = _ldsity_object__etl_id
				and cacr.classification_rule = _classification_rule
				)
		select
				w2.ldsity_object__label,
				w2.ldsity_object__description,
				w2.ldsity_object__label_en,
				w2.ldsity_object__description_en,
				w2.sub_population_category__label,
				w2.sub_population_category__description,
				w2.sub_population_category__label_en,
				w2.sub_population_category__description_en,
				w3.id as etl_id,
				w3.classification_rule
		from
				w2
				left join w3
				on w2.ldsity_object__id = w3.ldsity_object
				and w2.sub_population_category__id = w3.sub_population_category
		into
				_ldsity_object__label,
				_ldsity_object__description,
				_ldsity_object__label_en,
				_ldsity_object__description_en,
				_sub_population_category__label,
				_sub_population_category__description,
				_sub_population_category__label_en,
				_sub_population_category__description_en,
				_res_etl_id,
				_res_classification_rule;
	
		return query
		select
				_spc2classification_rule,
				_ldsity_object__etl_id,
				_ldsity_object__label,
				_ldsity_object__description,
				_ldsity_object__label_en,
				_ldsity_object__description_en,
				_sub_population_category__etl_id,
				_sub_population_category__label,
				_sub_population_category__description,
				_sub_population_category__label_en,
				_sub_population_category__description_en,
				_res_etl_id,
				_res_classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_spc2classification_rule(integer, integer, integer, text) is
'Function returns record ID from table cm_spc2classification_rule based on given parameters.';

grant execute on function target_data.fn_import_check_spc2classification_rule(integer, integer, integer, text) to public;