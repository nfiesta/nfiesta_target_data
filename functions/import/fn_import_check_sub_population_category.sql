--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_sub_population_category(integer, integer, character varying) CASCADE;

create or replace function target_data.fn_import_check_sub_population_category
(
	_sub_population_category		integer,
	_sub_population__etl_id			integer,
	_label_en						character varying
)
returns table
(
	sub_population_category			integer,
	sub_population__etl_id			integer,
	sub_population__label			character varying,
	sub_population__description		text,
	sub_population__label_en		character varying,
	sub_population__description_en 	text,
	etl_id							integer,
	label							character varying,
	description						text,
	label_en						character varying,
	description_en					text
)
as
$$
declare
		_sub_population__label					character varying;
		_sub_population__description			text;
		_sub_population__label_en				character varying;
		_sub_population__description_en 		text;
		_res_etl_id								integer;
		_res_label								character varying;
		_res_description						text;
		_res_label_en							character varying;
		_res_description_en						text;
begin
		if _sub_population_category is null
		then
			raise exception 'Error 01: fn_import_check_sub_population_category: Input argument _sub_population_category must not be NULL!';
		end if;
	
		if _sub_population__etl_id is null
		then
			raise exception 'Error 02: fn_import_check_sub_population_category: Input argument _sub_population__etl_id must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 03: fn_import_check_sub_population_category: Input argument _label_en must not be NULL!';
		end if;
	
		with
		w1 as	(select cad.* from target_data.c_sub_population as cad where cad.id = _sub_population__etl_id)
		,w2 as	(
				select
						cadc.id as etl_id,
						cadc.label,
						cadc.description,
						cadc.label_en,
						cadc.description_en,
						cadc.sub_population
				from 
						target_data.c_sub_population_category as cadc
				where
						cadc.sub_population = _sub_population__etl_id
				and
						cadc.label_en = _label_en
				)
		select
				w1.label as sub_population__label,
				w1.description as sub_population__description,
				w1.label_en as sub_population__label_en,
				w1.description_en as sub_population__description_en,
				w2.etl_id,
				w2.label,
				w2.description,
				w2.label_en,
				w2.description_en
		from
				w1 left join w2 on w1.id = w2.sub_population
		into
				_sub_population__label,
				_sub_population__description,
				_sub_population__label_en,
				_sub_population__description_en,
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query
		select
				_sub_population_category,
				_sub_population__etl_id,
				_sub_population__label,
				_sub_population__description,
				_sub_population__label_en,
				_sub_population__description_en,
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_sub_population_category(integer, integer, character varying) is
'Function returns record ID from table c_sub_population based on given parameters.';

grant execute on function target_data.fn_import_check_sub_population_category(integer, integer, character varying) to public;