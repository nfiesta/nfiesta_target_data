--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_unit_of_measure
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_unit_of_measure(integer, character varying) CASCADE;

create or replace function target_data.fn_import_check_unit_of_measure
(
	_unit_of_measure	integer,
	_label_en			character varying
)
returns table
(
	unit_of_measure		integer,
	etl_id				integer,
	label				character varying,
	description			text,
	label_en			character varying,
	description_en		text
)
as
$$
declare
		_res_etl_id							integer;
		_res_label							character varying;
		_res_description					text;
		_res_label_en						character varying;
		_res_description_en					text;
begin
		if _unit_of_measure is null
		then
			raise exception 'Error 01: fn_import_check_unit_of_measure: Input argument _unit_of_measure must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_import_check_unit_of_measure: Input argument _label_en must not be NULL!';
		end if;
	
		select
				cuom.id as etl_id,
				cuom.label,
				cuom.description,
				cuom.label_en,
				cuom.description_en
		from 
				target_data.c_unit_of_measure as cuom
		where
				cuom.label_en = _label_en
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query
		select
				_unit_of_measure,
				_res_etl_id, 
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_unit_of_measure(integer, character varying) is
'Function returns record ID from table c_unit_of_measure based on given parameters.';

grant execute on function target_data.fn_import_check_unit_of_measure(integer, character varying) to public;