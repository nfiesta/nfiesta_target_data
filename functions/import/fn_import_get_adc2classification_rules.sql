--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_adc2classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_adc2classification_rules(integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_adc2classification_rules
(
	_adc2classification_rule		integer,
	_area_domain_category__etl_id	integer,
	_ldsity_object__etl_id			integer,
	_etl_id							integer[] default null::integer[]
)
returns table
(
	adc2classification_rule					integer,
	ldsity_object__id						integer,
	ldsity_object__label					varchar,
	ldsity_object__description				text,
	ldsity_object__label_en					varchar,
	ldsity_object__description_en			text,
	area_domain_category__id				integer,
	area_domain_category__label				varchar,
	area_domain_category__description		text,
	area_domain_category__label_en			varchar,
	area_domain_category__description_en	text,
	etl_id									integer,
	classification_rule						text
)
as
$$
declare
begin
		if _adc2classification_rule is null
		then
			raise exception 'Error 01: fn_import_get_adc2classification_rules: Input argument _adc2classification_rule must not be NULL!';
		end if;
	
		if _area_domain_category__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_adc2classification_rules: Input argument _area_domain_category__etl_id must not be NULL!';
		end if;
	
		if _ldsity_object__etl_id is null
		then
			raise exception 'Error 03: fn_import_get_adc2classification_rules: Input argument _ldsity_object__etl_id must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 04: fn_import_get_adc2classification_rules: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		with
		w1 as	(
				select
						_adc2classification_rule as adc2classification_rule,
						cacr.id as etl_id,
						cacr.area_domain_category as area_domain_category__id,
						cacr.ldsity_object as ldsity_object__id,
						cacr.classification_rule
				from
						target_data.cm_adc2classification_rule as cacr
				where
						cacr.area_domain_category = _area_domain_category__etl_id
				and
						cacr.ldsity_object = _ldsity_object__etl_id
				and
						cacr.id not in (select unnest(_etl_id))
				)
		select
				w1.adc2classification_rule,
				w1.ldsity_object__id,
				clo.label as ldsity_object__label,
				clo.description as ldsity_object__description,
				clo.label_en as ldsity_object__label_en,
				clo.description_en as ldsity_object__description_en,
				w1.area_domain_category__id,
				cadc.label as area_domain_category__label,
				cadc.description as area_domain_category__description,
				cadc.label_en as area_domain_category__label_en,
				cadc.description_en as area_domain_category__description_en,
				w1.etl_id,
				w1.classification_rule
		from
				w1
				inner join target_data.c_area_domain_category as cadc on w1.area_domain_category__id = cadc.id
				inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
		order 
				by w1.ldsity_object__id, w1.area_domain_category__id, w1.adc2classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_adc2classification_rules(integer, integer, integer, integer[]) is
'Function returns records from table cm_adc2classification_rule.';

grant execute on function target_data.fn_import_get_adc2classification_rules(integer, integer, integer, integer[]) to public;