--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_adc_hierarchies
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_adc_hierarchies(integer, integer, integer, boolean, integer[]) CASCADE;

create or replace function target_data.fn_import_get_adc_hierarchies
(
	_id								integer,
	_variable_superior__etl_id		integer,
	_variable__etl_id				integer,
	_dependent						boolean,
	_etl_id							integer[] default null::integer[]
)
returns table
(
	id					integer,
	etl_id				integer,
	variable_superior	integer,
	variable			integer,
	dependent			boolean
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_adc_hierarchies: Input argument _id must not be NULL!';
		end if;
	
		if _variable_superior__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_adc_hierarchies: Input argument _variable_superior__etl_id must not be NULL!';
		end if;
	
		if _variable__etl_id is null
		then
			raise exception 'Error 03: fn_import_get_adc_hierarchies: Input argument _variable__etl_id must not be NULL!';
		end if;
	
		if _dependent is null
		then
			raise exception 'Error 04: fn_import_get_adc_hierarchies: Input argument _dependent must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 05: fn_import_get_adc_hierarchies: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		select
				_id as id,
				tah.id as etl_id,
				tah.variable_superior,
				tah.variable,
				tah.dependent
		from
				target_data.t_adc_hierarchy as tah
		where
				tah.id not in (select unnest(_etl_id))
		and		tah.variable_superior = _variable_superior__etl_id
		and		tah.variable = _variable__etl_id
		and		tah.dependent = _dependent;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_adc_hierarchies(integer, integer, integer, boolean, integer[]) is
'Function returns records from table t_adc_hierarchy.';

grant execute on function target_data.fn_import_get_adc_hierarchies(integer, integer, integer, boolean, integer[]) to public;