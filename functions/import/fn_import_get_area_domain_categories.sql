--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_area_domain_categories
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_area_domain_categories(integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_area_domain_categories
(
	_area_domain_category	integer,
	_area_domain__etl_id	integer,
	_etl_id					integer[] default null::integer[]
)
returns table
(
	area_domain_category	integer,
	etl_id					integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text
)
as
$$
declare
begin
		if _area_domain_category is null
		then
			raise exception 'Error 01: fn_import_get_area_domain_categories: Input argument _area_domain_category must not be NULL!';
		end if;
	
		if _area_domain__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_area_domain_categories: Input argument _area_domain__etl_id must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 03: fn_import_get_area_domain_categories: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		select
				_area_domain_category as area_domain_category,
				cadc.id as etl_id,
				cadc.label,
				cadc.description,
				cadc.label_en,
				cadc.description_en
		from 
				target_data.c_area_domain_category as cadc
		where
				cadc.area_domain = _area_domain__etl_id
		and
				cadc.id not in (select unnest(_etl_id))
		order
				by cadc.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_area_domain_categories(integer, integer, integer[]) is
'Function returns records from table c_area_domain_category.';

grant execute on function target_data.fn_import_get_area_domain_categories(integer, integer, integer[]) to public;