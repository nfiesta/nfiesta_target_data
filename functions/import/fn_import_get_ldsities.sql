--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_ldsities
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_ldsities(integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_ldsities
(
	_ldsity_object	integer,
	_ldsity			integer,
	_etl_id			integer[] default null::integer[]
)
returns table
(
	ldsity											integer,
	id												integer,
	label											varchar,
	description										text,
	label_en										varchar,
	description_en									text,
	ldsity_object__id								integer,
	ldsity_object__label							varchar,
	ldsity_object__description						text,
	ldsity_object__label_en							varchar,
	ldsity_object__description_en					text,
	column_expression								text,
	unit_of_measure__id								integer,
	unit_of_measure__label							varchar,
	unit_of_measure__description					text,
	unit_of_measure__label_en						varchar,
	unit_of_measure__description_en					text,	
	area_domain_category__id						integer[],
	area_domain_category__classification_rule		text[],
	sub_population_category__id						integer[],
	sub_population_category__classification_rule	text[],
	definition_variant__id							integer[],
	definition_variant__label						varchar[],
	definition_variant__description					text[],
	definition_variant__label_en					varchar[],
	definition_variant__description_en				text[]
)
as
$$
declare
begin
		if _ldsity_object is null
		then
			raise exception 'Error 01: fn_import_get_ldsities: Input argument _ldsity_object must not be NULL!';
		end if;

		if _ldsity is null
		then
			raise exception 'Error 02: fn_import_get_ldsities: Input argument _ldsity must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 03: fn_import_get_ldsities: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		with
		w1 as	(
				select
						_ldsity as ldsity,
						cl.id,
						cl.label,
						cl.description,
						cl.label_en,
						cl.description_en,
						cl.ldsity_object as ldsity_object__id,
						cl.column_expression,
						cl.unit_of_measure as unit_of_measure__id,
						cl.area_domain_category as area_domain_category__id,
						cl.sub_population_category as sub_population_category__id,
						cl.definition_variant as definition_variant__id
				from 
						target_data.c_ldsity as cl
				where
						cl.ldsity_object = _ldsity_object
				and 
						cl.id not in (select unnest(_etl_id))
				)
		,w2 as	(
				select
						w1.*,
						clo.label as ldsity_object__label,
						clo.description as ldsity_object__description,
						clo.label_en as ldsity_object__label_en,
						clo.description_en as ldsity_object__description_en,
						cuom.label as unit_of_measure__label,
						cuom.description as unit_of_measure__description,
						cuom.label_en as unit_of_measure__label_en,
						cuom.description_en as unit_of_measure__description_en,
						-------------------------------------------------------
						case
							when w1.area_domain_category__id is null then null::text[]
							else	(
									select array_agg(cacr.classification_rule order by cacr.id)
									from target_data.cm_adc2classification_rule as cacr
									where cacr.id in (select unnest(w1.area_domain_category__id))
									)
						end as area_domain_category__classification_rule,
						-------------------------------------------------------
						case
							when w1.sub_population_category__id is null then null::text[]
							else	(
									select array_agg(cscr.classification_rule order by cscr.id)
									from target_data.cm_spc2classification_rule as cscr
									where cscr.id in (select unnest(w1.sub_population_category__id))
									)
						end as sub_population_category__classification_rule,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::varchar[]
							else	(
									select array_agg(cdv_1.label order by cdv_1.id)
									from target_data.c_definition_variant as cdv_1
									where cdv_1.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__label,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::text[]
							else	(
									select array_agg(cdv_2.description order by cdv_2.id)
									from target_data.c_definition_variant as cdv_2
									where cdv_2.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__description,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::varchar[]
							else	(
									select array_agg(cdv_3.label_en order by cdv_3.id)
									from target_data.c_definition_variant as cdv_3
									where cdv_3.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__label_en,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::text[]
							else	(
									select array_agg(cdv_4.description_en order by cdv_4.id)
									from target_data.c_definition_variant as cdv_4
									where cdv_4.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__description_en
				from
						w1
						inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
						left join target_data.c_unit_of_measure as cuom on w1.unit_of_measure__id = cuom.id
				)
		select 
				w2.ldsity,
				w2.id,
				w2.label,
				w2.description,
				w2.label_en,
				w2.description_en,
				w2.ldsity_object__id,
				w2.ldsity_object__label,
				w2.ldsity_object__description,
				w2.ldsity_object__label_en,
				w2.ldsity_object__description_en,
				w2.column_expression,
				w2.unit_of_measure__id,
				w2.unit_of_measure__label,
				w2.unit_of_measure__description,
				w2.unit_of_measure__label_en,
				w2.unit_of_measure__description_en,
				w2.area_domain_category__id,
				w2.area_domain_category__classification_rule,
				w2.sub_population_category__id,
				w2.sub_population_category__classification_rule,
				w2.definition_variant__id,
				w2.definition_variant__label,
				w2.definition_variant__description,
				w2.definition_variant__label_en,
				w2.definition_variant__description_en
		from
				w2
		order
				by w2.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_ldsities(integer, integer, integer[]) is
'Function returns records from table c_ldsity.';

grant execute on function target_data.fn_import_get_ldsities(integer, integer, integer[]) to public;