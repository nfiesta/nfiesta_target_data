--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_ldsity2panel_refyearset_versions
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_ldsity2panel_refyearset_versions(integer, integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_ldsity2panel_refyearset_versions
(
	_id						integer,
	_etl_ldsity__etl_id		integer,
	_refyearset2panel		integer,
	_etl_version__etl_id	integer,
	_etl_id					integer[] default null::integer[]
)
returns table
(
	id					integer,
	etl_id				integer,
	ldsity				integer,
	refyearset2panel	integer,
	version				integer
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_ldsity2panel_refyearset_versions: Input argument _id must not be NULL!';
		end if;
	
		if _etl_ldsity__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_ldsity2panel_refyearset_versions: Input argument _etl_ldsity__etl_id must not be NULL!';
		end if;
	
		if _refyearset2panel is null
		then
			raise exception 'Error 03: fn_import_get_ldsity2panel_refyearset_versions: Input argument _refyearset2panel must not be NULL!';
		end if;

		if _etl_version__etl_id is null
		then
			raise exception 'Error 04: fn_import_get_ldsity2panel_refyearset_versions: Input argument _etl_version__etl_id must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 05: fn_import_get_ldsity2panel_refyearset_versions: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		select
				_id as id,
				cl2prv.id as etl_id,
				cl2prv.ldsity,
				cl2prv.refyearset2panel,
				cl2prv.version
		from
				target_data.cm_ldsity2panel_refyearset_version as cl2prv
		where
				cl2prv.id not in (select unnest(_etl_id))
		and		cl2prv.ldsity = _etl_ldsity__etl_id
		and		cl2prv.refyearset2panel = _refyearset2panel
		and		cl2prv.version = _etl_version__etl_id
		
		order by cl2prv.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_ldsity2panel_refyearset_versions(integer, integer, integer, integer, integer[]) is
'Function returns records from table cm_ldsity2panel_refyearset_version.';

grant execute on function target_data.fn_import_get_ldsity2panel_refyearset_versions(integer, integer, integer, integer, integer[]) to public;