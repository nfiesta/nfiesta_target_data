--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_ldsity_objects
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_ldsity_objects(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_ldsity_objects
(
	_ldsity_object	integer,
	_etl_id			integer[] default null::integer[]
)
returns table
(
	ldsity_object			integer,
	id						integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	table_name				varchar,
	upper_object			integer,
	areal_or_population		integer,
	column4upper_object		varchar,
	filter 					text
)
as
$$
declare
begin
		if _ldsity_object is null
		then
			raise exception 'Error 01: fn_import_get_ldsity_objects: Input argument _ldsity_object must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 02: fn_import_get_ldsity_objects: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		select
				_ldsity_object as ldsity_object,
				clo.id,
				clo.label,
				clo.description,
				clo.label_en,
				clo.description_en,
				clo.table_name,
				clo.upper_object,
				clo.areal_or_population,
				clo.column4upper_object,
				clo.filter
		from 
				target_data.c_ldsity_object as clo
		where
				clo.id not in (select unnest(_etl_id))
		order
				by clo.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_ldsity_objects(integer, integer[]) is
'Function returns records from table c_ldsity_object.';

grant execute on function target_data.fn_import_get_ldsity_objects(integer, integer[]) to public;