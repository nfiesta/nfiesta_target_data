--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_spc2classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_spc2classification_rules(integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_spc2classification_rules
(
	_spc2classification_rule			integer,
	_sub_population_category__etl_id	integer,
	_ldsity_object__etl_id				integer,
	_etl_id								integer[] default null::integer[]
)
returns table
(
	spc2classification_rule					integer,
	ldsity_object__id						integer,
	ldsity_object__label					varchar,
	ldsity_object__description				text,
	ldsity_object__label_en					varchar,
	ldsity_object__description_en			text,
	sub_population_category__id				integer,
	sub_population_category__label			varchar,
	sub_population_category__description	text,
	sub_population_category__label_en		varchar,
	sub_population_category__description_en	text,
	etl_id									integer,
	classification_rule						text
)
as
$$
declare
begin
		if _spc2classification_rule is null
		then
			raise exception 'Error 01: fn_import_get_spc2classification_rules: Input argument _spc2classification_rule must not be NULL!';
		end if;
	
		if _sub_population_category__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_spc2classification_rules: Input argument _sub_population_category__etl_id must not be NULL!';
		end if;
	
		if _ldsity_object__etl_id is null
		then
			raise exception 'Error 03: fn_import_get_spc2classification_rules: Input argument _ldsity_object__etl_id must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 04: fn_import_get_spc2classification_rules: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;	
	
		return query
		with
		w1 as	(
				select
						_spc2classification_rule as spc2classification_rule,
						cscr.id as etl_id,
						cscr.sub_population_category as sub_population_category__id,
						cscr.ldsity_object as ldsity_object__id,
						cscr.classification_rule
				from
						target_data.cm_spc2classification_rule as cscr
				where
						cscr.sub_population_category = _sub_population_category__etl_id
				and
						cscr.ldsity_object = _ldsity_object__etl_id
				and
						cscr.id not in (select unnest(_etl_id))
				)
		select
				w1.spc2classification_rule,
				w1.ldsity_object__id,
				clo.label as ldsity_object__label,
				clo.description as ldsity_object__description,
				clo.label_en as ldsity_object__label_en,
				clo.description_en as ldsity_object__description_en,
				w1.sub_population_category__id,
				cspc.label as sub_population_category__label,
				cspc.description as sub_population_category__description,
				cspc.label_en as sub_population_category__label_en,
				cspc.description_en as sub_population_category__description_en,
				w1.etl_id,
				w1.classification_rule
		from
				w1
				inner join target_data.c_sub_population_category as cspc on w1.sub_population_category__id = cspc.id
				inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
		order 
				by w1.ldsity_object__id, w1.sub_population_category__id, w1.spc2classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_spc2classification_rules(integer, integer, integer, integer[]) is
'Function returns records from table cm_spc2classification_rule.';

grant execute on function target_data.fn_import_get_spc2classification_rules(integer, integer, integer, integer[]) to public;