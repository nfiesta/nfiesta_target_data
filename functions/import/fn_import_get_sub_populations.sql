--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_sub_populations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_sub_populations(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_sub_populations
(
	_sub_population		integer,
	_etl_id				integer[] default null::integer[]
)
returns table
(
	sub_population		integer,
	etl_id				integer,
	label				varchar,
	description			text,
	label_en			varchar,
	description_en		text
)
as
$$
declare
begin
		if _sub_population is null
		then
			raise exception 'Error 01: fn_import_get_sub_populations: Input argument _sub_population must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 02: fn_import_get_sub_populations: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;	
	
		return query
		select
				_sub_population as sub_population,
				cad.id as etl_id,
				cad.label,
				cad.description,
				cad.label_en,
				cad.description_en
		from 
				target_data.c_sub_population as cad
		where
				cad.id not in (select unnest(_etl_id))
		order
				by cad.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_sub_populations(integer, integer[]) is
'Function returns records from table c_sub_population.';

grant execute on function target_data.fn_import_get_sub_populations(integer, integer[]) to public;