--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_unit_of_measures
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_unit_of_measures(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_unit_of_measures
(
	_unit_of_measure	integer,
	_etl_id				integer[] default null::integer[]
)
returns table
(
	unit_of_measure			integer,
	etl_id					integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text
)
as
$$
declare
begin
		if _unit_of_measure is null
		then
			raise exception 'Error 01: fn_import_get_unit_of_measures: Input argument _unit_of_measure must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 02: fn_import_get_unit_of_measures: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;	
	
		return query
		select
				_unit_of_measure as unit_of_measure,
				cuom.id as etl_id,
				cuom.label,
				cuom.description,
				cuom.label_en,
				cuom.description_en
		from 
				target_data.c_unit_of_measure as cuom
		where
				cuom.id not in (select unnest(_etl_id))
		order
				by cuom.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_unit_of_measures(integer, integer[]) is
'Function returns records from table c_unit_of_measure.';

grant execute on function target_data.fn_import_get_unit_of_measures(integer, integer[]) to public;