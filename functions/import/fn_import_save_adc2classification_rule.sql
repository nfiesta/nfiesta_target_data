--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_adc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_adc2classification_rule(integer, integer, integer, text, integer) CASCADE;

create or replace function target_data.fn_import_save_adc2classification_rule
(
	_adc2classification_rule			integer,
	_area_domain_category__etl_id		integer,
	_ldsity_object__etl_id				integer,
	_classification_rule				text,
	_refyearset2panel					integer
)
returns table
(
	adc2classification_rule						integer,
	etl_id										integer,
	cm_adc2classrule2panel_refyearset__etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _adc2classification_rule is null
	then
		raise exception 'Error 01: fn_import_save_adc2classification_rule: Input argument _adc2classification_rule must not be NULL!';
	end if;

	if _area_domain_category__etl_id is null
	then
		raise exception 'Error 02: fn_import_save_adc2classification_rule: Input argument _area_domain_category__etl_id must not be NULL!';
	end if;

	if _ldsity_object__etl_id is null
	then
		raise exception 'Error 03: fn_import_save_adc2classification_rule: Input argument _ldsity_object__etl_id must not be NULL!';
	end if;

	if _classification_rule is null
	then
		raise exception 'Error 04: fn_import_save_adc2classification_rule: Input argument _classification_rule must not be NULL!';
	end if;

	return query
	with
	w1 as	(
			select
					_area_domain_category__etl_id as adc__etl_id,
					_ldsity_object__etl_id as lo__etl_id,
					_classification_rule as cr
			)
	,w2 as	(
			insert into target_data.cm_adc2classification_rule
			(area_domain_category, ldsity_object, classification_rule)
			select
					w1.adc__etl_id,
					w1.lo__etl_id,
					w1.cr
			from
					w1
			returning cm_adc2classification_rule.id
			)
	insert into target_data.cm_adc2classrule2panel_refyearset(adc2classification_rule, refyearset2panel)
	select w2.id, _refyearset2panel from w2
	returning _adc2classification_rule, cm_adc2classrule2panel_refyearset.adc2classification_rule, cm_adc2classrule2panel_refyearset.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_adc2classification_rule(integer, integer, integer, text, integer) is
'Function inserts a record into table cm_adc2classification_rule based on given parameters.';

grant execute on function target_data.fn_import_save_adc2classification_rule(integer, integer, integer, text, integer) to public;