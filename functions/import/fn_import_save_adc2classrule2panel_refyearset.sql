--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_adc2classrule2panel_refyearset
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_adc2classrule2panel_refyearset(integer, integer, integer) CASCADE;

create or replace function target_data.fn_import_save_adc2classrule2panel_refyearset
(
	_id							integer,
	_adc2classification_rule	integer,
	_refyearset2panel			integer
)
returns table
(
	id			integer,
	etl_id		integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_import_save_adc2classrule2panel_refyearset: Input argument _id must not be NULL!';
	end if;

	if _adc2classification_rule is null
	then
		raise exception 'Error 02: fn_import_save_adc2classrule2panel_refyearset: Input argument _adc2classification_rule must not be NULL!';
	end if;

	if _refyearset2panel is null
	then
		raise exception 'Error 03: fn_import_save_adc2classrule2panel_refyearset: Input argument _refyearset2panel must not be NULL!';
	end if;

	insert into target_data.cm_adc2classrule2panel_refyearset(adc2classification_rule, refyearset2panel)
	select _adc2classification_rule, _refyearset2panel
	returning cm_adc2classrule2panel_refyearset.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_adc2classrule2panel_refyearset(integer, integer, integer) is
'Function inserts a record into table cm_adc2classrule2panel_refyearset based on given parameters.';

grant execute on function target_data.fn_import_save_adc2classrule2panel_refyearset(integer, integer, integer) to public;