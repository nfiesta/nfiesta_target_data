--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_area_domain(integer, varchar, text, varchar, text) CASCADE;

create or replace function target_data.fn_import_save_area_domain
(
	_area_domain			integer,
	_label					varchar,
	_description			text,
	_label_en				varchar,
	_description_en			text
)
returns table
(
	area_domain			integer,
	etl_id				integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _area_domain is null
	then
		raise exception 'Error 01: fn_import_save_area_domain: Input argument _area_domain must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_import_save_area_domain: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_import_save_area_domain: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_import_save_area_domain: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_import_save_area_domain: Input argument _description_en must not be NULL!';
	end if;

	insert into target_data.c_area_domain(label, description, label_en, description_en)
	select _label, _description, _label_en, _description_en
	returning c_area_domain.id
	into _etl_id;

	return query select _area_domain, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_area_domain(integer, varchar, text, varchar, text) is
'Function inserts a record into table c_area_domain based on given parameters.';

grant execute on function target_data.fn_import_save_area_domain(integer, varchar, text, varchar, text) to public;