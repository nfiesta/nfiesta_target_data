--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_ldsity(integer,varchar,text,varchar,text,integer,text,integer,integer[],integer[],integer[]) CASCADE;

create or replace function target_data.fn_import_save_ldsity
(
	_ldsity						integer,
	_label						varchar,
	_description				text,
	_label_en					varchar,
	_description_en				text,
	_ldsity_object				integer,
	_column_expression			text,
	_unit_of_measure			integer,
	_area_domain_category		integer[],
	_sub_population_category	integer[],
	_definition_variant			integer[]
)
returns integer
as
$$
declare
		_etl_id		integer;
begin
	if _ldsity is null
	then
		raise exception 'Error 01: fn_import_save_ldsity: Input argument _ldsity must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_import_save_ldsity: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_import_save_ldsity: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_import_save_ldsity: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_import_save_ldsity: Input argument _description_en must not be NULL!';
	end if;

	if _ldsity_object is null
	then
		raise exception 'Error 06: fn_import_save_ldsity: Input argument _ldsity_object must not be NULL!';
	end if;

	if _unit_of_measure is null
	then
		raise exception 'Error 07: fn_import_save_ldsity: Input argument _unit_of_measure must not be NULL!';
	end if;

	insert into target_data.c_ldsity
	(
		label, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category,
		definition_variant, label_en, description, description_en
	)
	select
		_label, _ldsity_object, _column_expression, _unit_of_measure, _area_domain_category, _sub_population_category,
		_definition_variant, _label_en, _description, _description_en
	returning
			c_ldsity.id
	into
			_etl_id;

	return _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_ldsity(integer,varchar,text,varchar,text,integer,text,integer,integer[],integer[],integer[]) is
'Function inserts a record into table c_ldsity based on given parameters.';

grant execute on function target_data.fn_import_save_ldsity(integer,varchar,text,varchar,text,integer,text,integer,integer[],integer[],integer[]) to public;