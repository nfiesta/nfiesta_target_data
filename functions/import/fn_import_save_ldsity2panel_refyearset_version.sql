--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_ldsity2panel_refyearset_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) CASCADE;

create or replace function target_data.fn_import_save_ldsity2panel_refyearset_version
(
	_id					integer,
	_ldsity				integer,
	_refyearset2panel	integer,
	_version			integer
)
returns table
(
	id			integer,
	etl_id		integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_import_save_ldsity2panel_refyearset_version: Input argument _id must not be NULL!';
	end if;

	if _ldsity is null
	then
		raise exception 'Error 02: fn_import_save_ldsity2panel_refyearset_version: Input argument _ldsity must not be NULL!';
	end if;

	if _refyearset2panel is null
	then
		raise exception 'Error 03: fn_import_save_ldsity2panel_refyearset_version: Input argument _refyearset2panel must not be NULL!';
	end if;

	if _version is null
	then
		raise exception 'Error 04: fn_import_save_ldsity2panel_refyearset_version: Input argument _version must not be NULL!';
	end if;

	insert into target_data.cm_ldsity2panel_refyearset_version(ldsity, refyearset2panel, version)
	select _ldsity, _refyearset2panel, _version
	returning cm_ldsity2panel_refyearset_version.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) is
'Function inserts a record into table cm_ldsity2panel_refyearset_version based on given parameters.';

grant execute on function target_data.fn_import_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) to public;