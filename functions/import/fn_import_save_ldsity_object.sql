--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_ldsity_object(integer, varchar, text, varchar, text, varchar, integer, integer, varchar, text) CASCADE;

create or replace function target_data.fn_import_save_ldsity_object
(
	_ldsity_object			integer,
	_label					varchar,
	_description			text,
	_label_en				varchar,
	_description_en			text,
	_table_name				varchar,
	_upper_object			integer,
	_areal_or_population	integer,
	_column4upper_object	varchar,
	_filter					text
)
returns integer
as
$$
declare
		_etl_id		integer;
begin
	if _ldsity_object is null
	then
		raise exception 'Error 01: fn_import_save_ldsity_object: Input argument _ldsity_object must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_import_save_ldsity_object: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_import_save_ldsity_object: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_import_save_ldsity_object: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_import_save_ldsity_object: Input argument _description_en must not be NULL!';
	end if;

	if _table_name is null
	then
		raise exception 'Error 06: fn_import_save_ldsity_object: Input argument _table_name must not be NULL!';
	end if;

	if _areal_or_population is null
	then
		raise exception 'Error 07: fn_import_save_ldsity_object: Input argument _areal_or_population must not be NULL!';
	end if;

	insert into target_data.c_ldsity_object
	(
		label, description, table_name, upper_object, areal_or_population, column4upper_object, label_en, description_en, filter
	)
	select
			_label, _description, _table_name, _upper_object, _areal_or_population, _column4upper_object, _label_en, _description_en, _filter
	returning
			c_ldsity_object.id
	into
			_etl_id;

	return _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_ldsity_object(integer, varchar, text, varchar, text, varchar, integer, integer, varchar, text) is
'Function inserts a record into table c_ldsity_object based on given parameters.';

grant execute on function target_data.fn_import_save_ldsity_object(integer, varchar, text, varchar, text, varchar, integer, integer, varchar, text) to public;