--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_spc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_spc2classification_rule(integer, integer, integer, text, integer) CASCADE;

create or replace function target_data.fn_import_save_spc2classification_rule
(
	_spc2classification_rule			integer,
	_sub_population_category__etl_id	integer,
	_ldsity_object__etl_id				integer,
	_classification_rule				text,
	_refyearset2panel					integer
)
returns table
(
	spc2classification_rule						integer,
	etl_id										integer,
	cm_spc2classrule2panel_refyearset__etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _spc2classification_rule is null
	then
		raise exception 'Error 01: fn_import_save_spc2classification_rule: Input argument _spc2classification_rule must not be NULL!';
	end if;

	if _sub_population_category__etl_id is null
	then
		raise exception 'Error 02: fn_import_save_spc2classification_rule: Input argument _sub_population_category__etl_id must not be NULL!';
	end if;

	if _ldsity_object__etl_id is null
	then
		raise exception 'Error 03: fn_import_save_spc2classification_rule: Input argument _ldsity_object__etl_id must not be NULL!';
	end if;

	if _classification_rule is null
	then
		raise exception 'Error 04: fn_import_save_spc2classification_rule: Input argument _classification_rule must not be NULL!';
	end if;

	return query
	with
	w1 as	(
			select
					_sub_population_category__etl_id as spc__etl_id,
					_ldsity_object__etl_id as lo__etl_id,
					_classification_rule as cr
			)
	,w2 as	(
			insert into target_data.cm_spc2classification_rule
			(sub_population_category, ldsity_object, classification_rule)
			select
					w1.spc__etl_id,
					w1.lo__etl_id,
					w1.cr
			from
					w1
			returning cm_spc2classification_rule.id
			)
	insert into target_data.cm_spc2classrule2panel_refyearset(spc2classification_rule, refyearset2panel)
	select w2.id, _refyearset2panel from w2
	returning _spc2classification_rule, cm_spc2classrule2panel_refyearset.spc2classification_rule, cm_spc2classrule2panel_refyearset.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_spc2classification_rule(integer, integer, integer, text, integer) is
'Function inserts a record into table cm_spc2classification_rule based on given parameters.';

grant execute on function target_data.fn_import_save_spc2classification_rule(integer, integer, integer, text, integer) to public;