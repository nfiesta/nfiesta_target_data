--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_spc_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_spc_hierarchy(integer, integer, integer, boolean) CASCADE;

create or replace function target_data.fn_import_save_spc_hierarchy
(
	_id						integer,
	_variable_superior		integer,
	_variable				integer,
	_dependent				boolean
)
returns table
(
	id			integer,
	etl_id		integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_import_save_spc_hierarchy: Input argument _id must not be NULL!';
	end if;

	if _variable_superior is null
	then
		raise exception 'Error 02: fn_import_save_spc_hierarchy: Input argument _variable_superior must not be NULL!';
	end if;

	if _variable is null
	then
		raise exception 'Error 03: fn_import_save_spc_hierarchy: Input argument _variable must not be NULL!';
	end if;

	if _dependent is null
	then
		raise exception 'Error 04: fn_import_save_spc_hierarchy: Input argument _dependent must not be NULL!';
	end if;

	insert into target_data.t_spc_hierarchy(variable_superior, variable, dependent)
	select _variable_superior, _variable, _dependent
	returning t_spc_hierarchy.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_spc_hierarchy(integer, integer, integer, boolean) is
'Function inserts a record into table t_spc_hierarchy based on given parameters.';

grant execute on function target_data.fn_import_save_spc_hierarchy(integer, integer, integer, boolean) to public;