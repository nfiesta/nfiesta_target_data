--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-----------------------------------------------
-- DDL
-----------------------------------------------
ALTER TABLE target_data.c_indicator ADD COLUMN label_en character varying(200);
ALTER TABLE target_data.c_indicator ADD COLUMN description_en text;

COMMENT ON COLUMN target_data.c_indicator.label_en IS 'English label of the indicator.';
COMMENT ON COLUMN target_data.c_indicator.description_en IS 'English description of the indicator.';

-- lookup for panel and refyearset groups
CREATE TABLE target_data.c_panel_refyearset_group (
id		serial		NOT NULL,
label		varchar(200) 	NOT NULL,
description	text		NOT NULL,
label_en	varchar(200),
description_en	text,
CONSTRAINT pkey__c_panel_refyearset_group__id PRIMARY KEY (id)
);

GRANT SELECT ON TABLE target_data.c_panel_refyearset_group TO PUBLIC;
GRANT SELECT ON SEQUENCE target_data.c_panel_refyearset_group_id_seq TO PUBLIC;

COMMENT ON TABLE target_data.c_panel_refyearset_group IS 'Lookup table with named aggregated sets of panels and corresponding reference year sets.';
COMMENT ON COLUMN target_data.c_panel_refyearset_group.id IS 'Identificator of the category.';
COMMENT ON COLUMN target_data.c_panel_refyearset_group.label IS 'Label of the category.';
COMMENT ON COLUMN target_data.c_panel_refyearset_group.description IS 'Detailed description of the category.';
COMMENT ON COLUMN target_data.c_panel_refyearset_group.label_en IS 'English label of the category.';
COMMENT ON COLUMN target_data.c_panel_refyearset_group.description_en IS 'Detailed English description of the category.';

-- addition of reference in c_ldsity
ALTER TABLE target_data.c_ldsity ADD COLUMN panel_refyearset_group integer;

ALTER TABLE target_data.c_ldsity ADD CONSTRAINT
fkey__c_ldsity__c_panel_refyearset_group FOREIGN KEY (panel_refyearset_group)
REFERENCES target_data.c_panel_refyearset_group (id);
COMMENT ON CONSTRAINT fkey__c_ldsity__c_panel_refyearset_group ON target_data.c_ldsity IS
'Foreign key to table c_panel_refyearset_group.';

CREATE INDEX fki__c_ldsity__c_panel_refyearset_group ON target_data.c_ldsity USING btree (panel_refyearset_group);
COMMENT ON INDEX target_data.fki__c_ldsity__c_panel_refyearset_group IS
'BTree index on foreign key fkey__c_ldsity__c_panel_refyearset_group.';

COMMENT ON COLUMN target_data.c_ldsity.panel_refyearset_group IS 'Group of panels and appropriate reference year sets which represents the local density availability. Foreign key to table c_panel_refyearset_group.';

-- t_panel_refyearset_group
CREATE TABLE target_data.t_panel_refyearset_group (
id			serial		NOT NULL,
panel_refyearset_group	integer		NOT NULL,
refyearset2panel	integer		NOT NULL,
CONSTRAINT pkey__t_panel_refyearset_group PRIMARY KEY (id)
);

ALTER TABLE target_data.t_panel_refyearset_group ADD CONSTRAINT
fkey__t_panel_refyearset_group__c_panel_refyearset_group FOREIGN KEY (panel_refyearset_group)
REFERENCES target_data.c_panel_refyearset_group (id);

ALTER TABLE target_data.t_panel_refyearset_group ADD CONSTRAINT
fkey__t_panel_refyearset_group__cm_refyearset2panel_mapping FOREIGN KEY (refyearset2panel)
REFERENCES sdesign.cm_refyearset2panel_mapping (id);

CREATE INDEX fki__t_panel_refyearset_group__c_panel_refyearset_group ON target_data.t_panel_refyearset_group USING btree (panel_refyearset_group);
COMMENT ON INDEX target_data.fki__t_panel_refyearset_group__c_panel_refyearset_group IS
'BTree index on foreign key fkey__t_panel_refyearset_group__c_panel_refyearset_group.';

CREATE INDEX fki__t_panel_refyearset_group__cm_refyearset2panel_mapping ON target_data.t_panel_refyearset_group USING btree (refyearset2panel);
COMMENT ON INDEX target_data.fki__t_panel_refyearset_group__cm_refyearset2panel_mapping IS
'BTree index on foreign key fkey__t_panel_refyearset_group__cm_refyearset2panel_mapping.';

GRANT SELECT ON TABLE target_data.t_panel_refyearset_group TO PUBLIC;
GRANT SELECT ON SEQUENCE target_data.t_panel_refyearset_group_id_seq TO PUBLIC;

COMMENT ON TABLE target_data.t_panel_refyearset_group IS 'Concrete group of panels and corresponding reference year sets under the labeled lookup item.';
COMMENT ON COLUMN target_data.t_panel_refyearset_group.panel_refyearset_group IS 'Identificator of the group, foreign key to table c_panel_refyearset_group.';
COMMENT ON COLUMN target_data.t_panel_refyearset_group.refyearset2panel IS 'Identificator of the combination of panel and reference year set, foregin key to table cm_refyearset2panel_mapping.';

SELECT pg_catalog.pg_extension_config_dump('target_data.c_panel_refyearset_group', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_panel_refyearset_group_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_panel_refyearset_group', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_panel_refyearset_group_id_seq', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_definition_variant','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_definition_variant_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_indicator','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_indicator_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_ldsity','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_ldsity_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_ldsity_object','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_ldsity_object_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_sub_population','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_sub_population_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_sub_population_category','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_sub_population_category_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_target_variable','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_target_variable_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_unit_of_measure','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_unit_of_measure_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.cm_adc2classification_rule','');
SELECT pg_catalog.pg_extension_config_dump('target_data.cm_adc2classification_rule_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.cm_ldsity2target_variable','');
SELECT pg_catalog.pg_extension_config_dump('target_data.cm_ldsity2target_variable_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.cm_spc2classification_rule','');
SELECT pg_catalog.pg_extension_config_dump('target_data.cm_spc2classification_rule_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_adc_hierarchy','');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_adc_hierarchy_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_available_datasets','');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_available_datasets_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_ldsity_values','');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_ldsity_values_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_spc_hierarchy','');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_spc_hierarchy_id_seq','');

GRANT SELECT ON TABLE				 target_data.c_areal_or_population TO public;
GRANT SELECT ON TABLE				 target_data.c_ldsity_object_type TO public;
GRANT SELECT ON TABLE				 target_data.c_state_or_change TO public;

GRANT SELECT ON TABLE				 target_data.c_area_domain TO public;
GRANT SELECT ON SEQUENCE			 target_data.c_area_domain_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.c_area_domain_category TO public;
GRANT SELECT ON SEQUENCE			 target_data.c_area_domain_category_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.c_definition_variant TO public;
GRANT SELECT ON SEQUENCE			 target_data.c_definition_variant_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.c_indicator TO public;
GRANT SELECT ON SEQUENCE			 target_data.c_indicator_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.c_ldsity TO public;
GRANT SELECT ON SEQUENCE			 target_data.c_ldsity_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.c_ldsity_object TO public;
GRANT SELECT ON SEQUENCE			 target_data.c_ldsity_object_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.c_sub_population TO public;
GRANT SELECT ON SEQUENCE			 target_data.c_sub_population_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.c_sub_population_category TO public;
GRANT SELECT ON SEQUENCE			 target_data.c_sub_population_category_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.c_target_variable TO public;
GRANT SELECT ON SEQUENCE			 target_data.c_target_variable_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.c_unit_of_measure TO public;
GRANT SELECT ON SEQUENCE			 target_data.c_unit_of_measure_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.cm_adc2classification_rule TO public;
GRANT SELECT ON SEQUENCE			 target_data.cm_adc2classification_rule_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.cm_ldsity2target_variable TO public;
GRANT SELECT ON SEQUENCE			 target_data.cm_ldsity2target_variable_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.cm_spc2classification_rule TO public;
GRANT SELECT ON SEQUENCE			 target_data.cm_spc2classification_rule_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.t_adc_hierarchy TO public;
GRANT SELECT ON SEQUENCE			 target_data.t_adc_hierarchy_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.t_available_datasets TO public;
GRANT SELECT ON SEQUENCE			 target_data.t_available_datasets_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.t_ldsity_values TO public;
GRANT SELECT ON SEQUENCE			 target_data.t_ldsity_values_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.t_spc_hierarchy TO public;
GRANT SELECT ON SEQUENCE			 target_data.t_spc_hierarchy_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.c_panel_refyearset_group TO public;
GRANT SELECT ON SEQUENCE			 target_data.c_panel_refyearset_group_id_seq TO public;
GRANT SELECT ON TABLE				 target_data.t_panel_refyearset_group TO public;
GRANT SELECT ON SEQUENCE			 target_data.t_panel_refyearset_group_id_seq TO public;

GRANT SELECT ON TABLE				 target_data.c_areal_or_population TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.c_ldsity_object_type TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.c_state_or_change TO app_nfiesta;

GRANT SELECT ON TABLE				 target_data.c_target_variable TO app_nfiesta;
GRANT SELECT ON SEQUENCE			 target_data.c_target_variable_id_seq TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.c_unit_of_measure TO app_nfiesta;
GRANT SELECT ON SEQUENCE			 target_data.c_unit_of_measure_id_seq TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.c_indicator TO app_nfiesta;
GRANT SELECT ON SEQUENCE			 target_data.c_indicator_id_seq TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.c_ldsity TO app_nfiesta;
GRANT SELECT ON SEQUENCE			 target_data.c_ldsity_id_seq TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.c_ldsity_object TO app_nfiesta;
GRANT SELECT ON SEQUENCE			 target_data.c_ldsity_object_id_seq TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.cm_ldsity2target_variable TO app_nfiesta;
GRANT SELECT ON SEQUENCE			 target_data.cm_ldsity2target_variable_id_seq TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.c_panel_refyearset_group TO app_nfiesta;
GRANT SELECT ON SEQUENCE			 target_data.c_panel_refyearset_group_id_seq TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.t_panel_refyearset_group TO app_nfiesta;
GRANT SELECT ON SEQUENCE			 target_data.t_panel_refyearset_group_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_area_domain TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_area_domain_id_seq TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_area_domain_category TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_area_domain_category_id_seq TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_definition_variant TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_definition_variant_id_seq TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_sub_population TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_sub_population_id_seq TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_sub_population_category TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_sub_population_category_id_seq TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.cm_adc2classification_rule TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.cm_adc2classification_rule_id_seq TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.cm_spc2classification_rule TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.cm_spc2classification_rule_id_seq TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.t_adc_hierarchy TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.t_adc_hierarchy_id_seq TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.t_available_datasets TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.t_available_datasets_id_seq TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.t_ldsity_values TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.t_ldsity_values_id_seq TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.t_spc_hierarchy TO app_nfiesta;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.t_spc_hierarchy_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_unit_of_measure TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_unit_of_measure_id_seq TO app_nfiesta_mng;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_ldsity TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_ldsity_id_seq TO app_nfiesta_mng;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_ldsity_object TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_ldsity_object_id_seq TO app_nfiesta_mng;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_indicator TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_indicator_id_seq TO app_nfiesta_mng;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_target_variable TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_target_variable_id_seq TO app_nfiesta_mng;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.cm_ldsity2target_variable TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.cm_ldsity2target_variable_id_seq TO app_nfiesta_mng;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_panel_refyearset_group TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_panel_refyearset_group_id_seq TO app_nfiesta_mng;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.t_panel_refyearset_group TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.t_panel_refyearset_group_id_seq TO app_nfiesta_mng;

-----------------------------------------------
-- Functions
-----------------------------------------------
-- <function name="fn_save_indicator" schema="target_data" src="functions/fn_save_indicator.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_indicator
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_indicator(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_indicator(_state_or_change integer, _label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text)
RETURNS integer
AS
$$
DECLARE
	_indicator integer;
BEGIN
	IF _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory parameter label (%) or description (%) is NULL!', _label, _description;
	END IF;

	INSERT INTO target_data.c_indicator(state_or_change, label, description, label_en, description_en)
	SELECT _state_or_change, _label, _description, _label_en, _description_en
	RETURNING id
	INTO _indicator;

	RETURN _indicator;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_indicator(integer, character varying, text, character varying, text) IS
'Functions provides insert into c_indicator table.';

-- </function>

-- <function name="fn_save_target_variable" schema="target_data" src="functions/fn_save_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_target_variable(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_target_variable(_indicator integer, _ldsity integer[], _ldsity_object_type integer[])
RETURNS integer
AS
$$
DECLARE
	_target_variable integer;
BEGIN
	IF _indicator IS NULL OR _ldsity IS NULL
	THEN
		RAISE EXCEPTION 'Some of input parameters are NULL (indicator=%,ldsity=%)!',_indicator,_ldsity;
	END IF;

	IF array_length(_ldsity,1) != array_length(_ldsity_object_type,1)
	THEN
		RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity, _ldsity_object_type;
	END IF;

	INSERT INTO target_data.c_target_variable(indicator)
	SELECT _indicator
	RETURNING id
	INTO _target_variable;

	INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category)
	SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[]
	FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
	INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
	ON t1.id = t2.id;

	RETURN _target_variable;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_target_variable(integer, integer[], integer[]) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.';

-- </function>

-- <function name=" fn_save_areal_or_pop" schema="target_data" src="functions/fn_save_areal_or_pop.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_areal_or_pop
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_areal_or_pop(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_areal_or_pop(_areal_or_population integer, _label varchar(200), _description text, _label_en varchar(200) DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text)
RETURNS integer
AS
$$
DECLARE
	_id integer;
BEGIN
	IF _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of label or description is null (%, %).', _label, _description;
	END IF; 

	CASE 
	WHEN _areal_or_population = 1 THEN
		INSERT INTO target_data.c_area_domain(label, description, label_en, description_en)
		SELECT _label, _description, _label_en, _description_en
		RETURNING id
		INTO _id;

	WHEN _areal_or_population = 2 THEN
		INSERT INTO target_data.c_sub_population(label, description, label_en, description_en)
		SELECT _label, _description, _label_en, _description_en
		RETURNING id
		INTO _id;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_areal_or_pop(integer, character varying, text, character varying, text) IS
'Functions inserts a record into table c_area_domain or c_sub_population based on given parameters.';

-- </function>

-- <function name="fn_save_categories" schema="target_data" src="functions/fn_save_categories.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categories
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_categories(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_categories(_parent integer, _areal_or_population integer, _label varchar(200)[], _description text[], _label_en varchar(200)[], _description_en text[])
RETURNS void
AS
$$
BEGIN
	IF _parent IS NULL OR _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent indicator (%) or label/descriptioin is null (%, %)!', _parent, _label, _description;
	END IF; 

	IF array_length(_label,1) != array_length(_description,1) OR
		array_length(_label_en,1) != array_length(_description_en,1)
	THEN
		RAISE EXCEPTION 'Given arrays of label and description (%,%) or label_en and description_en (%,%) must be of same length!', _label, _description, _label_en, _description_en;
	END IF;


	CASE 
	WHEN _areal_or_population = 1 THEN
		INSERT INTO target_data.c_area_domain_category(area_domain, label, description, label_en, description_en)
		SELECT 
			_parent,
			t1.label, t2.description, t3.label_en, t4.description_en
		FROM
			unnest(_label) WITH ORDINALITY AS t1(label, id)
		INNER JOIN
			unnest(_description) WITH ORDINALITY AS t2(description, id)
		ON t1.id = t2.id
		LEFT JOIN unnest(_label_en) WITH ORDINALITY AS t3(label_en, id)
		ON t1.id = t3.id
		LEFT JOIN unnest(_description_en) WITH ORDINALITY AS t4(description_en, id)
		ON t1.id  = t4.id;

	WHEN _areal_or_population = 2 THEN

		INSERT INTO target_data.c_sub_population_category(sub_population, label, description, label_en, description_en)
		SELECT 
			_parent,
			t1.label, t2.description, t3.label_en, t4.description_en
		FROM
			unnest(_label) WITH ORDINALITY AS t1(label, id)
		INNER JOIN
			unnest(_description) WITH ORDINALITY AS t2(description, id)
		ON t1.id = t2.id
		LEFT JOIN unnest(_label_en) WITH ORDINALITY AS t3(label_en, id)
		ON t1.id = t3.id
		LEFT JOIN unnest(_description_en) WITH ORDINALITY AS t4(description_en, id)
		ON t1.id  = t4.id;

	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;

	RETURN;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_categories(integer, integer, varchar(200)[], text[], varchar(200)[], text[]) IS
'Functions inserts a record into table c_area_domain_category or c_sub_population_category based on given parameters.';

-- </function>

-- <function name="fn_save_classification_rule" schema="target_data" src="functions/fn_save_classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_classification_rule.sql
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_classification_rule.sql(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_classification_rule(_parent integer, _areal_or_population_cat integer, _ldsity_object integer, _classification_rule text)
RETURNS integer
AS
$$
DECLARE
	_id integer;
BEGIN
	IF _parent IS NULL OR _ldsity_object IS NULL OR _classification_rule IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent indicator (%) or ldsity object (%) or classification rule (%) is null!', _parent, _ldsity_object, _classification_rule;
	END IF; 

	CASE 
	WHEN _areal_or_population = 1 THEN
		INSERT INTO target_data.cm_adc2conversion_string(area_domain_category, ldsity_object, conversion_string)
		SELECT 
			_parent,
			_ldsity_object, _classification_rule
		RETURNING id
		INTO _id;

	WHEN _areal_or_population = 2 THEN

		INSERT INTO target_data.cm_spc2conversion_string(sub_population_category, ldsity_object, conversion_string)
		SELECT 
			_parent,
			_ldsity_object, _classification_rule
		RETURNING id
		INTO _id;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_classification_rule(integer, integer, integer, text) IS
'Functions inserts a record into table c_adc2conversion_string or c_spc2conversion_string based on given parameters.';

-- </function>
