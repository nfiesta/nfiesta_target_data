--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- swapping the variable and variable_superior columns in the table t_adc_hieararchy
---------------------------------------------------------------------------------------------------
ALTER TABLE target_data.t_adc_hierarchy DROP CONSTRAINT fkey__t_adc_hierarchy__c_area_domain_category_1;
ALTER TABLE target_data.t_adc_hierarchy DROP CONSTRAINT fkey__t_adc_hierarchy__c_area_domain_category_2;
DROP INDEX target_data.fki__t_adc_hierarchy__c_area_domain_category_1;
DROP INDEX target_data.fki__t_adc_hierarchy__c_area_domain_category_2;

ALTER TABLE target_data.t_adc_hierarchy RENAME COLUMN variable_superior TO variable_superior_old;
ALTER TABLE target_data.t_adc_hierarchy RENAME COLUMN variable TO variable_superior;
ALTER TABLE target_data.t_adc_hierarchy RENAME COLUMN variable_superior_old TO variable;

ALTER TABLE target_data.t_adc_hierarchy
  ADD CONSTRAINT fkey__t_adc_hierarchy__c_area_domain_category_1 FOREIGN KEY (variable_superior)
      REFERENCES target_data.c_area_domain_category (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_adc_hierarchy__c_area_domain_category_1 ON target_data.t_adc_hierarchy IS
'Foreign key to table c_area_domain_category.';

ALTER TABLE target_data.t_adc_hierarchy
  ADD CONSTRAINT fkey__t_adc_hierarchy__c_area_domain_category_2 FOREIGN KEY (variable)
      REFERENCES target_data.c_area_domain_category (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_adc_hierarchy__c_area_domain_category_2 ON target_data.t_adc_hierarchy IS
'Foreign key to table c_area_domain_category.';

CREATE INDEX fki__t_adc_hierarchy__c_area_domain_category_1 ON target_data.t_adc_hierarchy USING btree (variable_superior);
COMMENT ON INDEX target_data.fki__t_adc_hierarchy__c_area_domain_category_1 IS
'BTree index on foreign key fkey__t_adc_hierarchy__c_area_domain_category_1.';

CREATE INDEX fki__t_adc_hierarchy__c_area_domain_category_2 ON target_data.t_adc_hierarchy USING btree (variable);
COMMENT ON INDEX target_data.fki__t_adc_hierarchy__c_area_domain_category_2 IS
'BTree index on foreign key fkey__t_adc_hierarchy__c_area_domain_category_2.';

COMMENT ON COLUMN target_data.t_adc_hierarchy.variable_superior IS '.';
COMMENT ON COLUMN target_data.t_adc_hierarchy.variable IS '.';
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- swapping the variable and variable_superior columns in the table t_spc_hieararchy
---------------------------------------------------------------------------------------------------
ALTER TABLE target_data.t_spc_hierarchy DROP CONSTRAINT fkey__t_spc_hierarchy__c_sub_population_category_1;
ALTER TABLE target_data.t_spc_hierarchy DROP CONSTRAINT fkey__t_spc_hierarchy__c_sub_population_category_2;
DROP INDEX target_data.fki__t_spc_hierarchy__c_sub_population_category_1;
DROP INDEX target_data.fki__t_spc_hierarchy__c_sub_population_category_2;

ALTER TABLE target_data.t_spc_hierarchy RENAME COLUMN variable_superior TO variable_superior_old;
ALTER TABLE target_data.t_spc_hierarchy RENAME COLUMN variable TO variable_superior;
ALTER TABLE target_data.t_spc_hierarchy RENAME COLUMN variable_superior_old TO variable;

ALTER TABLE target_data.t_spc_hierarchy
  ADD CONSTRAINT fkey__t_spc_hierarchy__c_sub_population_category_1 FOREIGN KEY (variable_superior)
      REFERENCES target_data.c_sub_population_category (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_spc_hierarchy__c_sub_population_category_1 ON target_data.t_spc_hierarchy IS
'Foreign key to table c_sub_population_category.';

ALTER TABLE target_data.t_spc_hierarchy
  ADD CONSTRAINT fkey__t_spc_hierarchy__c_sub_population_category_2 FOREIGN KEY (variable)
      REFERENCES target_data.c_sub_population_category (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_spc_hierarchy__c_sub_population_category_2 ON target_data.t_spc_hierarchy IS
'Foreign key to table c_sub_pupulation_category.';

CREATE INDEX fki__t_spc_hierarchy__c_sub_population_category_1 ON target_data.t_spc_hierarchy USING btree (variable_superior);
COMMENT ON INDEX target_data.fki__t_spc_hierarchy__c_sub_population_category_1 IS
'BTree index on foreign key fkey__t_spc_hierarchy__c_sub_population_category_1.';

CREATE INDEX fki__t_spc_hierarchy__c_sub_population_category_2 ON target_data.t_spc_hierarchy USING btree (variable);
COMMENT ON INDEX target_data.fki__t_spc_hierarchy__c_sub_population_category_2 IS
'BTree index on foreign key fkey__t_spc_hierarchy__c_sub_population_category_2.';

COMMENT ON COLUMN target_data.t_spc_hierarchy.variable_superior IS '.';
COMMENT ON COLUMN target_data.t_spc_hierarchy.variable IS '.';
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- <function name="fn_get_attribute_domain" schema="target_data" src="functions/fn_get_attribute_domain.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_attribute_domain(varchar(2), integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_attribute_domain
(
	in _attr_type varchar(2), -- 'ad' or 'sp'
    IN _attr_types integer[],
    IN _attribute_domain integer[] DEFAULT NULL::integer[]
)
  RETURNS TABLE(attr_1 integer[], attr_2 integer[], attr_3 integer[], attr_4 integer[], attr_5 integer[]) AS
$BODY$
	DECLARE
		_at_count		integer;
		_attr_1			integer[];
		_attr_2			integer[];
		_attr_3			integer[];
		_attr_4			integer[];
		_attr_5			integer[];
	
		_table_name					text;
		_table_name_category		text;
		_column_name				text;
		_table_name_hierarchy		text;
	BEGIN

		if _attr_type is null
		then
			raise exception 'Error 01: fn_get_attribute_domain: The input argument _attr_type must not be NULL!';
		end if;
	
		if not(_attr_type = any(array['ad','sp']))
		then
			raise exception 'Error 02: fn_get_attribute_domain: Allowed values for the input argument _attr_type are "ad" or "sp" !';
		end if;
	
		if _attr_type = 'ad'
		then
			_table_name := 'c_area_domain';
			_table_name_category := 'c_area_domain_category';
			_column_name := 'area_domain';
			_table_name_hierarchy := 't_adc_hierarchy';
		end if;
	
		if _attr_type = 'sp'
		then
			_table_name := 'c_sub_population';
			_table_name_category := 'c_sub_population_category';
			_column_name := 'sub_population';
			_table_name_hierarchy := 't_spc_hierarchy';
		end if;

	CASE	WHEN _attr_types IS NULL THEN

			RAISE EXCEPTION 'Error 03: fn_get_attribute_domain: The function cannot be called without selling at least one attribute type!';

		WHEN array_length(_attr_types,1) IS DISTINCT FROM array_length(array_remove(_attr_types,NULL),1) THEN

			RAISE EXCEPTION 'Error 04: fn_get_attribute_domain: At least one of the passed attribute types is NULL (attr_types = %)!', _attr_types;
	ELSE
		_at_count := array_length(_attr_types,1);
	END CASE;


-----------------------------------------------------------------------------------------------------------------------
-- 1 attribute
	CASE
		WHEN _at_count = 1 THEN

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0 AS attr1
			)
			SELECT
				array_agg(attr1 ORDER BY attr1),
				NULL::integer[],
				NULL::integer[],
				NULL::integer[],
				NULL::integer[]
			FROM
				w_attr1
			WHERE
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1;

----------------------------------------------------------------------------------------------------------------------
-- 2 attributes
		WHEN _at_count = 2 THEN 

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0, $1[1]
			),
			w_attr2 AS
			(
				SELECT id AS attr2, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[2]
				UNION SELECT 0, $1[2]
			),
			w_const AS
			(	SELECT	t1.ordinality::integer,
					t1.attribute_type,
					t2.id,
					t3.variable
				FROM	unnest($1) WITH ORDINALITY AS t1(attribute_type,ordinality)
				INNER JOIN
					(
					SELECT attr1 AS id, attribute_type FROM w_attr1
					UNION ALL
					SELECT attr2 AS id, attribute_type FROM w_attr2
					) AS t2
				ON
					t1.attribute_type = t2.attribute_type
				LEFT JOIN
					target_data.'||_table_name_hierarchy||' AS t3
				ON
					t2.id = t3.variable_superior
			),
			w_desc AS(
				SELECT	t1.ordinality, t1.attribute_type, t1.id, t1.variable
				FROM	w_const AS t1
				INNER JOIN
					w_const AS t2
				ON t1.variable = t2.id
				WHERE	t1.ordinality < t2.ordinality
			),
			w_total AS (
				SELECT count(*) AS total
				FROM (SELECT DISTINCT t1.ordinality FROM w_desc AS t1 GROUP BY t1.ordinality) as t1
			),
			w_ordi AS (
				SELECT array_agg(t1.ordinality ORDER BY t1.ordinality) AS ordi_agg, total
				FROM
					(SELECT DISTINCT t1.ordinality, total
					FROM w_desc AS t1, w_total) AS t1
				GROUP BY total
			),
			w_attr AS(
				SELECT	attr1, attr2
				FROM	w_attr1, w_attr2
			),
			w_exclude AS (
				SELECT
					variable_superior AS attr_ex
				FROM
					target_data.'||_table_name_hierarchy||'
				WHERE
					variable IS NULL
			),
			w_all AS (
				SELECT
					attr1,
					attr2
				FROM
					w_attr
				WHERE
					CASE WHEN NOT EXISTS (SELECT total FROM w_ordi) THEN true
					END
					OR
					CASE WHEN 1 = (SELECT total FROM w_ordi) THEN 
						CASE WHEN ARRAY[1] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0)
						END
					END
				ORDER BY attr1, attr2
			)
			SELECT
				array_agg(attr1 ORDER BY attr1,attr2),
				array_agg(attr2 ORDER BY attr1,attr2),
				NULL::integer[],
				NULL::integer[],
				NULL::integer[]
			FROM
				w_all
			WHERE
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr2 = 0 ELSE TRUE END AND
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1] AND
					attr2 = $2[2]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1, _attr_2;

-----------------------------------------------------------------------------------------------------------------------
-- 3 attributes
		WHEN _at_count = 3 THEN

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0, $1[1]
			),
			w_attr2 AS
			(
				SELECT id AS attr2, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[2]
				UNION SELECT 0, $1[2]
			),
			w_attr3 AS
			(
				SELECT id AS attr3, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[3]
				UNION SELECT 0, $1[3]
			),
			w_const AS
			(	SELECT	t1.ordinality::integer,
					t1.attribute_type,
					t2.id,
					t3.variable
				FROM	unnest($1) WITH ORDINALITY AS t1(attribute_type,ordinality)
				INNER JOIN
					(
					SELECT attr1 AS id, attribute_type FROM w_attr1
					UNION ALL
					SELECT attr2 AS id, attribute_type FROM w_attr2
					UNION ALL
					SELECT attr3 AS id, attribute_type FROM w_attr3
					) AS t2
				ON
					t1.attribute_type = t2.attribute_type
				LEFT JOIN
					target_data.'||_table_name_hierarchy||' AS t3
				ON
					t2.id = t3.variable_superior
				),
			w_desc AS(
				SELECT	t1.ordinality, t1.attribute_type, t1.id, t1.variable
				FROM	w_const AS t1
				INNER JOIN
					w_const AS t2
				ON t1.variable = t2.id
				WHERE	t1.ordinality < t2.ordinality
			),
			w_total_comb AS (
				SELECT count(*) AS total
				FROM (SELECT DISTINCT t1.ordinality FROM w_desc AS t1 GROUP BY t1.ordinality) as t1
			),
			w_ordi AS (
				SELECT array_agg(t1.ordinality ORDER BY t1.ordinality) AS ordi_agg, total
				FROM
					(SELECT DISTINCT t1.ordinality, total
					FROM w_desc AS t1, w_total_comb) AS t1
				GROUP BY total
			),
			w_attr AS(
				SELECT	attr1,attr2,attr3
				FROM	w_attr1, w_attr2, w_attr3
			),
			w_exclude AS (
				SELECT
					variable_superior AS attr_ex
				FROM
					target_data.'||_table_name_hierarchy||'
				WHERE
					variable IS NULL
			),
			w_all AS (
				SELECT	attr1,
					attr2,
					attr3
				FROM
					w_attr
				WHERE
					CASE WHEN NOT EXISTS (SELECT total FROM w_ordi) THEN true
					END
					OR
					CASE WHEN 1 = (SELECT total FROM w_ordi) THEN 
						CASE WHEN ARRAY[1] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable_superior] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0)
						END OR
						CASE WHEN ARRAY[2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END
					END
					OR
					CASE WHEN 2 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END
					END
				ORDER BY attr1, attr2, attr3
			)
			SELECT
				array_agg(attr1 ORDER BY attr1,attr2,attr3),
				array_agg(attr2 ORDER BY attr1,attr2,attr3),
				array_agg(attr3 ORDER BY attr1,attr2,attr3),
				NULL::integer[],
				NULL::integer[]
			FROM
				w_all
			WHERE
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr2 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND

				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1] AND
					attr2 = $2[2] AND
					attr3 = $2[3]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1, _attr_2, _attr_3;

-----------------------------------------------------------------------------------------------------------------------
-- 4 attributes
		WHEN _at_count = 4 THEN

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0, $1[1]
			),
			w_attr2 AS
			(
				SELECT id AS attr2, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[2]
				UNION SELECT 0, $1[2]
			),
			w_attr3 AS
			(
				SELECT id AS attr3, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[3]
				UNION SELECT 0, $1[3]
			),
			w_attr4 AS
			(
				SELECT id AS attr4, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[4]
				UNION SELECT 0, $1[4]
			),
			w_const AS
			(	SELECT	t1.ordinality::integer,
					t1.attribute_type,
					t2.id,
					t3.variable
				FROM	unnest($1) WITH ORDINALITY AS t1(attribute_type,ordinality)
				INNER JOIN
					(
					SELECT attr1 AS id, attribute_type FROM w_attr1
					UNION ALL
					SELECT attr2 AS id, attribute_type FROM w_attr2
					UNION ALL
					SELECT attr3 AS id, attribute_type FROM w_attr3
					UNION ALL
					SELECT attr4 AS id, attribute_type FROM w_attr4
					) AS t2
				ON
					t1.attribute_type = t2.attribute_type
				LEFT JOIN
					target_data.'||_table_name_hierarchy||' AS t3
				ON
					t2.id = t3.variable_superior
				),
			w_desc AS(
				SELECT	t1.ordinality, t1.attribute_type, t1.id, t1.variable
				FROM	w_const AS t1
				INNER JOIN
					w_const AS t2
				ON t1.variable = t2.id
				WHERE	t1.ordinality < t2.ordinality
			),
			w_total_comb AS (
				SELECT count(*) AS total
				FROM (SELECT DISTINCT t1.ordinality FROM w_desc AS t1 GROUP BY t1.ordinality) as t1
			),
			w_ordi AS (
				SELECT array_agg(t1.ordinality ORDER BY t1.ordinality) AS ordi_agg, total
				FROM
					(SELECT DISTINCT t1.ordinality, total
					FROM w_desc AS t1, w_total_comb) AS t1
				GROUP BY total
			),
			w_attr AS(
				SELECT	attr1,attr2,attr3,attr4
				FROM	w_attr1, w_attr2, w_attr3, w_attr4
			),
			w_exclude AS (
				SELECT
					variable_superior AS attr_ex
				FROM
					target_data.'||_table_name_hierarchy||'
				WHERE
					variable IS NULL
			),
			w_all AS (
				SELECT	attr1,
					attr2,
					attr3,
					attr4
				FROM	w_attr
				WHERE
					CASE WHEN NOT EXISTS (SELECT total FROM w_ordi) THEN true
					END
					OR
					CASE WHEN 1 = (SELECT total FROM w_ordi) THEN 
						CASE WHEN ARRAY[1] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0)
						END OR
						CASE WHEN ARRAY[2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END OR
						CASE WHEN ARRAY[3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END
					END
					OR
					CASE WHEN 2 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END OR
						CASE WHEN ARRAY[1,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[2,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END
					END
					OR
					CASE WHEN 3 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END
					END
				ORDER BY attr1,attr2,attr3,attr4
			)
			SELECT	array_agg(attr1 ORDER BY attr1,attr2,attr3,attr4),
				array_agg(attr2 ORDER BY attr1,attr2,attr3,attr4),
				array_agg(attr3 ORDER BY attr1,attr2,attr3,attr4),
				array_agg(attr4 ORDER BY attr1,attr2,attr3,attr4),
				NULL::integer[]
			FROM
				w_all
			WHERE
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr2 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND

				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND

				CASE WHEN attr3 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1] AND
					attr2 = $2[2] AND
					attr3 = $2[3] AND
					attr4 = $2[4]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1, _attr_2, _attr_3, _attr_4;

-----------------------------------------------------------------------------------------------------------------------
-- 5 attributes
		WHEN _at_count = 5 THEN

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0, $1[1]
			),
			w_attr2 AS
			(
				SELECT id AS attr2, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[2]
				UNION SELECT 0, $1[2]
			),
			w_attr3 AS
			(
				SELECT id AS attr3, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[3]
				UNION SELECT 0, $1[3]
			),
			w_attr4 AS
			(
				SELECT id AS attr4, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[4]
				UNION SELECT 0, $1[4]
			),
			w_attr5 AS
			(
				SELECT id AS attr5, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[5]
				UNION SELECT 0, $1[5]
			),
			w_const AS
			(	SELECT	t1.ordinality::integer,
					t1.attribute_type,
					t2.id,
					t3.variable
				FROM	unnest($1) WITH ORDINALITY AS t1(attribute_type,ordinality)
				INNER JOIN
					(
					SELECT attr1 AS id, attribute_type FROM w_attr1
					UNION ALL
					SELECT attr2 AS id, attribute_type FROM w_attr2
					UNION ALL
					SELECT attr3 AS id, attribute_type FROM w_attr3
					UNION ALL
					SELECT attr4 AS id, attribute_type FROM w_attr4
					UNION ALL
					SELECT attr5 AS id, attribute_type FROM w_attr5
					) AS t2
				ON
					t1.attribute_type = t2.attribute_type
				LEFT JOIN
					target_data.'||_table_name_hierarchy||' AS t3
				ON
					t2.id = t3.variable_superior
				),
			w_desc AS(
				SELECT	t1.ordinality, t1.attribute_type, t1.id, t1.variable
				FROM	w_const AS t1
				INNER JOIN
					w_const AS t2
				ON t1.variable = t2.id
				WHERE	t1.ordinality < t2.ordinality
			),
			w_total_comb AS (
				SELECT count(*) AS total
				FROM (SELECT DISTINCT t1.ordinality FROM w_desc AS t1 GROUP BY t1.ordinality) as t1
			),
			w_ordi AS (
				SELECT array_agg(t1.ordinality ORDER BY t1.ordinality) AS ordi_agg, total
				FROM
					(SELECT DISTINCT t1.ordinality, total
					FROM w_desc AS t1, w_total_comb) AS t1
				GROUP BY total
			),
			w_attr AS(
				SELECT	attr1,attr2,attr3,attr4,attr5
				FROM	w_attr1, w_attr2, w_attr3, w_attr4, w_attr5
			),
			w_exclude AS (
				SELECT
					variable_superior AS attr_ex
				FROM
					target_data.'||_table_name_hierarchy||'
				WHERE
					variable IS NULL
			),
			w_all AS (
				SELECT	attr1,
					attr2,
					attr3,
					attr4,
					attr5
				FROM	w_attr
				WHERE
					CASE WHEN NOT EXISTS (SELECT total FROM w_ordi) THEN true
					END
					OR
					CASE WHEN 1 = (SELECT total FROM w_ordi) THEN 
						CASE WHEN ARRAY[1] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0)
						END OR
						CASE WHEN ARRAY[2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END OR
						CASE WHEN ARRAY[3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END
					END
					OR
					CASE WHEN 2 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END OR
						CASE WHEN ARRAY[1,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[1,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END OR
						CASE WHEN ARRAY[2,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[2,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END OR
						CASE WHEN ARRAY[3,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END
					END
					OR
					CASE WHEN 3 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[1,3,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END OR
						CASE WHEN ARRAY[2,3,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END OR
						CASE WHEN ARRAY[1,2,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END
					END
					OR
					CASE WHEN 4 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2,3,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END
					END
				ORDER BY attr1,attr2,attr3,attr4,attr5
			)
			SELECT	array_agg(attr1 ORDER BY attr1,attr2,attr3,attr4,attr5),
				array_agg(attr2 ORDER BY attr1,attr2,attr3,attr4,attr5),
				array_agg(attr3 ORDER BY attr1,attr2,attr3,attr4,attr5),
				array_agg(attr4 ORDER BY attr1,attr2,attr3,attr4,attr5),
				array_agg(attr5 ORDER BY attr1,attr2,attr3,attr4,attr5)
			FROM
				w_all
			WHERE
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr2 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr5 = 0 ELSE TRUE END AND

				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND
				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr5 = 0 ELSE TRUE END AND

				CASE WHEN attr3 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND
				CASE WHEN attr3 IN (SELECT attr_ex FROM w_exclude) THEN attr5 = 0 ELSE TRUE END AND

				CASE WHEN attr4 IN (SELECT attr_ex FROM w_exclude) THEN attr5 = 0 ELSE TRUE END AND
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1] AND
					attr2 = $2[2] AND
					attr3 = $2[3] AND
					attr4 = $2[4] AND
					attr5 = $2[5]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1, _attr_2, _attr_3, _attr_4, _attr_5;
		ELSE
			RAISE EXCEPTION 'Error 05: The function is not implemented for the specified number of attribute types (at_count = %)',_at_count;
		END CASE;

		IF _attribute_domain IS NOT NULL
		THEN
			IF _attr_1 IS NULL THEN RAISE EXCEPTION 'Error 06: No combination between attribute types was found for the specified attribute domain (attribute_types = %, attribute_domain = %).', _attr_types, _attribute_domain;
			END IF;
		END IF;

		RETURN QUERY SELECT _attr_1, _attr_2, _attr_3, _attr_4, _attr_5;		
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  COMMENT ON FUNCTION target_data.fn_get_attribute_domain(varchar(2), integer[], integer[]) IS
'The function returns the attribute domain field table for the specified attribute type combination.';
-- </function>
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- <function name="fn_import_data" schema="target_data" src="functions/fn_import_data.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_data(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_import_data(_schema character varying)
RETURNS text
AS
$$
DECLARE
		_res	text;
BEGIN
	-- Data for Name: c_ldsity_object; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity_object(id, label, description, table_name, upper_object, areal_or_population, column4upper_object, condition_columns)
	select id, label, description, table_name, upper_object, areal_or_population, column4upper_object, condition_columns from '||_schema||'.c_ldsity_object order by id';

	-- Data for Name: c_unit_of_measure; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_unit_of_measure(id, label, description)
	select id, label, description from '||_schema||'.c_unit_of_measure order by id';

	-- Data for Name: c_definition_variant; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_definition_variant(id, label, description)
	select id, label, description from '||_schema||'.c_definition_variant order by id';	

	-- Data for Name: c_area_domain; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain(id, label, description, array_order)
	select id, label, description, array_order from '||_schema||'.c_area_domain order by id';

	-- Data for Name: c_area_domain_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain_category(id, label, description, area_domain)
	select id, label, description, area_domain from '||_schema||'.c_area_domain_category order by id';

	-- Data for Name: cm_adc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_adc2classification_rule(area_domain_category, ldsity_object, classification_rule)
	select area_domain_category, ldsity_object, classification_rule from '||_schema||'.cm_adc2classification_rule order by id';	

	-- Data for Name: c_sub_population; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population(id, label, description, array_order)
	select id, label, description, array_order from '||_schema||'.c_sub_population order by id';

	-- Data for Name: c_sub_population_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population_category(id, label, description, sub_population)
	select id, label, description, sub_population from '||_schema||'.c_sub_population_category order by id';

	-- Data for Name: cm_spc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_spc2classification_rule(sub_population_category, ldsity_object, classification_rule)
	select sub_population_category, ldsity_object, classification_rule from '||_schema||'.cm_spc2classification_rule order by id';	

	-- Data for Name: c_ldsity; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity(id, label, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant)
	select id, label, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant from '||_schema||'.c_ldsity order by id';

	-- Data for Name: t_adc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_adc_hierarchy(variable_superior, variable)
	select variable_superior, variable from '||_schema||'.t_adc_hierarchy order by id';

	-- Data for Name: t_spc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_spc_hierarchy(variable_superior, variable)
	select variable_superior, variable from '||_schema||'.t_spc_hierarchy order by id';

	-- Name: c_ldsity_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_ldsity_id_seq', (select max(id) from target_data.c_ldsity), true);


	_res := 'Import is comleted.';

	return _res;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION sdesign.fn_import_data(character varying) IS
'The function imports data from source tables into tables that are in the target_data schema.';
-- </function>
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- <function name="fn_get_classification_rule4category" schema="target_data" src="functions/fn_get_classification_rule4category.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4category(integer[], integer[], integer[], integer[], integer[]) CASCADE;

create or replace function target_data.fn_get_classification_rule4category
(
	_area_domain_category		integer[],
	_area_domain_object			integer[],
	_sub_population_category 	integer[],
	_sub_population_object		integer[],
	_ldsity_objects				integer[] default null::integer[]
)
returns table
(
adc2classification_rule				integer[],
spc2classification_rule				integer[],
classification_rule4case			text,
classification_rule4case_objects	text
)
as
$$
declare
		_area_domain_category_types		text[];
		_sub_population_category_types	text[];
		_categories						integer[];
		_category_objects				integer[];
		_category_types					text[];
		_table_name						text;
		_column_name					text;
		_classification_rule_i			text;
		_classification_rule_4case		text;
	
		_categories_upr					integer[];
		_category_objects_upr			integer[];
		_category_types_upr				text[];
	
		_id_adc2classification_rule		integer[];
		_id_ad_i						integer;
		_id_spc2classification_rule		integer[];
		_id_sp_i						integer;
	
		_pozice_i						integer;
		_column_name_i_array			text[];
		_classification_rule_i_pozice		text;
		_classification_rule_4case_pozice	text;
begin
		if	_area_domain_category is null
		then
			raise exception 'Error 01: fn_get_classification_rule4category: The input argument _area_domain_category must not by NULL!';
		end if;
		
		if	_sub_population_category is null
		then
			raise exception 'Error 02: fn_get_classification_rule4category: The input argument _sub_population_category must not by NULL!';
		end if;
	
		---------------------------------------------------------------------------------
		if _area_domain_category = array[0]
		then
			_id_adc2classification_rule := null::integer[];
		else
			for i in 1..array_length(_area_domain_category,1)
			loop
				select id from target_data.cm_adc2classification_rule
				where area_domain_category = _area_domain_category[i]
				and ldsity_object = _area_domain_object[i]
				into _id_ad_i;
			
				if i = 1
				then
					if _id_ad_i is null
					then
						_id_adc2classification_rule := null::integer[];
					else
						_id_adc2classification_rule := array[_id_ad_i];
					end if;
				else
					if _id_ad_i is null
					then
						_id_adc2classification_rule := _id_adc2classification_rule;
					else
						_id_adc2classification_rule := _id_adc2classification_rule || array[_id_ad_i];
					end if;
				end if;
			
			end loop;
		end if;
	
		---------------------------------------------------------------------------------
		if _sub_population_category = array[0]
		then
			_id_spc2classification_rule := null::integer[];
		else
			for i in 1..array_length(_sub_population_category,1)
			loop
				select id from target_data.cm_spc2classification_rule
				where sub_population_category = _sub_population_category[i]
				and ldsity_object = _sub_population_object[i]
				into _id_sp_i;
			
				if i = 1
				then
					if _id_sp_i is null
					then
						_id_spc2classification_rule := null::integer[];
					else
						_id_spc2classification_rule := array[_id_sp_i];
					end if;
				else
					if _id_sp_i is null
					then
						_id_spc2classification_rule := _id_spc2classification_rule;
					else
						_id_spc2classification_rule := _id_spc2classification_rule || array[_id_sp_i];
					end if;
				end if;
			
			end loop;
		end if;
	
		---------------------------------------------------------------------------------
		
		_area_domain_category_types := array_agg('ad'::text) from generate_series(1,array_length(_area_domain_category,1));
		_sub_population_category_types := array_agg('sp'::text) from generate_series(1,array_length(_sub_population_category,1));
		
		if _area_domain_category  = array[0] and _sub_population_category = array[0]
		then
			_categories := null::integer[];
		end if;
	
		if _area_domain_category != array[0] and _sub_population_category = array[0]
		then
			_categories := _area_domain_category;
			_category_objects := _area_domain_object;
			_category_types := _area_domain_category_types;
		end if;
	
		if _area_domain_category  = array[0] and _sub_population_category != array[0]
		then
			_categories := _sub_population_category;
			_category_objects := _sub_population_object;
			_category_types := _sub_population_category_types;
		end if;
	
		if _area_domain_category != array[0] and _sub_population_category != array[0]
		then
			_categories := _area_domain_category || _sub_population_category;
			_category_objects := _area_domain_object || _sub_population_object;
			_category_types := _area_domain_category_types || _sub_population_category_types;
		end if;
		
		if _categories is not null
		then
			for i in 1..array_length(_categories,1)
			loop
				if _categories[i] = 0
				then
					if i = 1
					then
						_categories_upr := null::int[];
						_category_objects_upr := null::int[];
						_category_types_upr := null::text[];
					else
						_categories_upr := _categories_upr;
						_category_objects_upr := _category_objects_upr;
						_category_types_upr := _category_types_upr;
					end if;
				else
					if i = 1
					then
						_categories_upr := array[_categories[i]];
						_category_objects_upr := array[_category_objects[i]];
						_category_types_upr := array[_category_types[i]];
					else
						_categories_upr := _categories_upr || array[_categories[i]];
						_category_objects_upr := _category_objects_upr || array[_category_objects[i]];
						_category_types_upr := _category_types_upr || array[_category_types[i]];
					end if;
				end if;
			end loop;
		
			_categories := _categories_upr;
			_category_objects := _category_objects_upr;
			_category_types := _category_types_upr;
		end if;
	

		if _categories is not null
		then
			for i in 1..array_length(_categories,1)
			loop
				if _category_types[i] = 'ad' then _table_name := 'cm_adc2classification_rule'; _column_name := 'area_domain_category'; end if;
				if _category_types[i] = 'sp' then _table_name := 'cm_spc2classification_rule'; _column_name := 'sub_population_category'; end if;
			
				execute '
				select case when classification_rule is null then ''true'' else classification_rule end as classification_rule
				from target_data.'||_table_name||' where '||_column_name||' = $1 and ldsity_object = $2'
				using _categories[i], _category_objects[i]
				into _classification_rule_i;
			
				------------------------------
				if _ldsity_objects is not null
				then
					_pozice_i := array_position(_ldsity_objects,_category_objects[i]);
										
					select array_agg(column_name::text order by ordinal_position) from information_schema.columns
				 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _category_objects[i])
				 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _category_objects[i])
					into _column_name_i_array;
								
					for ii in 1..array_length(_column_name_i_array,1)
					loop					
						if ii = 1
						then
							_classification_rule_i_pozice := replace(
																_classification_rule_i,
																_column_name_i_array[ii],
																concat(_column_name_i_array[ii],'_',_pozice_i)
																);
						else
							_classification_rule_i_pozice := replace(
																_classification_rule_i_pozice,
																_column_name_i_array[ii],
																concat(_column_name_i_array[ii],'_',_pozice_i)
																);
						end if;
	
					end loop;
				else
					_classification_rule_i_pozice := null::text;
				end if;
				------------------------------
						
				if i = 1
				then
					_classification_rule_4case := _classification_rule_i;
				else
					_classification_rule_4case := concat(_classification_rule_4case,' and ',_classification_rule_i);
				end if;
			
				if _ldsity_objects is not null
				then
					if i = 1
					then
						_classification_rule_4case_pozice := _classification_rule_i_pozice;
					else
						_classification_rule_4case_pozice := concat(_classification_rule_4case_pozice,' and ',_classification_rule_i_pozice);
					end if;				
				else
					_classification_rule_4case_pozice := null::text;
				end if;
				
			end loop;
		
			_classification_rule_4case := concat('case when (',_classification_rule_4case,') then true else false end');
		
			if _ldsity_objects is not null
			then
				_classification_rule_4case_pozice := concat('case when (',_classification_rule_4case_pozice,') then true else false end');
			else
				_classification_rule_4case_pozice := _classification_rule_4case_pozice;
			end if;
		
		else
			_classification_rule_4case := 'true';
		
			if _ldsity_objects is not null
			then
				_classification_rule_4case_pozice := 'true';
			else
				_classification_rule_4case_pozice := null::text;
			end if;
		end if;
	
		return query select _id_adc2classification_rule, _id_spc2classification_rule, _classification_rule_4case, _classification_rule_4case_pozice;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

  COMMENT ON FUNCTION target_data.fn_get_classification_rule4category(integer[], integer[], integer[], integer[], integer[]) IS
'The function for the specified list of area or sub-population categories returns their classification rule.';
-- </function>
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- <function name="fn_get_classification_rules4case" schema="target_data" src="functions/fn_get_classification_rules4case.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rules4case(integer[], integer[], integer[], integer[], integer[]) CASCADE;

create or replace function target_data.fn_get_classification_rules4case
(
	_area_domain integer[],
	_area_domain_object integer[],
	_sub_population integer[],
	_sub_population_object integer[],
	_ldsity_objects	integer[]
)
returns text
as
$$
declare
		_adc			integer[][];
		_spc			integer[][];
		_s4o			text[];
		_adc_length		integer;
		_spc_length		integer;
		_s4o_length		integer;
		_res			text;
begin
		with
		w as	(
				select
						area_domain_category,
						sub_population_category,
						adc2classification_rule,
						spc2classification_rule,
						classification_rule4case_objects
				from
						target_data.fn_get_classification_rules4categories(_area_domain,_area_domain_object,_sub_population,_sub_population_object,_ldsity_objects)
				order
						by area_domain_category, sub_population_category
				)
		select
				array_agg(area_domain_category order by area_domain_category, sub_population_category) as adc,
				array_agg(sub_population_category order by area_domain_category, sub_population_category) as spc,
				array_agg(classification_rule4case_objects order by area_domain_category, sub_population_category) as s4o
		from
				w
		into
				_adc,
				_spc,
				_s4o;
						
		_adc_length := array_length(_adc,1);
		_spc_length := array_length(_spc,1);
		_s4o_length := array_length(_s4o,1);
	
		if _adc_length != _spc_length then raise exception ''; end if;
		if _adc_length != _s4o_length then raise exception ''; end if;
		if _spc_length != _s4o_length then raise exception ''; end if;
	
		for i in 1..array_length(_adc,1)
		loop
			if i = 1
			then
				_res := concat('when area_domain_category = array[',array_to_string(_adc[i:i],',',''),'] and sub_population_category = array[',array_to_string(_spc[i:i],',',''),'] then ',_s4o[i]);
			else
				_res := _res || concat(' when area_domain_category = array[',array_to_string(_adc[i:i],',',''),'] and sub_population_category = array[',array_to_string(_spc[i:i],',',''),'] then ',_s4o[i]);
			end if;
		end loop;
	
		_res := concat('case ',_res,' end');

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

  COMMENT ON FUNCTION target_data.fn_get_classification_rules4case(integer[], integer[], integer[], integer[], integer[]) IS
'The function for the specified list of area or sub-population domains returns their classification rules in a single text format as one uppercase CASE.';
-- </function>
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- <function name="fn_get_classification_rules4categories" schema="target_data" src="functions/fn_get_classification_rules4categories.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rules4categories(integer[], integer[], integer[], integer[], integer[]) CASCADE;

create or replace function target_data.fn_get_classification_rules4categories
(
	_area_domain integer[],
	_area_domain_object integer[],
	_sub_population integer[],
	_sub_population_object integer[],
	_ldsity_objects	integer[] default null::integer[]
)
returns table
(
	area_domain_category integer[],
	sub_population_category integer[],
	adc2classification_rule integer[],
	spc2classification_rule integer[],
	classification_rule4case text,
	classification_rule4case_objects text
) as
$$
declare
		_string_ad_w1			text;
		_string_ad_w3			text;
		_string_columns_ad		text;
		_string_columns_ad_w3	text;
	
		_string_sp_w2			text;
		_string_sp_w3			text;
		_string_columns_sp		text;
		_string_columns_sp_w3	text;
	
		_query_res				text;
begin
		-------------------------------
		-------------------------------
		if
			_area_domain is null
		then
			_string_ad_w1 := 'select 0 as adc_1';
			_string_ad_w3 := 'array[adc_1] as area_domain_category';
		else		
			for i in 1..array_length(_area_domain,1)
			loop
					if i = 1
					then
						_string_columns_ad := concat('unnest(attr_',i,') as adc_',i);
						_string_columns_ad_w3 := concat('adc_',i);
					else
						_string_columns_ad := concat(_string_columns_ad,', ',concat('unnest(attr_',i,') as adc_',i));
						_string_columns_ad_w3 := concat(_string_columns_ad_w3,', adc_',i);
					end if;
			end loop;
		
			_string_ad_w1 := replace(replace(concat('select ',_string_columns_ad,' from target_data.fn_get_attribute_domain(''ad''::varchar,array[',_area_domain,'])'),'{',''),'}','');
			_string_ad_w3 := concat('array[',_string_columns_ad_w3,'] as area_domain_category');
		end if;
		-------------------------------
		-------------------------------
		if
			_sub_population is null
		then
			_string_sp_w2 := 'select 0 as spc_1';
			_string_sp_w3 := 'array[spc_1] as sub_population_category';
		else		
			for i in 1..array_length(_sub_population,1)
			loop
					if i = 1
					then
						_string_columns_sp := concat('unnest(attr_',i,') as spc_',i);
						_string_columns_sp_w3 := concat('spc_',i);
					else
						_string_columns_sp := concat(_string_columns_sp,', ',concat('unnest(attr_',i,') as spc_',i));
						_string_columns_sp_w3 := concat(_string_columns_sp_w3,', spc_',i);
					end if;
			end loop;
		
			_string_sp_w2 := replace(replace(concat('select ',_string_columns_sp,' from target_data.fn_get_attribute_domain(''sp''::varchar,array[',_sub_population,'])'),'{',''),'}','');
			_string_sp_w3 := concat('array[',_string_columns_sp_w3,'] as sub_population_category');
		end if;
		-------------------------------
		-------------------------------	

		_query_res := concat
		('
		with
		w1 as	(',_string_ad_w1,'),
		w2 as	(',_string_sp_w2,'),
		w3 as	(select ',_string_ad_w3,', ',_string_sp_w3,' from w1, w2),
		w4 as	(
				select
					area_domain_category,
					sub_population_category,
					(
					target_data.fn_get_classification_rule4category
						(
						area_domain_category,
						$1,
						sub_population_category,
						$2,
						$3
						)
					) as res
				from
					w3
				)
		select
				area_domain_category,
				sub_population_category,
				(res).adc2classification_rule,
				(res).spc2classification_rule,
				(res).classification_rule4case,
				(res).classification_rule4case_objects
		from
				w4 order by area_domain_category, sub_population_category;
		');

		return query execute ''||_query_res||'' using _area_domain_object, _sub_population_object, _ldsity_objects;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_get_classification_rules4categories(integer[], integer[], integer[], integer[], integer[]) IS
'The function for the specified list of area or sub-population domains returns their classification rules.';
-- </function>
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- <function name="fn_get_ldsity_objects" schema="target_data" src="functions/fn_get_ldsity_objects.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_objects(integer[]) CASCADE;

create or replace function target_data.fn_get_ldsity_objects(_ldsity_objects integer[])
returns integer[]
as
$$
declare
		_res_1	integer;
		_res	integer[];
begin
		if _ldsity_objects is null
		then
			raise exception 'Error 01: fn_get_ldsity_objects: The input argument _ldsity_objects must not be NULL !';
		end if;	
	
		select upper_object from target_data.c_ldsity_object where id = _ldsity_objects[1]
		into _res_1;	
	
		if _res_1 is null
		then
			_res := _ldsity_objects;
		else
			_res := target_data.fn_get_ldsity_objects(array[_res_1] || _ldsity_objects);
		end if;
	
	return _res;

end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_get_ldsity_objects(integer[]) IS
'The function for the specified ldsity object hierarchically returns a list of objects.';
-- </function>
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- <function name="fn_get_plots" schema="target_data" src="functions/fn_get_plots.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_plots(integer[], integer[]) CASCADE;

create or replace function target_data.fn_get_plots(_panels integer[], _reference_year_sets integer[])
returns table
(
	gid integer,
	cluster_configuration integer,
	reference_year_set integer,
	panel integer,
	stratum integer
)
as
$$
declare
begin
		if _panels is null
		then
			raise exception 'Error 01: fn_get_plots: The input argument _panels must not be NULL !';
		end if;
	
		if _reference_year_sets is null
		then
			raise exception 'Error 02: fn_get_plots: The input argument _reference_year_sets must not be NULL !';
		end if;
	
		return query execute
			'
			with
			w0 as	(
					select * from sdesign.cm_refyearset2panel_mapping
					where reference_year_set in (select unnest($2))
					)
			,w1 as	(
					select
							w0.reference_year_set,
							a.id as panel_id,
							a.stratum,
							a.cluster_configuration
					from
						w0
					inner join
						(select * from sdesign.t_panel where id in (select unnest($1))) as a
					on
						w0.panel = a.id
					)
			,w2 as	(
					select
						a.*, b.*, c.stratum
					from
						sdesign.t_cluster as a
					inner join 					
						(
						select panel, cluster from sdesign.cm_cluster2panel_mapping
						where panel in (select panel_id from w1)
						) as b
					on
						a.id = b.cluster
					
					inner join sdesign.t_panel as c on b.panel = c.id
					)				
			,w3 as	(
					select * from sdesign.cm_plot2cluster_config_mapping
					where cluster_configuration in
						(		
						select id from sdesign.t_cluster_configuration where id in
						(select distinct cluster_configuration from w1)
						)
					)
			,w4 as	(
					select * from sdesign.t_plot_measurement_dates
					where reference_year_set in (select unnest($2))
					)
			,w5 as	(
					select
							a.gid,
							w3.cluster_configuration,
							w4.reference_year_set,
							w2.panel,
							w2.stratum
					from sdesign.f_p_plot as a
					inner join w3 on a.gid = w3.plot
					inner join w4 on a.gid = w4.plot
					inner join w2 on a.cluster = w2.id
					)
			select
					gid,
					cluster_configuration,
					reference_year_set,
					panel,
					stratum
			from
					w5 order by gid;
			'
			using _panels, _reference_year_sets;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

  COMMENT ON FUNCTION target_data.fn_get_plots(integer[], integer[]) IS
'The function for the specified list of panels and reference_year_sets returns a point list for local densities.';
-- </function>
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- <function name="fn_t_ldsity_values_check_is_latest" schema="target_data" src="functions/fn_t_ldsity_values_check_is_latest.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_t_ldsity_values_check_is_latest() CASCADE;

create or replace function target_data.fn_t_ldsity_values_check_is_latest()
returns trigger AS
$$
begin
		if	(
			select count(*) > 1 from target_data.t_ldsity_values
			where plot = new.plot
			and available_datasets = new.available_datasets
			and is_latest = true
			)
		then
			raise exception 'Error 01: fn_t_ldsity_values_check_is_latest: Not allowed to update or insert the value of is_latest in the table t_ldsity_values for records plot = % and available_datasets = %, due to the fact that the value of is_latest = TRUE would be filled more than once!', new.plot, new.available_datasets;
		end if;

	return new;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_t_ldsity_values_check_is_latest() IS
'A trigger function that checks that only one value is_latest = TRUE for the same plot and available_datasets records.';
-- </function>
---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------
-- <function name="fn_import_ldsity_values" schema="target_data" src="functions/fn_import_ldsity_values.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_ldsity_values(integer[], integer[], integer, integer[], integer[], integer[], integer[], double precision) CASCADE;

create or replace function target_data.fn_import_ldsity_values
(
	_panels					integer[],
	_reference_year_sets	integer[],
	_target_variable		integer,
	_area_domain			integer[],
	_area_domain_object		integer[],
	_sub_population			integer[],
	_sub_pupulation_object	integer[],
	_threshold				double precision default 0.0
)
returns text
as
$$
declare
		_ldsity								integer;
		_ldsity_ldsity_object				integer;
		_ldsity_column_expresion			text;
		_ldsity_unit_of_measure				integer;
		_ldsity_area_domain_category		integer[];
		_ldsity_sub_population_category		integer[];
		_ldsity_definition_variant			integer[];
		_tv_area_domain_category			integer[];
		_tv_sub_population_category			integer[];
		_tv_definition_variant				integer[];
		_area_domain_category				integer[];
		_sub_population_category			integer[];
		_definition_variant					integer[];
		_area_domain_category_i				integer[];
		_area_domain_category_t				text[];
		_sub_population_category_i			integer[];
		_sub_population_category_t			text[];	
		_condition_ids						integer[];
		_condition_types					text[];
		_example_query						text;
		_ldsity_objects						integer[];
		_example_query_i					text;
		_example_query_array				text[];
		_query_res							text;
		_pozice_i							integer;
		_pozice_array						integer[];
		_classification_rule_i				text;
		_classification_rule_array			text[];
		_position_groups					integer[];
		_classification_rule_upr_ita_pozice	text[];
		_classification_rule_ita_pozice		text;
		_ldsity_objects_without_conditions	integer[];
		_position4plots						integer;
		_pkey_column						text;
		_ldsity_objects_without_con4plots	integer[];
		_res								text;
	
		_string4columns						text;
		_string4inner_joins					text;
		_position4join						integer;
		_table_name4join					text;
		_pkey_column4join					text;
		_i_column4join						text;
		_column_name_i_array				text[];
		_ii_column_new_text					text;
	
		_string4plots						text;
		_position4plots_join				integer;
		_pkey_column4join_plots				text;
		_string4plots_join					text;
		_string4area_domain_category		text;
		_string4sub_population_category		text;
		_string4ads							text;
		_string4inserts						text;
		_position4ldsity					integer;
		_string4ldsity						text;
	
		_string4case						text;
	
		_count_before_tad					integer;
		_count_before_tlv					integer;
		_count_after_tad					integer;
		_count_after_tlv					integer;
begin
		if _panels is null
		then
			raise exception 'Error 01: fn_import_ldsity_values: The input argument _panels must not be NULL !';
		end if;
	
		if _reference_year_sets is null
		then
			raise exception 'Error 02: fn_import_ldsity_values: The input argument _reference_year_sets must not be NULL !';
		end if;
	
		if _target_variable is null
		then
			raise exception 'Error 03: fn_import_ldsity_values: The input argument _target_variable must not be NULL !';
		end if;
	
		select
				ldsity
				area_domain_category,
				sub_population_category,
				definition_variant
		from
				target_data.cm_ldsity2target_variable where target_variable = _target_variable
		into
				_ldsity,
				_tv_area_domain_category,
				_tv_sub_population_category,
				_tv_definition_variant;
	
		if _ldsity is null
		then
			raise exception 'Error 04: fn_import_ldsity_values: Ldsity value not found for input argument _target_variable = % in table cm_ldsity2target_variable!',_target_variable;
		end if;
	
		select
				ldsity_object,
				column_expression,
				unit_of_measure,
				area_domain_category,
				sub_population_category,
				definition_variant
		from
				target_data.c_ldsity where id = _ldsity
		into
				_ldsity_ldsity_object,
				_ldsity_column_expresion,
				_ldsity_unit_of_measure,
				_ldsity_area_domain_category,
				_ldsity_sub_population_category,
				_ldsity_definition_variant;
							 
		_area_domain_category := _ldsity_area_domain_category || _tv_area_domain_category;
		_sub_population_category := _ldsity_sub_population_category || _tv_sub_population_category;
		_definition_variant := _ldsity_definition_variant || _tv_definition_variant;
	
		-----------------------------------------
		if _area_domain_category is not null
		then
			for i in 1..array_length(_area_domain_category,1)
			loop
				if i = 1
				then
					_area_domain_category_i := array[_area_domain_category[i]];
					_area_domain_category_t := array['ad'];
				else
					_area_domain_category_i := _area_domain_category_i || array[_area_domain_category[i]];
					_area_domain_category_t := _area_domain_category_t || array['ad'];
				end if;
			end loop;
		else
			_area_domain_category_i := null::integer[];
			_area_domain_category_t := null::text[];
		end if;
		-----------------------------------------
		if _sub_population_category is not null
		then
			for i in 1..array_length(_sub_population_category,1)
			loop
				if i = 1
				then
					_sub_population_category_i := array[_sub_population_category[i]];
					_sub_population_category_t := array['sp'];
				else
					_sub_population_category_i := _sub_population_category_i || array[_sub_population_category[i]];
					_sub_population_category_t := _sub_population_category_t || array['sp'];
				end if;
			end loop;
		else
			_sub_population_category_i := null::integer[];
			_sub_population_category_t := null::text[];
		end if;
		-----------------------------------------
		-----------------------------------------
	
		_condition_ids := _area_domain_category_i || _sub_population_category_i;
		_condition_types := _area_domain_category_t || _sub_population_category_t;
	
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _condition_ids is null
		then
			_example_query := 'w_&i& as (select #column_names_&i&# from #table_name#_&i& where #con4plots_&i&#)';
		else
			_example_query := 'w_&i& as (select #column_names_&i&# from #table_name#_&i& where #con4plots_&i&# and #conditions_&i&#)';
		end if;
		
		_ldsity_objects := target_data.fn_get_ldsity_objects(array[_ldsity_ldsity_object]);
	
			
		for i in 1..array_length(_ldsity_objects,1)
		loop
				_example_query_i := replace(_example_query,'&i&'::text,i::text);
			
				if i = 1
				then
					_example_query_array := array[_example_query_i];
				else
					_example_query_array := _example_query_array || _example_query_i;
				end if;
		end loop;
	
		for i in 1..array_length(_ldsity_objects,1)
		loop
				if i = 1
				then
					_query_res := concat('with ',_example_query_array[i]);
				else
					_query_res := _query_res || concat(',',_example_query_array[i]);
				end if;
		end loop;
	
		for i in 1..array_length(_ldsity_objects,1)
		loop
				_query_res := replace(_query_res,concat('#table_name#_',i),
							  (select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]));
							 
				_query_res := replace(_query_res,'&i&'::text,i::text);
		end loop;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
	
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		for i in 1..array_length(_ldsity_objects,1)
		loop
			select array_agg(column_name order by ordinal_position) from information_schema.columns
		 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
		 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
			into _column_name_i_array;
			
			for ii in 1..array_length(_column_name_i_array,1)
			loop
				if ii = 1
				then
					_ii_column_new_text := concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i);
				else
					_ii_column_new_text := concat(_ii_column_new_text,', ',concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i));
				end if;
			end loop;
		
			_query_res := replace(_query_res,concat('#column_names_',i::text,'#'),_ii_column_new_text);
			
		end loop;	
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _condition_ids is not null
		then
			for i in 1..array_length(_condition_ids,1)
			loop
				if _condition_types[i] = 'ad'
				then
					-- where?				
					select array_position(_ldsity_objects,
					(select ldsity_object from target_data.cm_adc2classification_rule where id = _condition_ids[i]))
					into _pozice_i;
				
					if _pozice_i is null then raise exception 'Error 05: fn_import_ldsity_values: For internal variable _condition_ids[i] = % not found position in internal variable _ldsity_objects = % !',_condition_ids[i],_ldsity_objects; end if;
				
					-- what?				
					select case when classification_rule is null then 'true' else classification_rule end
					from target_data.cm_adc2classification_rule where id = _condition_ids[i]
					into _classification_rule_i;
				
					if _classification_rule_i is null then raise exception 'Error 06: fn_import_ldsity_values: The classification rule in the cm_adc2classification_rule table was not found for the internal variable _condition_ids[i] = %!,',_condition_ids[i]; end if;
				
				end if;
			
				if _condition_types[i] = 'sp'
				then
					-- where?				
					select array_position(_ldsity_objects,
					(select ldsity_object from target_data.cm_spc2classification_rule where id = _condition_ids[i]))
					into _pozice_i;				
				
					if _pozice_i is null then raise exception 'Error 07: fn_import_ldsity_values: For internal variable _condition_ids[i] = % not found position in internal variable _ldsity_objects = % !',_condition_ids[i],_ldsity_objects; end if;
				
					-- what?			
					select case when classification_rule is null then 'true' else classification_rule end
					from target_data.cm_spc2classification_rule where id = _condition_ids[i]
					into _classification_rule_i;
				
					if _classification_rule_i is null then raise exception 'Error 08: fn_import_ldsity_values: The classification rule in the cm_spc2classification_rule table was not found for the internal variable _condition_ids[i] = %!,',_condition_ids[i]; end if;				
				
				end if;
			
			
				if i = 1
				then
					_pozice_array := array[_pozice_i];
					_classification_rule_array := array[_classification_rule_i];
				else
					_pozice_array := _pozice_array || array[_pozice_i];
					_classification_rule_array := _classification_rule_array || array[_classification_rule_i];
				end if;
				
			end loop;
					
			with
			w1 as	(select unnest(_pozice_array) as pozice)
			,w2 as 	(select distinct pozice from w1)
			select array_agg(pozice order by pozice) from w2
			into _position_groups;
		
			for i in 1..array_length(_position_groups,1)
			loop
				with
				w1 as	(
						select
						unnest(_pozice_array) as pozice,
						unnest(_classification_rule_array) as classification_rule
						)
				,w2 as	(
						select
								row_number() over () as new_id,
								pozice,
								classification_rule
						from w1
						)
				,w3 as	(
						select
								row_number() over (partition by pozice order by new_id) as new_id4order,
								w2.*
						from w2
						)
				select array_agg(classification_rule order by new_id4order) as classification_rule
				from w3 where pozice = _position_groups[i]
				into _classification_rule_upr_ita_pozice;
			
				for ii in 1..array_length(_classification_rule_upr_ita_pozice,1)
				loop
					if ii = 1
					then
						_classification_rule_ita_pozice := concat(_classification_rule_upr_ita_pozice[ii]);
					else
						_classification_rule_ita_pozice := concat(_classification_rule_ita_pozice,' and ',_classification_rule_upr_ita_pozice[ii]);
					end if;
				end loop;
			
				_query_res := replace(_query_res,concat('#conditions_',_position_groups[i],'#'),concat('(',_classification_rule_ita_pozice,')'));
			
			end loop;
		
			with
			w1 as	(select * from generate_series(1,array_length(_ldsity_objects,1)) as res)
			,w2 as	(select unnest(_position_groups) as res)
			,w3 as	(select res from w1 except select res from w2)
			select array_agg(res order by res) from w3
			into _ldsity_objects_without_conditions;
			
			if _ldsity_objects_without_conditions is not null
			then
				for i in 1..array_length(_ldsity_objects_without_conditions,1)
				loop
					_query_res := replace(_query_res,concat('#conditions_',_ldsity_objects_without_conditions[i],'#'),'(true)');
				end loop;
			end if;
					
		end if;	
	
		-----------------------------------------	
		select array_position(_ldsity_objects,(select id from target_data.c_ldsity_object where upper_object is null))
		into _position4plots;
	
		with
		w as	(
				select kcu.table_schema,
				       kcu.table_name,
				       tco.constraint_name,
				       kcu.ordinal_position as position,
				       kcu.column_name as key_column
				from information_schema.table_constraints tco
				join information_schema.key_column_usage kcu 
				     on kcu.constraint_name = tco.constraint_name
				     and kcu.constraint_schema = tco.constraint_schema
				     and kcu.constraint_name = tco.constraint_name
				where tco.constraint_type = 'PRIMARY KEY'
				order by kcu.table_schema,
				         kcu.table_name,
				         position
				 )
		 select key_column::text from w
		 where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where upper_object is null ) --'nfi_analytical'
		 and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where upper_object is null) --'t_plots'
		 into _pkey_column;
				
		_query_res :=
		replace
		(
			_query_res,
			concat('#con4plots_',_position4plots,'#'),
			replace(replace(concat('(',_pkey_column,' in ','(select gid from target_data.fn_get_plots(array[',_panels,'],array[',_reference_year_sets,']))',')'),
			'{',''),'}','')
		);
						
		with
		w1 as	(select * from generate_series(1,array_length(_ldsity_objects,1)) as res)
		,w2 as	(select _position4plots as res)
		,w3 as	(select res from w1 except select res from w2)
		select array_agg(res order by res) from w3
		into _ldsity_objects_without_con4plots;
		
		if _ldsity_objects_without_con4plots is not null
		then
			for i in 1..array_length(_ldsity_objects_without_con4plots,1)
			loop
				_query_res := replace(_query_res,concat('#con4plots_',_ldsity_objects_without_con4plots[i],'#'),'(true)');
			end loop;
		end if;
		-----------------------------------------

		-----------------------------------------
		for i in 1..array_length(_ldsity_objects,1)
		loop
			if i = 1
			then
				_string4columns := concat(',w_join as (select w_',i,'.*');
				_string4inner_joins := concat(' from w_',i);
			else
				_string4columns := concat(_string4columns,', w_',i,'.*');
			
				select array_position(_ldsity_objects, 
					(select upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]))
				into _position4join;
				
				for i in 1..array_length(_ldsity_objects,1)
				loop
						if i = _position4join
						then
							_table_name4join := (select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]);
						end if;
				end loop;
			
				with
				w as	(
						select kcu.table_schema,
						       kcu.table_name,
						       tco.constraint_name,
						       kcu.ordinal_position as position,
						       kcu.column_name as key_column
						from information_schema.table_constraints tco
						join information_schema.key_column_usage kcu 
						     on kcu.constraint_name = tco.constraint_name
						     and kcu.constraint_schema = tco.constraint_schema
						     and kcu.constraint_name = tco.constraint_name
						where tco.constraint_type = 'PRIMARY KEY'
						order by kcu.table_schema,
						         kcu.table_name,
						         position
						 )
				select key_column::text from w
				where table_schema = (substring(_table_name4join from 1 for (position('.' in _table_name4join) - 1))) 
				and table_name = (substring(_table_name4join from (position('.' in _table_name4join) + 1) for length(_table_name4join)))
				into _pkey_column4join;
				
				select column4upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]
				into _i_column4join;
			
				_string4inner_joins :=
				concat(_string4inner_joins,' inner join w_',i,' on ','w_',_position4join,'.',_pkey_column4join,'_',_position4join,' = ','w_',i,'.',_i_column4join,'_',i);
						
			end if;
		end loop;
		-----------------------------------------
		
		_query_res := _query_res || _string4columns || _string4inner_joins || ')';
	
		-----------------------------------------
		_string4plots := replace(replace(concat(',w_plots as (select * from target_data.fn_get_plots(array[',_panels,'],array[',_reference_year_sets,']))'),
							'{',''),'}','');
		-----------------------------------------
		select array_position(_ldsity_objects,(select id from target_data.c_ldsity_object where upper_object is null))
		into _position4plots_join;
	
		with
		w as	(
				select kcu.table_schema,
				       kcu.table_name,
				       tco.constraint_name,
				       kcu.ordinal_position as position,
				       kcu.column_name as key_column
				from information_schema.table_constraints tco
				join information_schema.key_column_usage kcu 
				     on kcu.constraint_name = tco.constraint_name
				     and kcu.constraint_schema = tco.constraint_schema
				     and kcu.constraint_name = tco.constraint_name
				where tco.constraint_type = 'PRIMARY KEY'
				order by kcu.table_schema,
				         kcu.table_name,
				         position
				 )
		select key_column::text from w
		where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where upper_object is null ) --'nfi_analytical'
		and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where upper_object is null) --'t_plots'
		into _pkey_column4join_plots;
		
		_string4plots_join := concat(',w_res as (select w_join.*, w_plots.*',', ',_target_variable,' as target_variable from w_join inner join w_plots on w_join.',_pkey_column4join_plots,'_',_position4plots_join,' = w_plots.gid)');
		-----------------------------------------
		_query_res := _query_res || _string4plots || _string4plots_join;
		-----------------------------------------
		
		-----------------------------------------
		-----------------------------------------	
		_string4case := target_data.fn_get_classification_rules4case(_area_domain,_area_domain_object,_sub_population,_sub_pupulation_object,_ldsity_objects);
	
		_string4ads := concat('
		,w_strings as	(
						select
								area_domain_category,
								sub_population_category,
								adc2classification_rule,
								spc2classification_rule,
								classification_rule4case_objects
						from
								target_data.fn_get_classification_rules4categories($1,$2,$3,$4,$5)
						)
		,w_pre_adc as	(
						select
								w_strings.*,
								w_res.*
						from
								w_res, w_strings
						)
		,w_ads as		(
						select
								',_string4case,' as res_case,
								w_pre_adc.*
						from
								w_pre_adc
						)
		');	
	
		-----------------------------------------
		_query_res := _query_res || _string4ads;
		-----------------------------------------
		
		_string4inserts := 
		'
		,w_dis as			(
							select distinct panel, reference_year_set, target_variable, adc2classification_rule as area_domain_category, spc2classification_rule as sub_population_category from w_ads
							)					
		,w4ads as			(
							select panel, reference_year_set, target_variable, area_domain_category, sub_population_category from w_dis
							except
							select panel, reference_year_set, target_variable, area_domain_category, sub_population_category from target_data.t_available_datasets
							)		
		,w_insert_ads as	(
							insert into target_data.t_available_datasets(panel,reference_year_set,target_variable,area_domain_category,sub_population_category)
							select panel,reference_year_set,target_variable,area_domain_category,sub_population_category from w4ads
							returning id,panel,reference_year_set,target_variable,area_domain_category,sub_population_category
							)
		,w_ads4join as		(
							select id,panel,reference_year_set,target_variable,area_domain_category,sub_population_category from w_insert_ads
							union
							select a.* from
								(select * from target_data.t_available_datasets) as a
							inner join
							(select distinct panel, reference_year_set, target_variable, area_domain_category, sub_population_category from w_dis) as b
							on (a.panel = b.panel)
							and (a.reference_year_set = b.reference_year_set)
							and (a.target_variable = b.target_variable)					
							and ((case when a.area_domain_category is null then array[0] else a.area_domain_category end)
								= (case when b.area_domain_category is null then array[0] else b.area_domain_category end))
							and ((case when a.sub_population_category is null then array[0] else a.sub_population_category end)
								= (case when b.sub_population_category is null then array[0] else b.sub_population_category end))
							)		
		,w_pre_insert_ldsity as	(
								select w_ads.*, w_ads4join.id as ads_id from w_ads left join w_ads4join
								on (w_ads.panel = w_ads4join.panel)
								and (w_ads.reference_year_set = w_ads4join.reference_year_set)
								and (w_ads.target_variable = w_ads4join.target_variable)					
								and ((case when w_ads.adc2classification_rule is null then array[0] else w_ads.adc2classification_rule end)
									= (case when w_ads4join.area_domain_category is null then array[0] else w_ads4join.area_domain_category end))
								and ((case when w_ads.spc2classification_rule is null then array[0] else w_ads.spc2classification_rule end)
									= (case when w_ads4join.sub_population_category is null then array[0] else w_ads4join.sub_population_category end))
								)
		';

		-----------------------------------------
		_query_res := _query_res || _string4inserts;
		-----------------------------------------
	
		_position4ldsity := array_position(_ldsity_objects,_ldsity_ldsity_object);
		_string4ldsity := concat(' ',
						'
						,w_res1 as	(
									select
											gid as plot,
											ads_id as available_datasets,
											sum(',_ldsity_column_expresion,'_',_position4ldsity,') as value,
											now() as creation_time,
											true as is_latest,
											(SELECT extversion FROM pg_extension WHERE extname = ''nfiesta_target_data'') as ext_version
									from
											w_pre_insert_ldsity where res_case = true
									group
											by gid, ads_id
									order
											by ads_id, gid
									)
						,w_res2 as	(
									select
											row_number() over() as new_id,
											w_res1.plot,
											w_res1.available_datasets,
											w_res1.value,
											w_res1.creation_time,
											w_res1.is_latest,
											w_res1.ext_version
									from
											w_res1
									)
						,w_tlv as	(
									select
											tlv.id,
											tlv.plot,
											tlv.available_datasets,
											tlv.value,
											w_res2.new_id,
											--case when tlv.value = w_res2.value then true else false end as value_identic
											case when (abs(1 - (tlv.value / w_res2.value)) * 100.0) <= ',_threshold,' then true else false end as value_identic
									from
											target_data.t_ldsity_values as tlv
									inner
											join w_res2
									on
											tlv.plot = w_res2.plot and
											tlv.available_datasets = w_res2.available_datasets
									where
											tlv.is_latest = true	
									)
							,w_update as	(
											update target_data.t_ldsity_values set is_latest = false where id in
											(select id from w_tlv where value_identic = false)	
											)								
							insert into target_data.t_ldsity_values(plot,available_datasets,value,creation_time,is_latest,ext_version)
							select
									w_res2.plot,
									w_res2.available_datasets,
									w_res2.value,
									w_res2.creation_time,
									w_res2.is_latest,
									w_res2.ext_version
							from
									w_res2
							where
									w_res2.new_id not in (select new_id from w_tlv where value_identic = true)
							;
						');
		-----------------------------------------
		_query_res := _query_res || _string4ldsity;
		-----------------------------------------
	
	---------------------------------------------
	select count(*) from target_data.t_available_datasets into _count_before_tad;
	select count(*) from target_data.t_ldsity_values into _count_before_tlv;
	execute ''||_query_res||'' using _area_domain,_area_domain_object,_sub_population,_sub_pupulation_object,_ldsity_objects;
	select count(*) from target_data.t_available_datasets into _count_after_tad;
	select count(*) from target_data.t_ldsity_values into _count_after_tlv;
	_res := concat('The ',(_count_after_tad - _count_before_tad),' new rows was imported into t_available_datasets and the ',(_count_after_tlv - _count_before_tlv),' new rows was imported into t_ldsity_values.');
	return _res;
	---------------------------------------------

end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_import_ldsity_values(integer[], integer[], integer, integer[], integer[], integer[], integer[], double precision) IS
'The function for the specified list of input arguments inserts data into the t_available_datasets table and inserts data into the t_ldsity_values table (aggregated local density at the plot level).';
-- </function>
---------------------------------------------------------------------------------------------------