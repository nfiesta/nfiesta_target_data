--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------
-- t_categorization_setup
---------------------------------------------------------------------
CREATE TABLE target_data.t_categorization_setup (id integer NOT NULL);

ALTER TABLE target_data.t_categorization_setup ADD CONSTRAINT pkey__t_categorization_setup__id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__t_categorization_setup__id ON target_data.t_categorization_setup IS 'Primary key.';

COMMENT ON TABLE target_data.t_categorization_setup IS '.';
COMMENT ON COLUMN target_data.t_categorization_setup.id IS 'Identifier of the record.';

SELECT pg_catalog.pg_extension_config_dump('target_data.t_categorization_setup', '');

GRANT SELECT ON TABLE target_data.t_categorization_setup TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE target_data.t_categorization_setup TO app_nfiesta_mng;
---------------------------------------------------------------------


---------------------------------------------------------------------
-- cm_ldsity2target2categorization_setup
---------------------------------------------------------------------
CREATE TABLE target_data.cm_ldsity2target2categorization_setup (
id serial NOT NULL,
ldsity2target_variable integer NOT NULL,
adc2classification_rule integer[],
spc2classification_rule integer[],
categorization_setup integer NOT NULL
);

ALTER TABLE target_data.cm_ldsity2target2categorization_setup ADD CONSTRAINT pkey__cm_ldsity2target2categorization_setup__id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__cm_ldsity2target2categorization_setup__id ON target_data.cm_ldsity2target2categorization_setup IS 'Primary key.';

COMMENT ON TABLE target_data.cm_ldsity2target2categorization_setup IS 'Table with mapping between ldsity2target_variable and categorization_setup.';
COMMENT ON COLUMN target_data.cm_ldsity2target2categorization_setup.id IS 'Identifier of the record.';
COMMENT ON COLUMN target_data.cm_ldsity2target2categorization_setup.ldsity2target_variable IS 'Identifier of the ldsity2target_variable, foreing key to table cm_ldsity2target_variable.';
COMMENT ON COLUMN target_data.cm_ldsity2target2categorization_setup.adc2classification_rule IS 'Identifier of the adc2classification_rule, array of foreing keys to table cm_adc2classification_rule.';
COMMENT ON COLUMN target_data.cm_ldsity2target2categorization_setup.spc2classification_rule IS 'Identifier of the spc2classification_rule, array of foreing keys to table cm_spc2classification_rule.';
COMMENT ON COLUMN target_data.cm_ldsity2target2categorization_setup.categorization_setup IS 'Identifier of the categorization_setup, foreing key to table t_categorization_setup.';


ALTER TABLE target_data.cm_ldsity2target2categorization_setup ADD CONSTRAINT
fkey__cm_ldsity2target2categorization_setup__cm_ldsity2target_variable FOREIGN KEY (ldsity2target_variable)
REFERENCES target_data.cm_ldsity2target_variable (id);
COMMENT ON CONSTRAINT fkey__cm_ldsity2target2categorization_setup__cm_ldsity2target_variable ON target_data.cm_ldsity2target2categorization_setup IS
'Foreign key to table cm_ldsity2target_variable.';

ALTER TABLE target_data.cm_ldsity2target2categorization_setup ADD CONSTRAINT
fkey__cm_ldsity2target2categorization_setup__t_categorization_setup FOREIGN KEY (categorization_setup)
REFERENCES target_data.t_categorization_setup (id);
COMMENT ON CONSTRAINT fkey__cm_ldsity2target2categorization_setup__t_categorization_setup ON target_data.cm_ldsity2target2categorization_setup IS
'Foreign key to table t_categorization_setup.';

CREATE INDEX fki__cm_ldsity2target2categorization_setup__cm_ldsity2target_variable ON target_data.cm_ldsity2target2categorization_setup USING btree (ldsity2target_variable);
COMMENT ON INDEX target_data.fki__cm_ldsity2target2categorization_setup__cm_ldsity2target_variable IS
'BTree index on foreign key fkey__cm_ldsity2target2categorization_setup__cm_ldsity2target_variable.';

CREATE INDEX fki__cm_ldsity2target2categorization_setup__t_categorization_setup ON target_data.cm_ldsity2target2categorization_setup USING btree (categorization_setup);
COMMENT ON INDEX target_data.fki__cm_ldsity2target2categorization_setup__t_categorization_setup IS
'BTree index on foreign key fkey__cm_ldsity2target2categorization_setup__t_categorization_setup.';

/*
-- SEQUENCE
CREATE SEQUENCE target_data.cm_ldsity2target2categorization_setup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE target_data.cm_ldsity2target2categorization_setup OWNED BY target_data.cm_ldsity2target2categorization_setup.id;
ALTER TABLE ONLY target_data.cm_ldsity2target2categorization_setup ALTER COLUMN id SET DEFAULT nextval('target_data.cm_ldsity2target2categorization_setup_id_seq'::regclass);
*/

GRANT USAGE ON SEQUENCE target_data.cm_ldsity2target2categorization_setup_id_seq TO public;
GRANT USAGE ON SEQUENCE	target_data.cm_ldsity2target2categorization_setup_id_seq TO app_nfiesta;

SELECT pg_catalog.pg_extension_config_dump('target_data.cm_ldsity2target2categorization_setup', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.cm_ldsity2target2categorization_setup_id_seq', '');

GRANT SELECT ON TABLE target_data.cm_ldsity2target2categorization_setup TO app_nfiesta;
GRANT SELECT, USAGE ON SEQUENCE target_data.cm_ldsity2target2categorization_setup_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE target_data.cm_ldsity2target2categorization_setup TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE target_data.cm_ldsity2target2categorization_setup_id_seq TO app_nfiesta_mng;
---------------------------------------------------------------------


---------------------------------------------------------------------
-- changes for table t_available_datasets
---------------------------------------------------------------------
ALTER TABLE target_data.t_available_datasets DROP CONSTRAINT fkey__t_available_datasets__c_target_variable;
DROP INDEX target_data.fki__t_available_datasets__c_target_variable;
ALTER TABLE target_data.t_available_datasets DROP COLUMN target_variable;
ALTER TABLE target_data.t_available_datasets DROP COLUMN area_domain_category;
ALTER TABLE target_data.t_available_datasets DROP COLUMN sub_population_category;

ALTER TABLE target_data.t_available_datasets ADD COLUMN categorization_setup integer;
COMMENT ON COLUMN target_data.t_available_datasets.categorization_setup IS 'Identifier of the categorization_setup, foreing key to table t_categorization_setup.';

ALTER TABLE target_data.t_available_datasets ADD CONSTRAINT
fkey__t_available_datasets__t_categorization_setup FOREIGN KEY (categorization_setup)
REFERENCES target_data.t_categorization_setup (id);
COMMENT ON CONSTRAINT fkey__t_available_datasets__t_categorization_setup ON target_data.t_available_datasets IS
'Foreign key to table t_categorization_setup.';

CREATE INDEX fki__t_available_datasets__t_categorization_setup ON target_data.t_available_datasets USING btree (categorization_setup);
COMMENT ON INDEX target_data.fki__t_available_datasets__t_categorization_setup IS
'BTree index on foreign key fkey__t_available_datasets__t_categorization_setup.';

ALTER TABLE target_data.t_available_datasets
  ADD CONSTRAINT ukey__t_available_datasets__panel__ref_year_set__categ_setup UNIQUE(panel, reference_year_set, categorization_setup);
COMMENT ON CONSTRAINT ukey__t_available_datasets__panel__ref_year_set__categ_setup ON target_data.t_available_datasets IS
'Unique identifier for panel, reference year set and categorization setup.';
---------------------------------------------------------------------

---------------------------------------------------------------------
-- changes for table c_ldsity_object
---------------------------------------------------------------------
ALTER TABLE target_data.c_ldsity_object ADD COLUMN filter text;
COMMENT ON COLUMN target_data.c_ldsity_object.filter IS 'Filter for ldsity object.';

ALTER TABLE target_data.c_ldsity_object DROP COLUMN condition_columns;
---------------------------------------------------------------------


---------------------------------------------------------------------
-- c_version
---------------------------------------------------------------------
CREATE TABLE target_data.c_version
(
  id integer NOT NULL,
  label varchar(200) NOT NULL,
  description text NOT NULL,
  label_en varchar(200),
  description_en text
);

ALTER TABLE target_data.c_version ADD CONSTRAINT pkey__c_version__id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_version__id ON target_data.c_version IS 'Primary key.';

COMMENT ON TABLE target_data.c_version IS '.';
COMMENT ON COLUMN target_data.c_version.id IS 'Identifier of the version.';
COMMENT ON COLUMN target_data.c_version.label IS 'Label of the version.';
COMMENT ON COLUMN target_data.c_version.description IS 'Description of the version.';
COMMENT ON COLUMN target_data.c_version.label_en IS 'English label of the version.';
COMMENT ON COLUMN target_data.c_version.description_en IS 'English description of the version.';

GRANT SELECT ON TABLE target_data.c_version TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE target_data.c_version TO app_nfiesta_mng;
---------------------------------------------------------------------


---------------------------------------------------------------------
-- changes for table cm_ldsity2target_variable
---------------------------------------------------------------------
ALTER TABLE target_data.cm_ldsity2target_variable ADD COLUMN use_negative boolean;
COMMENT ON COLUMN target_data.cm_ldsity2target_variable.use_negative IS 'Identifier of use negative.';

ALTER TABLE target_data.cm_ldsity2target_variable ADD COLUMN version integer;
COMMENT ON COLUMN target_data.cm_ldsity2target_variable.version IS 'Identifier of version.';

ALTER TABLE target_data.cm_ldsity2target_variable ADD CONSTRAINT
fkey__cm_ldsity2target_variable__c_version FOREIGN KEY (version)
REFERENCES target_data.c_version (id);
COMMENT ON CONSTRAINT fkey__cm_ldsity2target_variable__c_version ON target_data.cm_ldsity2target_variable IS
'Foreign key to table c_version.';

CREATE INDEX fki__cm_ldsity2target_variable__c_version ON target_data.cm_ldsity2target_variable USING btree (version);
COMMENT ON INDEX target_data.fki__cm_ldsity2target_variable__c_version IS
'BTree index on foreign key fkey__cm_ldsity2target_variable__c_version.';
---------------------------------------------------------------------


---------------------------------------------------------------------------------------------------;
-- Name: fn_import_data(character varying); Type: FUNCTION; Schema: target_data. Owner: -
---------------------------------------------------------------------------------------------------;
CREATE OR REPLACE FUNCTION target_data.fn_import_data(_schema character varying)
RETURNS text
AS
$$
DECLARE
		_res	text;
BEGIN
	-- Data for Name: c_ldsity_object; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity_object(id, label, description, table_name, upper_object, areal_or_population, column4upper_object, filter)
	select id, label, description, table_name, upper_object, areal_or_population, column4upper_object, filter from '||_schema||'.c_ldsity_object order by id';

	-- Data for Name: c_unit_of_measure; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_unit_of_measure(id, label, description)
	select id, label, description from '||_schema||'.c_unit_of_measure order by id';

	-- Data for Name: c_definition_variant; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_definition_variant(id, label, description)
	select id, label, description from '||_schema||'.c_definition_variant order by id';	

	-- Data for Name: c_area_domain; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain(id, label, description, /*ldsity_object,*/ array_order)
	select id, label, description, /*ldsity_object,*/ array_order from '||_schema||'.c_area_domain order by id';

	-- Data for Name: c_area_domain_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain_category(id, label, description, area_domain)
	select id, label, description, area_domain from '||_schema||'.c_area_domain_category order by id';

	-- Data for Name: cm_adc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_adc2classification_rule(area_domain_category, ldsity_object, classification_rule)
	select area_domain_category, ldsity_object, classification_rule from '||_schema||'.cm_adc2classification_rule order by id';	

	-- Data for Name: c_sub_population; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population(id, label, description, /*ldsity_object,*/ array_order)
	select id, label, description, /*ldsity_object,*/ array_order from '||_schema||'.c_sub_population order by id';

	-- Data for Name: c_sub_population_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population_category(id, label, description, sub_population)
	select id, label, description, sub_population from '||_schema||'.c_sub_population_category order by id';

	-- Data for Name: cm_spc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_spc2classification_rule(sub_population_category, ldsity_object, classification_rule)
	select sub_population_category, ldsity_object, classification_rule from '||_schema||'.cm_spc2classification_rule order by id';	

	-- Data for Name: c_ldsity; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity(id, label, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant)
	select id, label, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant from '||_schema||'.c_ldsity order by id';

	-- Data for Name: t_adc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_adc_hierarchy(variable, variable_superior)
	select variable, variable_superior from '||_schema||'.t_adc_hierarchy order by id';

	-- Data for Name: t_spc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_spc_hierarchy(variable, variable_superior)
	select variable, variable_superior from '||_schema||'.t_spc_hierarchy order by id';

  -- Data for Name: c_version; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_version(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_version order by id';

	-- Name: c_ldsity_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_ldsity_id_seq', (select max(id) from target_data.c_ldsity), true);


	_res := 'Import is comleted.';

	return _res;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_import_data(character varying) IS
'The function imports data from source tables into tables that are in the target_data schema.';
---------------------------------------------------------------------------------------------------;


DROP FUNCTION IF EXISTS target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer, integer[], integer[], integer) CASCADE;

---------------------------------------------------------------------------------------------------;
-- Name: fn_save_target_variable(character varying(200), text, boolean[], integer[], character varying(200), text, integer, integer[], integer[], integer); Type: FUNCTION; Schema: target_data. Owner: -
---------------------------------------------------------------------------------------------------;
CREATE OR REPLACE FUNCTION target_data.fn_save_target_variable
(
		_label character varying(200),
		_description text,
		_use_negative boolean[],
		_version integer[],
		_label_en character varying(200) DEFAULT NULL::varchar,
		_description_en text DEFAULT NULL::text,
		_state_or_change integer DEFAULT NULL::integer,
		_ldsity integer[] DEFAULT NULL::integer[],
		_ldsity_object_type integer[] DEFAULT NULL::integer[],
		_id integer DEFAULT NULL::integer
)
RETURNS integer
AS
$$
DECLARE
	_target_variable integer;
BEGIN
	IF _id IS NULL
	THEN
		IF _label IS NOT NULL AND _description IS NOT NULL AND _state_or_change IS NOT NULL AND _ldsity IS NOT NULL AND _use_negative IS NOT NULL AND _version IS NOT NULL
		THEN
			IF array_length(_ldsity,1) != array_length(_ldsity_object_type,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity, _ldsity_object_type;
			END IF;

			INSERT INTO target_data.c_target_variable(label, description, label_en, description_en, state_or_change)
			SELECT _label, _description, _label_en, _description_en, _state_or_change
			RETURNING id
			INTO _target_variable;

			INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category, use_negative, version)
			SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], use_negative, version
			FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id) ON t1.id = t2.id
			INNER JOIN unnest(_use_negative) WITH ORDINALITY AS t3(use_negative, id) ON t1.id = t3.id
			INNER JOIN unnest(_version) WITH ORDINALITY AS t4(version, id) ON t1.id = t4.id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%), state_or_change (%) and  must be not null!', _label, _description, _state_or_change;
		END IF;
	ELSE
		IF _ldsity IS NULL
		THEN
			UPDATE target_data.c_target_variable
			SET	label = _label,
				description = _description, 
				label_en = _label_en,
				description_en = _description_en,
				state_or_change = _state_or_change
			WHERE c_target_variable.id = _id;

			_target_variable := _id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing target variable and create new one with given ldsitys (%).', _ldsity;
		END IF;
	END IF;

	RETURN _target_variable;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_target_variable(character varying(200), text, boolean[], integer[], character varying(200), text, integer, integer[], integer[], integer) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_target_variable(character varying(200), text, boolean[], integer[], character varying(200), text, integer, integer[], integer[], integer) TO public;
---------------------------------------------------------------------------------------------------;