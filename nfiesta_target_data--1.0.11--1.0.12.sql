--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--adding label_en and description_en columns to c_area_domain and c_sub_population tables and removing array_order column from these tables
ALTER TABLE target_data.c_area_domain 
DROP COLUMN array_order, 
ADD COLUMN label_en character varying(200) UNIQUE,
ADD COLUMN description_en text UNIQUE;

COMMENT ON COLUMN target_data.c_area_domain.label_en IS 'English label of area domain.';
COMMENT ON COLUMN target_data.c_area_domain.description_en IS 'English description of area domain.';

ALTER TABLE target_data.c_sub_population
DROP COLUMN array_order, 
ADD COLUMN label_en character varying(200) UNIQUE,
ADD COLUMN description_en text UNIQUE;

COMMENT ON COLUMN target_data.c_sub_population.label_en IS 'English label of subpopulation.';
COMMENT ON COLUMN target_data.c_sub_population.description_en IS 'English description of subpopulation.';

----------------------------------------------
-- Functions
-----------------------------------------------
-- <function name="fn_import_data" schema="target_data" src="functions/fn_import_data.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_data(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_import_data(_schema character varying)
RETURNS text
AS
$$
DECLARE
		_res	text;
BEGIN
	-- Data for Name: c_ldsity_object; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity_object(id, label, description, table_name, upper_object, areal_or_population, column4upper_object, filter)
	select id, label, description, table_name, upper_object, areal_or_population, column4upper_object, filter from '||_schema||'.c_ldsity_object order by id';

	-- Data for Name: c_unit_of_measure; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_unit_of_measure(id, label, description)
	select id, label, description from '||_schema||'.c_unit_of_measure order by id';

	-- Data for Name: c_definition_variant; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_definition_variant(id, label, description)
	select id, label, description from '||_schema||'.c_definition_variant order by id';	

	-- Data for Name: c_area_domain; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain(id, label, description)
	select id, label, description from '||_schema||'.c_area_domain order by id';

	-- Data for Name: c_area_domain_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain_category(id, label, description, area_domain)
	select id, label, description, area_domain from '||_schema||'.c_area_domain_category order by id';

	-- Data for Name: cm_adc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_adc2classification_rule(area_domain_category, ldsity_object, classification_rule)
	select area_domain_category, ldsity_object, classification_rule from '||_schema||'.cm_adc2classification_rule order by id';	

	-- Data for Name: c_sub_population; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population(id, label, description)
	select id, label, description from '||_schema||'.c_sub_population order by id';

	-- Data for Name: c_sub_population_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population_category(id, label, description, sub_population)
	select id, label, description, sub_population from '||_schema||'.c_sub_population_category order by id';

	-- Data for Name: cm_spc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_spc2classification_rule(sub_population_category, ldsity_object, classification_rule)
	select sub_population_category, ldsity_object, classification_rule from '||_schema||'.cm_spc2classification_rule order by id';	

	-- Data for Name: c_ldsity; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity(id, label, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant)
	select id, label, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant from '||_schema||'.c_ldsity order by id';

	-- Data for Name: t_adc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_adc_hierarchy(variable_superior, variable)
	select variable_superior, variable from '||_schema||'.t_adc_hierarchy order by id';

	-- Data for Name: t_spc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_spc_hierarchy(variable_superior, variable)
	select variable_superior, variable from '||_schema||'.t_spc_hierarchy order by id';

	-- Data for Name: c_version; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_version(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_version order by id';	

	-- Name: c_ldsity_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_ldsity_id_seq', (select max(id) from target_data.c_ldsity), true);


	_res := 'Import is comleted.';

	return _res;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION sdesign.fn_import_data(character varying) IS
'The function imports data from source tables into tables that are in the target_data schema.';

-- </function>

-- <function name="fn_get_area_domain" schema="target_data" src="functions/fn_get_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_area_domain(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_area_domain(_id integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_area_domain AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_area_domain AS t1
		WHERE t1.id = _id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_area_domain(integer) IS
'Function returns records from c_area_domain table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_area_domain(integer) TO public;

-- </function>

-- <function name="fn_get_sub_population" schema="target_data" src="functions/fn_get_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_sub_population(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_sub_population(_id integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_sub_population AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given subpopulation does not exist in table c_sub_population (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_sub_population AS t1
		WHERE t1.id = _id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_sub_population(integer) IS
'Function returns records from c_sub_population table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_sub_population(integer) TO public;

-- </function>



