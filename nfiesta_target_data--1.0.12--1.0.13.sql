--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

----------------------------------------------
-- DDL
-----------------------------------------------
-- pg_extension_config_dump??

ALTER TABLE target_data.c_ldsity ADD COLUMN description text;
ALTER TABLE target_data.c_ldsity ADD COLUMN description_en text;

COMMENT ON COLUMN target_data.c_ldsity.description IS 'Detailed description of the category.';
COMMENT ON COLUMN target_data.c_ldsity.description_en IS 'Detailed English description of the category.';

ALTER TABLE target_data.c_ldsity ADD CONSTRAINT ukey__c_ldsity__label UNIQUE (label);
ALTER TABLE target_data.c_ldsity ADD CONSTRAINT ukey__c_ldsity__description UNIQUE (description);

ALTER TABLE target_data.c_area_domain ADD CONSTRAINT ukey__c_area_domain__label UNIQUE (label);
ALTER TABLE target_data.c_area_domain ADD CONSTRAINT ukey__c_area_domain__description UNIQUE (description);

ALTER TABLE target_data.c_sub_population ADD CONSTRAINT ukey__c_sub_population__label UNIQUE (label);
ALTER TABLE target_data.c_sub_population ADD CONSTRAINT ukey__c_sub_population__description UNIQUE (description);

ALTER TABLE target_data.c_area_domain_category ADD CONSTRAINT ukey__c_area_domain_category__label UNIQUE (label);
ALTER TABLE target_data.c_area_domain_category ADD CONSTRAINT ukey__c_area_domain_category__description UNIQUE (description);
ALTER TABLE target_data.c_area_domain_category ADD CONSTRAINT ukey__c_area_domain_category__label_en UNIQUE (label_en);
ALTER TABLE target_data.c_area_domain_category ADD CONSTRAINT ukey__c_area_domain_category__description_en UNIQUE (description_en);

ALTER TABLE target_data.c_sub_population_category ADD CONSTRAINT ukey__c_sub_population_category__label UNIQUE (label);
ALTER TABLE target_data.c_sub_population_category ADD CONSTRAINT ukey__c_sub_population_category__description UNIQUE (description);
ALTER TABLE target_data.c_sub_population_category ADD CONSTRAINT ukey__c_sub_population_category__label_en UNIQUE (label_en);
ALTER TABLE target_data.c_sub_population_category ADD CONSTRAINT ukey__c_sub_population_category__description_en UNIQUE (description_en);

ALTER TABLE target_data.c_ldsity_object ADD CONSTRAINT ukey__c_ldsity_object__label UNIQUE (label);
ALTER TABLE target_data.c_ldsity_object ADD CONSTRAINT ukey__c_ldsity_object__description UNIQUE (description);
ALTER TABLE target_data.c_ldsity_object ADD CONSTRAINT ukey__c_ldsity_object__label_en UNIQUE (label_en);
ALTER TABLE target_data.c_ldsity_object ADD CONSTRAINT ukey__c_ldsity_object__description_en UNIQUE (description_en);

ALTER TABLE target_data.c_version ADD CONSTRAINT ukey__c_version__label UNIQUE (label);
ALTER TABLE target_data.c_version ADD CONSTRAINT ukey__c_version__description UNIQUE (description);
ALTER TABLE target_data.c_version ADD CONSTRAINT ukey__c_version__label_en UNIQUE (label_en);
ALTER TABLE target_data.c_version ADD CONSTRAINT ukey__c_version__description_en UNIQUE (description_en);

ALTER TABLE target_data.c_unit_of_measure ADD CONSTRAINT ukey__c_unit_of_measure__label UNIQUE (label);
ALTER TABLE target_data.c_unit_of_measure ADD CONSTRAINT ukey__c_unit_of_measure__description UNIQUE (description);
ALTER TABLE target_data.c_unit_of_measure ADD CONSTRAINT ukey__c_unit_of_measure__label_en UNIQUE (label_en);
ALTER TABLE target_data.c_unit_of_measure ADD CONSTRAINT ukey__c_unit_of_measure__description_en UNIQUE (description_en);

ALTER TABLE target_data.c_definition_variant ADD CONSTRAINT ukey__c_definition_variant__label UNIQUE (label);
ALTER TABLE target_data.c_definition_variant ADD CONSTRAINT ukey__c_definition_variant__description UNIQUE (description);
ALTER TABLE target_data.c_definition_variant ADD CONSTRAINT ukey__c_definition_variant__label_en UNIQUE (label_en);
ALTER TABLE target_data.c_definition_variant ADD CONSTRAINT ukey__c_definition_variant__description_en UNIQUE (description_en);

----------------------------------------------
-- Functions
-----------------------------------------------

DROP FUNCTION IF EXISTS target_data.fn_save_indicator(character varying) CASCADE;
DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object(integer) CASCADE;
-- <function name="fn_get_ldsity_object" schema="target_data" src="functions/fn_get_ldsity_object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object()
RETURNS TABLE (
id			integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
areal_or_population	integer
)
AS
$$
BEGIN
	RETURN QUERY
	SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.table_name, t1.areal_or_population
	FROM target_data.c_ldsity_object AS t1;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object() IS
'Function returns records from c_ldsity_object table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object() TO public;

-- </function>

-- <function name="fn_get_ldsity_object4ld_object_group" schema="target_data" src="functions/fn_get_ldsity_object4ld_object_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object4ld_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object4ld_object_group(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object4ld_object_group(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
areal_or_population	integer
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t3.id AS ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, t1.table_name, t1.areal_or_population
		FROM target_data.c_ldsity_object AS t1
		INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
		ON t1.id = t2.ldsity_object
		INNER JOIN target_data.c_ldsity_object_group AS t3
		ON t2.ldsity_object_group = t3.id
		ORDER BY t1.id, t3.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		SELECT t1.id, t3.id AS ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, t1.table_name, t1.areal_or_population
		FROM target_data.c_ldsity_object AS t1
		INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
		ON t1.id = t2.ldsity_object
		INNER JOIN target_data.c_ldsity_object_group AS t3
		ON t2.ldsity_object_group = t3.id
		WHERE t3.id = _ldsity_object_group;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object4ld_object_group(integer) IS
'Function returns records from c_ldsity_object table, optionally for given topic.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object4ld_object_group(integer) TO public;

-- </function>

-- <function name="fn_get_unit_of_measure4ld_object_group" schema="target_data" src="functions/fn_get_unit_of_measure4ld_object_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_unit_of_measure4ld_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_unit_of_measure4ld_object_group(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_unit_of_measure4ld_object_group(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_unit_of_measure AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		WITH w_data AS (
			SELECT DISTINCT t1.id AS unit_of_measure, t4.ldsity_object
			FROM target_data.c_unit_of_measure AS t1
			INNER JOIN target_data.c_ldsity AS t2
			ON t1.id = t2.unit_of_measure
			INNER JOIN target_data.c_ldsity_object AS t3
			ON t2.ldsity_object = t3.id
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t4
			ON t3.id = t4.ldsity_object
			WHERE t4.ldsity_object_group = _ldsity_object_group
		), w_count AS (
			SELECT unit_of_measure, count(*) AS total
			FROM w_data
			GROUP BY unit_of_measure
		), w_objects AS (
			SELECT count(DISTINCT ldsity_object) AS total_objects
			FROM w_data
		), w_unit AS (
			SELECT 
				unit_of_measure
			FROM w_count AS t1
			INNER JOIN w_objects AS t2
			ON t1.total = t2.total_objects
		)			
		SELECT t1.id, _ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_unit_of_measure AS t1
		INNER JOIN w_unit AS t2
		ON t1.id = t2.unit_of_measure
		ORDER BY t1.id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_unit_of_measure4ld_object_group(integer) IS
'Function returns records from c_unit_of_measrue table, optionally for given ldsity object group.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_unit_of_measure4ld_object_group(integer) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_get_ldsity4object(integer) CASCADE;

-- <function name="fn_get_ldsity4object" schema="target_data" src="functions/fn_get_ldsity4object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity4object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity4object(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity4object(_ldsity_object integer DEFAULT NULL::integer, _unit_of_measure integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object		integer,
label			character varying(200),
label_en		character varying(200),
column_expression	text,
unit_of_measure		integer
)
AS
$$
BEGIN
	RETURN QUERY
	SELECT t1.id, t1.ldsity_object,
		t1.label, t1.label_en, t1.column_expression, t1.unit_of_measure 
	FROM target_data.c_ldsity AS t1
	LEFT JOIN target_data.c_ldsity_object AS t2
	ON t1.ldsity_object = t2.id
	WHERE
		CASE WHEN _ldsity_object IS NOT NULL THEN t2.id = _ldsity_object ELSE true END AND
		CASE WHEN _unit_of_measure IS NOT NULL THEN t1.unit_of_measure = _unit_of_measure ELSE true END;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity4object(integer, integer) IS
'Function returns records from c_ldsity table, optionally for given ldsity object and unit_of_measure.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity4object(integer, integer) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_save_target_variable(character varying(200), text, boolean[], integer[], character varying(200), text, integer, integer[], integer[]) CASCADE;

-- <function name="fn_save_target_variable" schema="target_data" src="functions/fn_save_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_target_variable
(
		_label character varying(200),
		_description text,
		_label_en character varying(200) DEFAULT NULL::varchar,
		_description_en text DEFAULT NULL::text,
		_ldsity integer[] DEFAULT NULL::integer[],
		_ldsity_object_type integer[] DEFAULT NULL::integer[],
		_version integer[] DEFAULT NULL::integer[],
		_id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
DECLARE
	_target_variable integer;
BEGIN
	IF _id IS NULL
	THEN
		IF _label IS NOT NULL AND _description IS NOT NULL AND _ldsity IS NOT NULL AND _version IS NOT NULL
		THEN
			IF array_length(_ldsity,1) != array_length(_ldsity_object_type,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity, _ldsity_object_type;
			END IF;

			IF array_length(_ldsity,1) != array_length(_version,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of versions (%) are not equal!', _ldsity, _version;
			END IF;

			INSERT INTO target_data.c_target_variable(label, description, label_en, description_en, state_or_change)
			SELECT _label, _description, _label_en, _description_en, 100
			RETURNING id
			INTO _target_variable;

			INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category, version, use_negative)
			SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], t3.version, false
			FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
			ON t1.id = t2.id
			INNER JOIN unnest(_version) WITH ORDINALITY AS t3(version, id)
			ON t1.id = t3.id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%) and local density contributions (%) must be not null!', _label, _description, _ldsity;
		END IF;
	ELSE
		IF _ldsity IS NULL
		THEN
			UPDATE target_data.c_target_variable
			SET	label = _label,
				description = _description, 
				label_en = _label_en,
				description_en = _description_en
			WHERE c_target_variable.id = _id;

			_target_variable := _id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing target variable and create new one with given ldsitys (%).', _ldsity;
		END IF;
	END IF;

	RETURN _target_variable;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer) TO public;

-- </function>

-- <function name="fn_save_change_target_variable" schema="target_data" src="functions/fn_save_change_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_change_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_change_target_variable(
	_label character varying(200), 
	_description text, 
	_label_en character varying(200) DEFAULT NULL::varchar, 
	_description_en text DEFAULT NULL::text, 
	_ldsity integer[] DEFAULT NULL::integer[], 
	_ldsity_object_type integer[] DEFAULT NULL::integer[], 
	_version integer[] DEFAULT NULL::integer[], 
	_ldsity_negat integer[] DEFAULT NULL::integer[], 
	_ldsity_object_type_negat integer[] DEFAULT NULL::integer[], 
	_version_negat integer[] DEFAULT NULL::integer[], 
	_id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
DECLARE
	_target_variable integer;
BEGIN
	IF _id IS NULL
	THEN
		IF _label IS NOT NULL AND _description IS NOT NULL AND (_ldsity IS NOT NULL OR _ldsity_negat IS NOT NULL)
		THEN
			IF array_length(_ldsity,1) != array_length(_ldsity_object_type,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity, _ldsity_object_type;
			END IF;

			IF array_length(_ldsity,1) != array_length(_version,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of versions (%) are not equal!', _ldsity, _version;
			END IF;

			IF array_length(_ldsity_negat,1) != array_length(_ldsity_object_type_negat,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions with negative value (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity_negat, _ldsity_object_type_negat;
			END IF;

			IF array_length(_ldsity_negat,1) != array_length(_version_negat,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions with negative value (%) and corresponding assignment of versions (%) are not equal!', _ldsity_negat, _version_negat;
			END IF;

			INSERT INTO target_data.c_target_variable(label, description, label_en, description_en, state_or_change)
			SELECT _label, _description, _label_en, _description_en, 200
			RETURNING id
			INTO _target_variable;

			INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category, version, use_negative)
			SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], t3.version, false
			FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
			ON t1.id = t2.id
			INNER JOIN unnest(_version) WITH ORDINALITY AS t3(version, id)
			ON t1.id = t3.id;

			INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category, version, use_negative)
			SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], t3.version, true
			FROM unnest(_ldsity_negat) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type_negat) WITH ORDINALITY AS t2(ldsity_object_type, id)
			ON t1.id = t2.id
			INNER JOIN unnest(_version_negat) WITH ORDINALITY AS t3(version, id)
			ON t1.id = t3.id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%), local density contributions of pozitive (%) or negative (%) value must be not null!', _label, _description, _ldsity, _ldsity_negat;
		END IF;
	ELSE
		IF _ldsity IS NULL
		THEN
			UPDATE target_data.c_target_variable
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_target_variable.id = _id;

			_target_variable := _id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing target variable and create new one with given ldsitys (%).', _ldsity;
		END IF;
	END IF;

	RETURN _target_variable;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) TO public;

-- </function>
