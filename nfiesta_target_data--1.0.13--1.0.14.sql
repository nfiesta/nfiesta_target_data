--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

----------------------------------------------
-- DDL
-----------------------------------------------
-- SEQUENCE
CREATE SEQUENCE target_data.c_version_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE target_data.c_version_id_seq OWNED BY target_data.c_version.id;
ALTER TABLE ONLY target_data.c_version ALTER COLUMN id SET DEFAULT nextval('target_data.c_version_id_seq'::regclass);

SELECT pg_catalog.pg_extension_config_dump('target_data.c_area_domain','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_area_domain_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_area_domain_category','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_area_domain_category_id_seq','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_version','');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_version_id_seq','');

CREATE TABLE target_data.cm_ldsity2panel_refyearset_version (
id 			serial,
ldsity			integer,
refyearset2panel	integer,
version			integer,
CONSTRAINT pkey__cm_ldsity2panel_refyearset_version__id PRIMARY KEY (id)
);

ALTER TABLE target_data.cm_ldsity2panel_refyearset_version ADD CONSTRAINT
fkey__cm_ldsity2panel_refyearset_version__c_ldsity FOREIGN KEY (ldsity)
REFERENCES target_data.c_ldsity(id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE target_data.cm_ldsity2panel_refyearset_version ADD CONSTRAINT
fkey__cm_ldsity2panel_refyearset_version__cm_refyearset2panel_mapping FOREIGN KEY (refyearset2panel)
REFERENCES sdesign.cm_refyearset2panel_mapping(id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE target_data.cm_ldsity2panel_refyearset_version ADD CONSTRAINT
fkey__cm_ldsity2panel_refyearset_version__c_version FOREIGN KEY (version)
REFERENCES target_data.c_version(id) ON UPDATE CASCADE ON DELETE NO ACTION;

CREATE INDEX fki__cm_ldsity2panel_refyearset_version__c_ldsity ON target_data.cm_ldsity2panel_refyearset_version USING btree (ldsity);
COMMENT ON INDEX target_data.fki__cm_ldsity2panel_refyearset_version__c_ldsity IS
'BTree index on foreign key fkey__cm_ldsity2panel_refyearset_version__c_ldsity.';

CREATE INDEX fki__cm_ldsity2panel_refyearset_version__cm_refyearset2panel_mapping ON target_data.cm_ldsity2panel_refyearset_version USING btree (refyearset2panel);
COMMENT ON INDEX target_data.fki__cm_ldsity2panel_refyearset_version__cm_refyearset2panel_mapping IS
'BTree index on foreign key fkey__cm_ldsity2panel_refyearset_version__cm_refyearset2panel_mapping.';

CREATE INDEX fki__cm_ldsity2panel_refyearset_version__c_version ON target_data.cm_ldsity2panel_refyearset_version USING btree (version);
COMMENT ON INDEX target_data.fki__cm_ldsity2panel_refyearset_version__c_version IS
'BTree index on foreign key fkey__cm_ldsity2panel_refyearset_version__c_version.';

COMMENT ON TABLE target_data.cm_ldsity2panel_refyearset_version IS 'Mapping table between local density contribution, panels and reference year sets and also versions. Its purpose is to model a spatial and time availability of the data according to their versions.';

COMMENT ON COLUMN target_data.cm_ldsity2panel_refyearset_version.id IS 'Primary key, identificator of the record.';
COMMENT ON COLUMN target_data.cm_ldsity2panel_refyearset_version.ldsity IS 'Identifier of the local density contribution, foreign key to table c_ldsity.';
COMMENT ON COLUMN target_data.cm_ldsity2panel_refyearset_version.refyearset2panel IS 'Identifier of the panel and reference year set combination, foreign key to table cm_refyearset2panel_mapping.';
COMMENT ON COLUMN target_data.cm_ldsity2panel_refyearset_version.version IS 'Identifier of the version, foreign key to table c_version.';

GRANT SELECT ON TABLE 				target_data.cm_ldsity2panel_refyearset_version TO PUBLIC;
GRANT SELECT, USAGE ON SEQUENCE 		target_data.cm_ldsity2panel_refyearset_version_id_seq TO PUBLIC;

GRANT SELECT ON TABLE				 target_data.cm_ldsity2panel_refyearset_version TO app_nfiesta;
GRANT SELECT, USAGE ON SEQUENCE			 target_data.cm_ldsity2panel_refyearset_version_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.cm_ldsity2panel_refyearset_version TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.cm_ldsity2panel_refyearset_version_id_seq TO app_nfiesta_mng;

-- missing documentation:
COMMENT ON COLUMN target_data.c_ldsity.column_expression IS 'Expression of local density contribution. It targets a specific column within a ldsity object table.';

COMMENT ON TABLE target_data.c_areal_or_population IS 'Lookup table coding if local density object has areal or population character.';
COMMENT ON TABLE target_data.c_ldsity IS 'Table with local density contributions - basiccaly a column within a local density object table.';
COMMENT ON TABLE target_data.c_ldsity_object IS 'Local density object (a table with columns - local density contributions). Basically a different object has a different sampling protocol.';
COMMENT ON TABLE target_data.c_ldsity_object_type IS 'Lookup table coding if the object (its contribution) is used to calculate the final local density at the plot level or is just used for attribute divisioning (the contribution has no effekt to the final density other than saying how much of it belongs into the specific category). E.g. crown projections are used to calculate the portion of coniferous and broadleaved forest area. A very concrete expamle could be local density at the plot level 1, broadleaved 0.4 and confiferous 0.6.';
COMMENT ON TABLE target_data.c_version IS 'Lookup table with versions. E.g. data before correction, data after correction because of revisited plot etc.';
COMMENT ON TABLE target_data.cm_adc2classification_rule IS 'Table with classification rules for area domain categories and each of local density object.';
COMMENT ON TABLE target_data.cm_spc2classification_rule IS 'Table with classification rules for sub population categories and each of local density object.';
COMMENT ON TABLE target_data.t_adc_hierarchy IS 'Table with hierarchy between area domain categories (e.g. when using aggregated land tenure categories and the most granular categories of land tenure next to each other, the natural parks can be seen only within a state owned category).';
COMMENT ON TABLE target_data.t_available_datasets IS 'Table with specified categorization setup (target variable and its area domain and sub population categories) for which the local densities are available for given panels and reference year sets.';
COMMENT ON TABLE target_data.t_categorization_setup IS 'Specific combination of target variable, area domain category, sub population category and possible variants of classification (which classification rule was used for each category even when more are available).';
COMMENT ON TABLE target_data.t_ldsity_values IS 'Calculated local densities at the plot level.';
COMMENT ON TABLE target_data.t_spc_hierarchy IS 'Table with hierarchy between sub population categories (e.g. when using coniferous/broadleaves categories and the most granular categories of species next to each other, the spruce can be seen only within a coniferous category).';

SELECT pg_catalog.pg_extension_config_dump('target_data.cm_ldsity2panel_refyearset_version','');
SELECT pg_catalog.pg_extension_config_dump('target_data.cm_ldsity2panel_refyearset_version_id_seq','');

ALTER TABLE target_data.c_ldsity DROP COLUMN panel_refyearset_group;

----------------------------------------------
-- Functions
-----------------------------------------------
-- <function name="fn_import_data" schema="target_data" src="functions/fn_import_data.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_data(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_import_data(_schema character varying)
RETURNS text
AS
$$
DECLARE
		_res	text;
BEGIN
	-- Data for Name: c_ldsity_object; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity_object(id, label, description, table_name, upper_object, areal_or_population, column4upper_object, filter)
	select id, label, description, table_name, upper_object, areal_or_population, column4upper_object, filter from '||_schema||'.c_ldsity_object order by id';

	-- Data for Name: c_unit_of_measure; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_unit_of_measure(id, label, description)
	select id, label, description from '||_schema||'.c_unit_of_measure order by id';

	-- Data for Name: c_definition_variant; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_definition_variant(id, label, description)
	select id, label, description from '||_schema||'.c_definition_variant order by id';	

	-- Data for Name: c_area_domain; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain(id, label, description)
	select id, label, description from '||_schema||'.c_area_domain order by id';

	-- Data for Name: c_area_domain_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain_category(id, label, description, area_domain)
	select id, label, description, area_domain from '||_schema||'.c_area_domain_category order by id';

	-- Data for Name: cm_adc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_adc2classification_rule(area_domain_category, ldsity_object, classification_rule)
	select area_domain_category, ldsity_object, classification_rule from '||_schema||'.cm_adc2classification_rule order by id';	

	-- Data for Name: c_sub_population; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population(id, label, description)
	select id, label, description from '||_schema||'.c_sub_population order by id';

	-- Data for Name: c_sub_population_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population_category(id, label, description, sub_population)
	select id, label, description, sub_population from '||_schema||'.c_sub_population_category order by id';

	-- Data for Name: cm_spc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_spc2classification_rule(sub_population_category, ldsity_object, classification_rule)
	select sub_population_category, ldsity_object, classification_rule from '||_schema||'.cm_spc2classification_rule order by id';	

	-- Data for Name: c_ldsity; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity(id, label, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant)
	select id, label, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant from '||_schema||'.c_ldsity order by id';

	-- Data for Name: t_adc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_adc_hierarchy(variable_superior, variable)
	select variable_superior, variable from '||_schema||'.t_adc_hierarchy order by id';

	-- Data for Name: t_spc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_spc_hierarchy(variable_superior, variable)
	select variable_superior, variable from '||_schema||'.t_spc_hierarchy order by id';

	-- Data for Name: c_version; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_version(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_version order by id';	

	-- Data for Name: c_version; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_ldsity2panel_refyearset_version(ldsity, refyearset2panel, version)
	select ldsity, refyearset2panel, version from '||_schema||'.cm_ldsity2panel_refyearset_version order by id';	

	-- Name: c_ldsity_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_ldsity_id_seq', (select max(id) from target_data.c_ldsity), true);


	_res := 'Import is comleted.';

	return _res;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION sdesign.fn_import_data(character varying) IS
'The function imports data from source tables into tables that are in the target_data schema.';

-- </function>

-- <function name="fn_get_version" schema="target_data" src="functions/fn_get_version.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_version(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_version(_ldsity integer DEFAULT NULL::integer, _refyearset2panel integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity			integer,
refyearset2panel	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text
)
AS
$$
BEGIN
	IF _ldsity IS NULL
	THEN
		RETURN QUERY
		SELECT DISTINCT t1.id, NULL::integer, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_version AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
		END IF;

		IF _refyearset2panel IS NOT NULL
		THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
			THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
			END IF;

			RETURN QUERY
			SELECT DISTINCT t1.id, t2.ldsity, t2.refyearset2panel, t1.label, t1.description, t1.label_en, t1.description_en
			FROM target_data.c_version AS t1
			INNER JOIN target_data.cm_ldsity2panel_refyearset_version AS t2
			ON t1.id = t2.version
			WHERE t2.ldsity = _ldsity AND t2.refyearset2panel = _refyearset2panel;
		ELSE
			RETURN QUERY
			SELECT DISTINCT t1.id, t2.ldsity, t2.refyearset2panel, t1.label, t1.description, t1.label_en, t1.description_en
			FROM target_data.c_version AS t1
			INNER JOIN target_data.cm_ldsity2panel_refyearset_version AS t2
			ON t1.id = t2.version
			WHERE t2.ldsity = _ldsity;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_version(integer, integer) IS
'Function returns records from c_unit_of_measrue table, optionally for given ldsity.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_version(integer, integer) TO public;

-- </function>

-- <function name="fn_save_version" schema="target_data" src="functions/fn_save_version.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_version(integer, character varying, text, character varying, text, integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_version(_id integer, _label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text)
RETURNS integer
AS
$$
BEGIN
	IF _id IS NOT NULL 
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_version AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given version does not exist in table c_version (%)', _id;
		END IF;

		IF _label IS NOT NULL OR _description IS NOT NULL
		THEN
			UPDATE target_data.c_version
			SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
			WHERE c_version.id = _id;
		ELSE
			RAISE EXCEPTION 'At least label or description must be not null when doing an update of c_version (%, %).', _label, _description;
		END IF;
	ELSE
		RAISE EXCEPTION 'Parameter _id must not be null (%)!', _id;
	END IF;

	RETURN _version;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_version(integer, character varying, text, character varying, text) IS
'Function provides update in c_version table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_version(integer, character varying, text, character varying, text) TO public;

-- </function>

DROP FUNCTION target_data.fn_get_ldsity4object(integer,integer);

-- <function name="fn_get_ldsity4object" schema="target_data" src="functions/fn_get_ldsity4object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity4object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity4object(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity4object(_ldsity_object integer DEFAULT NULL::integer, _unit_of_measure integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object		integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
column_expression	text,
unit_of_measure		integer,
version			integer
)
AS
$$
BEGIN
	RETURN QUERY
	SELECT DISTINCT t1.id, t1.ldsity_object, t1.label, t1.description, t1.label_en, t1.description_en, t1.column_expression, t1.unit_of_measure, t3.version
	FROM target_data.c_ldsity AS t1
	LEFT JOIN target_data.c_ldsity_object AS t2
	ON t1.ldsity_object = t2.id
	LEFT JOIN target_data.cm_ldsity2panel_refyearset_version AS t3
	ON t1.id = t3.ldsity
	WHERE
		CASE WHEN _ldsity_object IS NOT NULL THEN t2.id = _ldsity_object ELSE true END AND
		CASE WHEN _unit_of_measure IS NOT NULL THEN t1.unit_of_measure = _unit_of_measure ELSE true END;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity4object(integer, integer) IS
'Function returns records from c_ldsity table, optionally for given ldsity object and unit_of_measure.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity4object(integer, integer) TO public;

-- </function>
