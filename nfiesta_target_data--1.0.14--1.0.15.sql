--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

----------------------------------------------
-- DDL
-----------------------------------------------


----------------------------------------------
-- Functions
-----------------------------------------------
-- <function name="fn_get_classification_rule4adc" schema="target_data" src="functions/fn_get_classification_rule4adc.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4adc
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4adc(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_classification_rule4adc(_area_domain_category integer DEFAULT NULL::integer, _ldsity_object integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
area_domain_category	integer,
ldsity_object		integer,
classification_rule	text
)
AS
$$
BEGIN
	IF _area_domain_category IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.area_domain_category, t1.ldsity_object, t1.classification_rule
		FROM target_data.cm_adc2classification_rule AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain_category AS t1 WHERE t1.id = _area_domain_category)
		THEN RAISE EXCEPTION 'Given area_domain_category does not exist in table c_area_domain_category (%)', _area_domain_category;
		END IF;

		IF _ldsity_object IS NOT NULL
		THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
			THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
			END IF;

			RETURN QUERY
			SELECT t4.id, t1.id AS area_domain_category, t2.ldsity_object, t2.classification_rule
			FROM target_data.c_area_domain_category AS t1
			LEFT JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.id = t2.area_domain_category
			WHERE t1.id = _area_domain_category AND t2.ldsity_object = _ldsity_object
			ORDER BY t2.id;
		ELSE
			RETURN QUERY
			SELECT t4.id, t1.id AS area_domain_category, t2.ldsity_object, t2.classification_rule
			FROM target_data.c_area_domain_category AS t1
			LEFT JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.id = t2.area_domain_category
			WHERE t1.id = _area_domain_category
			ORDER BY t2.id;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_classification_rule4adc(integer, integer) IS
'Function returns records from cm_adc2classification_rule table, optionally for given area_domain_category and/or ldsity object.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_classification_rule4adc(integer, integer) TO public;

-- </function>

-- <function name="fn_get_classification_rule4spc" schema="target_data" src="functions/fn_get_classification_rule4spc.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4spc
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4spc(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_classification_rule4spc(_sub_population_category integer DEFAULT NULL::integer, _ldsity_object integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
sub_population_category	integer,
ldsity_object		integer,
classification_rule	text
)
AS
$$
BEGIN
	IF _sub_population_category IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.sub_population_category, t1.ldsity_object, t1.classification_rule
		FROM target_data.cm_spc2classification_rule AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population_category AS t1 WHERE t1.id = _sub_population_category)
		THEN RAISE EXCEPTION 'Given sub_population_category does not exist in table c_sub_population_category (%)', _sub_population_category;
		END IF;

		IF _ldsity_object IS NOT NULL
		THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
			THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
			END IF;

			RETURN QUERY
			SELECT t4.id, t1.id AS sub_population_category, t2.ldsity_object, t2.classification_rule
			FROM target_data.c_sub_population_category AS t1
			LEFT JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.id = _sub_population_category AND t2.ldsity_object = _ldsity_object
			ORDER BY t2.id;
		ELSE
			RETURN QUERY
			SELECT t4.id, t1.id AS sub_population_category, t2.ldsity_object, t2.classification_rule
			FROM target_data.c_sub_population_category AS t1
			LEFT JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.id = _sub_population_category
			ORDER BY t2.id;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_classification_rule4spc(integer, integer) IS
'Function returns records from cm_spc2classification_rule table, optionally for given sub_population_category and/or ldsity object.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_classification_rule4spc(integer, integer) TO public;

-- </function>

-- <function name="fn_get_ldsity_object4adsp" schema="target_data" src="functions/fn_get_ldsity_object4adsp.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object4adsp
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object4adsp(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object4adsp(_ldsity_object_group integer, _areal_or_population integer)
RETURNS TABLE (
id			integer,
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
areal_or_population	integer
)
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
	THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
	END IF;

	RETURN QUERY
	WITH w_data AS (
		SELECT t1.ldsity_object, t1.ldsity_object_group 
		FROM target_data.cm_ld_object2ld_object_group AS t1
		INNER JOIN target_data.c_ldsity_object_group AS t2
		ON t1.ldsity_object_group = t2.id
		WHERE t2.id = _ldsity_object_group
	),
	w_hier AS (
		SELECT t1.ldsity_object, t1.ldsity_object_group, target_data.fn_get_ldsity_objects(array[t1.ldsity_object]) AS ldsity_objects
		FROM w_data AS t1
	)
	SELECT DISTINCT t3.id, t1.ldsity_object_group, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, t3.areal_or_population
	FROM w_hier AS t1,
		unnest(t1.ldsity_objects) AS t2(ldsity_object)
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	WHERE t3.areal_or_population = _areal_or_population;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object4adsp(integer, integer) IS
'Function returns records from c_ldsity_object table, optionally for given topic.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object4adsp(integer, integer) TO public;

-- </function>

-- <function name="fn_save_areal_or_pop" schema="target_data" src="functions/fn_save_areal_or_pop.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_areal_or_pop
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_areal_or_pop(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_areal_or_pop(_areal_or_population integer, _label varchar(200), _description text, _label_en varchar(200) DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text, _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
DECLARE
	_areal_or_pop integer;
BEGIN
	IF _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of label or description is null (%, %).', _label, _description;
	END IF; 

	IF _id IS NULl
	THEN
		CASE 
		WHEN _areal_or_population = 100 THEN
			INSERT INTO target_data.c_area_domain(label, description, label_en, description_en)
			SELECT _label, _description, _label_en, _description_en
			RETURNING id
			INTO _areal_or_pop;

		WHEN _areal_or_population = 200 THEN
			INSERT INTO target_data.c_sub_population(label, description, label_en, description_en)
			SELECT _label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	ELSE
		CASE WHEN _areal_or_population = 100 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _id;
			END IF;

			UPDATE target_data.c_area_domain
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_area_domain.id = _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _id;
			END IF;

			UPDATE target_data.c_sub_population_category
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_sub_population_category.id = _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_areal_or_pop(integer, character varying, text, character varying, text) IS
'Functions inserts a record into table c_area_domain or c_sub_population based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_areal_or_pop(integer, character varying, text, character varying, text) TO public;

-- </function>

-- <function name="fn_save_category" schema="target_data" src="functions/fn_save_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_category(integer, varchar, text, varchar, text, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_category(_areal_or_population integer, _label varchar(200), _description text, _label_en varchar(200), _description_en text, _parent integer DEFAULT NULL::integer, _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
BEGIN
	IF _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent identifier (%) or label/descriptioin is null (%, %)!', _parent, _label, _description;
	END IF; 

	IF _id IS NULL THEN

		IF _parent IS NULL
		THEN
			RAISE EXCEPTION 'Parameter of parent idenfier (id from c_area_domain/c_sub_population) is null!';
		END IF;

		CASE 
		WHEN _areal_or_population = 100 THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _parent)
			THEN RAISE EXCEPTION 'Given area_domain does not exist in table c_area_domain (%).', _parent;
			END IF;

			INSERT INTO target_data.c_area_domain_category(area_domain, label, description, label_en, description_en)
			SELECT 
				_parent,
				_label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _parent)
			THEN RAISE EXCEPTION 'Given sub_population does not exist in table c_sub_population (%).', _parent;
			END IF;

			INSERT INTO target_data.c_sub_population_category(sub_population, label, description, label_en, description_en)
			SELECT 
				_parent,
				_label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	ELSE
		CASE WHEN _areal_or_population = 100 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain_category AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain category does not exist in table c_area_domain_category (%)', _id;
			END IF;

			UPDATE target_data.c_area_domain_category
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_area_domain_category.id = _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain_category AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain category does not exist in table c_area_domain_category (%)', _id;
			END IF;

			UPDATE target_data.c_sub_population_category
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_sub_population_category.id = _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_category(integer, varchar(200), text, varchar(200), text, integer, integer) IS
'Functions inserts a record into table c_area_domain_category or c_sub_population_category based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_category(integer, varchar(200), text, varchar(200), text, integer, integer) TO public;

-- </function>

-- <function name="fn_save_categories" schema="target_data" src="functions/fn_save_categories.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categories
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_categories(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_categories(_parent integer, _areal_or_population integer, _label varchar(200)[], _description text[], _label_en varchar(200)[], _description_en text[])
RETURNS void
AS
$$
BEGIN
	IF _parent IS NULL OR _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent indicator (%) or label/descriptioin is null (%, %)!', _parent, _label, _description;
	END IF; 

	IF array_length(_label,1) != array_length(_description,1) OR
		array_length(_label_en,1) != array_length(_description_en,1)
	THEN
		RAISE EXCEPTION 'Given arrays of label and description (%,%) or label_en and description_en (%,%) must be of same length!', _label, _description, _label_en, _description_en;
	END IF;


	CASE 
	WHEN _areal_or_population = 100 THEN
		INSERT INTO target_data.c_area_domain_category(area_domain, label, description, label_en, description_en)
		SELECT 
			_parent,
			t1.label, t2.description, t3.label_en, t4.description_en
		FROM
			unnest(_label) WITH ORDINALITY AS t1(label, id)
		INNER JOIN
			unnest(_description) WITH ORDINALITY AS t2(description, id)
		ON t1.id = t2.id
		LEFT JOIN unnest(_label_en) WITH ORDINALITY AS t3(label_en, id)
		ON t1.id = t3.id
		LEFT JOIN unnest(_description_en) WITH ORDINALITY AS t4(description_en, id)
		ON t1.id  = t4.id;

	WHEN _areal_or_population = 200 THEN

		INSERT INTO target_data.c_sub_population_category(sub_population, label, description, label_en, description_en)
		SELECT 
			_parent,
			t1.label, t2.description, t3.label_en, t4.description_en
		FROM
			unnest(_label) WITH ORDINALITY AS t1(label, id)
		INNER JOIN
			unnest(_description) WITH ORDINALITY AS t2(description, id)
		ON t1.id = t2.id
		LEFT JOIN unnest(_label_en) WITH ORDINALITY AS t3(label_en, id)
		ON t1.id = t3.id
		LEFT JOIN unnest(_description_en) WITH ORDINALITY AS t4(description_en, id)
		ON t1.id  = t4.id;

	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;

	RETURN;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_categories(integer, integer, varchar(200)[], text[], varchar(200)[], text[]) IS
'Functions inserts a record into table c_area_domain_category or c_sub_population_category based on given parameters.';


-- </function>
