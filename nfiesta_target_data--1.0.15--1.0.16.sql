--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4category(integer[], integer[], integer[], integer[], integer[]);
DROP FUNCTION IF EXISTS target_data.fn_get_classification_rules4case(integer[], integer[], integer[], integer[], integer[]);
DROP FUNCTION IF EXISTS target_data.fn_get_classification_rules4categories(integer[], integer[], integer[], integer[], integer[]);
DROP FUNCTION IF EXISTS target_data.fn_get_plots(integer[], integer[]);
DROP FUNCTION IF EXISTS target_data.fn_import_ldsity_values(integer[], integer[], integer, integer[], integer[], integer[], integer[], double precision);


----------------------------------------------
-- Functions
-----------------------------------------------

-- <function name="fn_get_classification_rule_id4category" schema="target_data" src="functions/fn_get_classification_rule_id4category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule_id4category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule_id4category(integer[], integer[], integer[], integer[]) CASCADE;

create or replace function target_data.fn_get_classification_rule_id4category
(
	_area_domain_category		integer[],
	_area_domain_object			integer[],
	_sub_population_category 	integer[],
	_sub_population_object		integer[]
)
returns table
(
adc2classification_rule				integer[],
spc2classification_rule				integer[]
)
as
$$
declare
		_id_adc2classification_rule		integer[];
		_id_ad_i						integer;
		_id_spc2classification_rule		integer[];
		_id_sp_i						integer;
begin
		if	_area_domain_category is null
		then
			raise exception 'Error 01: fn_get_classification_rule_id4category: Input argument _area_domain_category must not be NULL!';
		end if;
		
		if	_sub_population_category is null
		then
			raise exception 'Error 02: fn_get_classification_rule_id4category: Input argument _sub_population_category must not be NULL!';
		end if;
	
		---------------------------------------------------------------------------------
		if _area_domain_category = array[0]
		then
			_id_adc2classification_rule := null::integer[];
		else
			for i in 1..array_length(_area_domain_category,1)
			loop
				select id from target_data.cm_adc2classification_rule
				where area_domain_category = _area_domain_category[i]
				and ldsity_object = _area_domain_object[i]
				into _id_ad_i;
			
				if _area_domain_category[i] is distinct from 0 and _id_ad_i is null
				then
					raise exception 'Error 03: fn_get_classification_rule_id4category: For _area_domain_category = % and _area_domain_object = % not found any record in table cm_adc2classification_rule',_area_domain_category[i],_area_domain_object[i];
				end if;
			
				if i = 1
				then
					if _id_ad_i is null
					then
						_id_adc2classification_rule := null::integer[];
					else
						_id_adc2classification_rule := array[_id_ad_i];
					end if;
				else
					if _id_ad_i is null
					then
						_id_adc2classification_rule := _id_adc2classification_rule;
					else
						_id_adc2classification_rule := _id_adc2classification_rule || array[_id_ad_i];
					end if;
				end if;
			
			end loop;
		end if;
		---------------------------------------------------------------------------------
		if _sub_population_category = array[0]
		then
			_id_spc2classification_rule := null::integer[];
		else
			for i in 1..array_length(_sub_population_category,1)
			loop
				select id from target_data.cm_spc2classification_rule
				where sub_population_category = _sub_population_category[i]
				and ldsity_object = _sub_population_object[i]
				into _id_sp_i;
			
				if _sub_population_category[i] is distinct from 0 and _id_sp_i is null
				then
					raise exception 'Error 04: fn_get_classification_rule_id4category: For _sub_population_category = % and _sub_population_object = % not found any record in table cm_spc2classification_rule',_sub_population_category[i],_sub_population_object[i];
				end if;			
			
				if i = 1
				then
					if _id_sp_i is null
					then
						_id_spc2classification_rule := null::integer[];
					else
						_id_spc2classification_rule := array[_id_sp_i];
					end if;
				else
					if _id_sp_i is null
					then
						_id_spc2classification_rule := _id_spc2classification_rule;
					else
						_id_spc2classification_rule := _id_spc2classification_rule || array[_id_sp_i];
					end if;
				end if;
			
			end loop;
		end if;
		---------------------------------------------------------------------------------

		return query select _id_adc2classification_rule, _id_spc2classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;		

comment on function target_data.fn_get_classification_rule_id4category(integer[], integer[], integer[], integer[]) is
'The function for the area domain or sub population category returns the database identifier of their classifiaction_rules.';

grant execute on function target_data.fn_get_classification_rule_id4category(integer[], integer[], integer[], integer[]) to public;
-- </function>


-- <function name="fn_get_classification_rules_id4categories" schema="target_data" src="functions/fn_get_classification_rules_id4categories.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rules_id4categories
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[]]) CASCADE;

create or replace function target_data.fn_get_classification_rules_id4categories
(
	_area_domain integer[],
	_area_domain_object integer[],
	_sub_population integer[],
	_sub_population_object integer[]
)
returns table
(
	area_domain_category integer[],
	sub_population_category integer[],
	adc2classification_rule integer[],
	spc2classification_rule integer[]
) as
$$
declare
		_string_ad_w1			text;
		_string_ad_w3			text;
		_string_columns_ad		text;
		_string_columns_ad_w3	text;
	
		_string_sp_w2			text;
		_string_sp_w3			text;
		_string_columns_sp		text;
		_string_columns_sp_w3	text;
	
		_query_res				text;
begin
		-------------------------------
		-------------------------------
		if
			_area_domain is null
		then
			_string_ad_w1 := 'select 0 as adc_1';
			_string_ad_w3 := 'array[adc_1] as area_domain_category';
		else		
			for i in 1..array_length(_area_domain,1)
			loop
					if i = 1
					then
						_string_columns_ad := concat('unnest(attr_',i,') as adc_',i);
						_string_columns_ad_w3 := concat('adc_',i);
					else
						_string_columns_ad := concat(_string_columns_ad,', ',concat('unnest(attr_',i,') as adc_',i));
						_string_columns_ad_w3 := concat(_string_columns_ad_w3,', adc_',i);
					end if;
			end loop;
		
			_string_ad_w1 := replace(replace(concat('select ',_string_columns_ad,' from target_data.fn_get_attribute_domain(''ad''::varchar,array[',_area_domain,'])'),'{',''),'}','');
			_string_ad_w3 := concat('array[',_string_columns_ad_w3,'] as area_domain_category');
		end if;
		-------------------------------
		-------------------------------
		
		-------------------------------
		-------------------------------
		if
			_sub_population is null
		then
			_string_sp_w2 := 'select 0 as spc_1';
			_string_sp_w3 := 'array[spc_1] as sub_population_category';
		else		
			for i in 1..array_length(_sub_population,1)
			loop
					if i = 1
					then
						_string_columns_sp := concat('unnest(attr_',i,') as spc_',i);
						_string_columns_sp_w3 := concat('spc_',i);
					else
						_string_columns_sp := concat(_string_columns_sp,', ',concat('unnest(attr_',i,') as spc_',i));
						_string_columns_sp_w3 := concat(_string_columns_sp_w3,', spc_',i);
					end if;
			end loop;
		
			_string_sp_w2 := replace(replace(concat('select ',_string_columns_sp,' from target_data.fn_get_attribute_domain(''sp''::varchar,array[',_sub_population,'])'),'{',''),'}','');
			_string_sp_w3 := concat('array[',_string_columns_sp_w3,'] as sub_population_category');
		end if;
		-------------------------------
		-------------------------------	

		_query_res := concat
		('
		with
		w1 as	(',_string_ad_w1,'),
		w2 as	(',_string_sp_w2,'),
		w3 as	(select ',_string_ad_w3,', ',_string_sp_w3,' from w1, w2),
		w4 as	(
				select
					area_domain_category,
					sub_population_category,
					(
					target_data.fn_get_classification_rule_id4category
						(
						area_domain_category,
						$1,
						sub_population_category,
						$2
						)
					) as res
				from
					w3
				)
		select
				area_domain_category,
				sub_population_category,
				(res).adc2classification_rule,
				(res).spc2classification_rule
		from
				w4 order by area_domain_category, sub_population_category;
		');
	
		return query execute ''||_query_res||'' using _area_domain_object, _sub_population_object;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[]) is
'The function for the area domain or sub population returns a combination of attribute classifications and database identifiers of their classifiaction_rules.';

grant execute on function target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[]) to public;
-- </function>


-- <function name="fn_get_category4classifiaction_rule_id" schema="target_data" src="functions/fn_get_category4classifiaction_rule_id.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_category4classifiaction_rule_id
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_category4classifiaction_rule_id(character varying, integer[]) CASCADE;

create or replace function target_data.fn_get_category4classifiaction_rule_id
(
	_classification_rule_type	character varying,
	_classification_rule_id		integer[]
)
returns integer[]
as
$$
declare
		_table_name		text;
		_column_name	text;
		_category_i		integer;
		_res			integer[];
begin
		if _classification_rule_type is null
		then
			raise exception 'Error 01: fn_get_category4classifiaction_rule_id: Input argument _classification_rule_type must not by NULL!';
		end if;
	
		if _classification_rule_id is null
		then
			raise exception 'Error 02: fn_get_category4classifiaction_rule_id: Input argument _classification_rule_id must not by NULL!';
		end if;
	
		if _classification_rule_type = 'adc' then _table_name := 'cm_adc2classification_rule'; _column_name := 'area_domain_category'; end if;
		if _classification_rule_type = 'spc' then _table_name := 'cm_spc2classification_rule'; _column_name := 'sub_population_category'; end if;
	
		for i in 1..array_length(_classification_rule_id,1)
		loop
			execute 'select '||_column_name||' from target_data.'||_table_name||' where id = $1'
			using _classification_rule_id[i]
			into _category_i;
		
			if _category_i is null
			then
				raise exception 'Error 03: fn_get_category4classifiaction_rule_id: For input arguments _classification_rule_type = % and _classifacation_rule_id = % not found record in table % !',_classification_rule_type, _classification_rule_id[i], _table_name;
			end if;
		
			if i = 1
			then
				_res := array[_category_i];
			else
				_res := _res || array[_category_i];
			end if;
		end loop;
	
	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_category4classifiaction_rule_id(character varying, integer[]) is
'The function gets area domain or sub population category for input classification rules.';

grant execute on function target_data.fn_get_category4classifiaction_rule_id(character varying, integer[]) to public;
-- </function>


-- <function name="fn_get_classification_rule4classification_rule_id" schema="target_data" src="functions/fn_get_classification_rule4classification_rule_id.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4classification_rule_id
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) CASCADE;

create or replace function target_data.fn_get_classification_rule4classification_rule_id
(
	_classification_rule_type	character varying,
	_classification_rule_id		integer[],
	_ldsity_objects				integer[]
)	
returns text
as
$$
declare
		_table_name								text;
		_classification_rule_i					text;	
		_ldsity_object_i						integer;
		_pozice_i								integer;
		_column_name_i_array					text[];
		_classification_rule_i_pozice			text;
		_classification_rule_4case_pozice		text;
		_category								integer[];
begin
		if _classification_rule_type is null
		then
			raise exception 'Error 01: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_type must not by NULL !';
		end if;
	
		if not(_classification_rule_type = any(array['adc','spc']))
		then
			raise exception 'Error 02: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_type must be "adc" or "spc" !';
		end if;	
	
		if _classification_rule_id is null
		then
			raise exception 'Error 03: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_id must not by NULL !';
		end if;
	
		if _ldsity_objects is null
		then
			raise exception 'Error 04: fn_get_classification_rule4classification_rule_id: Input argument _ldsity_objects must not by NULL !';
		end if;	
	
		if _classification_rule_type = 'adc' then _table_name := 'cm_adc2classification_rule'; end if;
		if _classification_rule_type = 'spc' then _table_name := 'cm_spc2classification_rule'; end if;	
	
		for i in 1..array_length(_classification_rule_id,1)
		loop
				execute '
				select
						case when classification_rule is null then ''true'' else classification_rule end as classification_rule,
						ldsity_object
				from
						target_data.'||_table_name||'
				where
						id = $1'
				using	_classification_rule_id[i]
				into	_classification_rule_i,
						_ldsity_object_i;

				_pozice_i := array_position(_ldsity_objects,_ldsity_object_i);
										
				select array_agg(column_name::text order by ordinal_position) from information_schema.columns
			 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_object_i)
			 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_object_i)
				into _column_name_i_array;
								
				for ii in 1..array_length(_column_name_i_array,1)
				loop					
					if ii = 1
					then
						_classification_rule_i_pozice := replace(
															_classification_rule_i,
															_column_name_i_array[ii],
															concat(_column_name_i_array[ii],'_',_pozice_i)
															);
					else
						_classification_rule_i_pozice := replace(
															_classification_rule_i_pozice,
															_column_name_i_array[ii],
															concat(_column_name_i_array[ii],'_',_pozice_i)
															);
					end if;
				end loop;

				
				if i = 1
				then
					_classification_rule_4case_pozice := _classification_rule_i_pozice;
				else
					_classification_rule_4case_pozice := concat(_classification_rule_4case_pozice,' and ',_classification_rule_i_pozice);
				end if;		
						
		end loop;
	
		_classification_rule_4case_pozice := concat('(',_classification_rule_4case_pozice,')');

		return _classification_rule_4case_pozice;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) is
'The function gets text of classification rules for their classification rule identifiers.';

grant execute on function target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) to public;
-- </function>


-- <function name="fn_get_classification_rules4categorization_setups" schema="target_data" src="functions/fn_get_classification_rules4categorization_setups.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rules4categorization_setups
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rules4categorization_setups(integer[], integer) CASCADE;

create or replace function target_data.fn_get_classification_rules4categorization_setups
(
	_categorization_setups	integer[],
	_ldsity2target_variable	integer
)
returns text
as
$$
declare
		_ldsity_object			integer;
		_ldsity_objects			integer[];
		_string4case_i			text;
		_string4case			text;
begin
		if _categorization_setups is null
		then
			raise exception 'Error 01: fn_get_classification_rules4categorization_setups: Input argument _categorization_setups must not by null !';
		end if;
	
		if _ldsity2target_variable is null
		then
			raise exception 'Error 02: fn_get_classification_rules4categorization_setups: Input argument _ldsity2target_variable must not by null !';
		end if;	
	
		select ldsity_object from target_data.c_ldsity 
		where id =	(
					select ldsity from target_data.cm_ldsity2target_variable
					where id = _ldsity2target_variable
					)
		into _ldsity_object;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_get_classification_rules4categorization_setups: For input argument _ldsity2target_variable = % not found any ldsity_object in table c_ldsity!',_ldsity2target_variable;
		end if;
	
		_ldsity_objects := target_data.fn_get_ldsity_objects(array[_ldsity_object]);	
	
	
		-- ordering values in argument _categorization_setups
		with
		w as	(
				select distinct t.res from (select unnest(_categorization_setups) as res) as t
				)
		select array_agg(w.res order by w.res) from w
		into _categorization_setups;	
	

		for i in 1..array_length(_categorization_setups,1)
		loop
			select
				case
					when adc2classification_rule is null and spc2classification_rule is null
					then 'when area_domain_category = array[0] and sub_population_category = array[0] then true'
					
					when adc2classification_rule is not null and spc2classification_rule is null
					then concat('when area_domain_category = array[',
								array_to_string(target_data.fn_get_category4classifiaction_rule_id('adc',adc2classification_rule),',',''),
								'] and sub_population_category = array[0] then case when '
								,target_data.fn_get_classification_rule4classification_rule_id('adc',adc2classification_rule,_ldsity_objects),
								' then true else false end'
								)
								
					when adc2classification_rule is null and spc2classification_rule is not null
					then concat('when area_domain_category = array[0] and sub_population_category = array[',
								array_to_string(target_data.fn_get_category4classifiaction_rule_id('spc',spc2classification_rule),',',''),
								'] then case when '
								,target_data.fn_get_classification_rule4classification_rule_id('spc',spc2classification_rule,_ldsity_objects),
								' then true else false end'
								)
					
					when adc2classification_rule is not null and spc2classification_rule is not null
					then concat('when area_domain_category = array[',
								array_to_string(target_data.fn_get_category4classifiaction_rule_id('adc',adc2classification_rule),',',''),
								'] and sub_population_category = array[',
								array_to_string(target_data.fn_get_category4classifiaction_rule_id('spc',spc2classification_rule),',',''),
								'] then case when ('
								,target_data.fn_get_classification_rule4classification_rule_id('adc',adc2classification_rule,_ldsity_objects),
								' and '
								,target_data.fn_get_classification_rule4classification_rule_id('spc',spc2classification_rule,_ldsity_objects),
								') then true else false end'
								)		
				end as string4case												
			from
					target_data.cm_ldsity2target2categorization_setup
			where
					categorization_setup = _categorization_setups[i]
			and
					ldsity2target_variable = _ldsity2target_variable
			into
					_string4case_i;
			
			if i = 1
			then
					_string4case := _string4case_i;
			else
					_string4case := concat(_string4case,' ',_string4case_i);
			end if;
		end loop;

		_string4case := concat('case ',_string4case,' end');

		return _string4case;	
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_classification_rules4categorization_setups(integer[], integer) is
'The function gets text of classification rules for input categorization setups.';

grant execute on function target_data.fn_get_classification_rules4categorization_setups(integer[], integer) to public;
-- </function>


-- <function name="fn_get_plots" schema="target_data" src="functions/fn_get_plots.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_get_plots
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_plots(integer[], integer[], integer, boolean) CASCADE;

create or replace function target_data.fn_get_plots
(
	_panels integer[],
	_reference_year_sets integer[],
	_state_or_change integer,
	_use_negative boolean
)
returns table
(
	gid integer,
	cluster_configuration integer,
	reference_year_set4join integer,
	reference_year_set integer,
	panel integer,
	stratum integer
)
as
$$
declare
		_reference_year_set_i		integer;
		_reference_year_set_i_orig	integer;
		_reference_year_sets4join	integer[];
		_reference_year_sets4group	integer[];
	
		_reference_year_set_i_case	text;
		_reference_year_sets4case	text;
		_query						text;
begin
		if _panels is null
		then
			raise exception 'Error 01: fn_get_plots: Input argument _panels must not be NULL !';
		end if;
	
		if _reference_year_sets is null
		then
			raise exception 'Error 02: fn_get_plots: Input argument _reference_year_sets must not be NULL !';
		end if;
	
		if _state_or_change is null
		then
			raise exception 'Error 03: fn_get_plots: Input argument _state_or_change must not be NULL !';
		end if;
	
		if _use_negative is null
		then
			raise exception 'Error 04: fn_get_plots: Input argument _use_negative must not be NULL !';
		end if;
	
		for i in 1..array_length(_reference_year_sets,1)
		loop
			case
				when _state_or_change = 100 and _use_negative = false
						then
							_reference_year_set_i := _reference_year_sets[i];
							_reference_year_set_i_orig := _reference_year_sets[i];
							_reference_year_set_i_case := null::text;
							
				when _state_or_change = 200 and _use_negative = true
						then
							_reference_year_set_i :=	(
														select reference_year_set_begin
														from sdesign.t_reference_year_set
														where id = _reference_year_sets[i]
														);
							_reference_year_set_i_orig := _reference_year_sets[i];
							
							_reference_year_set_i_case := concat('when w4.reference_year_set = ',_reference_year_set_i,' then ',_reference_year_sets[i]);
					
				when _state_or_change = 200 and _use_negative = false
						then
							_reference_year_set_i :=	(
														select reference_year_set_end
														from sdesign.t_reference_year_set
														where id = _reference_year_sets[i]
														);
							_reference_year_set_i_orig := _reference_year_sets[i];
							
							_reference_year_set_i_case := concat('when w4.reference_year_set = ',_reference_year_set_i,' then ',_reference_year_sets[i]);
					
				else raise exception 'Error 05: fn_get_plots: This variant is not implemented yet !';
			end case;
		

			if i = 1
			then
				_reference_year_sets4join := array[_reference_year_set_i];
				_reference_year_sets4group := array[_reference_year_set_i_orig];
			
				if _reference_year_set_i_case is null
				then
					_reference_year_sets4case := null::text;
				else
					_reference_year_sets4case := _reference_year_set_i_case;
				end if;
			else
				_reference_year_sets4join := _reference_year_sets4join || array[_reference_year_set_i];
				_reference_year_sets4group := _reference_year_sets4group || array[_reference_year_set_i_orig];
			
				if _reference_year_set_i_case is null
				then
					_reference_year_sets4case := null::text;
				else
					_reference_year_sets4case := _reference_year_sets4case || ' ' || _reference_year_set_i_case;
				end if;
			end if;
		

		end loop;
	
		if _reference_year_sets4case is null
		then
			_reference_year_sets4case := 'w4.reference_year_set';
		else
			_reference_year_sets4case := concat('case ',_reference_year_sets4case,' end');
		end if;
	

		_query := concat(
			'
			with
			w0 as	(
					select * from sdesign.cm_refyearset2panel_mapping where reference_year_set in (select unnest($2))
					)
			,w1 as	(
					select
							w0.reference_year_set,
							a.id as panel_id,
							a.stratum,
							a.cluster_configuration
					from
						w0
					inner join
						(
						select * from sdesign.t_panel where id in (select unnest($1))
						) as a
					on
						w0.panel = a.id
					)
			---------------------------------------------------------
			,w2 as	(
					select
						a.*, b.*, c.stratum
					from
						sdesign.t_cluster as a
					inner join 					
						(
						select panel, cluster from sdesign.cm_cluster2panel_mapping where panel in (select panel_id from w1)
						) as b
					on
						a.id = b.cluster
					
					inner join sdesign.t_panel as c on b.panel = c.id
					)
			---------------------------------------------------------			
			,w3 as	(
					select * from sdesign.cm_plot2cluster_config_mapping
					where cluster_configuration in
						(		
						select id from sdesign.t_cluster_configuration where id in
						(select distinct cluster_configuration from w1)
						)
					)
			---------------------------------------------------------
			,w4 as	(
					select * from sdesign.t_plot_measurement_dates where reference_year_set in (select unnest($2))
					)
			,w5 as	(
					select
							a.gid,
							w3.cluster_configuration,
							w4.reference_year_set as reference_year_set4join,
							w2.panel,
							w2.stratum,
							',_reference_year_sets4case,' as reference_year_set
					from sdesign.f_p_plot as a
					inner join w3 on a.gid = w3.plot
					inner join w4 on a.gid = w4.plot
					inner join w2 on a.cluster = w2.id
					)
			select
					gid,
					cluster_configuration,
					reference_year_set4join,
					reference_year_set,
					panel,
					stratum
			from
					w5 order by gid;
			');
	
		return query execute ''||_query||'' using _panels, _reference_year_sets4join;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_plots(integer[], integer[], integer, boolean) is
'The function for the specified list of panels and reference_year_sets returns a list of points for local densities.';

grant execute on function target_data.fn_get_plots(integer[], integer[], integer, boolean) to public;
-- </function>


-- <function name="fn_set_attribute_configurations" schema="target_data" src="functions/fn_set_attribute_configurations.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_set_attribute_configurations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_set_attribute_configurations(integer, integer[], integer[], integer[][], integer[], integer[][]) CASCADE;

create or replace function target_data.fn_set_attribute_configurations
(
	_target_variable		integer,
	_target_variable_cm		integer[],
	_area_domain			integer[],
	_area_domain_object		integer[][],
	_sub_population			integer[],
	_sub_population_object	integer[][]
)
returns table(categorization_setup integer)
as
$$
declare
		_array_id							integer[];
		_array_id4check						integer[];
		_lot_100							integer;
		_lot_200							integer;
		_variant							integer;	
		_area_domain4input					integer[];
		_area_domain_object4input			integer[];
		_sub_population4input				integer[];
		_sub_population_object4input		integer[];	
		_area_domain4input_text				text;
		_area_domain_object4input_text		text;
		_sub_population4input_text			text;
		_sub_population_object4input_text	text;
		_string4attr						text;
		_query_res							text;
		_s4ij_p1							text;
		_s4ij_p2							text;
		_s4ij_p3							text;
		_s4ij_p4							text;
		_s4ij_p5							text;
		_string4incomming					text;
		_string4inner_join					text;
		_array_id_200						integer[];
		_string_inner_join					text;
		_string_incomming					text;
		_s4da_p1							text;
		_s4da_p2							text;
		_s4da_p3							text;
		_s4da_p4							text;
		_s4ia_p1							text;
		_s4ia_p2							text;
		_s4ia_p3							text;
		_s4ia_p4							text;
		_s4result_p1						text;
		_s4result_p2						text;
		_string4database_agg				text;
		_string4incomming_agg				text;	
		_string4result						text;
		_string_check_and_insert			text;
		_res								text;
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_set_attribute_configurations: The input argument _target_variable must not be NULL !';
		end if;

		if _target_variable_cm is null
		then
			raise exception 'Error 02: fn_set_attribute_configurations: The input argument _target_variable_cm must not be NULL !';
		end if;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where target_variable = _target_variable
		into
				_array_id;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where id in (select unnest(_target_variable_cm))
		into
				_array_id4check;			

		if _array_id != _array_id4check
			then raise exception 'Error 03: fn_set_attribute_configurations: The internal argument _array_id = % and internal argument _array_id4check = % are not the same!',_array_id, _array_id4check;
		end if;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 04: fn_set_attribute_configurations: This variant is not implemented yet!';
		end if;

		for bc in 1..array_length(_array_id,1)
		loop	
			-----------------------------------------
			-----------------------------------------
			if _area_domain is null and _sub_population is null
			then
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is null
			then		
				with
				w1 as (select unnest(_area_domain_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _area_domain_object4input;			
			
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;
			
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			------
			if _area_domain is null and _sub_population is not null
			then		
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
			
				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is not null
			then
				with
				w1 as (select unnest(_area_domain_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _area_domain_object4input;				
			
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
	
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;			

				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			end if;
			-----------------------------------------
			-----------------------------------------
			
			if _area_domain4input is null
			then
				_area_domain4input_text := 'null::integer[]';
			else
				_area_domain4input_text := replace(replace(concat('array[',_area_domain4input,']'),'{',''),'}','');
			end if;
		
			if _area_domain_object4input is null
			then
				_area_domain_object4input_text := 'null::integer[]';
			else
				_area_domain_object4input_text := replace(replace(concat('array[',_area_domain_object4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population4input is null
			then
				_sub_population4input_text := 'null::integer[]';
			else
				_sub_population4input_text := replace(replace(concat('array[',_sub_population4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population_object4input is null
			then
				_sub_population_object4input_text := 'null::integer[]';
			else
				_sub_population_object4input_text := replace(replace(concat('array[',_sub_population_object4input,']'),'{',''),'}','');
			end if;
			
			_string4attr := concat('
			select
					area_domain_category,
					sub_population_category,
					adc2classification_rule,
					spc2classification_rule
			from
					target_data.fn_get_classification_rules_id4categories(',_area_domain4input_text,',',_area_domain_object4input_text,',',_sub_population4input_text,',',_sub_population_object4input_text,')
			');
		
			if bc = 1
			then
				_query_res := concat('with w_',bc,' as (',_string4attr,')');
			else
				_query_res := concat(_query_res,',w_',bc,' as (',_string4attr,')');
			end if;
		end loop;
		
		-------------------------------------------------------------
		-------------------------------------------------------------
		if _variant in (1,2)
		then
		
			_s4ij_p1 := 'select row_number() over() as group_id_poradi';
			_s4ij_p3 := ' from ';
		
			for bc in 1..array_length(_array_id,1)
			loop
				if bc = 1
				then
			
					_s4ij_p2 := concat('
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.area_domain_category
										,t',bc,'.sub_population_category
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'');
				
					_string4incomming := concat(
						'
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'
										on
											t1.area_domain_category = t',bc,'.area_domain_category and
											t1.sub_population_category = t',bc,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				end if;
			end loop;
			
				_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4;
		end if;		
		-------------------------------------------------------------
		if _variant in (3,4)
		then	
			_s4ij_p1 := concat(
				'
				select
					row_number() over() as group_id_poradi,
					a.*,
					b.*
				from
					(
					select
							',_array_id[1],' as ldsity2target_variable_',_array_id[1],',
							area_domain_category as area_domain_category_',_array_id[1],',
							adc2classification_rule as adc2classification_rule_',_array_id[1],',
							spc2classification_rule as spc2classification_rule_',_array_id[1],'
					from w_1
					) as a
				inner
				join
					(select ');
				
			_s4ij_p3 := ' from ';
		
			_s4ij_p5 := concat('
						) as b
					on
						a.area_domain_category_',_array_id[1],' = b.area_domain_category');
					
			_string4incomming := concat(
				'
				select
						1 as group_id,
						group_id_poradi,
						area_domain_category,
						sub_population_category,
						ldsity2target_variable_',_array_id[1],' as ldsity2target_variable,
						adc2classification_rule_',_array_id[1],' as adc2classification_rule,
						spc2classification_rule_',_array_id[1],' as spc2classification_rule
				from
						w_inner_join
				');					
				
			_array_id_200 := array_remove(_array_id, _array_id[1]);
			
			for bc_200 in 1..array_length(_array_id_200,1)
			loop
				if bc_200 = 1
				then				
					_s4ij_p2 := concat('
										t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.area_domain_category
										,t',bc_200,'.sub_population_category
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'');
				
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');				
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'
										on
											t1.area_domain_category = t',bc_200,'.area_domain_category and
											t1.sub_population_category = t',bc_200,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');
					
				end if;
			end loop;
			
			_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4 || _s4ij_p5;
		
		end if;
		-------------------------------------------------------------
		-------------------------------------------------------------
		_string_inner_join := concat(',w_inner_join as (',_string4inner_join,')');
		_string_incomming := concat(',w_incomming as (',_string4incomming,')');
		-------------------------------------------------------------
		
		-------------------------------------------------------------
		-------------------------------------------------------------
		_s4da_p1 := 'select t0.categorization_setup';
		_s4da_p3 := ' from (select distinct categorization_setup from w_database) as t0 ';

		_s4ia_p1 := 'select t0.group_id_poradi';
		_s4ia_p3 := ' from (select distinct group_id_poradi from w_incomming) as t0 ';
	
		_s4result_p1 :=	'
						select
								a.*,
								b.categorization_setup
						from
								w_incomming_agg as a
						inner
						join	w_database_agg as b
						
						on ';	
	

		for i in 1..array_length(_array_id,1)
		loop
			if i = 1
			then
				_s4da_p2 := concat('
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');
								
				_s4ia_p2 := concat('
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');								
								
				_s4da_p4 := concat(' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat('
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],' and
									((case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end)
									= (case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)) and
									((case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end)
									= (case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end))
								');
			else
				_s4da_p2 := concat(_s4da_p2,
									'
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');
								
				_s4ia_p2 := concat(_s4ia_p2,
									'
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');								
								
				_s4da_p4 := concat(_s4da_p4,
									' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(_s4ia_p4,
									' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat(_s4result_p2,' and 
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],' and
									((case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end)
									= (case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)) and
									((case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end)
									= (case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end))
								');
			end if;
		end loop;
	
		_string4database_agg := _s4da_p1 || _s4da_p2 || _s4da_p3 || _s4da_p4;
		_string4incomming_agg := _s4ia_p1 || _s4ia_p2 || _s4ia_p3 || _s4ia_p4;
		_string4result := _s4result_p1 || _s4result_p2;
		-------------------------------------------------------------
		
		_string_check_and_insert := concat(
		'
		,w_check_1 as		(
							select
									a.*,
									b.id_cm_setup,
									b.categorization_setup		
							from
									w_incomming as a
							left
							join	(
									select
											id as id_cm_setup,
											ldsity2target_variable,
											adc2classification_rule,
											spc2classification_rule,
											categorization_setup
									from
											target_data.cm_ldsity2target2categorization_setup
									where
										ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
									) as b
							on
									a.ldsity2target_variable = b.ldsity2target_variable and			
									((case when a.adc2classification_rule is null then array[0] else a.adc2classification_rule end)
									= (case when b.adc2classification_rule is null then array[0] else b.adc2classification_rule end)) and
									((case when a.spc2classification_rule is null then array[0] else a.spc2classification_rule end)
									= (case when b.spc2classification_rule is null then array[0] else b.spc2classification_rule end))				
							)
		,w_check_2 as		(
							select
									w_check_1.*,
									case
										when categorization_setup is null
											then
												case
													when	(
															select min(t.group_id) from w_check_1 as t
															where t.categorization_setup is null
															and t.group_id_poradi = w_check_1.group_id_poradi
															)
															= 1
													then 0
													else 1
												end 
											else 0
									end as check_exists_group_id_1
							from
									w_check_1
							)
		,w_check_3 as		(
							select * from w_check_2 union all
							select distinct c.group_id, c.group_id_poradi, c.area_domain_category,
							c.sub_population_category, c.ldsity2target_variable,
							c.adc2classification_rule, c.spc2classification_rule,
							c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
							from
									(
									select
											1 as group_id,
											group_id_poradi,
											area_domain_category,
											sub_population_category,
											',_array_id[1],' as ldsity2target_variable,
											adc2classification_rule,
											null::integer[] as spc2classification_rule,
											null::integer as id_cm_setup,
											null::integer as categorization_setup,
											check_exists_group_id_1
									from
											w_check_2 where check_exists_group_id_1 = 1
									) as c
							)
		,w_check_4 as		(					
							select
									(select coalesce(max(id),0) from target_data.t_categorization_setup) + 
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as new_categorization_setup,
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as pozice,
									w_check_3.*
							from
									w_check_3
							where
									categorization_setup is null
							order
									by new_categorization_setup, pozice, group_id
							)
		,w_insert_1 as		(
							insert into target_data.t_categorization_setup(id)
							select distinct new_categorization_setup from w_check_4 order by new_categorization_setup
							returning id
							)
		,w_insert_2 as		(
							insert into target_data.cm_ldsity2target2categorization_setup
							(ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup)
							select ldsity2target_variable,adc2classification_rule,spc2classification_rule,new_categorization_setup
							from w_check_4
							returning id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
							)
		,w_database as		(
							select id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
							from target_data.cm_ldsity2target2categorization_setup where ldsity2target_variable in
							(select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
							union all
							select id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
							from w_insert_2
							)
		,w_database_agg as	('||_string4database_agg||')
		,w_incomming_agg as	('||_string4incomming_agg||')
		,w_result as		('||_string4result||')
		select
			categorization_setup
		from
			w_result order by categorization_setup;
		');	
		
		_res := _query_res || _string_inner_join || _string_incomming || _string_check_and_insert;
		
		return query execute ''||_res||'';
end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_set_attribute_configurations(integer,integer[],integer[],integer[][],integer[],integer[][]) IS
'The function sets attribute configurations.';

grant execute on function target_data.fn_set_attribute_configurations(integer,integer[],integer[],integer[][],integer[],integer[][]) to public;
-- </function>


-- <function name="fn_import_ldsity_values" schema="target_data" src="functions/fn_import_ldsity_values.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_import_ldsity_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_ldsity_values(integer[], integer[], integer[], double precision) CASCADE;
	
create or replace function target_data.fn_import_ldsity_values
(
	_panels						integer[],
	_reference_year_sets		integer[],
	_categorization_setups		integer[],
	_threshold					double precision default 0.0
)
returns text
as
$$
declare
		_array_id									integer[];
		_array_target_variable						integer[];
		_target_variable							integer;
		_lot_100									integer;
		_lot_200									integer;
		_variant									integer;
		_tv_target_variable							integer;
		_ldsity										integer;
		_ldsity_object_type							integer;
		_tv_area_domain_category					integer[];
		_tv_sub_population_category					integer[];
		_tv_definition_variant						integer[];
		_tv_use_negative							boolean;
		_tv_version									integer;
		_state_or_change							integer;
		_ldsity_ldsity_object						integer;
		_ldsity_column_expresion					text;
		_ldsity_unit_of_measure						integer;
		_ldsity_area_domain_category				integer[];
		_ldsity_sub_population_category				integer[];
		_ldsity_definition_variant					integer[];	
		_area_domain_category						integer[];
		_sub_population_category					integer[];
		_definition_variant							integer[];	
		_area_domain_category_i						integer[];
		_area_domain_category_t						text[];	
		_sub_population_category_i					integer[];
		_sub_population_category_t					text[];
		_condition_ids								integer[];
		_condition_types							text[];
		_example_query_1							text;
		_example_query								text;
		_ldsity_objects								integer[];
		_filter_i									text;
		_filters_array								text[];	
		_example_query_i							text;
		_example_query_array						text[];
		_query_res									text;
		_column_name_i_array						text[];	
		_ii_column_new_text							text;
		_pozice_i									integer;
		_classification_rule_i						text;	
		_pozice_array								integer[];
		_classification_rule_array					text[];	
		_position_groups							integer[];
		_classification_rule_upr_ita_pozice			text[];
		_classification_rule_ita_pozice				text;
		_ldsity_objects_without_conditions			integer[];
		_string4columns								text;
		_string4inner_joins							text;	
		_position4join								integer;
		_table_name4join							text;
		_pkey_column4join							text;
		_i_column4join								text;
		_string4attr								text;
		_string4case								text;
		_position4ldsity							integer;
		_string4ads									text;
		_query_res_big								text;
		_string_var									text;	
		_string_result_1							text;
		_string_insert_1							text;	
		_id_cat_setups_array						integer[];
		_check_bez_rozliseni						integer[];
		_check_bez_rozliseni_boolean				boolean[];	
		_string_var_100								text;
		_array_id_200								integer[];	
		_string_var_200								text;
		_string_check_insert						text;	
		_max_id_tlv									integer;
		_res										text;	
begin
		if _panels is null
		then
			raise exception 'Error 01: fn_import_ldsity_values: The input argument _panels must not be NULL !';
		end if;
	
		if _reference_year_sets is null
		then
			raise exception 'Error 02: fn_import_ldsity_values: The input argument _reference_year_sets must not be NULL !';
		end if;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_import_ldsity_values: The input argument _categorization_setups must not be NULL !';
		end if;
	
		if	(
			select count(t.res) > 0 from
			(select unnest(_categorization_setups) as res) as t
			where t.res is null
			)
		then
			raise exception 'Error 04: fn_import_ldsity_values: The input argument _categorization_setups must not contains NULL value!';
		end if;
	
		
		select
				array_agg(id order by ldsity_object_type, id) as id,
				array_agg(target_variable order by ldsity_object_type, id) as target_variable
		from
				target_data.cm_ldsity2target_variable
		where
				id in	(
						select ldsity2target_variable from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						)
		into
				_array_id,
				_array_target_variable;
		
		if _array_id is null
		then
			raise exception 'Error 05: fn_import_ldsity_values: For input values in argument _categorization_setups = % not found any record in table cm_ldsity2target_variable!',_categorization_setups;
		end if;
	
		_target_variable := (select distinct t.res from (select unnest(_array_target_variable) as res) as t);
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 06: fn_import_ldsity_values: This variant is not implemented yet!';
		end if;
	
		---------------------
		---------------------
		for bc in 1..array_length(_array_id,1)
		loop
			-- datas from table cm_ldsity2target_variable
			select
					target_variable,
					ldsity,
					ldsity_object_type,
					area_domain_category,
					sub_population_category,
					definition_variant,
					use_negative,
					version
			from
					target_data.cm_ldsity2target_variable
			where
					id = _array_id[bc]
			into
					_tv_target_variable,
					_ldsity,
					_ldsity_object_type,
					_tv_area_domain_category,
					_tv_sub_population_category,
					_tv_definition_variant,
					_tv_use_negative,
					_tv_version;
				
			-- datea from c_target_variable	
			select state_or_change from target_data.c_target_variable where id = _tv_target_variable
			into _state_or_change;
	
			-- datas from table c_ldsity
			select
					ldsity_object,
					column_expression,
					unit_of_measure,
					area_domain_category,
					sub_population_category,
					definition_variant
			from
					target_data.c_ldsity where id = _ldsity
			into
					_ldsity_ldsity_object,
					_ldsity_column_expresion,
					_ldsity_unit_of_measure,
					_ldsity_area_domain_category,
					_ldsity_sub_population_category,
					_ldsity_definition_variant;
							 
			_area_domain_category := _ldsity_area_domain_category || _tv_area_domain_category;
			_sub_population_category := _ldsity_sub_population_category || _tv_sub_population_category;
			_definition_variant := _ldsity_definition_variant || _tv_definition_variant;
	
			-----------------------------------------
			if _area_domain_category is not null
			then
				for i in 1..array_length(_area_domain_category,1)
				loop
					if i = 1
					then
						_area_domain_category_i := array[_area_domain_category[i]];
						_area_domain_category_t := array['ad'];
					else
						_area_domain_category_i := _area_domain_category_i || array[_area_domain_category[i]];
						_area_domain_category_t := _area_domain_category_t || array['ad'];
					end if;
				end loop;
			else
				_area_domain_category_i := null::integer[];
				_area_domain_category_t := null::text[];
			end if;
			-----------------------------------------
			-----------------------------------------
			if _sub_population_category is not null
			then
				for i in 1..array_length(_sub_population_category,1)
				loop
					if i = 1
					then
						_sub_population_category_i := array[_sub_population_category[i]];
						_sub_population_category_t := array['sp'];
					else
						_sub_population_category_i := _sub_population_category_i || array[_sub_population_category[i]];
						_sub_population_category_t := _sub_population_category_t || array['sp'];
					end if;
				end loop;
			else
				_sub_population_category_i := null::integer[];
				_sub_population_category_t := null::text[];
			end if;
			-----------------------------------------
			-----------------------------------------
	
			_condition_ids := _area_domain_category_i || _sub_population_category_i;
			_condition_types := _area_domain_category_t || _sub_population_category_t;
	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if _condition_ids is null
			then
				_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_panels,'],array[',_reference_year_sets,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');
				_example_query := concat('w_&i& as (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#)');
			else
				_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_panels,'],array[',_reference_year_sets,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');
				_example_query := concat('w_&i& as (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#)');
			end if;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_ldsity_objects := target_data.fn_get_ldsity_objects(array[_ldsity_ldsity_object]);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					select case when filter is null then 'true' else filter end
					from target_data.c_ldsity_object
					where id = _ldsity_objects[i]
					into _filter_i;
				
					if i = 1
					then
						_filters_array := array[_filter_i];
					else
						_filters_array := _filters_array || array[_filter_i];
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					if _ldsity_objects[i] = 100
					then
						_example_query_i := _example_query_1;
					else
						_example_query_i := _example_query;
					end if;
				
					_example_query_i := replace(_example_query_i,'&i&'::text,i::text);
					_example_query_i := replace(_example_query_i,'#con4filter#',(select case when filter is null then 'true' else filter end from target_data.c_ldsity_object where id = _ldsity_objects[i]));
					_example_query_i := replace(_example_query_i,concat('#table_name#_',i),(select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]));				
				
					if i = 1
					then
						_example_query_array := array[_example_query_i];
					else
						_example_query_array := _example_query_array || _example_query_i;
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					if i = 1
					then
						_query_res := concat('with ',_example_query_array[i]);
					else
						_query_res := _query_res || concat(',',_example_query_array[i]);
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
				select array_agg(column_name order by ordinal_position) from information_schema.columns
			 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
			 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
				into _column_name_i_array;
				
				for ii in 1..array_length(_column_name_i_array,1)
				loop
					if ii = 1
					then
						_ii_column_new_text := concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i);
					else
						_ii_column_new_text := concat(_ii_column_new_text,', ',concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i));
					end if;
				end loop;
			
				_query_res := replace(_query_res,concat('#column_names_',i::text,'#'),_ii_column_new_text);
				
			end loop;	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if _condition_ids is not null
			then
				for i in 1..array_length(_condition_ids,1)
				loop
					if _condition_types[i] = 'ad'
					then			
						select array_position(_ldsity_objects,
						(select ldsity_object from target_data.cm_adc2classification_rule where id = _condition_ids[i]))
						into _pozice_i;
					
						if _pozice_i is null then raise exception 'Error 07: fn_import_ldsity_values: For internal variable _condition_ids[i] = % not found position in internal variable _ldsity_objects = % !',_condition_ids[i],_ldsity_objects; end if;
								
						select case when classification_rule is null then 'true' else classification_rule end
						from target_data.cm_adc2classification_rule where id = _condition_ids[i]
						into _classification_rule_i;
					
						if _classification_rule_i is null then raise exception 'Error 08: fn_import_ldsity_values: The classification rule in the cm_adc2classification_rule table was not found for the internal variable _condition_ids[i] = %!,',_condition_ids[i]; end if;
					
					end if;
				
					if _condition_types[i] = 'sp'
					then			
						select array_position(_ldsity_objects,
						(select ldsity_object from target_data.cm_spc2classification_rule where id = _condition_ids[i]))
						into _pozice_i;				
					
						if _pozice_i is null then raise exception 'Error 09: fn_import_ldsity_values: For internal variable _condition_ids[i] = % not found position in internal variable _ldsity_objects = % !',_condition_ids[i],_ldsity_objects; end if;
							
						select case when classification_rule is null then 'true' else classification_rule end
						from target_data.cm_spc2classification_rule where id = _condition_ids[i]
						into _classification_rule_i;
					
						if _classification_rule_i is null then raise exception 'Error 10: fn_import_ldsity_values: The classification rule in the cm_spc2classification_rule table was not found for the internal variable _condition_ids[i] = %!,',_condition_ids[i]; end if;				
					
					end if;
				
					if i = 1
					then
						_pozice_array := array[_pozice_i];
						_classification_rule_array := array[_classification_rule_i];
					else
						_pozice_array := _pozice_array || array[_pozice_i];
						_classification_rule_array := _classification_rule_array || array[_classification_rule_i];
					end if;
					
				end loop;
					
				with
				w1 as	(select unnest(_pozice_array) as pozice)
				,w2 as 	(select distinct pozice from w1)
				select array_agg(pozice order by pozice) from w2
				into _position_groups;
			
				for i in 1..array_length(_position_groups,1)
				loop
					with
					w1 as	(
							select
							unnest(_pozice_array) as pozice,
							unnest(_classification_rule_array) as classification_rule
							)
					,w2 as	(
							select
									row_number() over () as new_id,
									pozice,
									classification_rule
							from w1
							)
					,w3 as	(
							select
									row_number() over (partition by pozice order by new_id) as new_id4order,
									w2.*
							from w2
							)
					select array_agg(classification_rule order by new_id4order) as classification_rule
					from w3 where pozice = _position_groups[i]
					into _classification_rule_upr_ita_pozice;
				
					for ii in 1..array_length(_classification_rule_upr_ita_pozice,1)
					loop
						if ii = 1
						then
							_classification_rule_ita_pozice := concat(_classification_rule_upr_ita_pozice[ii]);
						else
							_classification_rule_ita_pozice := concat(_classification_rule_ita_pozice,' and ',_classification_rule_upr_ita_pozice[ii]);
						end if;
					end loop;
				
					_query_res := replace(_query_res,concat('#conditions_',_position_groups[i],'#'),concat('(',_classification_rule_ita_pozice,')'));
				
				end loop;
			
				with
				w1 as	(select * from generate_series(1,array_length(_ldsity_objects,1)) as res)
				,w2 as	(select unnest(_position_groups) as res)
				,w3 as	(select res from w1 except select res from w2)
				select array_agg(res order by res) from w3
				into _ldsity_objects_without_conditions;
				
				if _ldsity_objects_without_conditions is not null
				then
					for i in 1..array_length(_ldsity_objects_without_conditions,1)
					loop
						_query_res := replace(_query_res,concat('#conditions_',_ldsity_objects_without_conditions[i],'#'),'(true)');
					end loop;
				end if;
						
			end if;	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
				if i = 1
				then
					_string4columns := concat(',w_res as (select w_',i,'.*');
					_string4inner_joins := concat(' from w_',i);
				else
					_string4columns := concat(_string4columns,', w_',i,'.*');
				
					select array_position(_ldsity_objects, 
						(select upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]))
					into _position4join;
					
					for i in 1..array_length(_ldsity_objects,1)
					loop
							if i = _position4join
							then
								_table_name4join := (select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]);
							end if;
					end loop;
				
					with
					w as	(
							select kcu.table_schema,
							       kcu.table_name,
							       tco.constraint_name,
							       kcu.ordinal_position as position,
							       kcu.column_name as key_column
							from information_schema.table_constraints tco
							join information_schema.key_column_usage kcu 
							     on kcu.constraint_name = tco.constraint_name
							     and kcu.constraint_schema = tco.constraint_schema
							     and kcu.constraint_name = tco.constraint_name
							where tco.constraint_type = 'PRIMARY KEY'
							order by kcu.table_schema,
							         kcu.table_name,
							         position
							 )
					select key_column::text from w
					where table_schema = (substring(_table_name4join from 1 for (position('.' in _table_name4join) - 1))) 
					and table_name = (substring(_table_name4join from (position('.' in _table_name4join) + 1) for length(_table_name4join)))
					into _pkey_column4join;
					
					select column4upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]
					into _i_column4join;
				
					_string4inner_joins :=
					concat(_string4inner_joins,' inner join w_',i,' on ','w_',_position4join,'.',_pkey_column4join,'_',_position4join,' = ','w_',i,'.',_i_column4join,'_',i);
							
				end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------		
			_query_res := _query_res || _string4columns || _string4inner_joins || ')';	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_string4attr := concat('
			select
					case
						when adc2classification_rule is null
								then array[0]
								else target_data.fn_get_category4classifiaction_rule_id(''adc'',adc2classification_rule)
					end as area_domain_category,
					case
						when spc2classification_rule is null
								then array[0]
								else target_data.fn_get_category4classifiaction_rule_id(''spc'',spc2classification_rule)
					end as sub_population_category,
					adc2classification_rule,
					spc2classification_rule,
					categorization_setup
			from
					target_data.cm_ldsity2target2categorization_setup
			where
					categorization_setup in (select unnest($1))
			and
					ldsity2target_variable = ',_array_id[bc],'
			');
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_string4case := target_data.fn_get_classification_rules4categorization_setups(_categorization_setups,_array_id[bc]);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_position4ldsity := array_position(_ldsity_objects,_ldsity_ldsity_object);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_string4ads := concat('
			,w_strings as	('||_string4attr||')
			,w_pre_adc as	(
							select
									w_strings.*,
									w_res.*
							from
									w_res, w_strings
							)
			,w_ads as		(
							select
									',_string4case,' as res_case,
									w_pre_adc.*
							from
									w_pre_adc
							)
			select
					res_case,
					area_domain_category,
					sub_population_category,
					adc2classification_rule,
					spc2classification_rule,
					(',case when _tv_use_negative = true then '(-1)' else '(1)' end,' * coalesce(',_ldsity_column_expresion,'_',_position4ldsity,',0.0)) as value,
					gid,
					cluster_configuration,
					reference_year_set4join,
					reference_year_set,
					panel,
					stratum,
					categorization_setup
			from
					w_ads
			');	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_query_res := _query_res || _string4ads;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if bc = 1
			then
				_query_res_big := concat('with bw_',bc,' as (',_query_res,')');
			else
				_query_res_big := concat(_query_res_big,',bw_',bc,' as (',_query_res,')');
			end if;
		end loop;		
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _variant in (1,2)
		then		
				for bc in 1..array_length(_array_id,1)
				loop
					if bc = 1
					then
						_string_var := concat(',w_union_all as (select ',_array_id[bc],' as ldsity2target_variable,* from bw_',bc,'');
					else
						_string_var := concat(_string_var,' union all select ',_array_id[bc],' as ldsity2target_variable,* from bw_',bc,'');
					end if;
				end loop;
			
				_string_var := concat(_string_var,')');
			
				_string_result_1 := concat(
				'
				,w_result_1 as		(
									select
											gid,
											panel,
											reference_year_set,
											categorization_setup,
											sum(value) as value
									from
											w_union_all
									where
											res_case = true
									group
											by gid, panel, reference_year_set, categorization_setup
									)
				');
			
				_string_insert_1 :=
				',w_insert_1 as		(
									insert into target_data.t_available_datasets(panel, reference_year_set, categorization_setup)
									select t.panel, t.reference_year_set, t.categorization_setup
									from (select distinct panel, reference_year_set, categorization_setup from w_union_all) as t
									except
									select panel, reference_year_set, categorization_setup from target_data.t_available_datasets
									where categorization_setup in (select unnest($1))
									order by categorization_setup, panel, reference_year_set				
									returning id, panel, reference_year_set, categorization_setup
									)';
		
		_query_res_big := _query_res_big || _string_var || _string_result_1 || _string_insert_1;
	
		end if;

		-----------------------------------------
		if _variant in (3,4)
		then
			for i in 1..array_length(_categorization_setups,1)
			loop
				select array_agg(id order by id)
				from target_data.cm_ldsity2target2categorization_setup
				where categorization_setup = _categorization_setups[i]
				into _id_cat_setups_array;
			
				for ii in 1..array_length(_id_cat_setups_array,1)
				loop
					if	(
						select count(*) = 1
						from target_data.cm_ldsity2target2categorization_setup
						where id = _id_cat_setups_array[ii]
						and (adc2classification_rule is null and spc2classification_rule is null)
						)
					then
						if ii = 1
						then
							_check_bez_rozliseni := array[1];
						else
							_check_bez_rozliseni := _check_bez_rozliseni || array[1];
						end if;
					else
						if ii = 1
						then
							_check_bez_rozliseni := array[0];
						else
							_check_bez_rozliseni := _check_bez_rozliseni || array[0];
						end if;
					end if;
				end loop;
			
				if		(
						select sum(t.res) = array_length(_id_cat_setups_array,1)
						from (select unnest(_check_bez_rozliseni) as res) as t
						)
				then
					if i = 1
					then
						_check_bez_rozliseni_boolean := array[true];
					else
						_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[true];
					end if;
				else
					if i = 1
					then
						_check_bez_rozliseni_boolean := array[false];
					else
						_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[false];
					end if;
				end if;	
			end loop;
		
			if	(
				select count(t.res) = 0
				from (select unnest(_check_bez_rozliseni_boolean) as res) as t
				where t.res = true
				)
			then
				with
				w1 as	(
						select * from target_data.cm_ldsity2target2categorization_setup
						where ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))
														)
						and (adc2classification_rule is null and spc2classification_rule is null)
						)
				,w2 as	(
						select categorization_setup, count(categorization_setup) as pocet
						from w1 group by categorization_setup
						)
				select array[categorization_setup] || _categorization_setups
				from w2 where pocet = (select max(pocet) from w2)
				into _categorization_setups;
			
			else
				_categorization_setups := _categorization_setups;
			end if;
		
			_string_var_100 := concat(',w_union_all_100 as (select ',_array_id[1],' as ldsity2target_variable,* from bw_1)');
		
			_array_id_200 := array_remove(_array_id, _array_id[1]);
				
			for bc_200 in 1..array_length(_array_id_200,1)
			loop
				if bc_200 = 1
				then
					_string_var_200 := concat(',w_union_all_200 as (select ',_array_id_200[bc_200],' as ldsity2target_variable,* from bw_',bc_200 + 1,'');
				else
					_string_var_200 := concat(_string_var_200,' union all select ',_array_id_200[bc_200],' as ldsity2target_variable,* from bw_',bc_200 + 1,'');
				end if;				
			end loop;
		
			_string_var_200 := concat(_string_var_200,')');
			
			_string_result_1 := concat(
			'
			,w_result_1 as		(
								select
										t.*,
										coalesce((t.value * coef_200.value4nasobeni),0.0) as value_res
								from
										(select * from w_union_all_100 where res_case = true and value is distinct from 0.0) as t
								inner
								join						
										(
										select
											t1.gid,
											t1.panel,
											t1.reference_year_set,
											t1.categorization_setup,
											t1.area_domain_category,
											t1.value,
											t2.value_sum,
											case when t2.value_sum = 0.0 then t2.value_sum else t1.value/t2.value_sum end as value4nasobeni
										from
											(
											select gid, panel, reference_year_set, categorization_setup, area_domain_category,
											sum(value) as value
											from w_union_all_200
											where res_case = true
											group by gid, panel, reference_year_set, categorization_setup, area_domain_category
											) as t1
										inner join 
											(
											select gid, panel, reference_year_set,
											sum(value) as value_sum
											from w_union_all_200
											where (area_domain_category = array[0] and sub_population_category = array[0])
											and res_case = true
											group by gid, panel, reference_year_set
											) as t2
										on
											t1.gid = t2.gid and
											t1.panel = t2.panel and
											t1.reference_year_set = t2.reference_year_set
										order
											by t1.gid
										) as coef_200
								on
										t.gid = coef_200.gid and
										t.categorization_setup = coef_200.categorization_setup and
										t.panel = coef_200.panel and
										t.reference_year_set = coef_200.reference_year_set
								)		
			');
		
			_string_insert_1 := 
			',w_insert_1 as		(
								insert into target_data.t_available_datasets(panel, reference_year_set, categorization_setup)
								select t2.panel, t2.reference_year_set, t2.categorization_setup
								from	(
										select distinct t1.panel, t1.reference_year_set, t1.categorization_setup
										from	(
												select distinct panel, reference_year_set, categorization_setup from w_union_all_100 union all
												select distinct panel, reference_year_set, categorization_setup from w_union_all_200
												) as t1
										) as t2
								except
								select panel, reference_year_set, categorization_setup from target_data.t_available_datasets
								where categorization_setup in (select unnest($1))
								order by categorization_setup, panel, reference_year_set				
								returning id, panel, reference_year_set, categorization_setup
								)';
		
			_query_res_big := _query_res_big || _string_var_100 || _string_var_200 || _string_result_1 || _string_insert_1;
		
		end if;
		-----------------------------------------
		-----------------------------------------
		_string_check_insert := concat('	
		,w_database_2 as	(
							select id, panel, reference_year_set, categorization_setup from target_data.t_available_datasets
							where categorization_setup in (select unnest($1))
							union all
							select id, panel, reference_year_set, categorization_setup from w_insert_1
							)
		,w_result_2 as		(
							select
									t1.gid as plot,
									t2.id as available_datasets,
									t1.value,
									now() as creation_time,
									true as is_latest,
									(SELECT extversion FROM pg_extension WHERE extname = ''nfiesta_target_data'') as ext_version
							from
									w_result_1 as t1
							inner
							join	w_database_2 as t2
							on
									t1.categorization_setup = t2.categorization_setup and
									t1.panel = t2.panel and
									t1.reference_year_set = t2.reference_year_set
							where
									t1.value is distinct from 0.0
							)
		,w_result_3 as		(
							select
									row_number() over() as new_id,
									w_result_2.plot,
									w_result_2.available_datasets,
									w_result_2.value,
									w_result_2.creation_time,
									w_result_2.is_latest,
									w_result_2.ext_version
							from
									w_result_2 where w_result_2.value is distinct from 0.0
							)
		,w_tlv as			(
							select
									tlv.id,
									tlv.plot,
									tlv.available_datasets,
									tlv.value,
									w_result_3.new_id,
									case
										when tlv.value = 0.0 and w_result_3.value = 0.0 then true
										when tlv.value = 0.0 and w_result_3.value is distinct from 0.0 then case when (abs(1 - (tlv.value / w_result_3.value)) * 100.0) <= ',_threshold,' then true else false end
										when tlv.value is distinct from 0.0 and w_result_3.value = 0.0 then case when (abs(1 - (w_result_3.value / tlv.value)) * 100.0) <= ',_threshold,' then true else false end
										else case when (abs(1 - (tlv.value / w_result_3.value)) * 100.0) <= ',_threshold,' then true else false end
									end as value_identic
							from
									target_data.t_ldsity_values as tlv
							inner
									join w_result_3
							on
									tlv.plot = w_result_3.plot and
									tlv.available_datasets = w_result_3.available_datasets
							where
									tlv.is_latest = true	
							)
		,w_update as		(
							update target_data.t_ldsity_values set is_latest = false where id in
							(select id from w_tlv where value_identic = false)	
							)
		insert into target_data.t_ldsity_values(plot,available_datasets,value,creation_time,is_latest,ext_version)
		select
				w_result_3.plot,
				w_result_3.available_datasets,
				w_result_3.value,
				w_result_3.creation_time,
				w_result_3.is_latest,
				w_result_3.ext_version
		from
				w_result_3
		where
				w_result_3.new_id not in (select new_id from w_tlv where value_identic = true)
		order
				by w_result_3.plot, w_result_3.available_datasets;
		');	
	
	_query_res_big := _query_res_big || _string_check_insert;
	
	---------------------------------------------
	---------------------------------------------
	select coalesce(max(id),0) from target_data.t_ldsity_values into _max_id_tlv;
	---------------------------------------------
	---------------------------------------------
	execute ''||_query_res_big||'' using _categorization_setups;
	---------------------------------------------
	---------------------------------------------
	_res := concat('The ',(select count(*) from target_data.t_ldsity_values where id > _max_id_tlv),' new local densities were prepared for ',(select count(t.plot) from (select distinct plot from target_data.t_ldsity_values where id > _max_id_tlv) as t),' plots.');
	---------------------------------------------
	---------------------------------------------
	return _res;
	---------------------------------------------
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_ldsity_values(integer[], integer[], integer[], double precision) IS
'The function for the specified list of input arguments inserts data into the t_available_datasets table and inserts data into the t_ldsity_values table (aggregated local density at the plot level).';

grant execute on function target_data.fn_import_ldsity_values(integer[], integer[], integer[], double precision) to public;
-- </function>
