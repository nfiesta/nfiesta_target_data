--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


----------------------------------------------
-- Functions
-----------------------------------------------

-- <function name="fn_get_attribute_domain" schema="target_data" src="functions/fn_get_attribute_domain.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_attribute_domain(varchar(2), integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_attribute_domain
(
	in _attr_type varchar(2), -- 'ad' or 'sp'
    IN _attr_types integer[],
    IN _attribute_domain integer[] DEFAULT NULL::integer[]
)
  RETURNS TABLE(attr_1 integer[], attr_2 integer[], attr_3 integer[], attr_4 integer[], attr_5 integer[]) AS
$BODY$
	DECLARE
		_at_count		integer;
		_attr_1			integer[];
		_attr_2			integer[];
		_attr_3			integer[];
		_attr_4			integer[];
		_attr_5			integer[];
	
		_table_name					text;
		_table_name_category		text;
		_column_name				text;
		_table_name_hierarchy		text;
	BEGIN

		if _attr_type is null
		then
			raise exception 'Error 01: fn_get_attribute_domain: The input argument _attr_type must not be NULL!';
		end if;
	
		if not(_attr_type = any(array['ad','sp']))
		then
			raise exception 'Error 02: fn_get_attribute_domain: Allowed values for the input argument _attr_type are "ad" or "sp" !';
		end if;
	
		if _attr_type = 'ad'
		then
			_table_name := 'c_area_domain';
			_table_name_category := 'c_area_domain_category';
			_column_name := 'area_domain';
			_table_name_hierarchy := 't_adc_hierarchy';
		end if;
	
		if _attr_type = 'sp'
		then
			_table_name := 'c_sub_population';
			_table_name_category := 'c_sub_population_category';
			_column_name := 'sub_population';
			_table_name_hierarchy := 't_spc_hierarchy';
		end if;

	CASE	WHEN _attr_types IS NULL THEN

			RAISE EXCEPTION 'Error 03: fn_get_attribute_domain: The function cannot be called without selling at least one attribute type!';

		WHEN array_length(_attr_types,1) IS DISTINCT FROM array_length(array_remove(_attr_types,NULL),1) THEN

			RAISE EXCEPTION 'Error 04: fn_get_attribute_domain: At least one of the passed attribute types is NULL (attr_types = %)!', _attr_types;
	ELSE
		_at_count := array_length(_attr_types,1);
	END CASE;


-----------------------------------------------------------------------------------------------------------------------
-- 1 attribute
	CASE
		WHEN _at_count = 1 THEN

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0 AS attr1
			)
			SELECT
				array_agg(attr1 ORDER BY attr1),
				NULL::integer[],
				NULL::integer[],
				NULL::integer[],
				NULL::integer[]
			FROM
				w_attr1
			WHERE
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1;

----------------------------------------------------------------------------------------------------------------------
-- 2 attributes
		WHEN _at_count = 2 THEN 

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0, $1[1]
			),
			w_attr2 AS
			(
				SELECT id AS attr2, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[2]
				UNION SELECT 0, $1[2]
			),
			w_const AS
			(	SELECT	t1.ordinality::integer,
					t1.attribute_type,
					t2.id,
					t3.variable
				FROM	unnest($1) WITH ORDINALITY AS t1(attribute_type,ordinality)
				INNER JOIN
					(
					SELECT attr1 AS id, attribute_type FROM w_attr1
					UNION ALL
					SELECT attr2 AS id, attribute_type FROM w_attr2
					) AS t2
				ON
					t1.attribute_type = t2.attribute_type
				LEFT JOIN
					target_data.'||_table_name_hierarchy||' AS t3
				ON
					t2.id = t3.variable_superior
			),
			w_desc AS(
				SELECT	t1.ordinality, t1.attribute_type, t1.id, t1.variable
				FROM	w_const AS t1
				INNER JOIN
					w_const AS t2
				ON t1.variable = t2.id
				WHERE	t1.ordinality < t2.ordinality
			),
			w_total AS (
				SELECT count(*) AS total
				FROM (SELECT DISTINCT t1.ordinality FROM w_desc AS t1 GROUP BY t1.ordinality) as t1
			),
			w_ordi AS (
				SELECT array_agg(t1.ordinality ORDER BY t1.ordinality) AS ordi_agg, total
				FROM
					(SELECT DISTINCT t1.ordinality, total
					FROM w_desc AS t1, w_total) AS t1
				GROUP BY total
			),
			w_attr AS(
				SELECT	attr1, attr2
				FROM	w_attr1, w_attr2
			),
			w_exclude AS (
				SELECT
					variable_superior AS attr_ex
				FROM
					target_data.'||_table_name_hierarchy||'
				WHERE
					variable IS NULL
			),
			w_all AS (
				SELECT
					attr1,
					attr2
				FROM
					w_attr
				WHERE
					CASE WHEN NOT EXISTS (SELECT total FROM w_ordi) THEN true
					END
					OR
					CASE WHEN 1 = (SELECT total FROM w_ordi) THEN 
						CASE WHEN ARRAY[1] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0)
						END
					END
				ORDER BY attr1, attr2
			)
			SELECT
				array_agg(attr1 ORDER BY attr1,attr2),
				array_agg(attr2 ORDER BY attr1,attr2),
				NULL::integer[],
				NULL::integer[],
				NULL::integer[]
			FROM
				w_all
			WHERE
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr2 = 0 ELSE TRUE END AND
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1] AND
					attr2 = $2[2]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1, _attr_2;

-----------------------------------------------------------------------------------------------------------------------
-- 3 attributes
		WHEN _at_count = 3 THEN

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0, $1[1]
			),
			w_attr2 AS
			(
				SELECT id AS attr2, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[2]
				UNION SELECT 0, $1[2]
			),
			w_attr3 AS
			(
				SELECT id AS attr3, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[3]
				UNION SELECT 0, $1[3]
			),
			w_const AS
			(	SELECT	t1.ordinality::integer,
					t1.attribute_type,
					t2.id,
					t3.variable
				FROM	unnest($1) WITH ORDINALITY AS t1(attribute_type,ordinality)
				INNER JOIN
					(
					SELECT attr1 AS id, attribute_type FROM w_attr1
					UNION ALL
					SELECT attr2 AS id, attribute_type FROM w_attr2
					UNION ALL
					SELECT attr3 AS id, attribute_type FROM w_attr3
					) AS t2
				ON
					t1.attribute_type = t2.attribute_type
				LEFT JOIN
					target_data.'||_table_name_hierarchy||' AS t3
				ON
					t2.id = t3.variable_superior
				),
			w_desc AS(
				SELECT	t1.ordinality, t1.attribute_type, t1.id, t1.variable
				FROM	w_const AS t1
				INNER JOIN
					w_const AS t2
				ON t1.variable = t2.id
				WHERE	t1.ordinality < t2.ordinality
			),
			w_total_comb AS (
				SELECT count(*) AS total
				FROM (SELECT DISTINCT t1.ordinality FROM w_desc AS t1 GROUP BY t1.ordinality) as t1
			),
			w_ordi AS (
				SELECT array_agg(t1.ordinality ORDER BY t1.ordinality) AS ordi_agg, total
				FROM
					(SELECT DISTINCT t1.ordinality, total
					FROM w_desc AS t1, w_total_comb) AS t1
				GROUP BY total
			),
			w_attr AS(
				SELECT	attr1,attr2,attr3
				FROM	w_attr1, w_attr2, w_attr3
			),
			w_exclude AS (
				SELECT
					variable_superior AS attr_ex
				FROM
					target_data.'||_table_name_hierarchy||'
				WHERE
					variable IS NULL
			),
			w_all AS (
				SELECT	attr1,
					attr2,
					attr3
				FROM
					w_attr
				WHERE
					CASE WHEN NOT EXISTS (SELECT total FROM w_ordi) THEN true
					END
					OR
					CASE WHEN 1 = (SELECT total FROM w_ordi) THEN 
						CASE WHEN ARRAY[1] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0)
						END OR
						CASE WHEN ARRAY[2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END
					END
					OR
					CASE WHEN 2 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END
					END
				ORDER BY attr1, attr2, attr3
			)
			SELECT
				array_agg(attr1 ORDER BY attr1,attr2,attr3),
				array_agg(attr2 ORDER BY attr1,attr2,attr3),
				array_agg(attr3 ORDER BY attr1,attr2,attr3),
				NULL::integer[],
				NULL::integer[]
			FROM
				w_all
			WHERE
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr2 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND

				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1] AND
					attr2 = $2[2] AND
					attr3 = $2[3]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1, _attr_2, _attr_3;

-----------------------------------------------------------------------------------------------------------------------
-- 4 attributes
		WHEN _at_count = 4 THEN

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0, $1[1]
			),
			w_attr2 AS
			(
				SELECT id AS attr2, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[2]
				UNION SELECT 0, $1[2]
			),
			w_attr3 AS
			(
				SELECT id AS attr3, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[3]
				UNION SELECT 0, $1[3]
			),
			w_attr4 AS
			(
				SELECT id AS attr4, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[4]
				UNION SELECT 0, $1[4]
			),
			w_const AS
			(	SELECT	t1.ordinality::integer,
					t1.attribute_type,
					t2.id,
					t3.variable
				FROM	unnest($1) WITH ORDINALITY AS t1(attribute_type,ordinality)
				INNER JOIN
					(
					SELECT attr1 AS id, attribute_type FROM w_attr1
					UNION ALL
					SELECT attr2 AS id, attribute_type FROM w_attr2
					UNION ALL
					SELECT attr3 AS id, attribute_type FROM w_attr3
					UNION ALL
					SELECT attr4 AS id, attribute_type FROM w_attr4
					) AS t2
				ON
					t1.attribute_type = t2.attribute_type
				LEFT JOIN
					target_data.'||_table_name_hierarchy||' AS t3
				ON
					t2.id = t3.variable_superior
				),
			w_desc AS(
				SELECT	t1.ordinality, t1.attribute_type, t1.id, t1.variable
				FROM	w_const AS t1
				INNER JOIN
					w_const AS t2
				ON t1.variable = t2.id
				WHERE	t1.ordinality < t2.ordinality
			),
			w_total_comb AS (
				SELECT count(*) AS total
				FROM (SELECT DISTINCT t1.ordinality FROM w_desc AS t1 GROUP BY t1.ordinality) as t1
			),
			w_ordi AS (
				SELECT array_agg(t1.ordinality ORDER BY t1.ordinality) AS ordi_agg, total
				FROM
					(SELECT DISTINCT t1.ordinality, total
					FROM w_desc AS t1, w_total_comb) AS t1
				GROUP BY total
			),
			w_attr AS(
				SELECT	attr1,attr2,attr3,attr4
				FROM	w_attr1, w_attr2, w_attr3, w_attr4
			),
			w_exclude AS (
				SELECT
					variable_superior AS attr_ex
				FROM
					target_data.'||_table_name_hierarchy||'
				WHERE
					variable IS NULL
			),
			w_all AS (
				SELECT	attr1,
					attr2,
					attr3,
					attr4
				FROM	w_attr
				WHERE
					CASE WHEN NOT EXISTS (SELECT total FROM w_ordi) THEN true
					END
					OR
					CASE WHEN 1 = (SELECT total FROM w_ordi) THEN 
						CASE WHEN ARRAY[1] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0)
						END OR
						CASE WHEN ARRAY[2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END OR
						CASE WHEN ARRAY[3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END
					END
					OR
					CASE WHEN 2 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END OR
						CASE WHEN ARRAY[1,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[2,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END
					END
					OR
					CASE WHEN 3 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END
					END
				ORDER BY attr1,attr2,attr3,attr4
			)
			SELECT	array_agg(attr1 ORDER BY attr1,attr2,attr3,attr4),
				array_agg(attr2 ORDER BY attr1,attr2,attr3,attr4),
				array_agg(attr3 ORDER BY attr1,attr2,attr3,attr4),
				array_agg(attr4 ORDER BY attr1,attr2,attr3,attr4),
				NULL::integer[]
			FROM
				w_all
			WHERE
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr2 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND

				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND

				CASE WHEN attr3 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1] AND
					attr2 = $2[2] AND
					attr3 = $2[3] AND
					attr4 = $2[4]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1, _attr_2, _attr_3, _attr_4;

-----------------------------------------------------------------------------------------------------------------------
-- 5 attributes
		WHEN _at_count = 5 THEN

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0, $1[1]
			),
			w_attr2 AS
			(
				SELECT id AS attr2, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[2]
				UNION SELECT 0, $1[2]
			),
			w_attr3 AS
			(
				SELECT id AS attr3, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[3]
				UNION SELECT 0, $1[3]
			),
			w_attr4 AS
			(
				SELECT id AS attr4, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[4]
				UNION SELECT 0, $1[4]
			),
			w_attr5 AS
			(
				SELECT id AS attr5, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[5]
				UNION SELECT 0, $1[5]
			),
			w_const AS
			(	SELECT	t1.ordinality::integer,
					t1.attribute_type,
					t2.id,
					t3.variable
				FROM	unnest($1) WITH ORDINALITY AS t1(attribute_type,ordinality)
				INNER JOIN
					(
					SELECT attr1 AS id, attribute_type FROM w_attr1
					UNION ALL
					SELECT attr2 AS id, attribute_type FROM w_attr2
					UNION ALL
					SELECT attr3 AS id, attribute_type FROM w_attr3
					UNION ALL
					SELECT attr4 AS id, attribute_type FROM w_attr4
					UNION ALL
					SELECT attr5 AS id, attribute_type FROM w_attr5
					) AS t2
				ON
					t1.attribute_type = t2.attribute_type
				LEFT JOIN
					target_data.'||_table_name_hierarchy||' AS t3
				ON
					t2.id = t3.variable_superior
				),
			w_desc AS(
				SELECT	t1.ordinality, t1.attribute_type, t1.id, t1.variable
				FROM	w_const AS t1
				INNER JOIN
					w_const AS t2
				ON t1.variable = t2.id
				WHERE	t1.ordinality < t2.ordinality
			),
			w_total_comb AS (
				SELECT count(*) AS total
				FROM (SELECT DISTINCT t1.ordinality FROM w_desc AS t1 GROUP BY t1.ordinality) as t1
			),
			w_ordi AS (
				SELECT array_agg(t1.ordinality ORDER BY t1.ordinality) AS ordi_agg, total
				FROM
					(SELECT DISTINCT t1.ordinality, total
					FROM w_desc AS t1, w_total_comb) AS t1
				GROUP BY total
			),
			w_attr AS(
				SELECT	attr1,attr2,attr3,attr4,attr5
				FROM	w_attr1, w_attr2, w_attr3, w_attr4, w_attr5
			),
			w_exclude AS (
				SELECT
					variable_superior AS attr_ex
				FROM
					target_data.'||_table_name_hierarchy||'
				WHERE
					variable IS NULL
			),
			w_all AS (
				SELECT	attr1,
					attr2,
					attr3,
					attr4,
					attr5
				FROM	w_attr
				WHERE
					CASE WHEN NOT EXISTS (SELECT total FROM w_ordi) THEN true
					END
					OR
					CASE WHEN 1 = (SELECT total FROM w_ordi) THEN 
						CASE WHEN ARRAY[1] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0)
						END OR
						CASE WHEN ARRAY[2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END OR
						CASE WHEN ARRAY[3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END
					END
					OR
					CASE WHEN 2 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END OR
						CASE WHEN ARRAY[1,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[1,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END OR
						CASE WHEN ARRAY[2,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[2,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END OR
						CASE WHEN ARRAY[3,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END
					END
					OR
					CASE WHEN 3 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[1,3,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END OR
						CASE WHEN ARRAY[2,3,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END OR
						CASE WHEN ARRAY[1,2,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END
					END
					OR
					CASE WHEN 4 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2,3,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END
					END
				ORDER BY attr1,attr2,attr3,attr4,attr5
			)
			SELECT	array_agg(attr1 ORDER BY attr1,attr2,attr3,attr4,attr5),
				array_agg(attr2 ORDER BY attr1,attr2,attr3,attr4,attr5),
				array_agg(attr3 ORDER BY attr1,attr2,attr3,attr4,attr5),
				array_agg(attr4 ORDER BY attr1,attr2,attr3,attr4,attr5),
				array_agg(attr5 ORDER BY attr1,attr2,attr3,attr4,attr5)
			FROM
				w_all
			WHERE
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr2 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr5 = 0 ELSE TRUE END AND

				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND
				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr5 = 0 ELSE TRUE END AND

				CASE WHEN attr3 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND
				CASE WHEN attr3 IN (SELECT attr_ex FROM w_exclude) THEN attr5 = 0 ELSE TRUE END AND

				CASE WHEN attr4 IN (SELECT attr_ex FROM w_exclude) THEN attr5 = 0 ELSE TRUE END AND
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1] AND
					attr2 = $2[2] AND
					attr3 = $2[3] AND
					attr4 = $2[4] AND
					attr5 = $2[5]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1, _attr_2, _attr_3, _attr_4, _attr_5;
		ELSE
			RAISE EXCEPTION 'Error 05: The function is not implemented for the specified number of attribute types (at_count = %)',_at_count;
		END CASE;

		IF _attribute_domain IS NOT NULL
		THEN
			IF _attr_1 IS NULL THEN RAISE EXCEPTION 'Error 06: No combination between attribute types was found for the specified attribute domain (attribute_types = %, attribute_domain = %).', _attr_types, _attribute_domain;
			END IF;
		END IF;

		RETURN QUERY SELECT _attr_1, _attr_2, _attr_3, _attr_4, _attr_5;		
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  COMMENT ON FUNCTION target_data.fn_get_attribute_domain(varchar(2), integer[], integer[]) IS
'The function returns the attribute domain field table for the specified attribute type combination.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_attribute_domain (varchar(2), integer[], integer[]) TO app_nfiesta;
-- </function>


-- <function name="fn_get_classification_rule4classification_rule_id" schema="target_data" src="functions/fn_get_classification_rule4classification_rule_id.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4classification_rule_id
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) CASCADE;

create or replace function target_data.fn_get_classification_rule4classification_rule_id
(
	_classification_rule_type	character varying,
	_classification_rule_id		integer[],
	_ldsity_objects				integer[]
)	
returns text
as
$$
declare
		_table_name								text;
		_classification_rule_i					text;	
		_ldsity_object_i						integer;
		_pozice_i								integer;
		_column_name_i_array					text[];
		_classification_rule_i_pozice			text;
		_classification_rule_4case_pozice		text;
		_category								integer[];
begin
		if _classification_rule_type is null
		then
			raise exception 'Error 01: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_type must not by NULL !';
		end if;
	
		if not(_classification_rule_type = any(array['adc','spc']))
		then
			raise exception 'Error 02: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_type must be "adc" or "spc" !';
		end if;	
	
		if _classification_rule_id is null
		then
			raise exception 'Error 03: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_id must not by NULL !';
		end if;
	
		if _ldsity_objects is null
		then
			raise exception 'Error 04: fn_get_classification_rule4classification_rule_id: Input argument _ldsity_objects must not by NULL !';
		end if;	
	
		if _classification_rule_type = 'adc' then _table_name := 'cm_adc2classification_rule'; end if;
		if _classification_rule_type = 'spc' then _table_name := 'cm_spc2classification_rule'; end if;	
	
		for i in 1..array_length(_classification_rule_id,1)
		loop
				execute '
				select
						case when classification_rule is null then ''true'' else classification_rule end as classification_rule,
						ldsity_object
				from
						target_data.'||_table_name||'
				where
						id = $1'
				using	_classification_rule_id[i]
				into	_classification_rule_i,
						_ldsity_object_i;

				-----------------------------------------------------
				if 	_classification_rule_i in ('EXISTS','NOT EXISTS')
				then
					_classification_rule_i := 'TRUE';
				else
					_classification_rule_i := _classification_rule_i;
				end if;
				-----------------------------------------------------						

				_pozice_i := array_position(_ldsity_objects,_ldsity_object_i);
										
				select array_agg(column_name::text order by ordinal_position) from information_schema.columns
			 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_object_i)
			 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_object_i)
				into _column_name_i_array;
								
				for ii in 1..array_length(_column_name_i_array,1)
				loop					
					if ii = 1
					then
						_classification_rule_i_pozice := replace(
															_classification_rule_i,
															_column_name_i_array[ii],
															concat(_column_name_i_array[ii],'_',_pozice_i)
															);
					else
						_classification_rule_i_pozice := replace(
															_classification_rule_i_pozice,
															_column_name_i_array[ii],
															concat(_column_name_i_array[ii],'_',_pozice_i)
															);
					end if;
				end loop;

				
				if i = 1
				then
					_classification_rule_4case_pozice := _classification_rule_i_pozice;
				else
					_classification_rule_4case_pozice := concat(_classification_rule_4case_pozice,' and ',_classification_rule_i_pozice);
				end if;		
						
		end loop;
	
		_classification_rule_4case_pozice := concat('(',_classification_rule_4case_pozice,')');

		return _classification_rule_4case_pozice;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) is
'The function gets text of classification rules for their classification rule identifiers.';

grant execute on function target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) to public;
-- </function>


-- <function name="fn_import_ldsity_values" schema="target_data" src="functions/fn_import_ldsity_values.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_import_ldsity_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_ldsity_values(integer[], integer[], integer[], double precision) CASCADE;
	
create or replace function target_data.fn_import_ldsity_values
(
	_panels						integer[],
	_reference_year_sets		integer[],
	_categorization_setups		integer[],
	_threshold					double precision default 0.0
)
returns text
as
$$
declare
		_array_id									integer[];
		_array_target_variable						integer[];
		_target_variable							integer;
		_lot_100									integer;
		_lot_200									integer;
		_variant									integer;
		_tv_target_variable							integer;
		_ldsity										integer;
		_ldsity_object_type							integer;
		_tv_area_domain_category					integer[];
		_tv_sub_population_category					integer[];
		_tv_definition_variant						integer[];
		_tv_use_negative							boolean;
		_tv_version									integer;
		_state_or_change							integer;
		_ldsity_ldsity_object						integer;
		_ldsity_column_expresion					text;
		_ldsity_unit_of_measure						integer;
		_ldsity_area_domain_category				integer[];
		_ldsity_sub_population_category				integer[];
		_ldsity_definition_variant					integer[];	
		_area_domain_category						integer[];
		_sub_population_category					integer[];
		_definition_variant							integer[];	
		_area_domain_category_i						integer[];
		_area_domain_category_t						text[];	
		_sub_population_category_i					integer[];
		_sub_population_category_t					text[];
		_condition_ids								integer[];
		_condition_types							text[];
		_example_query_1							text;
		_example_query								text;
		_ldsity_objects								integer[];
		_filter_i									text;
		_filters_array								text[];	
		_example_query_i							text;
		_example_query_array						text[];
		_query_res									text;
		_column_name_i_array						text[];	
		_ii_column_new_text							text;
		_pozice_i									integer;
		_classification_rule_i						text;	
		_pozice_array								integer[];
		_classification_rule_array					text[];	
		_position_groups							integer[];
		_classification_rule_upr_ita_pozice			text[];
		_classification_rule_ita_pozice				text;
		_ldsity_objects_without_conditions			integer[];
		_string4columns								text;
		_string4inner_joins							text;	
		_position4join								integer;
		_table_name4join							text;
		_pkey_column4join							text;
		_i_column4join								text;
		_string4attr								text;
		_string4case								text;
		_position4ldsity							integer;
		_string4ads									text;
		_query_res_big								text;
		_string_var									text;	
		_string_result_1							text;	
		_id_cat_setups_array						integer[];
		_check_bez_rozliseni						integer[];
		_check_bez_rozliseni_boolean				boolean[];	
		_string_var_100								text;
		_array_id_200								integer[];	
		_string_var_200								text;
		_string_check_insert						text;	
		_max_id_tlv									integer;
		_res										text;
		_ext_version_text							text;
		_case4available_datasets_text				text;
		_categorization_setups_adc					integer[];
		_categorization_setups_spc					integer[];
	
begin
		if _panels is null
		then
			raise exception 'Error 01: fn_import_ldsity_values: The input argument _panels must not be NULL !';
		end if;
	
		if _reference_year_sets is null
		then
			raise exception 'Error 02: fn_import_ldsity_values: The input argument _reference_year_sets must not be NULL !';
		end if;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_import_ldsity_values: The input argument _categorization_setups must not be NULL !';
		end if;
	
		if	(
			select count(t.res) > 0 from
			(select unnest(_categorization_setups) as res) as t
			where t.res is null
			)
		then
			raise exception 'Error 04: fn_import_ldsity_values: The input argument _categorization_setups must not contains NULL value!';
		end if;
	
		-------------------------------------------------------------
		-------------------------------------------------------------
		with
		w as	(
				select distinct categorization_setup
				from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(	
					select id from target_data.cm_ldsity2target_variable
					where target_variable =
								(
								select distinct target_variable from target_data.cm_ldsity2target_variable
								where id in	(
											select ldsity2target_variable
											from target_data.cm_ldsity2target2categorization_setup
											where categorization_setup in (select unnest(_categorization_setups))
											)
								)
					)
				and (
					adc2classification_rule is null
					or array_length(adc2classification_rule,1) <= 
						(select max(t.res) from
						(select array_length(adc2classification_rule,1) as res
						from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						and adc2classification_rule is not null) as t)
					)
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups_adc;

		with
		w as	(
				select distinct categorization_setup
				from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(	
					select id from target_data.cm_ldsity2target_variable
					where target_variable =
								(
								select distinct target_variable from target_data.cm_ldsity2target_variable
								where id in	(
											select ldsity2target_variable
											from target_data.cm_ldsity2target2categorization_setup
											where categorization_setup in (select unnest(_categorization_setups))
											)
								)
					)
				and (
					spc2classification_rule is null
					or array_length(spc2classification_rule,1) <= 
						(select max(t.res) from
						(select array_length(spc2classification_rule,1) as res
						from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						and spc2classification_rule is not null) as t)
					)
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups_spc;

		with
		w as	(
				select a.categorization_setup from
					(select unnest(_categorization_setups_adc) as categorization_setup) as a
				inner join
					(select unnest(_categorization_setups_spc) as categorization_setup) as b
				on a.categorization_setup = b.categorization_setup
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups;
		-------------------------------------------------------------
		-------------------------------------------------------------

		select
				array_agg(id order by ldsity_object_type, id) as id,
				array_agg(target_variable order by ldsity_object_type, id) as target_variable
		from
				target_data.cm_ldsity2target_variable
		where
				id in	(
						select ldsity2target_variable from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						)
		into
				_array_id,
				_array_target_variable;
		
		if _array_id is null
		then
			raise exception 'Error 05: fn_import_ldsity_values: For input values in argument _categorization_setups = % not found any record in table cm_ldsity2target_variable!',_categorization_setups;
		end if;
		
		_target_variable := (select distinct t.res from (select unnest(_array_target_variable) as res) as t);
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 06: fn_import_ldsity_values: This variant is not implemented yet!';
		end if;
	
		---------------------
		---------------------
		for bc in 1..array_length(_array_id,1)
		loop
			-- datas from table cm_ldsity2target_variable
			select
					target_variable,
					ldsity,
					ldsity_object_type,
					area_domain_category,
					sub_population_category,
					definition_variant,
					use_negative,
					version
			from
					target_data.cm_ldsity2target_variable
			where
					id = _array_id[bc]
			into
					_tv_target_variable,
					_ldsity,
					_ldsity_object_type,
					_tv_area_domain_category,
					_tv_sub_population_category,
					_tv_definition_variant,
					_tv_use_negative,
					_tv_version;
				
			-- datas from c_target_variable	
			select state_or_change from target_data.c_target_variable where id = _tv_target_variable
			into _state_or_change;
		
			-----------------------------------------------------------
			-- INTERMEDIATE STEP => insert values into table t_available_datasets
			-----------------------------------------------------------
			with
			w1 as	(
					select distinct reference_year_set, panel
					from target_data.fn_get_plots(_panels,_reference_year_sets,_state_or_change,_tv_use_negative)
					order by reference_year_set, panel
					)
			,w2 as	(select unnest(array[_categorization_setups]) as categorization_setup)
			,w3 as	(select w1.*, w2.* from w1, w2)
			insert into target_data.t_available_datasets(panel, reference_year_set, categorization_setup)
			select panel, reference_year_set, categorization_setup from w3
			except
			select panel, reference_year_set, categorization_setup from target_data.t_available_datasets
			where categorization_setup in (select unnest(array[_categorization_setups]))
			order by categorization_setup, panel, reference_year_set;
			-----------------------------------------------------------
			-----------------------------------------------------------
	
			-- datas from table c_ldsity
			select
					ldsity_object,
					column_expression,
					unit_of_measure,
					area_domain_category,
					sub_population_category,
					definition_variant
			from
					target_data.c_ldsity where id = _ldsity
			into
					_ldsity_ldsity_object,
					_ldsity_column_expresion,
					_ldsity_unit_of_measure,
					_ldsity_area_domain_category,
					_ldsity_sub_population_category,
					_ldsity_definition_variant;
							 
			_area_domain_category := _ldsity_area_domain_category || _tv_area_domain_category;
			_sub_population_category := _ldsity_sub_population_category || _tv_sub_population_category;
			_definition_variant := _ldsity_definition_variant || _tv_definition_variant;

			-----------------------------------------
			if _area_domain_category is not null
			then
				for i in 1..array_length(_area_domain_category,1)
				loop
					if i = 1
					then
						_area_domain_category_i := array[_area_domain_category[i]];
						_area_domain_category_t := array['ad'];
					else
						_area_domain_category_i := _area_domain_category_i || array[_area_domain_category[i]];
						_area_domain_category_t := _area_domain_category_t || array['ad'];
					end if;
				end loop;
			else
				_area_domain_category_i := null::integer[];
				_area_domain_category_t := null::text[];
			end if;
			-----------------------------------------
			-----------------------------------------
			if _sub_population_category is not null
			then
				for i in 1..array_length(_sub_population_category,1)
				loop
					if i = 1
					then
						_sub_population_category_i := array[_sub_population_category[i]];
						_sub_population_category_t := array['sp'];
					else
						_sub_population_category_i := _sub_population_category_i || array[_sub_population_category[i]];
						_sub_population_category_t := _sub_population_category_t || array['sp'];
					end if;
				end loop;
			else
				_sub_population_category_i := null::integer[];
				_sub_population_category_t := null::text[];
			end if;
			-----------------------------------------
			-----------------------------------------
	
			_condition_ids := _area_domain_category_i || _sub_population_category_i;
			_condition_types := _area_domain_category_t || _sub_population_category_t;
	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if _condition_ids is null
			then
				_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_panels,'],array[',_reference_year_sets,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');  -- pkey pro testy nahradit sloupcem plot
				_example_query := concat('w_&i& as (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#)');
			else
				_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_panels,'],array[',_reference_year_sets,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');
				_example_query := concat('w_&i& as (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#)');
			end if;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_ldsity_objects := target_data.fn_get_ldsity_objects(array[_ldsity_ldsity_object]);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					select case when filter is null then 'true' else filter end
					from target_data.c_ldsity_object
					where id = _ldsity_objects[i]
					into _filter_i;
				
					if i = 1
					then
						_filters_array := array[_filter_i];
					else
						_filters_array := _filters_array || array[_filter_i];
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					if _ldsity_objects[i] = 100
					then
						_example_query_i := _example_query_1;
					else
						_example_query_i := _example_query;
					end if;
				
					_example_query_i := replace(_example_query_i,'&i&'::text,i::text);
					_example_query_i := replace(_example_query_i,'#con4filter#',(select case when filter is null then 'true' else filter end from target_data.c_ldsity_object where id = _ldsity_objects[i]));
					_example_query_i := replace(_example_query_i,concat('#table_name#_',i),(select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]));				
				
					if i = 1
					then
						_example_query_array := array[_example_query_i];
					else
						_example_query_array := _example_query_array || _example_query_i;
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					if i = 1
					then
						_query_res := concat('with ',_example_query_array[i]);
					else
						_query_res := _query_res || concat(',',_example_query_array[i]);
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
				select array_agg(column_name order by ordinal_position) from information_schema.columns
			 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
			 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
				into _column_name_i_array;
				
				for ii in 1..array_length(_column_name_i_array,1)
				loop
					if ii = 1
					then
						_ii_column_new_text := concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i);
					else
						_ii_column_new_text := concat(_ii_column_new_text,', ',concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i));
					end if;
				end loop;
			
				_query_res := replace(_query_res,concat('#column_names_',i::text,'#'),_ii_column_new_text);
				
			end loop;	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if _condition_ids is not null
			then
				for i in 1..array_length(_condition_ids,1)
				loop
					if _condition_types[i] = 'ad'
					then			
						select array_position(_ldsity_objects,
						(select ldsity_object from target_data.cm_adc2classification_rule where id = _condition_ids[i]))
						into _pozice_i;
					
						if _pozice_i is null then raise exception 'Error 07: fn_import_ldsity_values: For internal variable _condition_ids[i] = % not found position in internal variable _ldsity_objects = % !',_condition_ids[i],_ldsity_objects; end if;
									
						select case when classification_rule is null then 'true' else classification_rule end
						from target_data.cm_adc2classification_rule where id = _condition_ids[i]
						into _classification_rule_i;
					
						if _classification_rule_i is null then raise exception 'Error 08: fn_import_ldsity_values: The classification rule in the cm_adc2classification_rule table was not found for the internal variable _condition_ids[i] = %!,',_condition_ids[i]; end if;
					
					end if;
				
					if _condition_types[i] = 'sp'
					then			
						select array_position(_ldsity_objects,
						(select ldsity_object from target_data.cm_spc2classification_rule where id = _condition_ids[i]))
						into _pozice_i;				
					
						if _pozice_i is null then raise exception 'Error 09: fn_import_ldsity_values: For internal variable _condition_ids[i] = % not found position in internal variable _ldsity_objects = % !',_condition_ids[i],_ldsity_objects; end if;
								
						select case when classification_rule is null then 'true' else classification_rule end
						from target_data.cm_spc2classification_rule where id = _condition_ids[i]
						into _classification_rule_i;
					
						if _classification_rule_i is null then raise exception 'Error 10: fn_import_ldsity_values: The classification rule in the cm_spc2classification_rule table was not found for the internal variable _condition_ids[i] = %!,',_condition_ids[i]; end if;				
					
					end if;
				
					if i = 1
					then
						_pozice_array := array[_pozice_i];
						_classification_rule_array := array[_classification_rule_i];
					else
						_pozice_array := _pozice_array || array[_pozice_i];
						_classification_rule_array := _classification_rule_array || array[_classification_rule_i];
					end if;
					
				end loop;
				
				with
				w1 as	(select unnest(_pozice_array) as pozice)
				,w2 as 	(select distinct pozice from w1)
				select array_agg(pozice order by pozice) from w2
				into _position_groups;
			
				for i in 1..array_length(_position_groups,1)
				loop
					with
					w1 as	(
							select
							unnest(_pozice_array) as pozice,
							unnest(_classification_rule_array) as classification_rule
							)
					,w2 as	(
							select
									row_number() over () as new_id,
									pozice,
									classification_rule
							from w1
							)
					,w3 as	(
							select
									row_number() over (partition by pozice order by new_id) as new_id4order,
									w2.*
							from w2
							)
					select array_agg(classification_rule order by new_id4order) as classification_rule
					from w3 where pozice = _position_groups[i]
					into _classification_rule_upr_ita_pozice;
				
					for ii in 1..array_length(_classification_rule_upr_ita_pozice,1)
					loop
						if ii = 1
						then
							_classification_rule_ita_pozice := concat(_classification_rule_upr_ita_pozice[ii]);
						else
							_classification_rule_ita_pozice := concat(_classification_rule_ita_pozice,' and ',_classification_rule_upr_ita_pozice[ii]);
						end if;
					end loop;
				
					_query_res := replace(_query_res,concat('#conditions_',_position_groups[i],'#'),concat('(',_classification_rule_ita_pozice,')'));
				
				end loop;
			
				with
				w1 as	(select * from generate_series(1,array_length(_ldsity_objects,1)) as res)
				,w2 as	(select unnest(_position_groups) as res)
				,w3 as	(select res from w1 except select res from w2)
				select array_agg(res order by res) from w3
				into _ldsity_objects_without_conditions;
				
				if _ldsity_objects_without_conditions is not null
				then
					for i in 1..array_length(_ldsity_objects_without_conditions,1)
					loop
						_query_res := replace(_query_res,concat('#conditions_',_ldsity_objects_without_conditions[i],'#'),'(true)');
					end loop;
				end if;
						
			end if;	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
				if i = 1
				then
					_string4columns := concat(',w_res as (select w_',i,'.*');
					_string4inner_joins := concat(' from w_',i);
				else
					_string4columns := concat(_string4columns,', w_',i,'.*');
				
					select array_position(_ldsity_objects, 
						(select upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]))
					into _position4join;
					
					for i in 1..array_length(_ldsity_objects,1)
					loop
							if i = _position4join
							then
								_table_name4join := (select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]);
							end if;
					end loop;
				
					with
					w as	(
							select kcu.table_schema,
							       kcu.table_name,
							       tco.constraint_name,
							       kcu.ordinal_position as position,
							       kcu.column_name as key_column
							from information_schema.table_constraints tco
							join information_schema.key_column_usage kcu 
							     on kcu.constraint_name = tco.constraint_name
							     and kcu.constraint_schema = tco.constraint_schema
							     and kcu.constraint_name = tco.constraint_name
							where tco.constraint_type = 'PRIMARY KEY'
							order by kcu.table_schema,
							         kcu.table_name,
							         position
							 )
					select key_column::text from w
					where table_schema = (substring(_table_name4join from 1 for (position('.' in _table_name4join) - 1))) 
					and table_name = (substring(_table_name4join from (position('.' in _table_name4join) + 1) for length(_table_name4join)))
					into _pkey_column4join;
					
					select column4upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]
					into _i_column4join;
				
					_string4inner_joins :=
					concat(_string4inner_joins,' inner join w_',i,' on ','w_',_position4join,'.',_pkey_column4join,'_',_position4join,' = ','w_',i,'.',_i_column4join,'_',i);
							
				end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------		
			_query_res := _query_res || _string4columns || _string4inner_joins || ')';	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if	(_variant in (3,4) and _ldsity_ldsity_object is distinct from (select id from target_data.c_ldsity_object where upper_object is null))
			then
				_string4attr := concat('
				select
						case
							when adc2classification_rule is null
									then array[0]
									else target_data.fn_get_category4classifiaction_rule_id(''adc'',adc2classification_rule)
						end as area_domain_category,
						case
							when spc2classification_rule is null
									then array[0]
									else target_data.fn_get_category4classifiaction_rule_id(''spc'',spc2classification_rule)
						end as sub_population_category,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in
												(
												select unnest($1)
												except
												select t.categorization_setup from
												(select a.*, b.* from
												(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
												from target_data.cm_ldsity2target2categorization_setup
												where categorization_setup in (select unnest($1))
												and ldsity2target_variable = ',_array_id[bc],') as a
												inner join target_data.cm_spc2classification_rule as b
												on a.id_cm_spc = b.id) as t
												where t.classification_rule = ''NOT EXISTS''	
												)
				and
						ldsity2target_variable = ',_array_id[bc],'
				');
			else			
				_string4attr := concat('
				select
						case
							when adc2classification_rule is null
									then array[0]
									else target_data.fn_get_category4classifiaction_rule_id(''adc'',adc2classification_rule)
						end as area_domain_category,
						case
							when spc2classification_rule is null
									then array[0]
									else target_data.fn_get_category4classifiaction_rule_id(''spc'',spc2classification_rule)
						end as sub_population_category,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest($1))
				and
						ldsity2target_variable = ',_array_id[bc],'
				');
			end if;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_string4case := target_data.fn_get_classification_rules4categorization_setups(_categorization_setups,_array_id[bc]);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_position4ldsity := array_position(_ldsity_objects,_ldsity_ldsity_object);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_string4ads := concat('
			,w_strings as	('||_string4attr||')
			,w_pre_adc as	(-- tady je cross join => zde dojde ke kazdemu zaznamu z w_res k rozjoinovani na vsechny kategorie, ktere jsou ve w_strings
							select
									w_strings.*,
									w_res.*
							from
									w_res, w_strings
							)
			,w_ads as		(
							select
									',_string4case,' as res_case,
									w_pre_adc.*
							from
									w_pre_adc
							)
			select
					res_case,
					area_domain_category,
					sub_population_category,
					adc2classification_rule,
					spc2classification_rule,
					(',case when _tv_use_negative = true then '(-1)' else '(1)' end,' * coalesce(',_ldsity_column_expresion,'_',_position4ldsity,',0.0)) as value,
					gid,
					cluster_configuration,
					reference_year_set4join,
					reference_year_set,
					panel,
					stratum,
					categorization_setup
			from
					w_ads
			');	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_query_res := _query_res || _string4ads;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if bc = 1
			then
				_query_res_big := concat('with bw_',bc,' as (',_query_res,')');
			else
				_query_res_big := concat(_query_res_big,',bw_',bc,' as (',_query_res,')');
			end if;
		
		end loop;		
		-----------------------------------------------------------------------
		-- end BIG CYCLE
		-----------------------------------------------------------------------

		_ext_version_text := (select extversion from pg_extension where extname = 'nfiesta_target_data');

		_case4available_datasets_text := target_data.fn_get_case_ad4categorization_setups(_categorization_setups);
		
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _variant in (1,2)
		then		
				for bc in 1..array_length(_array_id,1)
				loop
					if bc = 1
					then
						_string_var := concat(',w_union_all as (select ',_array_id[bc],' as ldsity2target_variable,* from bw_',bc,' where res_case = true');
					else
						_string_var := concat(_string_var,' union all select ',_array_id[bc],' as ldsity2target_variable,* from bw_',bc,' where res_case = true');
					end if;
				end loop;
			
				_string_var := concat(_string_var,')');
			
				_string_result_1 := concat(
				'
				,w_part_1 as		(
									select
											gid,
											panel,
											reference_year_set,
											categorization_setup,
											sum(value) as value
									from
											w_union_all as t1
									group
											by gid, panel, reference_year_set, categorization_setup
									)
				,w_result_1 as		(
									select
											t1.gid as plot,
											t1.panel,
											t1.reference_year_set,
											t1.categorization_setup,
											t1.value,
											',_case4available_datasets_text,' as available_datasets
									from
											w_part_1 as t1 where t1.value is distinct from 0 -- toto zajisti praci s nenulovymi hodnotama
									)
				');

		_query_res_big := _query_res_big || _string_var || _string_result_1;
	
		end if;

		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _variant in (3,4)
		then		
			for i in 1..array_length(_categorization_setups,1)
			loop
				select array_agg(id order by id)
				from target_data.cm_ldsity2target2categorization_setup
				where categorization_setup = _categorization_setups[i]
				into _id_cat_setups_array;
			
				for ii in 1..array_length(_id_cat_setups_array,1)
				loop
					if	(
						select count(*) = 1
						from target_data.cm_ldsity2target2categorization_setup
						where id = _id_cat_setups_array[ii]
						and (adc2classification_rule is null and spc2classification_rule is null)
						)
					then
						if ii = 1
						then
							_check_bez_rozliseni := array[1];
						else
							_check_bez_rozliseni := _check_bez_rozliseni || array[1];
						end if;
					else
						if ii = 1
						then
							_check_bez_rozliseni := array[0];
						else
							_check_bez_rozliseni := _check_bez_rozliseni || array[0];
						end if;
					end if;
				end loop;
			
				if		(
						select sum(t.res) = array_length(_id_cat_setups_array,1)
						from (select unnest(_check_bez_rozliseni) as res) as t
						)
				then
					if i = 1
					then
						_check_bez_rozliseni_boolean := array[true];
					else
						_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[true];
					end if;
				else
					if i = 1
					then
						_check_bez_rozliseni_boolean := array[false];
					else
						_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[false];
					end if;
				end if;	
			end loop;
		
			if	(
				select count(t.res) = 0
				from (select unnest(_check_bez_rozliseni_boolean) as res) as t
				where t.res = true
				)
			then
				with
				w1 as	(
						select * from target_data.cm_ldsity2target2categorization_setup
						where ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))
														)
						and (adc2classification_rule is null and spc2classification_rule is null)
						)
				,w2 as	(
						select categorization_setup, count(categorization_setup) as pocet
						from w1 group by categorization_setup
						)
				select array[categorization_setup] || _categorization_setups
				from w2 where pocet = (select max(pocet) from w2)
				into _categorization_setups;
			
			else
				_categorization_setups := _categorization_setups;
			end if;

			_string_var_100 := concat(',w_union_all_100 as (select ',_array_id[1],' as ldsity2target_variable,* from bw_1 where res_case = true)');
		
			_array_id_200 := array_remove(_array_id, _array_id[1]);
				
			for bc_200 in 1..array_length(_array_id_200,1)
			loop
				if bc_200 = 1
				then
					_string_var_200 := concat(',w_union_all_200 as (select ',_array_id_200[bc_200],' as ldsity2target_variable,* from bw_',bc_200 + 1,' where res_case = true');
				else
					_string_var_200 := concat(_string_var_200,' union all select ',_array_id_200[bc_200],' as ldsity2target_variable,* from bw_',bc_200 + 1,' where res_case = true');
				end if;				
			end loop;
		
			_string_var_200 := concat(_string_var_200,')');
			
			_string_result_1 := concat('
			,w_part_1 as		(
								select
										t1.*,
										',_case4available_datasets_text,' as available_datasets
								from
										w_union_all_100 as t1
								where
										value is distinct from 0.0			
								)
			,w_part_2 as		(			
								select
									t1.gid,
									t1.panel,
									t1.reference_year_set,
									t1.categorization_setup,
									t1.area_domain_category,
									t1.value,
									t2.value_sum,
									case when t2.value_sum = 0.0 then t2.value_sum else t1.value/t2.value_sum end as value4nasobeni,
									',_case4available_datasets_text,' as available_datasets
								from
									(
									select
									gid, panel, reference_year_set, categorization_setup, area_domain_category,
									sum(value) as value
									from w_union_all_200
									where value is distinct from 0.0
									group by gid, panel, reference_year_set, categorization_setup, area_domain_category
									) as t1
								inner join 
									(
									select gid, panel, reference_year_set,
									sum(value) as value_sum
									from w_union_all_200
									where (area_domain_category = array[0] and sub_population_category = array[0])
									and value is distinct from 0.0
									group by gid, panel, reference_year_set
									) as t2
								on
									t1.gid = t2.gid and
									t1.panel = t2.panel and
									t1.reference_year_set = t2.reference_year_set
							)
			,w_result as	(
							select
									t.*,
									coalesce((t.value * coef_200.value4nasobeni),0.0) as value_res
							from 
									w_part_1 as t inner join w_part_2 as coef_200
							on
									t.gid = coef_200.gid and t.available_datasets = coef_200.available_datasets
							)
			,w_not_exists as	(
								select * from w_part_1
								where gid in (select distinct gid from w_part_1 except select distinct gid from w_part_2)
								and categorization_setup in (
									select t.categorization_setup from
									(select a.*, b.* from
									(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
									from target_data.cm_ldsity2target2categorization_setup
									where ldsity2target_variable in (select unnest($2))
									) as a inner join target_data.cm_spc2classification_rule as b
									on a.id_cm_spc = b.id) as t
									where t.classification_rule = ''NOT EXISTS'')
								)
			,w_result_1 as	(
							select gid as plot, value_res as value, available_datasets from w_result union all
							select gid as plot, value, available_datasets from w_not_exists
							)
			');
		
			_query_res_big := _query_res_big || _string_var_100 || _string_var_200 || _string_result_1;
		
		end if;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
			
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		_string_check_insert := replace(replace(concat
		(',w_tlv as		(
						select
								tlv.id,
								tlv.plot,
								tlv.available_datasets,
								tlv.value,
								coalesce(w_result_1.plot,tlv.plot) as plot_upr,
								coalesce(w_result_1.available_datasets,tlv.available_datasets) as available_datasets_upr,
								case
									when tlv.value is     null and w_result_1.value is not null then w_result_1.value
									when tlv.value is not null and tlv.value is distinct from 0.0 and w_result_1.value is null then 0.0
									when tlv.value is not null and tlv.value = 0.0 and w_result_1.value is null then null::double precision
									else w_result_1.value
								end
									as value_upr,
								case
									when tlv.value is     null and w_result_1.value is not null then false
									when tlv.value is not null and tlv.value is distinct from 0.0 and w_result_1.value is null then false
									when tlv.value is not null and tlv.value = 0.0 and w_result_1.value is null then null::boolean									
									else
										case
											when tlv.value = 0.0 and w_result_1.value = 0.0 then true
											when tlv.value = 0.0 and w_result_1.value is distinct from 0.0
													then
														case
															when (abs(1 - (tlv.value / w_result_1.value)) * 100.0) <= ',_threshold,' then true
															else false
														end
											when tlv.value is distinct from 0.0 and w_result_1.value = 0.0
													then
														case
															when (abs(1 - (w_result_1.value / tlv.value)) * 100.0) <= ',_threshold,'	then true
															else false
														end
											else
												case
													when (abs(1 - (tlv.value / w_result_1.value)) * 100.0) <= ',_threshold,' then true
													else false
												end
										end									
								end
									as value_identic
						from
							(
							select * from target_data.t_ldsity_values
							where available_datasets in
														(
														select id from target_data.t_available_datasets
														where categorization_setup in (select unnest($1))
														and reference_year_set in (select unnest(array[',_reference_year_sets,']))
														and panel in (select unnest(array[',_panels,']))														
														)
							and is_latest = true
							) as tlv
						full outer join w_result_1
						on (tlv.plot = w_result_1.plot and tlv.available_datasets = w_result_1.available_datasets)
						)
		,w_update as	(
						update target_data.t_ldsity_values set is_latest = false where id in
						(select id from w_tlv where value_identic = false and id is not null)	
						)
		insert into target_data.t_ldsity_values(plot,available_datasets,value,creation_time,is_latest,ext_version)
		select
				w_tlv.plot_upr as plot,
				w_tlv.available_datasets_upr as available_datasets,
				w_tlv.value_upr as value,
				now() as creation_time,
				true as is_latest,
				''',_ext_version_text,''' as ext_version
		from
				w_tlv
		where
				w_tlv.value_identic = false
		order
				by w_tlv.plot_upr, w_tlv.available_datasets_upr;
		'),'{',''),'}','');	
	
	_query_res_big := _query_res_big || _string_check_insert;

	---------------------------------------------
	---------------------------------------------
	select coalesce(max(id),0) from target_data.t_ldsity_values into _max_id_tlv;
	---------------------------------------------
	---------------------------------------------
	execute ''||_query_res_big||'' using _categorization_setups, _array_id;
	---------------------------------------------
	---------------------------------------------
	_res := concat('The ',(select count(*) from target_data.t_ldsity_values where id > _max_id_tlv),' new local densities were prepared for ',(select count(t.plot) from (select distinct plot from target_data.t_ldsity_values where id > _max_id_tlv) as t),' plots.');
	---------------------------------------------
	---------------------------------------------
	return _res;
	---------------------------------------------
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_ldsity_values(integer[], integer[], integer[], double precision) IS
'The function for the specified list of input arguments inserts data into the t_available_datasets table and inserts data into the t_ldsity_values table (aggregated local density at the plot level).';

grant execute on function target_data.fn_import_ldsity_values(integer[], integer[], integer[], double precision) to public;
-- </function>