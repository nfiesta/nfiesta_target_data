--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

----------------------------------------------
-- DDL
-----------------------------------------------
-- drop unique on label categories
ALTER TABLE target_data.c_area_domain_category DROP CONSTRAINT ukey__c_area_domain_category__label;
ALTER TABLE target_data.c_area_domain_category DROP CONSTRAINT ukey__c_area_domain_category__label_en;
ALTER TABLE target_data.c_sub_population_category DROP CONSTRAINT ukey__c_sub_population_category__label;
ALTER TABLE target_data.c_sub_population_category DROP CONSTRAINT ukey__c_sub_population_category__label_en;

-- cm_spc2classrule2panel_refyearset
CREATE TABLE target_data.cm_spc2classrule2panel_refyearset(
id				serial,
spc2classification_rule 	integer NOT NULL,
refyearset2panel		integer NOT NULL,
CONSTRAINT pkey__cm_spc2classrule2panel_refyearset__id PRIMARY KEY (id)
);

ALTER TABLE target_data.cm_spc2classrule2panel_refyearset ADD CONSTRAINT
fkey__cm_spc2classrule2panel_refyearset__spc2classrule FOREIGN KEY (spc2classification_rule)
REFERENCES target_data.cm_spc2classification_rule (id)
ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE target_data.cm_spc2classrule2panel_refyearset ADD CONSTRAINT
fkey__cm_spc2classrule2panel_refyearset__refyearset2panel FOREIGN KEY (refyearset2panel)
REFERENCES sdesign.cm_refyearset2panel_mapping (id)
ON UPDATE CASCADE ON DELETE NO ACTION;

CREATE INDEX fki__cm_spc2classrule2panel_refyearset__spc2classrule ON target_data.cm_spc2classrule2panel_refyearset USING btree(spc2classification_rule);
CREATE INDEX fki__cm_spc2classrule2panel_refyearset__refyearset2panel ON target_data.cm_spc2classrule2panel_refyearset USING btree(refyearset2panel);

COMMENT ON TABLE target_data.cm_spc2classrule2panel_refyearset IS 'Mapping table between classification rules and panels/reference year sets. Its purpose is to determine the availability of classification rules for specific panels nad reference year sets.';
COMMENT ON COLUMN target_data.cm_spc2classrule2panel_refyearset.id IS 'Identifier of the record, primary key.';
COMMENT ON COLUMN target_data.cm_spc2classrule2panel_refyearset.spc2classification_rule IS 'Identifier of the classification rule, foreign key to table cm_spc2classification_rule.';
COMMENT ON COLUMN target_data.cm_spc2classrule2panel_refyearset.refyearset2panel IS 'Identifier of the panel and reference year set combination, foreign key to table cm_refyearset2panel_mapping.';

SELECT pg_catalog.pg_extension_config_dump('target_data.cm_spc2classrule2panel_refyearset','');
SELECT pg_catalog.pg_extension_config_dump('target_data.cm_spc2classrule2panel_refyearset_id_seq','');

GRANT SELECT ON TABLE 				target_data.cm_spc2classrule2panel_refyearset TO PUBLIC;
GRANT SELECT, USAGE ON SEQUENCE 		target_data.cm_spc2classrule2panel_refyearset_id_seq TO PUBLIC;

GRANT SELECT ON TABLE				 target_data.cm_spc2classrule2panel_refyearset TO app_nfiesta;
GRANT SELECT, USAGE ON SEQUENCE			 target_data.cm_spc2classrule2panel_refyearset_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.cm_spc2classrule2panel_refyearset TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.cm_spc2classrule2panel_refyearset_id_seq TO app_nfiesta_mng;

-- cm_adc2classrule2panel_refyearset
CREATE TABLE target_data.cm_adc2classrule2panel_refyearset(
id				serial,
adc2classification_rule 	integer NOT NULL,
refyearset2panel		integer NOT NULL,
CONSTRAINT pkey__cm_adc2classrule2panel_refyearset__id PRIMARY KEY (id)
);

ALTER TABLE target_data.cm_adc2classrule2panel_refyearset ADD CONSTRAINT
fkey__cm_adc2classrule2panel_refyearset__adc2classrule FOREIGN KEY (adc2classification_rule)
REFERENCES target_data.cm_adc2classification_rule (id)
ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE target_data.cm_adc2classrule2panel_refyearset ADD CONSTRAINT
fkey__cm_adc2classrule2panel_refyearset__refyearset2panel FOREIGN KEY (refyearset2panel)
REFERENCES sdesign.cm_refyearset2panel_mapping (id)
ON UPDATE CASCADE ON DELETE NO ACTION;

CREATE INDEX fki__cm_adc2classrule2panel_refyearset__adc2classrule ON target_data.cm_adc2classrule2panel_refyearset USING btree(adc2classification_rule);
CREATE INDEX fki__cm_adc2classrule2panel_refyearset__refyearset2panel ON target_data.cm_adc2classrule2panel_refyearset USING btree(refyearset2panel);

COMMENT ON TABLE target_data.cm_adc2classrule2panel_refyearset IS 'Mapping table between classification rules and panels/reference year sets. Its purpose is to determine the availability of classification rules for specific panels nad reference year sets.';
COMMENT ON COLUMN target_data.cm_adc2classrule2panel_refyearset.id IS 'Identifier of the record, primary key.';
COMMENT ON COLUMN target_data.cm_adc2classrule2panel_refyearset.adc2classification_rule IS 'Identifier of the classification rule, foreign key to table cm_adc2classification_rule.';
COMMENT ON COLUMN target_data.cm_adc2classrule2panel_refyearset.refyearset2panel IS 'Identifier of the panel and reference year set combination, foreign key to table cm_refyearset2panel_mapping.';

SELECT pg_catalog.pg_extension_config_dump('target_data.cm_adc2classrule2panel_refyearset','');
SELECT pg_catalog.pg_extension_config_dump('target_data.cm_adc2classrule2panel_refyearset_id_seq','');

GRANT SELECT ON TABLE 				target_data.cm_adc2classrule2panel_refyearset TO PUBLIC;
GRANT SELECT, USAGE ON SEQUENCE 		target_data.cm_adc2classrule2panel_refyearset_id_seq TO PUBLIC;

GRANT SELECT ON TABLE				 target_data.cm_adc2classrule2panel_refyearset TO app_nfiesta;
GRANT SELECT, USAGE ON SEQUENCE			 target_data.cm_adc2classrule2panel_refyearset_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.cm_adc2classrule2panel_refyearset TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.cm_adc2classrule2panel_refyearset_id_seq TO app_nfiesta_mng;

----------------------------------------------
-- Functions
-----------------------------------------------

DROP FUNCTION target_data.fn_save_indicator(integer, varchar, text, varchar, text, integer);
DROP FUNCTION target_data.fn_get_topic(integer);

-- <function name="fn_get_classification_rule4adc" schema="target_data" src="functions/fn_get_classification_rule4adc.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4adc
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4adc(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_classification_rule4adc(_area_domain_category integer DEFAULT NULL::integer, _ldsity_object integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
area_domain_category	integer,
ldsity_object		integer,
classification_rule	text
)
AS
$$
BEGIN
	IF _area_domain_category IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.area_domain_category, t1.ldsity_object, t1.classification_rule
		FROM target_data.cm_adc2classification_rule AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain_category AS t1 WHERE t1.id = _area_domain_category)
		THEN RAISE EXCEPTION 'Given area_domain_category does not exist in table c_area_domain_category (%)', _area_domain_category;
		END IF;

		IF _ldsity_object IS NOT NULL
		THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
			THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
			END IF;

			RETURN QUERY
			SELECT t4.id, t1.id AS area_domain_category, t2.ldsity_object, t2.classification_rule
			FROM target_data.c_area_domain_category AS t1
			LEFT JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.id = t2.area_domain_category
			WHERE t1.id = _area_domain_category AND t2.ldsity_object = _ldsity_object
			ORDER BY t2.id;
		ELSE
			RETURN QUERY
			SELECT t4.id, t1.id AS area_domain_category, t2.ldsity_object, t2.classification_rule
			FROM target_data.c_area_domain_category AS t1
			LEFT JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.id = t2.area_domain_category
			WHERE t1.id = _area_domain_category
			ORDER BY t2.id;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_classification_rule4adc(integer, integer) IS
'Function returns records from cm_adc2classification_rule table, optionally for given area_domain_category and/or ldsity object.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_classification_rule4adc(integer, integer) TO public;

-- </function>

-- <function name="fn_get_classification_rule4spc" schema="target_data" src="functions/fn_get_classification_rule4spc.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4spc
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4spc(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_classification_rule4spc(_sub_population_category integer DEFAULT NULL::integer, _ldsity_object integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
sub_population_category	integer,
ldsity_object		integer,
classification_rule	text
)
AS
$$
BEGIN
	IF _sub_population_category IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.sub_population_category, t1.ldsity_object, t1.classification_rule
		FROM target_data.cm_spc2classification_rule AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population_category AS t1 WHERE t1.id = _sub_population_category)
		THEN RAISE EXCEPTION 'Given sub_population_category does not exist in table c_sub_population_category (%)', _sub_population_category;
		END IF;

		IF _ldsity_object IS NOT NULL
		THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
			THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
			END IF;

			RETURN QUERY
			SELECT t4.id, t1.id AS sub_population_category, t2.ldsity_object, t2.classification_rule
			FROM target_data.c_sub_population_category AS t1
			LEFT JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.id = _sub_population_category AND t2.ldsity_object = _ldsity_object
			ORDER BY t2.id;
		ELSE
			RETURN QUERY
			SELECT t4.id, t1.id AS sub_population_category, t2.ldsity_object, t2.classification_rule
			FROM target_data.c_sub_population_category AS t1
			LEFT JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.id = _sub_population_category
			ORDER BY t2.id;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_classification_rule4spc(integer, integer) IS
'Function returns records from cm_spc2classification_rule table, optionally for given sub_population_category and/or ldsity object.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_classification_rule4spc(integer, integer) TO public;

-- </function>

-- <function name="fn_check_classification_rule_syntax" schema="target_data" src="functions/fn_check_classification_rule_syntax.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rule_syntax
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_check_classification_rule_syntax(integer, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rule_syntax(_ldsity_object integer, _rule text)
RETURNS boolean 
AS
$$
DECLARE
_table_name	varchar;
_test		boolean;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
	THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
	END IF;

	SELECT table_name
	FROM target_data.c_ldsity_object
	WHERE id = _ldsity_object
	INTO _table_name;

	BEGIN
	EXECUTE
	'SELECT EXISTS(SELECT *
	FROM '||_table_name||'
	WHERE '||_rule ||'
	)'
	INTO _test;

	IF _test IS NOT NULL 
	THEN _test := true;
	ELSE _test := false; 
	END IF;

	EXCEPTION WHEN OTHERS THEN
		_test := false;
	END;

	RETURN _test;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rule_syntax(integer, text) IS
'Function checks syntax in text field with classification rule.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rule_syntax(integer, text) TO public;

-- </function>

-- <function name="fn_check_classification_rule" schema="target_data" src="functions/fn_check_classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_check_classification_rule(integer, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rule(_ldsity integer, _rule text)
--boolean
RETURNS TABLE (
no_of_rules_met	integer,
no_of_objects	integer
) 
AS
$$
DECLARE
_ldsity_object		integer;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	_ldsity_object := (SELECT ldsity_object FROM target_data.c_ldsity WHERE id = _ldsity);

	SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
	INTO _test;

	IF _test = false
	THEN
		RAISE EXCEPTION 'Given rule has an invalid syntax.';
	END IF;
	
	_case := 'CASE WHEN '||_rule||' THEN true ELSE false END';

-- panels and reference year sets must be specified before the check

-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			information_schema.constraint_column_usage AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	WITH w_tables AS (
		SELECT
			t1.id, 	concat('(SELECT * FROM ', t1.table_name, 
				CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
				') AS ', _table_names_ws[t1.id]) AS table_select
		FROM
			unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
		LEFT JOIN
			(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
			FROM
				(SELECT t4.table_name, t3.classification_rule 
				FROM target_data.c_ldsity AS t1,
					unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
				LEFT JOIN target_data.cm_adc2classification_rule AS t3
				ON t2.rule = t3.id
				INNER JOIN target_data.c_ldsity_object AS t4
				ON t3.ldsity_object = t4.id
				WHERE t1.id = _ldsity
				UNION ALL
				SELECT t4.table_name, t3.classification_rule 
				FROM target_data.c_ldsity AS t1,
					unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
				LEFT JOIN target_data.cm_spc2classification_rule AS t3
				ON t2.rule = t3.id
				INNER JOIN target_data.c_ldsity_object AS t4
				ON t3.ldsity_object = t4.id
				WHERE t1.id = _ldsity
				) AS t1
			GROUP BY table_name
			) AS t2
		ON t1.table_name = t2.table_name
	)
	SELECT array_agg(table_select ORDER BY id)
	FROM w_tables
	INTO _table_selects;

	SELECT concat(' FROM ', _table_selects[1]) INTO _table1;
	FOR i IN 2..array_length(_table_selects,1) 
	LOOP
		SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
		INTO _join;

		_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
		--raise notice '%', _join_all;
	END LOOP;
	-- raise notice '%', _join_all;


	_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';

	_q := 'SELECT ' || _pkey || ', ' || '1' || ' AS rule_number, ' || _case || ' AS rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
--raise notice '%, %', _table1, _join_all;

--	raise notice '%',_q_all;

	RETURN QUERY EXECUTE
	'WITH w AS (' || _q || '),
	w2 AS (SELECT
			id, rul, sum(CASE WHEN rul=true THEN 1 ELSE 0 END) AS no_of_rules_met
		FROM
			w
		GROUP BY id, rul
	)
	SELECT no_of_rules_met::int, count(*)::int AS no_of_objects
	FROM w2
	GROUP BY no_of_rules_met';

--return query select true, _no_of_rules_met, _no_of_objects;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rule(integer, text) IS
'Function returns number of objects classified by the rule for given ldsity and attribute_type hierarchy.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rule(integer, text) TO public;

-- </function>

-- <function name="fn_check_classification_rules" schema="target_data" src="functions/fn_check_classification_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_check_classification_rules(integer, text, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rules(_ldsity integer, _rules text[], _panel_refyearset integer DEFAULT NULL::integer)
--boolean
RETURNS TABLE (
result		boolean,
message_id	integer,
message		text
) 
AS
$$
DECLARE
_ldsity_object		integer;
_rule			text;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_cases			varchar[];
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_q_all			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	_ldsity_object := (SELECT ldsity_object FROM target_data.c_ldsity WHERE id = _ldsity);

	FOREACH _rule IN ARRAY _rules
	LOOP
		SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
		INTO _test;

		_test_all := _test_all || array[_test];

		_case := 'CASE WHEN '||_rule||' THEN true ELSE false END';

		_cases := _cases || _case;
		--raise notice '%', _test_all;
	END LOOP;

	IF _test_all @> array[false]
	THEN
		RAISE EXCEPTION 'One or more given rules has an invalid syntax.';
	END IF;

-- panels and reference year sets must be specified before the check

-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			information_schema.constraint_column_usage AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	WITH w_tables AS (
		SELECT
			t1.id, 	concat('(SELECT * FROM ', t1.table_name, 
				CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
				') AS ', _table_names_ws[t1.id]) AS table_select
		FROM
			unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
		LEFT JOIN
			(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
			FROM
				(SELECT t4.table_name, t3.classification_rule 
				FROM target_data.c_ldsity AS t1,
					unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
				LEFT JOIN target_data.cm_adc2classification_rule AS t3
				ON t2.rule = t3.id
				INNER JOIN target_data.c_ldsity_object AS t4
				ON t3.ldsity_object = t4.id
				WHERE t1.id = _ldsity
				UNION ALL
				SELECT t4.table_name, t3.classification_rule 
				FROM target_data.c_ldsity AS t1,
					unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
				LEFT JOIN target_data.cm_spc2classification_rule AS t3
				ON t2.rule = t3.id
				INNER JOIN target_data.c_ldsity_object AS t4
				ON t3.ldsity_object = t4.id
				WHERE t1.id = _ldsity
				) AS t1
			GROUP BY table_name
			) AS t2
		ON t1.table_name = t2.table_name
	)
	SELECT array_agg(table_select ORDER BY id)
	FROM w_tables
	INTO _table_selects;

	SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', _table_names_ws[1], '.', _primary_keys[1], '
		FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
		INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
		INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
		INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
		_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) INTO _table1;

	FOR i IN 2..array_length(_table_selects,1) 
	LOOP
		SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
		INTO _join;

		_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
		--raise notice '%', _join_all;
	END LOOP;
	-- raise notice '%', _join_all;


	_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';

	FOR i IN 1..array_length(_cases,1)
	LOOP
		_q := 'SELECT panel_refyearset, ' || _pkey || ', ' || i || ' AS rule_number, ' || _cases[i] || ' AS rul' || _table1 || _join_all;

		_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL ' ELSE '' END), _q);
	END LOOP;

--	raise notice '%',_q_all;

	EXECUTE
	'WITH w AS (' || _q_all || '),
	w2 AS (SELECT
			id, rul, sum(CASE WHEN rul=true THEN 1 ELSE 0 END) AS no_of_rules_met
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
		GROUP BY id, rul
	), w_test AS (
		SELECT no_of_rules_met::int, count(*)::int AS no_of_objects
		FROM w2
		GROUP BY no_of_rules_met
	)
	SELECT
		array_agg(no_of_rules_met ORDER BY no_of_rules_met) AS no_of_rules_met,
		array_agg(no_of_objects ORDER BY no_of_rules_met) AS no_of_objects
	FROM
		w_test
	'
	INTO _no_of_rules_met, _no_of_objects;

--raise notice '%, %', _no_of_rules_met, _no_of_objects;

	_total := (SELECT sum(nob) FROM unnest(_no_of_objects) AS t(nob));

--return query select true, _no_of_rules_met, _no_of_objects;

	-- if both are NULL 
IF _no_of_rules_met IS NULL AND _no_of_objects IS NULL
THEN	
	RETURN QUERY 
		SELECT false, 0, concat('There were no records on which the rules could be tested - given combination of panels and reference year sets probably does not correspond with the local density contribution availability.')::text;
ELSE
-- if one rule given - all records have 1 rule met
	IF array_length(_rules,1) = 1
	THEN
		IF array_length(_no_of_rules_met,1) != 1
		THEN
			RETURN QUERY 
			SELECT false, 1, concat('Only one rule was given, so all records has to result into true. There were only ', _no_of_objects[2], ' number of objects successfully categorized from ', _total, ' overall total.')::text;
		ELSE
			IF _no_of_rules_met[1] = 0
			THEN
				RETURN QUERY
				SELECT false, 2, concat('Only one rule was given, and none of the ', _no_of_objects[1], ' number of objects was successfully categorized.')::text;
			ELSE
			--raise notice '%, %', _no_of_rules_met, _no_of_objects;
				RETURN QUERY 
				SELECT true, 3, concat('Only one rule was given and every object was successfully categorized by it. All from ', _no_of_objects[1], ' number of objects resulted in ', _total, ' successful records.')::text;
			END IF;
		END IF;
	END IF;


-- if 2 or more rules given - all records have 1 rule met and 0 rule met
	-- both numbers must be the same (logically if 1 object belogns to some rule, basically also fails to the other rules)

	IF array_length(_rules,1) > 1
	THEN
		-- there are objects with 2 or more rules met (intersection between rules, can be from 0,1,2 or 1,2)
		IF EXISTS(SELECT * FROM unnest(_no_of_rules_met) AS t(nor) WHERE nor >= 2)
		THEN
			RETURN QUERY 
			SELECT false, 4, concat('Some of the objects were categorized by more than one rule, there is an intersection between the rule conditions.')::text;
		ELSE
			-- there are objects which are not covered by rules (only 0,1)
			IF array_length(_no_of_rules_met,1) = 2
			THEN
				IF _no_of_objects[1] != _no_of_objects[2]
				THEN
					RETURN QUERY 
					SELECT false, 5, concat('Some of the objects (', _no_of_objects[1] - _no_of_objects[2], ' from total of ', _no_of_objects[1], ') were not successfully categorized by any of the rules. Consider editing hierarchy.')::text;
				ELSE
					-- the rest, where 0,1, both the same number of objects
					IF _no_of_objects[1] = _no_of_objects[2]
					THEN
						RETURN QUERY
						SELECT true, 6, concat('Every object was succesfully categorized without any intersection.')::text;
					ELSE
						--raise notice '%, %', _no_of_rules_met, _no_of_objects;
						RAISE EXCEPTION '01: Not known state of classification rules.';
					END IF;
				END IF;
			ELSE
				IF array_length(_no_of_rules_met,1) = 1 AND _no_of_rules_met[1] = 0
				THEN
					RETURN QUERY 
					SELECT false, 7, concat('None of the ', _no_of_objects[1], ' was successfully categorized by any of the rules.')::text;
				ELSE
					RAISE EXCEPTION '02: Not known state of classification rules.';
				END IF;
			END IF;
		END IF;
	END IF;
END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rules(integer, text[], integer) IS
'Function checks syntax in text field with classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rules(integer, text[], integer) TO public;

-- </function>

-- <function name="fn_get_ldsity_object4ld_object_adsp" schema="target_data" src="functions/fn_get_ldsity_object4ld_object_adsp.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object4ld_object_adsp
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(_ldsity_object integer, _areal_or_population integer, _id integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
classification_rule	boolean
)
AS
$$
BEGIN
	IF _ldsity_object IS NULL
	THEN
		RAISE EXCEPTION 'Parameter _ldsity_object is mandatory and must not be null!';
	END IF;

	IF NOT EXISTS (SELECT * FROM target_data.c_areal_or_population AS t1 WHERE t1.id = _areal_or_population)
	THEN
		RAISE EXCEPTION 'Areal or population value not found in c_areal_or_population!';
	END IF;

	RETURN QUERY
	WITH w AS (
		SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	)
	SELECT 	t3.id, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, 
		CASE WHEN t4.id IS NOT NULL OR t5.id IS NOT NULL THEN true END AS rule
	FROM w AS t1,
		unnest(t1.ldsity_objects) AS t2(ldsity_object)
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	LEFT JOIN 
		(SELECT t1.id, t2.ldsity_object, t2.classification_rule
		FROM target_data.c_area_domain_category AS t1
		INNER JOIN target_data.cm_adc2classification_rule AS t2
		ON t1.id = t2.area_domain_category
		WHERE t1.area_domain = _id) AS t4
	ON t3.id = t4.ldsity_object
	LEFT JOIN --target_data.cm_spc2classification_rule AS t5
		(SELECT t1.id, t2.ldsity_object, t2.classification_rule
		FROM target_data.c_sub_population_category AS t1
		INNER JOIN target_data.cm_spc2classification_rule AS t2
		ON t1.id = t2.sub_population_category
		WHERE t1.sub_population = _id) AS t5
	ON t3.id = t5.ldsity_object
	WHERE areal_or_population = _areal_or_population;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) IS
'Function returns records from c_ldsity_object table, optionally for given topic.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) TO public;

-- </function>

-- <function name="fn_get_attribute_domain" schema="target_data" src="functions/fn_get_attribute_domain.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_attribute_domain(varchar(2), integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_attribute_domain
(
	in _attr_type varchar(2), -- 'ad' or 'sp'
    IN _attr_types integer[],
    IN _attribute_domain integer[] DEFAULT NULL::integer[]
)
  RETURNS TABLE(attr_1 integer[], attr_2 integer[], attr_3 integer[], attr_4 integer[], attr_5 integer[]) AS
$BODY$
	DECLARE
		_at_count		integer;
		_attr_1			integer[];
		_attr_2			integer[];
		_attr_3			integer[];
		_attr_4			integer[];
		_attr_5			integer[];
	
		_table_name					text;
		_table_name_category		text;
		_column_name				text;
		_table_name_hierarchy		text;
	BEGIN

		if _attr_type is null
		then
			raise exception 'Error 01: fn_get_attribute_domain: The input argument _attr_type must not be NULL!';
		end if;
	
		if not(_attr_type = any(array['ad','sp']))
		then
			raise exception 'Error 02: fn_get_attribute_domain: Allowed values for the input argument _attr_type are "ad" or "sp" !';
		end if;
	
		if _attr_type = 'ad'
		then
			_table_name := 'c_area_domain';
			_table_name_category := 'c_area_domain_category';
			_column_name := 'area_domain';
			_table_name_hierarchy := 't_adc_hierarchy';
		end if;
	
		if _attr_type = 'sp'
		then
			_table_name := 'c_sub_population';
			_table_name_category := 'c_sub_population_category';
			_column_name := 'sub_population';
			_table_name_hierarchy := 't_spc_hierarchy';
		end if;

	CASE	WHEN _attr_types IS NULL THEN

			RAISE EXCEPTION 'Error 03: fn_get_attribute_domain: The function cannot be called without selling at least one attribute type!';

		WHEN array_length(_attr_types,1) IS DISTINCT FROM array_length(array_remove(_attr_types,NULL),1) THEN

			RAISE EXCEPTION 'Error 04: fn_get_attribute_domain: At least one of the passed attribute types is NULL (attr_types = %)!', _attr_types;
	ELSE
		_at_count := array_length(_attr_types,1);
	END CASE;


-----------------------------------------------------------------------------------------------------------------------
-- 1 attribute
	CASE
		WHEN _at_count = 1 THEN

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0 AS attr1
			)
			SELECT
				array_agg(attr1 ORDER BY attr1),
				NULL::integer[],
				NULL::integer[],
				NULL::integer[],
				NULL::integer[]
			FROM
				w_attr1
			WHERE
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1;

----------------------------------------------------------------------------------------------------------------------
-- 2 attributes
		WHEN _at_count = 2 THEN 

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0, $1[1]
			),
			w_attr2 AS
			(
				SELECT id AS attr2, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[2]
				UNION SELECT 0, $1[2]
			),
			w_const AS
			(	SELECT	t1.ordinality::integer,
					t1.attribute_type,
					t2.id,
					t3.variable
				FROM	unnest($1) WITH ORDINALITY AS t1(attribute_type,ordinality)
				INNER JOIN
					(
					SELECT attr1 AS id, attribute_type FROM w_attr1
					UNION ALL
					SELECT attr2 AS id, attribute_type FROM w_attr2
					) AS t2
				ON
					t1.attribute_type = t2.attribute_type
				LEFT JOIN
					target_data.'||_table_name_hierarchy||' AS t3
				ON
					t2.id = t3.variable_superior
			),
			w_desc AS(
				SELECT	t1.ordinality, t1.attribute_type, t1.id, t1.variable
				FROM	w_const AS t1
				INNER JOIN
					w_const AS t2
				ON t1.variable = t2.id
				WHERE	t1.ordinality < t2.ordinality
			),
			w_total AS (
				SELECT count(*) AS total
				FROM (SELECT DISTINCT t1.ordinality FROM w_desc AS t1 GROUP BY t1.ordinality) as t1
			),
			w_ordi AS (
				SELECT array_agg(t1.ordinality ORDER BY t1.ordinality) AS ordi_agg, total
				FROM
					(SELECT DISTINCT t1.ordinality, total
					FROM w_desc AS t1, w_total) AS t1
				GROUP BY total
			),
			w_attr AS(
				SELECT	attr1, attr2
				FROM	w_attr1, w_attr2
			),
			w_exclude AS (
				SELECT
					variable_superior AS attr_ex
				FROM
					target_data.'||_table_name_hierarchy||'
				WHERE
					variable IS NULL
			),
			w_all AS (
				SELECT
					attr1,
					attr2
				FROM
					w_attr
				WHERE
					CASE WHEN NOT EXISTS (SELECT total FROM w_ordi) THEN true
					END
					OR
					CASE WHEN 1 = (SELECT total FROM w_ordi) THEN 
						CASE WHEN ARRAY[1] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0)
						END
					END
				ORDER BY attr1, attr2
			)
			SELECT
				array_agg(attr1 ORDER BY attr1,attr2),
				array_agg(attr2 ORDER BY attr1,attr2),
				NULL::integer[],
				NULL::integer[],
				NULL::integer[]
			FROM
				w_all
			WHERE
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr2 = 0 ELSE TRUE END AND
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1] AND
					attr2 = $2[2]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1, _attr_2;

-----------------------------------------------------------------------------------------------------------------------
-- 3 attributes
		WHEN _at_count = 3 THEN

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0, $1[1]
			),
			w_attr2 AS
			(
				SELECT id AS attr2, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[2]
				UNION SELECT 0, $1[2]
			),
			w_attr3 AS
			(
				SELECT id AS attr3, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[3]
				UNION SELECT 0, $1[3]
			),
			w_const AS
			(	SELECT	t1.ordinality::integer,
					t1.attribute_type,
					t2.id,
					t3.variable
				FROM	unnest($1) WITH ORDINALITY AS t1(attribute_type,ordinality)
				INNER JOIN
					(
					SELECT attr1 AS id, attribute_type FROM w_attr1
					UNION ALL
					SELECT attr2 AS id, attribute_type FROM w_attr2
					UNION ALL
					SELECT attr3 AS id, attribute_type FROM w_attr3
					) AS t2
				ON
					t1.attribute_type = t2.attribute_type
				LEFT JOIN
					target_data.'||_table_name_hierarchy||' AS t3
				ON
					t2.id = t3.variable_superior
				),
			w_desc AS(
				SELECT	t1.ordinality, t1.attribute_type, t1.id, t1.variable
				FROM	w_const AS t1
				INNER JOIN
					w_const AS t2
				ON t1.variable = t2.id
				WHERE	t1.ordinality < t2.ordinality
			),
			w_total_comb AS (
				SELECT count(*) AS total
				FROM (SELECT DISTINCT t1.ordinality FROM w_desc AS t1 GROUP BY t1.ordinality) as t1
			),
			w_ordi AS (
				SELECT array_agg(t1.ordinality ORDER BY t1.ordinality) AS ordi_agg, total
				FROM
					(SELECT DISTINCT t1.ordinality, total
					FROM w_desc AS t1, w_total_comb) AS t1
				GROUP BY total
			),
			w_attr AS(
				SELECT	attr1,attr2,attr3
				FROM	w_attr1, w_attr2, w_attr3
			),
			w_exclude AS (
				SELECT
					variable_superior AS attr_ex
				FROM
					target_data.'||_table_name_hierarchy||'
				WHERE
					variable IS NULL
			),
			w_all AS (
				SELECT	attr1,
					attr2,
					attr3
				FROM
					w_attr
				WHERE
					CASE WHEN NOT EXISTS (SELECT total FROM w_ordi) THEN true
					END
					OR
					CASE WHEN 1 = (SELECT total FROM w_ordi) THEN 
						CASE WHEN ARRAY[1] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0)
						END OR
						CASE WHEN ARRAY[2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END
					END
					OR
					CASE WHEN 2 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END
					END
				ORDER BY attr1, attr2, attr3
			)
			SELECT
				array_agg(attr1 ORDER BY attr1,attr2,attr3),
				array_agg(attr2 ORDER BY attr1,attr2,attr3),
				array_agg(attr3 ORDER BY attr1,attr2,attr3),
				NULL::integer[],
				NULL::integer[]
			FROM
				w_all
			WHERE
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr2 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND

				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1] AND
					attr2 = $2[2] AND
					attr3 = $2[3]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1, _attr_2, _attr_3;

-----------------------------------------------------------------------------------------------------------------------
-- 4 attributes
		WHEN _at_count = 4 THEN

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0, $1[1]
			),
			w_attr2 AS
			(
				SELECT id AS attr2, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[2]
				UNION SELECT 0, $1[2]
			),
			w_attr3 AS
			(
				SELECT id AS attr3, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[3]
				UNION SELECT 0, $1[3]
			),
			w_attr4 AS
			(
				SELECT id AS attr4, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[4]
				UNION SELECT 0, $1[4]
			),
			w_const AS
			(	SELECT	t1.ordinality::integer,
					t1.attribute_type,
					t2.id,
					t3.variable
				FROM	unnest($1) WITH ORDINALITY AS t1(attribute_type,ordinality)
				INNER JOIN
					(
					SELECT attr1 AS id, attribute_type FROM w_attr1
					UNION ALL
					SELECT attr2 AS id, attribute_type FROM w_attr2
					UNION ALL
					SELECT attr3 AS id, attribute_type FROM w_attr3
					UNION ALL
					SELECT attr4 AS id, attribute_type FROM w_attr4
					) AS t2
				ON
					t1.attribute_type = t2.attribute_type
				LEFT JOIN
					target_data.'||_table_name_hierarchy||' AS t3
				ON
					t2.id = t3.variable_superior
				),
			w_desc AS(
				SELECT	t1.ordinality, t1.attribute_type, t1.id, t1.variable
				FROM	w_const AS t1
				INNER JOIN
					w_const AS t2
				ON t1.variable = t2.id
				WHERE	t1.ordinality < t2.ordinality
			),
			w_total_comb AS (
				SELECT count(*) AS total
				FROM (SELECT DISTINCT t1.ordinality FROM w_desc AS t1 GROUP BY t1.ordinality) as t1
			),
			w_ordi AS (
				SELECT array_agg(t1.ordinality ORDER BY t1.ordinality) AS ordi_agg, total
				FROM
					(SELECT DISTINCT t1.ordinality, total
					FROM w_desc AS t1, w_total_comb) AS t1
				GROUP BY total
			),
			w_attr AS(
				SELECT	attr1,attr2,attr3,attr4
				FROM	w_attr1, w_attr2, w_attr3, w_attr4
			),
			w_exclude AS (
				SELECT
					variable_superior AS attr_ex
				FROM
					target_data.'||_table_name_hierarchy||'
				WHERE
					variable IS NULL
			),
			w_all AS (
				SELECT	attr1,
					attr2,
					attr3,
					attr4
				FROM	w_attr
				WHERE
					CASE WHEN NOT EXISTS (SELECT total FROM w_ordi) THEN true
					END
					OR
					CASE WHEN 1 = (SELECT total FROM w_ordi) THEN 
						CASE WHEN ARRAY[1] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0)
						END OR
						CASE WHEN ARRAY[2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END OR
						CASE WHEN ARRAY[3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END
					END
					OR
					CASE WHEN 2 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END OR
						CASE WHEN ARRAY[1,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[2,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END
					END
					OR
					CASE WHEN 3 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END
					END
				ORDER BY attr1,attr2,attr3,attr4
			)
			SELECT	array_agg(attr1 ORDER BY attr1,attr2,attr3,attr4),
				array_agg(attr2 ORDER BY attr1,attr2,attr3,attr4),
				array_agg(attr3 ORDER BY attr1,attr2,attr3,attr4),
				array_agg(attr4 ORDER BY attr1,attr2,attr3,attr4),
				NULL::integer[]
			FROM
				w_all
			WHERE
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr2 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND

				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND

				CASE WHEN attr3 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1] AND
					attr2 = $2[2] AND
					attr3 = $2[3] AND
					attr4 = $2[4]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1, _attr_2, _attr_3, _attr_4;

-----------------------------------------------------------------------------------------------------------------------
-- 5 attributes
		WHEN _at_count = 5 THEN

			execute '
			WITH w_attr1 AS
			(
				SELECT id AS attr1, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[1]
				UNION SELECT 0, $1[1]
			),
			w_attr2 AS
			(
				SELECT id AS attr2, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[2]
				UNION SELECT 0, $1[2]
			),
			w_attr3 AS
			(
				SELECT id AS attr3, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[3]
				UNION SELECT 0, $1[3]
			),
			w_attr4 AS
			(
				SELECT id AS attr4, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[4]
				UNION SELECT 0, $1[4]
			),
			w_attr5 AS
			(
				SELECT id AS attr5, '||_column_name||' as attribute_type
				FROM target_data.'||_table_name_category||'
				WHERE '||_column_name||' = $1[5]
				UNION SELECT 0, $1[5]
			),
			w_const AS
			(	SELECT	t1.ordinality::integer,
					t1.attribute_type,
					t2.id,
					t3.variable
				FROM	unnest($1) WITH ORDINALITY AS t1(attribute_type,ordinality)
				INNER JOIN
					(
					SELECT attr1 AS id, attribute_type FROM w_attr1
					UNION ALL
					SELECT attr2 AS id, attribute_type FROM w_attr2
					UNION ALL
					SELECT attr3 AS id, attribute_type FROM w_attr3
					UNION ALL
					SELECT attr4 AS id, attribute_type FROM w_attr4
					UNION ALL
					SELECT attr5 AS id, attribute_type FROM w_attr5
					) AS t2
				ON
					t1.attribute_type = t2.attribute_type
				LEFT JOIN
					target_data.'||_table_name_hierarchy||' AS t3
				ON
					t2.id = t3.variable_superior
				),
			w_desc AS(
				SELECT	t1.ordinality, t1.attribute_type, t1.id, t1.variable
				FROM	w_const AS t1
				INNER JOIN
					w_const AS t2
				ON t1.variable = t2.id
				WHERE	t1.ordinality < t2.ordinality
			),
			w_total_comb AS (
				SELECT count(*) AS total
				FROM (SELECT DISTINCT t1.ordinality FROM w_desc AS t1 GROUP BY t1.ordinality) as t1
			),
			w_ordi AS (
				SELECT array_agg(t1.ordinality ORDER BY t1.ordinality) AS ordi_agg, total
				FROM
					(SELECT DISTINCT t1.ordinality, total
					FROM w_desc AS t1, w_total_comb) AS t1
				GROUP BY total
			),
			w_attr AS(
				SELECT	attr1,attr2,attr3,attr4,attr5
				FROM	w_attr1, w_attr2, w_attr3, w_attr4, w_attr5
			),
			w_exclude AS (
				SELECT
					variable_superior AS attr_ex
				FROM
					target_data.'||_table_name_hierarchy||'
				WHERE
					variable IS NULL
			),
			w_all AS (
				SELECT	attr1,
					attr2,
					attr3,
					attr4,
					attr5
				FROM	w_attr
				WHERE
					CASE WHEN NOT EXISTS (SELECT total FROM w_ordi) THEN true
					END
					OR
					CASE WHEN 1 = (SELECT total FROM w_ordi) THEN 
						CASE WHEN ARRAY[1] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0)
						END OR
						CASE WHEN ARRAY[2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END OR
						CASE WHEN ARRAY[3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END
					END
					OR
					CASE WHEN 2 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0)
						END OR
						CASE WHEN ARRAY[1,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[1,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END OR
						CASE WHEN ARRAY[2,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[2,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END OR
						CASE WHEN ARRAY[3,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END
					END
					OR
					CASE WHEN 3 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2,3] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0)
						END OR
						CASE WHEN ARRAY[1,3,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END OR
						CASE WHEN ARRAY[2,3,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END OR
						CASE WHEN ARRAY[1,2,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END
					END
					OR
					CASE WHEN 4 = (SELECT total FROM w_ordi) THEN
						CASE WHEN ARRAY[1,2,3,4] = (SELECT ordi_agg FROM w_ordi) THEN
									(array[attr1,attr2] IN (SELECT array[id,variable] FROM w_desc) OR (attr1 = 0 AND attr2 = 0) OR attr2 = 0) AND
									(array[attr2,attr3] IN (SELECT array[id,variable] FROM w_desc) OR (attr2 = 0 AND attr3 = 0) OR attr3 = 0) AND
									(array[attr3,attr4] IN (SELECT array[id,variable] FROM w_desc) OR (attr3 = 0 AND attr4 = 0) OR attr4 = 0) AND
									(array[attr4,attr5] IN (SELECT array[id,variable] FROM w_desc) OR (attr4 = 0 AND attr5 = 0) OR attr5 = 0)
						END
					END
				ORDER BY attr1,attr2,attr3,attr4,attr5
			)
			SELECT	array_agg(attr1 ORDER BY attr1,attr2,attr3,attr4,attr5),
				array_agg(attr2 ORDER BY attr1,attr2,attr3,attr4,attr5),
				array_agg(attr3 ORDER BY attr1,attr2,attr3,attr4,attr5),
				array_agg(attr4 ORDER BY attr1,attr2,attr3,attr4,attr5),
				array_agg(attr5 ORDER BY attr1,attr2,attr3,attr4,attr5)
			FROM
				w_all
			WHERE
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr2 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND
				CASE WHEN attr1 IN (SELECT attr_ex FROM w_exclude) THEN attr5 = 0 ELSE TRUE END AND

				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr3 = 0 ELSE TRUE END AND
				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND
				CASE WHEN attr2 IN (SELECT attr_ex FROM w_exclude) THEN attr5 = 0 ELSE TRUE END AND

				CASE WHEN attr3 IN (SELECT attr_ex FROM w_exclude) THEN attr4 = 0 ELSE TRUE END AND
				CASE WHEN attr3 IN (SELECT attr_ex FROM w_exclude) THEN attr5 = 0 ELSE TRUE END AND

				CASE WHEN attr4 IN (SELECT attr_ex FROM w_exclude) THEN attr5 = 0 ELSE TRUE END AND
				CASE WHEN
					$2 IS NOT NULL
				THEN
					attr1 = $2[1] AND
					attr2 = $2[2] AND
					attr3 = $2[3] AND
					attr4 = $2[4] AND
					attr5 = $2[5]
				ELSE
					true
				END
			'
			using _attr_types, _attribute_domain
			INTO _attr_1, _attr_2, _attr_3, _attr_4, _attr_5;
		ELSE
			RAISE EXCEPTION 'Error 05: The function is not implemented for the specified number of attribute types (at_count = %)',_at_count;
		END CASE;

		IF _attribute_domain IS NOT NULL
		THEN
			IF _attr_1 IS NULL THEN RAISE EXCEPTION 'Error 06: No combination between attribute types was found for the specified attribute domain (attribute_types = %, attribute_domain = %).', _attr_types, _attribute_domain;
			END IF;
		END IF;

		RETURN QUERY SELECT _attr_1, _attr_2, _attr_3, _attr_4, _attr_5;		
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  COMMENT ON FUNCTION target_data.fn_get_attribute_domain(varchar(2), integer[], integer[]) IS
'The function returns the attribute domain field table for the specified attribute type combination.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_attribute_domain (varchar(2), integer[], integer[]) TO app_nfiesta;

-- </function>

DROP FUNCTION target_data.fn_save_areal_or_pop(integer, varchar, text, varchar, text);

-- <function name="fn_save_areal_or_pop" schema="target_data" src="functions/fn_save_areal_or_pop.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_areal_or_pop
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_areal_or_pop(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_areal_or_pop(_areal_or_population integer, _label varchar(200), _description text, _label_en varchar(200) DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text, _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
BEGIN
	IF _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of label or description is null (%, %).', _label, _description;
	END IF; 

	IF _id IS NULl
	THEN
		CASE 
		WHEN _areal_or_population = 100 THEN
			INSERT INTO target_data.c_area_domain(label, description, label_en, description_en)
			SELECT _label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;

		WHEN _areal_or_population = 200 THEN
			INSERT INTO target_data.c_sub_population(label, description, label_en, description_en)
			SELECT _label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	ELSE
		CASE WHEN _areal_or_population = 100 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _id;
			END IF;

			UPDATE target_data.c_area_domain
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_area_domain.id = _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain does not exist in table c_sub_population (%)', _id;
			END IF;

			UPDATE target_data.c_sub_population_category
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_sub_population_category.id = _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_areal_or_pop(integer, character varying, text, character varying, text, integer) IS
'Functions inserts a record into table c_area_domain or c_sub_population based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_areal_or_pop(integer, character varying, text, character varying, text, integer) TO public;

-- </function>

-- <function name="fn_get_panel_refyearset" schema="target_data" src="functions/fn_get_panel_refyearset.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_panel_refyearset
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_panel_refyearset(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_panel_refyearset(_panel_refyearset integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
panel_id		integer,
panel			varchar(20),
panel_label		varchar(120),
refyearset_id		integer,
reference_year_set	varchar(20),
refyearset_label	varchar(120)
)
AS
$$
BEGIN
	IF _panel_refyearset IS NULL
	THEN
		RETURN QUERY
		SELECT t2.id, t1.id, t1.panel, t1.label, t3.id, t3.reference_year_set, t3.label
		FROM sdesign.t_panel AS t1
		INNER JOIN sdesign.cm_refyearset2panel_mapping AS t2
		ON t1.id = t2.panel
		INNER JOIN sdesign.t_reference_year_set AS t3
		ON t2.reference_year_set = t3.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM sdesign.cm_refyearset2panel_mapping AS t1 WHERE t1.id = _panel_refyearset)
		THEN RAISE EXCEPTION 'Given combination of panel and reference year set does not exist in table cm_refyearset2panel_mapping (%)', _panel_refyearset;
		END IF;

		RETURN QUERY
		SELECT t2.id, t1.id, t1.panel, t1.label, t3.id, t3.reference_year_set, t3.label
		FROM sdesign.t_panel AS t1
		INNER JOIN sdesign.cm_refyearset2panel_mapping AS t2
		ON t1.id = t2.panel
		INNER JOIN sdesign.t_reference_year_set AS t3
		ON t2.reference_year_set = t3.id
		WHERE t2.id = _panel_refyearset;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_panel_refyearset(integer) IS
'Function returns records from cm_refyearset2panel_mappping table, optionally for given panel_refyearset.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_panel_refyearset(integer) TO public;

-- </function>

-- <function name="fn_get_target_variable" schema="target_data" src="functions/fn_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_target_variable(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_target_variable(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
state_or_change		integer,
soc_label		character varying(200),
soc_description		text
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, t1.state_or_change, t2.label, t2.description
		FROM target_data.c_target_variable AS t1
		INNER JOIN target_data.c_state_or_change AS t2
		ON t1.state_or_change = t2.id; 	
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		SELECT DISTINCT t6.id, t1.id AS ldsity_object_group, t6.label, t6.description, t6.label_en, t6.description_en, t6.state_or_change, t7.label, t7.description
		FROM target_data.c_ldsity_object_group AS t1
		INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
		ON t1.id = t2.ldsity_object_group
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN target_data.c_ldsity AS t4
		ON t3.id = t4.ldsity_object
		INNER JOIN target_data.cm_ldsity2target_variable AS t5
		ON t4.id = t5.ldsity
		INNER JOIN target_data.c_target_variable AS t6
		ON t5.target_variable = t6.id
		INNER JOIN target_data.c_state_or_change AS t7
		ON t6.state_or_change = t7.id
		WHERE t1.id = _ldsity_object_group;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_target_variable(integer) IS
'Function returns records from c_target_variable table, optionally for given ldsity_object_group.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_target_variable(integer) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_get_definition_variant(integer) CASCADE;
-- <function name="fn_get_definition_variant" schema="target_data" src="functions/fn_get_definition_variant.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_definition_variant
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_definition_variant(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_definition_variant(_id integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_definition_variant AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_definition_variant AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given definition variant does not exist in table c_definition_variant (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_definition_variant AS t1
		WHERE t1.id = _id
		ORDER BY t2.id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_definition_variant(integer) IS
'Function returns records from c_definition_variant table, optionally for given id.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_definition_variant(integer) TO public;

-- </function>

-- <function name="fn_get_definition_variant4ldsity" schema="target_data" src="functions/fn_get_definition_variant4ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_definition_variant4ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_definition_variant4ldsity(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_definition_variant4ldsity(_ldsity integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
ldsity		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _ldsity IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_definition_variant AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
		END IF;

		RETURN QUERY
		SELECT t3.id, t1.id AS ldsity, t3.label, t3.description, t3.label_en, t3.description_en
		FROM target_data.c_ldsity AS t1,
		unnest(t1.definition_variant) WITH ORDINALITY AS t2(def, id)
		LEFT JOIN target_data.c_definition_variant AS t3
		ON t2.def = t3.id
		WHERE t1.id = _ldsity
		ORDER BY t2.id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_definition_variant4ldsity(integer) IS
'Function returns records from c_definition_variant table, optionally for given ldsity.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_definition_variant4ldsity(integer) TO public;

-- </function>
