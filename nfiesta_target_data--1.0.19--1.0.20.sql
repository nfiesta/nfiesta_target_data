--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

----------------------------------------------
-- DDL
-----------------------------------------------
-- sequences on area_domain?


----------------------------------------------
-- Functions
-----------------------------------------------

-- <function name="fn_get_definition_variant" schema="target_data" src="functions/fn_get_definition_variant.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_definition_variant
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_definition_variant(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_definition_variant(_id integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_definition_variant AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_definition_variant AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given definition variant does not exist in table c_definition_variant (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_definition_variant AS t1
		WHERE t1.id = _id
		ORDER BY t1.id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_definition_variant(integer) IS
'Function returns records from c_definition_variant table, optionally for given id.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_definition_variant(integer) TO public;

-- </function>

-- <function name="fn_try_delete_target_variable" schema="target_data" src="functions/fn_try_delete_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_delete_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_try_delete_target_variable(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_delete_target_variable(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _id)
	THEN RAISE EXCEPTION 'Given target variable does not exist in table c_target_variable (%)', _id;
	END IF;

	RETURN NOT EXISTS (
		SELECT t1.id
		FROM target_data.c_target_variable AS t1
		INNER JOIN target_data.cm_ldsity2target_variable AS t2
		ON t1.id = t2.ldsity
		WHERE t1.id = _id);
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_delete_target_variable(integer) IS
'Function provides test if it is possible to delete records from c_target_variable table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_delete_target_variable(integer) TO public;

-- </function>


DROP FUNCTION IF EXISTS target_data.fn_check_classification_rule(integer, text) CASCADE;
-- <function name="fn_check_classification_rule" schema="target_data" src="functions/fn_check_classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rule(_ldsity integer, _ldsity_object integer, _rule text, _panel_refyearset integer DEFAULT NULL::integer, _adc integer[] DEFAULT NULL::integer[], _spc integer[] DEFAULT NULL::integer[])
--boolean
RETURNS TABLE (
no_of_rules_met	integer,
no_of_objects	integer
) 
AS
$$
DECLARE
_ldsity_objects		integer[];
_adc_rule_test		integer[];
_spc_rule_test		integer[];
_table_name4rule	varchar;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
	INTO _test;

	IF _test = false
	THEN
		RAISE EXCEPTION 'Given rule has an invalid syntax.';
	END IF;
	
	_case := 'CASE WHEN '||_rule||' THEN true ELSE false END';

	_ldsity_objects := (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]));

	WITH w AS (
		SELECT _ldsity_objects AS ldsity_objects
	)
	SELECT array_agg(t1.rule ORDER BY t1.id) 
	FROM unnest(_adc) WITH ORDINALITY AS t1(rule, id)
	INNER JOIN target_data.cm_adc2classification_rule AS t2
	ON t1.rule = t2.id
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	INNER JOIN w AS t4
	ON ARRAY[t3.id] <@ t4.ldsity_objects
	INTO _adc_rule_test;

	IF _adc_rule_test != _adc
	THEN
		RAISE EXCEPTION 'Given area domain category classification rules does not meet the ldsity objects within hierarchy of local density contribution. Rules which can be used = %, rules given = %.', _adc_rule_test, _adc;
	END IF;

	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	)
	SELECT array_agg(t1.rule ORDER BY t1.id) 
	FROM unnest(_spc) WITH ORDINALITY AS t1(rule, id)
	INNER JOIN target_data.cm_spc2classification_rule AS t2
	ON t1.rule = t2.id
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	INNER JOIN w AS t4
	ON ARRAY[t3.id] <@ t4.ldsity_objects
	INTO _spc_rule_test;

	IF _spc_rule_test != _spc
	THEN
		RAISE EXCEPTION 'Given sub population category classification rules does not meet the ldsity objects within hierarchy of local density contribution. Rules which can be used = %, rules given = %.', _spc_rule_test, _spc;
	END IF;

-- panels and reference year sets must be specified before the check

-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			information_schema.constraint_column_usage AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	_table_name4rule := (SELECT table_name FROM target_data.c_ldsity_object WHERE id = _ldsity_object);

	WITH w_tables AS (
			SELECT
				t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
					CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
					CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',1, ' AS rule_number, ', _case,' AS rul ') ELSE '' END,
					' FROM ', t1.table_name, 
					CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
					') AS ', _table_names_ws[t1.id]) AS table_select
			FROM
				unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
			LEFT JOIN
				(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
				FROM
					(SELECT t4.table_name, t3.classification_rule 
					FROM target_data.c_ldsity AS t1,
						unnest(t1.area_domain_category || _adc) WITH ORDINALITY AS t2(rule, id)
					LEFT JOIN target_data.cm_adc2classification_rule AS t3
					ON t2.rule = t3.id
					INNER JOIN target_data.c_ldsity_object AS t4
					ON t3.ldsity_object = t4.id
					WHERE t1.id = _ldsity
					UNION ALL
					SELECT t4.table_name, t3.classification_rule 
					FROM target_data.c_ldsity AS t1,
						unnest(t1.sub_population_category || _spc) WITH ORDINALITY AS t2(rule, id)
					LEFT JOIN target_data.cm_spc2classification_rule AS t3
					ON t2.rule = t3.id
					INNER JOIN target_data.c_ldsity_object AS t4
					ON t3.ldsity_object = t4.id
					WHERE t1.id = _ldsity
					) AS t1
				GROUP BY table_name
				) AS t2
			ON t1.table_name = t2.table_name
		)
		SELECT array_agg(table_select ORDER BY id)
		FROM w_tables
		INTO _table_selects;

		SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', _table_names_ws[1], '.', _primary_keys[1], '
			FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
			INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
			INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
			INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
			_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
		INTO _table1;

		_join_all := NULL; _join := NULL;

		FOR i IN 2..array_length(_table_selects,1) 
		LOOP
			SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
			INTO _join;

			_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
			--raise notice '%', _join_all;
		END LOOP;
		--raise notice '%', _join_all;

		--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
		_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

		_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || _join_all;

	RETURN QUERY EXECUTE
	'WITH w AS (' || _q || '),
	w2 AS (SELECT
			id, rul, sum(CASE WHEN rul=true THEN 1 ELSE 0 END) AS no_of_rules_met
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
		GROUP BY id, rul
	)
	SELECT no_of_rules_met::int, count(*)::int AS no_of_objects
	FROM w2
	GROUP BY no_of_rules_met';

--return query select true, _no_of_rules_met, _no_of_objects;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) IS
'Function returns number of objects classified by the rule for given ldsity and attribute_type hierarchy.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_check_classification_rules(integer, text, integer) CASCADE;
-- <function name="fn_check_classification_rules" schema="target_data" src="functions/fn_check_classification_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rules(_ldsity integer, _ldsity_object integer, _rules text[], _panel_refyearset integer DEFAULT NULL::integer, _adc integer[] DEFAULT NULL::integer[], _spc integer[] DEFAULT NULL::integer[])
--boolean
RETURNS TABLE (
result		boolean,
message_id	integer,
message		text
) 
AS
$$
DECLARE
_ldsity_objects		integer[];
_adc_rule_test		integer[];
_spc_rule_test		integer[];
_table_name4rule	varchar;
_rule			text;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_cases			varchar[];
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_q_all			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	IF NOT array[_ldsity_object] <@ (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]))
	THEN
		RAISE EXCEPTION 'Given ldsity object (%) is not found within the hierarchy of ldsity contribution (%).', _ldsity_object, _ldsity;
	END IF;

	FOREACH _rule IN ARRAY _rules
	LOOP
		SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
		INTO _test;

		_test_all := _test_all || array[_test];

		_case := 'CASE WHEN '||_rule||' THEN true ELSE false END';

		_cases := _cases || _case;
		--raise notice '%', _test_all;
	END LOOP;

	IF _test_all @> array[false]
	THEN
		RAISE EXCEPTION 'One or more given rules has an invalid syntax.';
	END IF;

	_ldsity_objects := (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]));

	WITH w AS (
		SELECT _ldsity_objects AS ldsity_objects
	)
	SELECT array_agg(t1.rule ORDER BY t1.id) 
	FROM unnest(_adc) WITH ORDINALITY AS t1(rule, id)
	INNER JOIN target_data.cm_adc2classification_rule AS t2
	ON t1.rule = t2.id
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	INNER JOIN w AS t4
	ON ARRAY[t3.id] <@ t4.ldsity_objects
	INTO _adc_rule_test;

	IF _adc_rule_test != _adc
	THEN
		RAISE EXCEPTION 'Given area domain category classification rules does not meet the ldsity objects within hierarchy of local density contribution. Rules which can be used = %, rules given = %.', _adc_rule_test, _adc;
	END IF;

	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	)
	SELECT array_agg(t1.rule ORDER BY t1.id) 
	FROM unnest(_spc) WITH ORDINALITY AS t1(rule, id)
	INNER JOIN target_data.cm_spc2classification_rule AS t2
	ON t1.rule = t2.id
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	INNER JOIN w AS t4
	ON ARRAY[t3.id] <@ t4.ldsity_objects
	INTO _spc_rule_test;

	IF _spc_rule_test != _spc
	THEN
		RAISE EXCEPTION 'Given sub population category classification rules does not meet the ldsity objects within hierarchy of local density contribution. Rules which can be used = %, rules given = %.', _spc_rule_test, _spc;
	END IF;

-- panels and reference year sets must be specified before the check

-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			information_schema.constraint_column_usage AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	_table_name4rule := (SELECT table_name FROM target_data.c_ldsity_object WHERE id = _ldsity_object);

	FOR i IN 1..array_length(_cases,1)
	LOOP
		WITH w_tables AS (
			SELECT
				t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
					CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
					CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',i, ' AS rule_number, ', _cases[i],' AS rul ') ELSE '' END,
					' FROM ', t1.table_name, 
					CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
					') AS ', _table_names_ws[t1.id]) AS table_select
			FROM
				unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
			LEFT JOIN
				(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
				FROM
					(SELECT t4.table_name, t3.classification_rule 
					FROM target_data.c_ldsity AS t1,
						unnest(t1.area_domain_category || _adc) WITH ORDINALITY AS t2(rule, id)
					LEFT JOIN target_data.cm_adc2classification_rule AS t3
					ON t2.rule = t3.id
					INNER JOIN target_data.c_ldsity_object AS t4
					ON t3.ldsity_object = t4.id
					WHERE t1.id = _ldsity
					UNION ALL
					SELECT t4.table_name, t3.classification_rule 
					FROM target_data.c_ldsity AS t1,
						unnest(t1.sub_population_category || _spc) WITH ORDINALITY AS t2(rule, id)
					LEFT JOIN target_data.cm_spc2classification_rule AS t3
					ON t2.rule = t3.id
					INNER JOIN target_data.c_ldsity_object AS t4
					ON t3.ldsity_object = t4.id
					WHERE t1.id = _ldsity
					) AS t1
				GROUP BY table_name
				) AS t2
			ON t1.table_name = t2.table_name
		)
		SELECT array_agg(table_select ORDER BY id)
		FROM w_tables
		INTO _table_selects;

		SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', _table_names_ws[1], '.', _primary_keys[1], '
			FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
			INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
			INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
			INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
			_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
		INTO _table1;

		_join_all := NULL; _join := NULL;

		FOR i IN 2..array_length(_table_selects,1) 
		LOOP
			SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
			INTO _join;

			_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
			--raise notice '%', _join_all;
		END LOOP;
		--raise notice '%', _join_all;

		--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
		_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

		_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || _join_all;

		_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL ' ELSE '' END), _q);

	END LOOP;

	--raise notice '%',_q_all;

	EXECUTE
	'WITH w AS (' || _q_all || '),
	w2 AS (SELECT
			id, rul, sum(CASE WHEN rul=true THEN 1 ELSE 0 END) AS no_of_rules_met
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
		GROUP BY id, rul
	), w_test AS (
		SELECT no_of_rules_met::int, count(*)::int AS no_of_objects
		FROM w2
		GROUP BY no_of_rules_met
	)
	SELECT
		array_agg(no_of_rules_met ORDER BY no_of_rules_met) AS no_of_rules_met,
		array_agg(no_of_objects ORDER BY no_of_rules_met) AS no_of_objects
	FROM
		w_test
	'
	INTO _no_of_rules_met, _no_of_objects;

--raise notice '%, %', _no_of_rules_met, _no_of_objects;

	_total := (SELECT sum(nob) FROM unnest(_no_of_objects) AS t(nob));

--return query select true, _no_of_rules_met, _no_of_objects;

	-- if both are NULL 
IF _no_of_rules_met IS NULL AND _no_of_objects IS NULL
THEN	
	RETURN QUERY 
		SELECT false, 0, concat('There were no records on which the rules could be tested - given combination of panels and reference year sets probably does not correspond with the local density contribution availability.')::text;
ELSE
-- if one rule given - all records have 1 rule met
	IF array_length(_rules,1) = 1
	THEN
		IF array_length(_no_of_rules_met,1) != 1
		THEN
			RETURN QUERY 
			SELECT false, 1, concat('Only one rule was given, so all records has to result into true. There were only ', _no_of_objects[2], ' number of objects successfully categorized from ', _total, ' overall total.')::text;
		ELSE
			IF _no_of_rules_met[1] = 0
			THEN
				RETURN QUERY
				SELECT false, 2, concat('Only one rule was given, and none of the ', _no_of_objects[1], ' number of objects was successfully categorized.')::text;
			ELSE
			--raise notice '%, %', _no_of_rules_met, _no_of_objects;
				RETURN QUERY 
				SELECT true, 3, concat('Only one rule was given and every object was successfully categorized by it. All from ', _no_of_objects[1], ' number of objects resulted in ', _total, ' successful records.')::text;
			END IF;
		END IF;
	END IF;


-- if 2 or more rules given - all records have 1 rule met and 0 rule met
	-- both numbers must be the same (logically if 1 object belogns to some rule, basically also fails to the other rules)

	IF array_length(_rules,1) > 1
	THEN
		-- there are objects with 2 or more rules met (intersection between rules, can be from 0,1,2 or 1,2)
		IF EXISTS(SELECT * FROM unnest(_no_of_rules_met) AS t(nor) WHERE nor >= 2)
		THEN
			RETURN QUERY 
			SELECT false, 4, concat('Some of the objects were categorized by more than one rule, there is an intersection between the rule conditions.')::text;
		ELSE
			-- there are objects which are not covered by rules (only 0,1)
			IF array_length(_no_of_rules_met,1) = 2
			THEN
				IF _no_of_objects[1] != _no_of_objects[2]
				THEN
					RETURN QUERY 
					SELECT false, 5, concat('Some of the objects (', _no_of_objects[1] - _no_of_objects[2], ' from total of ', _no_of_objects[1], ') were not successfully categorized by any of the rules. Consider editing hierarchy.')::text;
				ELSE
					-- the rest, where 0,1, both the same number of objects
					IF _no_of_objects[1] = _no_of_objects[2]
					THEN
						RETURN QUERY
						SELECT true, 6, concat('Every object was succesfully categorized without any intersection.')::text;
					ELSE
						--raise notice '%, %', _no_of_rules_met, _no_of_objects;
						RAISE EXCEPTION '01: Not known state of classification rules.';
					END IF;
				END IF;
			ELSE
				IF array_length(_no_of_rules_met,1) = 1 AND _no_of_rules_met[1] = 0
				THEN
					RETURN QUERY 
					SELECT false, 7, concat('None of the ', _no_of_objects[1], ' was successfully categorized by any of the rules.')::text;
				ELSE
					RAISE EXCEPTION '02: Not known state of classification rules.';
				END IF;
			END IF;
		END IF;
	END IF;
END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[], integer[]) IS
'Function checks syntax in text field with classification rules for given ldsity contribution and ldsity object within its hierarchy. Optionally also for given panel x reference year set combination and/or  area domain/sub population category/ies.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[],  integer[]) TO public;

-- </function>

-- <function name="fn_get_classification_rule4adc" schema="target_data" src="functions/fn_get_classification_rule4adc.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4adc
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4adc(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_classification_rule4adc(_area_domain_category integer DEFAULT NULL::integer, _ldsity_object integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
area_domain_category	integer,
ldsity_object		integer,
classification_rule	text
)
AS
$$
BEGIN
	IF _area_domain_category IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.area_domain_category, t1.ldsity_object, t1.classification_rule
		FROM target_data.cm_adc2classification_rule AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain_category AS t1 WHERE t1.id = _area_domain_category)
		THEN RAISE EXCEPTION 'Given area_domain_category does not exist in table c_area_domain_category (%)', _area_domain_category;
		END IF;

		IF _ldsity_object IS NOT NULL
		THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
			THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
			END IF;

			RETURN QUERY
			SELECT t2.id, t1.id AS area_domain_category, t2.ldsity_object, t2.classification_rule
			FROM target_data.c_area_domain_category AS t1
			LEFT JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.id = t2.area_domain_category
			WHERE t1.id = _area_domain_category AND t2.ldsity_object = _ldsity_object
			ORDER BY t2.id;
		ELSE
			RETURN QUERY
			SELECT t2.id, t1.id AS area_domain_category, t2.ldsity_object, t2.classification_rule
			FROM target_data.c_area_domain_category AS t1
			LEFT JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.id = t2.area_domain_category
			WHERE t1.id = _area_domain_category
			ORDER BY t2.id;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_classification_rule4adc(integer, integer) IS
'Function returns records from cm_adc2classification_rule table, optionally for given area_domain_category and/or ldsity object.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_classification_rule4adc(integer, integer) TO public;

-- </function>

-- <function name="fn_get_classification_rule4spc" schema="target_data" src="functions/fn_get_classification_rule4spc.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4spc
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4spc(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_classification_rule4spc(_sub_population_category integer DEFAULT NULL::integer, _ldsity_object integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
sub_population_category	integer,
ldsity_object		integer,
classification_rule	text
)
AS
$$
BEGIN
	IF _sub_population_category IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.sub_population_category, t1.ldsity_object, t1.classification_rule
		FROM target_data.cm_spc2classification_rule AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population_category AS t1 WHERE t1.id = _sub_population_category)
		THEN RAISE EXCEPTION 'Given sub_population_category does not exist in table c_sub_population_category (%)', _sub_population_category;
		END IF;

		IF _ldsity_object IS NOT NULL
		THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
			THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
			END IF;

			RETURN QUERY
			SELECT t2.id, t1.id AS sub_population_category, t2.ldsity_object, t2.classification_rule
			FROM target_data.c_sub_population_category AS t1
			LEFT JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.id = _sub_population_category AND t2.ldsity_object = _ldsity_object
			ORDER BY t2.id;
		ELSE
			RETURN QUERY
			SELECT t2.id, t1.id AS sub_population_category, t2.ldsity_object, t2.classification_rule
			FROM target_data.c_sub_population_category AS t1
			LEFT JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.id = _sub_population_category
			ORDER BY t2.id;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_classification_rule4spc(integer, integer) IS
'Function returns records from cm_spc2classification_rule table, optionally for given sub_population_category and/or ldsity object.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_classification_rule4spc(integer, integer) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object4adsp(integer, integer) CASCADE;
-- <function name="fn_get_ldsity_object4adsp" schema="target_data" src="functions/fn_get_ldsity_object4adsp.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object4adsp
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object4adsp(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object4adsp(_ldsity_object integer, _areal_or_population integer)
RETURNS TABLE (
id			integer,
ldsity_object		integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
areal_or_population	integer
)
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
	THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object_group (%)', _ldsity_object;
	END IF;

	RETURN QUERY
	WITH w_hier AS (
		SELECT _ldsity_object, target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	)
	SELECT t3.id, t1.ldsity_object, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, t3.areal_or_population
	FROM w_hier AS t1,
		unnest(t1.ldsity_objects) AS t2(ldsity_object)
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	WHERE t3.areal_or_population = _areal_or_population;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object4adsp(integer, integer) IS
'Function returns records from c_ldsity_object table, optionally for given ldsity object. Returned records belong to the hierarchy of given ldsity object. Result can be subsetted by areal or populational character.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object4adsp(integer, integer) TO public;

-- </function>


