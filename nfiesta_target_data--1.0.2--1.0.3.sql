--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-----------------------------------------------
-- DDL
-----------------------------------------------
-- lookup for topics
CREATE TABLE target_data.c_topic (
id		serial		NOT NULL,
label		varchar(200) 	NOT NULL,
description	text		NOT NULL,
label_en	varchar(200),
description_en	text,
CONSTRAINT pkey__c_topic__id PRIMARY KEY (id)
);

GRANT SELECT ON TABLE target_data.c_topic TO PUBLIC;
GRANT SELECT, USAGE ON SEQUENCE target_data.c_topic_id_seq TO PUBLIC;

COMMENT ON TABLE target_data.c_topic IS 'Lookup table with topics (thematic groups).';
COMMENT ON COLUMN target_data.c_topic.id IS 'Identificator of the category.';
COMMENT ON COLUMN target_data.c_topic.label IS 'Label of the category.';
COMMENT ON COLUMN target_data.c_topic.description IS 'Detailed description of the category.';
COMMENT ON COLUMN target_data.c_topic.label_en IS 'English label of the category.';
COMMENT ON COLUMN target_data.c_topic.description_en IS 'Detailed English description of the category.';

CREATE TABLE target_data.cm_target_variable2topic (
id			serial NOT NULL,
topic			integer NOT NULL,
target_variable		integer NOT NULL,
CONSTRAINT pkey__cm_target_variable2topic__id PRIMARY KEY (id)
);

COMMENT ON TABLE target_data.cm_target_variable2topic IS 'Table with mapping between topic and target_variable.';
COMMENT ON COLUMN target_data.cm_target_variable2topic.id IS 'Identifier of the record.';
COMMENT ON COLUMN target_data.cm_target_variable2topic.topic IS 'Identifier of the topic, foreing key to table c_topic.';
COMMENT ON COLUMN target_data.cm_target_variable2topic.target_variable IS 'Identifier of the target_variable, foreing key to table c_target_variable.';

ALTER TABLE target_data.cm_target_variable2topic ADD CONSTRAINT
fkey__cm_target_variable2topic__c_topic FOREIGN KEY (topic)
REFERENCES target_data.c_topic (id);
COMMENT ON CONSTRAINT fkey__cm_target_variable2topic__c_topic ON target_data.cm_target_variable2topic IS
'Foreign key to table c_topic.';

ALTER TABLE target_data.cm_target_variable2topic ADD CONSTRAINT
fkey__cm_target_variable2topic__c_target_variable FOREIGN KEY (target_variable)
REFERENCES target_data.c_target_variable (id);
COMMENT ON CONSTRAINT fkey__cm_target_variable2topic__c_target_variable ON target_data.cm_target_variable2topic IS
'Foreign key to table c_target_variable.';

-- add usage on sequences
GRANT USAGE ON SEQUENCE			 target_data.c_area_domain_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.c_area_domain_category_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.c_definition_variant_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.c_indicator_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.c_ldsity_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.c_ldsity_object_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.c_sub_population_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.c_sub_population_category_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.c_target_variable_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.c_unit_of_measure_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.cm_adc2classification_rule_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.cm_ldsity2target_variable_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.cm_spc2classification_rule_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.t_adc_hierarchy_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.t_available_datasets_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.t_ldsity_values_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.t_spc_hierarchy_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.c_panel_refyearset_group_id_seq TO public;
GRANT USAGE ON SEQUENCE			 target_data.t_panel_refyearset_group_id_seq TO public;

GRANT USAGE ON SEQUENCE			 target_data.c_target_variable_id_seq TO app_nfiesta;
GRANT USAGE ON SEQUENCE			 target_data.c_unit_of_measure_id_seq TO app_nfiesta;
GRANT USAGE ON SEQUENCE			 target_data.c_indicator_id_seq TO app_nfiesta;
GRANT USAGE ON SEQUENCE			 target_data.c_ldsity_id_seq TO app_nfiesta;
GRANT USAGE ON SEQUENCE			 target_data.c_ldsity_object_id_seq TO app_nfiesta;
GRANT USAGE ON SEQUENCE			 target_data.cm_ldsity2target_variable_id_seq TO app_nfiesta;
GRANT USAGE ON SEQUENCE			 target_data.c_panel_refyearset_group_id_seq TO app_nfiesta;
GRANT USAGE ON SEQUENCE			 target_data.t_panel_refyearset_group_id_seq TO app_nfiesta;

-- new tables
SELECT pg_catalog.pg_extension_config_dump('target_data.c_topic', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_topic_id_seq', '');

GRANT SELECT ON TABLE				 target_data.c_topic TO app_nfiesta;
GRANT SELECT, USAGE ON SEQUENCE			 target_data.c_topic_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_topic TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_topic_id_seq TO app_nfiesta_mng;

-- schema usage
GRANT USAGE ON SCHEMA target_data TO PUBLIC;
GRANT USAGE ON SCHEMA target_data TO app_nfiesta;
GRANT CREATE ON SCHEMA target_data TO app_nfiesta;

GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA target_data TO PUBLIC;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA target_data TO app_nfiesta;

-----------------------------------------------
-- Functions
-----------------------------------------------
-- <function name="fn_get_topic" schema="target_data" src="functions/fn_get_topic.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_topic
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_topic(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_topic(_id integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_topic AS t1;
	ELSE
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_topic AS t1
		WHERE t1.id = _id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_topic(integer) IS
'Function returns records from c_topic table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_topic(integer) TO public;

-- </function>

-- <function name="fn_get_indicator" schema="target_data" src="functions/fn_get_indicator.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_indicator
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_indicator(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_indicator(_topic integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
topic		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _topic IS NULL
	THEN
		RETURN QUERY
		SELECT DISTINCT t1.id, t3.topic, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_indicator AS t1
		INNER JOIN target_data.c_target_variable AS t2
		ON t1.id = t2.indicator
		INNER JOIN target_data.cm_target_variable2topic AS t3
		ON t2.id = t3.target_variable;
	ELSE
		RETURN QUERY
		SELECT DISTINCT t1.id, t3.topic, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_indicator AS t1
		INNER JOIN target_data.c_target_variable AS t2
		ON t1.id = t2.indicator
		INNER JOIN target_data.cm_target_variable2topic AS t3
		ON t2.id = t3.target_variable
		WHERE t3.topic = _topic;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_indicator(integer) IS
'Function returns records from c_indicator table, optionally for given topic.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_indicator(integer) TO public;

-- </function>

-- <function name="fn_save_topic" schema="target_data" src="functions/fn_save_topic.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_topic
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_topic(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_topic(_label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text, _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
DECLARE
	_topic integer;
BEGIN
	IF _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory parameter label (%) or description (%) is NULL!', _label, _description;
	END IF;

	IF _id IS NULL THEN
		INSERT INTO target_data.c_topic(label, description, label_en, description_en)
		SELECT _label, _description, _label_en, _description_en
		RETURNING id
		INTO _topic;
	ELSE
		UPDATE target_data.c_topic
		SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
		WHERE c_topic.id = _id;

		_topic := _id;
	END IF;

	RETURN _topic;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_topic(character varying, text, character varying, text, integer) IS
'Function provides insert into c_topic table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_topic(character varying, text, character varying, text, integer) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_save_indicator(integer, character varying, text, character varying, text) CASCADE;
-- <function name="fn_save_indicator" schema="target_data" src="functions/fn_save_indicator.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_indicator
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_indicator(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_indicator(_state_or_change integer, _label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text, _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
DECLARE
	_indicator integer;
BEGIN
	IF _state_or_change IS NULL OR _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory parameters state_or_change (%) or label (%) or description (%) is NULL!', _state_or_change, _label, _description;
	END IF;

	IF _id IS NULL THEN
		INSERT INTO target_data.c_indicator(state_or_change, label, description, label_en, description_en)
		SELECT _state_or_change, _label, _description, _label_en, _description_en
		RETURNING id
		INTO _indicator;
	ELSE
		UPDATE target_data.c_indicator
		SET 	state_or_change = _state_or_change, label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
		WHERE c_indicator.id = _id;

		_indicator := _id;
	END IF;

	RETURN _indicator;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_indicator(integer, character varying, text, character varying, text, integer) IS
'Function provides insert into c_indicator table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_indicator(integer, character varying, text, character varying, text, integer) TO public;


-- </function>

DROP FUNCTION IF EXISTS target_data.fn_save_target_variable(integer, integer[], integer[]) CASCADE;

-- <function name="fn_save_target_variable" schema="target_data" src="functions/fn_save_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_target_variable(integer[], integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_target_variable(_topics integer[], _indicator integer, _ldsity integer[], _ldsity_object_type integer[])
RETURNS integer
AS
$$
DECLARE
	_target_variable integer;
BEGIN
	IF _topics IS NULL OR _indicator IS NULL OR _ldsity IS NULL
	THEN
		RAISE EXCEPTION 'Some of input parameters are NULL (topics=%,indicator=%,ldsity=%)!',_topics,_indicator,_ldsity;
	END IF;

	IF array_length(_ldsity,1) != array_length(_ldsity_object_type,1)
	THEN
		RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity, _ldsity_object_type;
	END IF;

	INSERT INTO target_data.c_target_variable(indicator)
	SELECT _indicator
	RETURNING id
	INTO _target_variable;

	INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category)
	SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[]
	FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
	INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
	ON t1.id = t2.id;

	INSERT INTO target_data.cm_target_variable2topic (topic, target_variable)
	SELECT unnest(_topics), _target_variable;

	RETURN _target_variable;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_target_variable(integer[], integer, integer[], integer[]) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_target_variable(integer[], integer, integer[], integer[]) TO public;

-- </function>



