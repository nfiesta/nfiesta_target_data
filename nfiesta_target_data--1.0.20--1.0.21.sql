--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

----------------------------------------------
-- DDL
-----------------------------------------------
-- sequences on area_domain

----------------------------------------------
-- Functions
-----------------------------------------------
DROP FUNCTION target_data.fn_check_classification_rules(integer, text[], integer);

-- <function name="fn_get_hierarchy" schema="target_data" src="functions/fn_get_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_hierarchy(integer, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_hierarchy(_areal_or_pop integer, _type_superior integer, _type_inferior integer, _null_inferior boolean DEFAULT true)
RETURNS TABLE (
superior_cat		integer,
inferior_cat		integer
)
AS
$$
BEGIN
	CASE WHEN _areal_or_pop = 100 THEN
	RETURN QUERY
		WITH w_sup AS (
			SELECT t2.id AS variable_superior, t3.variable
			FROM target_data.c_area_domain AS t1
			INNER JOIN target_data.c_area_domain_category AS t2
			ON t1.id = t2.area_domain AND t1.id = _type_superior
			LEFT JOIN target_data.t_adc_hierarchy AS t3
			ON t2.id = t3.variable_superior
		),
		w_inf AS (
			SELECT t3.variable_superior, t2.id AS variable
			FROM target_data.c_area_domain AS t1
			INNER JOIN target_data.c_area_domain_category AS t2
			ON t1.id = t2.area_domain AND t1.id = _type_inferior
			LEFT JOIN target_data.t_adc_hierarchy AS t3
			ON t2.id = t3.variable
			WHERE CASE WHEN _null_inferior = false THEN t3.variable_superior IS NULL ELSE true END
		)
		SELECT variable_superior, variable
		FROM w_sup
		UNION
		SELECT variable_superior, variable
		FROM w_inf
		ORDER BY variable_superior, variable;

	WHEN _areal_or_pop = 200 THEN
	RETURN QUERY
		WITH w_sup AS (
			SELECT t2.id AS variable_superior, t3.variable
			FROM target_data.c_sub_population AS t1
			INNER JOIN target_data.c_sub_population_category AS t2
			ON t1.id = t2.sub_population AND t1.id = _type_superior
			LEFT JOIN target_data.t_spc_hierarchy AS t3
			ON t2.id = t3.variable_superior
		),
		w_inf AS (
			SELECT t3.variable_superior, t2.id AS variable
			FROM target_data.c_sub_population AS t1
			INNER JOIN target_data.c_sub_population_category AS t2
			ON t1.id = t2.sub_population AND t1.id = _type_inferior
			LEFT JOIN target_data.t_spc_hierarchy AS t3
			ON t2.id = t3.variable
			WHERE CASE WHEN _null_inferior = false THEN t3.variable_superior IS NULL ELSE true END
		)
		SELECT variable_superior, variable
		FROM w_sup
		UNION
		SELECT variable_superior, variable
		FROM w_inf
		ORDER BY variable_superior, variable;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_hierarchy(integer, integer, integer, boolean) IS
'Function returns records from t_adc_hierarchy/t_spc_hierarchy table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_hierarchy(integer, integer, integer, boolean) TO public;

-- </function>

-- <function name="fn_save_hierarchy" schema="target_data" src="functions/fn_save_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_save_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_hierarchy(integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_hierarchy(_areal_or_pop integer, _superior_cat integer[], _inferior_cat integer[])
RETURNS TABLE (
superior_cat		integer,
inferior_cat		integer
)
AS
$$
DECLARE
_test			integer[];
_superior_cat_test	integer[];
_inferior_cat_test	integer[];
BEGIN
		-- reorder just for equality test
		SELECT array_agg(t1.sup ORDER BY t1.sup) AS test
		FROM unnest(_superior_cat) AS t1(sup)
		INTO _superior_cat_test;

		SELECT array_agg(t1.inf ORDER BY t1.inf) AS test
		FROM unnest(_inferior_cat) AS t1(inf)
		INTO _inferior_cat_test;

	CASE WHEN _areal_or_pop = 100 THEN

		SELECT array_agg(t1.sup ORDER BY t1.sup) AS test
		FROM unnest(_superior_cat) AS t1(sup)
		INNER JOIN target_data.c_area_domain_category AS t2
		ON t1.sup = t2.id
		INTO _test;

		IF _test != _superior_cat_test
		THEN RAISE EXCEPTION 'Given array of superior categories containes identificators which area not present in c_area_domain_category.';
		END IF;

		SELECT array_agg(t1.inf) AS test
		FROM unnest(_inferior_cat) AS t1(inf)
		INNER JOIN target_data.c_area_domain_category AS t2
		ON t1.inf = t2.id
		INTO _test;

		IF _test != _inferior_cat_test
		THEN RAISE EXCEPTION 'Given array of inferior categories containes identificators which area not present in c_area_domain_category.';
		END IF;

		RETURN QUERY
		INSERT INTO target_data.t_adc_hierarchy (variable_superior, variable)
		SELECT t1.sup, t2.inf
		FROM
			unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
		INNER JOIN
			unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
		ON t1.id = t2.id
		RETURNING variable_superior, variable;

	WHEN _areal_or_pop = 200 THEN

		SELECT array_agg(t1.sup) AS test
		FROM unnest(_superior_cat) AS t1(sup)
		INNER JOIN target_data.c_sub_population_category AS t2
		ON t1.sup = t2.id
		INTO _test;

		IF _test != _superior_cat_test
		THEN RAISE EXCEPTION 'Given array of superior categories containes identificators which area not present in c_sub_population_category.';
		END IF;

		SELECT array_agg(t1.inf) AS test
		FROM unnest(_inferior_cat) AS t1(inf)
		INNER JOIN target_data.c_sub_population_category AS t2
		ON t1.inf = t2.id
		INTO _test;

		IF _test != _inferior_cat_test
		THEN RAISE EXCEPTION 'Given array of inferior categories containes identificators which area not present in c_sub_population_category.';
		END IF;

		RETURN QUERY
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable)
		SELECT t1.sup, t2.inf
		FROM
			unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
		INNER JOIN
			unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
		ON t1.id = t2.id
		RETURNING variable_superior, variable;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_hierarchy(integer, integer[], integer[]) IS
'Function provides insert into table t_adc_hierarchy/t_spc_hierarchy.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_hierarchy(integer, integer[], integer[]) TO public;

-- </function>

-- <function name="fn_delete_hierarchy" schema="target_data" src="functions/fn_delete_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_delete_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_delete_hierarchy(integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_hierarchy(_areal_or_pop integer, _superior_cat integer[], _inferior_cat integer[])
RETURNS TABLE (
superior_cat		integer,
inferior_cat		integer
)
AS
$$
DECLARE
_test			integer[];
_superior_cat_test	integer[];
_inferior_cat_test	integer[];
BEGIN
		-- reorder just for equality test
		SELECT array_agg(t1.sup ORDER BY t1.sup) AS test
		FROM unnest(_superior_cat) AS t1(sup)
		INTO _superior_cat_test;

		SELECT array_agg(t1.inf ORDER BY t1.inf) AS test
		FROM unnest(_inferior_cat) AS t1(inf)
		INTO _inferior_cat_test;

	CASE WHEN _areal_or_pop = 100 THEN

		SELECT array_agg(t1.sup ORDER BY t1.sup)
		FROM
			unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
		INNER JOIN
			unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
		ON t1.id = t2.id
		INNER JOIN target_data.t_adc_hierarchy AS t3
		ON t1.sup = t3.variable_superior AND t2.inf = t3.variable
		INTO _test;

		IF _test != _superior_cat_test
		THEN RAISE EXCEPTION 'Given arrays of superior and inferior categories containe combinations which area not present in table t_adc_hierarchy.';
		END IF; 

		RETURN QUERY
		DELETE FROM target_data.t_adc_hierarchy AS a
		USING 
			(SELECT t1.sup, t2.inf
			FROM	unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
			INNER JOIN unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
			ON t1.id = t2.id) AS b
		WHERE a.variable_superior = b.sup AND a.variable = b.inf
		RETURNING a.variable_superior, a.variable;

	WHEN _areal_or_pop = 200 THEN

		SELECT array_agg(t1.sup ORDER BY t1.sup)
		FROM
			unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
		INNER JOIN
			unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
		ON t1.id = t2.id
		INNER JOIN target_data.t_spc_hierarchy AS t3
		ON t1.sup = t3.variable_superior AND t2.inf = t3.variable
		INTO _test;

		IF _test != _superior_cat_test
		THEN RAISE EXCEPTION 'Given arrays of superior and inferior categories containe combinations which area not present in table t_spc_hierarchy.';
		END IF; 

		RETURN QUERY
		DELETE FROM target_data.t_spc_hierarchy AS a
		USING 
			(SELECT t1.sup, t2.inf
			FROM	unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
			INNER JOIN unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
			ON t1.id = t2.id) AS b
		WHERE a.variable_superior = b.sup AND a.variable = b.inf
		RETURNING a.variable_superior, a.variable;

	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_hierarchy(integer, integer[], integer[]) IS
'Function deletes records from t_adc_hierarchy/t_spc_hierarchy table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_hierarchy(integer, integer[], integer[]) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object4adsp(integer, integer) CASCADE;
-- <function name="fn_get_ldsity_object4adsp" schema="target_data" src="functions/fn_get_ldsity_object4adsp.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object4adsp
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object4adsp(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object4adsp(_ldsity_object integer, _areal_or_population integer)
RETURNS TABLE (
id			integer,
ldsity_object		integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
areal_or_population	integer
)
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
	THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object_group (%)', _ldsity_object;
	END IF;

	RETURN QUERY
	WITH w_hier AS (
		SELECT _ldsity_object AS ldsity_object, target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	)
	SELECT t3.id, t1.ldsity_object, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, t3.areal_or_population
	FROM w_hier AS t1,
		unnest(t1.ldsity_objects) AS t2(ldsity_object)
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	WHERE t3.areal_or_population = _areal_or_population;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object4adsp(integer, integer) IS
'Function returns records from c_ldsity_object table, optionally for given ldsity object. Returned records belong to the hierarchy of given ldsity object. Result can be subsetted by areal or populational character.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object4adsp(integer, integer) TO public;

-- </function>


