--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <view name="fn_add_plot_target_attr" schema="target_data" src="views/v_variable_hierarchy.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--alter extension nfiesta_target_data drop view target_data.v_variable_hierarchy;
--DROP VIEW target_data.v_variable_hierarchy;
CREATE OR REPLACE VIEW target_data.v_variable_hierarchy AS
with w_categorization_setups as (
	select distinct
		t_categorization_setup.id as cs_id,
		c_target_variable.id as tv_id, spc_id, sp_id, adc_id, ad_id
		, c_target_variable.label as tv_label, spc_label, sp_label, adc_label, ad_label
	from target_data.t_categorization_setup
	inner join target_data.cm_ldsity2target2categorization_setup ON cm_ldsity2target2categorization_setup.categorization_setup = t_categorization_setup.id
	inner join target_data.cm_ldsity2target_variable ON cm_ldsity2target_variable.id = cm_ldsity2target2categorization_setup.ldsity2target_variable
	inner join target_data.c_target_variable ON c_target_variable.id = cm_ldsity2target_variable.target_variable
	, lateral (	select 
					array_agg(c_sub_population_category.label order by spc2clr.n) as spc_label,
					array_agg(c_sub_population.label order by spc2clr.n) as sp_label,
					array_agg(c_sub_population_category.id order by spc2clr.n) as spc_id,
					array_agg(c_sub_population.id order by spc2clr.n) as sp_id
				from unnest(cm_ldsity2target2categorization_setup.spc2classification_rule) with ordinality as spc2clr(spc2classification_rule, n)
				inner join target_data.cm_spc2classification_rule on (spc2clr.spc2classification_rule = cm_spc2classification_rule.id)
				inner join target_data.c_sub_population_category ON c_sub_population_category.id = cm_spc2classification_rule.sub_population_category
				inner join target_data.c_sub_population ON c_sub_population.id = c_sub_population_category.sub_population
			  ) as spc
	, lateral (	select 
					array_agg(c_area_domain_category.label order by adc2clr.n) as adc_label,
					array_agg(c_area_domain.label order by adc2clr.n) as ad_label,
					array_agg(c_area_domain_category.id order by adc2clr.n) as adc_id,
					array_agg(c_area_domain.id order by adc2clr.n) as ad_id
				from unnest(cm_ldsity2target2categorization_setup.adc2classification_rule) with ordinality as adc2clr(adc2classification_rule, n)
				inner join target_data.cm_adc2classification_rule on (adc2clr.adc2classification_rule = cm_adc2classification_rule.id)
				inner join target_data.c_area_domain_category ON c_area_domain_category.id = cm_adc2classification_rule.area_domain_category
				inner join target_data.c_area_domain ON c_area_domain.id = c_area_domain_category.area_domain
			  ) as adc
	where not (cm_ldsity2target_variable.ldsity_object_type = 100 and exists (
		select from target_data.cm_ldsity2target_variable as l2tv_temp 
		where l2tv_temp.target_variable = c_target_variable.id and l2tv_temp.ldsity_object_type=200
		))
	order by t_categorization_setup.id
)
, w_sup_inf_pairs as (
	select 
		cs_sup.cs_id as cs_id_sup,
		cs_inf.cs_id as cs_id_inf
		, cs_sup.tv_label as tv_label_sup, cs_sup.spc_label as spc_label_sup, cs_sup.adc_label as adc_label_sup
		, cs_inf.tv_label as tv_label_inf, cs_inf.spc_label as spc_label_inf, cs_inf.adc_label as adc_label_inf
	from w_categorization_setups 		as cs_sup
	inner join w_categorization_setups 	as cs_inf on (cs_sup.tv_id = cs_inf.tv_id) and
			(
				((cs_sup.adc_id = cs_inf.adc_id OR cs_sup.adc_id IS NULL AND cs_inf.adc_id IS NULL) AND
					((cs_sup.spc_id != cs_inf.spc_id AND cs_sup.spc_id <@ cs_inf.spc_id AND (array_length(cs_inf.spc_id,1) = array_length(cs_sup.spc_id,1) + 1)) OR
					 (cs_sup.spc_id IS NULL AND cs_inf.spc_id IS NOT NULL AND array_length(cs_inf.spc_id,1) = 1)))
				OR
				((cs_sup.spc_id = cs_inf.spc_id OR cs_sup.spc_id IS NULL AND cs_inf.spc_id IS NULL) AND
					((cs_sup.adc_id != cs_inf.adc_id AND cs_sup.adc_id <@ cs_inf.adc_id AND (array_length(cs_inf.adc_id,1) = array_length(cs_sup.adc_id,1) + 1)) OR
					 (cs_sup.adc_id IS NULL AND cs_inf.adc_id IS NOT NULL AND array_length(cs_inf.adc_id,1) = 1))))
	order by cs_sup.cs_id, cs_inf.cs_id
)
select 
	cs_id_sup as node,
	array_agg(cs_id_inf order by cs_id_inf) AS edges
	--, tv_label_sup
	, spc_label_sup, adc_label_sup
	--, array_agg(tv_label_inf order by cs_id_inf) AS tv_label_inf
	, array_agg(array_to_string(spc_label_inf, '~') order by cs_id_inf) AS spc_label_inf
	, array_agg(array_to_string(adc_label_inf, '~') order by cs_id_inf) AS adc_label_inf
from w_sup_inf_pairs
group by cs_id_sup
, tv_label_sup, spc_label_sup, adc_label_sup
order by cs_id_sup
;

GRANT SELECT ON TABLE target_data.v_variable_hierarchy TO PUBLIC;
-- </view>

-- <function name="fn_add_plot_target_attr" schema="target_data" src="functions/additivity/fn_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: target_data.fn_add_plot_target_attr(integer)

-- DROP FUNCTION target_data.fn_add_plot_target_attr(integer);

CREATE OR REPLACE FUNCTION target_data.fn_add_plot_target_attr(
	IN variables integer[],
	IN min_diff double precision default 0.0, IN include_null_diff boolean default true
)
  RETURNS TABLE(
	plot				integer,
	reference_year_set	integer,
	variable			integer,
	ldsity				double precision,
	ldsity_sum			double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff				double precision
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text text;
BEGIN
	--------------------------------QUERY--------------------------------
	_array_text := concat(quote_literal(variables::text), '::integer[]');
	--raise notice 'variables: %',  _array_text;
	_complete_query := '
with w_plot_var as not materialized (
	select
		f_p_plot.gid as plot, t_available_datasets.categorization_setup as variable, t_available_datasets.reference_year_set,
		coalesce(t_ldsity_values.value, 0) as value
	from sdesign.f_p_plot
	inner join sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
	inner join sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
	inner join sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
	inner join target_data.t_available_datasets on (t_panel.id = t_available_datasets.panel)
	inner join target_data.t_categorization_setup on (t_available_datasets.categorization_setup = t_categorization_setup.id)
	left join target_data.t_ldsity_values on (
		f_p_plot.gid = t_ldsity_values.plot
		and t_available_datasets.id = t_ldsity_values.available_datasets
		and t_ldsity_values.is_latest)
	WHERE t_available_datasets.categorization_setup = ANY (' || _array_text || ')
)
, w_node_sum as (
	select
		plot,
		w_plot_var.reference_year_set,
		w_plot_var.variable,
		value as node_sum,
		v_variable_hierarchy.node,
		v_variable_hierarchy.edges as edges_def
	from w_plot_var
	inner join target_data.v_variable_hierarchy on (v_variable_hierarchy.node = w_plot_var.variable)
)
, w_edge_sum as (
	select
		w_node_sum.plot,
		w_node_sum.reference_year_set,
		w_node_sum.node,
		w_node_sum.node_sum,
		w_node_sum.edges_def,
		array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
		sum(w_plot_var.value) as edges_sum
	from w_node_sum
	left join w_plot_var on (
		w_plot_var.plot = w_node_sum.plot
		and w_plot_var.reference_year_set = w_node_sum.reference_year_set
		and w_plot_var.variable = any(w_node_sum.edges_def))
	group by w_node_sum.plot, w_node_sum.reference_year_set,
		w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
)
, w_diff as (
	select
		plot,
		reference_year_set,
		node		as variable,
		node_sum	as ldsity,
		edges_sum	as ldsity_sum,
		edges_def	as variables_def,
		edges_found	as variables_found,
		case
			when edges_sum != 0.0 and node_sum = 0.0 then 1.0
			when node_sum = 0.0 and edges_sum = 0.0 then 0
			else abs( (node_sum - edges_sum) / node_sum )
		end as diff
	from w_edge_sum
)
select * from w_diff
where
	(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
	or
	(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
order by diff desc
;
';

	--RAISE NOTICE '%', _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;

comment on function target_data.fn_add_plot_target_attr(integer[], double precision, boolean) is
'Function showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Reference year set. FKEY to t_reference_year_set.id.
 * Target total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION target_data.fn_add_plot_target_attr TO PUBLIC;
-- </function>