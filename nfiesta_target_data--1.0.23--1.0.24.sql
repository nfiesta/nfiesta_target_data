--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

----------------------------------------------
-- DDL
-----------------------------------------------

----------------------------------------------
-- Functions
-----------------------------------------------

DROP FUNCTION target_data.fn_save_categories(integer, integer, varchar[], text[], varchar[], text[]) CASCADE;

-- <function name="fn_save_categories" schema="target_data" src="functions/fn_save_categories.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categories
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_categories(integer, integer, varchar[], text[], varchar[], text[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_categories(_parent integer, _areal_or_population integer, _label varchar(200)[], _description text[], _label_en varchar(200)[], _description_en text[])
RETURNS TABLE (
id		integer,
label		varchar,
description	text,
label_en	varchar,
description_en	text
)
AS
$$
BEGIN
	IF _parent IS NULL OR _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent indicator (%) or label/descriptioin is null (%, %)!', _parent, _label, _description;
	END IF; 

	IF array_length(_label,1) != array_length(_description,1) OR
		array_length(_label_en,1) != array_length(_description_en,1)
	THEN
		RAISE EXCEPTION 'Given arrays of label and description (%,%) or label_en and description_en (%,%) must be of same length!', _label, _description, _label_en, _description_en;
	END IF;

	CASE 
	WHEN _areal_or_population = 100 THEN

		RETURN QUERY
		INSERT INTO target_data.c_area_domain_category(area_domain, label, description, label_en, description_en)
		SELECT 
			_parent,
			t1.label, t2.description, t3.label_en, t4.description_en
		FROM
			unnest(_label) WITH ORDINALITY AS t1(label, id)
		INNER JOIN
			unnest(_description) WITH ORDINALITY AS t2(description, id)
		ON t1.id = t2.id
		LEFT JOIN unnest(_label_en) WITH ORDINALITY AS t3(label_en, id)
		ON t1.id = t3.id
		LEFT JOIN unnest(_description_en) WITH ORDINALITY AS t4(description_en, id)
		ON t1.id  = t4.id
		RETURNING id, label, description, label_en, description_en;

	WHEN _areal_or_population = 200 THEN

		RETURN QUERY
		INSERT INTO target_data.c_sub_population_category(sub_population, label, description, label_en, description_en)
		SELECT 
			_parent,
			t1.label, t2.description, t3.label_en, t4.description_en
		FROM
			unnest(_label) WITH ORDINALITY AS t1(label, id)
		INNER JOIN
			unnest(_description) WITH ORDINALITY AS t2(description, id)
		ON t1.id = t2.id
		LEFT JOIN unnest(_label_en) WITH ORDINALITY AS t3(label_en, id)
		ON t1.id = t3.id
		LEFT JOIN unnest(_description_en) WITH ORDINALITY AS t4(description_en, id)
		ON t1.id  = t4.id
		RETURNING id, label, description, label_en, description_en;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;

	RETURN;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_categories(integer, integer, varchar(200)[], text[], varchar(200)[], text[]) IS
'Functions inserts a record into table c_area_domain_category or c_sub_population_category based on given parameters.';


-- </function>

-- <function name="fn_save_category" schema="target_data" src="functions/fn_save_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_category
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_category(integer, varchar, text, varchar, text, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_category(_areal_or_population integer, _label varchar(200), _description text, _label_en varchar(200), _description_en text, _parent integer DEFAULT NULL::integer, _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
BEGIN
	IF _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent identifier (%) or label/descriptioin is null (%, %)!', _parent, _label, _description;
	END IF; 

	IF _id IS NULL THEN

		IF _parent IS NULL
		THEN
			RAISE EXCEPTION 'Parameter of parent idenfier (id from c_area_domain/c_sub_population) is null!';
		END IF;

		CASE 
		WHEN _areal_or_population = 100 THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _parent)
			THEN RAISE EXCEPTION 'Given area_domain does not exist in table c_area_domain (%).', _parent;
			END IF;

			INSERT INTO target_data.c_area_domain_category(area_domain, label, description, label_en, description_en)
			SELECT 
				_parent,
				_label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _parent)
			THEN RAISE EXCEPTION 'Given sub_population does not exist in table c_sub_population (%).', _parent;
			END IF;

			INSERT INTO target_data.c_sub_population_category(sub_population, label, description, label_en, description_en)
			SELECT 
				_parent,
				_label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	ELSE
		CASE WHEN _areal_or_population = 100 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain_category AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain category does not exist in table c_area_domain_category (%)', _id;
			END IF;

			UPDATE target_data.c_area_domain_category
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_area_domain_category.id = _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain_category AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain category does not exist in table c_area_domain_category (%)', _id;
			END IF;

			UPDATE target_data.c_sub_population_category
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_sub_population_category.id = _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_category(integer, varchar(200), text, varchar(200), text, integer, integer) IS
'Functions inserts a record into table c_area_domain_category or c_sub_population_category based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_category(integer, varchar(200), text, varchar(200), text, integer, integer) TO public;

-- </function>

-- <function name="fn_save_categories_and_rules" schema="target_data" src="functions/fn_save_categories_and_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categories_and_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_categories_and_rules(_ldsity_object integer,
	_areal_or_pop integer, _label varchar(200), _description text, 
	_label_en varchar(200), _description_en text,
	_cat_label varchar(200)[], _cat_description text[],
	_cat_label_en varchar(200)[], _cat_description_en text[],
	_rules text[], _panel_refyearset integer[]
)
RETURNS TABLE (
	areal_or_pop			integer,
	parent_id			integer,
	category_id			integer,	
	classification_rule		integer,
	refyearset2panel		integer
) 
AS
$$
DECLARE
BEGIN
	IF array_length(_cat_label,1) != array_length(_cat_description,1) OR
		array_length(_cat_label_en,1) != array_length(_cat_description_en,1)
	THEN
		RAISE EXCEPTION 'Given arrays of label and description (%,%) or label_en and description_en (%,%) must be of same length!', _label, _description, _label_en, _description_en;
	END IF;

	IF array_length(_rules,1) != array_length(_cat_label,1)
	THEN
		RAISE EXCEPTION 'Given arrays of rules (%) must be the same length as array of labels (%)!', _rules, _label;
	END IF;

	RETURN QUERY
	WITH w_parent AS (
		SELECT t1 AS id FROM target_data.fn_save_areal_or_pop(_areal_or_pop, _label, _description, _label_en, _description_en) AS t1
	), w_categories AS (
		SELECT 	t1.id AS parent_id, 
			t2.id AS array_id,
			t6 AS category_id, t2.label
		FROM 	w_parent AS t1,
			unnest(_cat_label) WITH ORDINALITY AS t2(label, id)
		LEFT JOIN unnest(_cat_description) WITH ORDINALITY AS t3(description, id) ON t2.id = t3.id
		LEFT JOIN unnest(_cat_label_en) WITH ORDINALITY AS t4(label_en, id) ON t3.id = t4.id
		LEFT JOIN unnest(_cat_description_en) WITH ORDINALITY AS t5(description_en, id) ON t4.id = t5.id,
			target_data.fn_save_category(_areal_or_pop, t2.label, t3.description, t4.label_en, t5.description_en, t1.id) AS t6
	), w_rules AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, t2.rule, t3 AS rule_id
		FROM 	w_categories AS t1
		INNER JOIN unnest(_rules) WITH ORDINALITY AS t2(rule,id) ON t1.array_id = t2.id,
			target_data.fn_save_classification_rule(t1.category_id, _areal_or_pop, _ldsity_object, t2.rule) AS t3
	),
	w_rule_agg AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, array_agg(t1.rule_id ORDER BY array_id) AS rule_ids
		FROM w_rules AS t1
		GROUP BY t1.parent_id, t1.array_id, t1.category_id
	)
	SELECT  _areal_or_pop, t1.parent_id, t1.category_id, t2.classification_rule, t2.refyearset2panel
	FROM 
		w_rule_agg AS t1,
		target_data.fn_save_class_rule_mapping(_areal_or_pop, t1.rule_ids, _panel_refyearset) AS t2
	;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], integer[]) IS
'Function inserts all necessary data into lookups/tables with area_domain/sub_population categories and its classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], integer[]) TO public;

-- </function>

-- <function name="fn_save_class_rule_mapping" schema="target_data" src="functions/fn_save_class_rule_mapping.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_class_rule_mapping
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_class_rule_mapping(integer, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_class_rule_mapping(_areal_or_pop integer, _rules integer[], _panel_refyearset integer[])
--boolean
RETURNS TABLE (
	classification_rule		integer,
	refyearset2panel		integer
) 
AS
$$
DECLARE
BEGIN
	CASE
	WHEN _areal_or_pop = 100
	THEN
		RETURN QUERY
		INSERT INTO target_data.cm_adc2classrule2panel_refyearset (adc2classification_rule, refyearset2panel)
		SELECT
			t1.rule, t2.panel_ref
		FROM
			unnest(_rules) WITH ORDINALITY AS t1(rule,id),
			unnest(_panel_refyearset) WITH ORDINALITY AS t2(panel_ref, id)
		RETURNING cm_adc2classrule2panel_refyearset.adc2classification_rule, cm_adc2classrule2panel_refyearset.refyearset2panel;

	WHEN _areal_or_pop = 200
	THEN
		RETURN QUERY
		INSERT INTO target_data.cm_spc2classrule2panel_refyearset (spc2classification_rule, refyearset2panel)
		SELECT
			t1.rule, t2.panel_ref
		FROM
			unnest(_rules) WITH ORDINALITY AS t1(rule,id),
			unnest(_panel_refyearset) WITH ORDINALITY AS t2(panel_ref, id)
		RETURNING cm_spc2classrule2panel_refyearset.spc2classification_rule, cm_spc2classrule2panel_refyearset.refyearset2panel;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_class_rule_mapping(integer, integer[], integer[]) IS
'Function checks syntax in text field with classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_class_rule_mapping(integer, integer[], integer[]) TO public;

-- </function>

DROP FUNCTION target_data.fn_save_classification_rule(integer, integer, integer, text) CASCADE;

-- <function name="fn_save_classification_rule" schema="target_data" src="functions/fn_save_classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_classification_rule.sql
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_classification_rule(integer, integer, integer, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_classification_rule(_parent integer, _areal_or_population integer, _ldsity_object integer, _classification_rule text)
RETURNS integer
AS
$$
DECLARE
	_id integer;
BEGIN
	IF _parent IS NULL OR _ldsity_object IS NULL OR _classification_rule IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent indicator (%) or ldsity object (%) or classification rule (%) is null!', _parent, _ldsity_object, _classification_rule;
	END IF; 

	CASE 
	WHEN _areal_or_population = 100 THEN
		INSERT INTO target_data.cm_adc2classification_rule(area_domain_category, ldsity_object, classification_rule)
		SELECT 
			_parent,
			_ldsity_object, _classification_rule
		RETURNING id
		INTO _id;

	WHEN _areal_or_population = 200 THEN

		INSERT INTO target_data.cm_spc2classification_rule(sub_population_category, ldsity_object, classification_rule)
		SELECT 
			_parent,
			_ldsity_object, _classification_rule
		RETURNING id
		INTO _id;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_classification_rule(integer, integer, integer, text) IS
'Functions inserts a record into table cm_adc2conversion_string or cm_spc2conversion_string based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_classification_rule(integer, integer, integer, text) TO public;

-- </function>

-- <function name="fn_get_ldsity_object4ld_object_adsp" schema="target_data" src="functions/fn_get_ldsity_object4ld_object_adsp.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object4ld_object_adsp
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(_ldsity_object integer, _areal_or_population integer, _id integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
classification_rule	boolean
)
AS
$$
BEGIN
	IF _ldsity_object IS NULL
	THEN
		RAISE EXCEPTION 'Parameter _ldsity_object is mandatory and must not be null!';
	END IF;

	IF NOT EXISTS (SELECT * FROM target_data.c_areal_or_population AS t1 WHERE t1.id = _areal_or_population)
	THEN
		RAISE EXCEPTION 'Areal or population value not found in c_areal_or_population!';
	END IF;

	RETURN QUERY
	WITH w AS (
		SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	)
	SELECT 	t3.id, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, 
		CASE WHEN t4.ldsity_object IS NOT NULL OR t5.ldsity_object IS NOT NULL THEN true END AS rule
	FROM w AS t1,
		unnest(t1.ldsity_objects) AS t2(ldsity_object)
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	LEFT JOIN 
		(SELECT DISTINCT t2.ldsity_object
		FROM target_data.c_area_domain_category AS t1
		INNER JOIN target_data.cm_adc2classification_rule AS t2
		ON t1.id = t2.area_domain_category
		WHERE t1.area_domain = _id) AS t4
	ON t3.id = t4.ldsity_object
	LEFT JOIN --target_data.cm_spc2classification_rule AS t5
		(SELECT DISTINCT t2.ldsity_object
		FROM target_data.c_sub_population_category AS t1
		INNER JOIN target_data.cm_spc2classification_rule AS t2
		ON t1.id = t2.sub_population_category
		WHERE t1.sub_population = _id) AS t5
	ON t3.id = t5.ldsity_object
	WHERE areal_or_population = _areal_or_population;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) IS
'Function returns records from c_ldsity_object table, optionally for given topic.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) TO public;

-- </function>

-- <function name="fn_check_classification_rule_syntax" schema="target_data" src="functions/fn_check_classification_rule_syntax.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rule_syntax
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_check_classification_rule_syntax(integer, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rule_syntax(_ldsity_object integer, _rule text)
RETURNS boolean 
AS
$$
DECLARE
_table_name	varchar;
_test		boolean;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
	THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
	END IF;

	SELECT table_name
	FROM target_data.c_ldsity_object
	WHERE id = _ldsity_object
	INTO _table_name;


	IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
	THEN _test = true;
	ELSE
		BEGIN
		EXECUTE
		'SELECT EXISTS(SELECT *
		FROM '||_table_name||'
		WHERE '||_rule ||'
		)'
		INTO _test;

		IF _test IS NOT NULL 
		THEN _test := true;
		ELSE _test := false; 
		END IF;

		EXCEPTION WHEN OTHERS THEN
			_test := false;
		END;
	END IF;

	RETURN _test;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rule_syntax(integer, text) IS
'Function checks syntax in text field with classification rule.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rule_syntax(integer, text) TO public;

-- </function>

-- <function name="fn_check_classification_rule" schema="target_data" src="functions/fn_check_classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rule(_ldsity integer, _ldsity_object integer, _rule text, _panel_refyearset integer DEFAULT NULL::integer, _adc integer[] DEFAULT NULL::integer[], _spc integer[] DEFAULT NULL::integer[])
--boolean
RETURNS TABLE (
no_of_rules_met	integer,
no_of_objects	integer
) 
AS
$$
DECLARE
_ldsity_objects		integer[];
_adc_rule_test		integer[];
_spc_rule_test		integer[];
_table_name4rule	varchar;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
_exist_test		boolean;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
	INTO _test;

	IF _test = false
	THEN
		RAISE EXCEPTION 'Given rule has an invalid syntax.';
	END IF;

	IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
	THEN
		_exist_test := true; _rule = true;
	ELSE 	_exist_test := false;
	END IF;
	
	_case := 'CASE WHEN '||_rule||' THEN true ELSE false END';

	_ldsity_objects := (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]));

	WITH w AS (
		SELECT _ldsity_objects AS ldsity_objects
	)
	SELECT array_agg(t1.rule ORDER BY t1.id) 
	FROM unnest(_adc) WITH ORDINALITY AS t1(rule, id)
	INNER JOIN target_data.cm_adc2classification_rule AS t2
	ON t1.rule = t2.id
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	INNER JOIN w AS t4
	ON ARRAY[t3.id] <@ t4.ldsity_objects
	INTO _adc_rule_test;

	IF _adc_rule_test != _adc
	THEN
		RAISE EXCEPTION 'Given area domain category classification rules does not meet the ldsity objects within hierarchy of local density contribution. Rules which can be used = %, rules given = %.', _adc_rule_test, _adc;
	END IF;

	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	)
	SELECT array_agg(t1.rule ORDER BY t1.id) 
	FROM unnest(_spc) WITH ORDINALITY AS t1(rule, id)
	INNER JOIN target_data.cm_spc2classification_rule AS t2
	ON t1.rule = t2.id
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	INNER JOIN w AS t4
	ON ARRAY[t3.id] <@ t4.ldsity_objects
	INTO _spc_rule_test;

	IF _spc_rule_test != _spc
	THEN
		RAISE EXCEPTION 'Given sub population category classification rules does not meet the ldsity objects within hierarchy of local density contribution. Rules which can be used = %, rules given = %.', _spc_rule_test, _spc;
	END IF;

-- panels and reference year sets must be specified before the check

-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			information_schema.constraint_column_usage AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	_table_name4rule := (SELECT table_name FROM target_data.c_ldsity_object WHERE id = _ldsity_object);

	WITH w_tables AS (
			SELECT
				t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
					CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
					CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',1, ' AS rule_number, ', _case,' AS rul ') ELSE '' END,
					' FROM ', t1.table_name, 
					CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
					') AS ', _table_names_ws[t1.id]) AS table_select
			FROM
				unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
			LEFT JOIN
				(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
				FROM
					(SELECT t4.table_name, t3.classification_rule 
					FROM target_data.c_ldsity AS t1,
						unnest(t1.area_domain_category || _adc) WITH ORDINALITY AS t2(rule, id)
					LEFT JOIN target_data.cm_adc2classification_rule AS t3
					ON t2.rule = t3.id
					INNER JOIN target_data.c_ldsity_object AS t4
					ON t3.ldsity_object = t4.id
					WHERE t1.id = _ldsity
					UNION ALL
					SELECT t4.table_name, t3.classification_rule 
					FROM target_data.c_ldsity AS t1,
						unnest(t1.sub_population_category || _spc) WITH ORDINALITY AS t2(rule, id)
					LEFT JOIN target_data.cm_spc2classification_rule AS t3
					ON t2.rule = t3.id
					INNER JOIN target_data.c_ldsity_object AS t4
					ON t3.ldsity_object = t4.id
					WHERE t1.id = _ldsity
					) AS t1
				GROUP BY table_name
				) AS t2
			ON t1.table_name = t2.table_name
		)
		SELECT array_agg(table_select ORDER BY id)
		FROM w_tables
		INTO _table_selects;

		SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
			_table_names_ws[1], '.', _primary_keys[1], '
			FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
			INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
			INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
			INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
			_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
		INTO _table1;

		_join_all := NULL; _join := NULL;

		FOR i IN 2..array_length(_table_selects,1) 
		LOOP
			SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
			INTO _join;

			_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
			--raise notice '%', _join_all;
		END LOOP;
		--raise notice '%', _join_all;

		--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
		_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

		_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || _join_all;

	RETURN QUERY EXECUTE
	'WITH w AS (' || _q || '),
	w2 AS (SELECT
			id, rul, sum(CASE WHEN rul=true THEN 1 ELSE 0 END) AS no_of_rules_met
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
		GROUP BY id, rul
	)
	SELECT no_of_rules_met::int, count(*)::int AS no_of_objects
	FROM w2
	GROUP BY no_of_rules_met';

--return query select true, _no_of_rules_met, _no_of_objects;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) IS
'Function returns number of objects classified by the rule for given ldsity and attribute_type hierarchy.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) TO public;

-- </function>

-- <function name="fn_check_classification_rules" schema="target_data" src="functions/fn_check_classification_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rules(_ldsity integer, _ldsity_object integer, _rules text[], _panel_refyearset integer DEFAULT NULL::integer, _adc integer[] DEFAULT NULL::integer[], _spc integer[] DEFAULT NULL::integer[])
--boolean
RETURNS TABLE (
result		boolean,
message_id	integer,
message		text
) 
AS
$$
DECLARE
_ldsity_objects		integer[];
_adc_rule_test		integer[];
_spc_rule_test		integer[];
_table_name4rule	varchar;
_rule			text;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_cases			varchar[];
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_q_all			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
_exist_test		boolean;
_exist_test_all		boolean[];
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	IF NOT array[_ldsity_object] <@ (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]))
	THEN
		RAISE EXCEPTION 'Given ldsity object (%) is not found within the hierarchy of ldsity contribution (%).', _ldsity_object, _ldsity;
	END IF;

	FOREACH _rule IN ARRAY _rules
	LOOP
		SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
		INTO _test;

		IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
		THEN
			_exist_test := true; _rule = true;
		ELSE 	_exist_test := false;
		END IF;

		_exist_test_all := _exist_test_all || _exist_test;

		_test_all := _test_all || array[_test];

		_case := 'CASE WHEN '||_rule||' THEN true ELSE false END';

		_cases := _cases || _case;
		--raise notice '%', _test_all;
	END LOOP;

	IF _test_all @> array[false]
	THEN
		RAISE EXCEPTION 'One or more given rules has an invalid syntax.';
	END IF;

	IF _exist_test_all @> array[true] AND _exist_test_all @> array[false]
	THEN
		RAISE EXCEPTION 'In array of given rules there is an EXISTS or NOT EXISTS rule but there are also another rules, this rules have to be alone within one sub population.';
	END IF;

	IF _exist_test_all @> array[true] AND _exist_test_all != ARRAY[true,true]
	THEN
		RAISE EXCEPTION 'The given array of rules has more or less then 2 rules (%). If this is a special case of EXISTS rule, then its counterpart NOT EXISTS has to be entered and nothing more else.',_rules;
	END IF;

	_ldsity_objects := (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]));

	WITH w AS (
		SELECT _ldsity_objects AS ldsity_objects
	)
	SELECT array_agg(t1.rule ORDER BY t1.id) 
	FROM unnest(_adc) WITH ORDINALITY AS t1(rule, id)
	INNER JOIN target_data.cm_adc2classification_rule AS t2
	ON t1.rule = t2.id
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	INNER JOIN w AS t4
	ON ARRAY[t3.id] <@ t4.ldsity_objects
	INTO _adc_rule_test;

	IF _adc_rule_test != _adc
	THEN
		RAISE EXCEPTION 'Given area domain category classification rules does not meet the ldsity objects within hierarchy of local density contribution. Rules which can be used = %, rules given = %.', _adc_rule_test, _adc;
	END IF;

	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	)
	SELECT array_agg(t1.rule ORDER BY t1.id) 
	FROM unnest(_spc) WITH ORDINALITY AS t1(rule, id)
	INNER JOIN target_data.cm_spc2classification_rule AS t2
	ON t1.rule = t2.id
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	INNER JOIN w AS t4
	ON ARRAY[t3.id] <@ t4.ldsity_objects
	INTO _spc_rule_test;

	IF _spc_rule_test != _spc
	THEN
		RAISE EXCEPTION 'Given sub population category classification rules does not meet the ldsity objects within hierarchy of local density contribution. Rules which can be used = %, rules given = %.', _spc_rule_test, _spc;
	END IF;

-- panels and reference year sets must be specified before the check

-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			information_schema.constraint_column_usage AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	_table_name4rule := (SELECT table_name FROM target_data.c_ldsity_object WHERE id = _ldsity_object);

	FOR i IN 1..array_length(_cases,1)
	LOOP
		WITH w_tables AS (
			SELECT
				t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
					CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
					CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',i, ' AS rule_number, ', _cases[i],' AS rul ') ELSE '' END,
					' FROM ', t1.table_name, 
					CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
					') AS ', _table_names_ws[t1.id]) AS table_select
			FROM
				unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
			LEFT JOIN
				(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
				FROM
					(SELECT t4.table_name, t3.classification_rule 
					FROM target_data.c_ldsity AS t1,
						unnest(t1.area_domain_category || _adc) WITH ORDINALITY AS t2(rule, id)
					LEFT JOIN target_data.cm_adc2classification_rule AS t3
					ON t2.rule = t3.id
					INNER JOIN target_data.c_ldsity_object AS t4
					ON t3.ldsity_object = t4.id
					WHERE t1.id = _ldsity
					UNION ALL
					SELECT t4.table_name, t3.classification_rule 
					FROM target_data.c_ldsity AS t1,
						unnest(t1.sub_population_category || _spc) WITH ORDINALITY AS t2(rule, id)
					LEFT JOIN target_data.cm_spc2classification_rule AS t3
					ON t2.rule = t3.id
					INNER JOIN target_data.c_ldsity_object AS t4
					ON t3.ldsity_object = t4.id
					WHERE t1.id = _ldsity
					) AS t1
				GROUP BY table_name
				) AS t2
			ON t1.table_name = t2.table_name
		)
		SELECT array_agg(table_select ORDER BY id)
		FROM w_tables
		INTO _table_selects;

		SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
			_table_names_ws[1], '.', _primary_keys[1], '
			FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
			INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
			INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
			INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
			_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
		INTO _table1;

		_join_all := NULL; _join := NULL;

		FOR i IN 2..array_length(_table_selects,1) 
		LOOP
			SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
			INTO _join;

			_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
			--raise notice '%', _join_all;
		END LOOP;

		--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
		_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

		_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
--raise notice '%', _table1;
		_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL ' ELSE '' END), _q);

	END LOOP;

--	raise notice '%',_q_all;

	EXECUTE
	'WITH w AS (' || _q_all || '),
	w2 AS (SELECT
			id, rul, sum(CASE WHEN rul=true THEN 1 ELSE 0 END) AS no_of_rules_met
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
		GROUP BY id, rul
	), w_test AS (
		SELECT no_of_rules_met::int, count(*)::int AS no_of_objects
		FROM w2
		GROUP BY no_of_rules_met
	)
	SELECT
		array_agg(no_of_rules_met ORDER BY no_of_rules_met) AS no_of_rules_met,
		array_agg(no_of_objects ORDER BY no_of_rules_met) AS no_of_objects
	FROM
		w_test
	'
	INTO _no_of_rules_met, _no_of_objects;

--raise notice '%, %', _no_of_rules_met, _no_of_objects;

	_total := (SELECT sum(nob) FROM unnest(_no_of_objects) AS t(nob));

--return query select true, _no_of_rules_met, _no_of_objects;

	-- if both are NULL 
IF _no_of_rules_met IS NULL AND _no_of_objects IS NULL
THEN	
	RETURN QUERY 
		SELECT false, 0, concat('There were no records on which the rules could be tested - given combination of panels and reference year sets probably does not correspond with the local density contribution availability.')::text;
ELSE
-- if one rule given - all records have 1 rule met
	IF array_length(_rules,1) = 1
	THEN
		IF array_length(_no_of_rules_met,1) != 1
		THEN
			RETURN QUERY 
			SELECT false, 1, concat('Only one rule was given, so all records has to result into true. There were only ', _no_of_objects[2], ' number of objects successfully categorized from ', _total, ' overall total.')::text;
		ELSE
			IF _no_of_rules_met[1] = 0
			THEN
				RETURN QUERY
				SELECT false, 2, concat('Only one rule was given, and none of the ', _no_of_objects[1], ' number of objects was successfully categorized.')::text;
			ELSE
			--raise notice '%, %', _no_of_rules_met, _no_of_objects;
				RETURN QUERY 
				SELECT true, 3, concat('Only one rule was given and every object was successfully categorized by it. All from ', _no_of_objects[1], ' number of objects resulted in ', _total, ' successful records.')::text;
			END IF;
		END IF;
	END IF;


-- if 2 or more rules given - all records have 1 rule met and 0 rule met
	-- both numbers must be the same (logically if 1 object belogns to some rule, basically also fails to the other rules)

	IF array_length(_rules,1) > 1
	THEN
		-- there are objects with 2 or more rules met (intersection between rules, can be from 0,1,2 or 1,2)
		IF EXISTS(SELECT * FROM unnest(_no_of_rules_met) AS t(nor) WHERE nor >= 2)
		THEN
			IF _exist_test_all = array[true, true]
			THEN
				RETURN QUERY
				SELECT true, 8, concat('Special case of EXISTS rules, implicitly true for all records.')::text;
			ELSE
				RETURN QUERY 
				SELECT false, 4, concat('Some of the objects were categorized by more than one rule, there is an intersection between the rule conditions.')::text;
			END IF;
		ELSE
			-- there are objects which are not covered by rules (only 0,1)
			IF array_length(_no_of_rules_met,1) = 2
			THEN
				IF _no_of_objects[1] != _no_of_objects[2]
				THEN
					RETURN QUERY 
					SELECT false, 5, concat('Some of the objects (', _no_of_objects[1] - _no_of_objects[2], ' from total of ', _no_of_objects[1], ') were not successfully categorized by any of the rules. Consider editing hierarchy.')::text;
				ELSE
					-- the rest, where 0,1, both the same number of objects
					IF _no_of_objects[1] = _no_of_objects[2]
					THEN
						RETURN QUERY
						SELECT true, 6, concat('Every object was succesfully categorized without any intersection.')::text;
					ELSE
						--raise notice '%, %', _no_of_rules_met, _no_of_objects;
						RAISE EXCEPTION '01: Not known state of classification rules.';
					END IF;
				END IF;
			ELSE
				IF array_length(_no_of_rules_met,1) = 1 AND _no_of_rules_met[1] = 0
				THEN
					RETURN QUERY 
					SELECT false, 7, concat('None of the ', _no_of_objects[1], ' was successfully categorized by any of the rules.')::text;
				ELSE
					RAISE EXCEPTION '02: Not known state of classification rules.';
				END IF;
			END IF;
		END IF;
	END IF;
END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[], integer[]) IS
'Function checks syntax in text field with classification rules for given ldsity contribution and ldsity object within its hierarchy. Optionally also for given panel x reference year set combination and/or  area domain/sub population category/ies.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[],  integer[]) TO public;

-- </function>

-- <function name="fn_get_ldsity" schema="target_data" src="functions/fn_get_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity(_target_variable integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
target_variable		integer,
label			character varying(200),
label_en		character varying(200),
column_expression	text,
unit_of_measure		integer,
ldsity_object		integer,
ldsity_object_type	integer,
object_type_label	character varying(200),
object_type_desc	text
)
AS
$$
BEGIN
	IF _target_variable IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS target_variable,
			t1.label, t1.label_en, t1.column_expression, t1.ldsity_object, t1.unit_of_measure,
			NULL::int, NULL::varchar(200), NULL::text
		FROM target_data.c_ldsity AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_target_variable (%)', _target_variable;
		END IF;

		RETURN QUERY
		SELECT t3.id, t1.id AS target_variable,
			t3.label, t3.label_en, t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
			t2.ldsity_object_type, t4.label, t4.description
		FROM target_data.c_target_variable AS t1
		INNER JOIN target_data.cm_ldsity2target_variable AS t2
		ON t1.id = t2.target_variable
		INNER JOIN target_data.c_ldsity AS t3
		ON t2.ldsity = t3.id
		INNER JOIN target_data.c_ldsity_object_type AS t4
		ON t2.ldsity_object_type = t4.id
		WHERE t1.id = _target_variable;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity(integer) IS
'Function returns records from c_ldsity table, optionally for given target_variable.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity(integer) TO public;

-- </function>

----------------------------------------------
-- dynamic data with new functions
-----------------------------------------------



-- dynamic data
-- area_domain
WITH w_rules AS (
	SELECT
		t1.id AS parent_id,
		t2.id AS category_id,
		t3.id AS rule_id,
		t3.ldsity_object,
		t3.classification_rule
	FROM
		target_data.c_area_domain AS t1
	INNER JOIN target_data.c_area_domain_category AS t2
	ON t1.id = t2.area_domain
	INNER JOIN target_data.cm_adc2classification_rule AS t3
	ON t2.id = t3.area_domain_category
), w_aggrules AS (
	SELECT
		ldsity_object, parent_id, 
		array_agg(classification_rule ORDER BY category_id) AS rules, 
		array_agg(rule_id ORDER BY category_id) AS rules_id
	FROM	w_rules
	GROUP BY ldsity_object, parent_id

), w_ldsity AS (
	SELECT
		t1.ldsity_object, parent_id, t1.rules, t1.rules_id, t2.id AS ldsity
	FROM
		w_aggrules AS t1
	INNER JOIN
		target_data.c_ldsity AS t2
	ON t1.ldsity_object = t2.ldsity_object

), w_panelref AS (
	SELECT t1.id AS panelref, t2.parent_id, t3.result, t2.ldsity, t2.ldsity_object, t2.rules_id
	FROM 	sdesign.cm_refyearset2panel_mapping AS t1,
		w_ldsity AS t2,
		target_data.fn_check_classification_rules(t2.ldsity,t2.ldsity_object,t2.rules,t1.id) AS t3
), w_panelref_agg AS (
	SELECT parent_id, rules_id, array_agg(t1.panelref ORDER BY t1.panelref) AS panelref
	FROM 
		w_panelref AS t1
	WHERE result = true
	GROUP BY ldsity_object, parent_id, rules_id
)
SELECT * 
FROM 	w_panelref_agg AS t1, 
--	target_data.fn_save_categories_and_rules(ldsity, ldsity_object, rules, panel_refyearset, adc, spc)
	target_data.fn_save_class_rule_mapping(100, rules_id, panelref);

-- sub_population
WITH w_rules AS (
	SELECT
		t1.id AS parent_id,
		t2.id AS category_id,
		t3.id AS rule_id,
		t3.ldsity_object,
		t3.classification_rule
	FROM
		target_data.c_sub_population AS t1
	INNER JOIN target_data.c_sub_population_category AS t2
	ON t1.id = t2.sub_population
	INNER JOIN target_data.cm_spc2classification_rule AS t3
	ON t2.id = t3.sub_population_category
), w_aggrules AS (
	SELECT
		ldsity_object, parent_id, 
		array_agg(classification_rule ORDER BY category_id) AS rules, 
		array_agg(rule_id ORDER BY category_id) AS rules_id
	FROM	w_rules
	GROUP BY ldsity_object, parent_id

), w_ldsity AS (
	SELECT
		t1.ldsity_object, parent_id, t1.rules, t1.rules_id, t2.id AS ldsity
	FROM
		w_aggrules AS t1
	INNER JOIN
		target_data.c_ldsity AS t2
	ON t1.ldsity_object = t2.ldsity_object

), w_panelref AS (
	SELECT t1.id AS panelref, t2.parent_id, t3.result, t2.ldsity, t2.ldsity_object, t2.rules_id
	FROM 	sdesign.cm_refyearset2panel_mapping AS t1,
		w_ldsity AS t2,
		target_data.fn_check_classification_rules(t2.ldsity,t2.ldsity_object,t2.rules,t1.id) AS t3
), w_panelref_agg AS (
	SELECT parent_id, rules_id, array_agg(t1.panelref ORDER BY t1.panelref) AS panelref
	FROM 
		w_panelref AS t1
	WHERE result = true
	GROUP BY ldsity_object, parent_id, rules_id
)
SELECT * 
FROM 	w_panelref_agg AS t1, 
--	target_data.fn_save_categories_and_rules(ldsity, ldsity_object, rules, panel_refyearset, adc, spc)
	target_data.fn_save_class_rule_mapping(200, rules_id, panelref);


