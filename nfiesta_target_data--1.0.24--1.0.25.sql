--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

COMMENT ON COLUMN target_data.c_ldsity.area_domain_category
    IS 'Array of identifiers of area domain category, array of foreing keys to table cm_adc2classification_rule.';

COMMENT ON COLUMN target_data.c_ldsity.sub_population_category
    IS 'Array of identifiers of sub-population category, array of foreing keys to table cm_spc2classification_rule.';

COMMENT ON COLUMN target_data.c_ldsity.definition_variant
    IS 'Array of identifiers of definition variant, array of foreign keys to table c_definition_variant.';

COMMENT ON COLUMN target_data.cm_ldsity2target_variable.area_domain_category
    IS 'Array of identifiers of area domain category, array of foreing keys to table cm_adc2classification_rule.';

COMMENT ON COLUMN target_data.cm_ldsity2target_variable.sub_population_category
    IS 'Array of identifiers of sub-population category, array of foreing keys to table cm_spc2classification_rule.';

COMMENT ON COLUMN target_data.cm_ldsity2target_variable.definition_variant
    IS 'Array of identifiers of definition variant category, array of foreign keys to table c_definition_variant.';