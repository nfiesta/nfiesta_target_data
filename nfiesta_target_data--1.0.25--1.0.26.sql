--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_try_delete_area_domain" schema="target_data" src="functions/fn_try_delete_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_delete_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_try_delete_area_domain(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_delete_area_domain(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_area_domain)
	THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _id;
	END IF;

	RETURN NOT EXISTS (
	SELECT id FROM target_data.t_available_datasets
	WHERE categorization_setup IN
	(
		SELECT DISTINCT t.categorization_setup  FROM 
		(SELECT
				categorization_setup,
				unnest(adc2classification_rule) AS adc2classification_rule
		FROM
				target_data.cm_ldsity2target2categorization_setup
		WHERE
				adc2classification_rule IS NOT NULL
		) AS t
		WHERE t.adc2classification_rule IN 
		(
		SELECT id FROM target_data.cm_adc2classification_rule WHERE area_domain_category in
		(SELECT id FROM target_data.c_area_domain_category WHERE area_domain = _id) 
		)
	)
	);		
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_delete_area_domain(integer) IS
'Function provides test if it is possible to delete records from c_area_domain table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_delete_area_domain(integer) TO public;
-- </function>

-- <function name="fn_delete_area_domain" schema="target_data" src="functions/fn_delete_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_area_domain(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_area_domain(_id integer)
RETURNS void
AS
$$
BEGIN

	DELETE FROM target_data.cm_adc2classrule2panel_refyearset 
	WHERE adc2classification_rule IN 
		(SELECT id FROM target_data.cm_adc2classification_rule 
		WHERE area_domain_category IN
			(SELECT id FROM target_data.c_area_domain_category 
			WHERE area_domain = _id)); 		
				
	DELETE FROM target_data.cm_adc2classification_rule 
	WHERE area_domain_category IN 
		(SELECT id FROM target_data.c_area_domain_category 
		WHERE area_domain = _id);
	
	DELETE FROM target_data.c_area_domain_category 
	WHERE area_domain = _id;
	
	DELETE FROM target_data.c_area_domain 
	WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_area_domain(integer) IS
'Function deletes records from c_area_domain, c_area_domain_category, cm_adc2classification_rule and cm_adc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_area_domain(integer) TO public;
-- </function>

-- <function name="fn_try_delete_sub_population" schema="target_data" src="functions/fn_try_delete_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_delete_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_try_delete_sub_population(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_delete_sub_population(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_sub_population)
	THEN RAISE EXCEPTION 'Given subpopulation does not exist in table c_sub_population (%)', _id;
	END IF;
	
	RETURN NOT EXISTS (
	SELECT id FROM target_data.t_available_datasets
	WHERE categorization_setup IN
	(
		SELECT DISTINCT t.categorization_setup  FROM 
		(SELECT
				categorization_setup,
				unnest(spc2classification_rule) AS spc2classification_rule
		FROM
				target_data.cm_ldsity2target2categorization_setup
		WHERE
				spc2classification_rule IS NOT NULL
		) AS t
		WHERE t.spc2classification_rule IN 
		(
		SELECT id FROM target_data.cm_spc2classification_rule WHERE sub_population_category in
		(SELECT id FROM target_data.c_sub_population_category WHERE sub_population = _id) 
		)
	)
	);		
	
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_delete_sub_population(integer) IS
'Function provides test if it is possible to delete records from c_sub_population table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_delete_sub_population(integer) TO public;
-- </function>

-- <function name="fn_delete_sub_population" schema="target_data" src="functions/fn_delete_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_sub_population(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_sub_population(_id integer)
RETURNS void
AS
$$
BEGIN

	DELETE FROM target_data.cm_spc2classrule2panel_refyearset 
	WHERE spc2classification_rule IN 
		(SELECT id FROM target_data.cm_spc2classification_rule 
		WHERE sub_population_category IN
			(SELECT id FROM target_data.c_sub_population_category 
			WHERE sub_population = _id)); 
				
	DELETE FROM target_data.cm_spc2classification_rule 
	WHERE sub_population_category IN 
		(SELECT id FROM target_data.c_sub_population_category 
		WHERE sub_population = _id);
	
	DELETE FROM target_data.c_sub_population_category 
	WHERE sub_population = _id;
	
	DELETE FROM target_data.c_sub_population
	WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_sub_population(integer) IS
'Function deletes records from c_sub_population, c_sub_population_category, cm_spc2classification_rule and cm_spc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_sub_population(integer) TO public;
-- </function>