--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-------------------
-- DDL
-------------------


-------------------
-- Functions
-------------------

DROP FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], integer[]) CASCADE;
-- <function name="fn_save_categories_and_rules" schema="target_data" src="functions/fn_save_categories_and_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categories_and_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_categories_and_rules(_ldsity_object integer,
	_areal_or_pop integer, _label varchar(200), _description text, 
	_label_en varchar(200), _description_en text,
	_cat_label varchar(200)[], _cat_description text[],
	_cat_label_en varchar(200)[], _cat_description_en text[],
	_rules text[], _panel_refyearset integer[], 
	_adc integer[][] DEFAULT NULL::int[][], _spc integer[][] DEFAULT NULL::int[][]
)
RETURNS TABLE (
	areal_or_pop			integer,
	parent_id			integer,
	category_id			integer,	
	classification_rule		integer,
	refyearset2panel		integer
) 
AS
$$
DECLARE
BEGIN
	IF array_length(_cat_label,1) != array_length(_cat_description,1) OR
		array_length(_cat_label_en,1) != array_length(_cat_description_en,1)
	THEN
		RAISE EXCEPTION 'Given arrays of label and description (%,%) or label_en and description_en (%,%) must be of same length!', _label, _description, _label_en, _description_en;
	END IF;

	IF array_length(_rules,1) != array_length(_cat_label,1)
	THEN
		RAISE EXCEPTION 'Given arrays of rules (%) must be the same length as array of labels (%)!', _rules, _label;
	END IF;

	RETURN QUERY
	WITH w_parent AS (
		SELECT t1 AS id FROM target_data.fn_save_areal_or_pop(_areal_or_pop, _label, _description, _label_en, _description_en) AS t1
	), w_categories AS (
		SELECT 	t1.id AS parent_id, 
			t2.id AS array_id,
			t6 AS category_id, t2.label
		FROM 	w_parent AS t1,
			unnest(_cat_label) WITH ORDINALITY AS t2(label, id)
		LEFT JOIN unnest(_cat_description) WITH ORDINALITY AS t3(description, id) ON t2.id = t3.id
		LEFT JOIN unnest(_cat_label_en) WITH ORDINALITY AS t4(label_en, id) ON t3.id = t4.id
		LEFT JOIN unnest(_cat_description_en) WITH ORDINALITY AS t5(description_en, id) ON t4.id = t5.id,
			target_data.fn_save_category(_areal_or_pop, t2.label, t3.description, t4.label_en, t5.description_en, t1.id) AS t6
	), w_rules AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, t2.rule, t3 AS rule_id
		FROM 	w_categories AS t1
		INNER JOIN unnest(_rules) WITH ORDINALITY AS t2(rule,id) ON t1.array_id = t2.id,
			target_data.fn_save_classification_rule(t1.category_id, _areal_or_pop, _ldsity_object, t2.rule) AS t3
	),
	w_rule_agg AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, array_agg(t1.rule_id ORDER BY array_id) AS rule_ids
		FROM w_rules AS t1
		GROUP BY t1.parent_id, t1.array_id, t1.category_id
	), w_hier_adc AS (
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable)
		SELECT t2.adc, t1.category_id
		FROM	w_categories AS t1,
			(SELECT 
				adc, id, ceil(id/(count(*) OVER())::double precision * array_length(_adc,1)) AS array_id
			FROM unnest(_adc) WITH ORDINALITY AS t1(adc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id
	), w_hier_spc AS (
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable)
		SELECT t2.spc, t1.category_id
		FROM	w_categories AS t1,
			(SELECT 
				spc, id, ceil(id/(count(*) OVER())::double precision * array_length(_spc,1)) AS array_id
			FROM unnest(_spc) WITH ORDINALITY AS t1(spc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id
	)
	SELECT  _areal_or_pop, t1.parent_id, t1.category_id, t2.classification_rule, t2.refyearset2panel
	FROM 
		w_rule_agg AS t1,
		target_data.fn_save_class_rule_mapping(_areal_or_pop, t1.rule_ids, _panel_refyearset) AS t2
	;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], integer[], integer[][], integer[][]) IS
'Function inserts all necessary data into lookups/tables with area_domain/sub_population categories and its classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], integer[], integer[][], integer[][]) TO public;

-- </function>

-- <function name="fn_save_rules" schema="target_data" src="functions/fn_save_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_rules(integer, integer, integer[], text[], integer[], integer[][], integer[][] CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_rules(_ldsity_object integer, _areal_or_pop integer, _category_ids integer[], _rules text[], _panel_refyearset integer[], 
		_adc integer[][] DEFAULT NULL::integer[][], _spc integer[][] DEFAULT NULL::integer[][])
RETURNS TABLE (
	areal_or_pop			integer,
	parent_id			integer,
	category_id			integer,	
	classification_rule		integer,
	refyearset2panel		integer
) 
AS
$$
DECLARE
BEGIN
	IF array_length(_rules,1) != array_length(_category_ids,1)
	THEN
		RAISE EXCEPTION 'Given arrays of rules (%) must be the same length as array of category ids (%)!', _rules, _category_ids;
	END IF;

	RETURN QUERY
	WITH w_categories AS (
		SELECT 
			t.id AS array_id,
			t.category
		FROM
			unnest(_category_ids) WITH ORDINALITY AS t(category, id)
	), w_rules AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, t2.rule, t3 AS rule_id
		FROM 	w_categories AS t1
		INNER JOIN unnest(_rules) WITH ORDINALITY AS t2(rule,id) ON t1.array_id = t2.id,
			target_data.fn_save_classification_rule(t1.category_id, _areal_or_pop, _ldsity_object, t2.rule) AS t3
	),
	w_rule_agg AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, array_agg(t1.rule_id ORDER BY array_id) AS rule_ids
		FROM w_rules AS t1
		GROUP BY t1.parent_id, t1.array_id, t1.category_id
	), w_hier_adc AS (
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable)
		SELECT t2.adc, t1.category_id
		FROM	w_categories AS t1,
			(SELECT 
				adc, id, ceil(id/(count(*) OVER())::double precision * array_length(_adc,1)) AS array_id
			FROM unnest(_adc) WITH ORDINALITY AS t1(adc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id
	), w_hier_spc AS (
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable)
		SELECT t2.spc, t1.category_id
		FROM	w_categories AS t1,
			(SELECT 
				spc, id, ceil(id/(count(*) OVER())::double precision * array_length(_spc,1)) AS array_id
			FROM unnest(_spc) WITH ORDINALITY AS t1(spc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id
	)
	SELECT  _areal_or_pop, t1.parent_id, t1.category_id, t2.classification_rule, t2.refyearset2panel
	FROM 
		w_rule_agg AS t1,
		target_data.fn_save_class_rule_mapping(_areal_or_pop, t1.rule_ids, _panel_refyearset) AS t2
	;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_rules(integer, integer, integer[], text[], integer[], integer[][], integer[][]) IS
'Function inserts all necessary data into lookups/tables with area_domain/sub_population categories and its classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_rules(integer, integer, integer[], text[], integer[], integer[][], integer[][]) TO public;

-- </function>

