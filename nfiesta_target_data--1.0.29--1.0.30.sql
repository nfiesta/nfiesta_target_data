--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-------------------
-- DDL
-------------------


-------------------
-- Functions
-------------------
DROP FUNCTION IF EXISTS target_data.fn_get_ldsity(integer) CASCADE;

-- <function name="fn_get_ldsity" schema="target_data" src="functions/fn_get_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity(integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity(_target_variable integer[] DEFAULT NULL::integer[], _ldsity_object_type integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
target_variable		integer,
label			character varying(200),
label_en		character varying(200),
column_expression	text,
unit_of_measure		integer,
ldsity_object		integer,
ldsity_object_type	integer,
object_type_label	character varying(200),
object_type_desc	text
)
AS
$$
DECLARE
_test		integer;
BEGIN
	IF _target_variable IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS target_variable,
			t1.label, t1.label_en, t1.column_expression, t1.ldsity_object, t1.unit_of_measure,
			NULL::int, NULL::varchar(200), NULL::text
		FROM target_data.c_ldsity AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable[1])
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_target_variable (%)', _target_variable;
		END IF;

		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_type AS t1 WHERE t1.id = _ldsity_object_type)
		THEN RAISE EXCEPTION 'Given ldsity_object_type does not exist in table c_ldsity_object_type (%)', _ldsity_object_type;
		END IF;


		CASE WHEN _ldsity_object_type = 100
		THEN
			-- test if all given target_variables has the same ldsitys 100
			--RETURN QUERY
			WITH w AS (
				SELECT 	DISTINCT t1.id AS target_variable, t3.id AS ldsity, 
					t2.area_domain_category, t2.sub_population_category
				FROM target_data.c_target_variable AS t1
				INNER JOIN target_data.cm_ldsity2target_variable AS t2
				ON t1.id = t2.target_variable
				INNER JOIN target_data.c_ldsity AS t3
				ON t2.ldsity = t3.id
				INNER JOIN target_data.c_ldsity_object_type AS t4
				ON t2.ldsity_object_type = t4.id
				WHERE array[t1.id] <@ _target_variable AND t4.id = 100
			), w_agg_target_variable AS (
				SELECT w.target_variable, w.area_domain_category, w.sub_population_category,
					array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) AS ldsity
				FROM w
				GROUP BY w.target_variable, w.area_domain_category, w.sub_population_category
			)
			SELECT count(DISTINCT ldsity)
			FROM w_agg_target_variable 
			INTO _test;

			IF _test > 1
			THEN
				RAISE EXCEPTION 'Given target variables (in array) does not have the same ldsitys of ldsity object type 100. There is either different ldsity, or the same ldsity but somehow constrained by area domain category or sub population category.';
			END IF;	

			-- return distinct objects for given target variables
			RETURN QUERY
			SELECT  DISTINCT t3.id, NULL::int AS target_variable,
				t3.label, t3.label_en, t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
				t2.ldsity_object_type, t4.label, t4.description
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _target_variable AND t4.id = 100;

		WHEN _ldsity_object_type = 200
		THEN
			WITH w AS (
				SELECT 	DISTINCT t3.id AS ldsity, t1.id AS target_variable,
					t2.area_domain_category, t2.sub_population_category
				FROM target_data.c_target_variable AS t1
				INNER JOIN target_data.cm_ldsity2target_variable AS t2
				ON t1.id = t2.target_variable
				INNER JOIN target_data.c_ldsity AS t3
				ON t2.ldsity = t3.id
				INNER JOIN target_data.c_ldsity_object_type AS t4
				ON t2.ldsity_object_type = t4.id
				WHERE array[t1.id] <@ _target_variable AND t4.id = 100
			), w_agg_target_variable AS (
				SELECT w.target_variable, w.area_domain_category, w.sub_population_category,
					array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) AS ldsity
				FROM w
				GROUP BY w.target_variable, w.area_domain_category, w.sub_population_category
			)
			SELECT count(DISTINCT ldsity)
			FROM w_agg_target_variable 
			INTO _test;

			IF _test > 1
			THEN
				RAISE EXCEPTION 'Given target variables (in array) does not have the same ldsitys of ldsity object type 100. There is either different ldsity, or the same ldsity but somehow constrained by area domain category or sub population category.';
			END IF;	

			RETURN QUERY
			SELECT  t3.id AS ldsity, t1.id AS target_variable,
				t3.label, t3.label_en, t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
				t2.ldsity_object_type, t4.label, t4.description
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _target_variable AND t4.id = 200;
		ELSE
			RAISE EXCEPTION 'Not known value of ldsity object type (%).', _ldsity_object_type;
		END CASE;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity(integer[], integer) IS
'Function returns records from c_ldsity table, optionally for given target_variable.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity(integer[], integer) TO public;

-- </function>

-- <function name="fn_save_target_variable" schema="target_data" src="functions/fn_save_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_target_variable
(
		_label character varying(200),
		_description text,
		_label_en character varying(200) DEFAULT NULL::varchar,
		_description_en text DEFAULT NULL::text,
		_ldsity integer[] DEFAULT NULL::integer[],
		_ldsity_object_type integer[] DEFAULT NULL::integer[],
		_version integer[] DEFAULT NULL::integer[],
		_id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
DECLARE
	_target_variable 	integer;
	_test			boolean;
	_total_100	 	integer;
	_total_200	 	integer;
BEGIN
	IF _id IS NULL
	THEN
		IF _label IS NOT NULL AND _description IS NOT NULL AND _ldsity IS NOT NULL AND _version IS NOT NULL
		THEN
			IF _ldsity IS NULL OR array_length(_ldsity,1) = 0
			THEN RAISE EXCEPTION 'Given array of ldsity contributions is null.';
			END IF;

			IF (SELECT bool_or(ldsity IS NULL) FROM unnest(_ldsity) t(ldsity)) = true
			THEN RAISE EXCEPTION 'Given array of ldsity contributions contains null values.';
			END IF;

			IF array_length(_ldsity,1) != array_length(_ldsity_object_type,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity, _ldsity_object_type;
			END IF;

			IF array_length(_ldsity,1) != array_length(_version,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of versions (%) are not equal!', _ldsity, _version;
			END IF;

			WITH w AS(
				SELECT array_agg(obj_type) AS obj_types
				FROM unnest(_ldsity_object_type) AS t1(obj_type)
				INNER JOIN target_data.c_ldsity_object_type AS t2
				ON t1.obj_type = t2.id
			)
			SELECT array_length(obj_types,1) = array_length(_ldsity_object_type,1)
			FROM w
			INTO _test;

			IF _test = false
			THEN RAISE EXCEPTION 'Given array of ldsity_object_types contains not know values.';
			END IF;

			-- check if correct combination of ldsity_object_types is choosen
			WITH w AS (
				SELECT obj_type, count(*) AS total
				FROM unnest(_ldsity_object_type) AS t(obj_type)
				GROUP BY obj_type
			)
			SELECT total
			FROM w AS t1
			WHERE obj_type = 100
			INTO _total_100;

			WITH w AS (
				SELECT obj_type, count(*) AS total
				FROM unnest(_ldsity_object_type) AS t(obj_type)
				GROUP BY obj_type
			)
			SELECT total
			FROM w AS t1
			WHERE obj_type = 200
			INTO _total_200;

			IF (_total_200 IS NOT NULL AND _total_200 > 0) AND (_total_100 IS NULL OR _total_100 = 0)
			THEN 
				RAISE EXCEPTION 'If some of the ldsity objects are types of 200 then there has to be one ldsity object of type 100 but none was given.';
			END IF;

			IF (_total_200 IS NOT NULL AND _total_200 > 0) AND (_total_100 > 1)
			THEN 
				RAISE EXCEPTION 'If some of the ldsity objects are types of 200 then there has to be one ldsity object of type 100 but more than one was given.';
			END IF;

			INSERT INTO target_data.c_target_variable(label, description, label_en, description_en, state_or_change)
			SELECT _label, _description, _label_en, _description_en, 100
			RETURNING id
			INTO _target_variable;

			INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category, version, use_negative)
			SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], t3.version, false
			FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
			ON t1.id = t2.id
			INNER JOIN unnest(_version) WITH ORDINALITY AS t3(version, id)
			ON t1.id = t3.id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%) and local density contributions (%) must be not null!', _label, _description, _ldsity;
		END IF;
	ELSE
		IF _ldsity IS NULL
		THEN
			UPDATE target_data.c_target_variable
			SET	label = _label,
				description = _description, 
				label_en = _label_en,
				description_en = _description_en
			WHERE c_target_variable.id = _id;

			_target_variable := _id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing target variable and create new one with given ldsitys (%).', _ldsity;
		END IF;
	END IF;

	RETURN _target_variable;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_get_target_variable(integer) CASCADE;
-- <function name="fn_get_target_variable" schema="target_data" src="functions/fn_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_target_variable(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_target_variable(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer[],
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
state_or_change		integer,
soc_label		character varying(200),
soc_description		text,
areal_or_population	integer
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		WITH 
		w_objects AS (
			SELECT t2.ldsity_object_group, array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			GROUP BY t2.ldsity_object_group
			),
		w_core AS (
			SELECT 	t5.target_variable, 
				array_agg(t4.ldsity_object ORDER BY ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		),
		w_target_variable AS (
			SELECT 	t2.ldsity_object_group, t1.target_variable, t3.label, t3.description, t3.label_en, t3.description_en, t3.state_or_change, 
				t4.label AS soc_label, t4.description AS soc_description
			FROM w_core AS t1
			INNER JOIN w_objects AS t2
			ON t1.ldsity_objects = t2.ldsity_objects
			INNER JOIN target_data.c_target_variable AS t3
			ON t1.target_variable = t3.id
			INNER JOIN target_data.c_state_or_change AS t4
			ON t3.state_or_change = t4.id
		),
		w_agg_200 AS (
			SELECT 	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category,
				t5.areal_or_population,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL),
				count(*) OVER (PARTITION BY t1.target_variable) AS no_of_contrib
			FROM w_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.target_variable = t2.target_variable AND t2.ldsity_object_type = 100
			INNER JOIN target_data.c_ldsity AS t4
			ON t2.ldsity = t4.id
			INNER JOIN target_data.c_ldsity_object AS t5
			ON t4.ldsity_object = t5.id
			LEFT JOIN target_data.cm_ldsity2target_variable AS t3
			ON t1.target_variable = t3.target_variable AND t3.ldsity_object_type = 200
			GROUP BY t1.target_variable, t1.ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, t5.areal_or_population
		), w_agg_id AS (
			-- query with one contribution which can be divided by other contributions
			(SELECT array_agg(t1.target_variable) AS ids, t1.ldsity_object_group,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib = 1
			GROUP BY t1.ldsity, t1.ldsity_object_group, 
				t1.area_domain_category, t1.sub_population_category,
				t1.state_or_change, t1.soc_label, t1.soc_description, t1.areal_or_population,
				t1.ldsity_object_type)
			UNION ALL
			-- query with more than one contribution, can not be divided
			(SELECT array_agg(DISTINCT t1.target_variable) AS ids, t1.ldsity_object_group, 
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib > 1
			GROUP BY t1.ldsity_object_group, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type)
		)
		SELECT ids AS id, t1.ldsity_object_group, t1.label[1], t1.description[1], t1.label_en[1], t1.description_en[1],
			t1.state_or_change, t1.soc_label, t1.soc_description,
			t1.areal_or_population[1]
		FROM w_agg_id AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		WITH 
		w_objects AS (
			SELECT t2.ldsity_object_group, array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			WHERE t1.id = _ldsity_object_group
			GROUP BY t2.ldsity_object_group
			),
		w_core AS (
			SELECT 	t5.target_variable, 
				array_agg(t4.ldsity_object ORDER BY ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		),
		w_target_variable AS (
			SELECT 	t2.ldsity_object_group, t1.target_variable, t3.label, t3.description, t3.label_en, t3.description_en, t3.state_or_change, 
				t4.label AS soc_label, t4.description AS soc_description
			FROM w_core AS t1
			INNER JOIN w_objects AS t2
			ON t1.ldsity_objects = t2.ldsity_objects
			INNER JOIN target_data.c_target_variable AS t3
			ON t1.target_variable = t3.id
			INNER JOIN target_data.c_state_or_change AS t4
			ON t3.state_or_change = t4.id
		),
		w_agg_200 AS (
			SELECT 	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category,
				t5.areal_or_population,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL),
				count(*) OVER (PARTITION BY t1.target_variable) AS no_of_contrib
			FROM w_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.target_variable = t2.target_variable AND t2.ldsity_object_type = 100
			INNER JOIN target_data.c_ldsity AS t4
			ON t2.ldsity = t4.id
			INNER JOIN target_data.c_ldsity_object AS t5
			ON t4.ldsity_object = t5.id
			LEFT JOIN target_data.cm_ldsity2target_variable AS t3
			ON t1.target_variable = t3.target_variable AND t3.ldsity_object_type = 200
			GROUP BY t1.target_variable, t1.ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, t5.areal_or_population
		), w_agg_id AS (
			-- query with one contribution which can be divided by other contributions
			(SELECT array_agg(t1.target_variable) AS ids, t1.ldsity_object_group,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib = 1
			GROUP BY t1.ldsity, t1.ldsity_object_group, 
				t1.area_domain_category, t1.sub_population_category,
				t1.state_or_change, t1.soc_label, t1.soc_description, t1.areal_or_population,
				t1.ldsity_object_type)
			UNION ALL
			-- query with more than one contribution, can not be divided
			(SELECT array_agg(DISTINCT t1.target_variable) AS ids, t1.ldsity_object_group, 
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib > 1
			GROUP BY t1.ldsity_object_group, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type)
		)
		SELECT ids AS id, t1.ldsity_object_group, t1.label[1], t1.description[1], t1.label_en[1], t1.description_en[1],
			t1.state_or_change, t1.soc_label, t1.soc_description,
			t1.areal_or_population[1]
		FROM w_agg_id AS t1;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_target_variable(integer) IS
'Function returns records from c_target_variable table, optionally for given ldsity_object_group.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_target_variable(integer) TO public;

-- </function>

-- <function name="fn_get_ldsity_subpop4object" schema="target_data" src="functions/fn_get_ldsity_subpop4object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity4object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity4object(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_subpop4object(_ldsity_object integer)
RETURNS TABLE (
id			integer,
ldsity_object		integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
column_expression	text,
unit_of_measure		integer,
definition_variant	integer[],
version			integer
)
AS
$$
BEGIN

	IF (SELECT areal_or_population FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object) != 100
	THEN
		RAISE EXCEPTION 'Given ldsity object (%) is not of areal type.', _ldsity_object;
	END IF;

	RETURN QUERY
	WITH RECURSIVE w_object AS (
		SELECT t1.id AS ldsity_object, t1.areal_or_population
		FROM target_data.c_ldsity_object AS t1
		WHERE t1.id = _ldsity_object
		UNION ALL
		SELECT t2.id AS ldsity_object, t2.areal_or_population
		FROM w_object AS t1
		INNER JOIN target_data.c_ldsity_object AS t2
		ON t1.ldsity_object = t2.upper_object
	)
	SELECT DISTINCT t2.id, t1.ldsity_object, t2.label, t2.description, t2.label_en, t2.description_en, t2.column_expression, t2.unit_of_measure, t2.definition_variant, t3.version
	FROM w_object AS t1
	INNER JOIN target_data.c_ldsity AS t2
	ON t1.ldsity_object = t2.ldsity_object
	LEFT JOIN target_data.cm_ldsity2panel_refyearset_version AS t3
	ON t2.id = t3.ldsity
	WHERE t1.areal_or_population = 200;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_subpop4object(integer) IS
'Function returns records from c_ldsity table, optionally for given ldsity object and unit_of_measure.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_subpop4object(integer) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_get_ldsity4object(integer, integer) CASCADE;
-- <function name="fn_get_ldsity4object" schema="target_data" src="functions/fn_get_ldsity4object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity4object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity4object(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity4object(_ldsity_object integer DEFAULT NULL::integer, _unit_of_measure integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object		integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
column_expression	text,
unit_of_measure		integer,
definition_variant	integer[],
version			integer
)
AS
$$
BEGIN
	RETURN QUERY
	SELECT DISTINCT t1.id, t1.ldsity_object, t1.label, t1.description, t1.label_en, t1.description_en, t1.column_expression, t1.unit_of_measure, t1.definition_variant, t3.version
	FROM target_data.c_ldsity AS t1
	LEFT JOIN target_data.c_ldsity_object AS t2
	ON t1.ldsity_object = t2.id
	LEFT JOIN target_data.cm_ldsity2panel_refyearset_version AS t3
	ON t1.id = t3.ldsity
	WHERE
		CASE WHEN _ldsity_object IS NOT NULL THEN t2.id = _ldsity_object ELSE true END AND
		CASE WHEN _unit_of_measure IS NOT NULL THEN t1.unit_of_measure = _unit_of_measure ELSE true END;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity4object(integer, integer) IS
'Function returns records from c_ldsity table, optionally for given ldsity object and unit_of_measure.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity4object(integer, integer) TO public;

-- </function>
