--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-----------------------------------------------
-- DDL
-----------------------------------------------
ALTER TABLE target_data.c_unit_of_measure ADD COLUMN label_en varchar(200);
ALTER TABLE target_data.c_unit_of_measure ADD COLUMN description_en text;

ALTER TABLE target_data.c_ldsity ADD COLUMN label_en varchar(200);

ALTER TABLE target_data.c_definition_variant ADD COLUMN label_en varchar(200);
ALTER TABLE target_data.c_definition_variant ADD COLUMN description_en text;

ALTER TABLE target_data.c_area_domain_category ADD COLUMN label_en varchar(200);
ALTER TABLE target_data.c_area_domain_category ADD COLUMN description_en text;

ALTER TABLE target_data.c_sub_population_category ADD COLUMN label_en varchar(200);
ALTER TABLE target_data.c_sub_population_category ADD COLUMN description_en text;

COMMENT ON COLUMN target_data.c_unit_of_measure.label_en IS 'English label of the category.';
COMMENT ON COLUMN target_data.c_unit_of_measure.description_en IS 'Detailed English description of the category.';

COMMENT ON COLUMN target_data.c_ldsity.label_en IS 'English label of the category.';

COMMENT ON COLUMN target_data.c_definition_variant.label_en IS 'English label of the category.';
COMMENT ON COLUMN target_data.c_definition_variant.description_en IS 'Detailed English description of the category.';

COMMENT ON COLUMN target_data.c_area_domain_category.label_en IS 'English label of the category.';
COMMENT ON COLUMN target_data.c_area_domain_category.description_en IS 'Detailed English description of the category.';

COMMENT ON COLUMN target_data.c_sub_population_category.label_en IS 'English label of the category.';
COMMENT ON COLUMN target_data.c_sub_population_category.description_en IS 'Detailed English description of the category.';

----------------------------------------------
-- Functions
-----------------------------------------------
-- <function name="fn_get_target_variable" schema="target_data" src="functions/fn_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_target_variable(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_target_variable(_indicator integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
indicator		integer
)
AS
$$
BEGIN
	IF _indicator IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.indicator
		FROM target_data.c_target_variable AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_indicator AS t1 WHERE t1.id = _indicator)
		THEN RAISE EXCEPTION 'Given target_variable does not exist in table c_indicator (%)', _indicator;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.indicator
		FROM target_data.c_target_variable AS t1
		WHERE t1.indicator = _indicator;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_target_variable(integer) IS
'Function returns records from c_target_variable table, optionally for given indicator.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_target_variable(integer) TO public;

-- </function>

-- <function name="fn_get_ldsity" schema="target_data" src="functions/fn_get_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity(_target_variable integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
target_variable		integer,
label			character varying(200),
label_en		character varying(200),
column_expression	text,
unit_of_measure		integer,
ldsity_object_type	integer,
object_type_label	character varying(200),
object_type_desc	text
)
AS
$$
BEGIN
	IF _target_variable IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS target_variable,
			t1.label, t1.label_en, t1.column_expression, t1.unit_of_measure,
			NULL::int, NULL::varchar(200), NULL::text
		FROM target_data.c_ldsity AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_target_variable (%)', _target_variable;
		END IF;

		RETURN QUERY
		SELECT t4.id, t1.id AS target_variable,
			t3.label, t3.label_en, t3.column_expression, t3.unit_of_measure, 
			t2.ldsity_object_type, t4.label, t4.description
		FROM target_data.c_target_variable AS t1
		INNER JOIN target_data.cm_ldsity2target_variable AS t2
		ON t1.id = t2.target_variable
		INNER JOIN target_data.c_ldsity AS t3
		ON t2.ldsity = t3.id
		INNER JOIN target_data.c_ldsity_object_type AS t4
		ON t2.ldsity_object_type = t4.id
		WHERE t1.id = _target_variable;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity(integer) IS
'Function returns records from c_ldsity table, optionally for given target_variable.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity(integer) TO public;

-- </function>

-- <function name="fn_get_unit_of_measure" schema="target_data" src="functions/fn_get_unit_of_measure.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_unit_of_measure
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_unit_of_measure(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_unit_of_measure(_ldsity integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
ldsity		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _ldsity IS NULL
	THEN
		RETURN QUERY
		SELECT DISTINCT t1.id, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_unit_of_measure AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
		END IF;

		RETURN QUERY
		SELECT DISTINCT t1.id, t2.id AS ldsity, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_unit_of_measure AS t1
		INNER JOIN target_data.c_ldsity AS t2
		ON t1.id = t2.unit_of_measure
		WHERE t2.id = _ldsity;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_unit_of_measure(integer) IS
'Function returns records from c_unit_of_measrue table, optionally for given ldsity.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_unit_of_measure(integer) TO public;

-- </function>

-- <function name="fn_get_definition_variant" schema="target_data" src="functions/fn_get_definition_variant.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_definition_variant
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_definition_variant(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_definition_variant(_ldsity integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
ldsity		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _ldsity IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_definition_variant AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
		END IF;

		RETURN QUERY
		SELECT t3.id, t1.id AS ldsity, t3.label, t3.description, t3.label_en, t3.description_en
		FROM target_data.c_ldsity AS t1,
		unnest(t1.definition_variant) WITH ORDINALITY AS t2(def, id)
		LEFT JOIN target_data.c_definition_variant AS t3
		ON t2.def = t3.id
		WHERE t1.id = _ldsity
		ORDER BY t2.id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_definition_variant(integer) IS
'Function returns records from c_definition_variant table, optionally for given ldsity.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_definition_variant(integer) TO public;

-- </function>

-- <function name="fn_get_area_domain_category" schema="target_data" src="functions/fn_get_area_domain_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_area_domain_category(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_area_domain_category(_ldsity integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
ldsity		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _ldsity IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_area_domain_category AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
		END IF;

		RETURN QUERY
		SELECT t4.id, t1.id AS ldsity, t4.label, t4.description, t4.label_en, t4.description_en
		FROM target_data.c_ldsity AS t1,
		unnest(t1.area_domain_category) WITH ORDINALITY AS t2(def, id)
		LEFT JOIN target_data.cm_adc2classification_rule AS t3
		ON t2.def = t3.id
		LEFT JOIN target_data.c_area_domain_category AS t4
		ON t3.area_domain_category = t4.id
		WHERE t1.id = _ldsity
		ORDER BY t2.id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_area_domain_category(integer) IS
'Function returns records from c_area_domain_category table, optionally for given ldsity.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_area_domain_category(integer) TO public;

-- </function>

-- <function name="fn_get_sub_population_category" schema="target_data" src="functions/fn_get_sub_population_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_sub_population_category(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_sub_population_category(_ldsity integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
ldsity		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _ldsity IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_sub_population_category AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
		END IF;

		RETURN QUERY
		SELECT t4.id, t1.id AS ldsity, t4.label, t4.description, t4.label_en, t4.description_en
		FROM target_data.c_ldsity AS t1,
		unnest(t1.sub_population_category) WITH ORDINALITY AS t2(def, id)
		LEFT JOIN target_data.cm_spc2classification_rule AS t3
		ON t2.def = t3.id
		LEFT JOIN target_data.c_sub_population_category AS t4
		ON t3.sub_population_category = t4.id
		WHERE t1.id = _ldsity
		ORDER BY t2.id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_sub_population_category(integer) IS
'Function returns records from c_sub_population_category table, optionally for given ldsity.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_sub_population_category(integer) TO public;

-- </function>



