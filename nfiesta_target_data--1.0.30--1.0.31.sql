--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <view name="v_variable_hierarchy" schema="target_data" src="views/v_variable_hierarchy.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--alter extension nfiesta_target_data drop view target_data.v_variable_hierarchy;
--DROP VIEW target_data.v_variable_hierarchy;
CREATE OR REPLACE VIEW target_data.v_variable_hierarchy AS
with w_categorization_setups as (
		select
		t_categorization_setup.id as cs_id,
		c_target_variable.id as tv_id 
		--, array_agg(distinct spc_id) filter (where spc_id is not null)
		, max(spc_id) as spc_id, max(sp_id) as sp_id, max(adc_id) as adc_id, max(ad_id) as ad_id
		, max(c_target_variable.label) as tv_label, max(spc_label) as spc_label, max(sp_label) as sp_label, max(adc_label) as adc_label, max(ad_label) as ad_label
	from target_data.t_categorization_setup
	inner join target_data.cm_ldsity2target2categorization_setup ON cm_ldsity2target2categorization_setup.categorization_setup = t_categorization_setup.id
	inner join target_data.cm_ldsity2target_variable ON cm_ldsity2target_variable.id = cm_ldsity2target2categorization_setup.ldsity2target_variable
	inner join target_data.c_target_variable ON c_target_variable.id = cm_ldsity2target_variable.target_variable
	, lateral (	select 
					array_agg(c_sub_population_category.label order by spc2clr.n) as spc_label,
					array_agg(c_sub_population.label order by spc2clr.n) as sp_label,
					array_agg(c_sub_population_category.id order by spc2clr.n) as spc_id,
					array_agg(c_sub_population.id order by spc2clr.n) as sp_id
				from unnest(cm_ldsity2target2categorization_setup.spc2classification_rule) with ordinality as spc2clr(spc2classification_rule, n)
				inner join target_data.cm_spc2classification_rule on (spc2clr.spc2classification_rule = cm_spc2classification_rule.id)
				inner join target_data.c_sub_population_category ON c_sub_population_category.id = cm_spc2classification_rule.sub_population_category
				inner join target_data.c_sub_population ON c_sub_population.id = c_sub_population_category.sub_population
			  ) as spc
	, lateral (	select 
					array_agg(c_area_domain_category.label order by adc2clr.n) as adc_label,
					array_agg(c_area_domain.label order by adc2clr.n) as ad_label,
					array_agg(c_area_domain_category.id order by adc2clr.n) as adc_id,
					array_agg(c_area_domain.id order by adc2clr.n) as ad_id
				from unnest(cm_ldsity2target2categorization_setup.adc2classification_rule) with ordinality as adc2clr(adc2classification_rule, n)
				inner join target_data.cm_adc2classification_rule on (adc2clr.adc2classification_rule = cm_adc2classification_rule.id)
				inner join target_data.c_area_domain_category ON c_area_domain_category.id = cm_adc2classification_rule.area_domain_category
				inner join target_data.c_area_domain ON c_area_domain.id = c_area_domain_category.area_domain
			  ) as adc
	group by t_categorization_setup.id, c_target_variable.id
	order by t_categorization_setup.id
)
, w_sup_inf_pairs as (
	select 
		cs_sup.cs_id as cs_id_sup,
		cs_inf.cs_id as cs_id_inf
		, cs_sup.tv_label as tv_label_sup, cs_sup.spc_label as spc_label_sup, cs_sup.adc_label as adc_label_sup
		, cs_inf.tv_label as tv_label_inf, cs_inf.spc_label as spc_label_inf, cs_inf.adc_label as adc_label_inf
	from w_categorization_setups 		as cs_sup
	inner join w_categorization_setups 	as cs_inf on (cs_sup.tv_id = cs_inf.tv_id) and
			(
				((cs_sup.adc_id = cs_inf.adc_id OR cs_sup.adc_id IS NULL AND cs_inf.adc_id IS NULL) AND
					((cs_sup.spc_id != cs_inf.spc_id AND cs_sup.spc_id <@ cs_inf.spc_id AND (array_length(cs_inf.spc_id,1) = array_length(cs_sup.spc_id,1) + 1)) OR
					 (cs_sup.spc_id IS NULL AND cs_inf.spc_id IS NOT NULL AND array_length(cs_inf.spc_id,1) = 1)))
				OR
				((cs_sup.spc_id = cs_inf.spc_id OR cs_sup.spc_id IS NULL AND cs_inf.spc_id IS NULL) AND
					((cs_sup.adc_id != cs_inf.adc_id AND cs_sup.adc_id <@ cs_inf.adc_id AND (array_length(cs_inf.adc_id,1) = array_length(cs_sup.adc_id,1) + 1)) OR
					 (cs_sup.adc_id IS NULL AND cs_inf.adc_id IS NOT NULL AND array_length(cs_inf.adc_id,1) = 1))))
	order by cs_sup.cs_id, cs_inf.cs_id
)
select 
	cs_id_sup as node,
	array_agg(cs_id_inf order by cs_id_inf) AS edges
	--, tv_label_sup
	, spc_label_sup, adc_label_sup
	--, array_agg(tv_label_inf order by cs_id_inf) AS tv_label_inf
	, array_agg(array_to_string(spc_label_inf, '~') order by cs_id_inf) AS spc_label_inf
	, array_agg(array_to_string(adc_label_inf, '~') order by cs_id_inf) AS adc_label_inf
from w_sup_inf_pairs
group by cs_id_sup
, tv_label_sup, spc_label_sup, adc_label_sup
order by cs_id_sup
;

GRANT SELECT ON TABLE target_data.v_variable_hierarchy TO PUBLIC;
-- </view>
