--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-------------------
-- DDL
-------------------
ALTER TABLE target_data.c_panel_refyearset_group ALTER COLUMN label_en SET NOT NULL;
ALTER TABLE target_data.c_definition_variant ALTER COLUMN label_en SET NOT NULL;
ALTER TABLE target_data.c_area_domain_category ALTER COLUMN label_en SET NOT NULL;
ALTER TABLE target_data.c_sub_population_category ALTER COLUMN label_en SET NOT NULL;
ALTER TABLE target_data.c_ldsity_object_group ALTER COLUMN label_en SET NOT NULL;
ALTER TABLE target_data.c_target_variable ALTER COLUMN label_en SET NOT NULL;
ALTER TABLE target_data.c_ldsity_object ALTER COLUMN label_en SET NOT NULL;
ALTER TABLE target_data.c_version ALTER COLUMN label_en SET NOT NULL;
ALTER TABLE target_data.c_area_domain ALTER COLUMN label_en SET NOT NULL;
ALTER TABLE target_data.c_sub_population ALTER COLUMN label_en SET NOT NULL;
ALTER TABLE target_data.c_ldsity ALTER COLUMN label_en SET NOT NULL;
ALTER TABLE target_data.c_unit_of_measure ALTER COLUMN label_en SET NOT NULL;

ALTER TABLE target_data.c_panel_refyearset_group ALTER COLUMN description_en SET NOT NULL;
ALTER TABLE target_data.c_definition_variant ALTER COLUMN description_en SET NOT NULL;
ALTER TABLE target_data.c_area_domain_category ALTER COLUMN description_en SET NOT NULL;
ALTER TABLE target_data.c_sub_population_category ALTER COLUMN description_en SET NOT NULL;
ALTER TABLE target_data.c_ldsity_object_group ALTER COLUMN description_en SET NOT NULL;
ALTER TABLE target_data.c_target_variable ALTER COLUMN description_en SET NOT NULL;
ALTER TABLE target_data.c_ldsity_object ALTER COLUMN description_en SET NOT NULL;
ALTER TABLE target_data.c_version ALTER COLUMN description_en SET NOT NULL;
ALTER TABLE target_data.c_area_domain ALTER COLUMN description_en SET NOT NULL;
ALTER TABLE target_data.c_sub_population ALTER COLUMN description_en SET NOT NULL;
ALTER TABLE target_data.c_ldsity ALTER COLUMN description_en SET NOT NULL;
ALTER TABLE target_data.c_unit_of_measure ALTER COLUMN description_en SET NOT NULL;


ALTER TABLE target_data.c_target_variable DROP CONSTRAINT c_target_variable_description_en_key; 
ALTER TABLE target_data.c_target_variable DROP CONSTRAINT c_target_variable_description_key; 
ALTER TABLE target_data.c_target_variable DROP CONSTRAINT c_target_variable_label_en_key; 
ALTER TABLE target_data.c_target_variable DROP CONSTRAINT c_target_variable_label_key; 

-------------------
-- Functions
-------------------
-- <function name="fn_save_change_target_variable" schema="target_data" src="functions/fn_save_change_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_change_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_change_target_variable(
	_label character varying(200), 
	_description text, 
	_label_en character varying(200) DEFAULT NULL::varchar, 
	_description_en text DEFAULT NULL::text, 
	_ldsity integer[] DEFAULT NULL::integer[], 
	_ldsity_object_type integer[] DEFAULT NULL::integer[], 
	_version integer[] DEFAULT NULL::integer[], 
	_ldsity_negat integer[] DEFAULT NULL::integer[], 
	_ldsity_object_type_negat integer[] DEFAULT NULL::integer[], 
	_version_negat integer[] DEFAULT NULL::integer[], 
	_id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
DECLARE
	_target_variable integer;
BEGIN
	IF _id IS NULL
	THEN
		IF _label IS NOT NULL AND _description IS NOT NULL AND (_ldsity IS NOT NULL OR _ldsity_negat IS NOT NULL)
		THEN
			IF array_length(_ldsity,1) != array_length(_ldsity_object_type,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity, _ldsity_object_type;
			END IF;

			IF array_length(_ldsity,1) != array_length(_version,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of versions (%) are not equal!', _ldsity, _version;
			END IF;

			IF array_length(_ldsity_negat,1) != array_length(_ldsity_object_type_negat,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions with negative value (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity_negat, _ldsity_object_type_negat;
			END IF;

			IF array_length(_ldsity_negat,1) != array_length(_version_negat,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions with negative value (%) and corresponding assignment of versions (%) are not equal!', _ldsity_negat, _version_negat;
			END IF;

			INSERT INTO target_data.c_target_variable(label, description, label_en, description_en, state_or_change)
			SELECT _label, _description, _label_en, _description_en, 200
			RETURNING id
			INTO _target_variable;

			INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category, version, use_negative)
			(SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], t3.version, false
			FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
			ON t1.id = t2.id
			INNER JOIN unnest(_version) WITH ORDINALITY AS t3(version, id)
			ON t1.id = t3.id
			UNION ALL
			SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], t3.version, true
			FROM unnest(_ldsity_negat) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type_negat) WITH ORDINALITY AS t2(ldsity_object_type, id)
			ON t1.id = t2.id
			INNER JOIN unnest(_version_negat) WITH ORDINALITY AS t3(version, id)
			ON t1.id = t3.id);
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%), local density contributions of pozitive (%) or negative (%) value must be not null!', _label, _description, _ldsity, _ldsity_negat;
		END IF;
	ELSE
		IF _ldsity IS NULL
		THEN
			UPDATE target_data.c_target_variable
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_target_variable.id = _id;

			_target_variable := _id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing target variable and create new one with given ldsitys (%).', _ldsity;
		END IF;
	END IF;

	RETURN _target_variable;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) TO public;

-- </function>

-- <function name="fn_get_ldsity" schema="target_data" src="functions/fn_get_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity(integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity(_target_variable integer[] DEFAULT NULL::integer[], _ldsity_object_type integer DEFAULT 100::integer)
RETURNS TABLE (
id			integer,
target_variable		integer,
label			character varying(200),
label_en		character varying(200),
column_expression	text,
unit_of_measure		integer,
ldsity_object		integer,
ldsity_object_type	integer,
object_type_label	character varying(200),
object_type_desc	text
)
AS
$$
DECLARE
_test		integer;
BEGIN
	IF _target_variable IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS target_variable,
			t1.label, t1.label_en, t1.column_expression, t1.ldsity_object, t1.unit_of_measure,
			NULL::int, NULL::varchar(200), NULL::text
		FROM target_data.c_ldsity AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable[1])
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_target_variable (%)', _target_variable;
		END IF;

		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_type AS t1 WHERE t1.id = _ldsity_object_type)
		THEN RAISE EXCEPTION 'Given ldsity_object_type does not exist in table c_ldsity_object_type (%)', _ldsity_object_type;
		END IF;

		CASE WHEN _ldsity_object_type = 100
		THEN
			-- test if all given target_variables has the same ldsitys 100
			--RETURN QUERY
			WITH w AS (
				SELECT 	DISTINCT t1.id AS target_variable, t3.id AS ldsity, 
					t2.area_domain_category, t2.sub_population_category
				FROM target_data.c_target_variable AS t1
				INNER JOIN target_data.cm_ldsity2target_variable AS t2
				ON t1.id = t2.target_variable
				INNER JOIN target_data.c_ldsity AS t3
				ON t2.ldsity = t3.id
				INNER JOIN target_data.c_ldsity_object_type AS t4
				ON t2.ldsity_object_type = t4.id
				WHERE array[t1.id] <@ _target_variable AND t4.id = 100
			), w_agg_target_variable AS (
				SELECT w.target_variable, w.area_domain_category, w.sub_population_category,
					array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) AS ldsity
				FROM w
				GROUP BY w.target_variable, w.area_domain_category, w.sub_population_category
			)
			SELECT count(DISTINCT ldsity)
			FROM w_agg_target_variable 
			INTO _test;

			IF _test > 1
			THEN
				RAISE EXCEPTION 'Given target variables (in array) does not have the same ldsitys of ldsity object type 100. There is either different ldsity, or the same ldsity but somehow constrained by area domain category or sub population category.';
			END IF;	

			-- return distinct objects for given target variables
			RETURN QUERY
			WITH w AS (
				SELECT  t1.id AS target_variable, count(CASE WHEN t2.ldsity_object_type = 200 THEN 1 ELSE NULL END) AS ldsity_200
				FROM target_data.c_target_variable AS t1
				INNER JOIN target_data.cm_ldsity2target_variable AS t2
				ON t1.id = t2.target_variable
				WHERE array[t1.id] <@ _target_variable
				GROUP BY t1.id
			)
			SELECT  t3.id, t1.id AS target_variable,
				t3.label, t3.label_en, t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
				t2.ldsity_object_type, t4.label, t4.description
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			INNER JOIN w AS t5 ON t1.id = t5.target_variable
			WHERE t4.id = 100 AND CASE WHEN array_length(_target_variable,1) > 1 THEN t5.ldsity_200 = 0 ELSE true END;

		WHEN _ldsity_object_type = 200
		THEN
			WITH w AS (
				SELECT 	DISTINCT t3.id AS ldsity, t1.id AS target_variable,
					t2.area_domain_category, t2.sub_population_category
				FROM target_data.c_target_variable AS t1
				INNER JOIN target_data.cm_ldsity2target_variable AS t2
				ON t1.id = t2.target_variable
				INNER JOIN target_data.c_ldsity AS t3
				ON t2.ldsity = t3.id
				INNER JOIN target_data.c_ldsity_object_type AS t4
				ON t2.ldsity_object_type = t4.id
				WHERE array[t1.id] <@ _target_variable AND t4.id = 100
			), w_agg_target_variable AS (
				SELECT w.target_variable, w.area_domain_category, w.sub_population_category,
					array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) AS ldsity
				FROM w
				GROUP BY w.target_variable, w.area_domain_category, w.sub_population_category
			)
			SELECT count(DISTINCT ldsity)
			FROM w_agg_target_variable 
			INTO _test;

			IF _test > 1
			THEN
				RAISE EXCEPTION 'Given target variables (in array) does not have the same ldsitys of ldsity object type 100. There is either different ldsity, or the same ldsity but somehow constrained by area domain category or sub population category.';
			END IF;	

			RETURN QUERY
			SELECT  t3.id AS ldsity, t1.id AS target_variable,
				t3.label, t3.label_en, t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
				t2.ldsity_object_type, t4.label, t4.description
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _target_variable AND t4.id = 200;
		ELSE
			RAISE EXCEPTION 'Not known value of ldsity object type (%).', _ldsity_object_type;
		END CASE;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity(integer[], integer) IS
'Function returns records from c_ldsity table, optionally for given target_variable.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity(integer[], integer) TO public;

-- </function>

DROP FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) CASCADE;

-- <function name="fn_check_classification_rule" schema="target_data" src="functions/fn_check_classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rule(_ldsity integer, _ldsity_object integer, _rule text, _panel_refyearset integer DEFAULT NULL::integer, _adc integer[] DEFAULT NULL::integer[], _spc integer[] DEFAULT NULL::integer[])
--boolean
RETURNS TABLE (
compliance_status	boolean,
no_of_objects		integer
) 
AS
$$
DECLARE
_ldsity_objects		integer[];
_adc_rule_test		integer[];
_spc_rule_test		integer[];
_table_name4rule	varchar;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
_exist_test		boolean;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
	INTO _test;

	IF _test = false
	THEN
		RAISE EXCEPTION 'Given rule has an invalid syntax.';
	END IF;

	IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
	THEN
		_exist_test := true; _rule = true;
	ELSE 	_exist_test := false;
	END IF;
	
	_case := '('||_rule||')';

	_ldsity_objects := (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]));

	WITH w AS (
		SELECT _ldsity_objects AS ldsity_objects
	)
	SELECT array_agg(t1.rule ORDER BY t1.id) 
	FROM unnest(_adc) WITH ORDINALITY AS t1(rule, id)
	INNER JOIN target_data.cm_adc2classification_rule AS t2
	ON t1.rule = t2.id
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	INNER JOIN w AS t4
	ON ARRAY[t3.id] <@ t4.ldsity_objects
	INTO _adc_rule_test;

	IF _adc_rule_test != _adc
	THEN
		RAISE EXCEPTION 'Given area domain category classification rules does not meet the ldsity objects within hierarchy of local density contribution. Rules which can be used = %, rules given = %.', _adc_rule_test, _adc;
	END IF;

	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	)
	SELECT array_agg(t1.rule ORDER BY t1.id) 
	FROM unnest(_spc) WITH ORDINALITY AS t1(rule, id)
	INNER JOIN target_data.cm_spc2classification_rule AS t2
	ON t1.rule = t2.id
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	INNER JOIN w AS t4
	ON ARRAY[t3.id] <@ t4.ldsity_objects
	INTO _spc_rule_test;

	IF _spc_rule_test != _spc
	THEN
		RAISE EXCEPTION 'Given sub population category classification rules does not meet the ldsity objects within hierarchy of local density contribution. Rules which can be used = %, rules given = %.', _spc_rule_test, _spc;
	END IF;

-- panels and reference year sets must be specified before the check

-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			information_schema.constraint_column_usage AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	_table_name4rule := (SELECT table_name FROM target_data.c_ldsity_object WHERE id = _ldsity_object);

	WITH w_tables AS (
			SELECT
				t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
					CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
					CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',1, ' AS rule_number, ', _case,' AS rul ') ELSE '' END,
					' FROM ', t1.table_name, 
					CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
					') AS ', _table_names_ws[t1.id]) AS table_select
			FROM
				unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
			LEFT JOIN
				(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
				FROM
					(SELECT t4.table_name, t3.classification_rule 
					FROM target_data.c_ldsity AS t1,
						unnest(t1.area_domain_category || _adc) WITH ORDINALITY AS t2(rule, id)
					LEFT JOIN target_data.cm_adc2classification_rule AS t3
					ON t2.rule = t3.id
					INNER JOIN target_data.c_ldsity_object AS t4
					ON t3.ldsity_object = t4.id
					WHERE t1.id = _ldsity
					UNION ALL
					SELECT t4.table_name, t3.classification_rule 
					FROM target_data.c_ldsity AS t1,
						unnest(t1.sub_population_category || _spc) WITH ORDINALITY AS t2(rule, id)
					LEFT JOIN target_data.cm_spc2classification_rule AS t3
					ON t2.rule = t3.id
					INNER JOIN target_data.c_ldsity_object AS t4
					ON t3.ldsity_object = t4.id
					WHERE t1.id = _ldsity
					) AS t1
				GROUP BY table_name
				) AS t2
			ON t1.table_name = t2.table_name
		)
		SELECT array_agg(table_select ORDER BY id)
		FROM w_tables
		INTO _table_selects;

		SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
			_table_names_ws[1], '.', _primary_keys[1], '
			FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
			INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
			INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
			INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
			_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
		INTO _table1;

		_join_all := NULL; _join := NULL;

		FOR i IN 2..array_length(_table_selects,1) 
		LOOP
			SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
			INTO _join;

			_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
			--raise notice '%', _join_all;
		END LOOP;
		--raise notice '%', _join_all;

		--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
		_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

		_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || _join_all;

	RETURN QUERY EXECUTE
	'WITH w AS (' || _q || '),
	w2 AS (SELECT
			id, rul
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
	)
	SELECT rul AS compliance_status, count(*)::int AS no_of_objects
	FROM w2
	GROUP BY rul';

--return query select true, _no_of_rules_met, _no_of_objects;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) IS
'Function returns number of objects classified by the rule for given ldsity and attribute_type hierarchy.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer) CASCADE;
-- <function name="fn_save_target_variable" schema="target_data" src="functions/fn_save_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_target_variable
(
		_label character varying(200),
		_description text,
		_label_en character varying(200) DEFAULT NULL::varchar,
		_description_en text DEFAULT NULL::text,
		_ldsity integer[] DEFAULT NULL::integer[],
		_ldsity_object_type integer[] DEFAULT NULL::integer[],	-- id from table c_ldsity_object_type, one 100 can be partitioned by one or several 200s
		_version integer[] DEFAULT NULL::integer[],		-- id from table c_version
		_id integer[] DEFAULT NULL::integer[])			-- one or more ids of existing records which should be updated (must have the same ldsitys of 100)
RETURNS integer[]
AS
$$
DECLARE
	_target_variable 	integer;
	_test			boolean;
	_total_100	 	integer;
	_total_200	 	integer;
	_target_variables	integer[];
BEGIN
	IF _id IS NULL
	THEN
		IF _label IS NOT NULL AND _description IS NOT NULL AND _ldsity IS NOT NULL AND _version IS NOT NULL
		THEN
			IF _ldsity IS NULL OR array_length(_ldsity,1) = 0
			THEN RAISE EXCEPTION 'Given array of ldsity contributions is null.';
			END IF;

			IF (SELECT bool_or(ldsity IS NULL) FROM unnest(_ldsity) t(ldsity)) = true
			THEN RAISE EXCEPTION 'Given array of ldsity contributions contains null values.';
			END IF;

			IF array_length(_ldsity,1) != array_length(_ldsity_object_type,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity, _ldsity_object_type;
			END IF;

			IF array_length(_ldsity,1) != array_length(_version,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of versions (%) are not equal!', _ldsity, _version;
			END IF;

			WITH w AS(
				SELECT array_agg(obj_type) AS obj_types
				FROM unnest(_ldsity_object_type) AS t1(obj_type)
				INNER JOIN target_data.c_ldsity_object_type AS t2
				ON t1.obj_type = t2.id
			)
			SELECT array_length(obj_types,1) = array_length(_ldsity_object_type,1)
			FROM w
			INTO _test;

			IF _test = false
			THEN RAISE EXCEPTION 'Given array of ldsity_object_types contains not known values.';
			END IF;

			-- check if correct combination of ldsity_object_types is choosen
			WITH w AS (
				SELECT obj_type, count(*) AS total
				FROM unnest(_ldsity_object_type) AS t(obj_type)
				GROUP BY obj_type
			)
			SELECT total
			FROM w AS t1
			WHERE obj_type = 100
			INTO _total_100;

			WITH w AS (
				SELECT obj_type, count(*) AS total
				FROM unnest(_ldsity_object_type) AS t(obj_type)
				GROUP BY obj_type
			)
			SELECT total
			FROM w AS t1
			WHERE obj_type = 200
			INTO _total_200;

			IF (_total_200 IS NOT NULL AND _total_200 > 0) AND (_total_100 IS NULL OR _total_100 = 0)
			THEN 
				RAISE EXCEPTION 'If some of the ldsity objects are types of 200 then there has to be one ldsity object of type 100 but none was given.';
			END IF;

			IF (_total_200 IS NOT NULL AND _total_200 > 0) AND (_total_100 > 1)
			THEN 
				RAISE EXCEPTION 'If some of the ldsity objects are types of 200 then there has to be one ldsity object of type 100 but more than one was given.';
			END IF;

			-- only one areal ldsity can be divided by 200
			IF _total_100 = 1 AND _total_200 > 0 AND (
					SELECT t2.areal_or_population
					FROM target_data.c_ldsity AS t1
					INNER JOIN target_data.c_ldsity_object AS t2
					ON t1.ldsity_object = t2.id 
					WHERE t1.id IN
						(SELECT
							ldsity --, ldsity_object_type
						FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
						INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
						ON t1.id = t2.id
						WHERE t2.ldsity_object_type = 100)

					) = 200
			THEN
				RAISE EXCEPTION 'Only areal ldsitys can be divided by ldsity_object_types 200.';
			END IF;

			IF _total_100 = 1 AND _total_200 > 0 AND (
					SELECT total
					FROM
						(SELECT count(*) AS total, t2.areal_or_population 
						FROM target_data.c_ldsity AS t1
						INNER JOIN target_data.c_ldsity_object AS t2
						ON t1.ldsity_object = t2.id 
						WHERE t1.id IN
							(SELECT ldsity --, ldsity_object_type
							FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
							INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
							ON t1.id = t2.id
							WHERE t2.ldsity_object_type = 200)
						GROUP BY t2.areal_or_population
						) AS t1
					WHERE areal_or_population = 100
					) > 0
			THEN
				RAISE EXCEPTION 'Only subpopulational ldsitys can divide areal ldsity of ldsity_object_type 100.';
			END IF;

			INSERT INTO target_data.c_target_variable(label, description, label_en, description_en, state_or_change)
			SELECT _label, _description, _label_en, _description_en, 100
			RETURNING id
			INTO _target_variable;

			INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category, version, use_negative)
			SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], t3.version, false
			FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
			ON t1.id = t2.id
			INNER JOIN unnest(_version) WITH ORDINALITY AS t3(version, id)
			ON t1.id = t3.id;

			_target_variables := array[_target_variable];
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%) and local density contributions (%) must be not null!', _label, _description, _ldsity;
		END IF;
	ELSE
		WITH w AS (
			SELECT 	DISTINCT t1.id AS target_variable, t3.id AS ldsity, 
				t2.area_domain_category, t2.sub_population_category
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _target_variable AND t4.id = 100
		), w_agg_target_variable AS (
			SELECT w.target_variable, w.area_domain_category, w.sub_population_category,
				array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) AS ldsity
			FROM w
			GROUP BY w.target_variable, w.area_domain_category, w.sub_population_category
		)
		SELECT count(DISTINCT ldsity)
		FROM w_agg_target_variable 
		INTO _test;

		IF _test > 1
		THEN
			RAISE EXCEPTION 'Given target variables (_id = %) does not have the same ldsitys of ldsity object type 100. There is either different ldsity, or the same ldsity but somehow constrained by area domain category or sub population category.', _id;
		END IF;	

		IF _ldsity IS NULL
		THEN
			UPDATE target_data.c_target_variable
			SET	label = _label,
				description = _description, 
				label_en = _label_en,
				description_en = _description_en
			WHERE array[c_target_variable.id] <@ _id;

			_target_variables := _id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing target variable and create new one with given ldsitys (%).', _ldsity;
		END IF;
	END IF;

	RETURN _target_variables;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[]) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[]) TO public;

-- </function>

-- <function name="fn_trg_check_c_ldsity" schema="target_data" src="functions/fn_trg_check_c_ldsity.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_c_ldsity
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_c_ldsity();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_c_ldsity() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
	IF (SELECT areal_or_population FROM target_data.c_ldsity_object WHERE id = NEW.ldsity_object) = 100
		AND NEW.sub_population_category IS NOT NULL
	THEN
		RAISE EXCEPTION 'Ldsity comes from areal ldsity object thus it has to have the sub population category NULL.';
	END IF;

RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_c_ldsity() IS
'This trigger function controls the newly inserted records into c_ldsity table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_c_ldsity() TO public;


-- </function>

-- <function name="fn_trg_check_cm_ldsity2target_variable" schema="target_data" src="functions/fn_trg_check_cm_ldsity2target_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_ldsity2target_variable
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() 
RETURNS TRIGGER AS 
$$
DECLARE
_test1	boolean;
_test2	boolean;
_test3	boolean;
_test4	boolean;
BEGIN
	-- after insert on cm_ldsity2target_variable
	WITH w_new AS (
		SELECT 	
			t2.id, t2.label, t2.description, t2.label_en, t2.description_en,
			array_agg(t1.ldsity ORDER BY t1.ldsity) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY t1.ldsity) AS ldsity_object_type,
			array_agg(t1.use_negative ORDER BY t1.ldsity) AS use_negative
		FROM 
			new_table AS t1
		INNER JOIN
			target_data.c_target_variable AS t2
		ON t1.target_variable = t2.id
		WHERE t1.ldsity_object_type = 100
		GROUP BY t2.id, t2.label, t2.description, t2.label_en, t2.description
	),
	w_existing AS (
		SELECT 	
			t2.id, t2.label, t2.description, t2.label_en, t2.description_en,
			array_agg(t1.ldsity ORDER BY t1.ldsity) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY t1.ldsity) AS ldsity_object_type,
			array_agg(t1.use_negative ORDER BY t1.ldsity) AS use_negative
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN
			target_data.c_target_variable AS t2
		ON t1.target_variable = t2.id
		WHERE t1.ldsity_object_type = 100
		GROUP BY t2.id, t2.label, t2.description, t2.label_en, t2.description
	)
	SELECT
		t1.label = t2.label AS label_test, t1.description = t2.description AS desc_test,
		t1.label_en = t2.label_en AS label_en_test, t1.description_en = t2.description_en AS desc_en_test
	FROM
		w_new AS t1
	INNER JOIN
		w_existing AS t2
	ON t1.ldsity = t2.ldsity AND t1.use_negative = t2.use_negative
	INTO _test1, _test2, _test3, _test4;

	-- if the join on the same ldsity with ldsity_object_type = 100 results in not null output
	-- it means, there already exists target variable with given 100 ldsitys (both constrained and non-constrained)

	IF _test1 IS NOT NULL
	THEN
		-- then we can test the equality
		IF _test1 = false OR _test2 = false OR _test3 = false OR _test4 = false
		THEN
			RAISE EXCEPTION 'Newly inserted target variable has to respect the label (%) and description (%), label_en (%) and description_en (%) respectively, of existing target variables with the same local density contributions (of ldsity_object_type = 100).', _test1, _test2, _test3, _test4;
		END IF;
	END IF; 

	RETURN NULL;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() IS
'This trigger function controls the newly inserted records into c_target_variable table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() TO public;


-- </function>

CREATE TRIGGER trg__c_ldsity
    BEFORE INSERT ON target_data.c_ldsity
    FOR EACH ROW EXECUTE FUNCTION target_data.fn_trg_check_c_ldsity();

CREATE TRIGGER trg__ins__cm_ldsity2target_variable
    AFTER INSERT ON target_data.cm_ldsity2target_variable
    REFERENCING NEW TABLE AS new_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable();

CREATE TRIGGER trg__upd__cm_ldsity2target_variable
    AFTER UPDATE ON target_data.cm_ldsity2target_variable
    REFERENCING NEW TABLE AS new_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable();


