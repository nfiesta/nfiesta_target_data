--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


-- <function name="fn_get_available_refyearset2panel4classifiaction_rule_id " schema="target_data" src="functions/fn_get_available_refyearset2panel4classifiaction_rule_id.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_available_refyearset2panel4classifiaction_rule_id
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_available_refyearset2panel4classifiaction_rule_id(character varying, integer) CASCADE;

create or replace function target_data.fn_get_available_refyearset2panel4classifiaction_rule_id
(
	_classification_rule_type	character varying,
	_classification_rule_id		integer
)
returns table
(
	classification_rule_id		integer,
	refyearset2panel			integer
)
as
$$
declare
		_table_name							text;
		_ldsity_object						integer;
		_classification_rule				text;
		_example_query						text;
		_ldsity_objects						integer[];
		_filter_i							text;
		_filters_array						text[];
		_example_query_i					text;
		_example_query_array				text[];
		_column_name_i_array				text[];
		_ii_column_new_text					text;
		_string4columns						text;
		_string4inner_joins					text;
		_position4join						integer;
		_table_name4join					text;
		_pkey_column4join					text;
		_i_column4join						text;
		_string4refyearset2panel_mapping	text;
		_query_res							text;
		_column_name_i_array_4_condition	text[];
		_string4condition_ii				text;
begin
		-----------------------------------------------------------------------
		if _classification_rule_type is null
		then
			raise exception 'Error 01: fn_get_available_refyearset2panel4classifiaction_rule_id: Input argument _classification_rule_type must not be NULL!';
		end if;
	
		if _classification_rule_id is null
		then
			raise exception 'Error 02: fn_get_available_refyearset2panel4classifiaction_rule_id: Input argument _classification_rule_id must not be NULL!';
		end if;

		if not(_classification_rule_type = any(array['adc','spc']))
		then
			raise exception 'Error 03: fn_get_available_refyearset2panel4classifiaction_rule_id: Input argument _classification_rule_type must be "adc" or "spc"!';
		end if;
		-----------------------------------------------------------------------
		if _classification_rule_type = 'adc'
		then
			_table_name := 'cm_adc2classification_rule'; 
		end if;
		
		if _classification_rule_type = 'spc'
		then
			_table_name := 'cm_spc2classification_rule';
		end if;
		-----------------------------------------------------------------------
		execute
		'
		select
				ldsity_object,
				case when classification_rule is null then ''true'' else classification_rule end as classification_rule
		from
				target_data.'||_table_name||'
		where
				id = $1
		'
		using _classification_rule_id
		into
				_ldsity_object,
				_classification_rule;
		-----------------------------------------------------------------------	
		if _classification_rule = any(array['EXISTS','NOT EXISTS'])
		then
			_query_res := concat(
			'
			with
			w as	(
					select
							0 as id,
							',_classification_rule_id,' as classification_rule_id,
							null::integer as refyearset2panel
					)
			select classification_rule_id, refyearset2panel from w
			where id > 0;
			');
		else
			-----------------------------------------------------------------------	
			_example_query := concat('w_&i& as (select #column_names_&i&# from #table_name#_&i& where #con4filter# and (#con4object#))');			
			-----------------------------------------------------------------------	
			_ldsity_objects := (select * from target_data.fn_get_ldsity_objects(array[_ldsity_object]));
			-----------------------------------------------------------------------	
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					select case when filter is null then 'true' else filter end
					from target_data.c_ldsity_object
					where id = _ldsity_objects[i]
					into _filter_i;
				
					if i = 1
					then
						_filters_array := array[_filter_i];
					else
						_filters_array := _filters_array || array[_filter_i];
					end if;
			end loop;
			-----------------------------------------------------------------------	
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop		
					_example_query_i := replace(_example_query,'&i&'::text,i::text);
					_example_query_i := replace(_example_query_i,'#con4filter#',(select case when filter is null then 'true' else filter end from target_data.c_ldsity_object where id = _ldsity_objects[i]));
					_example_query_i := replace(_example_query_i,concat('#table_name#_',i),(select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]));
				
					if (_ldsity_objects[i] = _ldsity_object) and _ldsity_object is distinct from 100
					then
							select array_agg(column_name order by ordinal_position) from information_schema.columns
						 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
						 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
							into _column_name_i_array_4_condition;
						
							for ii in 1..array_length(_column_name_i_array_4_condition,1)
							loop
								if (select position(_column_name_i_array_4_condition[ii] in _classification_rule)) > 0
								then
									if ii = 1
									then
										_string4condition_ii := concat(_column_name_i_array_4_condition[ii],' is not null');
									else
										_string4condition_ii := _string4condition_ii || ' and ' || concat(_column_name_i_array_4_condition[ii],' is not null');
									end if;
								else
									if ii = 1
									then
										_string4condition_ii := 'true';
									else
										_string4condition_ii := _string4condition_ii;
									end if;
								end if;
							end loop;
						
						_example_query_i := replace(_example_query_i,'#con4object#',_string4condition_ii);
					else
						_example_query_i := replace(_example_query_i,'#con4object#','true');
					end if;
				
					if i = 1
					then
						_example_query_array := array[_example_query_i];
					else
						_example_query_array := _example_query_array || _example_query_i;
					end if;
			end loop;
			-----------------------------------------------------------------------	
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					if i = 1
					then
						_query_res := concat('with ',_example_query_array[i]);
					else
						_query_res := _query_res || concat(',',_example_query_array[i]);
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
				select array_agg(column_name order by ordinal_position) from information_schema.columns
			 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
			 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
				into _column_name_i_array;
				
				for ii in 1..array_length(_column_name_i_array,1)
				loop
					if ii = 1
					then
						_ii_column_new_text := concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i);
					else
						_ii_column_new_text := concat(_ii_column_new_text,', ',concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i));
					end if;
				end loop;
			
				_query_res := replace(_query_res,concat('#column_names_',i::text,'#'),_ii_column_new_text);
				
			end loop;	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
				if i = 1
				then
					_string4columns := concat(',w_res as (select w_',i,'.*');
					_string4inner_joins := concat(' from w_',i);
				else
					_string4columns := concat(_string4columns,', w_',i,'.*');
				
					select array_position(_ldsity_objects, 
						(select upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]))
					into _position4join;
					
					for i in 1..array_length(_ldsity_objects,1)
					loop
							if i = _position4join
							then
								_table_name4join := (select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]);
							end if;
					end loop;
				
					with
					w as	(
							select kcu.table_schema,
							       kcu.table_name,
							       tco.constraint_name,
							       kcu.ordinal_position as position,
							       kcu.column_name as key_column
							from information_schema.table_constraints tco
							join information_schema.key_column_usage kcu 
							     on kcu.constraint_name = tco.constraint_name
							     and kcu.constraint_schema = tco.constraint_schema
							     and kcu.constraint_name = tco.constraint_name
							where tco.constraint_type = 'PRIMARY KEY'
							order by kcu.table_schema,
							         kcu.table_name,
							         position
							 )
					select key_column::text from w
					where table_schema = (substring(_table_name4join from 1 for (position('.' in _table_name4join) - 1))) 
					and table_name = (substring(_table_name4join from (position('.' in _table_name4join) + 1) for length(_table_name4join)))
					into _pkey_column4join;
					
					select column4upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]
					into _i_column4join;
				
					_string4inner_joins :=
					concat(_string4inner_joins,' inner join w_',i,' on ','w_',_position4join,'.',_pkey_column4join,'_',_position4join,' = ','w_',i,'.',_i_column4join,'_',i);
							
				end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------		
			_query_res := _query_res || _string4columns || _string4inner_joins || ')';	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_string4refyearset2panel_mapping := concat(
			'
			,w1 as	(
					select a.*, b.cluster, c.panel
					from			
						(
						select * from w_res
						) as a 
					inner join sdesign.f_p_plot as b on a.plot_1 = b.gid
					inner join sdesign.cm_cluster2panel_mapping as c on b.cluster = c.cluster
					)
			,w2 as	(
					select distinct panel, reference_year_set_1 as reference_year_set from w1
					)
			,w3 as	(
					select w2.*, t1.* from w2 inner join
					sdesign.cm_refyearset2panel_mapping as t1
					on w2.panel = t1.panel and w2.reference_year_set = t1.reference_year_set
					)
					select
							',_classification_rule_id,' as classification_rule_id,
							id as refyearset2panel
					from
							w3 order by id;
			');
			----------------------------------------------------------------------
			_query_res := _query_res || _string4refyearset2panel_mapping;
			----------------------------------------------------------------------
		end if;
	
		return query execute ''||_query_res||'';
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_available_refyearset2panel4classifiaction_rule_id(character varying, integer) is
'The function gets list of avilable IDs of refyear2panel for given input arguments.';

grant execute on function target_data.fn_get_available_refyearset2panel4classifiaction_rule_id(character varying, integer) to public;
-- </function>