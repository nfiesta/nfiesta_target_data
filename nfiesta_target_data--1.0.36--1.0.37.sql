--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-------------------
-- DDL
-------------------

-------------------
-- Functions
-------------------


DROP FUNCTION target_data.fn_save_target_variable(_label character varying, _description text, _use_negative boolean[], _version integer[], _label_en character varying,_description_en text, _state_or_change integer, _ldsity integer[], _ldsity_object_type integer[], _id integer);

-- <function name="fn_try_delete_ldsity_object_group" schema="target_data" src="functions/fn_try_delete_ldsity_object_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_delete_ldsity_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_try_delete_ldsity_object_group(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_delete_ldsity_object_group(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _id)
	THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _id;
	END IF;

	RETURN NOT EXISTS (
		WITH w_group AS (
			SELECT array_agg(t3.id ORDER BY t3.id) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			INNER JOIN target_data.c_ldsity_object AS t3
			ON t2.ldsity_object = t3.id
			WHERE t1.id = _id
		), w_tv AS (
			SELECT target_variable, array_agg(t4.ldsity_object ORDER BY t4.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			INNER JOIN target_data.c_target_variable AS t6
			ON t5.target_variable = t6.id
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		)
		SELECT t2.target_variable
		FROM w_group AS t1
		INNER JOIN w_tv AS t2
		ON t1.ldsity_objects = t2.ldsity_objects);
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_delete_ldsity_object_group(integer) IS
'Function provides test if it is possible to delete records from c_ldsity_object_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_delete_ldsity_object_group(integer) TO public;

-- </function>

DROP FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer,integer,integer);

-- <function name="fn_get_ldsity_object4ld_object_adsp" schema="target_data" src="functions/fn_get_ldsity_object4ld_object_adsp.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object4ld_object_adsp
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(_ldsity_object integer, _areal_or_population integer, _id integer)
RETURNS TABLE (
id			integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
classification_rule	boolean
)
AS
$$
BEGIN
	IF _ldsity_object IS NULL
	THEN
		RAISE EXCEPTION 'Parameter _ldsity_object is mandatory and must not be null!';
	END IF;

	IF _ldsity_object IS NULL
	THEN
		RAISE EXCEPTION 'Parameter _ldsity_object is mandatory and must not be null!';
	END IF;

	CASE
	WHEN _areal_or_population = 100
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
		THEN
			RAISE EXCEPTION 'Given area domain (%) does not exist in table c_area_domain!', _id;
		END IF;

		RETURN QUERY
		WITH w AS (
			SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
		)
		SELECT 	t3.id, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, 
			CASE WHEN t4.ldsity_object IS NOT NULL THEN true END AS rule
		FROM w AS t1,
			unnest(t1.ldsity_objects) AS t2(ldsity_object)
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		LEFT JOIN 
			(SELECT DISTINCT t1.area_domain, t2.ldsity_object
			FROM target_data.c_area_domain_category AS t1
			INNER JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.id = t2.area_domain_category
			WHERE t1.area_domain = _id) AS t4
		ON t3.id = t4.ldsity_object;

	WHEN _areal_or_population = 200
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _id)
		THEN
			RAISE EXCEPTION 'Given population (%) does not exist in table c_sub_population!', _id;
		END IF;

		RETURN QUERY
		WITH w AS (
			SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
		)
		SELECT 	t3.id, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, 
			CASE WHEN t4.ldsity_object IS NOT NULL THEN true END AS rule
		FROM w AS t1,
			unnest(t1.ldsity_objects) AS t2(ldsity_object)
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		LEFT JOIN 
			(SELECT DISTINCT t1.sub_population, t2.ldsity_object
			FROM target_data.c_sub_population_category AS t1
			INNER JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.sub_population = _id) AS t4
		ON t3.id = t4.ldsity_object;
	ELSE
		RAISE EXCEPTION 'Not known value of areal or population (%)!', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) IS
'Function returns records from c_ldsity_object table for given area domain or population.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) TO public;

-- </function>

-- <function name="fn_trg_check_cm_ldisty2target_variable" schema="target_data" src="functions/fn_trg_check_cm_ldsity2target_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_ldsity2target_variable
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() 
RETURNS TRIGGER AS 
$$
DECLARE
_test1		boolean;
_test2		boolean;
_test3		boolean;
_test4		boolean;
_variables 	integer[];
_use_negative	boolean[];
_100100		integer[];
_100200		integer[];
_200100		integer[];
_200200		integer[];
BEGIN
	-- after insert on cm_ldsity2target_variable
	WITH w_new AS (
		SELECT 	
			t2.id, t2.label, t2.description, t2.label_en, t2.description_en,
			array_agg(t1.ldsity ORDER BY t1.ldsity) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY t1.ldsity) AS ldsity_object_type,
			array_agg(t1.use_negative ORDER BY t1.ldsity) AS use_negative
		FROM 
			new_table AS t1
		INNER JOIN
			target_data.c_target_variable AS t2
		ON t1.target_variable = t2.id
		WHERE t1.ldsity_object_type = 100
		GROUP BY t2.id, t2.label, t2.description, t2.label_en, t2.description
	),
	w_existing AS (
		SELECT 	
			t2.id, t2.label, t2.description, t2.label_en, t2.description_en,
			array_agg(t1.ldsity ORDER BY t1.ldsity) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY t1.ldsity) AS ldsity_object_type,
			array_agg(t1.use_negative ORDER BY t1.ldsity) AS use_negative
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN
			target_data.c_target_variable AS t2
		ON t1.target_variable = t2.id
		WHERE t1.ldsity_object_type = 100
		GROUP BY t2.id, t2.label, t2.description, t2.label_en, t2.description
	)
	SELECT
		t1.label = t2.label AS label_test, t1.description = t2.description AS desc_test,
		t1.label_en = t2.label_en AS label_en_test, t1.description_en = t2.description_en AS desc_en_test
	FROM
		w_new AS t1
	INNER JOIN
		w_existing AS t2
	ON t1.ldsity = t2.ldsity AND t1.use_negative = t2.use_negative
	INTO _test1, _test2, _test3, _test4;

	-- if the join on the same ldsity with ldsity_object_type = 100 results in not null output
	-- it means, there already exists target variable with given 100 ldsitys (both constrained and non-constrained)

	IF _test1 IS NOT NULL
	THEN
		-- then we can test the equality
		IF _test1 = false OR _test2 = false OR _test3 = false OR _test4 = false
		THEN
			RAISE EXCEPTION 'Newly inserted target variable has to respect the label (%) and description (%), label_en (%) and description_en (%) respectively, of existing target variables with the same local density contributions (of ldsity_object_type = 100).', _test1, _test2, _test3, _test4;
		END IF;
	END IF; 

	-- check if correct combination of ldsity_object_types is choosen
	WITH 
	w_tv AS (SELECT distinct target_variable, use_negative
		FROM new_table
	),
	w_type AS (
		SELECT t1.target_variable, t1.ldsity_object_type, t3.areal_or_population, t1.use_negative, count(*) AS total
		FROM new_table AS t1
		INNER JOIN target_data.c_ldsity AS t2
		ON t1.ldsity = t2.id
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		GROUP BY t1.target_variable, t1.ldsity_object_type, t3.areal_or_population, t1.use_negative
	),
	w_total AS (
		SELECT 	t1.target_variable AS target_variable, t1.use_negative,
			coalesce(t2.total,0) AS total_100100, coalesce(t3.total,0) AS total_100200,
			coalesce(t4.total,0) AS total_200100, coalesce(t5.total,0) AS total_200200
		FROM
			w_tv AS t1
		LEFT JOIN w_type AS t2
		ON 	t1.target_variable = t2.target_variable AND t1.use_negative = t2.use_negative
			AND t2.ldsity_object_type = 100 AND t2.areal_or_population = 100
		LEFT JOIN w_type AS t3
		ON 	t1.target_variable = t3.target_variable AND t1.use_negative = t3.use_negative
			AND t3.ldsity_object_type = 100 AND t3.areal_or_population = 200
		LEFT JOIN w_type AS t4
		ON 	t1.target_variable = t4.target_variable AND t1.use_negative = t4.use_negative
			AND t4.ldsity_object_type = 200 AND t4.areal_or_population = 100
		LEFT JOIN w_type AS t5
		ON 	t1.target_variable = t5.target_variable AND t1.use_negative = t5.use_negative
			AND t5.ldsity_object_type = 200 AND t5.areal_or_population = 200
			  
	), w_result AS (
		SELECT
			target_variable, use_negative, total_100100, total_100200, total_200100, total_200200,
			CASE
			WHEN total_100100 = 1 AND total_100200 = 0 AND total_200100 = 0 AND total_200200 = 0 THEN true	-- only one areal
			WHEN total_100100 = 0 AND total_100200 >= 1 AND total_200100 = 0 AND total_200200 = 0 THEN true -- one or more populational
			WHEN total_100100 = 1 AND total_100200 = 0 AND total_200100 = 0 AND total_200200 >= 1 THEN true -- one areal divided by one or more populational
			ELSE false -- everything else does not make sense
			END AS validity	
		FROM w_total
	)
	SELECT 	
		array_agg(total_100100 ORDER BY target_variable, use_negative),
		array_agg(total_100200 ORDER BY target_variable, use_negative),
		array_agg(total_200100 ORDER BY target_variable, use_negative),
		array_agg(total_200200 ORDER BY target_variable, use_negative),
		array_agg(use_negative ORDER BY target_variable, use_negative),
		array_agg(target_variable ORDER BY target_variable, use_negative)
	FROM w_result
	WHERE validity = false
	INTO _100100,_100200,_200100,_200200,_use_negative,_variables;

--raise notice '%, %, %, %', _100100,_100200,_200100,_200200;

	IF _variables IS NOT NULL
	THEN
		RAISE EXCEPTION 'Some of the inserted target variables (%) (can be only pozitive or negative part of the variable) has violited ldsity_object_type and areal/population combination constraints.', _variables;
	END IF;

	RETURN NULL;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() IS
'This trigger function controls the newly inserted records into c_target_variable table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() TO public;


-- </function>
