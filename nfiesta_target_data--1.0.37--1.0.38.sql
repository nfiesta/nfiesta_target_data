--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-------------------
-- DDL
-------------------

-------------------
-- Functions
-------------------


-- <function name="fn_import_data" schema="target_data" src="functions/fn_import_data.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_data(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_import_data(_schema character varying)
RETURNS text
AS
$$
DECLARE
		_res	text;
BEGIN
	-- Data for Name: c_ldsity_object; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity_object(id, label, description, label_en, description_en, table_name, upper_object, areal_or_population, column4upper_object, filter)
	select id, label, description, label_en, description_en, table_name, upper_object, areal_or_population, column4upper_object, filter from '||_schema||'.c_ldsity_object order by id';

	-- Data for Name: c_unit_of_measure; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_unit_of_measure(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_unit_of_measure order by id';

	-- Data for Name: c_definition_variant; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_definition_variant(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_definition_variant order by id';	

	-- Data for Name: c_area_domain; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_area_domain order by id';

	-- Data for Name: c_area_domain_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain_category(id, label, description, label_en, description_en, area_domain)
	select id, label, description, label_en, description_en, area_domain from '||_schema||'.c_area_domain_category order by id';

	-- Data for Name: cm_adc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_adc2classification_rule(area_domain_category, ldsity_object, classification_rule)
	select area_domain_category, ldsity_object, classification_rule from '||_schema||'.cm_adc2classification_rule order by id';	

	-- Data for Name: c_sub_population; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_sub_population order by id';

	-- Data for Name: c_sub_population_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population_category(id, label, description, label_en, description_en, sub_population)
	select id, label, description, label_en, description_en, sub_population from '||_schema||'.c_sub_population_category order by id';

	-- Data for Name: cm_spc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_spc2classification_rule(sub_population_category, ldsity_object, classification_rule)
	select sub_population_category, ldsity_object, classification_rule from '||_schema||'.cm_spc2classification_rule order by id';	

	-- Data for Name: c_ldsity; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity(id, label, description, label_en, description_en, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant)
	select id, label, description, label_en, description_en, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant from '||_schema||'.c_ldsity order by id';

	-- Data for Name: t_adc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_adc_hierarchy(variable_superior, variable)
	select variable_superior, variable from '||_schema||'.t_adc_hierarchy order by id';

	-- Data for Name: t_spc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_spc_hierarchy(variable_superior, variable)
	select variable_superior, variable from '||_schema||'.t_spc_hierarchy order by id';

	-- Data for Name: c_version; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_version(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_version order by id';	

	-- Data for Name: c_version; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_ldsity2panel_refyearset_version(ldsity, refyearset2panel, version)
	select ldsity, refyearset2panel, version from '||_schema||'.cm_ldsity2panel_refyearset_version order by id';

	-- Data for Name: cm_adc2classrule2panel_refyearset; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_adc2classrule2panel_refyearset(adc2classification_rule, refyearset2panel)
	select adc2classification_rule, refyearset2panel from '||_schema||'.cm_adc2classrule2panel_refyearset order by id';	

	-- Data for Name: cm_spc2classrule2panel_refyearset; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_spc2classrule2panel_refyearset(spc2classification_rule, refyearset2panel)
	select spc2classification_rule, refyearset2panel from '||_schema||'.cm_spc2classrule2panel_refyearset order by id';		

	-- Name: c_ldsity_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_ldsity_id_seq', (select max(id) from target_data.c_ldsity), true);


	_res := 'Import is comleted.';

	return _res;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_import_data(character varying) IS
'The function imports data from source tables into tables that are in the target_data schema.';
-- </function>



-- <function name="fn_trg_check_cm_adc2classification_rule" schema="target_data" src="functions/fn_trg_check_cm_adc2classification_rule.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_adc2classification_rule
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_adc2classification_rule();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_adc2classification_rule() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF NOT(new.classification_rule = any(array['EXISTS','NOT EXISTS']))
		THEN
			IF	(
				SELECT count(*) FROM target_data.cm_adc2classrule2panel_refyearset
				WHERE adc2classification_rule = new.id
				) = 0
			THEN
				RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_adc2classification_rule: For new inserted classification rule: [area_domain_category = %, ldsity_object = %, classification_rule = "%"], not exists any record in table cm_adc2classrule2panel_refyearset!', new.area_domain_category, new.ldsity_object, new.classification_rule;
			END IF;
		END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_adc2classification_rule() IS
'This trigger function controls that for new inserted record into cm_adc2classification_rule table exists at least one record in cm_adc2classrule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_adc2classification_rule() TO public;
-- </function>



-- <function name="fn_trg_check_cm_spc2classification_rule" schema="target_data" src="functions/fn_trg_check_cm_spc2classification_rule.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_spc2classification_rule
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_spc2classification_rule();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_spc2classification_rule() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF NOT(new.classification_rule = any(array['EXISTS','NOT EXISTS']))
		THEN
			IF	(
				SELECT count(*) FROM target_data.cm_spc2classrule2panel_refyearset
				WHERE spc2classification_rule = new.id
				) = 0
			THEN
				RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_spc2classification_rule: For new inserted classification rule: [sub_population_category = %, ldsity_object = %, classification_rule = "%"], not exists any record in table cm_adc2classrule2panel_refyearset!', new.sub_population_category, new.ldsity_object, new.classification_rule;
			END IF;
		END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_spc2classification_rule() IS
'This trigger function controls that for new inserted record into cm_spc2classification_rule table exists at least one record in cm_spc2classrule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_spc2classification_rule() TO public;
-- </function>



-- <function name="fn_trg_check_cm_adc2classrule2panel_refyearset" schema="target_data" src="functions/fn_trg_check_cm_adc2classrule2panel_refyearset.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_adc2classrule2panel_refyearset
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_adc2classrule2panel_refyearset();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_adc2classrule2panel_refyearset() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF (TG_OP = 'DELETE')
		THEN
			IF	(
				SELECT count(*) FROM target_data.cm_adc2classrule2panel_refyearset
				WHERE adc2classification_rule = old.adc2classification_rule
				AND id IS DISTINCT FROM old.id
				) = 0
			THEN
				RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_adc2classrule2panel_refyearset: This record [adc2classification_rule = %, refyearset2panel = %] cannot be deleted from the cm_adc2classrule2panel_refyearset table, becouse at least one record for a given classification rule must remain in the table.', old.adc2classification_rule, old.refyearset2panel;
			END IF;

			RETURN OLD;
		END IF;

		IF (TG_OP = 'UPDATE')
		THEN
			IF	(
				SELECT count(*) FROM target_data.cm_adc2classrule2panel_refyearset
				WHERE adc2classification_rule = old.adc2classification_rule
				AND id IS DISTINCT FROM old.id
				) = 0
			THEN
				RAISE EXCEPTION 'Error: 02: fn_trg_check_cm_adc2classrule2panel_refyearset: This record [adc2classification_rule = %, refyearset2panel = %] cannot be updated in the cm_adc2classrule2panel_refyearset table, becouse after update at least one record for a given classification rule must remain in the table.', old.adc2classification_rule, old.refyearset2panel;
			END IF;

			RETURN NEW;
		END IF;

	RETURN NULL;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_adc2classrule2panel_refyearset() IS
'This trigger function controls wether given of record can be deleted or updated. After delete or after update must remain at least one record for a given rule in the table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_adc2classrule2panel_refyearset() TO public;
-- </function>



-- <function name="fn_trg_check_cm_spc2classrule2panel_refyearset" schema="target_data" src="functions/fn_trg_check_cm_spc2classrule2panel_refyearset.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_spc2classrule2panel_refyearset
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_spc2classrule2panel_refyearset();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_spc2classrule2panel_refyearset() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF (TG_OP = 'DELETE')
		THEN
			IF	(
				SELECT count(*) FROM target_data.cm_spc2classrule2panel_refyearset
				WHERE spc2classification_rule = old.spc2classification_rule
				AND id IS DISTINCT FROM old.id
				) = 0
			THEN
				RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_spc2classrule2panel_refyearset: This record [spc2classification_rule = %, refyearset2panel = %] cannot be deleted from the cm_spc2classrule2panel_refyearset table, becouse at least one record for a given classification rule must remain in the table.', old.spc2classification_rule, old.refyearset2panel;
			END IF;

			RETURN OLD;
		END IF;

		IF (TG_OP = 'UPDATE')
		THEN
			IF	(
				SELECT count(*) FROM target_data.cm_spc2classrule2panel_refyearset
				WHERE spc2classification_rule = old.spc2classification_rule
				AND id IS DISTINCT FROM old.id
				) = 0
			THEN
				RAISE EXCEPTION 'Error: 02: fn_trg_check_cm_spc2classrule2panel_refyearset: This record [spc2classification_rule = %, refyearset2panel = %] cannot be updated in the cm_spc2classrule2panel_refyearset table, becouse after update at least one record for a given classification rule must remain in the table.', old.spc2classification_rule, old.refyearset2panel;
			END IF;

			RETURN NEW;
		END IF;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_spc2classrule2panel_refyearset() IS
'This trigger function controls wether given of record can be deleted or updated. After delete or after update must remain at least one record for a given rule in the table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_spc2classrule2panel_refyearset() TO public;
-- </function>




-------------------
-- Triggers
-------------------

-- DROP TRIGGER trg_cm_adc2classification_rule_after_insert ON target_data.cm_adc2classification_rule;

CREATE CONSTRAINT TRIGGER trg_cm_adc2classification_rule_after_insert
  AFTER INSERT
  ON target_data.cm_adc2classification_rule
  DEFERRABLE INITIALLY DEFERRED
  FOR EACH ROW
  EXECUTE PROCEDURE target_data.fn_trg_check_cm_adc2classification_rule();
COMMENT ON TRIGGER trg_cm_adc2classification_rule_after_insert ON target_data.cm_adc2classification_rule IS
'This trigger controls that for new inserted record into cm_adc2classification_rule table exists at least one record in cm_adc2classrule2panel_refyearset table.';


-- DROP TRIGGER trg_cm_spc2classification_rule_after_insert ON target_data.cm_spc2classification_rule;

CREATE CONSTRAINT TRIGGER trg_cm_spc2classification_rule_after_insert
  AFTER INSERT
  ON target_data.cm_spc2classification_rule
  DEFERRABLE INITIALLY DEFERRED
  FOR EACH ROW
  EXECUTE PROCEDURE target_data.fn_trg_check_cm_spc2classification_rule();
COMMENT ON TRIGGER trg_cm_spc2classification_rule_after_insert ON target_data.cm_spc2classification_rule IS
'This trigger controls that for new inserted record into cm_spc2classification_rule table exists at least one record in cm_spc2classrule2panel_refyearset table.';


-- DROP TRIGGER trg_cm_adc2classrule2panel_refyearset_before_delete ON target_data.cm_adc2classrule2panel_refyearset;

CREATE TRIGGER trg_cm_adc2classrule2panel_refyearset_before_delete
  BEFORE DELETE
  ON target_data.cm_adc2classrule2panel_refyearset
  FOR EACH ROW
  EXECUTE PROCEDURE target_data.fn_trg_check_cm_adc2classrule2panel_refyearset();
COMMENT ON TRIGGER trg_cm_adc2classrule2panel_refyearset_before_delete ON target_data.cm_adc2classrule2panel_refyearset IS
'This trigger controls that after delete record from cm_adc2classrule2panel_refyearset table will remain at least one record in cm_adc2classrule2panel_refyearset table for given classification rule.';


-- DROP TRIGGER trg_cm_spc2classrule2panel_refyearset_before_delete ON target_data.cm_spc2classrule2panel_refyearset;

CREATE TRIGGER trg_cm_spc2classrule2panel_refyearset_before_delete
  BEFORE DELETE
  ON target_data.cm_spc2classrule2panel_refyearset
  FOR EACH ROW
  EXECUTE PROCEDURE target_data.fn_trg_check_cm_spc2classrule2panel_refyearset();
COMMENT ON TRIGGER trg_cm_spc2classrule2panel_refyearset_before_delete ON target_data.cm_spc2classrule2panel_refyearset IS
'This trigger controls that after delete record from cm_spc2classrule2panel_refyearset table will remain at least one record in cm_spc2classrule2panel_refyearset table for given classification rule.';


-- DROP TRIGGER trg_cm_adc2classrule2panel_refyearset_before_update ON target_data.cm_adc2classrule2panel_refyearset;

CREATE TRIGGER trg_cm_adc2classrule2panel_refyearset_before_update
  BEFORE UPDATE
  ON target_data.cm_adc2classrule2panel_refyearset
  FOR EACH ROW
  EXECUTE PROCEDURE target_data.fn_trg_check_cm_adc2classrule2panel_refyearset();
COMMENT ON TRIGGER trg_cm_adc2classrule2panel_refyearset_before_update ON target_data.cm_adc2classrule2panel_refyearset IS
'This trigger controls that after update record in cm_adc2classrule2panel_refyearset table will remain at least one record in cm_adc2classrule2panel_refyearset table for given classification rule.';


-- DROP TRIGGER trg_cm_spc2classrule2panel_refyearset_before_update ON target_data.cm_spc2classrule2panel_refyearset;

CREATE TRIGGER trg_cm_spc2classrule2panel_refyearset_before_update
  BEFORE UPDATE
  ON target_data.cm_spc2classrule2panel_refyearset
  FOR EACH ROW
  EXECUTE PROCEDURE target_data.fn_trg_check_cm_spc2classrule2panel_refyearset();
COMMENT ON TRIGGER trg_cm_spc2classrule2panel_refyearset_before_update ON target_data.cm_spc2classrule2panel_refyearset IS
'This trigger controls that after update record in cm_spc2classrule2panel_refyearset table will remain at least one record in cm_spc2classrule2panel_refyearset table for given classification rule.';



-------------------
-- UKEYs
-------------------

-- ALTER TABLE target_data.cm_adc2classrule2panel_refyearset DROP CONSTRAINT ukey__cm_adc2classrule2panel_refyearset;

ALTER TABLE target_data.cm_adc2classrule2panel_refyearset
  ADD CONSTRAINT ukey__cm_adc2classrule2panel_refyearset UNIQUE(adc2classification_rule, refyearset2panel);
COMMENT ON CONSTRAINT ukey__cm_adc2classrule2panel_refyearset ON target_data.cm_adc2classrule2panel_refyearset IS
'A unique key that prevents repeated combinations for refyearset2panel within one classification rule.';



-- ALTER TABLE target_data.cm_spc2classrule2panel_refyearset DROP CONSTRAINT ukey__cm_spc2classrule2panel_refyearset;

ALTER TABLE target_data.cm_spc2classrule2panel_refyearset
  ADD CONSTRAINT ukey__cm_spc2classrule2panel_refyearset UNIQUE(spc2classification_rule, refyearset2panel);
COMMENT ON CONSTRAINT ukey__cm_spc2classrule2panel_refyearset ON target_data.cm_spc2classrule2panel_refyearset IS
'A unique key that prevents repeated combinations for refyearset2panel within one classification rule.';