--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-------------------
-- Functions
-------------------

-- <function name="fn_delete_pyrgroup" schema="target_data" src="functions/fn_delete_pyrgroup.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_pyrgroup
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_pyrgroup(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_pyrgroup(_id integer)
RETURNS void
AS
$$
BEGIN
	
	DELETE FROM target_data.t_panel_refyearset_group 
	WHERE panel_refyearset_group  = _id;
	
	DELETE FROM target_data.c_panel_refyearset_group  
	WHERE id = _id;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_pyrgroup(integer) IS
'Function deletes records from c_panel_refyearset_group and t_panel_refyearset_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_pyrgroup(integer) TO public;
-- </function>

-- <function name="fn_get_panel_refyearset_group" schema="target_data" src="functions/fn_get_panel_refyearset_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_panel_refyearset_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_panel_refyearset_group(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_panel_refyearset_group(_id integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_panel_refyearset_group AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_panel_refyearset_group AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given group of panels and refyearsets does not exist in table c_panel_refyearset_group (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_panel_refyearset_group AS t1
		WHERE t1.id = _id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_panel_refyearset_group(integer) IS
'Function returns records from c_panel_refyearset_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_panel_refyearset_group(integer) TO public;

-- </function>

-- <function name="fn_try_pyr_group_refyearset2panel_mapping" schema="target_data" src="functions/fn_try_pyr_group_refyearset2panel_mapping.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_pyr_group_refyearset2panel_mapping
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_try_pyr_group_refyearset2panel_mapping(integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_pyr_group_refyearset2panel_mapping(
	_refyearset2panels integer[])
RETURNS bool
AS
$$
DECLARE 
	_check_result boolean;

BEGIN	

	IF _refyearset2panels IS NULL
	THEN RAISE EXCEPTION 'Parameter _refyearset2panels must be provided.';
	END IF;	

SELECT EXISTS(
	SELECT 
		array_agg(refyearset2panel) AS refyearset2panel 
	FROM 
	target_data.t_panel_refyearset_group 
	GROUP BY panel_refyearset_group HAVING array_agg(refyearset2panel) <@ _refyearset2panels AND array_agg(refyearset2panel) @> _refyearset2panels) INTO _check_result;

RETURN _check_result; 

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_pyr_group_refyearset2panel_mapping(integer[]) IS
'Function checks if a combination of panels and reference year sets already exists in t_panel_refyearset_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_pyr_group_refyearset2panel_mapping(integer[]) TO public;
-- </function>

-- <function name="fn_update_pyrgroup" schema="target_data" src="functions/fn_update_pyrgroup.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_update_pyrgroup
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_update_pyrgroup(character varying, text, character varying, text, integer);

CREATE OR REPLACE FUNCTION target_data.fn_update_pyrgroup(
	_label character varying, 
	_description text, 
	_label_en character varying, 
	_description_en text, 
	_id integer)
RETURNS void
AS
$$
BEGIN

	IF _label IS NULL THEN 
		RAISE EXCEPTION 'Parameter label must be given.';
	END IF;

	IF _description IS NULL THEN 
		RAISE EXCEPTION 'Parameter description must be given.';
	END IF;

	IF _label_en IS NULL THEN 
		RAISE EXCEPTION 'Parameter label_en must be given.';
	END IF;

	IF _description_en IS NULL THEN 
		RAISE EXCEPTION 'Parameter description_en must be given.';
	END IF;

	UPDATE target_data.c_panel_refyearset_group 
	SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
	WHERE c_panel_refyearset_group.id = _id;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_update_pyrgroup(character varying, text, character varying, text, integer) IS
'Function provides update in c_panel_refyearset_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_update_pyrgroup(character varying, text, character varying, text, integer) TO public;

-- </function>

