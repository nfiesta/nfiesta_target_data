--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-----------------------------------------------
-- DDL
-----------------------------------------------
UPDATE target_data.c_ldsity_object_type SET description = 'Contribution to local density which will be aggregated and is the input into the estimate of total.' WHERE id = 100;
UPDATE target_data.c_ldsity_object_type SET description = 'This contribution to local density serves only to devide another contribution to local density by some sub population or area domain, hence it will never enter the esimate of total.' WHERE id = 200;


----------------------------------------------
-- Functions
-----------------------------------------------

DROP FUNCTION IF EXISTS target_data.fn_get_indicator(integer) CASCADE;

-- <function name="fn_get_indicator" schema="target_data" src="functions/fn_get_indicator.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_indicator
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_indicator(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_indicator(_topic integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
topic		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text,
state_or_change	integer,
soc_label	character varying(200),
soc_description	text
)
AS
$$
BEGIN
	IF _topic IS NULL
	THEN
		RETURN QUERY
		SELECT DISTINCT t1.id, t3.topic, t1.label, t1.description, t1.label_en, t1.description_en, t1.state_or_change, t4.label, t4.description
		FROM target_data.c_indicator AS t1
		INNER JOIN target_data.c_target_variable AS t2
		ON t1.id = t2.indicator
		INNER JOIN target_data.cm_target_variable2topic AS t3
		ON t2.id = t3.target_variable
		INNER JOIN target_data.c_state_or_change AS t4
		ON t1.state_or_change = t4.id;
	ELSE
		RETURN QUERY
		SELECT DISTINCT t1.id, t3.topic, t1.label, t1.description, t1.label_en, t1.description_en, t1.state_or_change, t4.label, t4.description
		FROM target_data.c_indicator AS t1
		INNER JOIN target_data.c_target_variable AS t2
		ON t1.id = t2.indicator
		INNER JOIN target_data.cm_target_variable2topic AS t3
		ON t2.id = t3.target_variable
		INNER JOIN target_data.c_state_or_change AS t4
		ON t1.state_or_change = t4.id
		WHERE t3.topic = _topic;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_indicator(integer) IS
'Function returns records from c_indicator table, optionally for given topic.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_indicator(integer) TO public;

-- </function>

-- <function name="fn_get_state_or_change" schema="target_data" src="functions/fn_get_state_or_change.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_state_or_change
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_state_or_change(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_state_or_change()
RETURNS TABLE (
id		integer,
label		character varying(200),
description	text
)
AS
$$
BEGIN
	RETURN QUERY
	SELECT t1.id, t1.label, t1.description
	FROM target_data.c_state_or_change AS t1;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_state_or_change() IS
'Function returns records from c_state_or_change table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_state_or_change() TO public;

-- </function>

-- <function name="fn_get_areal_or_population" schema="target_data" src="functions/fn_get_areal_or_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_areal_or_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_areal_or_population(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_areal_or_population()
RETURNS TABLE (
id		integer,
label		character varying(200),
description	text
)
AS
$$
BEGIN
	RETURN QUERY
	SELECT t1.id, t1.label, t1.description
	FROM target_data.c_areal_or_population AS t1;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_areal_or_population() IS
'Function returns records from c_areal_or_population table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_areal_or_population() TO public;

-- </function>

-- <function name="fn_get_ldsity_object_type" schema="target_data" src="functions/fn_get_ldsity_object_type.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity_object_type
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object_type(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object_type()
RETURNS TABLE (
id		integer,
label		character varying(200),
description	text
)
AS
$$
BEGIN
	RETURN QUERY
	SELECT t1.id, t1.label, t1.description
	FROM target_data.c_ldsity_object_type AS t1;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object_type() IS
'Function returns records from c_ldsity_object_type table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object_type() TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_get_ldsity(integer) CASCADE;

-- <function name="fn_get_ldsity" schema="target_data" src="functions/fn_get_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity(_target_variable integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
target_variable		integer,
label			character varying(200),
label_en		character varying(200),
column_expression	text,
unit_of_measure		integer,
ldsity_object		integer,
ldsity_object_type	integer,
object_type_label	character varying(200),
object_type_desc	text
)
AS
$$
BEGIN
	IF _target_variable IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS target_variable,
			t1.label, t1.label_en, t1.column_expression, t1.ldsity_object, t1.unit_of_measure,
			NULL::int, NULL::varchar(200), NULL::text
		FROM target_data.c_ldsity AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_target_variable (%)', _target_variable;
		END IF;

		RETURN QUERY
		SELECT t4.id, t1.id AS target_variable,
			t3.label, t3.label_en, t3.column_expression, t3.ldsity_object, t3.unit_of_measure, 
			t2.ldsity_object_type, t4.label, t4.description
		FROM target_data.c_target_variable AS t1
		INNER JOIN target_data.cm_ldsity2target_variable AS t2
		ON t1.id = t2.target_variable
		INNER JOIN target_data.c_ldsity AS t3
		ON t2.ldsity = t3.id
		INNER JOIN target_data.c_ldsity_object_type AS t4
		ON t2.ldsity_object_type = t4.id
		WHERE t1.id = _target_variable;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity(integer) IS
'Function returns records from c_ldsity table, optionally for given target_variable.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity(integer) TO public;

-- </function>


