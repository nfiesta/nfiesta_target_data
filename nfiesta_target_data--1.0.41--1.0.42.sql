--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-------------------
-- Functions
-------------------

-- <function name="fn_get_ldsity_object4ld_object_adsp" schema="target_data" src="functions/fn_get_ldsity_object4ld_object_adsp.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object4ld_object_adsp
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(_ldsity_object integer, _areal_or_population integer, _id integer)
RETURNS TABLE (
id			integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
classification_rule	boolean
)
AS
$$
BEGIN
	IF _ldsity_object IS NULL
	THEN
		RAISE EXCEPTION 'Parameter _ldsity_object is mandatory and must not be null!';
	END IF;

	IF _ldsity_object IS NULL
	THEN
		RAISE EXCEPTION 'Parameter _ldsity_object is mandatory and must not be null!';
	END IF;

	CASE
	WHEN _areal_or_population = 100
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
		THEN
			RAISE EXCEPTION 'Given area domain (%) does not exist in table c_area_domain!', _id;
		END IF;

		RETURN QUERY
		WITH w AS (
			SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
		)
		SELECT 	t3.id, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, 
			CASE WHEN t4.ldsity_object IS NOT NULL THEN true ELSE false END AS rule
		FROM w AS t1,
			unnest(t1.ldsity_objects) AS t2(ldsity_object)
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		LEFT JOIN 
			(SELECT DISTINCT t1.area_domain, t2.ldsity_object
			FROM target_data.c_area_domain_category AS t1
			INNER JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.id = t2.area_domain_category
			WHERE t1.area_domain = _id) AS t4
		ON t3.id = t4.ldsity_object
		WHERE t3.areal_or_population = 100;

	WHEN _areal_or_population = 200
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _id)
		THEN
			RAISE EXCEPTION 'Given population (%) does not exist in table c_sub_population!', _id;
		END IF;

		RETURN QUERY
		WITH w AS (
			SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
		)
		SELECT 	t3.id, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, 
			CASE WHEN t4.ldsity_object IS NOT NULL THEN true ELSE false END AS rule
		FROM w AS t1,
			unnest(t1.ldsity_objects) AS t2(ldsity_object)
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		LEFT JOIN 
			(SELECT DISTINCT t1.sub_population, t2.ldsity_object
			FROM target_data.c_sub_population_category AS t1
			INNER JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.sub_population = _id) AS t4
		ON t3.id = t4.ldsity_object
		WHERE t3.areal_or_population = 200;
	ELSE
		RAISE EXCEPTION 'Not known value of areal or population (%)!', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) IS
'Function returns records from c_ldsity_object table for given area domain or population.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) TO public;

-- </function>


