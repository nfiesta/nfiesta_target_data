--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-------------------
-- Functions
-------------------

-- <function name="fn_get_panel_refyearset_group" schema="target_data" src="functions/fn_get_panel_refyearset_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- ALTER TABLE target_data.cm_ldsity2panel_refyearset_version DROP CONSTRAINT ukey__cm_ldsity2panel_refyearset_version;

ALTER TABLE target_data.cm_ldsity2panel_refyearset_version
  ADD CONSTRAINT ukey__cm_ldsity2panel_refyearset_version UNIQUE(ldsity, refyearset2panel, version);
COMMENT ON CONSTRAINT ukey__cm_ldsity2panel_refyearset_version ON target_data.cm_ldsity2panel_refyearset_version IS
'A unique key that prevents repeated combinations for refyearset2panel within one ldsity and version.';