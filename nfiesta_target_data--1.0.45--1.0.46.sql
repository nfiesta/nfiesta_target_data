--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-------------------
-- Functions
-------------------

DROP FUNCTION IF EXISTS target_data.fn_save_categorization_setup(integer, integer[], integer[], integer[][], integer[], integer[][]) CASCADE;

-- <function name="fn_save_categorization_setup" schema="target_data" src="functions/fn_save_categorization_setup.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categorization_setup
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_categorization_setup(integer, integer[], integer[], integer[][], integer[], integer[][], boolean) CASCADE;

create or replace function target_data.fn_save_categorization_setup
(
	_target_variable		integer,
	_target_variable_cm		integer[],
	_area_domain			integer[],
	_area_domain_object		integer[][],
	_sub_population			integer[],
	_sub_population_object	integer[][],
	_save					boolean default true
)
returns table(categorization_setup integer)
as
$$
declare
		_array_id							integer[];
		_array_id4check						integer[];
		_lot_100							integer;
		_lot_200							integer;
		_variant							integer;	
		_area_domain4input					integer[];
		_area_domain_object4input			integer[];
		_sub_population4input				integer[];
		_sub_population_object4input		integer[];	
		_area_domain4input_text				text;
		_area_domain_object4input_text		text;
		_sub_population4input_text			text;
		_sub_population_object4input_text	text;
		_string4attr						text;
		_query_res							text;
		_s4ij_p1							text;
		_s4ij_p2							text;
		_s4ij_p3							text;
		_s4ij_p4							text;
		_s4ij_p5							text;
		_string4incomming					text;
		_string4inner_join					text;
		_array_id_200						integer[];
		_string_inner_join					text;
		_string_incomming_0					text;
		_string_incomming					text;
		_string_check_1						text;
		_string_check_2						text;
		_string_check_3						text;
		_string_check_4						text;
		_string_insert_1					text;
		_string_insert_2					text;
		_s4da_p1							text;
		_s4da_p2							text;
		_s4da_p3							text;
		_s4da_p4							text;
		_s4ia_p1							text;
		_s4ia_p2							text;
		_s4ia_p3							text;
		_s4ia_p4							text;
		_s4result_p1						text;
		_s4result_p2						text;
		_string4database_agg				text;
		_string4incomming_agg				text;	
		_string4result						text;
		_string_agg_and_result				text;
		_string_result_save_false			text;
		_res								text;
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_save_categorization_setup: The input argument _target_variable must not be NULL !';
		end if;

		if _target_variable_cm is null
		then
			raise exception 'Error 02: fn_save_categorization_setup: The input argument _target_variable_cm must not be NULL !';
		end if;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where target_variable = _target_variable
		into
				_array_id;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where id in (select unnest(_target_variable_cm))
		into
				_array_id4check;			

		if _array_id != _array_id4check
			then raise exception 'Error 03: fn_save_categorization_setup: The internal argument _array_id = % and internal argument _array_id4check = % are not the same!',_array_id, _array_id4check;
		end if;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 04: fn_save_categorization_setup: This variant is not implemented yet!';
		end if;

		for bc in 1..array_length(_array_id,1)
		loop	
			-----------------------------------------
			-----------------------------------------
			if _area_domain is null and _sub_population is null
			then
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is null
			then
				if _variant in (3,4)
				then
					with
					w1 as (select unnest(_area_domain_object[1:1]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;				
				else		
					with
					w1 as (select unnest(_area_domain_object[bc:bc]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				end if;			
			
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;
			
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			------
			if _area_domain is null and _sub_population is not null
			then		
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
			
				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is not null
			then
				if _variant in (3,4)
				then
					with
					w1 as (select unnest(_area_domain_object[1:1]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				else
					with
					w1 as (select unnest(_area_domain_object[bc:bc]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				end if;				
			
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
	
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;			

				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			end if;
			-----------------------------------------
			-----------------------------------------
			
			if _area_domain4input is null
			then
				_area_domain4input_text := 'null::integer[]';
			else
				_area_domain4input_text := replace(replace(concat('array[',_area_domain4input,']'),'{',''),'}','');
			end if;
		
			if _area_domain_object4input is null
			then
				_area_domain_object4input_text := 'null::integer[]';
			else
				_area_domain_object4input_text := replace(replace(concat('array[',_area_domain_object4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population4input is null
			then
				_sub_population4input_text := 'null::integer[]';
			else
				_sub_population4input_text := replace(replace(concat('array[',_sub_population4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population_object4input is null
			then
				_sub_population_object4input_text := 'null::integer[]';
			else
				_sub_population_object4input_text := replace(replace(concat('array[',_sub_population_object4input,']'),'{',''),'}','');
			end if;
			
			_string4attr := concat('
			select
					area_domain_category,
					sub_population_category,
					adc2classification_rule,
					spc2classification_rule
			from
					target_data.fn_get_classification_rules_id4categories(',_area_domain4input_text,',',_area_domain_object4input_text,',',_sub_population4input_text,',',_sub_population_object4input_text,')
			');
		
			if bc = 1
			then
				_query_res := concat('with w_',bc,' as (',_string4attr,')');
			else
				_query_res := concat(_query_res,',w_',bc,' as (',_string4attr,')');
			end if;
		end loop;
		
		-------------------------------------------------------------
		-------------------------------------------------------------
		if _variant in (1,2)
		then
		
			_s4ij_p1 := 'select row_number() over() as group_id_poradi';
			_s4ij_p3 := ' from ';
		
			for bc in 1..array_length(_array_id,1)
			loop
				if bc = 1
				then
			
					_s4ij_p2 := concat('
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.area_domain_category
										,t',bc,'.sub_population_category
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'');
				
					_string4incomming := concat(
						'
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'
										on
											t1.area_domain_category = t',bc,'.area_domain_category and
											t1.sub_population_category = t',bc,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				end if;
			end loop;
			
				_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4;
		end if;		
		-------------------------------------------------------------
		if _variant in (3,4)
		then	
			_s4ij_p1 := concat(
				'
				select
					row_number() over() as group_id_poradi,
					a.*,
					b.*
				from
					(
					select
							',_array_id[1],' as ldsity2target_variable_',_array_id[1],',
							area_domain_category as area_domain_category_',_array_id[1],',
							adc2classification_rule as adc2classification_rule_',_array_id[1],',
							spc2classification_rule as spc2classification_rule_',_array_id[1],'
					from w_1
					) as a
				inner
				join
					(select ');
				
			_s4ij_p3 := ' from ';
		
			_s4ij_p5 := concat('
						) as b
					on
						a.area_domain_category_',_array_id[1],' = b.area_domain_category');
					
			_string4incomming := concat(
				'
				select
						1 as group_id,
						group_id_poradi,
						area_domain_category,
						sub_population_category,
						ldsity2target_variable_',_array_id[1],' as ldsity2target_variable,
						adc2classification_rule_',_array_id[1],' as adc2classification_rule,
						spc2classification_rule_',_array_id[1],' as spc2classification_rule
				from
						w_inner_join
				');					
				
			_array_id_200 := array_remove(_array_id, _array_id[1]);
			
			for bc_200 in 1..array_length(_array_id_200,1)
			loop
				if bc_200 = 1
				then				
					_s4ij_p2 := concat('
										t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.area_domain_category
										,t',bc_200,'.sub_population_category
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'');
				
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');				
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'
										on
											t1.area_domain_category = t',bc_200,'.area_domain_category and
											t1.sub_population_category = t',bc_200,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');
					
				end if;
			end loop;
			
			_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4 || _s4ij_p5;
		
		end if;
		-------------------------------------------------------------
		-------------------------------------------------------------
		_string_inner_join := concat(',w_inner_join as (',_string4inner_join,')');
		_string_incomming_0 := concat(',w_incomming_0 as (',_string4incomming,')');
		-------------------------------------------------------------
		if _variant in (3,4)
		then
			_string_incomming := concat(
				'
				,w_incomming as	(
								select
										group_id,
										group_id_poradi,
										area_domain_category,
										sub_population_category,
										ldsity2target_variable,
										adc2classification_rule,
										case when ldsity2target_variable in (',array_to_string(_array_id_200,','),') then null::integer[] else adc2classification_rule end as adc2classification_rule4insert,
										spc2classification_rule
								from
									w_incomming_0
								)
				');


			_string_check_1 := concat(
				'
				,w_check_1 as		(
									select
											a.*,
											b.id_cm_setup,
											b.categorization_setup		
									from
											w_incomming as a
									left
									join	(
											select
													t1.*,
													t2.area_domain_category
											from
												(
												select
														id as id_cm_setup,
														ldsity2target_variable,
														adc2classification_rule,
														spc2classification_rule,
														categorization_setup
												from
														target_data.cm_ldsity2target2categorization_setup
												where
														ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
												) as t1
												inner join
												(
												select
														categorization_setup,
														case when adc2classification_rule is null then array[0] else (select id_category from target_data.fn_get_category4classifiaction_rule_id(''adc''::varchar,adc2classification_rule)) end as area_domain_category
												from
														target_data.cm_ldsity2target2categorization_setup
												where
														ldsity2target_variable = ',_array_id[1],'
												) as t2
											on t1.categorization_setup = t2.categorization_setup
											) as b
									on
											a.ldsity2target_variable = b.ldsity2target_variable and			
											((case when a.adc2classification_rule4insert is null then array[0] else a.adc2classification_rule4insert end)
											= (case when b.adc2classification_rule is null then array[0] else b.adc2classification_rule end)) and
											((case when a.spc2classification_rule is null then array[0] else a.spc2classification_rule end)
											= (case when b.spc2classification_rule is null then array[0] else b.spc2classification_rule end))

											and a.area_domain_category = b.area_domain_category				
									)
			');

			_string_check_3 := concat(
				'
				,w_check_3 as		(
									select * from w_check_2 union all
									select distinct c.group_id, c.group_id_poradi, c.area_domain_category,
									c.sub_population_category, c.ldsity2target_variable,
									c.adc2classification_rule, c.spc2classification_rule,
									c.adc2classification_rule4insert,
									c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
									from
											(
											select
													1 as group_id,
													group_id_poradi,
													area_domain_category,
													sub_population_category,
													',_array_id[1],' as ldsity2target_variable,
													adc2classification_rule,
													null::integer[] as spc2classification_rule,
													adc2classification_rule4insert,
													null::integer as id_cm_setup,
													null::integer as categorization_setup,
													check_exists_group_id_1
											from
													w_check_2 where check_exists_group_id_1 = 1
											) as c
									)				
			');

			_string_insert_2 :=
			'
			,w_insert_2 as		(
								insert into target_data.cm_ldsity2target2categorization_setup
								(ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup)
								select ldsity2target_variable,adc2classification_rule4insert,spc2classification_rule,new_categorization_setup
								from w_check_4
								returning id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
								)			
			';
		else
			_string_incomming := concat(
				'
				,w_incomming as	(
								select
										group_id,
										group_id_poradi,
										area_domain_category,
										sub_population_category,
										ldsity2target_variable,
										adc2classification_rule,
										spc2classification_rule
								from
									w_incomming_0
								)
				');

			_string_check_1 := concat(
				'
				,w_check_1 as		(
									select
											a.*,
											b.id_cm_setup,
											b.categorization_setup		
									from
											w_incomming as a
									left
									join	(
											select
													id as id_cm_setup,
													ldsity2target_variable,
													adc2classification_rule,
													spc2classification_rule,
													categorization_setup
											from
													target_data.cm_ldsity2target2categorization_setup
											where
												ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
											) as b
									on
											a.ldsity2target_variable = b.ldsity2target_variable and			
											((case when a.adc2classification_rule is null then array[0] else a.adc2classification_rule end)
											= (case when b.adc2classification_rule is null then array[0] else b.adc2classification_rule end)) and
											((case when a.spc2classification_rule is null then array[0] else a.spc2classification_rule end)
											= (case when b.spc2classification_rule is null then array[0] else b.spc2classification_rule end))				
									)
				'
			);

			_string_check_3 := concat(
				'
				,w_check_3 as		(
									select * from w_check_2 union all
									select distinct c.group_id, c.group_id_poradi, c.area_domain_category,
									c.sub_population_category, c.ldsity2target_variable,
									c.adc2classification_rule, c.spc2classification_rule,
									c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
									from
											(
											select
													1 as group_id,
													group_id_poradi,
													area_domain_category,
													sub_population_category,
													',_array_id[1],' as ldsity2target_variable,
													adc2classification_rule,
													null::integer[] as spc2classification_rule,
													null::integer as id_cm_setup,
													null::integer as categorization_setup,
													check_exists_group_id_1
											from
													w_check_2 where check_exists_group_id_1 = 1
											) as c
									)				
			');

			_string_insert_2 :=
			'
			,w_insert_2 as		(
								insert into target_data.cm_ldsity2target2categorization_setup
								(ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup)
								select ldsity2target_variable,adc2classification_rule,spc2classification_rule,new_categorization_setup
								from w_check_4
								returning id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
								)			
			';			
		end if;
		-------------------------------------------------------------
		
		-------------------------------------------------------------
		-------------------------------------------------------------
		_s4da_p1 := 'select t0.categorization_setup';
		_s4da_p3 := ' from (select distinct categorization_setup from w_database) as t0 ';

		_s4ia_p1 := 'select t0.group_id_poradi';
		_s4ia_p3 := ' from (select distinct group_id_poradi from w_incomming) as t0 ';
	
		_s4result_p1 :=	'
						select
								a.*,
								b.categorization_setup
						from
								w_incomming_agg as a
						inner
						join	w_database_agg as b
						
						on ';	
	

		for i in 1..array_length(_array_id,1)
		loop
			if i = 1
			then
				_s4da_p2 := concat('
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');

				if _variant in (3,4)
				then				
					_s4ia_p2 := concat('
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule4insert as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');
				else
					_s4ia_p2 := concat('
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');				
				end if;								
								
				_s4da_p4 := concat(' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat('
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],' and
									((case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end)
									= (case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)) and
									((case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end)
									= (case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end))
								');
			else
				_s4da_p2 := concat(_s4da_p2,
									'
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');
								
				if _variant in (3,4)
				then				
					_s4ia_p2 := concat(_s4ia_p2,
										'
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule4insert as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');
				else
					_s4ia_p2 := concat(_s4ia_p2,
										'
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');				
				end if;								
								
				_s4da_p4 := concat(_s4da_p4,
									' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(_s4ia_p4,
									' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat(_s4result_p2,' and 
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],' and
									((case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end)
									= (case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)) and
									((case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end)
									= (case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end))
								');
			end if;
		end loop;
	
		_string4database_agg := _s4da_p1 || _s4da_p2 || _s4da_p3 || _s4da_p4;
		_string4incomming_agg := _s4ia_p1 || _s4ia_p2 || _s4ia_p3 || _s4ia_p4;
		_string4result := _s4result_p1 || _s4result_p2;
		-------------------------------------------------------------
		-------------------------------------------------------------

		_string_check_2 := 
		'
		,w_check_2 as		(
							select
									w_check_1.*,
									case
										when categorization_setup is null
											then
												case
													when	(
															select min(t.group_id) from w_check_1 as t
															where t.categorization_setup is null
															and t.group_id_poradi = w_check_1.group_id_poradi
															)
															= 1
													then 0
													else 1
												end 
											else 0
									end as check_exists_group_id_1
							from
									w_check_1
							)		
		';


		_string_check_4 :=
		'
		,w_check_4 as		(					
							select
									(select coalesce(max(id),0) from target_data.t_categorization_setup) + 
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as new_categorization_setup,
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as pozice,
									w_check_3.*
							from
									w_check_3
							where
									categorization_setup is null
							order
									by new_categorization_setup, pozice, group_id
							)		
		';

		_string_insert_1 :=
		'
		,w_insert_1 as		(
							insert into target_data.t_categorization_setup(id)
							select distinct new_categorization_setup from w_check_4 order by new_categorization_setup
							returning id
							)		
		';		
		
		_string_agg_and_result := concat(
		'
		,w_database as		(
							select id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
							from target_data.cm_ldsity2target2categorization_setup where ldsity2target_variable in
							(select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
							union all
							select id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
							from w_insert_2
							)
		,w_database_agg as	('||_string4database_agg||')
		,w_incomming_agg as	('||_string4incomming_agg||')
		,w_result as		('||_string4result||')
		select
			categorization_setup
		from
			w_result order by categorization_setup;
		');

		_string_result_save_false := 'select distinct new_categorization_setup from w_check_4 order by new_categorization_setup;';	
		
		if _save = true
		then
			_res := _query_res || _string_inner_join || _string_incomming_0 || _string_incomming || _string_check_1 || _string_check_2 || _string_check_3 || _string_check_4 || _string_insert_1 || _string_insert_2 || _string_agg_and_result;
		else
			_res := _query_res || _string_inner_join || _string_incomming_0 || _string_incomming || _string_check_1 || _string_check_2 || _string_check_3 || _string_check_4 || _string_result_save_false;
		end if;
		
		return query execute ''||_res||'';
end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_save_categorization_setup(integer,integer[],integer[],integer[][],integer[],integer[][],boolean) IS
'The function sets attribute configurations.';

grant execute on function target_data.fn_save_categorization_setup(integer,integer[],integer[],integer[][],integer[],integer[][],boolean) to public;
-- </function>

