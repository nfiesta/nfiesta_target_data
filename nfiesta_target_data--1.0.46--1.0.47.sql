--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-------------------
-- Functions
-------------------

DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT []);

-- <function name="fn_get_eligible_panel_refyearset_combinations" schema="target_data" src="functions/fn_get_eligible_panel_refyearset_combinations.sql">
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_eligible_panel_refyearset_combinations 
--------------------------------------------------------------------------------
-- ALTER EXTENSION nfiesta_target_data DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT []);
-- DROP FUNCTION IF EXISTS target_data.fn_get_eligible_panel_refyearset_combinations(INT []);

CREATE OR REPLACE FUNCTION 
target_data.fn_get_eligible_panel_refyearset_combinations(IN _categorization_setup INT[])
RETURNS TABLE (
	refyearset2panel_mapping INT, 
	panel_label VARCHAR(20), 
	panel_description VARCHAR(120), 
	refyearset_label VARCHAR(20), 
	refyearset_description VARCHAR(120))  AS 
$$
DECLARE 
	_refyearset2panel4ldsity INT[]; -- set of panel and refrerence yearset combinations for which local densities are available
	_refyearset2panel4ldsity_spc INT[]; -- set of panel and reference yearset combinations for which values local densities are ETL-constrained to the specified subpopulations
	_refyearset2panel4ldsity_adc INT[]; -- set of panel and reference yearset combinations for which values local densities are ETL-constrained to the specified areal domain  
	_refyearset2panel4ldsity2target_variable_spc INT[]; -- set of panel and reference yearset combinations for which values local densities were constrained to the specified subpopulations by the user   
	_refyearset2panel4ldsity2target_variable_adc INT[]; -- set of panel and reference yearset combinations for which values local densities were constrained to the specified area domains by the user
	_refyearset2panel4spc INT[]; -- set of panel and reference yearset combinations for which values local densities can be classified using a particular set of subpopulation categories
	_refyearset2panel4adc INT[]; -- set of panel and reference yearset combinations for which values local densities can be classified using a particular set of area domain categories 
	_refyearset2panel_common INT[]; -- set of panel and reference yearset combinations for which values local densities for which local densitities of a target variable can be calculated
	_rules_present bool; -- logical value coding whether there are some rules defined    
BEGIN
	
	-- raising an exception on NULL input
	IF _categorization_setup IS NULL then 
		RAISE EXCEPTION 'Input parameter _categorization_setup INT[] is required!';
	END IF; 
	
	-- assigning _refyearset2panel4ldsity
	WITH w_ldsity AS (
		SELECT DISTINCT 
			t4.id AS ldsity, 
			t3."version" 
		FROM 
			target_data.t_categorization_setup  AS t1
		INNER JOIN 
			target_data.cm_ldsity2target2categorization_setup AS t2
			ON  
			t1.id = ANY(_categorization_setup) AND -- vsechna 
			t1.id = t2.categorization_setup -- dva zaznamy, dva prispevky (jeden kmenu hroubi, jeden kmenu nehroubi), kazdy s vlastni sadou pravidel 
		INNER JOIN 
		 	target_data.cm_ldsity2target_variable AS t3
			ON
		 	t2.ldsity2target_variable = t3.id
		INNER JOIN 
			target_data.c_ldsity AS t4
			ON	
		 	t3.ldsity = t4.id
	),
	w_refyearset2panel AS (
		SELECT 
			t2.refyearset2panel 
		FROM 
			w_ldsity  AS t1
		INNER JOIN 
			target_data.cm_ldsity2panel_refyearset_version AS t2
			ON 
			t2.ldsity = t1.ldsity AND 
			t2."version" = t2."version"
		GROUP BY t2.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity)
	)
	SELECT array_agg(refyearset2panel) FROM w_refyearset2panel
	INTO _refyearset2panel4ldsity;

	-- assigning _refyearset2panel4ldsity_spc
	WITH w_ldsity_spc_rules AS (
		SELECT DISTINCT
			 unnest(t4.sub_population_category) AS spc2classification_rule 
		FROM 
			target_data.t_categorization_setup  AS t1
		INNER JOIN 
			target_data.cm_ldsity2target2categorization_setup AS t2
			ON  
			t1.id = ANY(_categorization_setup) AND 
			t1.id = t2.categorization_setup  
		INNER JOIN 
		 	target_data.cm_ldsity2target_variable AS t3
			ON
		 	t2.ldsity2target_variable = t3.id
		INNER JOIN 
			target_data.c_ldsity AS t4
			ON	
		 	t3.ldsity = t4.id
	),
	w_refyearset2panel AS (
		SELECT 
			t3.refyearset2panel
		FROM
			w_ldsity_spc_rules AS t1	
		INNER JOIN
			target_data.cm_spc2classification_rule AS t2
			ON
			t1.spc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_spc2classrule2panel_refyearset AS t3 
			ON 
			t3.spc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity_spc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_ldsity_spc_rules) FROM w_refyearset2panel 
	INTO _refyearset2panel4ldsity_spc, _rules_present; 

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity_spc IS NULL AND _rules_present THEN 
	RETURN;
	END IF;

	-- assigning _refyearset2panel4ldsity_adc
	WITH w_ldsity_adc_rules AS (
		SELECT DISTINCT
			 unnest(t4.area_domain_category) AS adc2classification_rule 
		FROM 
			target_data.t_categorization_setup  AS t1
		INNER JOIN 
			target_data.cm_ldsity2target2categorization_setup AS t2
			ON  
			t1.id = ANY(_categorization_setup) AND
			t1.id = t2.categorization_setup 
		INNER JOIN 
		 	target_data.cm_ldsity2target_variable AS t3
			ON
		 	t2.ldsity2target_variable = t3.id
		INNER JOIN 
			target_data.c_ldsity AS t4
			ON	
		 	t3.ldsity = t4.id
	),
	w_refyearset2panel AS (
		SELECT 
			t3.refyearset2panel
		FROM
			w_ldsity_adc_rules AS t1	
		INNER JOIN
			target_data.cm_adc2classification_rule AS t2
			ON
			t1.adc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_adc2classrule2panel_refyearset AS t3 
			ON 
			t3.adc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity_adc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_ldsity_adc_rules) FROM w_refyearset2panel 
	INTO _refyearset2panel4ldsity_adc, _rules_present;  

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity_adc IS NULL AND _rules_present THEN 
	RETURN;
	END IF;

	-- assigning _refyearset2panel4ldsity2target_variable_spc
	WITH w_ldsity2target_variable_spc_rules AS (
		SELECT DISTINCT
			unnest(t3.sub_population_category) AS spc2classification_rule 
		FROM 
			target_data.t_categorization_setup  AS t1
		INNER JOIN 
			target_data.cm_ldsity2target2categorization_setup AS t2
			ON  
			t1.id = ANY(_categorization_setup) AND 
			t1.id = t2.categorization_setup
		INNER JOIN 
		 	target_data.cm_ldsity2target_variable AS t3
			ON
		 	t2.ldsity2target_variable = t3.id
	),
	w_refyearset2panel AS (
		SELECT 
			t3.refyearset2panel
		FROM
			w_ldsity2target_variable_spc_rules AS t1	
		INNER JOIN
			target_data.cm_spc2classification_rule AS t2
			ON
			t1.spc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_spc2classrule2panel_refyearset AS t3 
			ON 
			t3.spc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity2target_variable_spc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >=1 FROM w_ldsity2target_variable_spc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4ldsity2target_variable_spc, _rules_present; 

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity2target_variable_spc IS NULL AND _rules_present THEN 
	RETURN;
	END IF;


	-- assigning _refyearset2panel4ldsity2target_variable_adc
	WITH w_ldsity2target_variable_adc_rules AS (
		SELECT DISTINCT
			unnest(t3.area_domain_category) AS adc2classification_rule 
		FROM 
			target_data.t_categorization_setup  AS t1
		INNER JOIN 
			target_data.cm_ldsity2target2categorization_setup AS t2
			ON  
			t1.id = ANY(_categorization_setup) AND 
			t1.id = t2.categorization_setup
		INNER JOIN 
		 	target_data.cm_ldsity2target_variable AS t3
			ON
		 	t2.ldsity2target_variable = t3.id
	),
	w_refyearset2panel AS (
		SELECT
			t3.refyearset2panel
		FROM
			w_ldsity2target_variable_adc_rules AS t1	
		INNER JOIN
			target_data.cm_adc2classification_rule AS t2
			ON
			t1.adc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_adc2classrule2panel_refyearset AS t3
			ON 
			t3.adc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity2target_variable_adc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_ldsity2target_variable_adc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4ldsity2target_variable_adc, _rules_present; 

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity2target_variable_adc IS NULL AND _rules_present THEN 
	RETURN;
	END IF;


	-- assigning _refyearset2panel4spc
	WITH w_spc_rules AS (
	SELECT  
		 unnest(t2.spc2classification_rule) AS spc_classification_rule 
	FROM 
		target_data.t_categorization_setup  AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.id = ANY(_categorization_setup)  AND 
		t1.id = t2.categorization_setup 
	),
	w_refyearset2panel AS (
		SELECT 
			refyearset2panel
		FROM 
			w_spc_rules AS t1
		INNER JOIN 
			target_data.cm_spc2classification_rule AS t2
			ON
			t1.spc_classification_rule = t2.id
		INNER JOIN 
			target_data.c_sub_population_category AS t3
			ON
			t2.sub_population_category = t3.id
		INNER JOIN 
			target_data.cm_spc2classrule2panel_refyearset AS t4
			ON 
			t4.spc2classification_rule = t2.id
		GROUP BY t4.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_spc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >=1 FROM w_spc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4spc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4spc IS NULL AND _rules_present THEN 
	RETURN;
	END IF;

	-- assigning _refyearset2panel4adc
	WITH w_adc_rules AS (
	SELECT  
		 unnest(t2.adc2classification_rule) AS adc_classification_rule 
	FROM 
		target_data.t_categorization_setup  AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.id = ANY(ARRAY[_categorization_setup])  AND 
		t1.id = t2.categorization_setup 
	),
	w_refyearset2panel AS (
		SELECT 
			refyearset2panel
		FROM 
			w_adc_rules AS t1
		INNER JOIN 
			target_data.cm_adc2classification_rule AS t2
			ON
			t1.adc_classification_rule = t2.id
		INNER JOIN 
			target_data.c_area_domain_category AS t3
			ON
			t2.area_domain_category = t3.id
		INNER JOIN 
			target_data.cm_adc2classrule2panel_refyearset AS t4
			ON 
			t4.adc2classification_rule = t2.id
		GROUP BY t4.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_adc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_adc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4adc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4adc IS NULL AND _rules_present THEN 
	RETURN;
	END IF;

	WITH w_refyearset2panel_intersection AS (
		SELECT unnest(_refyearset2panel4ldsity) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity_spc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity_adc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity2target_variable_spc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity2target_variable_adc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4spc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4adc,_refyearset2panel4ldsity)) AS refyearset2panel
	)
	SELECT array_agg(refyearset2panel) FROM  w_refyearset2panel_intersection INTO _refyearset2panel_common;
	
	RETURN QUERY
		WITH w_refyearset2panel_mapping AS (
			SELECT unnest(_refyearset2panel_common) AS refyearset2panel_mapping
		)
		SELECT 
			t1.refyearset2panel_mapping,
			t4.panel AS panel_label,
			t4."label" AS panel_description,
			t3.reference_year_set AS refyerset_label,
			t3."label" AS refyearset_description
		FROM
			w_refyearset2panel_mapping AS t1 
		INNER JOIN	sdesign.cm_refyearset2panel_mapping AS t2
			ON t1.refyearset2panel_mapping = t2.id
		INNER JOIN
			sdesign.t_reference_year_set AS t3
			ON t2.reference_year_set = t3.id
		INNER JOIN
			sdesign.t_panel AS t4
			ON t2.panel = t4.id;
END
$$ 
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER; 

COMMENT ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(int[]) IS 
'Function returns combinations of panel and reference-yearsets, for which local densities can be calculated.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(int[]) TO public;

/* -- tests

-- checking if the function returns some records  
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(ARRAY[55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105]);

-- checking if the function raises an exception for NULL input
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(NULL);
*/
 

/*
-- inspection query to select panel and reference yearset combinations for which the contributions to local density of a particular 
-- target variable (id = 3) can be uploaded from the source (analytical) database  
SELECT DISTINCT
	*
	-- array_agg(t3.categorization_setup) 
	-- t5.refyearset2panel
FROM 
	target_data.c_target_variable AS t1 
INNER JOIN 
	target_data.cm_ldsity2target_variable AS t2
	ON 
	t1.id = 3 AND -- number of merchantable and regeneration stems
	t2.target_variable = t1.id
INNER JOIN
	target_data.cm_ldsity2target2categorization_setup AS t3
	ON
	t3.ldsity2target_variable = t2.id
INNER JOIN
	target_data.c_ldsity AS t4
	ON 
	t2.ldsity = t4.id  -- porad 34 zaznamu
INNER JOIN 
	target_data.cm_ldsity2panel_refyearset_version AS t5
	ON 
	t5.ldsity = t4.id AND 
	t2."version" = t5."version";

-- inspection query to find out the values of t_categorization_setup which are linked to a particular target variable (id = 3)  
SELECT
	*
	-- array_agg(DISTINCT t3.categorization_setup) 
	-- t5.refyearset2panel
FROM 
	target_data.c_target_variable AS t1 
INNER JOIN 
	target_data.cm_ldsity2target_variable AS t2
	ON 
	t1.id = 3 AND -- number of merchantable and regeneration stems
	t2.target_variable = t1.id
INNER JOIN
	target_data.cm_ldsity2target2categorization_setup AS t3
	ON
	t3.ldsity2target_variable = t2.id
INNER JOIN
	target_data.c_ldsity AS t4
	ON 
	t2.ldsity = t4.id  -- porad 34 zaznamu
INNER JOIN 
	target_data.cm_ldsity2panel_refyearset_version AS t5
	ON 
	t5.ldsity = t4.id AND 
	t2."version" = t5."version";           

-- a query to find the set of panel and refrerence yearset combinations for which local densities are available	
WITH w_ldsity AS (
	SELECT DISTINCT 
		t4.id AS ldsity, 
		t3."version" 
	FROM 
		target_data.t_categorization_setup  AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.id = ANY(ARRAY[55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105]) AND 
		t1.id = t2.categorization_setup
	INNER JOIN 
	 	target_data.cm_ldsity2target_variable AS t3
		ON
	 	t2.ldsity2target_variable = t3.id
	INNER JOIN 
		target_data.c_ldsity AS t4
		ON	
	 	t3.ldsity = t4.id
),
w_refyearset2panel AS (
	SELECT 
		t2.refyearset2panel 
	FROM 
		w_ldsity  AS t1
	INNER JOIN 
		target_data.cm_ldsity2panel_refyearset_version AS t2
		ON 
		t2.ldsity = t1.ldsity AND 
		t2."version" = t2."version"
	GROUP BY t2.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity)
)
SELECT array_agg(refyearset2panel) AS refyearset2panel FROM w_refyearset2panel;

-- a query to find the set of panel and reference yearset combinations for which values local densities are ETL-constrained to the specified subpopulations
WITH w_ldsity_spc_rules AS (
	SELECT DISTINCT
		 unnest(t4.sub_population_category) AS spc2classification_rule 
	FROM 
		target_data.t_categorization_setup  AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.id = ANY(ARRAY[55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105]) AND 
		t1.id = t2.categorization_setup
	INNER JOIN 
	 	target_data.cm_ldsity2target_variable AS t3
		ON
	 	t2.ldsity2target_variable = t3.id
	INNER JOIN 
		target_data.c_ldsity AS t4
		ON	
	 	t3.ldsity = t4.id
),
w_refyearset2panel AS (
	SELECT 
		t3.refyearset2panel
	FROM
		w_ldsity_spc_rules AS t1	
	INNER JOIN
		target_data.cm_spc2classification_rule AS t2
		ON
		t1.spc2classification_rule = t2.id
	INNER JOIN 
		target_data.cm_spc2classrule2panel_refyearset AS t3 
		ON 
		t3.spc2classification_rule = t2.id 
	GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity_spc_rules)
)
SELECT array_agg(refyearset2panel) AS refyearset2panel FROM w_refyearset2panel; 

-- a query to find the set of panel and reference yearset combinations for which values local densities are ETL-constrained to the specified areal domain  
WITH w_ldsity_adc_rules AS (
	SELECT DISTINCT
		 unnest(t4.area_domain_category) AS adc2classification_rule 
	FROM 
		target_data.t_categorization_setup  AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.id = ANY(ARRAY[55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105]) AND
		t1.id = t2.categorization_setup
	INNER JOIN 
	 	target_data.cm_ldsity2target_variable AS t3
		ON
	 	t2.ldsity2target_variable = t3.id
	INNER JOIN 
		target_data.c_ldsity AS t4
		ON	
	 	t3.ldsity = t4.id
),
w_refyearset2panel AS (
	SELECT 
		t3.refyearset2panel
	FROM
		w_ldsity_adc_rules AS t1	
	INNER JOIN
		target_data.cm_adc2classification_rule AS t2
		ON
		t1.adc2classification_rule = t2.id
	INNER JOIN 
		target_data.cm_adc2classrule2panel_refyearset AS t3 
		ON 
		t3.adc2classification_rule = t2.id 
	GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity_adc_rules)
)
SELECT array_agg(refyearset2panel) AS refyearset2panel FROM w_refyearset2panel; 

-- a query to find the set of panel and reference yearset combinations for which values local densities were constrained to the specified subpopulations by the user 
WITH w_ldsity2target_variable_spc_rules AS (
	SELECT DISTINCT
		unnest(t3.sub_population_category) AS spc2classification_rule 
	FROM 
		target_data.t_categorization_setup  AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.id = ANY(ARRAY[55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105]) AND 
		t1.id = t2.categorization_setup
	INNER JOIN 
	 	target_data.cm_ldsity2target_variable AS t3
		ON
	 	t2.ldsity2target_variable = t3.id
),
w_refyearset2panel AS (
	SELECT 
		t3.refyearset2panel
	FROM
		w_ldsity2target_variable_spc_rules AS t1	
	INNER JOIN
		target_data.cm_spc2classification_rule AS t2
		ON
		t1.spc2classification_rule = t2.id
	INNER JOIN 
		target_data.cm_spc2classrule2panel_refyearset AS t3 
		ON 
		t3.spc2classification_rule = t2.id 
	GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity2target_variable_spc_rules)
)
SELECT array_agg(refyearset2panel) AS refyearset2panel FROM w_refyearset2panel; 

-- a query to find the set of panel and reference yearset combinations for which values local densities were constrained to the specified area domains by the user 
WITH w_ldsity2target_variable_adc_rules AS (
	SELECT DISTINCT
		unnest(t3.area_domain_category) AS adc2classification_rule 
	FROM 
		target_data.t_categorization_setup  AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.id = ANY(ARRAY[55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105]) AND
		t1.id = t2.categorization_setup
	INNER JOIN 
	 	target_data.cm_ldsity2target_variable AS t3
		ON
	 	t2.ldsity2target_variable = t3.id
),
w_refyearset2panel AS (
	SELECT
		t3.refyearset2panel
	FROM
		w_ldsity2target_variable_adc_rules AS t1	
	INNER JOIN
		target_data.cm_adc2classification_rule AS t2
		ON
		t1.adc2classification_rule = t2.id
	INNER JOIN 
		target_data.cm_adc2classrule2panel_refyearset AS t3
		ON 
		t3.adc2classification_rule = t2.id 
	GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity2target_variable_adc_rules)
)
SELECT array_agg(refyearset2panel) AS refyearset2panel FROM w_refyearset2panel; 

-- a query to find a set of panel and reference yearset combinations for which values local densities can be classified using a particular set of subpopulation categories
WITH w_spc_rules AS (
SELECT  
	 -- t1.id AS categorization_setup,
	 -- t2.spc2classification_rule 
	 unnest(t2.spc2classification_rule) AS spc_classification_rule 
FROM 
	target_data.t_categorization_setup  AS t1
INNER JOIN 
	target_data.cm_ldsity2target2categorization_setup AS t2
	ON  
	t1.id = ANY(ARRAY[55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105])  AND 
	t1.id = t2.categorization_setup 
),
w_refyearset2panel AS (
	SELECT 
		refyearset2panel
	FROM 
		w_spc_rules AS t1
	INNER JOIN 
		target_data.cm_spc2classification_rule AS t2
		ON
		t1.spc_classification_rule = t2.id
	INNER JOIN 
		target_data.c_sub_population_category AS t3
		ON
		t2.sub_population_category = t3.id
	INNER JOIN 
		target_data.cm_spc2classrule2panel_refyearset AS t4
		ON 
		t4.spc2classification_rule = t2.id
	GROUP BY t4.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_spc_rules)
)
SELECT array_agg(refyearset2panel) AS refyearset2panel, (SELECT count(*) >= 1 FROM w_spc_rules) AS rules_present FROM w_refyearset2panel;

-- a query to find the set of panel and reference yearset combinations for which values local densities can be classified using a particular set of area domain categories
WITH w_adc_rules AS (
SELECT  
	-- t1.id AS categorization_setup,
	-- t2.spc2classification_rule 
	 unnest(t2.adc2classification_rule) AS adc_classification_rule 
FROM 
	target_data.t_categorization_setup  AS t1
INNER JOIN 
	target_data.cm_ldsity2target2categorization_setup AS t2
	ON  
	t1.id = ANY(ARRAY[55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105])  AND 
	t1.id = t2.categorization_setup 
),
w_refyearset2panel AS (
	SELECT 
		refyearset2panel
	FROM 
		w_adc_rules AS t1
	INNER JOIN 
		target_data.cm_adc2classification_rule AS t2
		ON
		t1.adc_classification_rule = t2.id
	INNER JOIN 
		target_data.c_area_domain_category AS t3
		ON
		t2.area_domain_category = t3.id
	INNER JOIN 
		target_data.cm_adc2classrule2panel_refyearset AS t4
		ON 
		t4.adc2classification_rule = t2.id
	GROUP BY t4.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_adc_rules)
)
SELECT array_agg(refyearset2panel) AS refyearset2panel FROM w_refyearset2panel

SELECT NULL;
*/
-- </function>

DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_groups(INT []);

-- <function name="fn_get_eligible_panel_refyearset_groups" schema="target_data" src="functions/fn_get_eligible_panel_refyearset_groups.sql">
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_eligible_panel_refyearset_groups
--------------------------------------------------------------------------------

-- ALTER EXTENSION nfiesta_target_data DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_groups(INT[])
-- DROP FUNCTION IF EXISTS target_data.fn_get_eligible_panel_refyearset_groups(INT []);

CREATE OR REPLACE FUNCTION 
target_data.fn_get_eligible_panel_refyearset_groups(IN _eligible_refyearset2panel_mappings INT[])
RETURNS TABLE (panel_refyearset_group INT, label VARCHAR(200), description TEXT, label_en VARCHAR(200), description_en TEXT) AS 
$$
DECLARE 
	-- the set of combinations of panel and reference-yearset of an eligible group is a subset of eligible panel and reference-yearset combinations handed over in _eligible_refyearset2panel_mappings INT[] 
	_eligible_groups INT[]; -- eligible groups of panel and reference-yearset combinations      
BEGIN
	
	-- raising an exception on NULL input
	IF _eligible_refyearset2panel_mappings IS NULL then 
		RAISE EXCEPTION 'Input parameter _eligible_refyearset2panel_mappings INT[] is required!';
	END IF; 
	
	WITH w_eligible_groups AS (
		SELECT
			t1.panel_refyearset_group, 
			array_agg(refyearset2panel) -- aggregation of all refyearset and panel combinations for each group    
		FROM
			target_data.t_panel_refyearset_group AS t1
		GROUP BY t1.panel_refyearset_group HAVING array_agg(refyearset2panel) <@ _eligible_refyearset2panel_mappings -- keep only those groups which are subsets of the set eligible combinations
	)
	SELECT array_agg(t1.panel_refyearset_group) FROM w_eligible_groups AS t1 INTO _eligible_groups; 
	
	RETURN QUERY 
	WITH w_eligible_groups AS (
		SELECT
			unnest(_eligible_groups) AS panel_refyearset_group
	)
	SELECT 
		t1.panel_refyearset_group,
		t2."label",
		t2.description,
		t2.label_en,
		t2.description_en
	FROM 
		w_eligible_groups AS t1
	INNER JOIN
		target_data.c_panel_refyearset_group AS t2 
	ON	
		t1.panel_refyearset_group = t2.id;
	
END
$$ 
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER; 

COMMENT ON FUNCTION target_data.fn_get_eligible_panel_refyearset_groups(int[]) IS 
'Function returns combinations of panel and reference-yearsets, for which local densities can be calculated';

GRANT EXECUTE ON FUNCTION target_data.fn_get_eligible_panel_refyearset_groups(int[]) TO public;

/* -- tests
 -- test if an exception is raised 
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_groups(NULL);
 
-- return some groups
 SELECT * FROM target_data.fn_get_eligible_panel_refyearset_groups(ARRAY[1,2,3]);  
 */

-- </function>

