--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------;
-- Table: c_ldsity_object_group
--------------------------------------------------------------------;

DROP TABLE target_data.c_topic CASCADE;

CREATE TABLE target_data.c_ldsity_object_group
(
	id			serial NOT NULL,
	label		character varying(200) NOT NULL UNIQUE,
  	description			text NOT NULL UNIQUE,
	label_en	character varying(200) UNIQUE,
	description_en		text UNIQUE
);

COMMENT ON TABLE target_data.c_ldsity_object_group IS 'Lookup table with groups of objects.';
COMMENT ON COLUMN target_data.c_ldsity_object_group.id IS 'Identifier of the category.';
COMMENT ON COLUMN target_data.c_ldsity_object_group.label IS 'Label of the category.';
COMMENT ON COLUMN target_data.c_ldsity_object_group.description IS 'Detailed description of the category.';
COMMENT ON COLUMN target_data.c_ldsity_object_group.label_en IS 'English label of the category.';
COMMENT ON COLUMN target_data.c_ldsity_object_group.description_en IS 'Detailed English description of the category.';

ALTER TABLE target_data.c_ldsity_object_group ADD CONSTRAINT pkey__c_ldsity_object_group_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_ldsity_object_group_id ON target_data.c_ldsity_object_group IS 'Primary key.';

SELECT pg_catalog.pg_extension_config_dump('target_data.c_ldsity_object_group', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_ldsity_object_group_id_seq', '');

GRANT SELECT ON TABLE 				target_data.c_ldsity_object_group TO PUBLIC;
GRANT SELECT, USAGE ON SEQUENCE 		target_data.c_ldsity_object_group_id_seq TO PUBLIC;

GRANT SELECT ON TABLE				 target_data.c_ldsity_object_group TO app_nfiesta;
GRANT SELECT, USAGE ON SEQUENCE			 target_data.c_ldsity_object_group_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_ldsity_object_group TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_ldsity_object_group_id_seq TO app_nfiesta_mng;


--------------------------------------------------------------------;
-- Table: cm_ld_object2ld_object_group
--------------------------------------------------------------------;

DROP TABLE target_data.cm_target_variable2topic CASCADE;

CREATE TABLE target_data.cm_ld_object2ld_object_group
(
	id 			serial NOT NULL,
	ldsity_object_group		integer NOT NULL,
	ldsity_object 		integer NOT NULL
);

COMMENT ON TABLE target_data.cm_ld_object2ld_object_group IS 'Table with mapping between ldsity_object and ldsity_object_group.';
COMMENT ON COLUMN target_data.cm_ld_object2ld_object_group.id IS 'Identifier of the record.';
COMMENT ON COLUMN target_data.cm_ld_object2ld_object_group.ldsity_object_group IS 'Identifier of the ldsity_object_group, foreign key to table c_ldsity_object_group.';
COMMENT ON COLUMN target_data.cm_ld_object2ld_object_group.ldsity_object IS 'Identifier of the ldsity_object, foreign key to table c_ldsity_object.';

ALTER TABLE target_data.cm_ld_object2ld_object_group ADD CONSTRAINT pkey__cm_ld_object2ld_object_group_id PRIMARY KEY(id);

ALTER TABLE target_data.cm_ld_object2ld_object_group ADD CONSTRAINT
fkey__cm_ld_object2ld_object_group__c_ldsity_object_group FOREIGN KEY (ldsity_object_group)
REFERENCES target_data.c_ldsity_object_group (id);
COMMENT ON CONSTRAINT fkey__cm_ld_object2ld_object_group__c_ldsity_object_group ON target_data.cm_ld_object2ld_object_group IS
'Foreign key to table c_ldsity_object_group.';

ALTER TABLE target_data.cm_ld_object2ld_object_group ADD CONSTRAINT
fkey__cm_ld_object2ld_object_group__c_ldsity_object FOREIGN KEY (ldsity_object)
REFERENCES target_data.c_ldsity_object (id);
COMMENT ON CONSTRAINT fkey__cm_ld_object2ld_object_group__c_ldsity_object ON target_data.cm_ld_object2ld_object_group IS
'Foreign key to table c_ldsity_object.';

GRANT SELECT ON TABLE 				target_data.cm_ld_object2ld_object_group TO PUBLIC;
GRANT SELECT, USAGE ON SEQUENCE 		target_data.cm_ld_object2ld_object_group_id_seq TO PUBLIC;

GRANT SELECT ON TABLE				 target_data.cm_ld_object2ld_object_group TO app_nfiesta;
GRANT SELECT, USAGE ON SEQUENCE			 target_data.cm_ld_object2ld_object_group_id_seq TO app_nfiesta;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.cm_ld_object2ld_object_group TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.cm_ld_object2ld_object_group_id_seq TO app_nfiesta_mng;

SELECT pg_catalog.pg_extension_config_dump('target_data.cm_ld_object2ld_object_group', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.cm_ld_object2ld_object_group_id_seq', '');

--------------------------------------------------------------------;
-- Table: c_target_variable --ADJUST
--------------------------------------------------------------------;

DROP TABLE target_data.c_indicator CASCADE;
DROP INDEX target_data.fki__c_target_variable__c_indicator;

ALTER TABLE target_data.c_target_variable
DROP COLUMN indicator,
ADD COLUMN  label character varying(200) NOT NULL UNIQUE,
ADD COLUMN description text NOT NULL UNIQUE,
ADD COLUMN state_or_change integer NOT NULL,
ADD COLUMN label_en	character varying(200) UNIQUE,
ADD COLUMN description_en text UNIQUE;

COMMENT ON COLUMN target_data.c_target_variable.label IS 'Label of the category.';
COMMENT ON COLUMN target_data.c_target_variable.state_or_change IS 'Foreign key to table c_state_or_change.';
COMMENT ON COLUMN target_data.c_target_variable.description IS 'Detailed description of the category.';
COMMENT ON COLUMN target_data.c_target_variable.label_en IS 'English label of the category.';
COMMENT ON COLUMN target_data.c_target_variable.description_en IS 'Detailed English description of the category.';

ALTER TABLE target_data.c_target_variable ADD CONSTRAINT
fkey__c_target_variable__c_state_or_change FOREIGN KEY (state_or_change)
REFERENCES target_data.c_state_or_change (id);
COMMENT ON CONSTRAINT fkey__c_target_variable__c_state_or_change ON target_data.c_target_variable IS
'Foreign key to table c_state_or_change.';

-----------------------------------------------
-- other DDL
-----------------------------------------------

ALTER TABLE target_data.c_ldsity_object ADD COLUMN label_en character varying(200);
ALTER TABLE target_data.c_ldsity_object ADD COLUMN description_en text;

----------------------------------------------
-- Functions
-----------------------------------------------

DROP FUNCTION IF EXISTS target_data.fn_get_topic() CASCADE;
DROP FUNCTION IF EXISTS target_data.fn_get_indicator(integer) CASCADE;

-- <function name="fn_get_ldsity_object_group" schema="target_data" src="functions/fn_get_ldsity_object_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object_group(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object_group(_id integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_ldsity_object_group AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_ldsity_object_group AS t1
		WHERE t1.id = _id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object_group(integer) IS
'Function returns records from c_ldsity_object_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object_group(integer) TO public;

-- </function>

-- <function name="fn_delete_ldsity_object_group" schema="target_data" src="functions/fn_delete_ldsity_object_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_ldsity_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_ldsity_object_group(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_ldsity_object_group(_id integer)
RETURNS void
AS
$$
BEGIN
	DELETE FROM target_data.c_ldsity_object_group WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_ldsity_object_group(integer) IS
'Function returns records from c_ldsity_object_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_ldsity_object_group(integer) TO public;

-- </function>

-- <function name="fn_try_delete_ldsity_object_group" schema="target_data" src="functions/fn_try_delete_ldsity_object_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_delete_ldsity_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_try_delete_ldsity_object_group(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_delete_ldsity_object_group(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _id)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity_object_group (%)', _id;
	END IF;

	RETURN NOT EXISTS (SELECT * FROM target_data.cm_ld_object2ld_object_group AS t1 WHERE t1.ldsity_object_group = _id);
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_delete_ldsity_object_group(integer) IS
'Function provides test if it is possible to delete records from c_ldsity_object_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_delete_ldsity_object_group(integer) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_save_topic(character varying, text, character varying, text, integer) CASCADE;

-- <function name="fn_save_ldsity_object_group" schema="target_data" src="functions/fn_save_ldsity_object_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_ldsity_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_ldsity_object_group(character varying, text, character varying, text, integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_ldsity_object_group(_label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text, _ldsity_objects integer[] DEFAULT NULL::integer[], _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
DECLARE
	_ldsity_object_group integer;
BEGIN
	IF _id IS NULL
	THEN 
		IF _label IS NOT NULL AND _description IS NOT NULL AND _ldsity_objects IS NOT NULL
		THEN
			INSERT INTO target_data.c_ldsity_object_group(label, description, label_en, description_en)
			SELECT _label, _description, _label_en, _description_en
			RETURNING id
			INTO _ldsity_object_group;

			INSERT INTO target_data.cm_ld_object2ld_object_group(ldsity_object_group, ldsity_object)
			SELECT _ldsity_object_group, unnest(_ldsity_objects);
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%) and ldsity objects array (%) must be not null!', _label, _description, _ldisty_objects;
		END IF;
	ELSE
		IF _ldsity_objects IS NULL
		THEN
			UPDATE target_data.c_ldsity_object_group
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_ldsity_object_group.id = _id;

			_ldsity_object_group := _id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing group and create new one with given ldsity objects (%).', _ldsity_objects;
		END IF;
	END IF;

	RETURN _ldsity_object_group;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_ldsity_object_group(character varying, text, character varying, text, integer[], integer) IS
'Function provides insert into/update in c_ldsity_object_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_ldsity_object_group(character varying, text, character varying, text, integer[], integer) TO public;

-- </function>

-- <function name="fn_get_ldsity_object" schema="target_data" src="functions/fn_get_ldsity_object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200)
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t3.id AS ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, t1.table_name
		FROM target_data.c_ldsity_object AS t1
		LEFT JOIN target_data.cm_ld_object2ld_object_group AS t2
		ON t1.id = t2.ldsity_object
		LEFT JOIN target_data.c_ldsity_object_group AS t3
		ON t2.ldsity_object_group = t3.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		SELECT t1.id, t3.id AS ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, t1.table_name
		FROM target_data.c_ldsity_object AS t1
		INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
		ON t1.id = t2.ldsity_object
		INNER JOIN target_data.c_ldsity_object_group AS t3
		ON t2.ldsity_object_group = t3.id
		WHERE t3.id = _ldsity_object_group;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object(integer) IS
'Function returns records from c_ldsity_object table, optionally for given topic.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object(integer) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_get_ldsity(integer) CASCADE;

-- <function name="fn_get_ldsity" schema="target_data" src="functions/fn_get_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity(_target_variable integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
target_variable		integer,
label			character varying(200),
label_en		character varying(200),
column_expression	text,
unit_of_measure		integer,
ldsity_object		integer,
ldsity_object_type	integer,
object_type_label	character varying(200),
object_type_desc	text
)
AS
$$
BEGIN
	IF _target_variable IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS target_variable,
			t1.label, t1.label_en, t1.column_expression, t1.ldsity_object, t1.unit_of_measure,
			NULL::int, NULL::varchar(200), NULL::text
		FROM target_data.c_ldsity AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_target_variable (%)', _target_variable;
		END IF;

		RETURN QUERY
		SELECT t4.id, t1.id AS target_variable,
			t3.label, t3.label_en, t3.column_expression, t3.ldsity_object, t3.unit_of_measure, 
			t2.ldsity_object_type, t4.label, t4.description
		FROM target_data.c_target_variable AS t1
		INNER JOIN target_data.cm_ldsity2target_variable AS t2
		ON t1.id = t2.target_variable
		INNER JOIN target_data.c_ldsity AS t3
		ON t2.ldsity = t3.id
		INNER JOIN target_data.c_ldsity_object_type AS t4
		ON t2.ldsity_object_type = t4.id
		WHERE t1.id = _target_variable;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity(integer) IS
'Function returns records from c_ldsity table, optionally for given target_variable.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity(integer) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_get_target_variable(integer) CASCADE;

-- <function name="fn_get_target_variable" schema="target_data" src="functions/fn_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_target_variable(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_target_variable(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
state_or_change		integer,
soc_label		character varying(200),
soc_description		text
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, t1.state_or_change, t2.label, t2.description
		FROM target_data.c_target_variable AS t1
		INNER JOIN target_data.c_state_or_change AS t2
		ON t1.state_or_change = t2.id; 	
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		SELECT t6.id, t1.id AS ldsity_object_group, t6.label, t6.description, t6.label_en, t6.description_en, t6.state_or_change, t7.label, t7.description
		FROM target_data.c_ldsity_object_group AS t1
		INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
		ON t1.id = t2.ldsity_object_group
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN target_data.c_ldsity AS t4
		ON t3.id = t4.ldsity_object
		INNER JOIN target_data.cm_ldsity2target_variable AS t5
		ON t4.id = t5.ldsity
		INNER JOIN target_data.c_target_variable AS t6
		ON t5.target_variable = t6.id
		INNER JOIN target_data.c_state_or_change AS t7
		ON t6.state_or_change = t7.id
		WHERE t1.id = _ldsity_object_group;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_target_variable(integer) IS
'Function returns records from c_target_variable table, optionally for given ldsity_object_group.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_target_variable(integer) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_save_target_variable(integer[], integer, integer[], integer[]) CASCADE;

-- <function name="fn_save_target_variable" schema="target_data" src="functions/fn_save_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer, integer[], integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_target_variable(_label character varying(200), _description text, _label_en character varying(200) DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text, _state_or_change integer DEFAULT NULL::integer, _ldsity integer[] DEFAULT NULL::integer[], _ldsity_object_type integer[] DEFAULT NULL::integer[], _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
DECLARE
	_target_variable integer;
BEGIN
	IF _id IS NULL
	THEN
		IF _label IS NOT NULL AND _description IS NOT NULL AND _state_or_change IS NOT NULL AND _ldsity IS NOT NULL
		THEN
			IF array_length(_ldsity,1) != array_length(_ldsity_object_type,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity, _ldsity_object_type;
			END IF;

			INSERT INTO target_data.c_target_variable(label, description, label_en, description_en, state_or_change)
			SELECT _label, _description, _label_en, _description_en, _state_or_change
			RETURNING id
			INTO _target_variable;

			INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category)
			SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[]
			FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
			ON t1.id = t2.id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%), state_or_change (%) and  must be not null!', _label, _description, _state_or_change;
		END IF;
	ELSE
		IF _ldsity IS NULL
		THEN
			UPDATE target_data.c_target_variable
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en,
				state_or_change = _state_or_change
			WHERE c_target_variable.id = _id;

			_target_variable := _id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing target variable and create new one with given ldsitys (%).', _ldsity;
		END IF;
	END IF;

	RETURN _target_variable;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer, integer[], integer[], integer) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer, integer[], integer[], integer) TO public;

-- </function>


