--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-------------------
-- Functions
-------------------


-- <function name="fn_get_eligible_panel_refyearset_combinations_internal" schema="target_data" src="functions/fn_get_eligible_panel_refyearset_combinations_internal.sql">
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_eligible_panel_refyearset_combinations_internal 
--------------------------------------------------------------------------------
-- ALTER EXTENSION nfiesta_target_data DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations_internal(INT, BOOL);
-- DROP FUNCTION IF EXISTS target_data.fn_get_eligible_panel_refyearset_combinations_internal(INT, BOOL);

CREATE OR REPLACE FUNCTION 
target_data.fn_get_eligible_panel_refyearset_combinations_internal(IN _target_variable INT, _use_negative BOOL)
RETURNS INT[] AS 
$$
DECLARE 

	_refyearset2panel4ldsity INT[]; -- set of panel and refrerence yearset combinations for which local densities are available
	_refyearset2panel4ldsity_spc INT[]; -- set of panel and reference yearset combinations for which values local densities are ETL-constrained to the specified subpopulations
	_refyearset2panel4ldsity_adc INT[]; -- set of panel and reference yearset combinations for which values local densities are ETL-constrained to the specified areal domain  
	_refyearset2panel4ldsity2target_variable_spc INT[]; -- set of panel and reference yearset combinations for which values local densities were constrained to the specified subpopulations by the user   
	_refyearset2panel4ldsity2target_variable_adc INT[]; -- set of panel and reference yearset combinations for which values local densities were constrained to the specified area domains by the user
	_refyearset2panel4spc INT[]; -- set of panel and reference yearset combinations for which values local densities can be classified using a particular set of subpopulation categories
	_refyearset2panel4adc INT[]; -- set of panel and reference yearset combinations for which values local densities can be classified using a particular set of area domain categories 
	_refyearset2panel_common INT[]; -- set of panel and reference yearset combinations for which values local densities of a target variable can be calculated
	_rules_present bool; -- logical value coding whether there are some rules defined    

BEGIN
	
	-- raising an exception on NULL input
	IF _target_variable IS NULL then 
		RAISE EXCEPTION 'Input parameter _target_variable INT not given, but it is required!';
	END IF; 

	-- raising an exception on NULL input
	IF _use_negative IS NULL then 
		RAISE EXCEPTION 'Input parameter _use_negative INT not given, but it is required!';
	END IF; 

	-- assigning _refyearset2panel4ldsity
	WITH w_ldsity AS (
		SELECT DISTINCT 
			t2.id AS ldsity, 
			t1."version" 
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN 
			target_data.c_ldsity AS t2
			ON	
		 	t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative AND
			t1.ldsity = t2.id
	),
	w_refyearset2panel AS (
		SELECT 
			t2.refyearset2panel 
		FROM 
			w_ldsity  AS t1
		INNER JOIN 
			target_data.cm_ldsity2panel_refyearset_version AS t2
			ON 
			t2.ldsity = t1.ldsity AND 
			t2."version" = t2."version"
		GROUP BY t2.refyearset2panel, t2."version" HAVING count(*) = (SELECT count(*) FROM w_ldsity)
	)
	SELECT array_agg(DISTINCT refyearset2panel) FROM w_refyearset2panel
	INTO _refyearset2panel4ldsity;

	-- assigning _refyearset2panel4ldsity_spc
	WITH w_ldsity_spc_rules AS (
		SELECT DISTINCT
			 unnest(t2.sub_population_category) AS spc2classification_rule 
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN 
			target_data.c_ldsity AS t2
			ON	
			t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative AND
			t1.ldsity = t2.id
	),
	w_refyearset2panel AS (
		SELECT 
			t3.refyearset2panel
		FROM
			w_ldsity_spc_rules AS t1	
		INNER JOIN
			target_data.cm_spc2classification_rule AS t2
			ON
			t1.spc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_spc2classrule2panel_refyearset AS t3 
			ON 
			t3.spc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity_spc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_ldsity_spc_rules) FROM w_refyearset2panel 
	INTO _refyearset2panel4ldsity_spc, _rules_present; 

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity_spc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	-- assigning _refyearset2panel4ldsity_adc
	WITH w_ldsity_adc_rules AS (
		SELECT DISTINCT
			 unnest(t2.area_domain_category) AS adc2classification_rule 
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN 
			target_data.c_ldsity AS t2
			ON	
			t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative AND
			t1.ldsity = t2.id
	),
	w_refyearset2panel AS (
		SELECT 
			t3.refyearset2panel
		FROM
			w_ldsity_adc_rules AS t1	
		INNER JOIN
			target_data.cm_adc2classification_rule AS t2
			ON
			t1.adc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_adc2classrule2panel_refyearset AS t3 
			ON 
			t3.adc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity_adc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_ldsity_adc_rules) FROM w_refyearset2panel 
	INTO _refyearset2panel4ldsity_adc, _rules_present;  

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity_adc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	-- assigning _refyearset2panel4ldsity2target_variable_spc
	WITH w_ldsity2target_variable_spc_rules AS (
		SELECT DISTINCT
			unnest(t1.sub_population_category) AS spc2classification_rule 
		FROM 
		 	target_data.cm_ldsity2target_variable AS t1
		WHERE
		 	t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative
	),
	w_refyearset2panel AS (
		SELECT 
			t3.refyearset2panel
		FROM
			w_ldsity2target_variable_spc_rules AS t1	
		INNER JOIN
			target_data.cm_spc2classification_rule AS t2
			ON
			t1.spc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_spc2classrule2panel_refyearset AS t3 
			ON 
			t3.spc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity2target_variable_spc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >=1 FROM w_ldsity2target_variable_spc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4ldsity2target_variable_spc, _rules_present; 

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity2target_variable_spc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;


	-- assigning _refyearset2panel4ldsity2target_variable_adc
	WITH w_ldsity2target_variable_adc_rules AS (
		SELECT DISTINCT
			unnest(t1.area_domain_category) AS adc2classification_rule
		FROM 
		 	target_data.cm_ldsity2target_variable AS t1
		WHERE
		 	t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative
	),
	w_refyearset2panel AS (
		SELECT
			t3.refyearset2panel
		FROM
			w_ldsity2target_variable_adc_rules AS t1	
		INNER JOIN
			target_data.cm_adc2classification_rule AS t2
			ON
			t1.adc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_adc2classrule2panel_refyearset AS t3
			ON 
			t3.adc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity2target_variable_adc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_ldsity2target_variable_adc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4ldsity2target_variable_adc, _rules_present; 

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity2target_variable_adc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	-- assigning _refyearset2panel4spc
	WITH w_spc_rules AS (
	SELECT DISTINCT 
		 unnest(t2.spc2classification_rule) AS spc_classification_rule 
	FROM 
		target_data.cm_ldsity2target_variable AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.target_variable = _target_variable AND 
		t1.use_negative = _use_negative AND
		t1.id = t2.ldsity2target_variable 
	),
	w_refyearset2panel AS (
		SELECT 
			refyearset2panel
		FROM 
			w_spc_rules AS t1
		INNER JOIN 
			target_data.cm_spc2classification_rule AS t2
			ON
			t1.spc_classification_rule = t2.id
		INNER JOIN 
			target_data.cm_spc2classrule2panel_refyearset AS t3
			ON 
			t3.spc2classification_rule = t2.id
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_spc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >=1 FROM w_spc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4spc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4spc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	-- assigning _refyearset2panel4adc
	WITH w_adc_rules AS (
	SELECT DISTINCT
		 unnest(t2.adc2classification_rule) AS adc_classification_rule 
	FROM 
		target_data.cm_ldsity2target_variable AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.target_variable = _target_variable AND 
		t1.use_negative = _use_negative AND
		t1.id = t2.ldsity2target_variable 
	),
	w_refyearset2panel AS (
		SELECT 
			refyearset2panel
		FROM 
			w_adc_rules AS t1
		INNER JOIN 
			target_data.cm_adc2classification_rule AS t2
			ON
			t1.adc_classification_rule = t2.id
		INNER JOIN 
			target_data.cm_adc2classrule2panel_refyearset AS t3
			ON 
			t3.adc2classification_rule = t2.id
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_adc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_adc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4adc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4adc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	WITH w_refyearset2panel_intersection AS (
		SELECT unnest(_refyearset2panel4ldsity) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity_spc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity_adc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity2target_variable_spc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity2target_variable_adc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4spc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4adc,_refyearset2panel4ldsity)) AS refyearset2panel
	)
	SELECT array_agg(refyearset2panel) FROM  w_refyearset2panel_intersection INTO _refyearset2panel_common;
	
	RETURN _refyearset2panel_common;
		
END
$$ 
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER; 

COMMENT ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations_internal(INT, BOOL) IS 
'Function returns combinations of panel and reference-yearsets, for which local densities can be calculated.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations_internal(int, BOOL) TO public;

/*
-- tests
-- inspection of the list of target variables 
SELECT * FROM target_data.c_target_variable; 

-- checking if the function returns some combinations of panels and reference yearsets for forest area   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, FALSE);

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, TRUE); 
-- no records, state variable has no negative contributions to local density

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(4, FALSE);
-- no records, please check and eventually adjust test data and its availability

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(4, TRUE);
-- no records, state variable has no negative contributions to local density

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(5, FALSE);
-- no records, please check and eventually adjust test data and its availability

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(5, TRUE);
-- no records, state variable has no negative contributions to local density

-- checking if the function returns some combinations of panels and reference yearsets for change of forest area 
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(6, FALSE);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(6, TRUE);

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(2, FALSE);

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(2, TRUE);
-- no records, state variable has no negative contributions to local density

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood and regeneration stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(3, FALSE);

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(3, TRUE);
-- no records, state variable has no negative contributions to local density

-- checking if the function raises an exception for NULL input
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(NULL, NULL);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, NULL);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(NULL, TRUE);
*/
-- </function>

-- remove function for which the input parameters need to be changed in addtition to other adjustments  
DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT []);

-- <function name="fn_get_eligible_panel_refyearset_combinations" schema="target_data" src="functions/fn_get_eligible_panel_refyearset_combinations.sql">
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_eligible_panel_refyearset_combinations 
--------------------------------------------------------------------------------
-- ALTER EXTENSION nfiesta_target_data DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT);
-- DROP FUNCTION IF EXISTS target_data.fn_get_eligible_panel_refyearset_combinations(INT);

CREATE OR REPLACE FUNCTION 
target_data.fn_get_eligible_panel_refyearset_combinations(IN _target_variable INT)
RETURNS TABLE (
	refyearset2panel_mapping INT, 
	panel_label VARCHAR(20), 
	panel_description VARCHAR(120), 
	refyearset_label VARCHAR(20), 
	refyearset_description VARCHAR(120))  AS 
$$
DECLARE 

	_state_or_change INT; -- 100 state variable, 200 change variable, 300 dynamic variable
	_refyearset2panel_eligible_sum INT[]; -- eligible combinations of panel and reference yearset for positive terms of the local density     
	_refyearset2panel_eligible_subtract INT[]; -- eligible combinations of panel and reference yearset for negative terms of the local density

BEGIN
	
	-- raising an exception on NULL input
	IF _target_variable IS NULL then 
		RAISE EXCEPTION 'Input parameter _target_variable INT not given, but it is required!';
	END IF; 

	-- set the state or change variable
	SELECT state_or_change FROM target_data.c_target_variable WHERE id = _target_variable INTO _state_or_change; 	

	SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(_target_variable, FALSE) 
	INTO _refyearset2panel_eligible_sum;
	
	IF _refyearset2panel_eligible_sum IS NULL THEN 
		RETURN;
	END IF;


	IF _state_or_change = 200 THEN 
	
		SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(_target_variable, TRUE) 
		INTO _refyearset2panel_eligible_subtract;
		
		IF _refyearset2panel_eligible_subtract IS NULL THEN 
			RETURN;
		END IF;
	
		RETURN QUERY
		SELECT 
			t1.id AS refyearset2panel_mapping,
			t5.panel AS panel_label,
			t5."label" AS panel_description,
			t2.reference_year_set AS refyearset_label,
			t2."label" AS refyearset_description
		FROM 
			sdesign.cm_refyearset2panel_mapping AS t1
		INNER JOIN 
			sdesign.t_reference_year_set AS t2
			ON
			ARRAY[t1.id] <@ _refyearset2panel_eligible_sum AND
			t1.reference_year_set = t2.reference_year_set_end
		INNER JOIN
			sdesign.cm_refyearset2panel_mapping AS t3
			ON
			ARRAY[t3.id] <@ _refyearset2panel_eligible_subtract AND
			t1.panel = t3.panel
		INNER JOIN 
			sdesign.t_reference_year_set AS t4
			ON
			t3.reference_year_set = t4.reference_year_set_begin AND 
			t2.id = t4.id
		 INNER JOIN 
		 	sdesign.t_panel AS t5
		 	ON
		 	t1.panel = t5.id;
	

	ELSE -- state or dynamic variables with no subtraction of local density contributions
	
		RETURN QUERY
		WITH w_refyearset2panel_mapping AS (
			SELECT unnest(_refyearset2panel_eligible_sum) AS refyearset2panel_mapping
		)
		SELECT 
			t1.refyearset2panel_mapping,
			t4.panel AS panel_label,
			t4."label" AS panel_description,
			t3.reference_year_set AS refyerset_label,
			t3."label" AS refyearset_description
		FROM
			w_refyearset2panel_mapping AS t1 
		INNER JOIN	
			sdesign.cm_refyearset2panel_mapping AS t2
			ON t1.refyearset2panel_mapping = t2.id
		INNER JOIN
			sdesign.t_reference_year_set AS t3
			ON t2.reference_year_set = t3.id
		INNER JOIN
			sdesign.t_panel AS t4
			ON t2.panel = t4.id;
	END IF;
	
 RETURN;

END
$$ 
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER; 

COMMENT ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT) IS 
'Function returns combinations of panel and reference-yearsets, for which local densities can be calculated.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT) TO public;

/*
-- tests
-- inspection of the list of target variables 
SELECT * FROM target_data.c_target_variable; 

-- checking if the function returns some combinations of panels and reference yearsets for forest area   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(1);

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(4);
-- no records, please check and eventually adjust test data and its availability

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(5);
-- no records, please check and eventually adjust test data and its availability

-- checking if the function returns some combinations of panels and reference yearsets for change of forest area 
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(6);

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(2);

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood and regeneration stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(3);

 -- checking if the function raises an exception for NULL input
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(NULL);
*/
-- </function>
