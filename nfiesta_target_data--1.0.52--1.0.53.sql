--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-------------------
-- Functions
-------------------

-- <function name="fn_get_target_variable" schema="target_data" src="functions/fn_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_target_variable(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_target_variable(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer[],
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
state_or_change		integer,
soc_label		character varying(200),
soc_description		text,
areal_or_population	integer
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		WITH 
		w_objects AS (
			SELECT t2.ldsity_object_group, array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			GROUP BY t2.ldsity_object_group
			),
		w_core AS (
			SELECT 	t5.target_variable, 
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = false) AS ldsity_objects_pozit,
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = true) AS ldsity_objects_negat
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		),
		w_target_variable AS (
			SELECT 	t2.ldsity_object_group, t1.target_variable, t3.label, t3.description, t3.label_en, t3.description_en, t3.state_or_change, 
				t4.label AS soc_label, t4.description AS soc_description
			FROM w_core AS t1
			INNER JOIN w_objects AS t2
			ON t2.ldsity_objects = t1.ldsity_objects_pozit OR (t2.ldsity_objects @> t1.ldsity_objects_pozit AND t2.ldsity_objects @> t1.ldsity_objects_pozit)
			INNER JOIN target_data.c_target_variable AS t3
			ON t1.target_variable = t3.id
			INNER JOIN target_data.c_state_or_change AS t4
			ON t3.state_or_change = t4.id
		),
		w_agg_200 AS (
			SELECT 	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category,
				t5.areal_or_population,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = false) AS ldsity_pozit,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = true) AS ldsity_negat,
				count(*) OVER (PARTITION BY t1.target_variable) AS no_of_contrib
			FROM w_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.target_variable = t2.target_variable AND t2.ldsity_object_type = 100
			INNER JOIN target_data.c_ldsity AS t4
			ON t2.ldsity = t4.id
			INNER JOIN target_data.c_ldsity_object AS t5
			ON t4.ldsity_object = t5.id
			LEFT JOIN target_data.cm_ldsity2target_variable AS t3
			ON t1.target_variable = t3.target_variable AND t3.ldsity_object_type = 200 AND t2.use_negative = t3.use_negative
			GROUP BY t1.target_variable, t1.ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, t5.areal_or_population
		), w_agg_id AS (
			-- query with one contribution which can be divided by other contributions
			(SELECT array_agg(t1.target_variable) AS ids, t1.ldsity_object_group,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib = 1
			GROUP BY t1.ldsity, t1.ldsity_object_group, 
				t1.area_domain_category, t1.sub_population_category,
				t1.state_or_change, t1.soc_label, t1.soc_description, t1.areal_or_population,
				t1.ldsity_object_type)
			UNION ALL
			-- query with more than one contribution, can not be divided
			(SELECT array_agg(DISTINCT t1.target_variable) AS ids, t1.ldsity_object_group, 
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib > 1
			GROUP BY t1.ldsity_object_group, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type)
		)
		SELECT ids AS id, t1.ldsity_object_group, t1.label[1], t1.description[1], t1.label_en[1], t1.description_en[1],
			t1.state_or_change, t1.soc_label, t1.soc_description,
			t1.areal_or_population[1]
		FROM w_agg_id AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		WITH 
		w_objects AS (
			SELECT t2.ldsity_object_group, array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			WHERE t1.id = _ldsity_object_group
			GROUP BY t2.ldsity_object_group
			),
		w_core AS (
			SELECT 	t5.target_variable,
 				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = false) AS ldsity_objects_pozit,
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = true) AS ldsity_objects_negat
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		),
		w_target_variable AS (
			SELECT 	t2.ldsity_object_group, t1.target_variable, t3.label, t3.description, t3.label_en, t3.description_en, t3.state_or_change, 
				t4.label AS soc_label, t4.description AS soc_description
			FROM w_core AS t1
			INNER JOIN w_objects AS t2
			ON t2.ldsity_objects = t1.ldsity_objects_pozit OR (t2.ldsity_objects @> t1.ldsity_objects_pozit AND t2.ldsity_objects @> t1.ldsity_objects_pozit)
			INNER JOIN target_data.c_target_variable AS t3
			ON t1.target_variable = t3.id
			INNER JOIN target_data.c_state_or_change AS t4
			ON t3.state_or_change = t4.id
		),
		w_agg_200 AS (
			SELECT 	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category,
				t5.areal_or_population,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = false) AS ldsity_pozit,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = true) AS ldsity_negat,
				count(*) OVER (PARTITION BY t1.target_variable) AS no_of_contrib
			FROM w_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.target_variable = t2.target_variable AND t2.ldsity_object_type = 100
			INNER JOIN target_data.c_ldsity AS t4
			ON t2.ldsity = t4.id
			INNER JOIN target_data.c_ldsity_object AS t5
			ON t4.ldsity_object = t5.id
			LEFT JOIN target_data.cm_ldsity2target_variable AS t3
			ON t1.target_variable = t3.target_variable AND t3.ldsity_object_type = 200 AND t2.use_negative = t3.use_negative
			GROUP BY t1.target_variable, t1.ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, t5.areal_or_population
		), w_agg_id AS (
			-- query with one contribution which can be divided by other contributions
			(SELECT array_agg(t1.target_variable) AS ids, t1.ldsity_object_group,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib = 1
			GROUP BY t1.ldsity, t1.ldsity_object_group, 
				t1.area_domain_category, t1.sub_population_category,
				t1.state_or_change, t1.soc_label, t1.soc_description, t1.areal_or_population,
				t1.ldsity_object_type)
			UNION ALL
			-- query with more than one contribution, can not be divided
			(SELECT array_agg(DISTINCT t1.target_variable) AS ids, t1.ldsity_object_group, 
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib > 1
			GROUP BY t1.ldsity_object_group, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type)
		)
		SELECT ids AS id, t1.ldsity_object_group, t1.label[1], t1.description[1], t1.label_en[1], t1.description_en[1],
			t1.state_or_change, t1.soc_label, t1.soc_description,
			t1.areal_or_population[1]
		FROM w_agg_id AS t1;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_target_variable(integer) IS
'Function returns records from c_target_variable table, optionally for given ldsity_object_group.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_target_variable(integer) TO public;

-- </function>

DROP FUNCTION target_data.fn_get_ldsity(integer[], integer) CASCADE;

-- <function name="fn_get_ldsity" schema="target_data" src="functions/fn_get_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_ldsity(integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity(_target_variable integer[] DEFAULT NULL::integer[], _ldsity_object_type integer DEFAULT 100::integer)
RETURNS TABLE (
id			integer,
target_variable		integer,
label			character varying(200),
label_en		character varying(200),
column_expression	text,
unit_of_measure		integer,
ldsity_object		integer,
ldsity_object_type	integer,
object_type_label	character varying(200),
object_type_desc	text,
use_negative		boolean
)
AS
$$
DECLARE
_test_pozit		integer;
_test_negat		integer;
BEGIN
	IF _target_variable IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS target_variable,
			t1.label, t1.label_en, t1.column_expression, t1.ldsity_object, t1.unit_of_measure,
			NULL::int, NULL::varchar(200), NULL::text, NULL::boolean
		FROM target_data.c_ldsity AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable[1])
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_target_variable (%)', _target_variable;
		END IF;

		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_type AS t1 WHERE t1.id = _ldsity_object_type)
		THEN RAISE EXCEPTION 'Given ldsity_object_type does not exist in table c_ldsity_object_type (%)', _ldsity_object_type;
		END IF;

		-- test if all given target_variables has the same ldsitys 100
		WITH w AS (
			SELECT 	DISTINCT t1.id AS target_variable, t3.id AS ldsity, 
				t2.area_domain_category, t2.sub_population_category, t2.use_negative
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _target_variable AND t4.id = 100
		), w_agg_target_variable AS (
			SELECT w.target_variable, w.area_domain_category, w.sub_population_category,
				array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) FILTER (WHERE w.use_negative = false) AS ldsity_pozit,
				array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) FILTER (WHERE w.use_negative = true) AS ldsity_negat
			FROM w
			GROUP BY w.target_variable, w.area_domain_category, w.sub_population_category
		)
		SELECT count(DISTINCT ldsity_pozit), count(DISTINCT ldsity_negat)
		FROM w_agg_target_variable 
		INTO _test_pozit, _test_negat;

		IF _test_pozit > 1 OR _test_negat > 1
		THEN
			RAISE EXCEPTION 'Given target variables (in array) does not have the same ldsitys of ldsity object type 100 (pozitive or negative). There is either different ldsity, or the same ldsity but somehow constrained by area domain category or sub population category.';
		END IF;
	
		CASE WHEN _ldsity_object_type = 100
		THEN
		

			-- return distinct objects for given target variables
			RETURN QUERY
			WITH w AS (
				SELECT  t1.id AS target_variable, count(CASE WHEN t2.ldsity_object_type = 200 THEN 1 ELSE NULL END) AS ldsity_200
				FROM target_data.c_target_variable AS t1
				INNER JOIN target_data.cm_ldsity2target_variable AS t2
				ON t1.id = t2.target_variable
				WHERE array[t1.id] <@ _target_variable
				GROUP BY t1.id
			)
			SELECT  t3.id, t1.id AS target_variable,
				t3.label, t3.label_en, t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
				t2.ldsity_object_type, t4.label, t4.description, t2.use_negative
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			INNER JOIN w AS t5 ON t1.id = t5.target_variable
			WHERE t4.id = 100 AND CASE WHEN array_length(_target_variable,1) > 1 THEN t5.ldsity_200 = 0 ELSE true END;

		WHEN _ldsity_object_type = 200
		THEN
			RETURN QUERY
			SELECT  t3.id AS ldsity, t1.id AS target_variable,
				t3.label, t3.label_en, t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
				t2.ldsity_object_type, t4.label, t4.description, t2.use_negative
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _target_variable AND t4.id = 200;
		ELSE
			RAISE EXCEPTION 'Not known value of ldsity object type (%).', _ldsity_object_type;
		END CASE;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity(integer[], integer) IS
'Function returns records from c_ldsity table, optionally for given target_variable.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity(integer[], integer) TO public;

-- </function>

