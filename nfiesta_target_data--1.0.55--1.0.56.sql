--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------
-- Functions
---------------------

-- <function name="fn_save_ldsity_object" schema="target_data" src="functions/fn_save_ldsity_object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_ldsity_object(integer, character varying, text, character varying, text, integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_ldsity_object(_id integer, _label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text)
RETURNS integer
AS
$$
BEGIN
	IF _id IS NOT NULL 
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _id;
		END IF;

		IF _label IS NOT NULL OR _description IS NOT NULL
		THEN
			UPDATE target_data.c_ldsity_object
			SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
			WHERE c_ldsity_object.id = _id;
		ELSE
			RAISE EXCEPTION 'At least label or description must be not null when doing an update of c_ldsity_object (%, %).', _label, _description;
		END IF;
	ELSE
		RAISE EXCEPTION 'Parameter _id must not be null (%)!', _id;
	END IF;

	RETURN _ldsity_object;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_ldsity_object(integer, character varying, text, character varying, text) IS
'Function provides update in c_ldsity_object table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_ldsity_object(integer, character varying, text, character varying, text) TO public;

-- </function>

-- <function name="fn_save_ldsity" schema="target_data" src="functions/fn_save_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_ldsity(integer, character varying, text, character varying, text, integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_ldsity(_id integer, _label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text)
RETURNS integer
AS
$$
BEGIN
	IF _id IS NOT NULL 
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _id;
		END IF;

		IF _label IS NOT NULL OR _description IS NOT NULL
		THEN
			UPDATE target_data.c_ldsity
			SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
			WHERE c_ldsity.id = _id;
		ELSE
			RAISE EXCEPTION 'At least label or description must be not null when doing an update of c_ldsity (%, %).', _label, _description;
		END IF;
	ELSE
		RAISE EXCEPTION 'Parameter _id must not be null (%)!', _id;
	END IF;

	RETURN _ldsity;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_ldsity(integer, character varying, text, character varying, text) IS
'Function provides update in c_ldsity table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_ldsity(integer, character varying, text, character varying, text) TO public;

-- </function>

-- <function name="fn_save_definition_variant" schema="target_data" src="functions/fn_save_definition_variant.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_definition_variant
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_definition_variant(integer, character varying, text, character varying, text, integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_definition_variant(_id integer, _label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text)
RETURNS integer
AS
$$
BEGIN
	IF _id IS NOT NULL 
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_definition_variant AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given definition variant does not exist in table c_definition_variant (%)', _id;
		END IF;

		IF _label IS NOT NULL OR _description IS NOT NULL
		THEN
			UPDATE target_data.c_definition_variant
			SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
			WHERE c_definition_variant.id = _id;
		ELSE
			RAISE EXCEPTION 'At least label or description must be not null when doing an update of c_definition_variant (%, %).', _label, _description;
		END IF;
	ELSE
		RAISE EXCEPTION 'Parameter _id must not be null (%)!', _id;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_definition_variant(integer, character varying, text, character varying, text) IS
'Function provides update in c_definition_variant table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_definition_variant(integer, character varying, text, character varying, text) TO public;

-- </function>

-- <function name="fn_save_unit_of_measure" schema="target_data" src="functions/fn_save_unit_of_measure.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_unit_of_measure
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_unit_of_measure(integer, character varying, text, character varying, text, integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_unit_of_measure(_id integer, _label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text)
RETURNS integer
AS
$$
BEGIN
	IF _id IS NOT NULL 
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_unit_of_measure AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given unit of measure does not exist in table c_unit_of_measure (%)', _id;
		END IF;

		IF _label IS NOT NULL OR _description IS NOT NULL
		THEN
			UPDATE target_data.c_unit_of_measure
			SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
			WHERE c_unit_of_measure.id = _id;
		ELSE
			RAISE EXCEPTION 'At least label or description must be not null when doing an update of c_unit_of_measure (%, %).', _label, _description;
		END IF;
	ELSE
		RAISE EXCEPTION 'Parameter _id must not be null (%)!', _id;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_unit_of_measure(integer, character varying, text, character varying, text) IS
'Function provides update in c_unit_of_measure table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_unit_of_measure(integer, character varying, text, character varying, text) TO public;

-- </function>

DROP FUNCTION target_data.fn_get_ldsity(integer[], integer) CASCADE;
-- <function name="fn_get_ldsity" schema="target_data" src="functions/fn_get_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_ldsity(integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity(_target_variable integer[] DEFAULT NULL::integer[], _ldsity_object_type integer DEFAULT 100::integer)
RETURNS TABLE (
id			integer,
target_variable		integer,
label			character varying(200),
label_en		character varying(200),
column_expression	text,
unit_of_measure		integer,
ldsity_object		integer,
ldsity_object_type	integer,
object_type_label	character varying(200),
object_type_desc	text,
use_negative		boolean,
version			integer
)
AS
$$
DECLARE
_test_pozit		integer;
_test_negat		integer;
BEGIN
	IF _target_variable IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS target_variable,
			t1.label, t1.label_en, t1.column_expression, t1.ldsity_object, t1.unit_of_measure,
			NULL::int, NULL::varchar(200), NULL::text, NULL::boolean, NULL::integer
		FROM target_data.c_ldsity AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable[1])
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_target_variable (%)', _target_variable;
		END IF;

		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_type AS t1 WHERE t1.id = _ldsity_object_type)
		THEN RAISE EXCEPTION 'Given ldsity_object_type does not exist in table c_ldsity_object_type (%)', _ldsity_object_type;
		END IF;

		-- test if all given target_variables has the same ldsitys 100
		WITH w AS (
			SELECT 	DISTINCT t1.id AS target_variable, t3.id AS ldsity, 
				t2.area_domain_category, t2.sub_population_category, t2.use_negative
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _target_variable AND t4.id = 100
		), w_agg_target_variable AS (
			SELECT w.target_variable, w.area_domain_category, w.sub_population_category,
				array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) FILTER (WHERE w.use_negative = false) AS ldsity_pozit,
				array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) FILTER (WHERE w.use_negative = true) AS ldsity_negat
			FROM w
			GROUP BY w.target_variable, w.area_domain_category, w.sub_population_category
		)
		SELECT count(DISTINCT ldsity_pozit), count(DISTINCT ldsity_negat)
		FROM w_agg_target_variable 
		INTO _test_pozit, _test_negat;

		IF _test_pozit > 1 OR _test_negat > 1
		THEN
			RAISE EXCEPTION 'Given target variables (in array) does not have the same ldsitys of ldsity object type 100 (pozitive or negative). There is either different ldsity, or the same ldsity but somehow constrained by area domain category or sub population category.';
		END IF;
	
		CASE WHEN _ldsity_object_type = 100
		THEN
			-- return distinct objects for given target variables
			RETURN QUERY
			WITH w AS (
				SELECT  t1.id AS target_variable, count(CASE WHEN t2.ldsity_object_type = 200 THEN 1 ELSE NULL END) AS ldsity_200 
				FROM target_data.c_target_variable AS t1
				INNER JOIN target_data.cm_ldsity2target_variable AS t2
				ON t1.id = t2.target_variable
				WHERE array[t1.id] <@ _target_variable
				GROUP BY t1.id
			)
			SELECT  t3.id, t1.id AS target_variable,
				t3.label, t3.label_en, t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
				t2.ldsity_object_type, t4.label, t4.description, t2.use_negative, t2.version
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			INNER JOIN w AS t5 ON t1.id = t5.target_variable
			WHERE t4.id = 100 AND CASE WHEN array_length(_target_variable,1) > 1 THEN t5.ldsity_200 = 0 ELSE true END;

		WHEN _ldsity_object_type = 200
		THEN
			RETURN QUERY
			SELECT  t3.id AS ldsity, t1.id AS target_variable,
				t3.label, t3.label_en, t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
				t2.ldsity_object_type, t4.label, t4.description, t2.use_negative, t2.version
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _target_variable AND t4.id = 200;
		ELSE
			RAISE EXCEPTION 'Not known value of ldsity object type (%).', _ldsity_object_type;
		END CASE;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity(integer[], integer) IS
'Function returns records from c_ldsity table, optionally for given target_variable.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity(integer[], integer) TO public;

-- </function>

-- <function name="fn_save_areal_or_pop" schema="target_data" src="functions/fn_save_areal_or_pop.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_areal_or_pop
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_areal_or_pop(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_areal_or_pop(_areal_or_population integer, _label varchar(200), _description text, _label_en varchar(200) DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text, _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
BEGIN
	IF _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of label or description is null (%, %).', _label, _description;
	END IF; 

	IF _id IS NULl
	THEN
		CASE 
		WHEN _areal_or_population = 100 THEN
			INSERT INTO target_data.c_area_domain(label, description, label_en, description_en)
			SELECT _label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;

		WHEN _areal_or_population = 200 THEN
			INSERT INTO target_data.c_sub_population(label, description, label_en, description_en)
			SELECT _label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	ELSE
		CASE WHEN _areal_or_population = 100 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _id;
			END IF;

			UPDATE target_data.c_area_domain
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_area_domain.id = _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain does not exist in table c_sub_population (%)', _id;
			END IF;

			UPDATE target_data.c_sub_population
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_sub_population.id = _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_areal_or_pop(integer, character varying, text, character varying, text, integer) IS
'Functions inserts a record into table c_area_domain or c_sub_population based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_areal_or_pop(integer, character varying, text, character varying, text, integer) TO public;

-- </function>

-- <function name="fn_save_hierarchy" schema="target_data" src="functions/fn_save_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_save_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_hierarchy(integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_hierarchy(_areal_or_pop integer, _superior_cat integer[], _inferior_cat integer[])
RETURNS TABLE (
superior_cat		integer,
inferior_cat		integer
)
AS
$$
DECLARE
_test			integer[];
_superior_cat_test	integer[];
_inferior_cat_test	integer[];
BEGIN
		-- reorder just for equality test
		SELECT array_agg(t1.sup ORDER BY t1.sup) AS test
		FROM unnest(_superior_cat) AS t1(sup)
		INTO _superior_cat_test;

		SELECT array_agg(t1.inf ORDER BY t1.inf) AS test
		FROM unnest(_inferior_cat) AS t1(inf)
		INTO _inferior_cat_test;

	CASE WHEN _areal_or_pop = 100 THEN

		SELECT array_agg(t1.sup ORDER BY t1.sup) AS test
		FROM unnest(_superior_cat) AS t1(sup)
		INNER JOIN target_data.c_area_domain_category AS t2
		ON t1.sup = t2.id
		INTO _test;

		IF _test != _superior_cat_test
		THEN RAISE EXCEPTION 'Given array of superior categories containes identificators which are not present in c_area_domain_category.';
		END IF;

		SELECT array_agg(t1.inf) AS test
		FROM unnest(_inferior_cat) AS t1(inf)
		INNER JOIN target_data.c_area_domain_category AS t2
		ON t1.inf = t2.id
		INTO _test;

		IF _test != _inferior_cat_test
		THEN RAISE EXCEPTION 'Given array of inferior categories containes identificators which are not present in c_area_domain_category.';
		END IF;

		RETURN QUERY
		INSERT INTO target_data.t_adc_hierarchy (variable_superior, variable)
		SELECT t1.sup, t2.inf
		FROM
			unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
		INNER JOIN
			unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
		ON t1.id = t2.id
		RETURNING variable_superior, variable;

	WHEN _areal_or_pop = 200 THEN

		SELECT array_agg(t1.sup) AS test
		FROM unnest(_superior_cat) AS t1(sup)
		INNER JOIN target_data.c_sub_population_category AS t2
		ON t1.sup = t2.id
		INTO _test;

		IF _test != _superior_cat_test
		THEN RAISE EXCEPTION 'Given array of superior categories containes identificators which are not present in c_sub_population_category.';
		END IF;

		SELECT array_agg(t1.inf) AS test
		FROM unnest(_inferior_cat) AS t1(inf)
		INNER JOIN target_data.c_sub_population_category AS t2
		ON t1.inf = t2.id
		INTO _test;

		IF _test != _inferior_cat_test
		THEN RAISE EXCEPTION 'Given array of inferior categories containes identificators which are not present in c_sub_population_category.';
		END IF;

		RETURN QUERY
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable)
		SELECT t1.sup, t2.inf
		FROM
			unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
		INNER JOIN
			unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
		ON t1.id = t2.id
		RETURNING variable_superior, variable;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_hierarchy(integer, integer[], integer[]) IS
'Function provides insert into table t_adc_hierarchy/t_spc_hierarchy.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_hierarchy(integer, integer[], integer[]) TO public;

-- </function>

-- <function name="fn_check_classification_rules" schema="target_data" src="functions/fn_check_classification_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rules(_ldsity integer, _ldsity_object integer, _rules text[], _panel_refyearset integer DEFAULT NULL::integer, _adc integer[] DEFAULT NULL::integer[], _spc integer[] DEFAULT NULL::integer[])
--boolean
RETURNS TABLE (
result		boolean,
message_id	integer,
message		text
) 
AS
$$
DECLARE
_ldsity_objects		integer[];
_adc_rule_test		integer[];
_spc_rule_test		integer[];
_table_name4rule	varchar;
_rule			text;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_cases			varchar[];
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_q_all			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
_exist_test		boolean;
_exist_test_all		boolean[];
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	IF NOT array[_ldsity_object] <@ (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]))
	THEN
		RAISE EXCEPTION 'Given ldsity object (%) is not found within the hierarchy of ldsity contribution (%).', _ldsity_object, _ldsity;
	END IF;

	FOREACH _rule IN ARRAY _rules
	LOOP
		SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
		INTO _test;

		IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
		THEN
			_exist_test := true; _rule = true;
		ELSE 	_exist_test := false;
		END IF;

		_exist_test_all := _exist_test_all || _exist_test;

		_test_all := _test_all || array[_test];

		_case := 'CASE WHEN '||_rule||' THEN true ELSE false END';

		_cases := _cases || _case;
		--raise notice '%', _test_all;
	END LOOP;

	IF _test_all @> array[false]
	THEN
		RAISE EXCEPTION 'One or more given rules has an invalid syntax.';
	END IF;

	IF _exist_test_all @> array[true] AND _exist_test_all @> array[false]
	THEN
		RAISE EXCEPTION 'In array of given rules there is an EXISTS or NOT EXISTS rule but there are also another rules, this rules have to be alone within one sub population.';
	END IF;

	IF _exist_test_all @> array[true] AND _exist_test_all != ARRAY[true,true]
	THEN
		RAISE EXCEPTION 'The given array of rules has more or less then 2 rules (%). If this is a special case of EXISTS rule, then its counterpart NOT EXISTS has to be entered and nothing else.',_rules;
	END IF;

	_ldsity_objects := (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]));

	WITH w AS (
		SELECT _ldsity_objects AS ldsity_objects
	)
	SELECT array_agg(t1.rule ORDER BY t1.id) 
	FROM unnest(_adc) WITH ORDINALITY AS t1(rule, id)
	INNER JOIN target_data.cm_adc2classification_rule AS t2
	ON t1.rule = t2.id
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	INNER JOIN w AS t4
	ON ARRAY[t3.id] <@ t4.ldsity_objects
	INTO _adc_rule_test;

	IF _adc_rule_test != _adc
	THEN
		RAISE EXCEPTION 'Given area domain category classification rules does not meet the ldsity objects within hierarchy of local density contribution. Rules which can be used = %, rules given = %.', _adc_rule_test, _adc;
	END IF;

	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	)
	SELECT array_agg(t1.rule ORDER BY t1.id) 
	FROM unnest(_spc) WITH ORDINALITY AS t1(rule, id)
	INNER JOIN target_data.cm_spc2classification_rule AS t2
	ON t1.rule = t2.id
	INNER JOIN target_data.c_ldsity_object AS t3
	ON t2.ldsity_object = t3.id
	INNER JOIN w AS t4
	ON ARRAY[t3.id] <@ t4.ldsity_objects
	INTO _spc_rule_test;

	IF _spc_rule_test != _spc
	THEN
		RAISE EXCEPTION 'Given sub population category classification rules does not meet the ldsity objects within hierarchy of local density contribution. Rules which can be used = %, rules given = %.', _spc_rule_test, _spc;
	END IF;

-- panels and reference year sets must be specified before the check

-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			information_schema.constraint_column_usage AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	_table_name4rule := (SELECT table_name FROM target_data.c_ldsity_object WHERE id = _ldsity_object);

	FOR i IN 1..array_length(_cases,1)
	LOOP
		WITH w_tables AS (
			SELECT
				t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
					CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
					CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',i, ' AS rule_number, ', _cases[i],' AS rul ') ELSE '' END,
					' FROM ', t1.table_name, 
					CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
					') AS ', _table_names_ws[t1.id]) AS table_select
			FROM
				unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
			LEFT JOIN
				(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
				FROM
					(SELECT t4.table_name, t3.classification_rule 
					FROM target_data.c_ldsity AS t1,
						unnest(t1.area_domain_category || _adc) WITH ORDINALITY AS t2(rule, id)
					LEFT JOIN target_data.cm_adc2classification_rule AS t3
					ON t2.rule = t3.id
					INNER JOIN target_data.c_ldsity_object AS t4
					ON t3.ldsity_object = t4.id
					WHERE t1.id = _ldsity
					UNION ALL
					SELECT t4.table_name, t3.classification_rule 
					FROM target_data.c_ldsity AS t1,
						unnest(t1.sub_population_category || _spc) WITH ORDINALITY AS t2(rule, id)
					LEFT JOIN target_data.cm_spc2classification_rule AS t3
					ON t2.rule = t3.id
					INNER JOIN target_data.c_ldsity_object AS t4
					ON t3.ldsity_object = t4.id
					WHERE t1.id = _ldsity
					) AS t1
				GROUP BY table_name
				) AS t2
			ON t1.table_name = t2.table_name
		)
		SELECT array_agg(table_select ORDER BY id)
		FROM w_tables
		INTO _table_selects;

		SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
			_table_names_ws[1], '.', _primary_keys[1], '
			FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
			INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
			INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
			INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
			_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
		INTO _table1;

		_join_all := NULL; _join := NULL;

		FOR i IN 2..array_length(_table_selects,1) 
		LOOP
			SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
			INTO _join;

			_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
			--raise notice '%', _join_all;
		END LOOP;

		--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
		_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

		_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
--raise notice '%', _table1;
		_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL ' ELSE '' END), _q);

	END LOOP;

--	raise notice '%',_q_all;

	EXECUTE
	'WITH w AS (' || _q_all || '),
	w2 AS (SELECT
			id, rul, sum(CASE WHEN rul=true THEN 1 ELSE 0 END) AS no_of_rules_met
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
		GROUP BY id, rul
	), w_test AS (
		SELECT no_of_rules_met::int, count(*)::int AS no_of_objects
		FROM w2
		GROUP BY no_of_rules_met
	)
	SELECT
		array_agg(no_of_rules_met ORDER BY no_of_rules_met) AS no_of_rules_met,
		array_agg(no_of_objects ORDER BY no_of_rules_met) AS no_of_objects
	FROM
		w_test
	'
	INTO _no_of_rules_met, _no_of_objects;

--raise notice '%, %', _no_of_rules_met, _no_of_objects;

	_total := (SELECT sum(nob) FROM unnest(_no_of_objects) AS t(nob));

--return query select true, _no_of_rules_met, _no_of_objects;

	-- if both are NULL 
IF _no_of_rules_met IS NULL AND _no_of_objects IS NULL
THEN	
	RETURN QUERY 
		SELECT false, 0, concat('There were no records on which the rules could be tested - given combination of panels and reference year sets probably does not correspond with the local density contribution availability.')::text;
ELSE
-- if one rule given - all records have 1 rule met
	IF array_length(_rules,1) = 1
	THEN
		IF array_length(_no_of_rules_met,1) != 1
		THEN
			RETURN QUERY 
			SELECT false, 1, concat('Only one rule was given, so all records has to result into true. There were only ', _no_of_objects[2], ' number of objects successfully categorized from ', _total, ' overall total.')::text;
		ELSE
			IF _no_of_rules_met[1] = 0
			THEN
				RETURN QUERY
				SELECT false, 2, concat('Only one rule was given, and none of the ', _no_of_objects[1], ' number of objects was successfully categorized.')::text;
			ELSE
			--raise notice '%, %', _no_of_rules_met, _no_of_objects;
				RETURN QUERY 
				SELECT true, 3, concat('Only one rule was given and every object was successfully categorized by it. All from ', _no_of_objects[1], ' number of objects resulted in ', _total, ' successful records.')::text;
			END IF;
		END IF;
	END IF;


-- if 2 or more rules given - all records have 1 rule met and 0 rule met
	-- both numbers must be the same (logically if 1 object belogns to some rule, basically also fails to the other rules)

	IF array_length(_rules,1) > 1
	THEN
		-- there are objects with 2 or more rules met (intersection between rules, can be from 0,1,2 or 1,2)
		IF EXISTS(SELECT * FROM unnest(_no_of_rules_met) AS t(nor) WHERE nor >= 2)
		THEN
			IF _exist_test_all = array[true, true]
			THEN
				RETURN QUERY
				SELECT true, 8, concat('Special case of EXISTS rules, implicitly true for all records.')::text;
			ELSE
				RETURN QUERY 
				SELECT false, 4, concat('Some of the objects were categorized by more than one rule, there is an intersection between the rule conditions.')::text;
			END IF;
		ELSE
			-- there are objects which are not covered by rules (only 0,1)
			IF array_length(_no_of_rules_met,1) = 2
			THEN
				IF _no_of_objects[1] != _no_of_objects[2]
				THEN
					RETURN QUERY 
					SELECT false, 5, concat('Some of the objects (', _no_of_objects[1] - _no_of_objects[2], ' from total of ', _no_of_objects[1], ') were not successfully categorized by any of the rules. Consider editing hierarchy.')::text;
				ELSE
					-- the rest, where 0,1, both the same number of objects
					IF _no_of_objects[1] = _no_of_objects[2]
					THEN
						RETURN QUERY
						SELECT true, 6, concat('Every object was succesfully categorized without any intersection.')::text;
					ELSE
						--raise notice '%, %', _no_of_rules_met, _no_of_objects;
						RAISE EXCEPTION '01: Not known state of classification rules.';
					END IF;
				END IF;
			ELSE
				IF array_length(_no_of_rules_met,1) = 1 AND _no_of_rules_met[1] = 0
				THEN
					RETURN QUERY 
					SELECT false, 7, concat('None of the ', _no_of_objects[1], ' was successfully categorized by any of the rules.')::text;
				ELSE
					RAISE EXCEPTION '02: Not known state of classification rules.';
				END IF;
			END IF;
		END IF;
	END IF;
END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[], integer[]) IS
'Function checks syntax in text field with classification rules for given ldsity contribution and ldsity object within its hierarchy. Optionally also for given panel x reference year set combination and/or  area domain/sub population category/ies.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[],  integer[]) TO public;

-- </function>
