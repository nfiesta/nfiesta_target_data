--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------
-- Functions
---------------------

-- <function name="fn_get_refyearset2panel_mapping4group" schema="target_data" src="functions/fn_get_refyearset2panel_mapping4group.sql">
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_refyearset2panel_mapping4group 
--------------------------------------------------------------------------------
-- ALTER EXTENSION nfiesta_target_data DROP FUNCTION target_data.fn_get_refyearset2panel_mapping4group(INT);
-- DROP FUNCTION IF EXISTS target_data.fn_get_refyearset2panel_mapping4group(INT);

CREATE OR REPLACE FUNCTION 
target_data.fn_get_refyearset2panel_mapping4group(IN _panel_refyearset_group INT)
RETURNS INT [] AS 
$$
DECLARE 

_refyearset2panel INT[];

BEGIN
	
	-- raising an exception on NULL input
	IF _panel_refyearset_group IS NULL then 
		RAISE EXCEPTION 'Input parameter _panel_refyearset_group INT not given, but it is required!';
	END IF; 
	
	SELECT 
		array_agg(t3.id) AS _refyearset2panel
	FROM 
		target_data.c_panel_refyearset_group AS t1
	INNER JOIN
		target_data.t_panel_refyearset_group AS t2
	ON
		t1.id = _panel_refyearset_group AND
		t2.panel_refyearset_group = t1.id
	INNER JOIN
		sdesign.cm_refyearset2panel_mapping AS t3
	ON
		t2.refyearset2panel = t3.id
	INTO _refyearset2panel;

RETURN _refyearset2panel;

END
$$ 
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER; 

COMMENT ON FUNCTION target_data.fn_get_refyearset2panel_mapping4group(INT) IS 
'Function returns an array of panel and reference year-set combinations (sdesign.cm_refyearset2panel_mapping) belonging to a '
' particular group (sdesign.c_panel_refyearset_group).';

GRANT EXECUTE ON FUNCTION target_data.fn_get_refyearset2panel_mapping4group(INT) TO public;

/*
-- tests
-- checking if the function returns some combinations of panels and reference yearsets   
SELECT * FROM target_data.fn_get_refyearset2panel_mapping4group(9);
SELECT * FROM target_data.fn_get_refyearset2panel_mapping4group(13);

 -- checking if the function raises an exception for NULL input
SELECT * FROM target_data.fn_get_refyearset2panel_mapping4group(NULL);
*/
-- </function>