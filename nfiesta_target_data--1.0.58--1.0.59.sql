--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------
-- Functions
---------------------

-- <function name="fn_save_target_variable" schema="target_data" src="functions/fn_save_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_target_variable
(
		_label character varying(200),
		_description text,
		_label_en character varying(200) DEFAULT NULL::varchar,
		_description_en text DEFAULT NULL::text,
		_ldsity integer[] DEFAULT NULL::integer[],
		_ldsity_object_type integer[] DEFAULT NULL::integer[],	-- id from table c_ldsity_object_type, one 100 can be partitioned by one or several 200s
		_version integer[] DEFAULT NULL::integer[],		-- id from table c_version
		_id integer[] DEFAULT NULL::integer[])			-- one or more ids of existing records which should be updated (must have the same ldsitys of 100)
RETURNS integer[]
AS
$$
DECLARE
	_target_variable 	integer;
	_test			boolean;
	_total			integer;
	_total_100	 	integer;
	_total_200	 	integer;
	_target_variables	integer[];
	_mtext			text;
	_mcont			text;
BEGIN
	IF _id IS NULL
	THEN
		IF _label IS NOT NULL AND _description IS NOT NULL AND _ldsity IS NOT NULL AND _version IS NOT NULL
		THEN
			IF _ldsity IS NULL OR array_length(_ldsity,1) = 0
			THEN RAISE EXCEPTION 'Given array of ldsity contributions is null.';
			END IF;

			IF (SELECT bool_or(ldsity IS NULL) FROM unnest(_ldsity) t(ldsity)) = true
			THEN RAISE EXCEPTION 'Given array of ldsity contributions contains null values.';
			END IF;

			IF array_length(_ldsity,1) != array_length(_ldsity_object_type,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity, _ldsity_object_type;
			END IF;

			IF array_length(_ldsity,1) != array_length(_version,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of versions (%) are not equal!', _ldsity, _version;
			END IF;

			WITH w AS(
				SELECT array_agg(obj_type) AS obj_types
				FROM unnest(_ldsity_object_type) AS t1(obj_type)
				INNER JOIN target_data.c_ldsity_object_type AS t2
				ON t1.obj_type = t2.id
			)
			SELECT array_length(obj_types,1) = array_length(_ldsity_object_type,1)
			FROM w
			INTO _test;

			IF _test = false
			THEN RAISE EXCEPTION 'Given array of ldsity_object_types contains not known values.';
			END IF;

			-- check if correct combination of ldsity_object_types is choosen
			WITH w AS (
				SELECT obj_type, count(*) AS total
				FROM unnest(_ldsity_object_type) AS t(obj_type)
				GROUP BY obj_type
			)
			SELECT total
			FROM w AS t1
			WHERE obj_type = 100
			INTO _total_100;

			WITH w AS (
				SELECT obj_type, count(*) AS total
				FROM unnest(_ldsity_object_type) AS t(obj_type)
				GROUP BY obj_type
			)
			SELECT total
			FROM w AS t1
			WHERE obj_type = 200
			INTO _total_200;

			IF (_total_200 IS NOT NULL AND _total_200 > 0) AND (_total_100 IS NULL OR _total_100 = 0)
			THEN 
				RAISE EXCEPTION 'If some of the ldsity objects are types of 200 then there has to be one ldsity object of type 100 but none was given.';
			END IF;

			IF (_total_200 IS NOT NULL AND _total_200 > 0) AND (_total_100 > 1)
			THEN 
				RAISE EXCEPTION 'If some of the ldsity objects are types of 200 then there has to be one ldsity object of type 100 but more than one was given.';
			END IF;

			-- only one areal ldsity can be divided by 200
			IF _total_100 = 1 AND _total_200 > 0 AND (
					SELECT t2.areal_or_population
					FROM target_data.c_ldsity AS t1
					INNER JOIN target_data.c_ldsity_object AS t2
					ON t1.ldsity_object = t2.id 
					WHERE t1.id IN
						(SELECT
							ldsity --, ldsity_object_type
						FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
						INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
						ON t1.id = t2.id
						WHERE t2.ldsity_object_type = 100)

					) = 200
			THEN
				RAISE EXCEPTION 'Only areal ldsitys can be divided by ldsity_object_types 200.';
			END IF;

			IF _total_100 = 1 AND _total_200 > 0 AND (
					SELECT total
					FROM
						(SELECT count(*) AS total, t2.areal_or_population 
						FROM target_data.c_ldsity AS t1
						INNER JOIN target_data.c_ldsity_object AS t2
						ON t1.ldsity_object = t2.id 
						WHERE t1.id IN
							(SELECT ldsity --, ldsity_object_type
							FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
							INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
							ON t1.id = t2.id
							WHERE t2.ldsity_object_type = 200)
						GROUP BY t2.areal_or_population
						) AS t1
					WHERE areal_or_population = 100
					) > 0
			THEN
				RAISE EXCEPTION 'Only subpopulational ldsitys can divide areal ldsity of ldsity_object_type 100.';
			END IF;

			BEGIN
				INSERT INTO target_data.c_target_variable(label, description, label_en, description_en, state_or_change)
				SELECT _label, _description, _label_en, _description_en, 100
				RETURNING id
				INTO _target_variable;

				INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category, version, use_negative)
				SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], t3.version, false
				FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
				INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
				ON t1.id = t2.id
				INNER JOIN unnest(_version) WITH ORDINALITY AS t3(version, id)
				ON t1.id = t3.id;

				EXCEPTION WHEN OTHERS THEN
				GET STACKED DIAGNOSTICS _mtext := MESSAGE_TEXT, _mcont := PG_EXCEPTION_CONTEXT;

				PERFORM pg_catalog.setval('target_data.cm_ldsity2target_variable_id_seq', (SELECT max(id) FROM target_data.cm_ldsity2target_variable), true);
				DELETE FROM target_data.c_target_variable WHERE id = _target_variable;
				PERFORM pg_catalog.setval('target_data.c_target_variable_id_seq', (SELECT max(id) FROM target_data.c_target_variable), true);
				RAISE EXCEPTION '%
				CONTEXT: %', _mtext, _mcont;
			END;

			_target_variables := array[_target_variable];
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%) and local density contributions (%) must be not null!', _label, _description, _ldsity;
		END IF;
	ELSE
		WITH w AS (
			SELECT 	DISTINCT t1.id AS target_variable, t3.id AS ldsity, 
				t2.area_domain_category, t2.sub_population_category, t2.use_negative
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _id AND t4.id = 100
		), w_agg_target_variable AS (
			SELECT w.target_variable, w.area_domain_category, w.sub_population_category,
				array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) AS ldsity
			FROM w
			GROUP BY w.target_variable, w.area_domain_category, w.sub_population_category
		)
		SELECT count(DISTINCT ldsity)
		FROM w_agg_target_variable 
		INTO _total;

		IF _total > 1
		THEN
			RAISE EXCEPTION 'Given target variables (_id = %) does not have the same ldsitys of ldsity object type 100. There is either different ldsity, or the same ldsity but somehow constrained by area domain category or sub population category.', _id;
		END IF;	

		-- test on the same label and description set
		WITH w AS (
			SELECT 	DISTINCT t3.id AS ldsity, 
				t2.area_domain_category, t2.sub_population_category, t2.use_negative
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _id AND t4.id = 100
		), w_agg_target_variable AS (
			SELECT --w.target_variable,
				w.area_domain_category, w.sub_population_category,
				array_agg(w.ldsity ORDER BY w.ldsity) FILTER (WHERE use_negative = false) AS ldsity_pozit,
				array_agg(w.ldsity ORDER BY w.ldsity) FILTER (WHERE use_negative = true) AS ldsity_negat
			FROM w
			GROUP BY --w.target_variable, 
				w.area_domain_category, w.sub_population_category
		),
		w_all AS (
			SELECT 	t1.id AS target_variable, 
				array_agg(t3.id ORDER BY t3.id) FILTER (WHERE use_negative = false) AS ldsity_pozit, 
				array_agg(t3.id ORDER BY t3.id) FILTER (WHERE use_negative = true) AS ldsity_negat, 
				t2.area_domain_category, t2.sub_population_category
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE t4.id = 100
			GROUP BY t1.id, t2.area_domain_category, t2.sub_population_category
		), w_tv AS (
			SELECT t1.target_variable
			FROM w_all AS t1
			INNER JOIN w_agg_target_variable AS t2
			ON 	coalesce(t1.area_domain_category,array[0]) = coalesce(t2.area_domain_category,array[0]) AND 
				coalesce(t1.sub_population_category,array[0]) = coalesce(t2.sub_population_category,array[0]) AND
				coalesce(t1.ldsity_pozit,array[0]) = coalesce(t2.ldsity_pozit,array[0]) AND 
				coalesce(t1.ldsity_negat,array[0]) = coalesce(t2.ldsity_negat,array[0])
		)
		SELECT array_agg(target_variable)
		FROM w_tv
		INTO _target_variables;

		IF _target_variables != _id
		THEN
			RAISE EXCEPTION 'Given array of target_variables (%) is not compatible with the array of target_variables with the same ldsitys (%). It is either incomplete or containes target variables with different ldsitys.', _id, _target_variables;
		END IF;

		IF _ldsity IS NULL
		THEN
			UPDATE target_data.c_target_variable
			SET	label = _label,
				description = _description, 
				label_en = _label_en,
				description_en = _description_en
			WHERE array[c_target_variable.id] <@ _id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing target variable and create new one with given ldsitys (%).', _ldsity;
		END IF;
	END IF;

	RETURN _target_variables;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[]) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[]) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) CASCADE;
-- <function name="fn_save_change_target_variable" schema="target_data" src="functions/fn_save_change_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_change_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_change_target_variable(
	_label character varying(200), 
	_description text, 
	_label_en character varying(200) DEFAULT NULL::varchar, 
	_description_en text DEFAULT NULL::text, 
	_ldsity integer[] DEFAULT NULL::integer[], 
	_ldsity_object_type integer[] DEFAULT NULL::integer[], 
	_version integer[] DEFAULT NULL::integer[], 
	_ldsity_negat integer[] DEFAULT NULL::integer[], 
	_ldsity_object_type_negat integer[] DEFAULT NULL::integer[], 
	_version_negat integer[] DEFAULT NULL::integer[], 
	_id integer DEFAULT NULL::integer)
RETURNS integer[]
AS
$$
DECLARE
	_target_variable integer;
	_target_variables integer[];
BEGIN
	IF _id IS NULL
	THEN
		IF _label IS NOT NULL AND _description IS NOT NULL AND (_ldsity IS NOT NULL OR _ldsity_negat IS NOT NULL)
		THEN
			IF array_length(_ldsity,1) != array_length(_ldsity_object_type,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity, _ldsity_object_type;
			END IF;

			IF array_length(_ldsity,1) != array_length(_version,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions (%) and corresponding assignment of versions (%) are not equal!', _ldsity, _version;
			END IF;

			IF array_length(_ldsity_negat,1) != array_length(_ldsity_object_type_negat,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions with negative value (%) and corresponding assignment of ldsity object types (%) are not equal!', _ldsity_negat, _ldsity_object_type_negat;
			END IF;

			IF array_length(_ldsity_negat,1) != array_length(_version_negat,1)
			THEN
				RAISE EXCEPTION 'Given arrays of ldsity contributions with negative value (%) and corresponding assignment of versions (%) are not equal!', _ldsity_negat, _version_negat;
			END IF;

			INSERT INTO target_data.c_target_variable(label, description, label_en, description_en, state_or_change)
			SELECT _label, _description, _label_en, _description_en, 200
			RETURNING id
			INTO _target_variable;

			INSERT INTO target_data.cm_ldsity2target_variable(target_variable, ldsity, ldsity_object_type, area_domain_category, sub_population_category, version, use_negative)
			(SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], t3.version, false
			FROM unnest(_ldsity) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type) WITH ORDINALITY AS t2(ldsity_object_type, id)
			ON t1.id = t2.id
			INNER JOIN unnest(_version) WITH ORDINALITY AS t3(version, id)
			ON t1.id = t3.id
			UNION ALL
			SELECT _target_variable, ldsity, ldsity_object_type, NULL::int[], NULL::int[], t3.version, true
			FROM unnest(_ldsity_negat) WITH ORDINALITY AS t1(ldsity, id)
			INNER JOIN unnest(_ldsity_object_type_negat) WITH ORDINALITY AS t2(ldsity_object_type, id)
			ON t1.id = t2.id
			INNER JOIN unnest(_version_negat) WITH ORDINALITY AS t3(version, id)
			ON t1.id = t3.id);
	
			_target_variables := array[_target_variable];
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%), local density contributions of pozitive (%) or negative (%) value must be not null!', _label, _description, _ldsity, _ldsity_negat;
		END IF;
	ELSE
		-- test on the same label and description set
		WITH w AS (
			SELECT 	DISTINCT t3.id AS ldsity, 
				t2.area_domain_category, t2.sub_population_category, t2.use_negative
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _id AND t4.id = 100
		), w_agg_target_variable AS (
			SELECT --w.target_variable,
				w.area_domain_category, w.sub_population_category,
				array_agg(w.ldsity ORDER BY w.ldsity) FILTER (WHERE use_negative = false) AS ldsity_pozit,
				array_agg(w.ldsity ORDER BY w.ldsity) FILTER (WHERE use_negative = true) AS ldsity_negat
			FROM w
			GROUP BY --w.target_variable, 
				w.area_domain_category, w.sub_population_category
		),
		w_all AS (
			SELECT 	t1.id AS target_variable, 
				array_agg(t3.id ORDER BY t3.id) FILTER (WHERE use_negative = false) AS ldsity_pozit, 
				array_agg(t3.id ORDER BY t3.id) FILTER (WHERE use_negative = true) AS ldsity_negat, 
				t2.area_domain_category, t2.sub_population_category
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE t4.id = 100
			GROUP BY t1.id, t2.area_domain_category, t2.sub_population_category
		), w_tv AS (
			SELECT t1.target_variable
			FROM w_all AS t1
			INNER JOIN w_agg_target_variable AS t2
			ON 	coalesce(t1.area_domain_category,array[0]) = coalesce(t2.area_domain_category,array[0]) AND 
				coalesce(t1.sub_population_category,array[0]) = coalesce(t2.sub_population_category,array[0]) AND
				coalesce(t1.ldsity_pozit,array[0]) = coalesce(t2.ldsity_pozit,array[0]) AND 
				coalesce(t1.ldsity_negat,array[0]) = coalesce(t2.ldsity_negat,array[0])
		)
		SELECT array_agg(target_variable)
		FROM w_tv
		INTO _target_variables;

		IF _target_variables != _id
		THEN
			RAISE EXCEPTION 'Given array of target_variables (%) is not compatible with the array of target_variables with the same ldsitys (%). It is either incomplete or containes target variables with different ldsitys.', _id, _target_variables;
		END IF;

		IF _ldsity IS NULL
		THEN
			UPDATE target_data.c_target_variable
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_target_variable.id = _id;

		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing target variable and create new one with given ldsitys (%).', _ldsity;
		END IF;
	END IF;

	RETURN _target_variables;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) IS
'Functions inserts records into tables c_target_variable and cm_ldsity2target_variable for given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_change_target_variable(character varying(200), text, character varying(200), text, integer[], integer[], integer[], integer[], integer[], integer[], integer) TO public;

-- </function>

-- <function name="fn_save_ldsity_object" schema="target_data" src="functions/fn_save_ldsity_object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_ldsity_object(integer, character varying, text, character varying, text, integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_ldsity_object(_id integer, _label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text)
RETURNS integer
AS
$$
BEGIN
	IF _id IS NOT NULL 
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _id;
		END IF;

		IF _label IS NOT NULL OR _description IS NOT NULL
		THEN
			UPDATE target_data.c_ldsity_object
			SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
			WHERE c_ldsity_object.id = _id;
		ELSE
			RAISE EXCEPTION 'At least label or description must be not null when doing an update of c_ldsity_object (%, %).', _label, _description;
		END IF;
	ELSE
		RAISE EXCEPTION 'Parameter _id must not be null (%)!', _id;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_ldsity_object(integer, character varying, text, character varying, text) IS
'Function provides update in c_ldsity_object table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_ldsity_object(integer, character varying, text, character varying, text) TO public;

-- </function>

-- <function name="fn_save_ldsity" schema="target_data" src="functions/fn_save_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_ldsity(integer, character varying, text, character varying, text, integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_ldsity(_id integer, _label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text)
RETURNS integer
AS
$$
BEGIN
	IF _id IS NOT NULL 
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _id;
		END IF;

		IF _label IS NOT NULL OR _description IS NOT NULL
		THEN
			UPDATE target_data.c_ldsity
			SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
			WHERE c_ldsity.id = _id;
		ELSE
			RAISE EXCEPTION 'At least label or description must be not null when doing an update of c_ldsity (%, %).', _label, _description;
		END IF;
	ELSE
		RAISE EXCEPTION 'Parameter _id must not be null (%)!', _id;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_ldsity(integer, character varying, text, character varying, text) IS
'Function provides update in c_ldsity table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_ldsity(integer, character varying, text, character varying, text) TO public;

-- </function>

-- <function name="fn_get_ldsity" schema="target_data" src="functions/fn_get_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_ldsity(integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity(_target_variable integer[] DEFAULT NULL::integer[], _ldsity_object_type integer DEFAULT 100::integer)
RETURNS TABLE (
id			integer,
target_variable		integer,
label			character varying(200),
label_en		character varying(200),
column_expression	text,
unit_of_measure		integer,
ldsity_object		integer,
ldsity_object_type	integer,
object_type_label	character varying(200),
object_type_desc	text,
use_negative		boolean,
version			integer
)
AS
$$
DECLARE
_test_pozit		integer;
_test_negat		integer;
BEGIN
	IF _target_variable IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS target_variable,
			t1.label, t1.label_en, t1.column_expression, t1.ldsity_object, t1.unit_of_measure,
			NULL::int, NULL::varchar(200), NULL::text, NULL::boolean, NULL::integer
		FROM target_data.c_ldsity AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable[1])
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_target_variable (%)', _target_variable;
		END IF;

		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_type AS t1 WHERE t1.id = _ldsity_object_type)
		THEN RAISE EXCEPTION 'Given ldsity_object_type does not exist in table c_ldsity_object_type (%)', _ldsity_object_type;
		END IF;

		-- test if all given target_variables has the same ldsitys 100
		WITH w AS (
			SELECT 	DISTINCT t1.id AS target_variable, t3.id AS ldsity, 
				t2.area_domain_category, t2.sub_population_category, t2.use_negative
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _target_variable AND t4.id = 100
		), w_agg_target_variable AS (
			SELECT w.target_variable, w.area_domain_category, w.sub_population_category,
				array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) FILTER (WHERE w.use_negative = false) AS ldsity_pozit,
				array_agg(w.ldsity ORDER BY w.target_variable, w.ldsity) FILTER (WHERE w.use_negative = true) AS ldsity_negat
			FROM w
			GROUP BY w.target_variable, w.area_domain_category, w.sub_population_category
		)
		SELECT count(DISTINCT ldsity_pozit), count(DISTINCT ldsity_negat)
		FROM w_agg_target_variable 
		INTO _test_pozit, _test_negat;

		IF _test_pozit > 1 OR _test_negat > 1
		THEN
			RAISE EXCEPTION 'Given target variables (in array) does not have the same ldsitys of ldsity object type 100 (pozitive or negative). There is either different ldsity, or the same ldsity but somehow constrained by area domain category or sub population category.';
		END IF;
	
		CASE WHEN _ldsity_object_type = 100
		THEN
			-- return distinct objects for given target variables
			RETURN QUERY
			WITH w AS (
				SELECT  t1.id AS target_variable, count(CASE WHEN t2.ldsity_object_type = 200 THEN 1 ELSE NULL END) AS ldsity_200 
				FROM target_data.c_target_variable AS t1
				INNER JOIN target_data.cm_ldsity2target_variable AS t2
				ON t1.id = t2.target_variable
				WHERE array[t1.id] <@ _target_variable
				GROUP BY t1.id
			)
			SELECT  t3.id, t1.id AS target_variable,
				t3.label, t3.label_en, t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
				t2.ldsity_object_type, t4.label, t4.description, t2.use_negative, t2.version
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			INNER JOIN w AS t5 ON t1.id = t5.target_variable
			WHERE t4.id = 100 AND CASE WHEN array_length(_target_variable,1) > 1 THEN t5.ldsity_200 = 0 ELSE true END
			ORDER BY t1.id, t3.id;

		WHEN _ldsity_object_type = 200
		THEN
			RETURN QUERY
			SELECT  t3.id AS ldsity, t1.id AS target_variable,
				t3.label, t3.label_en, t3.column_expression, t3.unit_of_measure, t3.ldsity_object, 
				t2.ldsity_object_type, t4.label, t4.description, t2.use_negative, t2.version
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			INNER JOIN target_data.c_ldsity_object_type AS t4
			ON t2.ldsity_object_type = t4.id
			WHERE array[t1.id] <@ _target_variable AND t4.id = 200
			ORDER BY t1.id, t3.id;
		ELSE
			RAISE EXCEPTION 'Not known value of ldsity object type (%).', _ldsity_object_type;
		END CASE;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity(integer[], integer) IS
'Function returns records from c_ldsity table, optionally for given target_variable.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity(integer[], integer) TO public;

-- </function>

-- <function name="fn_save_category" schema="target_data" src="functions/fn_save_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_category
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_category(integer, varchar, text, varchar, text, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_category(_areal_or_population integer, _label varchar(200), _description text, _label_en varchar(200), _description_en text, _parent integer DEFAULT NULL::integer, _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
BEGIN
	IF _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent identifier (%) or label/descriptioin is null (%, %)!', _parent, _label, _description;
	END IF; 

	IF _id IS NULL THEN

		IF _parent IS NULL
		THEN
			RAISE EXCEPTION 'Parameter of parent idenfier (id from c_area_domain/c_sub_population) is null!';
		END IF;

		CASE 
		WHEN _areal_or_population = 100 THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _parent)
			THEN RAISE EXCEPTION 'Given area_domain does not exist in table c_area_domain (%).', _parent;
			END IF;

			INSERT INTO target_data.c_area_domain_category(area_domain, label, description, label_en, description_en)
			SELECT 
				_parent,
				_label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _parent)
			THEN RAISE EXCEPTION 'Given sub_population does not exist in table c_sub_population (%).', _parent;
			END IF;

			INSERT INTO target_data.c_sub_population_category(sub_population, label, description, label_en, description_en)
			SELECT 
				_parent,
				_label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	ELSE
		CASE WHEN _areal_or_population = 100 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain_category AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain category does not exist in table c_area_domain_category (%)', _id;
			END IF;

			UPDATE target_data.c_area_domain_category
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_area_domain_category.id = _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_sub_population_domain_category AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain category does not exist in table c_area_domain_category (%)', _id;
			END IF;

			UPDATE target_data.c_sub_population_category
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_sub_population_category.id = _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_category(integer, varchar(200), text, varchar(200), text, integer, integer) IS
'Functions inserts a record into table c_area_domain_category or c_sub_population_category based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_category(integer, varchar(200), text, varchar(200), text, integer, integer) TO public;

-- </function>

-- <function name="fn_trg_check_cm_ldsity2target_variable" schema="target_data" src="functions/fn_trg_check_cm_ldsity2target_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_ldsity2target_variable
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() 
RETURNS TRIGGER AS 
$$
DECLARE
_test1		boolean;
_test2		boolean;
_test3		boolean;
_test4		boolean;
_variables 	integer[];
_use_negative	boolean[];
_100100		integer[];
_100200		integer[];
_200100		integer[];
_200200		integer[];
_total_eq	integer;
_id		integer;
BEGIN

	-- check if already exists
	WITH w_new AS (
		SELECT  t1.target_variable,
			array_agg(t1.ldsity ORDER BY ldsity, use_negative) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY ldsity, use_negative) AS ldsity_object_type, 
			array_agg(t1.use_negative ORDER BY ldsity, use_negative) AS use_negative, 
			array_agg(t1.version ORDER BY ldsity, use_negative) AS version,
			t1.area_domain_category AS adc,
			t1.sub_population_category AS spc
		FROM new_table AS t1
		GROUP BY target_variable, t1.area_domain_category, t1.sub_population_category
	), w_existing AS (
		SELECT  t1.target_variable,
			array_agg(t1.ldsity ORDER BY ldsity, use_negative) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY ldsity, use_negative) AS ldsity_object_type, 
			array_agg(t1.use_negative ORDER BY ldsity, use_negative) AS use_negative, 
			array_agg(t1.version ORDER BY ldsity, use_negative) AS version,
			t1.area_domain_category AS adc,
			t1.sub_population_category AS spc
		FROM (SELECT * FROM target_data.cm_ldsity2target_variable EXCEPT SELECT * FROM new_table) AS t1
		GROUP BY target_variable, t1.area_domain_category, t1.sub_population_category
	)
	, w_comp AS (
		SELECT t2.target_variable
		FROM w_new AS t1
		INNER JOIN w_existing AS t2
		ON 	coalesce(t1.ldsity,array[0]) = coalesce(t2.ldsity,array[0]) AND
		 	coalesce(t1.ldsity_object_type,array[0]) = coalesce(t2.ldsity_object_type,array[0]) AND
		 	coalesce(t1.use_negative,array[NULL::boolean]) = coalesce(t2.use_negative,array[NULL::boolean]) AND
		 	coalesce(t1.version,array[0]) = coalesce(t2.version,array[0]) AND
		 	coalesce(t1.adc,array[0]) = coalesce(t2.adc,array[0]) AND
		 	coalesce(t1.spc,array[0]) = coalesce(t2.spc,array[0])
	)
	SELECT count(*) FROM w_comp
	INTO _total_eq;

--raise notice '%', _total_eq;

	--_id := (SELECT max(id) 
	--	FROM 	(SELECT * FROM target_data.cm_ldsity2target_variable EXCEPT SELECT * FROM new_table) AS t1);

	IF _total_eq IS NOT NULL AND _total_eq > 0
	THEN 
		--PERFORM pg_catalog.setval('target_data.cm_ldsity2target_variable_id_seq', _id, true);
		RAISE EXCEPTION 'Some of inserted/udated target variables already exists in table target_data.cm_ldsity2target_variable. This means that given combination of ldsitys is already assigned to some target variable.';
	END IF;

	-- after insert on cm_ldsity2target_variable
	WITH w_new AS (
		SELECT 	
			t2.id, t2.label, t2.description, t2.label_en, t2.description_en,
			array_agg(t1.ldsity ORDER BY t1.ldsity) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY t1.ldsity) AS ldsity_object_type,
			array_agg(t1.use_negative ORDER BY t1.ldsity) AS use_negative
		FROM 
			new_table AS t1
		INNER JOIN
			target_data.c_target_variable AS t2
		ON t1.target_variable = t2.id
		WHERE t1.ldsity_object_type = 100
		GROUP BY t2.id, t2.label, t2.description, t2.label_en, t2.description
	),
	w_existing AS (
		SELECT 	
			t2.id, t2.label, t2.description, t2.label_en, t2.description_en,
			array_agg(t1.ldsity ORDER BY t1.ldsity) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY t1.ldsity) AS ldsity_object_type,
			array_agg(t1.use_negative ORDER BY t1.ldsity) AS use_negative
		FROM 
			(SELECT * FROM target_data.cm_ldsity2target_variable EXCEPT SELECT * FROM new_table) AS t1
		INNER JOIN
			target_data.c_target_variable AS t2
		ON t1.target_variable = t2.id
		WHERE t1.ldsity_object_type = 100
		GROUP BY t2.id, t2.label, t2.description, t2.label_en, t2.description
	)
	SELECT
		t1.label = t2.label AS label_test, t1.description = t2.description AS desc_test,
		t1.label_en = t2.label_en AS label_en_test, t1.description_en = t2.description_en AS desc_en_test
	FROM
		w_new AS t1
	INNER JOIN
		w_existing AS t2
	ON t1.ldsity = t2.ldsity AND t1.use_negative = t2.use_negative
	INTO _test1, _test2, _test3, _test4;

	-- if the join on the same ldsity with ldsity_object_type = 100 results in not null output
	-- it means, there already exists target variable with given 100 ldsitys (both constrained and non-constrained)

	IF _test1 IS NOT NULL
	THEN
		-- then we can test the equality
		IF _test1 = false OR _test2 = false OR _test3 = false OR _test4 = false
		THEN
			RAISE EXCEPTION 'Newly inserted target variable has to respect the label (%) and description (%), label_en (%) and description_en (%) respectively, of existing target variables with the same local density contributions (of ldsity_object_type = 100).', _test1, _test2, _test3, _test4;
		END IF;
	END IF; 

	-- check if correct combination of ldsity_object_types is choosen
	WITH 
	w_tv AS (SELECT distinct target_variable, use_negative
		FROM new_table
	),
	w_type AS (
		SELECT t1.target_variable, t1.ldsity_object_type, t3.areal_or_population, t1.use_negative, count(*) AS total
		FROM new_table AS t1
		INNER JOIN target_data.c_ldsity AS t2
		ON t1.ldsity = t2.id
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		GROUP BY t1.target_variable, t1.ldsity_object_type, t3.areal_or_population, t1.use_negative
	),
	w_total AS (
		SELECT 	t1.target_variable AS target_variable, t1.use_negative,
			coalesce(t2.total,0) AS total_100100, coalesce(t3.total,0) AS total_100200,
			coalesce(t4.total,0) AS total_200100, coalesce(t5.total,0) AS total_200200
		FROM
			w_tv AS t1
		LEFT JOIN w_type AS t2
		ON 	t1.target_variable = t2.target_variable AND t1.use_negative = t2.use_negative
			AND t2.ldsity_object_type = 100 AND t2.areal_or_population = 100
		LEFT JOIN w_type AS t3
		ON 	t1.target_variable = t3.target_variable AND t1.use_negative = t3.use_negative
			AND t3.ldsity_object_type = 100 AND t3.areal_or_population = 200
		LEFT JOIN w_type AS t4
		ON 	t1.target_variable = t4.target_variable AND t1.use_negative = t4.use_negative
			AND t4.ldsity_object_type = 200 AND t4.areal_or_population = 100
		LEFT JOIN w_type AS t5
		ON 	t1.target_variable = t5.target_variable AND t1.use_negative = t5.use_negative
			AND t5.ldsity_object_type = 200 AND t5.areal_or_population = 200
			  
	), w_result AS (
		SELECT
			target_variable, use_negative, total_100100, total_100200, total_200100, total_200200,
			CASE
			WHEN total_100100 = 1 AND total_100200 = 0 AND total_200100 = 0 AND total_200200 = 0 THEN true	-- only one areal
			WHEN total_100100 = 0 AND total_100200 >= 1 AND total_200100 = 0 AND total_200200 = 0 THEN true -- one or more populational
			WHEN total_100100 = 1 AND total_100200 = 0 AND total_200100 = 0 AND total_200200 >= 1 THEN true -- one areal divided by one or more populational
			ELSE false -- everything else does not make sense
			END AS validity	
		FROM w_total
	)
	SELECT 	
		array_agg(total_100100 ORDER BY target_variable, use_negative),
		array_agg(total_100200 ORDER BY target_variable, use_negative),
		array_agg(total_200100 ORDER BY target_variable, use_negative),
		array_agg(total_200200 ORDER BY target_variable, use_negative),
		array_agg(use_negative ORDER BY target_variable, use_negative),
		array_agg(target_variable ORDER BY target_variable, use_negative)
	FROM w_result
	WHERE validity = false
	INTO _100100,_100200,_200100,_200200,_use_negative,_variables;

--raise notice '%, %, %, %', _100100,_100200,_200100,_200200;

	IF _variables IS NOT NULL
	THEN
		RAISE EXCEPTION 'Some of the inserted target variables (%) (can be only pozitive or negative part of the variable) has violited ldsity_object_type and areal/population combination constraints. (%, %, %, %, %)', _variables, _100100, _100200, _200100, _200200, _use_negative;
	END IF;


	RETURN NULL;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() IS
'This trigger function controls the newly inserted records into c_target_variable table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() TO public;


-- </function>

