--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------
-- Functions
---------------------

-- <function name="fn_trg_check_cm_ldsity2target_variable_version" schema="target_data" src="functions/fn_trg_check_cm_ldsity2target_variable_version.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_ldsity2target_variable_version
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable_version();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable_version() 
RETURNS TRIGGER AS 
$$
DECLARE
		_state_or_change	integer;
		_check_version		integer;
BEGIN
		SELECT state_or_change FROM target_data.c_target_variable WHERE id = NEW.target_variable
		INTO _state_or_change;

		IF _state_or_change IS NULL
		THEN
			RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_ldsity2target_variable_version: For the newly inserted value of "target_variable = %" is not filled value of "state_or_change" in table c_target_variable!',NEW.target_variable;
		END IF;

		IF NOT(_state_or_change = ANY(ARRAY[100,200,300]))
		THEN
			RAISE EXCEPTION 'Error: 02: fn_trg_check_cm_ldsity2target_variable_version: For the newly inserted value of "target_variable = %" must exists value of "state_or_change = any(array[100,200,300])"!',NEW.target_variable;
		END IF;

		-------------------------------------------------------------
		-- STATE --
		IF _state_or_change = 100
		THEN
			IF NEW.version IS NULL
			THEN
				RAISE EXCEPTION 'Error: 03: fn_trg_check_cm_ldsity2target_variable_version: If target variable is type "state" than value of "version" must not be NULL!';
			END IF;

			with
			w1 as	(
					SELECT array_agg(cmlv.version) as version
					FROM target_data.cm_ldsity2target_variable as cmlv
					WHERE cmlv.target_variable = NEW.target_variable
					)
			,w2 as	(SELECT (w1.version || NEW.version) as version FROM w1)
			,w3 as	(select distinct t.version FROM (SELECT unnest(w2.version) as version FROM w2) as t)
			SELECT count(w3.version) FROM w3
			INTO _check_version;

			IF _check_version IS DISTINCT FROM 1
			THEN
				RAISE EXCEPTION 'Error: 04: fn_trg_check_cm_ldsity2target_variable_version: If target variable is type "state" than all values of "version" for all ldsity2target_variable must be the same!';
			END IF;
		END IF;
		-------------------------------------------------------------
		-- change --
		IF _state_or_change = 200 AND NEW.version IS NULL
		THEN
			RAISE EXCEPTION 'Error: 05: fn_trg_check_cm_ldsity2target_variable_version: If target variable is type "change" than value of "version" must not be NULL!';
		END IF;
		-------------------------------------------------------------
		-- dynamic
		IF _state_or_change = 300 AND NEW.version IS NOT NULL
		THEN
			RAISE EXCEPTION 'Error: 06: fn_trg_check_cm_ldsity2target_variable_version: If target variable is type "dynamic" than value of "version" must be NULL!';
		END IF;
		-------------------------------------------------------------
	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable_version() IS
'This trigger function controls that value of version is filled according to given rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable_version() TO public;
-- </function>

