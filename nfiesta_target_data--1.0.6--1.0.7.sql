--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

----------------------------------------------
-- Functions
-----------------------------------------------

-- <function name="fn_get_ldsity" schema="target_data" src="functions/fn_get_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity(_target_variable integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
target_variable		integer,
label			character varying(200),
label_en		character varying(200),
column_expression	text,
unit_of_measure		integer,
ldsity_object		integer,
ldsity_object_type	integer,
object_type_label	character varying(200),
object_type_desc	text
)
AS
$$
BEGIN
	IF _target_variable IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS target_variable,
			t1.label, t1.label_en, t1.column_expression, t1.ldsity_object, t1.unit_of_measure,
			NULL::int, NULL::varchar(200), NULL::text
		FROM target_data.c_ldsity AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_target_variable (%)', _target_variable;
		END IF;

		RETURN QUERY
		SELECT t3.id, t1.id AS target_variable,
			t3.label, t3.label_en, t3.column_expression, t3.ldsity_object, t3.unit_of_measure, 
			t2.ldsity_object_type, t4.label, t4.description
		FROM target_data.c_target_variable AS t1
		INNER JOIN target_data.cm_ldsity2target_variable AS t2
		ON t1.id = t2.target_variable
		INNER JOIN target_data.c_ldsity AS t3
		ON t2.ldsity = t3.id
		INNER JOIN target_data.c_ldsity_object_type AS t4
		ON t2.ldsity_object_type = t4.id
		WHERE t1.id = _target_variable;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity(integer) IS
'Function returns records from c_ldsity table, optionally for given target_variable.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity(integer) TO public;

-- </function>

-- <function name="fn_get_ldsity4object" schema="target_data" src="functions/fn_get_ldsity4object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_ldsity4object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity4object(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity4object(_ldsity_object integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object		integer,
label			character varying(200),
label_en		character varying(200),
column_expression	text,
unit_of_measure		integer
)
AS
$$
BEGIN
	IF _ldsity_object IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.ldsity_object,
			t1.label, t1.label_en, t1.column_expression, t1.unit_of_measure
		FROM target_data.c_ldsity AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
		THEN RAISE EXCEPTION 'Given ldsity_object does not exist in table c_ldsity_object (%)', _ldsity_object;
		END IF;

		RETURN QUERY
		SELECT t2.id, t2.ldsity_object,
			t2.label, t2.label_en, t2.column_expression, t2.unit_of_measure 
		FROM target_data.c_ldsity_object AS t1
		INNER JOIN target_data.c_ldsity AS t2
		ON t1.id = t2.ldsity_object
		WHERE t1.id = _ldsity_object;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity4object(integer) IS
'Function returns records from c_ldsity table, optionally for given ldsity object.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity4object(integer) TO public;

-- </function>

-- <function name="fn_try_delete_ldsity_object_group" schema="target_data" src="functions/fn_try_delete_ldsity_object_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_delete_ldsity_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_try_delete_ldsity_object_group(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_delete_ldsity_object_group(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _id)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity_object_group (%)', _id;
	END IF;

	RETURN NOT EXISTS (
		SELECT t6.id
		FROM target_data.c_ldsity_object_group AS t1
		INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
		ON t1.id = t2.ldsity_object_group
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN target_data.c_ldsity AS t4
		ON t3.id = t4.ldsity_object
		INNER JOIN target_data.cm_ldsity2target_variable AS t5
		ON t4.id = t5.ldsity
		INNER JOIN target_data.c_target_variable AS t6
		ON t5.target_variable = t6.id
		WHERE t1.id = _id);

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_delete_ldsity_object_group(integer) IS
'Function provides test if it is possible to delete records from c_ldsity_object_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_delete_ldsity_object_group(integer) TO public;

-- </function>

-- <function name="fn_delete_ldsity_object_group" schema="target_data" src="functions/fn_delete_ldsity_object_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_ldsity_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_ldsity_object_group(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_ldsity_object_group(_id integer)
RETURNS void
AS
$$
BEGIN

	DELETE FROM target_data.cm_ld_object2ld_object_group WHERE ldsity_object_group = _id;
	DELETE FROM target_data.c_ldsity_object_group WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_ldsity_object_group(integer) IS
'Function deletes records from c_ldsity_object_group and cm_ld_object2ld_object_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_ldsity_object_group(integer) TO public;

-- </function>
