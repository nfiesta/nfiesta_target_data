--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP FUNCTION IF EXISTS target_data.fn_get_attribute_domain(varchar(2), integer[], integer[]);

-- <function name="fn_get_attribute_domain" schema="target_data" src="functions/fn_get_attribute_domain.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_attribute_domain(varchar(2), integer[]);

CREATE OR REPLACE FUNCTION target_data.fn_get_attribute_domain
(
	IN _attr_type varchar(2), -- 'ad' or 'sp'
	IN _attr_types integer[]
)
  RETURNS TABLE(attribute_domain integer[], category integer[], label character varying[], description text[]) AS
$BODY$
	DECLARE
		_at_count					integer;
		_table_name_category		text;
		_column_name				text;
		_table_name_hierarchy		text;
		
		_attr_types_text			text;
		_carthesian_product			text;
		_constraints				text;
		_constraints_text			text;
		_complete_query				text;
	BEGIN

		if _attr_type is null
		then
			raise exception 'Error 01: fn_get_attribute_domain: The input argument _attr_type must not be NULL!';
		end if;
	
		if not(_attr_type = any(array['ad','sp']))
		then
			raise exception 'Error 02: fn_get_attribute_domain: Allowed values for the input argument _attr_type are "ad" or "sp" !';
		end if;
	
		if _attr_type = 'ad'
		then
			_table_name_category := 'target_data.c_area_domain_category';
			_column_name := 'area_domain';
			_table_name_hierarchy := 'target_data.t_adc_hierarchy';
		end if;
	
		if _attr_type = 'sp'
		then
			_table_name_category := 'target_data.c_sub_population_category';
			_column_name := 'sub_population';
			_table_name_hierarchy := 'target_data.t_spc_hierarchy';
		end if;

	CASE	WHEN _attr_types IS NULL THEN

			RAISE EXCEPTION 'Error 03: fn_get_attribute_domain: The function cannot be called without selling at least one attribute type!';

		WHEN array_length(_attr_types,1) IS DISTINCT FROM array_length(array_remove(_attr_types,NULL),1) THEN

			RAISE EXCEPTION 'Error 04: fn_get_attribute_domain: At least one of the passed attribute types is NULL (attr_types = %)!', _attr_types;
	ELSE
		_at_count := array_length(_attr_types,1);
	END CASE;

-----------------------------------------------------------------------------------------------------------------------
		_attr_types_text := concat(quote_literal(_attr_types::text), '::integer[]');
		_carthesian_product := '
			with recursive /*w_groupnums as (
				select 
					distinct ' || _column_name || ' as grp
				from ' || _table_name_category || '
				where ' || _column_name || ' = ANY (' || _attr_types_text || ')
			)
			,*/ w_groupnums as (select domains.item as grp, domains.id as gnum 
						from unnest( ' || _attr_types_text || ' ) with ordinality as domains(item, id) )
			, w_numbering as (
				select 
					/*grp, row_number() over(order by grp) as gnum */
					grp, gnum 
				from w_groupnums
			)
			, w_category as (
				select id, label, description, ' || _column_name || ' as sup_domain from ' || _table_name_category || '
				union all
				select 0 as id, ''bez rozlišení'' as label, ''bez rozlišení'' as description, ' || _column_name || '
				from ' || _table_name_category || ' group by ' || _column_name || '
			)
			, w_numbered as (
				select 
					w_numbering.gnum, 
					w_numbering.grp, 
					w_category.id as c_category__id,
					w_category.label as c_category__label,
					w_category.description as c_category__description
				from w_numbering 
				join w_category on w_numbering.grp = w_category.sup_domain
				order by w_numbering.grp, w_category.id nulls first
			)
			, w_exploded as (
					select gnum, 
						array[grp] as varray,
						array[c_category__id] as parray, 
						array[c_category__label] as larray,
						array[c_category__description] as darray
					from w_numbered
					where gnum = 1
				union all
					select w_numbered.gnum, 
						array_append(w_exploded.varray,w_numbered.grp),
						array_append(w_exploded.parray,w_numbered.c_category__id),
						array_append(w_exploded.larray,w_numbered.c_category__label),
						array_append(w_exploded.darray,w_numbered.c_category__description)
					from w_numbered 
					join w_exploded on w_exploded.gnum = w_numbered.gnum - 1
			)
			, w_carth_prod as (
				select varray as attribute_domain, parray as category, larray as label, darray as description
				from w_exploded
				where gnum = (select max(gnum) from w_numbering)
				order by larray
			)
			select * 
			from w_carth_prod
';
-------------------------------------------------------------
		_constraints := '
		with w_constraints_explicit as (
			select
				category_sup.' || _column_name || ' as sup_domain, 
				category_sup.id as sup_category, 
				category_sup.label as sup_label, 
				category.' || _column_name || ' as domain,
				category.id as category, 
				category.label
			from ' || _table_name_hierarchy || ' as constraints
			join ' || _table_name_category || ' as category_sup on (category_sup.id = constraints.variable_superior)
			join ' || _table_name_category || ' as category on (category.id = constraints.variable)
		)
		, w_constraints_sup_implicit as (
			select
				category_table.' || _column_name || ' as sup_domain, 
				category_table.id as sup_category, 
				category_table.label as sup_label, 
				null::int as domain,
				null::int as category, 
				''not found'' as label 
			from ' || _table_name_category || ' as category_table
			where 
				(category_table.' || _column_name || ' in (select sup_domain from w_constraints_explicit)
				and
				category_table.id not in (select sup_category from w_constraints_explicit))
		)
		, w_constraints_sup_implicit_combinations as (
			select 
				w_constraints_sup_implicit.sup_domain,
				w_constraints_sup_implicit.sup_category,
				w_constraints_sup_implicit.sup_label,
				domain_combination.domain,
				w_constraints_sup_implicit.category,
				w_constraints_sup_implicit.label
			from (select 
					distinct sup_domain, domain 
					from w_constraints_explicit) as domain_combination
			join w_constraints_sup_implicit on (domain_combination.sup_domain = 
												w_constraints_sup_implicit.sup_domain ) 
		)
		, w_constraints_implicit as (
			select 
				null::int as sup_domain, 
				null::int as sup_category, 
				''not found'' as sup_label, 
				category_table.' || _column_name || ',
				category_table.id as category, 
				category_table.label  
			from ' || _table_name_category || ' as category_table
			where 
				(category_table.' || _column_name || ' in (select domain from w_constraints_explicit)
				and
				category_table.id not in (select category from w_constraints_explicit))
		)
		, w_constraints_implicit_combinations as (
			select 
				w_constraints_implicit.sup_domain,
				w_constraints_implicit.sup_category,
				w_constraints_implicit.sup_label,
				domain_combination.domain,
				w_constraints_implicit.category,
				w_constraints_implicit.label
			from (select 
					distinct sup_domain, domain 
					from w_constraints_explicit) as domain_combination
			join w_constraints_implicit on (domain_combination.sup_domain = 
											w_constraints_implicit.sup_domain ) 
		)
		, w_constraints_implicit_nodistinction as (
			select distinct 
				sup_domain, 0 as sup_category, ''bez rozlišení'' as sup_label, 
				domain, 0 as sup_category, ''bez rozlišení'' as sup_label
			from w_constraints_explicit
		)
		, w_constraints_union as (
			select * from w_constraints_explicit
			union all
			select * from w_constraints_sup_implicit_combinations
			union all
			select * from w_constraints_implicit_combinations
			union all
			select * from w_constraints_implicit_nodistinction
		)
		, w_sub_constraints as (
			select 
				sup_domain, sup_category, domain, array_agg(category order by category) as categories,
				format(''category[array_position(attribute_domain, %s)] = %s'', sup_domain, sup_category) as const_1,
				format(''category[array_position(attribute_domain, %s)] in (%s)'', domain, array_to_string(array_agg(category order by category), '','')) as const_2,
				format(''category[array_position(attribute_domain, %s)] = 0'', domain) as const_3
			from w_constraints_union
			group by sup_domain, domain, sup_category
			order by sup_domain, domain, sup_category
		)
		, w_constraints as (
			select
				sup_domain, domain,
				case when categories = ''{NULL}'' or categories = ''{0}''
					then format(E''(%s and %s)'', const_1, const_3) 
					else format(E''(%s and (%s or %s))'', const_1, const_2, const_3) 
				end as const
			from w_sub_constraints
		)
		, w_constraint as (
			select 
				format (E''\n(\n\t(array_position(attribute_domain, %s) is not null and array_position(attribute_domain, %s) is not null)\n\tand (\n\t\t%s\n))'', 
							sup_domain, domain, array_to_string(array_agg(const), E''\n\tor\t ''))
					as const
				from w_constraints group by	sup_domain, domain
				having sup_domain = ANY (' || _attr_types_text || ') 
					and domain = ANY (' || _attr_types_text || ')
		)
		select 
			format (E''%s'', array_to_string(array_agg(const), E''\nand\n''))
			from w_constraint;
		';
		
		EXECUTE _constraints INTO _constraints_text;
		
		if _constraints_text = '' then 
			_complete_query := concat(_carthesian_product, ';');
		else
			_complete_query := concat(_carthesian_product, E'where\n', _constraints_text, ';');
		end if;

		/*IF _attribute_domain IS NOT NULL
		THEN
			IF _attr_1 IS NULL THEN RAISE EXCEPTION 'Error 06: No combination between attribute types was found for the specified attribute domain (attribute_types = %, attribute_domain = %).', _attr_types, _attribute_domain;
			END IF;
		END IF;*/
		
		--raise notice '%		fn_get_attribute_domain -- %', TimeOfDay(), _complete_query;
		RETURN QUERY EXECUTE _complete_query;
	END;
	$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

  COMMENT ON FUNCTION target_data.fn_get_attribute_domain(varchar(2), integer[]) IS
'The function returns the attribute domain field table for the specified attribute type combination.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_attribute_domain (varchar(2), integer[]) TO app_nfiesta;

-- </function>


-- <function name="fn_get_classification_rules_id4categories" schema="target_data" src="functions/fn_get_classification_rules_id4categories.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rules_id4categories
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[]]) CASCADE;

create or replace function target_data.fn_get_classification_rules_id4categories
(
	_area_domain integer[],
	_area_domain_object integer[],
	_sub_population integer[],
	_sub_population_object integer[]
)
returns table
(
	area_domain_category integer[],
	sub_population_category integer[],
	adc2classification_rule integer[],
	spc2classification_rule integer[]
) as
$$
declare
		_string_ad_w1			text;
		_string_ad_w3			text;
		_string_columns_ad		text;
		_string_columns_ad_w3	text;
	
		_string_sp_w2			text;
		_string_sp_w3			text;
		_string_columns_sp		text;
		_string_columns_sp_w3	text;
	
		_query_res				text;
begin
		-------------------------------
		-------------------------------
		if
			_area_domain is null
		then
			/*
			_string_ad_w1 := 'select 0 as adc_1';
			_string_ad_w3 := 'array[adc_1] as area_domain_category';
			*/

			_string_ad_w1 := 'select array[0] as area_domain, array[0] as area_domain_category';
		else
			/*		
			for i in 1..array_length(_area_domain,1)
			loop
					if i = 1
					then
						_string_columns_ad := concat('unnest(attr_',i,') as adc_',i);
						_string_columns_ad_w3 := concat('adc_',i);
					else
						_string_columns_ad := concat(_string_columns_ad,', ',concat('unnest(attr_',i,') as adc_',i));
						_string_columns_ad_w3 := concat(_string_columns_ad_w3,', adc_',i);
					end if;
			end loop;
		
			_string_ad_w1 := replace(replace(concat('select ',_string_columns_ad,' from target_data.fn_get_attribute_domain(''ad''::varchar,array[',_area_domain,'])'),'{',''),'}','');
			_string_ad_w3 := concat('array[',_string_columns_ad_w3,'] as area_domain_category');
			*/

			_string_ad_w1 := replace(replace(concat('select attribute_domain as area_domain, category as area_domain_category from target_data.fn_get_attribute_domain(''ad''::varchar,array[',_area_domain,'])'),'{',''),'}','');
		end if;
				
		-------------------------------
		-------------------------------
		
		-------------------------------
		-------------------------------
		if
			_sub_population is null
		then
			/*
			_string_sp_w2 := 'select 0 as spc_1';
			_string_sp_w3 := 'array[spc_1] as sub_population_category';
			*/

			_string_sp_w2 := 'select array[0] as sub_population_domain, array[0] as sub_population_category';
		else
			/*
			for i in 1..array_length(_sub_population,1)
			loop
					if i = 1
					then
						_string_columns_sp := concat('unnest(attr_',i,') as spc_',i);
						_string_columns_sp_w3 := concat('spc_',i);
					else
						_string_columns_sp := concat(_string_columns_sp,', ',concat('unnest(attr_',i,') as spc_',i));
						_string_columns_sp_w3 := concat(_string_columns_sp_w3,', spc_',i);
					end if;
			end loop;
		
			_string_sp_w2 := replace(replace(concat('select ',_string_columns_sp,' from target_data.fn_get_attribute_domain(''sp''::varchar,array[',_sub_population,'])'),'{',''),'}','');
			_string_sp_w3 := concat('array[',_string_columns_sp_w3,'] as sub_population_category');
			*/

			_string_sp_w2 := replace(replace(concat('select attribute_domain as sub_population_domain, category as sub_population_category from target_data.fn_get_attribute_domain(''sp''::varchar,array[',_sub_population,'])'),'{',''),'}','');
		end if;
		
		-------------------------------
		-------------------------------	

		_query_res := concat
		('
		with
		w1 as	(',_string_ad_w1,'),
		w2 as	(',_string_sp_w2,'),
		w_data_ad as (
			select
		 		$3 				as domain_orig, 
				w1.area_domain			as domain_reorder,
				$1 				as object_orig
			from w1 limit 1
		)
		, w_unnest_ad as (
			select
				domain_orig.id, domain_orig.item as domain_orig, 
		 		domain_reorder.item as domain_reorder,
		 		object_orig.item as object_orig
			from w_data_ad as w_data,
					unnest(w_data.domain_orig)		with ordinality as domain_orig(item, id)
			join 	unnest(w_data.domain_reorder) 	with ordinality as domain_reorder(item, id) 	on domain_orig.id = domain_reorder.id
			join 	unnest(w_data.object_orig) 		with ordinality as object_orig(item, id) 		on object_orig.id = domain_reorder.id
		)
		, w_sort_ad as (
			select
				w_unnest.object_orig,
				array_position(w_data.domain_reorder, w_unnest.domain_orig)
			from w_data_ad as w_data, w_unnest_ad as w_unnest
		)
		, w_reorder_ad as (
			select 
				array_agg(object_orig order by array_position) as object_reorder 
			from w_sort_ad as w_sort
		),
		w_data_sp as (
			select
		 		$4 				as domain_orig,
		 		w2.sub_population_domain	as domain_reorder,
		 		$2 				as object_orig
			from w2 limit 1
		)
		, w_unnest_sp as (
			select
				domain_orig.id, domain_orig.item as domain_orig, 
		 		domain_reorder.item as domain_reorder, 
		 		object_orig.item as object_orig
			from w_data_sp as w_data,
					unnest(w_data.domain_orig)		with ordinality as domain_orig(item, id)
			join 	unnest(w_data.domain_reorder) 	with ordinality as domain_reorder(item, id) 	on domain_orig.id = domain_reorder.id
			join 	unnest(w_data.object_orig) 		with ordinality as object_orig(item, id) 		on object_orig.id = domain_reorder.id
		)
		, w_sort_sp as (
			select
				w_unnest.object_orig,
				array_position(w_data.domain_reorder, w_unnest.domain_orig)
			from w_data_sp as w_data, w_unnest_sp as w_unnest
		)
		, w_reorder_sp as (
			select 
				array_agg(object_orig order by array_position) as object_reorder 
			from w_sort_sp as w_sort
		),
		w3 as	(select w1.area_domain_category, w2.sub_population_category from w1, w2),
		w4 as	(
				select
					area_domain_category,
					sub_population_category,
					(
					target_data.fn_get_classification_rule_id4category
						(
						area_domain_category,
						w_reorder_ad.object_reorder,
						sub_population_category,
						w_reorder_sp.object_reorder
						)
					) as res
				from
					w3, w_reorder_ad, w_reorder_sp
				)
		select
				area_domain_category,
				sub_population_category,
				(res).adc2classification_rule,
				(res).spc2classification_rule
		from
				w4 order by area_domain_category, sub_population_category;
		');
		return query execute ''||_query_res||'' using _area_domain_object, _sub_population_object, _area_domain, _sub_population;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[]) is
'The function for the area domain or sub population returns a combination of attribute classifications and database identifiers of their classifiaction_rules.';

grant execute on function target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[]) to public;

-- </function>
