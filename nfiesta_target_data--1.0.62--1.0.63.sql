--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------
-- DDL
---------------------
ALTER TABLE target_data.t_adc_hierarchy ADD COLUMN dependent boolean;
-- restrictive
UPDATE target_data.t_adc_hierarchy SET dependent = true;
ALTER TABLE target_data.t_adc_hierarchy ALTER COLUMN dependent SET NOT NULL;

ALTER TABLE target_data.t_spc_hierarchy ADD COLUMN dependent boolean;
-- restrictive
UPDATE target_data.t_spc_hierarchy SET dependent = true;
ALTER TABLE target_data.t_spc_hierarchy ALTER COLUMN dependent SET NOT NULL;

---------------------
-- Functions
---------------------

DROP FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], integer[], integer[][], integer[][]) CASCADE;

-- <function name="fn_save_categories_and_rules" schema="target_data" src="functions/fn_save_categories_and_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categories_and_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], integer[], integer[][], integer[][]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_categories_and_rules(_ldsity_object integer,
	_areal_or_pop integer, _label varchar(200), _description text, 
	_label_en varchar(200), _description_en text,
	_cat_label varchar(200)[], _cat_description text[],
	_cat_label_en varchar(200)[], _cat_description_en text[],
	_rules text[], _panel_refyearset integer[], 
	_adc integer[][] DEFAULT NULL::int[][], _spc integer[][] DEFAULT NULL::int[][]
)
RETURNS TABLE (
	areal_or_pop			integer,
	cat_label			varchar(200),
	cat_label_en			varchar(200),
	parent_id			integer,
	category_id			integer,	
	classification_rule		integer,
	refyearset2panel		integer
) 
AS
$$
DECLARE
BEGIN
	IF array_length(_cat_label,1) != array_length(_cat_description,1) OR
		array_length(_cat_label_en,1) != array_length(_cat_description_en,1)
	THEN
		RAISE EXCEPTION 'Given arrays of label and description (%,%) or label_en and description_en (%,%) must be of same length!', _label, _description, _label_en, _description_en;
	END IF;

	IF array_length(_rules,1) != array_length(_cat_label,1)
	THEN
		RAISE EXCEPTION 'Given arrays of rules (%) must be the same length as array of labels (%)!', _rules, _label;
	END IF;

	RETURN QUERY
	WITH w_parent AS (
		SELECT t1 AS id FROM target_data.fn_save_areal_or_pop(_areal_or_pop, _label, _description, _label_en, _description_en) AS t1
	), w_categories AS (
		SELECT 	t1.id AS parent_id, -- t1.label, t1.label_en,
			t2.id AS array_id,
			t6 AS category_id, t2.label AS label_cat, t4.label_en AS label_en_cat
		FROM 	w_parent AS t1,
			unnest(_cat_label) WITH ORDINALITY AS t2(label, id)
		LEFT JOIN unnest(_cat_description) WITH ORDINALITY AS t3(description, id) ON t2.id = t3.id
		LEFT JOIN unnest(_cat_label_en) WITH ORDINALITY AS t4(label_en, id) ON t3.id = t4.id
		LEFT JOIN unnest(_cat_description_en) WITH ORDINALITY AS t5(description_en, id) ON t4.id = t5.id,
			target_data.fn_save_category(_areal_or_pop, t2.label, t3.description, t4.label_en, t5.description_en, t1.id) AS t6
	), w_rules AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, t2.rule, t3 AS rule_id, t1.label_cat, t1.label_en_cat
		FROM 	w_categories AS t1
		INNER JOIN unnest(_rules) WITH ORDINALITY AS t2(rule,id) ON t1.array_id = t2.id,
			target_data.fn_save_classification_rule(t1.category_id, _areal_or_pop, _ldsity_object, t2.rule) AS t3
	),
	w_rule_agg AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, t1.label_cat, t1.label_en_cat,
			array_agg(t1.rule_id ORDER BY array_id) AS rule_ids
		FROM w_rules AS t1
		GROUP BY t1.parent_id, t1.array_id, t1.category_id, t1.label_cat, t1.label_en_cat
	), w_hier_adc AS (
		INSERT INTO target_data.t_adc_hierarchy (variable_superior, variable, dependent)
		SELECT t2.adc, t1.category_id, true
		FROM	w_categories AS t1,
			(SELECT 
				adc, id, ceil(id/(count(*) OVER())::double precision * array_length(_adc,1)) AS array_id
			FROM unnest(_adc) WITH ORDINALITY AS t1(adc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id AND t2.adc IS NOT NULL
	), w_hier_spc AS (
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable, dependent)
		SELECT t2.spc, t1.category_id, true
		FROM	w_categories AS t1,
			(SELECT 
				spc, id, ceil(id/(count(*) OVER())::double precision * array_length(_spc,1)) AS array_id
			FROM unnest(_spc) WITH ORDINALITY AS t1(spc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id AND t2.spc IS NOT NULL
	)
	SELECT  _areal_or_pop, t1.label_cat, t1.label_en_cat,
		 t1.parent_id, t1.category_id, t2.classification_rule, t2.refyearset2panel
	FROM 
		w_rule_agg AS t1,
		target_data.fn_save_class_rule_mapping(_areal_or_pop, t1.rule_ids, _panel_refyearset) AS t2
	;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], integer[], integer[][], integer[][]) IS
'Function inserts all necessary data into lookups/tables with area_domain/sub_population categories and its classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], integer[], integer[][], integer[][]) TO public;

-- </function>

-- <function name="fn_save_category" schema="target_data" src="functions/fn_save_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_category
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_category(integer, varchar, text, varchar, text, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_category(_areal_or_population integer, _label varchar(200), _description text, _label_en varchar(200), _description_en text, _parent integer DEFAULT NULL::integer, _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
BEGIN
	IF _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent identifier (%) or label/descriptioin is null (%, %)!', _parent, _label, _description;
	END IF; 

	IF _id IS NULL THEN

		IF _parent IS NULL
		THEN
			RAISE EXCEPTION 'Parameter of parent idenfier (id from c_area_domain/c_sub_population) is null!';
		END IF;

		CASE 
		WHEN _areal_or_population = 100 THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _parent)
			THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%).', _parent;
			END IF;

			INSERT INTO target_data.c_area_domain_category(area_domain, label, description, label_en, description_en)
			SELECT 
				_parent,
				_label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _parent)
			THEN RAISE EXCEPTION 'Given sub population does not exist in table c_sub_population (%).', _parent;
			END IF;

			INSERT INTO target_data.c_sub_population_category(sub_population, label, description, label_en, description_en)
			SELECT 
				_parent,
				_label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	ELSE
		CASE WHEN _areal_or_population = 100 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain_category AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain category does not exist in table c_area_domain_category (%)', _id;
			END IF;

			UPDATE target_data.c_area_domain_category
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_area_domain_category.id = _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_sub_population_category AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given sub population category does not exist in table c_sub_population_category (%)', _id;
			END IF;

			UPDATE target_data.c_sub_population_category
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_sub_population_category.id = _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_category(integer, varchar(200), text, varchar(200), text, integer, integer) IS
'Functions inserts a record into table c_area_domain_category or c_sub_population_category based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_category(integer, varchar(200), text, varchar(200), text, integer, integer) TO public;

-- </function>

-- <function name="fn_save_rules" schema="target_data" src="functions/fn_save_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_rules(integer, integer, integer[], text[], integer[], integer[][], integer[][] CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_rules(_ldsity_object integer, _areal_or_pop integer, _category_ids integer[], _rules text[], _panel_refyearset integer[], 
		_adc integer[][] DEFAULT NULL::integer[][], _spc integer[][] DEFAULT NULL::integer[][])
RETURNS TABLE (
	areal_or_pop			integer,
	parent_id			integer,
	category_id			integer,	
	classification_rule		integer,
	refyearset2panel		integer
) 
AS
$$
DECLARE
BEGIN
	IF array_length(_rules,1) != array_length(_category_ids,1)
	THEN
		RAISE EXCEPTION 'Given arrays of rules (%) must be the same length as array of category ids (%)!', _rules, _category_ids;
	END IF;

	RETURN QUERY
	WITH w_categories AS (
		SELECT 
			t1.id AS array_id,
			t1.category_id,
			CASE WHEN _areal_or_pop = 100 THEN t2.area_domain
			WHEN _areal_or_pop = 200 THEN t3.sub_population
			END AS parent_id
		FROM
			unnest(_category_ids) WITH ORDINALITY AS t1(category_id, id)
		LEFT JOIN target_data.c_area_domain_category AS t2
		ON t2.id = t1.category_id AND _areal_or_pop = 100
		LEFT JOIN target_data.c_sub_population_category AS t3
		ON t3.id = t1.category_id AND _areal_or_pop = 200

	), w_rules AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, t2.rule, t3 AS rule_id
		FROM 	w_categories AS t1
		INNER JOIN unnest(_rules) WITH ORDINALITY AS t2(rule,id) ON t1.array_id = t2.id,
			target_data.fn_save_classification_rule(t1.category_id, _areal_or_pop, _ldsity_object, t2.rule) AS t3
	),
	w_rule_agg AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, array_agg(t1.rule_id ORDER BY array_id) AS rule_ids
		FROM w_rules AS t1
		GROUP BY t1.parent_id, t1.array_id, t1.category_id
	), w_hier_adc AS (
		INSERT INTO target_data.t_adc_hierarchy (variable_superior, variable, dependent)
		SELECT t2.adc, t1.category_id, true
		FROM	w_categories AS t1,
			(SELECT 
				adc, id, ceil(id/(count(*) OVER())::double precision * array_length(_adc,1)) AS array_id
			FROM unnest(_adc) WITH ORDINALITY AS t1(adc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id AND t2.adc IS NOT NULL
	), w_hier_spc AS (
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable, dependent)
		SELECT t2.spc, t1.category_id, true
		FROM	w_categories AS t1,
			(SELECT 
				spc, id, ceil(id/(count(*) OVER())::double precision * array_length(_spc,1)) AS array_id
			FROM unnest(_spc) WITH ORDINALITY AS t1(spc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id AND t2.spc IS NOT NULL
	)
	SELECT  _areal_or_pop, t1.parent_id, t1.category_id, t2.classification_rule, t2.refyearset2panel
	FROM 
		w_rule_agg AS t1,
		target_data.fn_save_class_rule_mapping(_areal_or_pop, t1.rule_ids, _panel_refyearset) AS t2
	;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_rules(integer, integer, integer[], text[], integer[], integer[][], integer[][]) IS
'Function inserts all necessary data into lookups/tables with area_domain/sub_population categories and its classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_rules(integer, integer, integer[], text[], integer[], integer[][], integer[][]) TO public;

-- </function>

-- <function name="fn_get_area_domain_category4domain" schema="target_data" src="functions/fn_get_area_domain_category4domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_area_domain_category4domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_area_domain_category4domain(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_area_domain_category4domain(_area_domain integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
area_domain	integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _area_domain IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.area_domain, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_area_domain_category AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _area_domain)
		THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _area_domain;
		END IF;

		RETURN QUERY
		SELECT t4.id, t4.area_domain, t4.label, t4.description, t4.label_en, t4.description_en
		FROM target_data.c_area_domain_category AS t4
		WHERE t4.area_domain = _area_domain
		ORDER BY t4.id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_area_domain_category4domain(integer) IS
'Function returns records from c_area_domain_category table, optionally for given area domain.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_area_domain_category4domain(integer) TO public;

-- </function>

-- <function name="fn_get_sub_population_category4population" schema="target_data" src="functions/fn_get_sub_population_category4population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_sub_population_category4population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_sub_population_category4population(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_sub_population_category4population(_sub_population integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
sub_population	integer,
label		character varying(200),
description	text,
label_en	character varying(200),
description_en	text
)
AS
$$
BEGIN
	IF _sub_population IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.sub_population, t1.label, t1.description, t1.label_en, t1.description_en
		FROM target_data.c_sub_population_category AS t1
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _sub_population)
		THEN RAISE EXCEPTION 'Given sub population does not exist in table c_sub_population (%)', _sub_population;
		END IF;

		RETURN QUERY
		SELECT t4.id, t4.sub_population, t4.label, t4.description, t4.label_en, t4.description_en
		FROM target_data.c_sub_population_category AS t4
		WHERE t4.sub_population = _sub_population
		ORDER BY t4.id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_sub_population_category4population(integer) IS
'Function returns records from c_sub_population_category table, optionally for given sub population.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_sub_population_category4population(integer) TO public;

-- </function>

-- <function name="fn_get_target_variable" schema="target_data" src="functions/fn_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_target_variable(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_target_variable(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer[],
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
state_or_change		integer,
soc_label		character varying(200),
soc_description		text,
areal_or_population	integer
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		WITH 
		w_objects AS (
			SELECT t2.ldsity_object_group, array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			GROUP BY t2.ldsity_object_group
			),
		w_core AS (
			SELECT 	t5.target_variable, 
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = false) AS ldsity_objects_pozit,
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = true) AS ldsity_objects_negat
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		),
		w_target_variable AS (
			SELECT 	t2.ldsity_object_group, t1.target_variable, t3.label, t3.description, t3.label_en, t3.description_en, t3.state_or_change, 
				t4.label AS soc_label, t4.description AS soc_description
			FROM w_core AS t1
			INNER JOIN w_objects AS t2
			ON t2.ldsity_objects = t1.ldsity_objects_pozit OR (t2.ldsity_objects @> t1.ldsity_objects_pozit AND t2.ldsity_objects @> t1.ldsity_objects_negat)
			INNER JOIN target_data.c_target_variable AS t3
			ON t1.target_variable = t3.id
			INNER JOIN target_data.c_state_or_change AS t4
			ON t3.state_or_change = t4.id
		),
		w_agg_200 AS (
			SELECT 	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category,
				t5.areal_or_population,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = false) AS ldsity_pozit,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = true) AS ldsity_negat,
				count(*) OVER (PARTITION BY t1.target_variable) AS no_of_contrib
			FROM w_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.target_variable = t2.target_variable AND t2.ldsity_object_type = 100
			INNER JOIN target_data.c_ldsity AS t4
			ON t2.ldsity = t4.id
			INNER JOIN target_data.c_ldsity_object AS t5
			ON t4.ldsity_object = t5.id
			LEFT JOIN target_data.cm_ldsity2target_variable AS t3
			ON t1.target_variable = t3.target_variable AND t3.ldsity_object_type = 200 AND t2.use_negative = t3.use_negative
			GROUP BY t1.target_variable, t1.ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, t5.areal_or_population
		), w_agg_id AS (
			-- query with one contribution which can be divided by other contributions
			(SELECT array_agg(t1.target_variable) AS ids, t1.ldsity_object_group,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib = 1
			GROUP BY t1.ldsity, t1.ldsity_object_group, 
				t1.area_domain_category, t1.sub_population_category,
				t1.state_or_change, t1.soc_label, t1.soc_description, t1.areal_or_population,
				t1.ldsity_object_type)
			UNION ALL
			-- query with more than one contribution, can not be divided
			(SELECT array_agg(DISTINCT t1.target_variable) AS ids, t1.ldsity_object_group, 
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib > 1
			GROUP BY t1.ldsity_object_group, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type)
		)
		SELECT ids AS id, t1.ldsity_object_group, t1.label[1], t1.description[1], t1.label_en[1], t1.description_en[1],
			t1.state_or_change, t1.soc_label, t1.soc_description,
			t1.areal_or_population[1]
		FROM w_agg_id AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		WITH 
		w_objects AS (
			SELECT t2.ldsity_object_group, array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			WHERE t1.id = _ldsity_object_group
			GROUP BY t2.ldsity_object_group
			),
		w_core AS (
			SELECT 	t5.target_variable,
 				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = false) AS ldsity_objects_pozit,
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = true) AS ldsity_objects_negat
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		),
		w_target_variable AS (
			SELECT 	t2.ldsity_object_group, t1.target_variable, t3.label, t3.description, t3.label_en, t3.description_en, t3.state_or_change, 
				t4.label AS soc_label, t4.description AS soc_description
			FROM w_core AS t1
			INNER JOIN w_objects AS t2
			ON t2.ldsity_objects = t1.ldsity_objects_pozit OR (t2.ldsity_objects @> t1.ldsity_objects_pozit AND t2.ldsity_objects @> t1.ldsity_objects_pozit)
			INNER JOIN target_data.c_target_variable AS t3
			ON t1.target_variable = t3.id
			INNER JOIN target_data.c_state_or_change AS t4
			ON t3.state_or_change = t4.id
		),
		w_agg_200 AS (
			SELECT 	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category,
				t5.areal_or_population,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = false) AS ldsity_pozit,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = true) AS ldsity_negat,
				count(*) OVER (PARTITION BY t1.target_variable) AS no_of_contrib
			FROM w_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.target_variable = t2.target_variable AND t2.ldsity_object_type = 100
			INNER JOIN target_data.c_ldsity AS t4
			ON t2.ldsity = t4.id
			INNER JOIN target_data.c_ldsity_object AS t5
			ON t4.ldsity_object = t5.id
			LEFT JOIN target_data.cm_ldsity2target_variable AS t3
			ON t1.target_variable = t3.target_variable AND t3.ldsity_object_type = 200 AND t2.use_negative = t3.use_negative
			GROUP BY t1.target_variable, t1.ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, t5.areal_or_population
		), w_agg_id AS (
			-- query with one contribution which can be divided by other contributions
			(SELECT array_agg(t1.target_variable) AS ids, t1.ldsity_object_group,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib = 1
			GROUP BY t1.ldsity, t1.ldsity_object_group, 
				t1.area_domain_category, t1.sub_population_category,
				t1.state_or_change, t1.soc_label, t1.soc_description, t1.areal_or_population,
				t1.ldsity_object_type)
			UNION ALL
			-- query with more than one contribution, can not be divided
			(SELECT array_agg(DISTINCT t1.target_variable) AS ids, t1.ldsity_object_group, 
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib > 1
			GROUP BY t1.ldsity_object_group, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type)
		)
		SELECT ids AS id, t1.ldsity_object_group, t1.label[1], t1.description[1], t1.label_en[1], t1.description_en[1],
			t1.state_or_change, t1.soc_label, t1.soc_description,
			t1.areal_or_population[1]
		FROM w_agg_id AS t1;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_target_variable(integer) IS
'Function returns records from c_target_variable table, optionally for given ldsity_object_group.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_target_variable(integer) TO public;

-- </function>

-- <function name="fn_delete_area_domain" schema="target_data" src="functions/fn_delete_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_area_domain(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_area_domain(_id integer)
RETURNS void
AS
$$
BEGIN

	DELETE FROM target_data.cm_adc2classrule2panel_refyearset 
	WHERE adc2classification_rule IN 
		(SELECT id FROM target_data.cm_adc2classification_rule 
		WHERE area_domain_category IN
			(SELECT id FROM target_data.c_area_domain_category 
			WHERE area_domain = _id)); 		
				
	DELETE FROM target_data.cm_adc2classification_rule 
	WHERE area_domain_category IN 
		(SELECT id FROM target_data.c_area_domain_category 
		WHERE area_domain = _id);

	IF EXISTS (SELECT * FROM target_dta.t_adc_hierarchy
		WHERE variable_superior IN (SELECT id FROM target_data.c_area_domain_category
			WHERE area_domain = _id) AND
			dependent = true
		)
	THEN 
		RAISE EXCEPTION 'Given area domain cannot be deleted, because some of its categories are present in table t_adc_hierarchy in the superior position. It can destroy the dependency between the categories. You have to delete inferior categories (area domain) first.';
	END IF;

	DELETE FROM target_data.t_adc_hierarchy
	WHERE	variable IN (SELECT id FROM target_data.c_area_domain_category
			WHERE area_domain = _id) OR
		variable_superior IN (SELECT id FROM target_data.c_area_domain_category
			WHERE area_domain = _id);
	
	DELETE FROM target_data.c_area_domain_category 
	WHERE area_domain = _id;
	
	DELETE FROM target_data.c_area_domain 
	WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_area_domain(integer) IS
'Function deletes records from c_area_domain, c_area_domain_category, cm_adc2classification_rule and cm_adc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_area_domain(integer) TO public;

-- </function>

-- <function name="fn_delete_sub_population" schema="target_data" src="functions/fn_delete_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_sub_population(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_sub_population(_id integer)
RETURNS void
AS
$$
BEGIN

	DELETE FROM target_data.cm_spc2classrule2panel_refyearset 
	WHERE spc2classification_rule IN 
		(SELECT id FROM target_data.cm_spc2classification_rule 
		WHERE sub_population_category IN
			(SELECT id FROM target_data.c_sub_population_category 
			WHERE sub_population = _id)); 
				
	DELETE FROM target_data.cm_spc2classification_rule 
	WHERE sub_population_category IN 
		(SELECT id FROM target_data.c_sub_population_category 
		WHERE sub_population = _id);

	IF EXISTS (SELECT * FROM target_data.t_spc_hierarchy
		WHERE variable_superior IN (SELECT id FROM target_data.c_sub_population_category
			WHERE sub_population = _id) AND 
			dependent = true
		)
	THEN 
		RAISE EXCEPTION 'Given sub population cannot be deleted, because some of its categories are present in table t_spc_hierarchy in the superior position. It can destroy the dependency between the categories. You have to delete inferior categories (sub population) first.';
	END IF;

	DELETE FROM target_data.t_spc_hierarchy
	WHERE 	variable IN (SELECT id FROM target_data.c_sub_population_category
			WHERE sub_population = _id) OR 
		variable_superior IN (SELECT id FROM target_data.c_sub_population_category
			WHERE sub_population = _id);

	DELETE FROM target_data.c_sub_population_category 
	WHERE sub_population = _id;
	
	DELETE FROM target_data.c_sub_population
	WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_sub_population(integer) IS
'Function deletes records from c_sub_population, c_sub_population_category, cm_spc2classification_rule and cm_spc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_sub_population(integer) TO public;

-- </function>

-- <function name="fn_save_hierarchy" schema="target_data" src="functions/fn_save_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_save_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_hierarchy(integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_hierarchy(_areal_or_pop integer, _superior_cat integer[], _inferior_cat integer[])
RETURNS TABLE (
superior_cat		integer,
inferior_cat		integer
)
AS
$$
DECLARE
_test			integer[];
_superior_cat_test	integer[];
_inferior_cat_test	integer[];
BEGIN
		-- reorder just for equality test
		SELECT array_agg(t1.sup ORDER BY t1.sup) AS test
		FROM unnest(_superior_cat) AS t1(sup)
		INTO _superior_cat_test;

		SELECT array_agg(t1.inf ORDER BY t1.inf) AS test
		FROM unnest(_inferior_cat) AS t1(inf)
		INTO _inferior_cat_test;

	CASE WHEN _areal_or_pop = 100 THEN

		SELECT array_agg(t1.sup ORDER BY t1.sup) AS test
		FROM unnest(_superior_cat) AS t1(sup)
		INNER JOIN target_data.c_area_domain_category AS t2
		ON t1.sup = t2.id
		INTO _test;

		IF _test != _superior_cat_test
		THEN RAISE EXCEPTION 'Given array of superior categories containes identificators which are not present in c_area_domain_category.';
		END IF;

		SELECT array_agg(t1.inf ORDER BY t1.inf) AS test
		FROM unnest(_inferior_cat) AS t1(inf)
		INNER JOIN target_data.c_area_domain_category AS t2
		ON t1.inf = t2.id
		INTO _test;

		IF _test != _inferior_cat_test
		THEN RAISE EXCEPTION 'Given array of inferior categories containes identificators which are not present in c_area_domain_category.';
		END IF;

		RETURN QUERY
		INSERT INTO target_data.t_adc_hierarchy (variable_superior, variable, dependent)
		SELECT t1.sup, t2.inf, false
		FROM
			unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
		INNER JOIN
			unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
		ON t1.id = t2.id
		RETURNING variable_superior, variable;

	WHEN _areal_or_pop = 200 THEN

		SELECT array_agg(t1.sup ORDER BY t1.sup) AS test
		FROM unnest(_superior_cat) AS t1(sup)
		INNER JOIN target_data.c_sub_population_category AS t2
		ON t1.sup = t2.id
		INTO _test;

		IF _test != _superior_cat_test
		THEN RAISE EXCEPTION 'Given array of superior categories containes identificators which are not present in c_sub_population_category.';
		END IF;

		SELECT array_agg(t1.inf ORDER BY t1.inf) AS test
		FROM unnest(_inferior_cat) AS t1(inf)
		INNER JOIN target_data.c_sub_population_category AS t2
		ON t1.inf = t2.id
		INTO _test;

		IF _test != _inferior_cat_test
		THEN RAISE EXCEPTION 'Given array of inferior categories containes identificators which are not present in c_sub_population_category.';
		END IF;

		RETURN QUERY
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable, dependent)
		SELECT t1.sup, t2.inf, false
		FROM
			unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
		INNER JOIN
			unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
		ON t1.id = t2.id
		RETURNING variable_superior, variable;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_hierarchy(integer, integer[], integer[]) IS
'Function provides insert into table t_adc_hierarchy/t_spc_hierarchy.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_hierarchy(integer, integer[], integer[]) TO public;

-- </function>

DROP FUNCTION target_data.fn_get_hierarchy(integer, integer, integer, boolean) CASCADE;
-- <function name="fn_get_hierarchy" schema="target_data" src="functions/fn_get_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_hierarchy(integer, integer, integer, boolean, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_hierarchy(_areal_or_pop integer, _type_superior integer, _type_inferior integer, _dependent boolean DEFAULT false, _null_inferior boolean DEFAULT true)
RETURNS TABLE (
superior_cat		integer,
inferior_cat		integer,
dependent		boolean
)
AS
$$
BEGIN
	CASE WHEN _areal_or_pop = 100 THEN
	RETURN QUERY
		WITH w_sup AS (
			SELECT t2.id AS variable_superior, t3.variable, t3.dependent
			FROM target_data.c_area_domain AS t1
			INNER JOIN target_data.c_area_domain_category AS t2
			ON t1.id = t2.area_domain AND t1.id = _type_superior
			LEFT JOIN target_data.t_adc_hierarchy AS t3
			ON t2.id = t3.variable_superior
		),
		w_inf AS (
			SELECT t3.variable_superior, t2.id AS variable, t3.dependent
			FROM target_data.c_area_domain AS t1
			INNER JOIN target_data.c_area_domain_category AS t2
			ON t1.id = t2.area_domain AND t1.id = _type_inferior
			LEFT JOIN target_data.t_adc_hierarchy AS t3
			ON t2.id = t3.variable
			WHERE 	CASE WHEN _null_inferior = false THEN t3.variable_superior IS NULL ELSE true END
			 	AND t1.dependent = _dependent
		)
		SELECT variable_superior, variable, w_sup.dependent
		FROM w_sup
		UNION
		SELECT variable_superior, variable, w_inf.dependent
		FROM w_inf
		ORDER BY variable_superior, variable;

	WHEN _areal_or_pop = 200 THEN
	RETURN QUERY
		WITH w_sup AS (
			SELECT t2.id AS variable_superior, t3.variable, t3.dependent
			FROM target_data.c_sub_population AS t1
			INNER JOIN target_data.c_sub_population_category AS t2
			ON t1.id = t2.sub_population AND t1.id = _type_superior
			LEFT JOIN target_data.t_spc_hierarchy AS t3
			ON t2.id = t3.variable_superior
		),
		w_inf AS (
			SELECT t3.variable_superior, t2.id AS variable, t3.dependent
			FROM target_data.c_sub_population AS t1
			INNER JOIN target_data.c_sub_population_category AS t2
			ON t1.id = t2.sub_population AND t1.id = _type_inferior
			LEFT JOIN target_data.t_spc_hierarchy AS t3
			ON t2.id = t3.variable
			WHERE 	CASE WHEN _null_inferior = false THEN t3.variable_superior IS NULL ELSE true END
			 	AND t3.dependent = _dependent
		)
		SELECT variable_superior, variable, w_sup.dependent
		FROM w_sup
		UNION
		SELECT variable_superior, variable, w_inf.dependent
		FROM w_inf
		ORDER BY variable_superior, variable;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_hierarchy(integer, integer, integer, boolean, boolean) IS
'Function returns records from t_adc_hierarchy/t_spc_hierarchy table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_hierarchy(integer, integer, integer, boolean, boolean) TO public;

-- </function>

-- <function name="fn_delete_hierarchy" schema="target_data" src="functions/fn_delete_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_delete_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_delete_hierarchy(integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_hierarchy(_areal_or_pop integer, _superior_cat integer[], _inferior_cat integer[])
RETURNS TABLE (
superior_cat		integer,
inferior_cat		integer
)
AS
$$
DECLARE
_test			integer[];
_superior_cat_test	integer[];
_inferior_cat_test	integer[];
BEGIN
		-- reorder just for equality test
		SELECT array_agg(t1.sup ORDER BY t1.sup) AS test
		FROM unnest(_superior_cat) AS t1(sup)
		INTO _superior_cat_test;

		SELECT array_agg(t1.inf ORDER BY t1.inf) AS test
		FROM unnest(_inferior_cat) AS t1(inf)
		INTO _inferior_cat_test;

	CASE WHEN _areal_or_pop = 100 THEN

		SELECT array_agg(t1.sup ORDER BY t1.sup)
		FROM
			unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
		INNER JOIN
			unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
		ON t1.id = t2.id
		INNER JOIN target_data.t_adc_hierarchy AS t3
		ON t1.sup = t3.variable_superior AND t2.inf = t3.variable
		WHERE t3.dependent = false
		INTO _test;

		IF _test != _superior_cat_test
		THEN RAISE EXCEPTION 'Given arrays of superior and inferior categories contain combinations which are not present in table t_adc_hierarchy. Or the inferior category is dependent on the superior - hierarchy can be deleted only by deleting inferior categories itself.';
		END IF; 

		RETURN QUERY
		DELETE FROM target_data.t_adc_hierarchy AS a
		USING 
			(SELECT t1.sup, t2.inf
			FROM	unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
			INNER JOIN unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
			ON t1.id = t2.id) AS b
		WHERE a.variable_superior = b.sup AND a.variable = b.inf
		RETURNING a.variable_superior, a.variable;

	WHEN _areal_or_pop = 200 THEN

		SELECT array_agg(t1.sup ORDER BY t1.sup)
		FROM
			unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
		INNER JOIN
			unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
		ON t1.id = t2.id
		INNER JOIN target_data.t_spc_hierarchy AS t3
		ON t1.sup = t3.variable_superior AND t2.inf = t3.variable
		INTO _test;

		IF _test != _superior_cat_test
		THEN RAISE EXCEPTION 'Given arrays of superior and inferior categories contain combinations which are not present in table t_spc_hierarchy. Or the inferior category is dependent on the superior - hierarchy can be deleted only by deleting inferior categories itself.';
		END IF; 

		RETURN QUERY
		DELETE FROM target_data.t_spc_hierarchy AS a
		USING 
			(SELECT t1.sup, t2.inf
			FROM	unnest(_superior_cat) WITH ORDINALITY AS t1(sup, id)
			INNER JOIN unnest(_inferior_cat) WITH ORDINALITY AS t2(inf, id)
			ON t1.id = t2.id) AS b
		WHERE a.variable_superior = b.sup AND a.variable = b.inf
		RETURNING a.variable_superior, a.variable;

	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_hierarchy(integer, integer[], integer[]) IS
'Function deletes records from t_adc_hierarchy/t_spc_hierarchy table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_hierarchy(integer, integer[], integer[]) TO public;

-- </function>

-- <function name="fn_import_data" schema="target_data" src="functions/fn_import_data.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_data(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_import_data(_schema character varying)
RETURNS text
AS
$$
DECLARE
		_res	text;
BEGIN
	-- Data for Name: c_ldsity_object; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity_object(id, label, description, label_en, description_en, table_name, upper_object, areal_or_population, column4upper_object, filter)
	select id, label, description, label_en, description_en, table_name, upper_object, areal_or_population, column4upper_object, filter from '||_schema||'.c_ldsity_object order by id';

	-- Data for Name: c_unit_of_measure; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_unit_of_measure(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_unit_of_measure order by id';

	-- Data for Name: c_definition_variant; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_definition_variant(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_definition_variant order by id';	

	-- Data for Name: c_area_domain; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_area_domain order by id';

	-- Data for Name: c_area_domain_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain_category(id, label, description, label_en, description_en, area_domain)
	select id, label, description, label_en, description_en, area_domain from '||_schema||'.c_area_domain_category order by id';

	-- Data for Name: cm_adc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_adc2classification_rule(area_domain_category, ldsity_object, classification_rule)
	select area_domain_category, ldsity_object, classification_rule from '||_schema||'.cm_adc2classification_rule order by id';	

	-- Data for Name: c_sub_population; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_sub_population order by id';

	-- Data for Name: c_sub_population_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population_category(id, label, description, label_en, description_en, sub_population)
	select id, label, description, label_en, description_en, sub_population from '||_schema||'.c_sub_population_category order by id';

	-- Data for Name: cm_spc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_spc2classification_rule(sub_population_category, ldsity_object, classification_rule)
	select sub_population_category, ldsity_object, classification_rule from '||_schema||'.cm_spc2classification_rule order by id';	

	-- Data for Name: c_ldsity; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity(id, label, description, label_en, description_en, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant)
	select id, label, description, label_en, description_en, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant from '||_schema||'.c_ldsity order by id';

	-- Data for Name: t_adc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_adc_hierarchy(variable_superior, variable, dependent)
	select variable_superior, variable, dependent from '||_schema||'.t_adc_hierarchy order by id';

	-- Data for Name: t_spc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_spc_hierarchy(variable_superior, variable, dependent)
	select variable_superior, variable, dependent from '||_schema||'.t_spc_hierarchy order by id';

	-- Data for Name: c_version; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_version(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_version order by id';	

	-- Data for Name: c_version; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_ldsity2panel_refyearset_version(ldsity, refyearset2panel, version)
	select ldsity, refyearset2panel, version from '||_schema||'.cm_ldsity2panel_refyearset_version order by id';

	-- Data for Name: cm_adc2classrule2panel_refyearset; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_adc2classrule2panel_refyearset(adc2classification_rule, refyearset2panel)
	select adc2classification_rule, refyearset2panel from '||_schema||'.cm_adc2classrule2panel_refyearset order by id';	

	-- Data for Name: cm_spc2classrule2panel_refyearset; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_spc2classrule2panel_refyearset(spc2classification_rule, refyearset2panel)
	select spc2classification_rule, refyearset2panel from '||_schema||'.cm_spc2classrule2panel_refyearset order by id';		

	-- Name: c_ldsity_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_ldsity_id_seq', (select max(id) from target_data.c_ldsity), true);


	_res := 'Import is comleted.';

	return _res;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_import_data(character varying) IS
'The function imports data from source tables into tables that are in the target_data schema.';

-- </function>

-- <function name="fn_check_classification_rule" schema="target_data" src="functions/fn_check_classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rule(_ldsity integer, _ldsity_object integer, _rule text, _panel_refyearset integer DEFAULT NULL::integer, _adc integer[] DEFAULT NULL::integer[], _spc integer[] DEFAULT NULL::integer[])
--boolean
RETURNS TABLE (
compliance_status	boolean,
no_of_objects		integer
) 
AS
$$
DECLARE
_ldsity_objects		integer[];
_adc_rule_test		integer[];
_spc_rule_test		integer[];
_table_name4rule	varchar;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
_exist_test		boolean;
adc			integer;
spc			integer;
_qall			text;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
	INTO _test;

	IF _test = false
	THEN
		RAISE EXCEPTION 'Given rule has an invalid syntax.';
	END IF;

	IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
	THEN
		_exist_test := true; _rule = true;
	ELSE 	_exist_test := false;
	END IF;
	
	_case := '('||_rule||')';

-- test all adc/spc has to come from one area domain/sub population

	IF (SELECT count(DISTINCT t2.area_domain)
		FROM unnest(_adc) AS t1(adc),
			target_data.c_area_domain_category AS t2
		WHERE t1.adc = t2.id) > 1
	THEN 
		RAISE EXCEPTION 'Given array of area domain categories does not come from one area domain.';
	END IF;
	
	IF (SELECT count(DISTINCT t2.sub_population)
		FROM unnest(_spc) AS t1(spc),
			target_data.c_sub_population_category AS t2
		WHERE t1.spc = t2.id) > 1
	THEN 
		RAISE EXCEPTION 'Given array of sub population categories does not come from one sub population.';
	END IF;


	_ldsity_objects := (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]));

-- get id from cm_adc2classification_rule id for each adc
	WITH w AS(
		SELECT
			t1.id, t1.area_domain_category
		FROM
			target_data.cm_adc2classification_rule AS t1,
			unnest(_ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		WHERE
			array[area_domain_category] <@ _adc AND
			t1.ldsity_object = t2.ldsity_object
		ORDER BY t2.id DESC 
		LIMIT array_length(_adc,1)
	)
	SELECT array_agg(id ORDER BY id)
	FROM w
	INTO _adc;

	WITH w AS(
		SELECT
			t1.id, t1.sub_population_category
		FROM
			target_data.cm_spc2classification_rule AS t1,
			unnest(_ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		WHERE
			array[sub_population_category] <@ _spc AND
			t1.ldsity_object = t2.ldsity_object
		ORDER BY t2.id DESC 
		LIMIT array_length(_spc,1)
	)
	SELECT array_agg(id ORDER BY id)
	FROM w
	INTO _spc;

-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			information_schema.constraint_column_usage AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	_table_name4rule := (SELECT table_name FROM target_data.c_ldsity_object WHERE id = _ldsity_object);

-- _adc or _spc is filled, depends on categories (rules), if they are populational, or areal
-- one category (rule) can be defined under more than one superior category, each of them has to be checked

CASE WHEN _adc IS NOT NULL
THEN
	FOREACH adc IN ARRAY _adc
	LOOP
		WITH w_tables AS (
				SELECT
					t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
						CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
						CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',1, ' AS rule_number, ', _case,' AS rul ') ELSE '' END,
						' FROM ', t1.table_name, 
						CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
						') AS ', _table_names_ws[t1.id]) AS table_select
				FROM
					unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
				LEFT JOIN
					(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
					FROM
						(SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.area_domain_category || adc) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_adc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_spc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						) AS t1
					GROUP BY table_name
					) AS t2
				ON t1.table_name = t2.table_name
			)
			SELECT array_agg(table_select ORDER BY id)
			FROM w_tables
			INTO _table_selects;

			SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
				_table_names_ws[1], '.', _primary_keys[1], '
				FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
				INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
				INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
				INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
				_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
			INTO _table1;

			_join_all := NULL; _join := NULL;

			FOR i IN 2..array_length(_table_selects,1) 
			LOOP
				SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
				INTO _join;

				_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
				--raise notice '%', _join_all;
			END LOOP;
			--raise notice '%', _join_all;

			--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
			_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

			_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || _join_all;

			_qall := concat(CASE WHEN _qall IS NOT NULL THEN concat(_qall, ' UNION ALL ') ELSE '' END, _q);
	END LOOP;

WHEN _spc IS NOT NULL
THEN
	FOREACH spc IN ARRAY _spc
	LOOP
		WITH w_tables AS (
				SELECT
					t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
						CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
						CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',1, ' AS rule_number, ', _case,' AS rul ') ELSE '' END,
						' FROM ', t1.table_name, 
						CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
						') AS ', _table_names_ws[t1.id]) AS table_select
				FROM
					unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
				LEFT JOIN
					(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
					FROM
						(SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_adc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.sub_population_category || spc) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_spc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						) AS t1
					GROUP BY table_name
					) AS t2
				ON t1.table_name = t2.table_name
			)
			SELECT array_agg(table_select ORDER BY id)
			FROM w_tables
			INTO _table_selects;

			SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
				_table_names_ws[1], '.', _primary_keys[1], '
				FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
				INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
				INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
				INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
				_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
			INTO _table1;

			_join_all := NULL; _join := NULL;

			FOR i IN 2..array_length(_table_selects,1) 
			LOOP
				SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
				INTO _join;

				_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
				--raise notice '%', _join_all;
			END LOOP;
			--raise notice '%', _join_all;

			--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
			_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

			_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || _join_all;

			_qall := concat(CASE WHEN _qall IS NOT NULL THEN concat(_qall, ' UNION ALL ') ELSE '' END, _q);
	END LOOP;
ELSE
			WITH w_tables AS (
				SELECT
					t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
						CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
						CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',1, ' AS rule_number, ', _case,' AS rul ') ELSE '' END,
						' FROM ', t1.table_name, 
						CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
						') AS ', _table_names_ws[t1.id]) AS table_select
				FROM
					unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
				LEFT JOIN
					(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
					FROM
						(SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_adc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_spc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						) AS t1
					GROUP BY table_name
					) AS t2
				ON t1.table_name = t2.table_name
			)
			SELECT array_agg(table_select ORDER BY id)
			FROM w_tables
			INTO _table_selects;

			SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
				_table_names_ws[1], '.', _primary_keys[1], '
				FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
				INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
				INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
				INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
				_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
			INTO _table1;

			_join_all := NULL; _join := NULL;

			FOR i IN 2..array_length(_table_selects,1) 
			LOOP
				SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
				INTO _join;

				_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
				--raise notice '%', _join_all;
			END LOOP;
			--raise notice '%', _join_all;

			--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
			_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

			_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
			_qall := _q;
END CASE;

	RETURN QUERY EXECUTE
	'WITH w AS (' || _qall || '),
	w2 AS (SELECT
			id, rul
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
	)
	SELECT rul AS compliance_status, count(*)::int AS no_of_objects
	FROM w2
	GROUP BY rul';

--return query select true, _no_of_rules_met, _no_of_objects;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) IS
'Function returns number of objects classified by the rule for given ldsity and attribute_type hierarchy.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) TO public;

-- </function>

-- <function name="fn_check_spc_rule_has_panelref" schema="target_data" src="functions/fn_check_spc_rule_has_panelref.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_spc_rule_has_panelref
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_check_spc_rule_has_panelref(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_spc_rule_has_panelref(_id integer)
RETURNS boolean
AS
$$
BEGIN
	RETURN (SELECT
		CASE WHEN EXISTS( SELECT * FROM target_data.cm_spc2classification_rule WHERE id = _id )
		THEN EXISTS( SELECT * FROM target_data.cm_spc2classrule2panel_refyearset WHERE spc2classification_rule = _id)
		ELSE TRUE
		END);		
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_spc_rule_has_panelref(integer) IS
'Function provides test if classification rule has some records in cm_spc2classrule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_spc_rule_has_panelref(integer) TO public;

-- </function>

-- <function name="fn_check_adc_rule_has_panelref" schema="target_data" src="functions/fn_check_adc_rule_has_panelref.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_adc_rule_has_panelref
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_check_adc_rule_has_panelref(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_adc_rule_has_panelref(_id integer)
RETURNS boolean
AS
$$
BEGIN
	RETURN (SELECT
		CASE WHEN EXISTS( SELECT * FROM target_data.cm_adc2classification_rule WHERE id = _id )
		THEN EXISTS( SELECT * FROM target_data.cm_adc2classrule2panel_refyearset WHERE adc2classification_rule = _id)
		ELSE TRUE
		END);		
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_adc_rule_has_panelref(integer) IS
'Function provides test if classification rule has some records in cm_adc2classrule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_adc_rule_has_panelref(integer) TO public;

-- </function>

-- <function name="fn_trg_check_cm_adc2classrule2panel_refyearset" schema="target_data" src="functions/fn_trg_check_cm_adc2classrule2panel_refyearset.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_adc2classrule2panel_refyearset
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_adc2classrule2panel_refyearset();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_adc2classrule2panel_refyearset() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF (TG_OP = 'DELETE')
		THEN
			IF NOT target_data.fn_check_adc_rule_has_panelref( OLD.adc2classification_rule )
			THEN
				RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_adc2classrule2panel_refyearset: This record [adc2classification_rule = %, refyearset2panel = %] cannot be deleted from the cm_adc2classrule2panel_refyearset table, becouse at least one record for a given classification rule must remain in the table.', old.adc2classification_rule, old.refyearset2panel;
			END IF;

			RETURN OLD;
		END IF;

		IF (TG_OP = 'UPDATE')
		THEN
			IF NOT target_data.fn_check_adc_rule_has_panelref( OLD.adc2classification_rule )
			THEN
				RAISE EXCEPTION 'Error: 02: fn_trg_check_cm_adc2classrule2panel_refyearset: This record [adc2classification_rule = %, refyearset2panel = %] cannot be updated in the cm_adc2classrule2panel_refyearset table, becouse after update at least one record for a given classification rule must remain in the table.', old.adc2classification_rule, old.refyearset2panel;
			END IF;

			RETURN NEW;
		END IF;

	RETURN NULL;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_adc2classrule2panel_refyearset() IS
'This trigger function controls wether given of record can be deleted or updated. After delete or after update must remain at least one record for a given rule in the table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_adc2classrule2panel_refyearset() TO public;


-- </function>

-- <function name="fn_trg_check_cm_spc2classrule2panel_refyearset" schema="target_data" src="functions/fn_trg_check_cm_spc2classrule2panel_refyearset.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_spc2classrule2panel_refyearset
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_spc2classrule2panel_refyearset();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_spc2classrule2panel_refyearset() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF (TG_OP = 'DELETE')
		THEN
			IF NOT target_data.fn_check_spc_rule_has_panelref( OLD.spc2classification_rule )
			THEN
				RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_spc2classrule2panel_refyearset: This record [spc2classification_rule = %, refyearset2panel = %] cannot be deleted from the cm_spc2classrule2panel_refyearset table, becouse at least one record for a given classification rule must remain in the table.', old.spc2classification_rule, old.refyearset2panel;
			END IF;

			RETURN OLD;
		END IF;

		IF (TG_OP = 'UPDATE')
		THEN
			IF NOT target_data.fn_check_spc_rule_has_panelref( OLD.spc2classification_rule )
			THEN
				RAISE EXCEPTION 'Error: 02: fn_trg_check_cm_spc2classrule2panel_refyearset: This record [spc2classification_rule = %, refyearset2panel = %] cannot be updated in the cm_spc2classrule2panel_refyearset table, becouse after update at least one record for a given classification rule must remain in the table.', old.spc2classification_rule, old.refyearset2panel;
			END IF;

			RETURN NEW;
		END IF;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_spc2classrule2panel_refyearset() IS
'This trigger function controls wether given of record can be deleted or updated. After delete or after update must remain at least one record for a given rule in the table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_spc2classrule2panel_refyearset() TO public;


-- </function>

---------------------
-- Triggers
---------------------
DROP TRIGGER trg_cm_adc2classrule2panel_refyearset_before_delete ON target_data.cm_adc2classrule2panel_refyearset;

CREATE CONSTRAINT TRIGGER trg_cm_adc2classrule2panel_refyearset_after_delete
  AFTER DELETE
  ON target_data.cm_adc2classrule2panel_refyearset
  DEFERRABLE INITIALLY DEFERRED
  FOR EACH ROW
  EXECUTE PROCEDURE target_data.fn_trg_check_cm_adc2classrule2panel_refyearset();
COMMENT ON TRIGGER trg_cm_adc2classrule2panel_refyearset_after_delete ON target_data.cm_adc2classrule2panel_refyearset IS
'This trigger controls that after delete record from cm_adc2classrule2panel_refyearset table will remain at least one record in cm_adc2classrule2panel_refyearset table for given classification rule.';


DROP TRIGGER trg_cm_spc2classrule2panel_refyearset_before_delete ON target_data.cm_spc2classrule2panel_refyearset;

CREATE CONSTRAINT TRIGGER trg_cm_spc2classrule2panel_refyearset_after_delete
  AFTER DELETE
  ON target_data.cm_spc2classrule2panel_refyearset
  DEFERRABLE INITIALLY DEFERRED
  FOR EACH ROW
  EXECUTE PROCEDURE target_data.fn_trg_check_cm_spc2classrule2panel_refyearset();
COMMENT ON TRIGGER trg_cm_spc2classrule2panel_refyearset_after_delete ON target_data.cm_spc2classrule2panel_refyearset IS
'This trigger controls that after delete record from cm_spc2classrule2panel_refyearset table will remain at least one record in cm_spc2classrule2panel_refyearset table for given classification rule.';


DROP TRIGGER trg_cm_adc2classrule2panel_refyearset_before_update ON target_data.cm_adc2classrule2panel_refyearset;

CREATE CONSTRAINT TRIGGER trg_cm_adc2classrule2panel_refyearset_after_update
  AFTER UPDATE
  ON target_data.cm_adc2classrule2panel_refyearset
  DEFERRABLE INITIALLY DEFERRED
  FOR EACH ROW
  EXECUTE PROCEDURE target_data.fn_trg_check_cm_adc2classrule2panel_refyearset();
COMMENT ON TRIGGER trg_cm_adc2classrule2panel_refyearset_after_update ON target_data.cm_adc2classrule2panel_refyearset IS
'This trigger controls that after update record in cm_adc2classrule2panel_refyearset table will remain at least one record in cm_adc2classrule2panel_refyearset table for given classification rule.';


DROP TRIGGER trg_cm_spc2classrule2panel_refyearset_before_update ON target_data.cm_spc2classrule2panel_refyearset;

CREATE CONSTRAINT TRIGGER trg_cm_spc2classrule2panel_refyearset_after_update
  AFTER UPDATE
  ON target_data.cm_spc2classrule2panel_refyearset
  DEFERRABLE INITIALLY DEFERRED
  FOR EACH ROW
  EXECUTE PROCEDURE target_data.fn_trg_check_cm_spc2classrule2panel_refyearset();
COMMENT ON TRIGGER trg_cm_spc2classrule2panel_refyearset_after_update ON target_data.cm_spc2classrule2panel_refyearset IS
'This trigger controls that after update record in cm_spc2classrule2panel_refyearset table will remain at least one record in cm_spc2classrule2panel_refyearset table for given classification rule.';



