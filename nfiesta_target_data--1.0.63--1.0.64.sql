--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------
-- DDL
---------------------
COMMENT ON COLUMN target_data.t_adc_hierarchy.dependent IS 'Identification of the type of hierarchy. If the hierarchy codes dependenci between categories then it is true. Otherwise it is only a logical aggregation without any impact on number of categorized objects.';
COMMENT ON COLUMN target_data.t_spc_hierarchy.dependent IS 'Identification of the type of hierarchy. If the hierarchy codes dependenci between categories then it is true. Otherwise it is only a logical aggregation without any impact on number of categorized objects.';

ALTER TABLE target_data.c_area_domain_category DROP CONSTRAINT ukey__c_area_domain_category__description;
ALTER TABLE target_data.c_area_domain_category DROP CONSTRAINT ukey__c_area_domain_category__description_en;
ALTER TABLE target_data.c_sub_population_category DROP CONSTRAINT ukey__c_sub_population_category__description;
ALTER TABLE target_data.c_sub_population_category DROP CONSTRAINT ukey__c_sub_population_category__description_en;

ALTER TABLE target_data.c_area_domain_category ADD CONSTRAINT ukey__c_area_domain_category__label_area_dom UNIQUE (label, area_domain);
ALTER TABLE target_data.c_area_domain_category ADD CONSTRAINT ukey__c_area_domain_category__label_en_area_dom UNIQUE (label_en, area_domain);
ALTER TABLE target_data.c_area_domain_category ADD CONSTRAINT ukey__c_area_domain_category__description_area_dom UNIQUE (description, area_domain);
ALTER TABLE target_data.c_area_domain_category ADD CONSTRAINT ukey__c_area_domain_category__description_en_area_dom UNIQUE (description_en, area_domain);

ALTER TABLE target_data.c_sub_population_category ADD CONSTRAINT ukey__c_sub_population_category__label_sub_pop UNIQUE (label, sub_population);
ALTER TABLE target_data.c_sub_population_category ADD CONSTRAINT ukey__c_sub_population_category__label_en_sub_pop UNIQUE (label_en, sub_population);
ALTER TABLE target_data.c_sub_population_category ADD CONSTRAINT ukey__c_sub_population_category__description_sub_pop UNIQUE (description, sub_population);
ALTER TABLE target_data.c_sub_population_category ADD CONSTRAINT ukey__c_sub_population_category__description_en_sub_pop UNIQUE (description_en, sub_population);


---------------------
-- Functions
---------------------

-- <function name="fn_get_hierarchy" schema="target_data" src="functions/fn_get_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_hierarchy(integer, integer, integer, boolean, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_hierarchy(_areal_or_pop integer, _type_superior integer, _type_inferior integer, _dependent boolean DEFAULT false, _null_inferior boolean DEFAULT true)
RETURNS TABLE (
superior_cat		integer,
inferior_cat		integer,
dependent		boolean
)
AS
$$
BEGIN
	CASE WHEN _areal_or_pop = 100 THEN
	RETURN QUERY
		WITH w_sup AS (
			SELECT t2.id AS variable_superior, t3.variable, t3.dependent
			FROM target_data.c_area_domain AS t1
			INNER JOIN target_data.c_area_domain_category AS t2
			ON t1.id = t2.area_domain AND t1.id = _type_superior
			LEFT JOIN target_data.t_adc_hierarchy AS t3
			ON t2.id = t3.variable_superior
		),
		w_inf AS (
			SELECT t3.variable_superior, t2.id AS variable, t3.dependent
			FROM target_data.c_area_domain AS t1
			INNER JOIN target_data.c_area_domain_category AS t2
			ON t1.id = t2.area_domain AND t1.id = _type_inferior
			LEFT JOIN target_data.t_adc_hierarchy AS t3
			ON t2.id = t3.variable
			WHERE 	CASE WHEN _null_inferior = false THEN t3.variable_superior IS NULL ELSE true END
			 	AND t3.dependent = _dependent
		)
		SELECT variable_superior, variable, w_sup.dependent
		FROM w_sup
		UNION
		SELECT variable_superior, variable, w_inf.dependent
		FROM w_inf
		ORDER BY variable_superior, variable;

	WHEN _areal_or_pop = 200 THEN
	RETURN QUERY
		WITH w_sup AS (
			SELECT t2.id AS variable_superior, t3.variable, t3.dependent
			FROM target_data.c_sub_population AS t1
			INNER JOIN target_data.c_sub_population_category AS t2
			ON t1.id = t2.sub_population AND t1.id = _type_superior
			LEFT JOIN target_data.t_spc_hierarchy AS t3
			ON t2.id = t3.variable_superior
		),
		w_inf AS (
			SELECT t3.variable_superior, t2.id AS variable, t3.dependent
			FROM target_data.c_sub_population AS t1
			INNER JOIN target_data.c_sub_population_category AS t2
			ON t1.id = t2.sub_population AND t1.id = _type_inferior
			LEFT JOIN target_data.t_spc_hierarchy AS t3
			ON t2.id = t3.variable
			WHERE 	CASE WHEN _null_inferior = false THEN t3.variable_superior IS NULL ELSE true END
			 	AND t3.dependent = _dependent
		)
		SELECT variable_superior, variable, w_sup.dependent
		FROM w_sup
		UNION
		SELECT variable_superior, variable, w_inf.dependent
		FROM w_inf
		ORDER BY variable_superior, variable;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_hierarchy(integer, integer, integer, boolean, boolean) IS
'Function returns records from t_adc_hierarchy/t_spc_hierarchy table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_hierarchy(integer, integer, integer, boolean, boolean) TO public;

-- </function>

---------------------
-- Triggers
---------------------


