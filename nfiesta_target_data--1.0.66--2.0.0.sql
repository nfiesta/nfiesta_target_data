--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

ALTER TABLE target_data.c_ldsity ALTER COLUMN description SET NOT NULL;

--------------------------------------------------------------------;
-- Table: c_export_connection
--------------------------------------------------------------------;
-- DROP TABLE target_data.c_export_connection;

CREATE TABLE target_data.c_export_connection
(
  id				integer NOT NULL,
  label				varchar(200) NOT NULL,
  description		text NOT NULL,
  label_en			varchar(200) NOT NULL,
  description_en	text NOT NULL
);

COMMENT ON TABLE target_data.c_export_connection IS 'Table of export connections.';
COMMENT ON COLUMN target_data.c_export_connection.id IS 'Identifier of the connection, primary key.';
COMMENT ON COLUMN target_data.c_export_connection.label IS 'Label of connection.';
COMMENT ON COLUMN target_data.c_export_connection.description IS 'Description of connection.';
COMMENT ON COLUMN target_data.c_export_connection.label_en IS 'English label of connection.';
COMMENT ON COLUMN target_data.c_export_connection.description_en IS 'English description of connection.';


ALTER TABLE target_data.c_export_connection ADD CONSTRAINT pkey__c_export_connection_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_export_connection_id ON target_data.c_export_connection IS 'Primary key.';

ALTER TABLE target_data.c_export_connection ADD CONSTRAINT ukey__c_export_connection__label UNIQUE (label);
COMMENT ON CONSTRAINT ukey__c_export_connection__label ON target_data.c_export_connection IS 'Unique key on column label.';

-- SEQUENCE
CREATE SEQUENCE target_data.c_export_connection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE target_data.c_export_connection_id_seq OWNED BY target_data.c_export_connection.id;
ALTER TABLE ONLY target_data.c_export_connection ALTER COLUMN id SET DEFAULT nextval('target_data.c_export_connection_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: t_etl_target_variable
--------------------------------------------------------------------;
-- DROP TABLE target_data.t_etl_target_variable;

CREATE TABLE target_data.t_etl_target_variable
(
  id					integer NOT NULL,
  export_connection		integer NOT NULL,
  target_variable		integer NOT NULL,
  etl_id				integer NOT NULL
);

COMMENT ON TABLE target_data.t_etl_target_variable IS 'Table of target variables for ETL.';
COMMENT ON COLUMN target_data.t_etl_target_variable.id IS 'Identifier of target variable for ETL, primary key.';
COMMENT ON COLUMN target_data.t_etl_target_variable.export_connection IS 'Identifier of export connection, foreign key to c_export_connection.';
COMMENT ON COLUMN target_data.t_etl_target_variable.target_variable IS 'Identifier of target variable, foreign key to c_target_variable.';
COMMENT ON COLUMN target_data.t_etl_target_variable.etl_id IS 'Identifier of target variable, foreign key to nfiesta.c_target_variable.';

ALTER TABLE target_data.t_etl_target_variable ADD CONSTRAINT pkey__t_etl_target_variable_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__t_etl_target_variable_id ON target_data.t_etl_target_variable IS 'Primary key.';

ALTER TABLE target_data.t_etl_target_variable ADD CONSTRAINT ukey__t_etl_target_variable UNIQUE (export_connection, target_variable, etl_id);
COMMENT ON CONSTRAINT ukey__t_etl_target_variable ON target_data.t_etl_target_variable IS 'Unique key on columns export_connection, target_variable, etl_id.';

ALTER TABLE target_data.t_etl_target_variable
  ADD CONSTRAINT fkey__t_etl_target_variable__c_export_connection FOREIGN KEY (export_connection)
      REFERENCES target_data.c_export_connection (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_etl_target_variable__c_export_connection ON target_data.t_etl_target_variable IS
'Foreign key to table c_export_connection.';

ALTER TABLE target_data.t_etl_target_variable
  ADD CONSTRAINT fkey__t_etl_target_variable__c_target_variable FOREIGN KEY (target_variable)
      REFERENCES target_data.c_target_variable (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_etl_target_variable__c_target_variable ON target_data.t_etl_target_variable IS
'Foreign key to table c_target_variable.';

CREATE INDEX fki__t_etl_target_variable__c_export_connection ON target_data.t_etl_target_variable USING btree (export_connection);
COMMENT ON INDEX target_data.fki__t_etl_target_variable__c_export_connection IS
'BTree index on foreign key fkey__t_etl_target_variable__c_export_connection.';

CREATE INDEX fki__t_etl_target_variable__c_target_variable ON target_data.t_etl_target_variable USING btree (target_variable);
COMMENT ON INDEX target_data.fki__t_etl_target_variable__c_target_variable IS
'BTree index on foreign key fkey__t_etl_target_variable__c_target_variable.';

CREATE INDEX idx__t_etl_target_variable__etl_id ON target_data.t_etl_target_variable USING btree (etl_id);
COMMENT ON INDEX target_data.idx__t_etl_target_variable__etl_id IS
'BTree index on column etl_id.';

-- SEQUENCE
CREATE SEQUENCE target_data.t_etl_target_variable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE target_data.t_etl_target_variable_id_seq OWNED BY target_data.t_etl_target_variable.id;
ALTER TABLE ONLY target_data.t_etl_target_variable ALTER COLUMN id SET DEFAULT nextval('target_data.t_etl_target_variable_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: t_etl_area_domain
--------------------------------------------------------------------;
-- DROP TABLE target_data.t_etl_area_domain;

CREATE TABLE target_data.t_etl_area_domain
(
  id					integer NOT NULL,
  export_connection		integer NOT NULL,
  area_domain			integer[] NOT NULL,
  etl_id				integer NOT NULL
);

COMMENT ON TABLE target_data.t_etl_area_domain IS 'Table of area domains for ETL.';
COMMENT ON COLUMN target_data.t_etl_area_domain.id IS 'Identifier of area domain for ETL, primary key.';
COMMENT ON COLUMN target_data.t_etl_area_domain.export_connection IS 'Identifier of export connection, foreign key to c_export_connection.';
COMMENT ON COLUMN target_data.t_etl_area_domain.area_domain IS 'Identifier of area domain, array of foreign keys to c_area_domain.';
COMMENT ON COLUMN target_data.t_etl_area_domain.etl_id IS 'Identifier of area domain, foreign key to nfiesta.c_area_domain.';

ALTER TABLE target_data.t_etl_area_domain ADD CONSTRAINT pkey__t_etl_area_domain_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__t_etl_area_domain_id ON target_data.t_etl_area_domain IS 'Primary key.';

ALTER TABLE target_data.t_etl_area_domain ADD CONSTRAINT ukey__t_etl_area_domain UNIQUE (export_connection, area_domain, etl_id);
COMMENT ON CONSTRAINT ukey__t_etl_area_domain ON target_data.t_etl_area_domain IS 'Unique key on columns export_connection, area_domain, etl_id.';

ALTER TABLE target_data.t_etl_area_domain
  ADD CONSTRAINT fkey__t_etl_area_domain__c_export_connection FOREIGN KEY (export_connection)
      REFERENCES target_data.c_export_connection (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_etl_area_domain__c_export_connection ON target_data.t_etl_area_domain IS
'Foreign key to table c_export_connection.';

CREATE INDEX fki__t_etl_area_domain__c_export_connection ON target_data.t_etl_area_domain USING btree (export_connection);
COMMENT ON INDEX target_data.fki__t_etl_area_domain__c_export_connection IS
'BTree index on foreign key fkey__t_etl_area_domain__c_export_connection.';

CREATE INDEX idx__t_etl_area_domain__area_domain ON target_data.t_etl_area_domain USING btree (area_domain);
COMMENT ON INDEX target_data.idx__t_etl_area_domain__area_domain IS
'BTree index on column area_domain.';

CREATE INDEX idx__t_etl_area_domain__etl_id ON target_data.t_etl_area_domain USING btree (etl_id);
COMMENT ON INDEX target_data.idx__t_etl_area_domain__etl_id IS
'BTree index on column etl_id.';

-- SEQUENCE
CREATE SEQUENCE target_data.t_etl_area_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE target_data.t_etl_area_domain_id_seq OWNED BY target_data.t_etl_area_domain.id;
ALTER TABLE ONLY target_data.t_etl_area_domain ALTER COLUMN id SET DEFAULT nextval('target_data.t_etl_area_domain_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: t_etl_area_domain_category
--------------------------------------------------------------------;
-- DROP TABLE target_data.t_etl_area_domain_category;

CREATE TABLE target_data.t_etl_area_domain_category
(
  id					integer NOT NULL,
  area_domain_category	integer[] NOT NULL,
  etl_id				integer NOT NULL,
  etl_area_domain		integer NOT NULL
);

COMMENT ON TABLE target_data.t_etl_area_domain_category IS 'Table of area domain categories for ETL.';
COMMENT ON COLUMN target_data.t_etl_area_domain_category.id IS 'Identifier of area domain category for ETL, primary key.';
COMMENT ON COLUMN target_data.t_etl_area_domain_category.area_domain_category IS 'Identifier of area domain category, array of foreign keys to c_area_domain_category.';
COMMENT ON COLUMN target_data.t_etl_area_domain_category.etl_id IS 'Identifier of area domain category, foreign key to nfiesta.c_area_domain_category.';
COMMENT ON COLUMN target_data.t_etl_area_domain_category.etl_area_domain IS 'Identifier of area domain, foreign key to c_area_domain.';

ALTER TABLE target_data.t_etl_area_domain_category ADD CONSTRAINT pkey__t_etl_area_domain_category_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__t_etl_area_domain_category_id ON target_data.t_etl_area_domain_category IS 'Primary key.';

ALTER TABLE target_data.t_etl_area_domain_category ADD CONSTRAINT ukey__t_etl_area_domain_category UNIQUE (area_domain_category, etl_id, etl_area_domain);
COMMENT ON CONSTRAINT ukey__t_etl_area_domain_category ON target_data.t_etl_area_domain_category IS 'Unique key on columns area_domain_category, etl_id, etl_area_domain.';

ALTER TABLE target_data.t_etl_area_domain_category
  ADD CONSTRAINT fkey__t_etl_area_domain_category__t_etl_area_domain FOREIGN KEY (etl_area_domain)
      REFERENCES target_data.t_etl_area_domain (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_etl_area_domain_category__t_etl_area_domain ON target_data.t_etl_area_domain_category IS
'Foreign key to table t_etl_area_domain.';

CREATE INDEX fki__t_etl_area_domain_category__t_etl_area_domain ON target_data.t_etl_area_domain_category USING btree (etl_area_domain);
COMMENT ON INDEX target_data.fki__t_etl_area_domain_category__t_etl_area_domain IS
'BTree index on foreign key fkey__t_etl_area_domain_category__t_etl_area_domain.';

CREATE INDEX idx__t_etl_area_domain_category__area_domain_category ON target_data.t_etl_area_domain_category USING btree (area_domain_category);
COMMENT ON INDEX target_data.idx__t_etl_area_domain_category__area_domain_category IS
'BTree index on column area_domain_category.';

CREATE INDEX idx__t_etl_area_domain_category__etl_id ON target_data.t_etl_area_domain_category USING btree (etl_id);
COMMENT ON INDEX target_data.idx__t_etl_area_domain_category__etl_id IS
'BTree index on column etl_id.';

-- SEQUENCE
CREATE SEQUENCE target_data.t_etl_area_domain_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE target_data.t_etl_area_domain_category_id_seq OWNED BY target_data.t_etl_area_domain_category.id;
ALTER TABLE ONLY target_data.t_etl_area_domain_category ALTER COLUMN id SET DEFAULT nextval('target_data.t_etl_area_domain_category_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: t_etl_sub_population
--------------------------------------------------------------------;
-- DROP TABLE target_data.t_etl_sub_population;

CREATE TABLE target_data.t_etl_sub_population
(
  id					integer NOT NULL,
  export_connection		integer NOT NULL,
  sub_population		integer[] NOT NULL,
  etl_id				integer NOT NULL
);

COMMENT ON TABLE target_data.t_etl_sub_population IS 'Table of sub-populations for ETL.';
COMMENT ON COLUMN target_data.t_etl_sub_population.id IS 'Identifier of sub-population for ETL, primary key.';
COMMENT ON COLUMN target_data.t_etl_sub_population.export_connection IS 'Identifier of export connection, foreign key to c_export_connection.';
COMMENT ON COLUMN target_data.t_etl_sub_population.sub_population IS 'Identifier of sub-population, array of foreign keys to c_sub_population.';
COMMENT ON COLUMN target_data.t_etl_sub_population.etl_id IS 'Identifier of sub-population, foreign key to nfiesta.c_sub_population.';

ALTER TABLE target_data.t_etl_sub_population ADD CONSTRAINT pkey__t_etl_sub_population_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__t_etl_sub_population_id ON target_data.t_etl_sub_population IS 'Primary key.';

ALTER TABLE target_data.t_etl_sub_population ADD CONSTRAINT ukey__t_etl_sub_population UNIQUE (export_connection, sub_population, etl_id);
COMMENT ON CONSTRAINT ukey__t_etl_sub_population ON target_data.t_etl_sub_population IS 'Unique key on columns export_connection, sub_population, etl_id.';

ALTER TABLE target_data.t_etl_sub_population
  ADD CONSTRAINT fkey__t_etl_sub_population__c_export_connection FOREIGN KEY (export_connection)
      REFERENCES target_data.c_export_connection (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_etl_sub_population__c_export_connection ON target_data.t_etl_sub_population IS
'Foreign key to table c_export_connection.';

CREATE INDEX fki__t_etl_sub_population__c_export_connection ON target_data.t_etl_sub_population USING btree (export_connection);
COMMENT ON INDEX target_data.fki__t_etl_sub_population__c_export_connection IS
'BTree index on foreign key fkey__t_etl_sub_population__c_export_connection.';

CREATE INDEX idx__t_etl_sub_population__sub_population ON target_data.t_etl_sub_population USING btree (sub_population);
COMMENT ON INDEX target_data.idx__t_etl_sub_population__sub_population IS
'BTree index on column sub_population.';

CREATE INDEX idx__t_etl_sub_population__etl_id ON target_data.t_etl_sub_population USING btree (etl_id);
COMMENT ON INDEX target_data.idx__t_etl_sub_population__etl_id IS
'BTree index on column etl_id.';

-- SEQUENCE
CREATE SEQUENCE target_data.t_etl_sub_population_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE target_data.t_etl_sub_population_id_seq OWNED BY target_data.t_etl_sub_population.id;
ALTER TABLE ONLY target_data.t_etl_sub_population ALTER COLUMN id SET DEFAULT nextval('target_data.t_etl_sub_population_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: t_etl_sub_population_category
--------------------------------------------------------------------;
-- DROP TABLE target_data.t_etl_sub_population_category;

CREATE TABLE target_data.t_etl_sub_population_category
(
  id						integer NOT NULL,
  sub_population_category	integer[] NOT NULL,
  etl_id					integer NOT NULL,
  etl_sub_population		integer NOT NULL
);

COMMENT ON TABLE target_data.t_etl_sub_population_category IS 'Table of sub-population categories for ETL.';
COMMENT ON COLUMN target_data.t_etl_sub_population_category.id IS 'Identifier of sub-population category for ETL, primary key.';
COMMENT ON COLUMN target_data.t_etl_sub_population_category.sub_population_category IS 'Identifier of sub-population category, array of foreign keys to c_sub_population_category.';
COMMENT ON COLUMN target_data.t_etl_sub_population_category.etl_id IS 'Identifier of sub-population category, foreign key to nfiesta.c_sub_population_category.';
COMMENT ON COLUMN target_data.t_etl_sub_population_category.etl_sub_population IS 'Identifier of sub-population, foreign key to c_sub_population.';

ALTER TABLE target_data.t_etl_sub_population_category ADD CONSTRAINT pkey__t_etl_sub_population_category_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__t_etl_sub_population_category_id ON target_data.t_etl_sub_population_category IS 'Primary key.';

ALTER TABLE target_data.t_etl_sub_population_category ADD CONSTRAINT ukey__t_etl_sub_population_category UNIQUE (sub_population_category, etl_id, etl_sub_population);
COMMENT ON CONSTRAINT ukey__t_etl_sub_population_category ON target_data.t_etl_sub_population_category IS 'Unique key on columns sub_population_category, etl_id, etl_sub_population.';

ALTER TABLE target_data.t_etl_sub_population_category
  ADD CONSTRAINT fkey__t_etl_sub_population_category__t_etl_sub_population FOREIGN KEY (etl_sub_population)
      REFERENCES target_data.t_etl_sub_population (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_etl_sub_population_category__t_etl_sub_population ON target_data.t_etl_sub_population_category IS
'Foreign key to table t_etl_sub_population.';

CREATE INDEX fki__t_etl_sub_population_category__t_etl_sub_population ON target_data.t_etl_sub_population_category USING btree (etl_sub_population);
COMMENT ON INDEX target_data.fki__t_etl_sub_population_category__t_etl_sub_population IS
'BTree index on foreign key fkey__t_etl_sub_population_category__t_etl_sub_population.';

CREATE INDEX idx__t_etl_sub_population_category__sub_population_category ON target_data.t_etl_sub_population_category USING btree (sub_population_category);
COMMENT ON INDEX target_data.idx__t_etl_sub_population_category__sub_population_category IS
'BTree index on column sub_population_category.';

CREATE INDEX idx__t_etl_sub_population_category__etl_id ON target_data.t_etl_sub_population_category USING btree (etl_id);
COMMENT ON INDEX target_data.idx__t_etl_sub_population_category__etl_id IS
'BTree index on column etl_id.';

-- SEQUENCE
CREATE SEQUENCE target_data.t_etl_sub_population_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE target_data.t_etl_sub_population_category_id_seq OWNED BY target_data.t_etl_sub_population_category.id;
ALTER TABLE ONLY target_data.t_etl_sub_population_category ALTER COLUMN id SET DEFAULT nextval('target_data.t_etl_sub_population_category_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: t_etl_log
--------------------------------------------------------------------;
-- DROP TABLE target_data.t_etl_log;

CREATE TABLE target_data.t_etl_log
(
  id						integer NOT NULL,
  etl_target_variable		integer NOT NULL,
  refyearset2panel_mapping	integer NOT NULL,
  etl_time					timestamp with time zone NOT NULL
);

COMMENT ON TABLE target_data.t_etl_log IS 'Table of ETL times for ETL target variable.';
COMMENT ON COLUMN target_data.t_etl_log.id IS 'Database identifier, primary key.';
COMMENT ON COLUMN target_data.t_etl_log.etl_target_variable IS 'Identifier of ETL target varible, foreign key to t_etl_target_variable.';
COMMENT ON COLUMN target_data.t_etl_log.refyearset2panel_mapping IS 'Identifier of combination of panels and reference year sets, foreign key to sdesign.cm_refyearset2panel_mapping.';
COMMENT ON COLUMN target_data.t_etl_log.etl_time IS 'ETL time.';

ALTER TABLE target_data.t_etl_log ADD CONSTRAINT pkey__t_etl_log_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__t_etl_log_id ON target_data.t_etl_log IS 'Primary key.';

ALTER TABLE target_data.t_etl_log ADD CONSTRAINT ukey__t_etl_log UNIQUE (etl_target_variable, refyearset2panel_mapping, etl_time);
COMMENT ON CONSTRAINT ukey__t_etl_log ON target_data.t_etl_log IS 'Unique key on columns etl_target_variable, refyearset2panel_mapping and etl_time.';

ALTER TABLE target_data.t_etl_log
  ADD CONSTRAINT fkey__t_etl_log__t_etl_target_variable FOREIGN KEY (etl_target_variable)
      REFERENCES target_data.t_etl_target_variable (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;

COMMENT ON CONSTRAINT fkey__t_etl_log__t_etl_target_variable ON target_data.t_etl_log IS
'Foreign key to table t_etl_target_variable.';	  

ALTER TABLE target_data.t_etl_log
  ADD CONSTRAINT fkey__t_etl_log__cm_refyearset2panel_mapping FOREIGN KEY (refyearset2panel_mapping)
      REFERENCES sdesign.cm_refyearset2panel_mapping (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;	  

COMMENT ON CONSTRAINT fkey__t_etl_log__cm_refyearset2panel_mapping ON target_data.t_etl_log IS
'Foreign key to table sdesign.cm_refyearset2panel_mapping.';

CREATE INDEX fki__t_etl_log__t_etl_target_variable ON target_data.t_etl_log USING btree (etl_target_variable);
COMMENT ON INDEX target_data.fki__t_etl_log__t_etl_target_variable IS
'BTree index on foreign key fkey__t_etl_log__t_etl_target_variable.';

CREATE INDEX fki__t_etl_log__cm_refyearset2panel_mapping ON target_data.t_etl_log USING btree (refyearset2panel_mapping);
COMMENT ON INDEX target_data.fki__t_etl_log__cm_refyearset2panel_mapping IS
'BTree index on foreign key fkey__t_etl_log__cm_refyearset2panel_mapping.';

CREATE INDEX idx__t_etl_log__etl_time ON target_data.t_etl_log USING btree (etl_time);
COMMENT ON INDEX target_data.idx__t_etl_log__etl_time IS
'BTree index on column etl_time.';

-- SEQUENCE
CREATE SEQUENCE target_data.t_etl_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE target_data.t_etl_log_id_seq OWNED BY target_data.t_etl_log.id;
ALTER TABLE ONLY target_data.t_etl_log ALTER COLUMN id SET DEFAULT nextval('target_data.t_etl_log_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
GRANT SELECT ON TABLE				 target_data.c_export_connection			TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.t_etl_area_domain				TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.t_etl_area_domain_category		TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.t_etl_sub_population			TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.t_etl_sub_population_category	TO app_nfiesta;
GRANT SELECT ON TABLE				 target_data.t_etl_log						TO app_nfiesta;

GRANT SELECT, USAGE ON SEQUENCE			 target_data.c_export_connection_id_seq 			TO public;
GRANT SELECT, USAGE ON SEQUENCE			 target_data.t_etl_area_domain_id_seq 				TO public;
GRANT SELECT, USAGE ON SEQUENCE			 target_data.t_etl_area_domain_category_id_seq 		TO public;
GRANT SELECT, USAGE ON SEQUENCE			 target_data.t_etl_sub_population_id_seq 			TO public;
GRANT SELECT, USAGE ON SEQUENCE			 target_data.t_etl_sub_population_category_id_seq	TO public;
GRANT SELECT, USAGE ON SEQUENCE			 target_data.t_etl_log_id_seq						TO public;

GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.c_export_connection 			TO app_nfiesta_mng;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.t_etl_area_domain 				TO app_nfiesta_mng;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.t_etl_area_domain_category 	TO app_nfiesta_mng;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.t_etl_sub_population 			TO app_nfiesta_mng;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.t_etl_sub_population_category 	TO app_nfiesta_mng;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.t_etl_log 						TO app_nfiesta_mng;

GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.c_export_connection_id_seq 			TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.t_etl_area_domain_id_seq 				TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.t_etl_area_domain_category_id_seq 		TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.t_etl_sub_population_id_seq 			TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.t_etl_sub_population_category_id_seq	TO app_nfiesta_mng;
GRANT USAGE, SELECT, UPDATE ON SEQUENCE		 target_data.t_etl_log_id_seq						TO app_nfiesta_mng;

SELECT pg_catalog.pg_extension_config_dump('target_data.c_export_connection', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.c_export_connection_id_seq', '');

SELECT pg_catalog.pg_extension_config_dump('target_data.t_etl_area_domain', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_etl_area_domain_id_seq', '');

SELECT pg_catalog.pg_extension_config_dump('target_data.t_etl_area_domain_category', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_etl_area_domain_category_id_seq', '');

SELECT pg_catalog.pg_extension_config_dump('target_data.t_etl_sub_population', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_etl_sub_population_id_seq', '');

SELECT pg_catalog.pg_extension_config_dump('target_data.t_etl_sub_population_category', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_etl_sub_population_category_id_seq', '');

SELECT pg_catalog.pg_extension_config_dump('target_data.t_etl_log', '');
SELECT pg_catalog.pg_extension_config_dump('target_data.t_etl_log_id_seq', '');
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_state_or_change => add columns
--------------------------------------------------------------------;
/*
ALTER TABLE target_data.c_state_or_change ADD COLUMN label_en character varying(200);
ALTER TABLE target_data.c_state_or_change ADD COLUMN description_en text;
COMMENT ON COLUMN target_data.c_state_or_change.label_en IS 'English label of state or change.';
COMMENT ON COLUMN target_data.c_state_or_change.description_en IS 'English description of state or change.';

UPDATE target_data.c_state_or_change
SET
	label = 'stavová veličina',
	description = 'Stavová veličina.',
	label_en = 'state variable',
	description_en = 'State variable.'
where
	id = 100;

UPDATE target_data.c_state_or_change
SET
	label = 'změnová veličina',
	description = 'Změnová veličina.',
	label_en = 'change variable',
	description_en = 'Change variable.'
where
	id = 200;

INSERT INTO target_data.c_state_or_change(id,label,description,label_en,description_en) VALUES
(300,'dynamická veličina','Dynamická veličina.','dynamic variable','Dynamic variable.');

ALTER TABLE target_data.c_state_or_change ALTER COLUMN label_en SET NOT NULL;
ALTER TABLE target_data.c_state_or_change ALTER COLUMN description_en SET NOT NULL;
*/

INSERT INTO target_data.c_state_or_change(id,label,description) VALUES
(300,'dynamic variable','Dynamic variable.');
--------------------------------------------------------------------;



-- ETL FUNCTIONS



-- <function name="fn_etl_get_export_connections" schema="target_data" src="functions/etl/fn_etl_get_export_connections.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_export_connections
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_export_connections() CASCADE;

create or replace function target_data.fn_etl_get_export_connections()
returns table
(
	id				integer,
	label			character varying(200),
	description		text,
	label_en		character varying(200),
	description_en	text
)
as
$$
begin
	return query
	select t.id, t.label, t.description, t.label_en, t.description_en
	from target_data.c_export_connection as t;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_export_connections() is
'Function returns records from c_export_connection table.';

grant execute on function target_data.fn_etl_get_export_connections() to public;
-- </function>



-- <function name="fn_etl_save_export_connection" schema="target_data" src="functions/etl/fn_etl_save_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_save_export_connection(varchar, text, varchar, text, integer) CASCADE;

create or replace function target_data.fn_etl_save_export_connection
(
	_label				varchar(200),
	_description		text,
	_label_en			varchar(200),
	_description_en		text,
	_id					integer default null::integer
)
returns integer
as
$$
begin
	if _label is null or _description is null or _label_en is null or _description_en is null
	then
		raise exception 'Error 01: fn_etl_save_export_connection: Mandatory input of label, description, label_en, or description_en is null (%, %, %, %).', _label, _description, _label_en, _description_en;
	end if; 

	if _id is null
	then
		insert into target_data.c_export_connection(label, description, label_en, description_en)
		select _label, _description, _label_en, _description_en
		returning id
		into _id;
	else
		update target_data.c_export_connection
		set
			label = _label,
			description = _description,
			label_en = _label_en,
			description_en = _description_en
		where
			target_data.id = _id;
	end if;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_save_export_connection(varchar, text, varchar, text, integer) is
'Function inserts a record into table c_export_connection based on given parameters.';

grant execute on function target_data.fn_etl_save_export_connection(varchar, text, varchar, text, integer) to public;
-- </function>



-- <function name="fn_etl_get_target_variable" schema="target_data" src="functions/etl/fn_etl_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(character varying) CASCADE;

create or replace function target_data.fn_etl_get_target_variable
(
	_national_language	character varying(2) default 'en'::character varying(2)
)
returns table
(
	id			integer,
	metadata	json
)
as
$$
declare
begin
		if _national_language is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable: Input argument _national_language must not be null!';
		end if;

		return query
		with
		w1 as	(
				select distinct cmltv.target_variable from target_data.cm_ldsity2target_variable as cmltv where cmltv.id in
				(select distinct cmlcs.ldsity2target_variable from target_data.cm_ldsity2target2categorization_setup as cmlcs where cmlcs.categorization_setup in
				(select distinct tad.categorization_setup from target_data.t_available_datasets as tad where tad.id in
				(select distinct tlv.available_datasets from target_data.t_ldsity_values as tlv)))
				)
		,w2 as	(
				select ctv.* from target_data.c_target_variable as ctv
				where ctv.id in (select w1.target_variable from w1)
				)
		,w3 as	(
				select
						w2.id,
						target_data.fn_etl_get_target_variable_metadata(w2.id,_national_language) as metadata
				from
						w2
				)
		,w4 as	(
				select
						w3.id,
						w3.metadata->_national_language as metadata
				from
						w3
				)
				select
						w4.id,
						json_build_object(_national_language,w4.metadata) as metadata
				from
						w4 order by w4.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_target_variable(character varying) IS
'The function returs available target variables for ETL.';

grant execute on function target_data.fn_etl_get_target_variable(character varying) to public;
-- </function>



-- <function name="fn_etl_check_target_variable" schema="target_data" src="functions/etl/fn_etl_check_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_target_variable(integer, integer) CASCADE;

create or replace function target_data.fn_etl_check_target_variable
(
	_export_connection		integer,
	_target_variable		integer
)
returns integer
as
$$
declare
		_id					integer;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_target_variable: Input argument _export_connection must not by NULL!';
		end if; 
	
		if _target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_target_variable: Input argument _target_variable must not by NULL!';
		end if;
	
		select id from target_data.t_etl_target_variable
		where export_connection = _export_connection
		and target_variable = _target_variable
		into _id;
	
		return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_target_variable(integer, integer) is
'Function returns record ID from table t_etl_target_variable based on given parameters.';

grant execute on function target_data.fn_etl_check_target_variable(integer, integer) to public;
-- </function>



-- <function name="fn_etl_get_target_variable_metadata" schema="target_data" src="functions/etl/fn_etl_get_target_variable_metadata.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_target_variable_metadata
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable_metadata(integer, character varying) CASCADE;

create or replace function target_data.fn_etl_get_target_variable_metadata
(
	_target_variable	integer,
	_national_language	character varying(2) default 'en'::character varying(2)
)
returns json
as
$$
declare
		_s_or_ch_label								varchar;
		_s_or_ch_description						text;
		_s_or_ch_label_en							varchar;
		_s_or_ch_description_en						text;
		---------------------------------------------------
		_tv_label									varchar;
		_tv_description								text;
		_tv_label_en								varchar;
		_tv_description_en							text;
		---------------------------------------------------
		_unit_of_measure_array						integer[];
		_u_of_m_label								varchar;
		_u_of_m_description							text;
		_u_of_m_label_en							varchar;
		_u_of_m_description_en						text;
		---------------------------------------------------
		_array_id									integer[];
		---------------------------------------------------
		_i_cm_ldsity								integer;
		_i_cm_ldsity_object_type					integer;
		_i_cm_area_domain_category					integer[];
		_i_cm_sub_population_category				integer[];
		_i_cm_version								integer;
		_i_cm_use_negative							boolean;
		---------------------------------------------------
		_i_clot_label								varchar;
		_i_clot_description							text;
		_i_clot_label_en							varchar;
		_i_clot_description_en						text;
		---------------------------------------------------
		_i_cv_label									varchar;
		_i_cv_description							text;
		_i_cv_label_en								varchar;
		_i_cv_description_en						text;
		---------------------------------------------------
		_i_cl_ldsity_object							integer;
		_i_cl_label									varchar;
		_i_cl_description							text;
		_i_cl_label_en								varchar;
		_i_cl_description_en						text;
		_i_cl_definition_variant					integer[];
		_i_cl_area_domain_category					integer[];
		_i_cl_sub_population_category				integer[];
		---------------------------------------------------
		_i_clo_label								varchar;
		_i_clo_description							text;
		_i_clo_label_en								varchar;
		_i_clo_description_en						text;
		---------------------------------------------------
		_i_definition_variant						integer[];
		_i_area_domain_category						integer[];
		_i_sub_population_category					integer[];
		---------------------------------------------------
		_i_definition_variant_label					varchar[];
		_i_definition_variant_description			text[];
		_i_definition_variant_label_en				varchar[];
		_i_definition_variant_description_en		text[];
		---------------------------------------------------
		_i_area_domain_category_label				varchar[];
		_i_area_domain_category_description			text[];
		_i_area_domain_category_label_en			varchar[];
		_i_area_domain_category_description_en		text[];
		---------------------------------------------------
		_i_sub_population_category_label			varchar[];
		_i_sub_population_category_description		text[];
		_i_sub_population_category_label_en			varchar[];
		_i_sub_population_category_description_en	text[];
		---------------------------------------------------
		_i_object_json_nl							json;
		_objects_json_nl							json;
		_json_nl									json;
		---------------------------------------------------
		_i_object_json_en							json;
		_objects_json_en							json;
		_json_en									json;
		---------------------------------------------------
		_json_res									json;
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable_metadata: Input argument _target_variable must not by NULL!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_get_target_variable_metadata: Input argument _national_language must not by NULL!';
		end if;		
		---------------------------------------------------
		-- state or change
		---------------------------------------------------
		select
				label,
				description,
				label as label_en,
				description as description_en
		from
				target_data.c_state_or_change
		where
				id = (select state_or_change from target_data.c_target_variable where id = _target_variable)
		into
				_s_or_ch_label,
				_s_or_ch_description,
				_s_or_ch_label_en,
				_s_or_ch_description_en;
		---------------------------------------------------
		-- indicator
		---------------------------------------------------
		select
				label,
				description,
				label_en,
				description_en
		from
				target_data.c_target_variable
		where
				id = _target_variable
		into
				_tv_label,
				_tv_description,
				_tv_label_en,
				_tv_description_en;
		---------------------------------------------------
		-- unit of measure
		---------------------------------------------------
		select array_agg(unit_of_measure) from target_data.c_ldsity where id in
		(select ldsity from target_data.cm_ldsity2target_variable where target_variable = _target_variable and ldsity_object_type = 100)
		into _unit_of_measure_array;

		if	(
			select count(t2.res) is distinct from 1
			from (select distinct t1.res from (select unnest(_unit_of_measure_array) as res) as t1) as t2
			)
		then
			raise exception 'Error 02: fn_etl_get_target_variable_metadata: More different unit of measures for target variable = %!',_target_variable;
		end if;
	
		select
				label,
				description,
				label_en,
				description_en
		from
				target_data.c_unit_of_measure where id = _unit_of_measure_array[1]
		into
				_u_of_m_label,
				_u_of_m_description,
				_u_of_m_label_en,
				_u_of_m_description_en;
		---------------------------------------------------
		-- OBJECTS --	
		select array_agg(id order by ldsity_object_type, id)
		from target_data.cm_ldsity2target_variable where target_variable = _target_variable
		into _array_id;
		---------------------------------------------------
		-- cycle for objects [LOCAL DENSITIES]
		---------------------------------------------------
		for i in 1..array_length(_array_id,1)
		loop
			select
					ldsity,
					ldsity_object_type,
					area_domain_category,
					sub_population_category,
					version,
					use_negative
			from
					target_data.cm_ldsity2target_variable where id = _array_id[i]
			into
					_i_cm_ldsity,
					_i_cm_ldsity_object_type,
					_i_cm_area_domain_category,		-- can be NULL
					_i_cm_sub_population_category,	-- can be NULL
					_i_cm_version,					-- can be NULL
					_i_cm_use_negative;
			-------------------------------------
			-- ldsity_object_type
			-------------------------------------
			select
					label,
					description,
					label as label_en,
					description as description_en
			from
					target_data.c_ldsity_object_type
			where
					id = _i_cm_ldsity_object_type
			into
					_i_clot_label,
					_i_clot_description,
					_i_clot_label_en,
					_i_clot_description_en;
			-------------------------------------
			-- version
			-------------------------------------	
			select
					label,
					description,
					label_en,
					description_en
			from
					target_data.c_version where id = _i_cm_version -- case when _i_cm_version is null then zero rows is returned
			into
					_i_cv_label,			-- can be NULL
					_i_cv_description,		-- can be NULL
					_i_cv_label_en,			-- can be NULL
					_i_cv_description_en;	-- can be NULL
			-------------------------------------
			-- contribution
			-------------------------------------				
			select
					ldsity_object,
					label,
					description,
					label_en,
					description_en,
					definition_variant,
					area_domain_category,
					sub_population_category
			from
					target_data.c_ldsity where id = _i_cm_ldsity
			into
					_i_cl_ldsity_object,
					_i_cl_label,
					_i_cl_description,
					_i_cl_label_en,
					_i_cl_description_en,
					_i_cl_definition_variant,		-- can be NULL
					_i_cl_area_domain_category,		-- can be NULL
					_i_cl_sub_population_category;	-- can be NULL
			-------------------------------------
			-- object
			-------------------------------------
			select
					label,
					description,
					label_en,
					description_en
			from
					target_data.c_ldsity_object where id = _i_cl_ldsity_object
			into
					_i_clo_label,
					_i_clo_description,
					_i_clo_label_en,
					_i_clo_description_en;
			-------------------------------------		
			_i_definition_variant := _i_cl_definition_variant;												-- can be NULL
			_i_area_domain_category := _i_cl_area_domain_category || _i_cm_area_domain_category;			-- can be NULL
			_i_sub_population_category := _i_cl_sub_population_category || _i_cm_sub_population_category;	-- can be NULL
			-------------------------------------
			if _i_definition_variant is null
			then
					_i_definition_variant_label := null::varchar[];
					_i_definition_variant_description := null::text[];
					_i_definition_variant_label_en := null::varchar[];
					_i_definition_variant_description_en := null::text[];					
			else
					select
							array_agg(label order by label),
							array_agg(description order by label)
					from
							target_data.c_definition_variant where id in (select unnest(_i_definition_variant))
					into
							_i_definition_variant_label,
							_i_definition_variant_description;
						
					select
							array_agg(label_en order by label_en),
							array_agg(description_en order by label_en)
					from
							target_data.c_definition_variant where id in (select unnest(_i_definition_variant))
					into
							_i_definition_variant_label_en,
							_i_definition_variant_description_en;												
			end if;
			-------------------------------------
			if _i_area_domain_category is null
			then
					_i_area_domain_category_label := null::varchar[];
					_i_area_domain_category_description := null::text[];
					_i_area_domain_category_label_en := null::varchar[];
					_i_area_domain_category_description_en := null::text[];					
			else				
					select
							array_agg(label order by label),
							array_agg(description order by label)
					from
							target_data.c_area_domain_category
					where id in	(
								select area_domain_category from target_data.cm_adc2classification_rule where id in
								(select unnest(_i_area_domain_category))
								)
					into
							_i_area_domain_category_label,
							_i_area_domain_category_description;
						
					select
							array_agg(label_en order by label_en),
							array_agg(description_en order by label_en)
					from
							target_data.c_area_domain_category
					where id in	(
								select area_domain_category from target_data.cm_adc2classification_rule where id in
								(select unnest(_i_area_domain_category))
								)
					into
							_i_area_domain_category_label_en,
							_i_area_domain_category_description_en;				
			end if;
			-------------------------------------
			if _i_sub_population_category is null
			then
					_i_sub_population_category_label := null::varchar[];
					_i_sub_population_category_description := null::text[];
					_i_sub_population_category_label_en := null::varchar[];
					_i_sub_population_category_description_en := null::text[];					
			else				
					select
							array_agg(label order by label),
							array_agg(description order by label)
					from
							target_data.c_sub_population_category
					where id in	(
								select sub_population_category from target_data.cm_spc2classification_rule where id in
								(select unnest(_i_sub_population_category))
								)
					into
							_i_sub_population_category_label,
							_i_sub_population_category_description;
						
					select
							array_agg(label_en order by label_en),
							array_agg(description_en order by label_en)
					from
							target_data.c_sub_population_category
					where id in	(
								select sub_population_category from target_data.cm_spc2classification_rule where id in
								(select unnest(_i_sub_population_category))
								)
					into
							_i_sub_population_category_label_en,
							_i_sub_population_category_description_en;				
			end if;
			-------------------------------------
			-------------------------------------
			-- get JSON for i- local density

			-- national language
			select json_build_object(
									'object type label',_i_clot_label,				-- c_ldsity_object_type
									'object type description',_i_clot_description,	-- c_ldsity_object_type
									'object label',_i_clo_label,					-- c_ldsity_object
									'object description',_i_clo_description,		-- c_ldsity_object
									'label',_i_cl_label,							-- c_ldsity
									'description',_i_cl_description,				-- c_ldsity
									'version',json_build_object	(
																'label',_i_cv_label,
																'description',_i_cv_description
																),
									'definition variant',json_build_object	(
																			'label',_i_definition_variant_label,
																			'description',_i_definition_variant_description
																			),
									'area domain',json_build_object	(
																	'label',_i_area_domain_category_label,
																	'description',_i_area_domain_category_description
																	),
									'population',json_build_object	(
																	'label',_i_sub_population_category_label,
																	'description',_i_sub_population_category_description
																	),
									'use_negative',_i_cm_use_negative
									)
			into _i_object_json_nl;

			-- english language
			select json_build_object(
									'object type label',_i_clot_label_en,				-- c_ldsity_object_type
									'object type description',_i_clot_description_en,	-- c_ldsity_object_type
									'object label',_i_clo_label_en,						-- c_ldsity_object
									'object description',_i_clo_description_en,			-- c_ldsity_object
									'label',_i_cl_label_en,								-- c_ldsity
									'description',_i_cl_description_en,					-- c_ldsity
									'version',json_build_object	(
																'label',_i_cv_label_en,
																'description',_i_cv_description_en
																),
									'definition variant',json_build_object	(
																			'label',_i_definition_variant_label_en,
																			'description',_i_definition_variant_description_en
																			),
									'area domain',json_build_object	(
																	'label',_i_area_domain_category_label_en,
																	'description',_i_area_domain_category_description_en
																	),
									'population',json_build_object	(
																	'label',_i_sub_population_category_label_en,
																	'description',_i_sub_population_category_description_en
																	),
									'use_negative',_i_cm_use_negative
									)
			into _i_object_json_en;
			-------------------------------------
			-------------------------------------
			if i = 1
			then
					select json_agg(_i_object_json_nl)
					into _objects_json_nl;
				
					select json_agg(_i_object_json_en)
					into _objects_json_en;				
			else
					with
					w1 as	(
							select
									row_number() over () as new_id,
									json_array_elements(_objects_json_nl) as res
							)
					,w2 as	(
							select
									(row_number() over ()) + (select max(new_id) from w1) as new_id,
									_i_object_json_nl as res
							)
					,w3 as	(
							select new_id, res from w1 union all
							select new_id, res from w2
							)
					select json_agg(w3.res order by w3.new_id) from w3
					into _objects_json_nl;
				
					with
					w1 as	(
							select
									row_number() over () as new_id,
									json_array_elements(_objects_json_en) as res
							)
					,w2 as	(
							select
									(row_number() over ()) + (select max(new_id) from w1) as new_id,
									_i_object_json_en as res
							)
					,w3 as	(
							select new_id, res from w1 union all
							select new_id, res from w2
							)
					select json_agg(w3.res order by w3.new_id) from w3
					into _objects_json_en;				
			end if;					
		end loop;
		-------------------------------------
		select json_build_object
			(
			'indicator', json_build_object('label',_tv_label,'description',_tv_description),
			'state or change', json_build_object('label',_s_or_ch_label,'description',_s_or_ch_description),
			'unit',json_build_object('label',_u_of_m_label,'description',_u_of_m_description),
			'local densities',_objects_json_nl
			)
		into _json_nl;
		-------------------------------------
		select json_build_object
			(
			'indicator', json_build_object('label',_tv_label_en,'description',_tv_description_en),
			'state or change', json_build_object('label',_s_or_ch_label_en,'description',_s_or_ch_description_en),
			'unit',json_build_object('label',_u_of_m_label_en,'description',_u_of_m_description_en),
			'local densities',_objects_json_en
			)
		into _json_en;
		-------------------------------------
		if _national_language = 'en'
		then
			select json_build_object
				(
				'en',_json_en
				)
			into _json_res;		
		else
			select json_build_object
				(
				_national_language,_json_nl,	-- set national language from input argument
				'en',_json_en
				)
			into _json_res;
		end if;
		-------------------------------------
	return _json_res;
	---------------------------------------------
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_target_variable_metadata(integer, character varying) IS
'The function returs metadata for input target variable.';

grant execute on function target_data.fn_etl_get_target_variable_metadata(integer, character varying) to public;
-- </function>



-- <function name="fn_etl_save_target_variable" schema="target_data" src="functions/etl/fn_etl_save_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_save_target_variable(integer, integer, integer) CASCADE;

create or replace function target_data.fn_etl_save_target_variable
(
	_export_connection		integer,
	_target_variable		integer,
	_etl_id					integer
)
returns integer
as
$$
declare
		_id integer;
begin
	if _export_connection is null
	then
		raise exception 'Error 01: fn_etl_save_target_variable: Input argument _export_connection must not by NULL!';
	end if; 

	if _target_variable is null
	then
		raise exception 'Error 02: fn_etl_save_target_variable: Input argument _target_variable must not by NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 03: fn_etl_save_target_variable: Input argument _etl_id must not by NULL!';
	end if;

	insert into target_data.t_etl_target_variable(export_connection, target_variable, etl_id)
	select _export_connection, _target_variable, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_save_target_variable(integer, integer, integer) is
'Function inserts a record into table t_etl_target_variable based on given parameters.';

grant execute on function target_data.fn_etl_save_target_variable(integer, integer, integer) to public;
-- </function>



-- <function name="fn_etl_supplement_categorization_setups" schema="target_data" src="functions/etl/fn_etl_supplement_categorization_setups.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_supplement_categorization_setups
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_supplement_categorization_setups(integer[]) CASCADE;

create or replace function target_data.fn_etl_supplement_categorization_setups
(
	_categorization_setups	integer[]
)
returns integer[]
as
$$
declare
		_categorization_setups_adc		integer[];
		_categorization_setups_spc		integer[];
		_res							integer[];
begin
		if _categorization_setups is null
		then
			raise exception 'Error 01: fn_etl_supplement_categorization_setups: Input argument _categorization_setups must not by NULL!';
		end if;
		
		with
		w as	(
				select distinct categorization_setup
				from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(	
					select id from target_data.cm_ldsity2target_variable
					where target_variable =
								(
								select distinct target_variable from target_data.cm_ldsity2target_variable
								where id in	(
											select ldsity2target_variable
											from target_data.cm_ldsity2target2categorization_setup
											where categorization_setup in (select unnest(_categorization_setups))
											)
								)
					)
				and (
					adc2classification_rule is null
					or array_length(adc2classification_rule,1) <= 
						(select max(t.res) from
						(select array_length(adc2classification_rule,1) as res
						from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						and adc2classification_rule is not null) as t)
					)
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups_adc;

		with
		w as	(
				select distinct categorization_setup
				from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(	
					select id from target_data.cm_ldsity2target_variable
					where target_variable =
								(
								select distinct target_variable from target_data.cm_ldsity2target_variable
								where id in	(
											select ldsity2target_variable
											from target_data.cm_ldsity2target2categorization_setup
											where categorization_setup in (select unnest(_categorization_setups))
											)
								)
					)
				and (
					spc2classification_rule is null
					or array_length(spc2classification_rule,1) <= 
						(select max(t.res) from
						(select array_length(spc2classification_rule,1) as res
						from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						and spc2classification_rule is not null) as t)
					)
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups_spc;

		with
		w as	(
				select a.categorization_setup from
					(select unnest(_categorization_setups_adc) as categorization_setup) as a
				inner join
					(select unnest(_categorization_setups_spc) as categorization_setup) as b
				on a.categorization_setup = b.categorization_setup
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _res;
		
		return _res;

		/*
		-------------------------------------------------------------
		-------------------------------------------------------------
		-- add missing categorization_setups which are not prepared in t_available_datasets for incomming target variable,
		-- list of panels and list of reference year sets, but are needed to check additivity (especially are needed in
		-- process ETL Variable hierarchy)
		with
		w1 as	(
				select
						ldsity2target_variable,
						case
						when adc2classification_rule is null
							then array[0]
							else (select id_type from target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule))
						end as id_adt,
						case
						when spc2classification_rule is null
							then array[0]
							else (select id_type from target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule))
						end as id_spt
				from target_data.cm_ldsity2target2categorization_setup
				where categorization_setup in (select unnest(_categorization_setups))
				)
		,w2 as	(select distinct ldsity2target_variable, id_adt, id_spt from w1)
		,w3 as	(
				select
						id,
						ldsity2target_variable,
						categorization_setup,
						case
						when adc2classification_rule is null
							then array[0]
							else (select id_type from target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule))
						end as id_adt,
						case
						when spc2classification_rule is null
							then array[0]
							else (select id_type from target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule))
						end as id_spt
				from target_data.cm_ldsity2target2categorization_setup	
				where ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = _target_variable)
				)
		,w4 as	(
				select w3.* from w3 inner join w2
				on	(
					w3.ldsity2target_variable = w2.ldsity2target_variable
					and target_data.fn_etl_array_compare(w3.id_adt,w2.id_adt)
					and target_data.fn_etl_array_compare(w3.id_spt,w2.id_spt)
					)
				)
		select array_agg(t.categorization_setup order by t.categorization_setup) from
		(select distinct categorization_setup from w4) as t
		into _categorization_setups;
		-------------------------------------------------------------
		-------------------------------------------------------------
		*/
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_supplement_categorization_setups(integer[]) is
'Function supplement of missing categorie for input categorization setups.';

grant execute on function target_data.fn_etl_supplement_categorization_setups(integer[]) to public;
-- </function>



-- <function name="fn_etl_get_panel_refyearset_combinations" schema="target_data" src="functions/etl/fn_etl_get_panel_refyearset_combinations.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_panel_refyearset_combinations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_panel_refyearset_combinations(integer) CASCADE;

create or replace function target_data.fn_etl_get_panel_refyearset_combinations
(
	_id_t_etl_target_variable	integer
)
returns table
(
	refyearset2panel		integer,
	panel					varchar,	
	reference_year_set		varchar
)
as
$$
declare
		_res_panels_and_reference_year_sets		json;
		_country_array							integer[];
		_res_country							varchar;
		_target_variable						integer;
		_etl_id									integer;
		_categorization_setups					integer[];
		_res									json;
begin		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_panel_refyearset_combinations: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;

		select
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_target_variable;

		if _target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_panel_refyearset_combinations: For input argument _id_t_etl_target_variable = % not exists any target_variable in table t_etl_target_variable!',_id_t_etl_target_variable;
		end if;

		return query
		with
		w1 as	(
				select tad.* from target_data.t_available_datasets as tad where tad.categorization_setup in
					(
					select distinct cmlcs.categorization_setup
					from target_data.cm_ldsity2target2categorization_setup as cmlcs
					where cmlcs.ldsity2target_variable in
						(
						select cmltv.id from target_data.cm_ldsity2target_variable as cmltv
						where cmltv.target_variable = _target_variable
						)
					)
				)
		,w2 as	(
				select distinct tlv.available_datasets from target_data.t_ldsity_values as tlv
				where tlv.available_datasets in (select w1.id from w1)
				and is_latest = true
				)
		,w3 as	(
				select distinct tad1.panel, tad1.reference_year_set
				from target_data.t_available_datasets as tad1 where tad1.id in
				(select w2.available_datasets from w2)
				)
		,w4 as	(
				select
						w3.panel as panel_id,
						w3.reference_year_set as reference_year_set_id,
						crpm.id as refyearset2panel,
						tp.panel,
						trys.reference_year_set from w3
				inner join sdesign.cm_refyearset2panel_mapping as crpm on w3.panel = crpm.panel and w3.reference_year_set = crpm.reference_year_set
				inner join sdesign.t_panel as tp on w3.panel = tp.id
				inner join sdesign.t_reference_year_set as trys on w3.reference_year_set = trys.id
				)
		select
				w4.refyearset2panel,
				w4.panel,
				w4.reference_year_set
		from
				w4 order by refyearset2panel;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_panel_refyearset_combinations(integer) is
'Function returns list of combinations of panels and reference year sets for given target variable that are available for ETL.';

grant execute on function target_data.fn_etl_get_panel_refyearset_combinations(integer) to public;
-- </function>



-- <function name="fn_etl_get_variables" schema="target_data" src="functions/etl/fn_etl_get_variables.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_variables
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_variables(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_get_variables
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns json
as
$$
declare
		_res_panels_and_reference_year_sets		json;
		_country_array							integer[];
		_res_country							varchar;
		_export_connection						integer;
		_target_variable						integer;
		_etl_id									integer;
		_categorization_setups					integer[];
		_res									json;

		_core_and_division						boolean;
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_variables: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_variables: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;

		-- get labels for combinations of panels and reference year sets
		with
		w1 as	(
				select id, panel, reference_year_set from sdesign.cm_refyearset2panel_mapping
				where id in (select unnest(array[_refyearset2panel_mapping]))
				order by id
				)
		,w2 as	(
				select
						w1.id,
						w1.panel,
						w1.reference_year_set,
						tp.panel as panel_label,
						trys.reference_year_set as reference_year_set_label
				from
							w1
				inner join 	sdesign.t_panel as tp on w1.panel = tp.id
				inner join	sdesign.t_reference_year_set as trys on w1.reference_year_set = trys.id
				order by	w1.id
				)
		select
		json_agg(json_build_object(
				'panel',  w2.panel_label,
				'reference_year_set', w2.reference_year_set_label)
				)
		from w2
		into _res_panels_and_reference_year_sets;
	
		select array_agg(tss.country order by tss.country) from sdesign.t_strata_set as tss where tss.id in
		(select strata_set from sdesign.t_stratum where id in
		(select stratum from sdesign.t_panel
		where id in ((select panel from sdesign.cm_refyearset2panel_mapping where id in (select unnest(array[_refyearset2panel_mapping]))))))
		into _country_array;
	
		if array_length(_country_array,1) is distinct from 1
		then
			raise exception 'Error 03: fn_etl_get_variables: Input argument _refyearset2panel_mapping contains values for two or more different countries!';
		end if;
	
		select label from sdesign.c_country where id = _country_array[1]
		into _res_country;
	
		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;				

		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/					
	
		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups for target variable that is reduced by
				 -- combination of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(-- list of IDs of available datasets from t_ldsity_values table
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(-- list of categorization setups that are selected for ETL
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
		
		-- get attributes and result as a json
		with
		w_etl_ad as		(
						select * from target_data.t_etl_area_domain	where export_connection = _export_connection
						)
		,w_etl_adc as	(
						select * from target_data.t_etl_area_domain_category where etl_area_domain in (select id from w_etl_ad)
						)
		,w_etl_sp as	(
						select * from target_data.t_etl_sub_population where export_connection = _export_connection
						)
		,w_etl_spc as	(
						select * from target_data.t_etl_sub_population_category where etl_sub_population in (select id from w_etl_sp)
						)
		,w1 as	(
				select
					id,
					ldsity2target_variable,
					categorization_setup,
					_core_and_division as core_and_division,				
					(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type as id_spt_orig,
					(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type as id_adt_orig,
					(target_data.fn_get_category4classification_rule_id('spc', t.spc2classification_rule)).id_category as id_spc_orig,
					(target_data.fn_get_category4classification_rule_id('adc', t.adc2classification_rule)).id_category as id_adc_orig,
					(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_spt_orig,
					(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_adt_orig,
					(target_data.fn_get_category4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_spc_orig,
					(target_data.fn_get_category4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_adc_orig
				from
					target_data.cm_ldsity2target2categorization_setup as t
				where
					ldsity2target_variable
				in
						(
						select id from target_data.cm_ldsity2target_variable
						where target_variable = _target_variable
						)
				and categorization_setup in (select unnest(_categorization_setups))
				)
		,w2 as	(
				select
						w1.*,
						cltv.ldsity_object_type as core_or_division
				from
						w1
						inner join target_data.cm_ldsity2target_variable as cltv on w1.ldsity2target_variable = cltv.id
				)
		,w3 as	(				
				select
						w2.*,
						-------------
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w2a.id_spt_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w2a.id_spc_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w2a.id_adt_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w2a.id_adc_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adc,
						-------------
						case
							when core_and_division = false
								then label_spt_orig
								else
									case
										when core_or_division = 200
										then label_spt_orig
										else (select distinct w2a.label_spt_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 200)
									end
						end as label_spt,
						
						case
							when core_and_division = false
								then label_spc_orig
								else
									case
										when core_or_division = 200
										then label_spc_orig
										else (select distinct w2a.label_spc_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 200)
									end
						end as label_spc,
						
						case
							when core_and_division = false
								then label_adt_orig
								else
									case
										when core_or_division = 100
										then label_adt_orig
										else (select distinct w2a.label_adt_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 100)
									end
						end as label_adt,
						
						case
							when core_and_division = false
								then label_adc_orig
								else
									case
										when core_or_division = 100
										then label_adc_orig
										else (select distinct w2a.label_adc_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 100)
									end
						end as label_adc						
				from
						w2
				)
		,w4 as	(
				select distinct categorization_setup, id_spt, id_adt, id_spc, id_adc, label_spt, label_adt, label_spc, label_adc from w3 order by categorization_setup
				)
		,w5 as	(
				select
						w4.*,
						w_etl_ad.etl_id as etl_id_ad,
						w_etl_sp.etl_id as etl_id_sp,
						w_etl_adc.etl_id as etl_id_adc,
						w_etl_spc.etl_id as etl_id_spc
				from
							w4
				left join	w_etl_ad	on w4.id_adt = w_etl_ad.area_domain
				left join	w_etl_sp	on w4.id_spt= w_etl_sp.sub_population
				left join 	w_etl_adc	on w4.id_adc = w_etl_adc.area_domain_category
				left join	w_etl_spc	on w4.id_spc = w_etl_spc.sub_population_category
				)
		,w6 as	(
				select
						case when label_spt is null then 'null'::varchar else label_spt end as variables_sp,
						case when label_adt is null then 'null'::varchar else label_adt end as variables_ad,
						case when label_spc is null then 'null'::varchar else label_spc end as variables_spc,
						case when label_adc is null then 'null'::varchar else label_adc end as variables_adc,					
						case when etl_id_sp is null then 0 else etl_id_sp end as etl_id_sp,
						case when etl_id_spc is null then 0 else etl_id_spc end as etl_id_spc,
						case when etl_id_ad is null then 0 else etl_id_ad end as etl_id_ad,
						case when etl_id_adc is null then 0 else etl_id_adc end as etl_id_adc
				from
						w5
				)
		select
				json_build_object
				(
				'panels_and_reference_year_sets', _res_panels_and_reference_year_sets,
				'country', _res_country,
				'target_variable', _etl_id,
				'variables',json_agg(json_build_object(
					'sub_population',  w6.variables_sp,
					'sub_population_category', w6.variables_spc,
					'area_domain',  w6.variables_ad,
					'area_domain_category', w6.variables_adc,
					'sub_population_etl_id', w6.etl_id_sp,
					'sub_population_category_etl_id', w6.etl_id_spc,
					'area_domain_etl_id', w6.etl_id_ad,
					'area_domain_category_etl_id', w6.etl_id_adc))
				)
		from w6
		into _res;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_variables(integer[], integer) is
'Function returns variables for given input arguments.';

grant execute on function target_data.fn_etl_get_variables(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_array_compare" schema="target_data" src="functions/etl/fn_etl_array_compare.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_array_compare
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_array_compare(anyarray, anyarray) CASCADE;

create or replace function target_data.fn_etl_array_compare(anyarray,anyarray) 
returns boolean as $$
declare
		_res	boolean;
begin
	_res := (	
			select
				case
			  		when array_dims($1) <> array_dims($2) then 'f'
			 		when array_length($1,1) <> array_length($2,1) then 'f'
			  	else
				    not exists	(
						        select 1
						        from unnest($1) a 
						        full join unnest($2) b on (a=b) 
						        where a is null or b is null
				   				)
		  		end
		  	);
	
	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_array_compare(anyarray, anyarray) is
'Function returns true (the number of elements and the elements are identical) or false (the number of elements and the elements are not identical).';

grant execute on function target_data.fn_etl_array_compare(anyarray, anyarray) to public;
-- </function>



-- <function name="fn_etl_check_sub_population" schema="target_data" src="functions/etl/fn_etl_check_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_sub_population(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_check_sub_population
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns table
(
	id						integer,
	export_connection		integer,
	sub_population			integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_sp				integer
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
	
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.spc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.spc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			return query
			with w as	(
						select
								0 as id,
								0 as export_connection,
								array[0] as sub_population,
								''::varchar as label,
								''::text as description,
								''::varchar as label_en,
								''::text as description_en,
								0 as etl_id,
								0 as id_t_etl_sp
						)
			select w.id, w.export_connection, w.sub_population, w.label, w.description, w.label_en, w.description_en, w.etl_id, w.id_t_etl_sp
			from w
			where w.id is distinct from 0;		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.spc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.spc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select
							_export_connection as export_connection,
							w2.id_type as sub_population,
							w2.label,
							w2.description,
							w2.label_en,
							w2.description_en
					from
							w2
					)
			,w4 as	(
					select
							w3.export_connection,
							w3.sub_population,
							w3.label,
							w3.description,
							w3.label_en,
							w3.description_en,
							t.etl_id,
							t.id as id_t_etl_sp
					from
							w3 left join target_data.t_etl_sub_population as t
					on
							w3.export_connection = t.export_connection
					and	
							target_data.fn_etl_array_compare(w3.sub_population,t.sub_population)
					order
							by w3.sub_population
					)
			select
					(row_number() over ())::integer as id_order,
					w4.export_connection,
					w4.sub_population,
					w4.label,
					w4.description,
					w4.label_en,
					w4.description_en,
					w4.etl_id,
					w4.id_t_etl_sp
			from
					w4;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_sub_population(integer[], integer) is
'Function returns records for ETL c_sub_population table.';

grant execute on function target_data.fn_etl_check_sub_population(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_save_sub_population" schema="target_data" src="functions/etl/fn_etl_save_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_save_sub_population(integer, integer[], integer) CASCADE;

create or replace function target_data.fn_etl_save_sub_population
(
	_export_connection		integer,
	_sub_population			integer[],
	_etl_id					integer
)
returns integer
as
$$
declare
		_id integer;
begin
	if _export_connection is null
	then
		raise exception 'Error 01: fn_etl_save_sub_population: Input argument _export_connection must not by NULL!';
	end if; 

	if _sub_population is null
	then
		raise exception 'Error 02: fn_etl_save_sub_population: Input argument _sub_population must not by NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 03: fn_etl_save_sub_population: Input argument _etl_id must not by NULL!';
	end if;

	insert into target_data.t_etl_sub_population(export_connection, sub_population, etl_id)
	select _export_connection, _sub_population, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_save_sub_population(integer, integer[], integer) is
'Function inserts a record into table t_etl_sub_population based on given parameters.';

grant execute on function target_data.fn_etl_save_sub_population(integer, integer[], integer) to public;
-- </function



-- <function name="fn_etl_check_sub_population_category" schema="target_data" src="functions/etl/fn_etl_check_sub_population_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_sub_population_category(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_check_sub_population_category
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns table
(
	id						integer,
	export_connection		integer,
	etl_id_sub_population	integer,
	label_sub_population	varchar,
	label_en_sub_population	varchar,
	sub_population_category	integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_spc			integer,
	id_t_etl_sp				integer
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population_category: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population_category: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------

		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.spc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type,
						(target_data.fn_get_category4classification_rule_id('spc',t.spc2classification_rule)).id_category
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))							
													)
				and
						t.spc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type, w.id_category from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			raise exception 'Error 04: fn_etl_check_sub_population_category: If exists sub_population than must exists any sub_population_category!'; 		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.spc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label as label_type,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_en_type,
							(target_data.fn_get_category4classification_rule_id('spc',t.spc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.spc2classification_rule is not null
					)
			,w2 as	(
					select distinct _export_connection as export_connection, w1.id_type, w1.label_type, w1.label_en_type, w1.id_category, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select w2.*, tesp.etl_id as etl_sub_population, tesp.id as id_t_etl_sp
					from w2 inner join target_data.t_etl_sub_population as tesp
					on w2.export_connection = tesp.export_connection
					and target_data.fn_etl_array_compare(w2.id_type,tesp.sub_population)
					)
			,w4 as	(
					select w3.*, a.etl_id, a.id from w3
					left join
								(
								select * from target_data.t_etl_sub_population_category
								where etl_sub_population in (select distinct w3.id_t_etl_sp from w3)
								) as a
					on target_data.fn_etl_array_compare(w3.id_category,a.sub_population_category)
					order
							by w3.id_category
					)	
			select
					(row_number() over ())::integer as id_order,
					w4.export_connection,
					w4.etl_sub_population,
					w4.label_type as label_sub_population,
					w4.label_en_type as label_en_sub_population,
					w4.id_category as sub_population_category,
					w4.label,
					w4.description,
					w4.label_en,
					w4.description_en,
					w4.etl_id,
					w4.id as id_t_etl_spc,
					w4.id_t_etl_sp
			from
					w4;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_sub_population_category(integer[], integer) is
'Function returns records for ETL c_sub_population_category table.';

grant execute on function target_data.fn_etl_check_sub_population_category(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_save_sub_population_category" schema="target_data" src="functions/etl/fn_etl_save_sub_population_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_save_sub_population_category(integer[], integer, integer) CASCADE;

create or replace function target_data.fn_etl_save_sub_population_category
(
	_sub_population_category	integer[],
	_etl_id						integer,
	_etl_sub_population			integer
)
returns integer
as
$$
declare
		_id integer;
begin
	if _sub_population_category is null
	then
		raise exception 'Error 01: fn_etl_save_sub_population_category: Input argument _sub_population_category must not by NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_sub_population_category: Input argument _etl_id must not by NULL!';
	end if;

	if _etl_sub_population is null
	then
		raise exception 'Error 03: fn_etl_save_sub_population_category: Input argument _etl_sub_population must not by NULL!';
	end if;

	insert into target_data.t_etl_sub_population_category(sub_population_category, etl_id, etl_sub_population)
	select _sub_population_category, _etl_id, _etl_sub_population
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_save_sub_population_category(integer[], integer, integer) is
'Function inserts a record into table t_etl_sub_population_category based on given parameters.';

grant execute on function target_data.fn_etl_save_sub_population_category(integer[], integer, integer) to public;
-- </function>



-- <function name="fn_etl_check_area_domain" schema="target_data" src="functions/etl/fn_etl_check_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_area_domain(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_check_area_domain
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns table
(
	id						integer,
	export_connection		integer,
	area_domain				integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_ad				integer
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/					

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
			
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.adc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.adc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			return query
			with w as	(
						select
								0 as id,
								0 as export_connection,
								array[0] as area_domain,
								''::varchar as label,
								''::text as description,
								''::varchar as label_en,
								''::text as description_en,
								0 as etl_id,
								0 as id_t_etl_ad
						)
			select w.id, w.export_connection, w.area_domain, w.label, w.description, w.label_en, w.description_en, w.etl_id, w.id_t_etl_ad
			from w
			where w.id is distinct from 0;		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.adc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.adc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select
							_export_connection as export_connection,
							w2.id_type as area_domain,
							w2.label,
							w2.description,
							w2.label_en,
							w2.description_en
					from
							w2
					)
			,w4 as	(
					select
							w3.export_connection,
							w3.area_domain,
							w3.label,
							w3.description,
							w3.label_en,
							w3.description_en,
							t.etl_id,
							t.id as id_t_etl_ad
					from
							w3 left join target_data.t_etl_area_domain as t
					on
							w3.export_connection = t.export_connection
					and	
							target_data.fn_etl_array_compare(w3.area_domain,t.area_domain)
					order
							by w3.area_domain
					)
			select
					(row_number() over ())::integer as id_order,
					w4.export_connection,
					w4.area_domain,
					w4.label,
					w4.description,
					w4.label_en,
					w4.description_en,
					w4.etl_id,
					w4.id_t_etl_ad
			from
					w4;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_area_domain(integer[], integer) is
'Function returns records for ETL c_area_domain table.';

grant execute on function target_data.fn_etl_check_area_domain(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_save_area_domain" schema="target_data" src="functions/etl/fn_etl_save_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_save_area_domain(integer, integer[], integer) CASCADE;

create or replace function target_data.fn_etl_save_area_domain
(
	_export_connection		integer,
	_area_domain			integer[],
	_etl_id					integer
)
returns integer
as
$$
declare
		_id	integer;
begin
	if _export_connection is null
	then
		raise exception 'Error 01: fn_etl_save_area_domain: Input argument _export_connection must not by NULL!';
	end if; 

	if _area_domain is null
	then
		raise exception 'Error 02: fn_etl_save_area_domain: Input argument _area_domain must not by NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 03: fn_etl_save_area_domain: Input argument _etl_id must not by NULL!';
	end if;

	insert into target_data.t_etl_area_domain(export_connection, area_domain, etl_id)
	select _export_connection, _area_domain, _etl_id
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_save_area_domain(integer, integer[], integer) is
'Function inserts a record into table t_etl_area_domain based on given parameters.';

grant execute on function target_data.fn_etl_save_area_domain(integer, integer[], integer) to public;
-- </function>



-- <function name="fn_etl_check_area_domain_category" schema="target_data" src="functions/etl/fn_etl_check_area_domain_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_area_domain_category(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_check_area_domain_category
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns table
(
	id						integer,
	export_connection		integer,
	etl_id_area_domain		integer,
	label_area_domain		varchar,
	label_en_area_domain	varchar,
	area_domain_category	integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_adc			integer,
	id_t_etl_ad				integer
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_category: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain_category: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable 
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/					

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------

		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.adc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type,
						(target_data.fn_get_category4classification_rule_id('adc',t.adc2classification_rule)).id_category
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.adc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type, w.id_category from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			raise exception 'Error 03: fn_etl_check_area_domain_category: If exists area_domain than must exists any area_domain_category!'; 		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.adc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label as label_type,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_en_type,
							(target_data.fn_get_category4classification_rule_id('adc',t.adc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.adc2classification_rule is not null
					)
			,w2 as	(
					select distinct _export_connection as export_connection, w1.id_type, w1.label_type, w1.label_en_type, w1.id_category, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select w2.*, tead.etl_id as etl_area_domain, tead.id as id_t_etl_ad
					from w2 inner join target_data.t_etl_area_domain as tead
					on w2.export_connection = tead.export_connection
					and target_data.fn_etl_array_compare(w2.id_type,tead.area_domain)
					)
			,w4 as	(
					select w3.*, a.etl_id, a.id from w3
					left join
								(
								select * from target_data.t_etl_area_domain_category
								where etl_area_domain in (select distinct w3.id_t_etl_ad from w3)
								) as a
					on target_data.fn_etl_array_compare(w3.id_category,a.area_domain_category)
					order
							by w3.id_category
					)	
			select
					(row_number() over ())::integer as id_order,
					w4.export_connection,
					w4.etl_area_domain,
					w4.label_type as label_area_domain,
					w4.label_en_type as label_en_area_domain,
					w4.id_category as area_domain_category,
					w4.label,
					w4.description,
					w4.label_en,
					w4.description_en,
					w4.etl_id,
					w4.id as id_t_etl_adc,
					w4.id_t_etl_ad
			from
					w4;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_area_domain_category(integer[], integer) is
'Function returns records for ETL c_area_domain_category table.';

grant execute on function target_data.fn_etl_check_area_domain_category(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_save_area_domain_category" schema="target_data" src="functions/etl/fn_etl_save_area_domain_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_save_area_domain_category(integer[], integer, integer) CASCADE;

create or replace function target_data.fn_etl_save_area_domain_category
(
	_area_domain_category		integer[],
	_etl_id						integer,
	_etl_area_domain			integer
)
returns integer
as
$$
declare
		_id integer;
begin
	if _area_domain_category is null
	then
		raise exception 'Error 01: fn_etl_save_area_domain_category: Input argument _area_domain_category must not by NULL!';
	end if;

	if _etl_id is null
	then
		raise exception 'Error 02: fn_etl_save_area_domain_category: Input argument _etl_id must not by NULL!';
	end if;

	if _etl_area_domain is null
	then
		raise exception 'Error 03: fn_etl_save_area_domain_category: Input argument _etl_area_domain must not by NULL!';
	end if;

	insert into target_data.t_etl_area_domain_category(area_domain_category, etl_id, etl_area_domain)
	select _area_domain_category, _etl_id, _etl_area_domain
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_save_area_domain_category(integer[], integer, integer) is
'Function inserts a record into table t_etl_area_domain_category based on given parameters.';

grant execute on function target_data.fn_etl_save_area_domain_category(integer[], integer, integer) to public;
-- </function>



-- <function name="fn_etl_get_etl_id4target_variable" schema="target_data" src="functions/etl/fn_etl_get_etl_id4target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_etl_id4target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_etl_id4target_variable(integer) CASCADE;

create or replace function target_data.fn_etl_get_etl_id4target_variable
(
	_id_t_etl_target_variable	integer
)
returns integer
as
$$
declare
		_etl_id		integer;
begin
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_etl_id4target_variable: Input argument _id_t_etl_target_variable must not by NULL!';
		end if; 
		
		select etl_id from target_data.t_etl_target_variable
		where id = _id_t_etl_target_variable
		into _etl_id;
	
		return _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_etl_id4target_variable(integer) is
'Function returns record ETL_ID from table t_etl_target_variable based on given parameters.';

grant execute on function target_data.fn_etl_get_etl_id4target_variable(integer) to public;
-- </function>



-- <function name="fn_etl_export_variable" schema="target_data" src="functions/etl/fn_etl_export_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_variable(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_variable
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_categorization_setups		integer[];
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_variable: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_variable: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 04: fn_etl_export_variable: Internal argument _categorization_setups must not by NULL!';
		end if;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);

		with
		w1 as	(
				select
						id,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest(_categorization_setups))
				)
		,w2 as	(
				select
						w1.*,
						cltv.ldsity_object_type as core_or_division
				from
						w1
						inner join target_data.cm_ldsity2target_variable as cltv on w1.ldsity2target_variable = cltv.id
				)
		,w3 as	(				
				select
						w2.*,
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w2a.id_spt_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w2a.id_spc_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w2a.id_adt_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w2a.id_adc_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adc
				from
						w2
				)
		,w4 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w3 order by categorization_setup
				)
		,w5 as	(
				select * from w4 where id_spt is null and id_spc is null and id_adt is null and id_adc is null -- choosing of row without distinction
				)
		,w6 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from (select * from w4 where categorization_setup is distinct from (select categorization_setup from w5)) as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(t.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(t.id_adt,tead.area_domain)
				
				)
		,w7 as	(
				select
						w6.*,
						tespc.etl_id as etl_id_spc,
						teadc.etl_id as etl_id_adc
				from w6
				
				left join	target_data.t_etl_sub_population_category as tespc
				on  w6.id_sub_population = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w6.id_spc,tespc.sub_population_category)
				
				left join	target_data.t_etl_area_domain_category as teadc
				on  w6.id_area_domain = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w6.id_adc,teadc.area_domain_category)
				)
		,w8 as	(
				select categorization_setup, _etl_id as etl_id_tv, 0 as etl_id_spc, 0 as etl_id_adc from w5 -- row without distinction 
				union all
				select
						categorization_setup,
						_etl_id as etl_id_tv,
						case when etl_id_spc is null then 0 else etl_id_spc end as etl_id_spc,
						case when etl_id_adc is null then 0 else etl_id_adc end as etl_id_adc
				from
						w7 order by categorization_setup
				)
		select
			json_agg(json_build_object(
				'target_variable', w8.etl_id_tv,
				'sub_population_category', w8.etl_id_spc, 
				'area_domain_category', w8.etl_id_adc))
		from
				w8 into _res;
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_variable(integer[], integer) is
'Function returns records for ETL t_variable table.';

grant execute on function target_data.fn_etl_export_variable(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_export_variable_hierarchy" schema="target_data" src="functions/etl/fn_etl_export_variable_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_variable_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_variable_hierarchy(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_variable_hierarchy
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_categorization_setups		integer[];
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_variable_hierarchy: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_variable_hierarchy: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_etl_export_variable_hierarchy: Internal argument _categorization_setups must not by NULL!';
		end if;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);

		-------------------------------------------------------------
		refresh materialized view target_data.v_variable_hierarchy;
		-------------------------------------------------------------
		
		with
		w1 as	(
				select
						id,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in	(select unnest(_categorization_setups))
				)
		,w2 as	(
				select
						w1.*,
						cltv.ldsity_object_type as core_or_division
				from
						w1
						inner join target_data.cm_ldsity2target_variable as cltv on w1.ldsity2target_variable = cltv.id
				)
		,w3 as	(				
				select
						w2.*,
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w2a.id_spt_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w2a.id_spc_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w2a.id_adt_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w2a.id_adc_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adc
				from
						w2
				)
		,w4 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w3 order by categorization_setup
				)
		,w5 as	(	
				select
						node as variable_superior,
						unnest(edges) as variable
				from
						target_data.v_variable_hierarchy
				where
						node in (select unnest(_categorization_setups))
				)	
		,w6 as	(
				select
						_etl_id as etl_id_target_variable,
						w5.variable,
						w5.variable_superior,
						w4b.id_spt,
						w4a.id_spt as id_spt_sup,
						w4b.id_spc,
						w4a.id_spc as id_spc_sup,
						w4b.id_adt,
						w4a.id_adt as id_adt_sup,
						w4b.id_adc,
						w4a.id_adc as id_adc_sup
				from		w5
				left join	w4 as w4a	on w5.variable_superior = w4a.categorization_setup
				left join	w4 as w4b	on w5.variable = w4b.categorization_setup
				order by	w5.variable_superior, w5.variable
				)
		,w7 as	(
				select
						w6.*,
						tesp.id 	as id_sub_population_podrizene,
						tesp_sup.id as id_sub_population_nadrizene,
						tead.id 	as id_area_domain_podrizene,
						tead_sup.id	as id_area_domain_nadrizene
				from
						w6
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(w6.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp_sup
				on target_data.fn_etl_array_compare(w6.id_spt_sup,tesp_sup.sub_population)		
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(w6.id_adt,tead.area_domain)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead_sup
				on target_data.fn_etl_array_compare(w6.id_adt_sup,tead_sup.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						tespc.etl_id		as etl_id_spc,
						tespc_sup.etl_id	as etl_id_spc_sup,
						teadc.etl_id		as etl_id_adc,
						teadc_sup.etl_id	as etl_id_adc_sup
				from
						w7
				
				left join	target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population_podrizene = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc,tespc.sub_population_category)		
		
				left join	target_data.t_etl_sub_population_category as tespc_sup
				on  w7.id_sub_population_nadrizene = tespc_sup.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc_sup,tespc_sup.sub_population_category)			
			
				left join	target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain_podrizene = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc,teadc.area_domain_category)
		
				left join	target_data.t_etl_area_domain_category as teadc_sup
				on  w7.id_area_domain_nadrizene = teadc_sup.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc_sup,teadc_sup.area_domain_category)
				)
		,w9 as	(
				select
						etl_id_target_variable												as etl_id_tv,
						case when etl_id_spc is null then 0 else etl_id_spc end				as etl_id_spc,
						case when etl_id_spc_sup is null then 0 else etl_id_spc_sup end 	as etl_id_spc_sup,
						case when etl_id_adc is null then 0 else etl_id_adc end				as etl_id_adc,
						case when etl_id_adc_sup is null then 0 else etl_id_adc_sup end		as etl_id_adc_sup
				from
						w8
				)
		select
			json_agg(json_build_object(
				'target_variable', w9.etl_id_tv,
				'sub_population_category', w9.etl_id_spc,
				'sub_population_category_superior', w9.etl_id_spc_sup,
				'area_domain_category', w9.etl_id_adc,
				'area_domain_category_superior', w9.etl_id_adc_sup))
		from
				w9 into _res;
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_variable_hierarchy(integer[], integer) is
'Function returns records for ETL t_variable_hierarchy table.';

grant execute on function target_data.fn_etl_export_variable_hierarchy(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_export_ldsity_values" schema="target_data" src="functions/etl/fn_etl_export_ldsity_values.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_ldsity_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_ldsity_values(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_ldsity_values
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_available_datasets			integer[];
		_categorization_setups		integer[];
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_ldsity_values: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_ldsity_values: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;					
	
		with
		w1 as	(-- all categorization setups for input target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups reduced by combinations of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(-- list of IDs of available datasets from t_ldsity_values table
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		select array_agg(w3.available_datasets order by w3.available_datasets) from w3
		into _available_datasets;
		/*
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
		*/
		
		with
		w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select unnest(_available_datasets))
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_etl_export_ldsity_values: Internal argument _categorization_setups must not by NULL!';
		end if;

		/*
		with
		w1 as	(
				select
						id as id_available_datasets,
						panel,
						reference_year_set,
						categorization_setup
				from
					target_data.t_available_datasets
				where
					categorization_setup in (select unnest(_categorization_setups))
				and
					panel in (select unnest(_panels))
				and
					reference_year_set in (select unnest(_reference_year_sets))
				)
		,w2 as	(
				select t1.*, w1.*
				from target_data.t_ldsity_values as t1
				inner join w1 on t1.available_datasets = w1.id_available_datasets
				where t1.is_latest = true
				)
		*/

		with
		w1 as	(
				select
						tad.id as id_available_datasets,
						tad.panel,
						tad.reference_year_set,
						tad.categorization_setup
				from
						target_data.t_available_datasets as tad
				where
						tad.id in (select unnest(_available_datasets))
				)
		,w2 as	(
				select t1.*, w1.*
				from target_data.t_ldsity_values as t1
				inner join w1 on t1.available_datasets = w1.id_available_datasets
				where t1.is_latest = true
				)
		,w3 as	(
				select
						id as id_cm_ldsity2target2categorization_setup,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig				
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest(_categorization_setups))
				)
		,w4 as	(
				select
						w3.*,
						cltv.ldsity_object_type as core_or_division
				from
						w3
						inner join target_data.cm_ldsity2target_variable as cltv on w3.ldsity2target_variable = cltv.id
				)
		,w5 as	(				
				select
						w4.*,
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w4a.id_spt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w4a.id_spc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w4a.id_adt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w4a.id_adc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adc
				from
						w4
				)
		,w6 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w5
				)
		,w7 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from w6 as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(t.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(t.id_adt,tead.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						case when tespc.etl_id is null then 0 else tespc.etl_id end as etl_id_spc,
						case when teadc.etl_id is null then 0 else teadc.etl_id end as etl_id_adc
				from w7
				
				left join target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc,tespc.sub_population_category)
				
				left join target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc,teadc.area_domain_category)
				)
		,w9 as	(
				select
						a.*,
						b.strata_set as id_strata_set,
						c.country as id_country,
						d.label as country_label
				from 
					(
					select
							id as id_panel,
							stratum as id_stratum
					from
							sdesign.t_panel
					where
							id in	(
									select panel from sdesign.cm_refyearset2panel_mapping
									where id in (select unnest(_refyearset2panel_mapping))
									)
					) as	a
				inner join	sdesign.t_stratum		as b on a.id_stratum = b.id
				inner join	sdesign.t_strata_set	as c on b.strata_set = c.id
				inner join	sdesign.c_country		as d on c.country = d.id
				)
		,w10 as	(
				select
						_etl_id as etl_id_tv,
						w2.plot,
						w2.value,
						tp.panel,
						trys.reference_year_set,
						w9.country_label,
						w8.etl_id_spc,
						w8.etl_id_adc
				from
						w2
				inner join w8 on w2.categorization_setup = w8.categorization_setup
				inner join sdesign.t_panel as tp on w2.panel = tp.id
				inner join sdesign.t_reference_year_set as trys on w2.reference_year_set = trys.id
				inner join w9 on w2.panel = w9.id_panel
				)
		,w11 as	(
				select
						w10.etl_id_tv,
						w10.plot,
						w10.value,
						w10.panel,
						w10.reference_year_set,
						w10.country_label,
						w10.etl_id_spc,
						w10.etl_id_adc
				from
						w10
				order
				by		w10.country_label,
						w10.reference_year_set,
						w10.panel,
						w10.etl_id_tv,
						w10.plot,
						w10.etl_id_spc,
						w10.etl_id_adc 
				)
		select
			json_agg(json_build_object(
				'target_variable',   		w11.etl_id_tv,
				'plot',						w11.plot,
				'value',					w11.value,
				'panel',					w11.panel,
				'reference_year_set',		w11.reference_year_set,
				'country',					w11.country_label,
				'sub_population_category',	w11.etl_id_spc,
				'area_domain_category',		w11.etl_id_adc))
		from
				w11 into _res;
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_ldsity_values(integer[], integer) is
'Function returns records for ETL t_target_data and t_available_datasets table.';

grant execute on function target_data.fn_etl_export_ldsity_values(integer[], integer) to public;
-- </function>
