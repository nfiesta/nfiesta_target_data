--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

----------------------------------------------
-- Functions
-----------------------------------------------

-- <function name="fn_get_ldsity_object" schema="target_data" src="functions/fn_get_ldsity_object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200)
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, t1.table_name
		FROM target_data.c_ldsity_object AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		SELECT t1.id, t3.id AS ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, t1.table_name
		FROM target_data.c_ldsity_object AS t1
		INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
		ON t1.id = t2.ldsity_object
		INNER JOIN target_data.c_ldsity_object_group AS t3
		ON t2.ldsity_object_group = t3.id
		WHERE t3.id = _ldsity_object_group;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object(integer) IS
'Function returns records from c_ldsity_object table, optionally for given topic.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object(integer) TO public;
-- </function>

-- <function name="fn_save_ldsity_object_group" schema="target_data" src="functions/fn_save_ldsity_object_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_ldsity_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_ldsity_object_group(character varying, text, character varying, text, integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_ldsity_object_group(_label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text, _ldsity_objects integer[] DEFAULT NULL::integer[], _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
DECLARE
	_ldsity_object_group integer;
	_ldsity_object_group_existing integer;
BEGIN
	IF _id IS NULL
	THEN 
		IF _label IS NOT NULL AND _description IS NOT NULL AND _ldsity_objects IS NOT NULL
		THEN
			
			WITH w_param AS (
				SELECT array_agg(t.ldsity_object ORDER BY t.ldsity_object) AS ldsity_objects_param
				FROM 
					(SELECT t1.ldsity_object
					FROM unnest(_ldsity_objects) AS t1(ldsity_object)
					) AS t
				),
			w_existing AS (
				SELECT ldsity_object_group, array_agg(ldsity_object ORDER BY ldsity_object) AS ldsity_objects
				FROM
					target_data.cm_ld_object2ld_object_group
				GROUP BY
					ldsity_object_group
			)
			SELECT ldsity_object_group 
			FROM w_existing AS t1 
			INNER JOIN w_param AS t2
			ON t1.ldsity_objects = t2.ldsity_objects_param
			INTO _ldsity_object_group_existing;
		
			IF _ldsity_object_group_existing IS NOT NULL
			THEN RAISE EXCEPTION 'Group % contains the same combination of objects. Duplicate insert is not allowed.', _ldsity_object_group_existing;
			END IF;		
		
			INSERT INTO target_data.c_ldsity_object_group(label, description, label_en, description_en)
			SELECT _label, _description, _label_en, _description_en
			RETURNING id
			INTO _ldsity_object_group;

			INSERT INTO target_data.cm_ld_object2ld_object_group(ldsity_object_group, ldsity_object)
			SELECT _ldsity_object_group, unnest(_ldsity_objects);
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%) and ldsity objects array (%) must be not null!', _label, _description, _ldsity_objects;
		END IF;
	ELSE
		IF _ldsity_objects IS NULL
		THEN
			UPDATE target_data.c_ldsity_object_group
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_ldsity_object_group.id = _id;

			_ldsity_object_group := _id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing group and create new one with given ldsity objects (%).', _ldsity_objects;
		END IF;
	END IF;

	RETURN _ldsity_object_group;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_ldsity_object_group(character varying, text, character varying, text, integer[], integer) IS
'Function provides insert into/update in c_ldsity_object_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_ldsity_object_group(character varying, text, character varying, text, integer[], integer) TO public;

-- </function>

