--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

----------------------------------------------
-- Functions
-----------------------------------------------

DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object(integer) CASCADE;
-- <function name="fn_get_ldsity_object" schema="target_data" src="functions/fn_get_ldsity_object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_object(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
table_name		character varying(200),
areal_or_population	integer
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, NULL::int AS ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, t1.table_name, t1.areal_or_population
		FROM target_data.c_ldsity_object AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		SELECT t1.id, t3.id AS ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, t1.table_name, t1.areal_or_population
		FROM target_data.c_ldsity_object AS t1
		INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
		ON t1.id = t2.ldsity_object
		INNER JOIN target_data.c_ldsity_object_group AS t3
		ON t2.ldsity_object_group = t3.id
		WHERE t3.id = _ldsity_object_group;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object(integer) IS
'Function returns records from c_ldsity_object table, optionally for given topic.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object(integer) TO public;

-- </function>

-- <function name="fn_save_ldsity_object_group" schema="target_data" src="functions/fn_save_ldsity_object_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_ldsity_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_ldsity_object_group(character varying, text, character varying, text, integer[], integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_ldsity_object_group(_label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text, _ldsity_objects integer[] DEFAULT NULL::integer[], _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
DECLARE
	_ldsity_object_group integer;
	_ldsity_object_group_existing integer;
	_arepop_test integer;
BEGIN
	IF _id IS NULL
	THEN 
		IF _label IS NOT NULL AND _description IS NOT NULL AND _ldsity_objects IS NOT NULL
		THEN
			
			WITH w_param AS (
				SELECT array_agg(t.ldsity_object ORDER BY t.ldsity_object) AS ldsity_objects_param
				FROM 
					(SELECT t1.ldsity_object
					FROM unnest(_ldsity_objects) AS t1(ldsity_object)
					) AS t
				),
			w_existing AS (
				SELECT ldsity_object_group, array_agg(ldsity_object ORDER BY ldsity_object) AS ldsity_objects
				FROM
					target_data.cm_ld_object2ld_object_group
				GROUP BY
					ldsity_object_group
			)
			SELECT ldsity_object_group 
			FROM w_existing AS t1 
			INNER JOIN w_param AS t2
			ON t1.ldsity_objects = t2.ldsity_objects_param
			INTO _ldsity_object_group_existing;
		
			IF _ldsity_object_group_existing IS NOT NULL
			THEN RAISE EXCEPTION 'Group % contains the same combination of objects. Duplicate insert is not allowed.', _ldsity_object_group_existing;
			END IF;		
		
			WITH w_arepop AS (
				SELECT 	t1.id, t1.areal_or_population
				FROM	target_data.c_ldsity_object AS t1
				INNER JOIN
					unnest(_ldsity_objects) AS t2(ldsity_object)
				ON t1.id = t2.ldsity_object
			)
			SELECT count(*)
			FROM
				(SELECT areal_or_population, count(*) AS total
				FROM w_arepop
				GROUP BY areal_or_population) AS t1
			INTO _arepop_test;

			IF _arepop_test > 1
			THEN RAISE EXCEPTION 'Given array of ldsity objects containes mix of areal and population types.';
			END IF;

			INSERT INTO target_data.c_ldsity_object_group(label, description, label_en, description_en)
			SELECT _label, _description, _label_en, _description_en
			RETURNING id
			INTO _ldsity_object_group;

			INSERT INTO target_data.cm_ld_object2ld_object_group(ldsity_object_group, ldsity_object)
			SELECT _ldsity_object_group, unnest(_ldsity_objects);
		ELSE
			RAISE EXCEPTION 'If parameter id is NULL, then label (%), description (%) and ldsity objects array (%) must be not null!', _label, _description, _ldsity_objects;
		END IF;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _id;
		END IF;

		IF _ldsity_objects IS NULL
		THEN
			UPDATE target_data.c_ldsity_object_group
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_ldsity_object_group.id = _id;

			_ldsity_object_group := _id;
		ELSE
			RAISE EXCEPTION 'If parameter id is NOT NULL, only labels and descriptions can be edited. 
					Try to delete existing group and create new one with given ldsity objects (%).', _ldsity_objects;
		END IF;
	END IF;

	RETURN _ldsity_object_group;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_ldsity_object_group(character varying, text, character varying, text, integer[], integer) IS
'Function provides insert into/update in c_ldsity_object_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_ldsity_object_group(character varying, text, character varying, text, integer[], integer) TO public;

-- </function>

-- <function name="fn_try_delete_target_variable" schema="target_data" src="functions/fn_try_delete_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_delete_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_try_delete_target_variable(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_delete_target_variable(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _id)
	THEN RAISE EXCEPTION 'Given target variable does not exist in table c_target_variable (%)', _id;
	END IF;

	RETURN NOT EXISTS (
		SELECT t6.id
		FROM target_data.c_target_variable AS t1
		INNER JOIN target_data.cm_ldsity2target_variable AS t2
		ON t1.id = t2.ldsity
		WHERE t1.id = _id);
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_delete_target_variable(integer) IS
'Function provides test if it is possible to delete records from c_target_variable table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_delete_target_variable(integer) TO public;

-- </function>

-- <function name="fn_try_delete_ldsity_object_group" schema="target_data" src="functions/fn_try_delete_ldsity_object_group.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_try_delete_ldsity_object_group
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_try_delete_ldsity_object_group(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_try_delete_ldsity_object_group(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _id)
	THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _id;
	END IF;

	RETURN NOT EXISTS (
		SELECT t6.id
		FROM target_data.c_ldsity_object_group AS t1
		INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
		ON t1.id = t2.ldsity_object_group
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN target_data.c_ldsity AS t4
		ON t3.id = t4.ldsity_object
		INNER JOIN target_data.cm_ldsity2target_variable AS t5
		ON t4.id = t5.ldsity
		INNER JOIN target_data.c_target_variable AS t6
		ON t5.target_variable = t6.id
		WHERE t1.id = _id);

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_try_delete_ldsity_object_group(integer) IS
'Function provides test if it is possible to delete records from c_ldsity_object_group table.';

GRANT EXECUTE ON FUNCTION target_data.fn_try_delete_ldsity_object_group(integer) TO public;

-- </function>

-- <function name="fn_delete_target_variable" schema="target_data" src="functions/fn_delete_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_target_variable(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_target_variable(_id integer)
RETURNS void
AS
$$
BEGIN

	IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _id)
	THEN RAISE EXCEPTION 'Given target variable does not exist in table c_target_variable (%)', _id;
	END IF;

	DELETE FROM target_data.cm_ldsity2target_variable WHERE target_variable = _id;
	DELETE FROM target_data.c_target_variable WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_target_variable(integer) IS
'Function deletes records from c_target_variable and cm_ldsity2target_variable table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_target_variable(integer) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_save_categories(integer, integer, varchar, text, varchar, text) CASCADE;
-- <function name="fn_save_category" schema="target_data" src="functions/fn_save_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_category(integer, varchar, text, varchar, text, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_category(_areal_or_population integer, _label varchar(200), _description text, _label_en varchar(200), _description_en text, _parent integer DEFAULT NULL::integer, _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
BEGIN
	IF _label IS NULL OR _description IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent identifier (%) or label/descriptioin is null (%, %)!', _parent, _label, _description;
	END IF; 

	IF _id IS NULL THEN

		IF _parent IS NULL
		THEN
			RAISE EXCEPTION 'Parameter of parent idenfier (id from c_area_domain/c_sub_population) is null!';
		END IF;

		CASE 
		WHEN _areal_or_population = 1 THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _parent)
			THEN RAISE EXCEPTION 'Given area_domain does not exist in table c_area_domain (%).', _parent;
			END IF;

			INSERT INTO target_data.c_area_domain_category(area_domain, label, description, label_en, description_en)
			SELECT 
				_parent,
				_label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;

		WHEN _areal_or_population = 2 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _parent)
			THEN RAISE EXCEPTION 'Given sub_population does not exist in table c_sub_population (%).', _parent;
			END IF;

			INSERT INTO target_data.c_sub_population_category(sub_population, label, description, label_en, description_en)
			SELECT 
				_parent,
				_label, _description, _label_en, _description_en
			RETURNING id
			INTO _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	ELSE
		CASE WHEN _areal_or_population = 1 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain_category AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain category does not exist in table c_area_domain_category (%)', _id;
			END IF;

			UPDATE target_data.c_area_domain_category
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_area_domain_category.id = _id;

		WHEN _areal_or_population = 2 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain_category AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain category does not exist in table c_area_domain_category (%)', _id;
			END IF;

			UPDATE target_data.c_sub_population_category
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_sub_population_category.id = _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_category(integer, varchar(200), text, varchar(200), text, integer, integer) IS
'Functions inserts a record into table c_area_domain_category or c_sub_population_category based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_category(integer, varchar(200), text, varchar(200), text, integer, integer) TO public;

-- </function>

-- <function name="fn_save_unit_of_measure" schema="target_data" src="functions/fn_save_unit_of_measure.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_unit_of_measure
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_unit_of_measure(integer, character varying, text, character varying, text, integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_unit_of_measure(_id integer, _label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text)
RETURNS integer
AS
$$
BEGIN
	IF _id IS NOT NULL 
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_unit_of_measure AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given unit of measure does not exist in table c_unit_of_measure (%)', _id;
		END IF;

		IF _label IS NOT NULL OR _description IS NOT NULL
		THEN
			UPDATE target_data.c_unit_of_measure
			SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
			WHERE c_unit_of_measure.id = _id;
		ELSE
			RAISE EXCEPTION 'At least label or description must be not null when doing an update of c_unit_of_measure (%, %).', _label, _description;
		END IF;
	ELSE
		RAISE EXCEPTION 'Parameter _id must not be null (%)!', _id;
	END IF;

	RETURN _unit_of_measure;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_unit_of_measure(integer, character varying, text, character varying, text) IS
'Function provides update in c_unit_of_measure table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_unit_of_measure(integer, character varying, text, character varying, text) TO public;

-- </function>

-- <function name="fn_save_definition_variant" schema="target_data" src="functions/fn_save_definition_variant.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_definition_variant
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_definition_variant(integer, character varying, text, character varying, text, integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_definition_variant(_id integer, _label character varying, _description text, _label_en character varying DEFAULT NULL::varchar, _description_en text DEFAULT NULL::text)
RETURNS integer
AS
$$
BEGIN
	IF _id IS NOT NULL 
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_definition_variant AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given definition variant does not exist in table c_definition_variant (%)', _id;
		END IF;

		IF _label IS NOT NULL OR _description IS NOT NULL
		THEN
			UPDATE target_data.c_definition_variant
			SET 	label = _label, description = _description, 
			label_en = _label_en, description_en = _description_en
			WHERE c_definition_variant.id = _id;
		ELSE
			RAISE EXCEPTION 'At least label or description must be not null when doing an update of c_definition_variant (%, %).', _label, _description;
		END IF;
	ELSE
		RAISE EXCEPTION 'Parameter _id must not be null (%)!', _id;
	END IF;

	RETURN _definition_variant;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_definition_variant(integer, character varying, text, character varying, text) IS
'Function provides update in c_definition_variant table.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_definition_variant(integer, character varying, text, character varying, text) TO public;

-- </function>


