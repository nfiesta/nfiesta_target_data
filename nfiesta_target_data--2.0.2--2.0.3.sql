--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_get_classification_rule4classification_rule_id" schema="target_data" src="functions/fn_get_classification_rule4classification_rule_id.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4classification_rule_id
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) CASCADE;

create or replace function target_data.fn_get_classification_rule4classification_rule_id
(
	_classification_rule_type	character varying,
	_classification_rule_id		integer[],
	_ldsity_objects				integer[]
)	
returns text
as
$$
declare
		_table_name								text;
		_classification_rule_i					text;	
		_ldsity_object_i						integer;
		_pozice_i								integer;
		_column_name_i_array					text[];
		_classification_rule_i_pozice			text;
		_classification_rule_4case_pozice		text;
		_category								integer[];
begin
		if _classification_rule_type is null
		then
			raise exception 'Error 01: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_type must not by NULL !';
		end if;
	
		if not(_classification_rule_type = any(array['adc','spc']))
		then
			raise exception 'Error 02: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_type must be "adc" or "spc" !';
		end if;	
	
		if _classification_rule_id is null
		then
			raise exception 'Error 03: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_id must not by NULL !';
		end if;
	
		if _ldsity_objects is null
		then
			raise exception 'Error 04: fn_get_classification_rule4classification_rule_id: Input argument _ldsity_objects must not by NULL !';
		end if;	
	
		if _classification_rule_type = 'adc' then _table_name := 'cm_adc2classification_rule'; end if;
		if _classification_rule_type = 'spc' then _table_name := 'cm_spc2classification_rule'; end if;	
	
		for i in 1..array_length(_classification_rule_id,1)
		loop
				execute '
				select
						case when classification_rule is null then ''true'' else classification_rule end as classification_rule,
						ldsity_object
				from
						target_data.'||_table_name||'
				where
						id = $1'
				using	_classification_rule_id[i]
				into	_classification_rule_i,
						_ldsity_object_i;

				-----------------------------------------------------
				if 	_classification_rule_i in ('EXISTS','NOT EXISTS')
				then
					_classification_rule_i := 'TRUE';
				else
					_classification_rule_i := _classification_rule_i;
				end if;
				-----------------------------------------------------						

				_pozice_i := array_position(_ldsity_objects,_ldsity_object_i);
										
				select array_agg(column_name::text order by ordinal_position) from information_schema.columns
			 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_object_i)
			 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_object_i)
				into _column_name_i_array;
								
				for ii in 1..array_length(_column_name_i_array,1)
				loop					
					_classification_rule_i := regexp_replace(_classification_rule_i,concat('(\+|\*|\-|\/| |\A|\(|\)|\=)(',_column_name_i_array[ii],')(\+|\*|\-|\/| |\Z|\(|\)|\=)'),concat('\1\2','_',_pozice_i,'\3'),'g'); 
				end loop;
		
				if i = 1
				then
					_classification_rule_4case_pozice := _classification_rule_i;
				else
					_classification_rule_4case_pozice := concat(_classification_rule_4case_pozice,' and ',_classification_rule_i);
				end if;		
						
		end loop;
	
		_classification_rule_4case_pozice := concat('(',_classification_rule_4case_pozice,')');

		return _classification_rule_4case_pozice;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) is
'The function gets text of classification rules for their classification rule identifiers.';

grant execute on function target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_modified_ldsity_column_expression" schema="target_data" src="functions/fn_get_modified_ldsity_column_expression.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_modified_ldsity_column_expression(text, integer, integer) CASCADE;

create or replace function target_data.fn_get_modified_ldsity_column_expression
(
	_ldsity_column_expression		text,
	_position4ldsity				integer,
	_ldsity_ldsity_object			integer
)
returns text
as
$$
declare
		_column_name_i_array		text[];
begin
		if _ldsity_column_expression is null
		then
			raise exception 'Error 01: fn_get_modified_ldsity_column_expression: Input argument _ldsity_column_expression must not by null !';
		end if;

		if _position4ldsity is null
		then
			raise exception 'Error 02: fn_get_modified_ldsity_column_expression: Input argument _position4ldsity must not by null !';
		end if;

		if _ldsity_ldsity_object is null
		then
			raise exception 'Error 03: fn_get_modified_ldsity_column_expression: Input argument _ldsity_ldsity_object must not by null !';
		end if;

		select array_agg(column_name order by ordinal_position) from information_schema.columns
	 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_ldsity_object)
	 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_ldsity_object)
		into _column_name_i_array;

		for i in 1..array_length(_column_name_i_array,1)
		loop
			--_ldsity_column_expression := replace(_ldsity_column_expression,_column_name_i_array[i],concat(_column_name_i_array[i],'_',_position4ldsity));
			--_ldsity_column_expression := regexp_replace(_ldsity_column_expression,concat('(\+|\*|\-|\/| |^)(',_column_name_i_array[i],')(\+|\*|\-|\/| |$)'),'\1\2_2\3','g');
			--_ldsity_column_expression := regexp_replace(_ldsity_column_expression,concat('(\+|\*|\-|\/| |\A)(',_column_name_i_array[i],')(\+|\*|\-|\/| |\Z)'),'\1\2_2\3','g');
			_ldsity_column_expression := regexp_replace(_ldsity_column_expression,concat('(\+|\*|\-|\/| |\A|\(|\)|\=)(',_column_name_i_array[i],')(\+|\*|\-|\/| |\Z|\(|\)|\=)'),concat('\1\2','_',_position4ldsity,'\3'),'g');   
		end loop;
	
		return _ldsity_column_expression;	
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_modified_ldsity_column_expression(text, integer, integer) is
'The function modified input ldsity column exression for given ldsity object and their position in ldsity objects.';

grant execute on function target_data.fn_get_modified_ldsity_column_expression(text, integer, integer) to public;
-- </function>

