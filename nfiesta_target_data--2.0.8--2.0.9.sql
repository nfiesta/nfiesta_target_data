--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


-- <function name="fn_etl_save_log" schema="target_data" src="functions/etl/fn_etl_save_log.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_log
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_save_log(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_save_log
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns text
as
$$
declare
		_max_id_tel		integer;
		_time_now		timestamptz;
		_res			text;
begin
	if _refyearset2panel_mapping is null
	then
		raise exception 'Error 01: fn_etl_save_log: Input argument _refyearset2panel_mapping must not by NULL!';
	end if; 

	if _id_t_etl_target_variable is null
	then
		raise exception 'Error 02: fn_etl_save_log: Input argument _id_t_etl_target_variable must not by NULL!';
	end if;

	if	(
		select count(tetv.*) is distinct from 1
		from target_data.t_etl_target_variable as tetv where tetv.id = _id_t_etl_target_variable
		)
	then
		raise exception 'Error 03: fn_etl_save_log: For input argument _id_t_etl_target_variable = % not exist any record in table t_etl_target_variable!',_id_t_etl_target_variable;
	end if;

	for i in 1..array_length(_refyearset2panel_mapping,1)
	loop
		if	(
			select count(crpm.*) is distinct from 1
			from sdesign.cm_refyearset2panel_mapping as crpm where crpm.id = _refyearset2panel_mapping[i]
			)
		then
			raise exception 'Error 04: fn_etl_save_log: For input argument _refyearset2panel_mapping[i] = % not exist any record in table sdesign.cm_refyearset2panel_mapping!',_refyearset2panel_mapping[i];
		end if;
	end loop;

	_time_now := (now())::timestamptz;

	_max_id_tel := (select coalesce(max(id),0) from target_data.t_etl_log);

	with
	w1 as	(
			select
					_id_t_etl_target_variable as etl_target_variable,
					unnest(_refyearset2panel_mapping) as refyearset2panel_mapping,
					_time_now as time_now
			)
	insert into target_data.t_etl_log(etl_target_variable, refyearset2panel_mapping, etl_time)
	select
			w1.etl_target_variable,
			w1.refyearset2panel_mapping,
			w1.time_now
	from
			w1 order by w1.refyearset2panel_mapping;

	_res := concat('The ',(select count(*) from target_data.t_etl_log where id > _max_id_tel),' new records were inserted into t_etl_log table.');
	
	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_save_log(integer[], integer) is
'Function inserts a records into table t_etl_log based on given input parameters.';

grant execute on function target_data.fn_etl_save_log(integer[], integer) to public;
-- </function>



DROP FUNCTION IF EXISTS target_data.fn_etl_check_target_variable(integer, integer);



-- <function name="fn_etl_check_target_variable" schema="target_data" src="functions/etl/fn_etl_check_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_target_variable(integer, integer) CASCADE;

create or replace function target_data.fn_etl_check_target_variable
(
	_export_connection		integer,
	_target_variable		integer
)
returns table
(
	id_etl_target_variable		integer,
	check_target_variable		boolean,
	refyearset2panel_mapping	integer[]
)
as
$$
declare
		_id_etl_target_variable		integer;
		_refyearset2panel_mapping	integer[];
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_target_variable: Input argument _export_connection must not by NULL!';
		end if; 
	
		if _target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_target_variable: Input argument _target_variable must not by NULL!';
		end if;
	
		select id from target_data.t_etl_target_variable
		where export_connection = _export_connection
		and target_variable = _target_variable
		into _id_etl_target_variable;
	
		if _id_etl_target_variable is null
		then
			return query select
								_id_etl_target_variable as etl_target_variable,
								false as check_target_variable,
								null::integer[] as refyearset2panel_mapping;
		else
			with
			w1 as	(
					select * from target_data.t_etl_log
					where etl_target_variable = _id_etl_target_variable
					)
			,w2 as	(
					select w1.refyearset2panel_mapping, max(w1.etl_time) as max_etl_time from w1
					group by w1.refyearset2panel_mapping
					)
			,w3 as	(
					select w1.*, w2.max_etl_time from w1 inner join w2
					on w1.refyearset2panel_mapping = w2.refyearset2panel_mapping
					and w1.etl_time = w2.max_etl_time
					)
			,w4 as	(-- list of panels and reference year sets that was ELT, source is from t_etl_log
					select
							w3.id as id_etl_log,
							w3.etl_target_variable,
							w3.refyearset2panel_mapping as refyearset2panel_mapping_in_log,
							w3.etl_time,
							w3.max_etl_time,
							crpm.panel as panel_in_log,
							crpm.reference_year_set as reference_year_set_in_log
					from w3
					inner join sdesign.cm_refyearset2panel_mapping as crpm on w3.refyearset2panel_mapping = crpm.id
					)
			,w5 as	(-- list of panels and reference year sets that are in configuration for given target variable
					select tad.* from target_data.t_available_datasets as tad
					where categorization_setup in
						(
						select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
						where ldsity2target_variable in
							(
							select id from target_data.cm_ldsity2target_variable
							where target_variable = (
													select tetv.target_variable
													from target_data.t_etl_target_variable as tetv
													where tetv.id = _id_etl_target_variable
													)
							)
						)
					)
			,w6 as	(
					-- list of panels and reference year sets that was prepared in t_ldsity_values
					select
							tad.id as available_datasets,
							tad.panel,
							tad.reference_year_set,
							tad.categorization_setup
					from target_data.t_available_datasets as tad where tad.id in
					(select distinct tlv.available_datasets from target_data.t_ldsity_values as tlv
					where tlv.available_datasets in (select w5.id from w5) and tlv.is_latest = true)
					)
			,w7 as	(-- all current ldsity values from t_ldsity_values for all panels and reference year sets from with w6
					select
							tlv.*,
							w6.*
					from target_data.t_ldsity_values as tlv
					inner join w6 on tlv.available_datasets = w6.available_datasets
					where tlv.is_latest = true
					)
			,w8 as	(
					select w7.*, w4.* from w7 left join w4
					on w7.panel = w4.panel_in_log and w7.reference_year_set = w4.reference_year_set_in_log
					)
			,w9 as	(-- list of panels and reference year sets that were not ETL yet
					select crpm.id as refyearset2panel_mapping
					from sdesign.cm_refyearset2panel_mapping as crpm
					inner join
								(
								select distinct w8.panel, w8.reference_year_set from w8
								where w8.id_etl_log is null
								) as t
					on crpm.panel = t.panel and crpm.reference_year_set = t.reference_year_set
					)
			,w10 as	(-- list of panels and reference year sets that were ETL but exists newer local densities
					select distinct w8.refyearset2panel_mapping_in_log as refyearset2panel_mapping
					from w8 where w8.id_etl_log is not null and w8.creation_time > w8.max_etl_time
					order by w8.refyearset2panel_mapping_in_log
					)
			select
				array_agg(a.refyearset2panel_mapping order by a.refyearset2panel_mapping) as refyearset2panel_mapping
			from
				(
				select w9.refyearset2panel_mapping from w9 union all
				select w10.refyearset2panel_mapping from w10
				) as a
			into
				_refyearset2panel_mapping;

			if _refyearset2panel_mapping is null
			then
				return query select
									_id_etl_target_variable as etl_target_variable,
									true as check_target_variable,
									null::integer[] as refyearset2panel_mapping;
			else
				return query select
									_id_etl_target_variable as etl_target_variable,
									false as check_target_variable,
									_refyearset2panel_mapping as refyearset2panel_mapping;
			end if;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_target_variable(integer, integer) is
'Function checks that target variable and all theirs current datas was ETL (true) or not (false).';

grant execute on function target_data.fn_etl_check_target_variable(integer, integer) to public;
-- </function>



DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(character varying);



-- <function name="fn_etl_get_target_variable" schema="target_data" src="functions/etl/fn_etl_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(integer, character varying, boolean) CASCADE;

create or replace function target_data.fn_etl_get_target_variable
(
	_export_connection	integer,
	_national_language	character varying(2) default 'en'::character varying(2),
	_not_etl			boolean default false
)
returns table
(
	id							integer,
	label						varchar,
	id_etl_target_variable		integer,
	check_target_variable		boolean,
	refyearset2panel_mapping	integer[],
	metadata					json
)
as
$$
declare
		_cond			text;
		_set_column		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable: Input argument _export_connection must not be null!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_get_target_variable: Input argument _national_language must not be null!';
		end if;

		if _not_etl is null
		then
			raise exception 'Error 03: fn_etl_get_target_variable: Input argument _not_etl must not be null!';
		end if;

		if _not_etl = TRUE
		then
			_cond := 'w2.check_target_variable = FALSE';
		else
			_cond := 'TRUE';
		end if;

		if _national_language = 'en'
		then
			_set_column := 'w1.label_en';
		else
			_set_column := 'w1.label';
		end if;

		return query execute
		'
		with
		w1 as	(
				select 
						ctv.*,
						(target_data.fn_etl_check_target_variable($1,ctv.id)) as res
				from 
						target_data.c_target_variable as ctv order by ctv.id
				)
		,w2 as	(
				select
						w1.id,
						'|| _set_column ||' as label,
						(w1.res).id_etl_target_variable,
						(w1.res).check_target_variable,
						(w1.res).refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select
						w2.*,
						target_data.fn_etl_get_target_variable_metadata(w2.id,$2) as metadata
				from
						w2 where '|| _cond ||'
				)
		,w4 as	(
				select
						w3.id,
						w3.label,
						w3.id_etl_target_variable,
						w3.check_target_variable,
						w3.refyearset2panel_mapping,
						w3.metadata->$2 as metadata
				from
						w3
				)
				select
						w4.id,
						w4.label,
						w4.id_etl_target_variable,
						w4.check_target_variable,
						w4.refyearset2panel_mapping,
						json_build_object($2,w4.metadata) as metadata
				from
						w4 order by w4.id;
		'
		using _export_connection, _national_language;

end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_target_variable(integer, character varying, boolean) IS
'The function returs target variables and informations for ETL.';

grant execute on function target_data.fn_etl_get_target_variable(integer, character varying, boolean) to public;
-- </function>



 DROP FUNCTION IF EXISTS target_data.fn_etl_get_panel_refyearset_combinations(integer);



-- <function name="fn_etl_get_panel_refyearset_combinations" schema="target_data" src="functions/etl/fn_etl_get_panel_refyearset_combinations.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_panel_refyearset_combinations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_panel_refyearset_combinations(integer,integer[]) CASCADE;

create or replace function target_data.fn_etl_get_panel_refyearset_combinations
(
	_id_t_etl_target_variable	integer,
	_refyearset2panel_mapping	integer[]
)
returns table
(
	refyearset2panel		integer,
	panel					varchar,	
	reference_year_set		varchar
)
as
$$
declare
		_res_panels_and_reference_year_sets		json;
		_country_array							integer[];
		_res_country							varchar;
		_target_variable						integer;
		_etl_id									integer;
		_categorization_setups					integer[];
		_res									json;
begin		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_panel_refyearset_combinations: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;

		select
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_target_variable;

		if _target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_panel_refyearset_combinations: For input argument _id_t_etl_target_variable = % not exists any target_variable in table t_etl_target_variable!',_id_t_etl_target_variable;
		end if;

		if _refyearset2panel_mapping is null
		then
			return query
			with
			w1 as	(
					select tad.* from target_data.t_available_datasets as tad where tad.categorization_setup in
						(
						select distinct cmlcs.categorization_setup
						from target_data.cm_ldsity2target2categorization_setup as cmlcs
						where cmlcs.ldsity2target_variable in
							(
							select cmltv.id from target_data.cm_ldsity2target_variable as cmltv
							where cmltv.target_variable = _target_variable
							)
						)
					)
			,w2 as	(
					select distinct tlv.available_datasets from target_data.t_ldsity_values as tlv
					where tlv.available_datasets in (select w1.id from w1)
					and is_latest = true
					)
			,w3 as	(
					select distinct tad1.panel, tad1.reference_year_set
					from target_data.t_available_datasets as tad1 where tad1.id in
					(select w2.available_datasets from w2)
					)
			,w4 as	(
					select
							w3.panel as panel_id,
							w3.reference_year_set as reference_year_set_id,
							crpm.id as refyearset2panel,
							tp.panel,
							trys.reference_year_set from w3
					inner join sdesign.cm_refyearset2panel_mapping as crpm on w3.panel = crpm.panel and w3.reference_year_set = crpm.reference_year_set
					inner join sdesign.t_panel as tp on w3.panel = tp.id
					inner join sdesign.t_reference_year_set as trys on w3.reference_year_set = trys.id
					)
			select
					w4.refyearset2panel,
					w4.panel,
					w4.reference_year_set
			from
					w4 order by w4.refyearset2panel;
		else
			return query
			with
			w1 as	(
					select cm.* from sdesign.cm_refyearset2panel_mapping as cm
					where cm.id in (select unnest(_refyearset2panel_mapping))
					)
			,w2 as	(
					select
							w1.id as refyearset2panel,
							w1.panel as panel_id,
							w1.reference_year_set as reference_year_set_id,
							tp.panel,
							trys.reference_year_set
					from
							w1
							inner join sdesign.t_panel as tp on w1.panel = tp.id
							inner join sdesign.t_reference_year_set as trys on w1.reference_year_set = trys.id
					)
			select
					w2.refyearset2panel,
					w2.panel,
					w2.reference_year_set
			from
					w2 order by w2.refyearset2panel;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_panel_refyearset_combinations(integer, integer[]) is
'Function returns list of combinations of panels and reference year sets for given target variable that are available for ETL.';

grant execute on function target_data.fn_etl_get_panel_refyearset_combinations(integer, integer[]) to public;
-- </function>