--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_check_t_ldsity_values" schema="target_data" src="functions/fn_check_t_ldsity_values.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_check_t_ldsity_values
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_t_ldsity_values();

CREATE OR REPLACE FUNCTION target_data.fn_check_t_ldsity_values() RETURNS TRIGGER AS $src$
    DECLARE
		_vars int[];
		_plots int[];
		_refyearsets int[];
		_errp json;
    BEGIN
		IF (TG_OP = 'DELETE') THEN
			with w_vars as (
					select array_prepend(node, edges) as vs 
				from old_table
				inner join target_data.t_available_datasets on new_table.available_datasets = t_available_datasets.id
				inner join target_data.v_variable_hierarchy 
					on (t_available_datasets.categorization_setup = v_variable_hierarchy.node 
						or t_available_datasets.categorization_setup = any (v_variable_hierarchy.edges))
			)
			, w_vars_un as (
					select unnest(vs) as v from w_vars
			)
			select array_agg(distinct v) into _vars from w_vars_un;

			select array_agg(distinct plot) into _plots from old_table;

			select array_agg(distinct reference_year_set) into _refyearsets from target_data.t_available_datasets where id in (select distinct available_datasets from old_table);

			if _vars is not null
			then
				with w_err as (
					select target_data.fn_add_plot_target_attr(_vars, _plots, _refyearsets, 1e-6, true) as fn_err
				)
				, w_err_t as (
					select (fn_err).* from w_err
				)
				, w_err_json as (
					select 
						json_build_object(
							'plot'					, plot,
							'reference_year_set'	, reference_year_set,
							'variable'				, variable,
							'ldsity'				, ldsity,
							'ldsity_sum'			, ldsity_sum,
							'variables_def'			, variables_def,
							'variables_found'		, variables_found,
							'diff'					, diff
						) as errj
					from w_err_t
				)
				select json_agg(errj) into _errp from w_err_json;
	
				IF 
					(json_array_length(_errp) > 0) 
					THEN RAISE EXCEPTION 'fn_check_t_ldsity_values -- delete -- plot level local densities are not additive:
						checked vars: %, checked plots: %, 
						found err plots: 
						%', _vars, _plots, jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE WARNING 'fn_check_t_ldsity_values -- delete -- additivity check was skiped: 
						* 0 rows edited (next line displays NULL)
						* corresponding variable hierarchy not found (next line displays array)
						old_table available_datasets: %', (select array_agg(available_datasets) from old_table) as foo;
			end if;
------------------------------------------------
        ELSIF (TG_OP = 'UPDATE') THEN
			with w_vars as (
					select array_prepend(node, edges) as vs 
				from new_table
				inner join target_data.t_available_datasets on new_table.available_datasets = t_available_datasets.id
				inner join target_data.v_variable_hierarchy 
					on (t_available_datasets.categorization_setup = v_variable_hierarchy.node 
						or t_available_datasets.categorization_setup = any (v_variable_hierarchy.edges))
			)
			, w_vars_un as (
					select unnest(vs) as v from w_vars
			)
			select array_agg(distinct v) into _vars from w_vars_un;

			select array_agg(distinct plot) into _plots from new_table;

			select array_agg(distinct reference_year_set) into _refyearsets from target_data.t_available_datasets where id in (select distinct available_datasets from new_table);

			if _vars is not null
			then
				--raise notice '%		fn_check_t_ldsity_values -- UPDATE CHECK START -- select target_data.fn_add_plot_target_attr(%, %, %, %, %);', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', _vars, _plots, _refyearsets, 1e-6, true;
				with w_err as (
					select target_data.fn_add_plot_target_attr(_vars, _plots, _refyearsets, 1e-6, true) as fn_err
				)
				, w_err_t as (
					select (fn_err).* from w_err
				)
				, w_err_json as (
					select 
						json_build_object(
							'plot'					, plot,
							'reference_year_set'	, reference_year_set,
							'variable'				, variable,
							'ldsity'				, ldsity,
							'ldsity_sum'			, ldsity_sum,
							'variables_def'			, variables_def,
							'variables_found'		, variables_found,
							'diff'					, diff
						) as errj
					from w_err_t
				)
				select json_agg(errj) into _errp from w_err_json;
				--raise notice '%		fn_check_t_ldsity_values -- UPDATE CHECK STOP', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague';
				IF 
					(json_array_length(_errp) > 0) 
					THEN RAISE EXCEPTION 'fn_check_t_ldsity_values -- update -- plot level local densities are not additive:
						checked vars: %, checked plots: %, 
						found err plots: 
						%', _vars, _plots, jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE WARNING 'fn_check_t_ldsity_values -- update -- additivity check was skiped: 
						* 0 rows edited (next line displays NULL)
						* corresponding variable hierarchy not found (next line displays array)
						new_table available_datasets: %', (select array_agg(available_datasets) from new_table) as foo;
			end if;
------------------------------------------------
        ELSIF (TG_OP = 'INSERT') THEN
			with w_vars as (
					select array_prepend(node, edges) as vs 
				from new_table
				inner join target_data.t_available_datasets on new_table.available_datasets = t_available_datasets.id
				inner join target_data.v_variable_hierarchy 
					on (t_available_datasets.categorization_setup = v_variable_hierarchy.node 
						or t_available_datasets.categorization_setup = any (v_variable_hierarchy.edges))
			)
			, w_vars_un as (
					select unnest(vs) as v from w_vars
			)
			select array_agg(distinct v) into _vars from w_vars_un;

			select array_agg(distinct plot) into _plots from new_table;

			select array_agg(distinct reference_year_set) into _refyearsets from target_data.t_available_datasets where id in (select distinct available_datasets from new_table);
			
			if _vars is not null
			then
				--raise notice '%		fn_check_t_ldsity_values -- INSERT CHECK START -- select target_data.fn_add_plot_target_attr(%, %, %, %, %);', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', _vars, _plots, _refyearsets, 1e-6, true;
				with w_err as (
					select target_data.fn_add_plot_target_attr(_vars, _plots, _refyearsets, 1e-6, true) as fn_err
				)
				, w_err_t as (
					select (fn_err).* from w_err
				)
				, w_err_json as (
					select 
						json_build_object(
							'plot'					, plot,
							'reference_year_set'	, reference_year_set,
							'variable'				, variable,
							'ldsity'				, ldsity,
							'ldsity_sum'			, ldsity_sum,
							'variables_def'			, variables_def,
							'variables_found'		, variables_found,
							'diff'					, diff
						) as errj
					from w_err_t
				)
				select json_agg(errj) into _errp from w_err_json;
				--raise notice '%		fn_check_t_ldsity_values -- INSERT CHECK STOP', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague';
				IF 
					(json_array_length(_errp) > 0) 
					THEN RAISE EXCEPTION 'fn_check_t_ldsity_values -- insert -- plot level local densities are not additive:
						checked vars: %, checked plots: %, 
						found err plots: 
						%', _vars, _plots, jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE WARNING 'fn_check_t_ldsity_values -- insert -- additivity check was skiped: 
						* 0 rows edited (next line displays NULL)
						* corresponding variable hierarchy not found (next line displays array)
						new_table available_datasets: %', (select array_agg(available_datasets) from new_table) as foo;
			end if;
        END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$src$ LANGUAGE plpgsql;

drop trigger if exists trg__ldsity_values__ins ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__ins
    AFTER INSERT ON target_data.t_ldsity_values
    REFERENCING NEW TABLE AS new_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__ldsity_values__upd ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__upd
    AFTER UPDATE ON target_data.t_ldsity_values
    REFERENCING NEW TABLE AS new_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__ldsity_values__del ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__del
    AFTER DELETE ON target_data.t_ldsity_values
    REFERENCING OLD TABLE AS old_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

-- </function>
