--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(integer, character varying, boolean);


-- <function name="fn_etl_get_target_variable" schema="target_data" src="functions/etl/fn_etl_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(integer, character varying, boolean) CASCADE;

create or replace function target_data.fn_etl_get_target_variable
(
	_export_connection	integer,
	_national_language	character varying(2) default 'en'::character varying(2),
	_etl				boolean default null::boolean
)
returns table
(
	id							integer,
	label						varchar,
	id_etl_target_variable		integer,
	check_target_variable		boolean,
	refyearset2panel_mapping	integer[],
	metadata					json
)
as
$$
declare
		_cond			text;
		_set_column		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable: Input argument _export_connection must not be null!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_get_target_variable: Input argument _national_language must not be null!';
		end if;

		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w2.check_target_variable = TRUE';
			else
				_cond := 'w2.check_target_variable = FALSE';
			end if;
		end if;

		if _national_language = 'en'
		then
			_set_column := 'w1.label_en';
		else
			_set_column := 'w1.label';
		end if;

		return query execute
		'
		with
		w1 as	(
				select 
						ctv.*,
						(target_data.fn_etl_check_target_variable($1,ctv.id)) as res
				from 
						target_data.c_target_variable as ctv order by ctv.id
				)
		,w2 as	(
				select
						w1.id,
						'|| _set_column ||' as label,
						(w1.res).id_etl_target_variable,
						(w1.res).check_target_variable,
						(w1.res).refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select
						w2.*,
						target_data.fn_etl_get_target_variable_metadata(w2.id,$2) as metadata
				from
						w2 where '|| _cond ||'
				)
		,w4 as	(
				select
						w3.id,
						w3.label,
						w3.id_etl_target_variable,
						w3.check_target_variable,
						w3.refyearset2panel_mapping,
						w3.metadata->$2 as metadata
				from
						w3
				)
				select
						w4.id,
						w4.label,
						w4.id_etl_target_variable,
						w4.check_target_variable,
						w4.refyearset2panel_mapping,
						json_build_object($2,w4.metadata) as metadata
				from
						w4 order by w4.id;
		'
		using _export_connection, _national_language;

end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_target_variable(integer, character varying, boolean) IS
'The function returs target variables and informations for ETL. If input argument _etl = TRUE then function
returns target variables that was ETL and all theirs datas ar current. If input argument _etl = FALSE
then function retursn target variables that was not ETL yet or returns target variables that was ETL
and exists newer datas for ETL. If input argument _etl IS NULL (_etl = true or false) then function
returns both cases.';

grant execute on function target_data.fn_etl_get_target_variable(integer, character varying, boolean) to public;
-- </function>



-- <function name="fn_etl_get_target_variable_metadatas" schema="target_data" src="functions/etl/fn_etl_get_target_variable_metadatas.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_target_variable_metadatas
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable_metadatas(integer[], character varying) CASCADE;

create or replace function target_data.fn_etl_get_target_variable_metadatas
(
	_id_etl_target_variable	integer[],
	_national_language		character varying(2) default 'en'::character varying(2)
)
returns json
as
$$
declare
		_json_res	json;
begin
		if _id_etl_target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable_metadatas: Input argument _id_etl_target_variable must not by NULL!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_get_target_variable_metadatas: Input argument _national_language must not by NULL!';
		end if;
		---------------------------------------------------
		with
		w1 as	(
				select tetv.* from target_data.t_etl_target_variable as tetv
				where tetv.id in (select unnest(_id_etl_target_variable))
				)
		,w2 as	(
				select
						w1.target_variable,	-- id of target variable from source DB
						w1.etl_id,			-- id of target variable that is saved in target DB
						target_data.fn_etl_get_target_variable_metadata(w1.target_variable,_national_language) as metadata
				from
						w1 order by w1.target_variable
				)
		select
			json_agg(
			json_build_object
			(
			'target_variable',w2.target_variable,
			'etl_id',w2.etl_id,
			'metadata',w2.metadata
			))
		from
			w2
		into
			_json_res;
		-------------------------------------
		return _json_res;
		-------------------------------------
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_target_variable_metadatas(integer[], character varying) IS
'The function returs metadata for input target variables.';

grant execute on function target_data.fn_etl_get_target_variable_metadatas(integer[], character varying) to public;
-- </function>



--------------------------------------------------------------------;
-- Table: c_export_connection
--------------------------------------------------------------------;
ALTER TABLE target_data.c_export_connection DROP CONSTRAINT ukey__c_export_connection__label;

ALTER TABLE target_data.c_export_connection RENAME COLUMN description TO description_old;

ALTER TABLE target_data.c_export_connection ADD COLUMN host character varying;
ALTER TABLE target_data.c_export_connection ADD COLUMN dbname character varying;
ALTER TABLE target_data.c_export_connection ADD COLUMN port integer;
ALTER TABLE target_data.c_export_connection ADD COLUMN description text;

UPDATE target_data.c_export_connection
SET
	host = replace(replace(trim(split_part(label_en,' ',1)),'dbname=',''),'host=',''),
	dbname = replace(replace(trim(split_part(label_en,' ',2)),'host=',''),'dbname=',''),
	port = 5432::integer,
	description = coalesce(description_en,''::text)
WHERE
	id = id;

ALTER TABLE target_data.c_export_connection ALTER COLUMN host SET NOT NULL;
ALTER TABLE target_data.c_export_connection ALTER COLUMN dbname SET NOT NULL;
ALTER TABLE target_data.c_export_connection ALTER COLUMN port SET NOT NULL;

COMMENT ON COLUMN target_data.c_export_connection.host IS 'Host of connection.';
COMMENT ON COLUMN target_data.c_export_connection.dbname IS 'Database name of connection.';
COMMENT ON COLUMN target_data.c_export_connection.port IS 'Port of connection.';
COMMENT ON COLUMN target_data.c_export_connection.description IS 'Description of connection.';

ALTER TABLE target_data.c_export_connection ADD CONSTRAINT ukey__c_export_connection__host_dbname_port UNIQUE (host,dbname,port);
COMMENT ON CONSTRAINT ukey__c_export_connection__host_dbname_port ON target_data.c_export_connection IS 'Unique key on columns host, dbname and port.';

ALTER TABLE target_data.c_export_connection DROP COLUMN label;
ALTER TABLE target_data.c_export_connection DROP COLUMN description_old;
ALTER TABLE target_data.c_export_connection DROP COLUMN label_en;
ALTER TABLE target_data.c_export_connection DROP COLUMN description_en;
--------------------------------------------------------------------;



DROP FUNCTION IF EXISTS target_data.fn_etl_get_export_connections() CASCADE;
DROP FUNCTION IF EXISTS target_data.fn_etl_save_export_connection(varchar, text, varchar, text, integer) CASCADE;



-- <function name="fn_etl_get_export_connections" schema="target_data" src="functions/etl/fn_etl_get_export_connections.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_export_connections
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_export_connections() CASCADE;

create or replace function target_data.fn_etl_get_export_connections()
returns table
(
	id				integer,
	host			character varying,
	dbname			character varying,
	port			integer,
	description		text
)
as
$$
begin
	return query
	select t.id, t.host, t.dbname, t.port, t.description
	from target_data.c_export_connection as t
	order by t.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_export_connections() is
'Function returns records from c_export_connection table.';

grant execute on function target_data.fn_etl_get_export_connections() to public;
-- </function>



-- <function name="fn_etl_save_export_connection" schema="target_data" src="functions/etl/fn_etl_save_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_save_export_connection(varchar, varchar, integer, text) CASCADE;

create or replace function target_data.fn_etl_save_export_connection
(
	_host				varchar,
	_dbname				varchar,
	_port				integer,
	_description		text default null::text
)
returns integer
as
$$
declare
		_id		integer;
begin
	if _host is null or _dbname is null or _port is null
	then
		raise exception 'Error 01: fn_etl_save_export_connection: Mandatory input of host, dbname or port is null (%, %, %).', _host, _dbname, _port;
	end if; 

	insert into target_data.c_export_connection(host, dbname, port, description)
	select _host, _dbname, _port, _description
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_save_export_connection(varchar, varchar, integer, text) is
'Function inserts a record into table c_export_connection based on given parameters.';

grant execute on function target_data.fn_etl_save_export_connection(varchar, varchar, integer, text) to public;
-- </function>



-- <function name="fn_etl_update_export_connection" schema="target_data" src="functions/etl/fn_etl_update_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_update_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_etl_update_export_connection(integer, varchar, varchar, integer, text);

CREATE OR REPLACE FUNCTION target_data.fn_etl_update_export_connection
(
	_id integer,
	_host varchar,
	_dbname varchar, 
	_port integer, 
	_description text default null::text
)
RETURNS void
AS
$$
BEGIN
	
	IF _id IS NULL THEN 
		RAISE EXCEPTION 'Error: 01: fn_etl_update_export_connection: Parameter _id must not be null.';
	END IF;

	IF NOT EXISTS (SELECT * FROM target_data.c_export_connection AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Error: 02: fn_etl_update_export_connection: Given export connection (%) does not exist in c_export_connection table.', _id;
	END IF;
	
	IF _host IS NULL THEN 
		RAISE EXCEPTION 'Error: 03: fn_etl_update_export_connection: Parameter host must be given.';
	END IF;

	IF _dbname IS NULL THEN 
		RAISE EXCEPTION 'Error: 04: fn_etl_update_export_connection: Parameter dbname must be given.';
	END IF;

	IF _port IS NULL THEN 
		RAISE EXCEPTION 'Error: 05: fn_etl_update_export_connection: Parameter port must be given.';
	END IF;	

	UPDATE target_data.c_export_connection 
	SET 	host = _host, dbname = _dbname, 
			port = _port, description = _description
	WHERE c_export_connection.id = _id;	

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_etl_update_export_connection(integer, varchar, varchar, integer, text) IS
'Function provides update in c_export_connection table.';

GRANT EXECUTE ON FUNCTION target_data.fn_etl_update_export_connection(integer, varchar, varchar, integer, text) TO public;
-- </function>



-- <function name="fn_etl_try_delete_export_connection" schema="target_data" src="functions/etl/fn_etl_try_delete_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_try_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_try_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_etl_try_delete_export_connection(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_export_connection)
	THEN RAISE EXCEPTION 'Error 01: fn_etl_try_delete_export_connection: Given export connection does not exist in table c_export_connection (%)', _id;
	END IF;

	return not exists (
	select tetv.id from target_data.t_etl_target_variable as tetv where tetv.export_connection = _id	union all
	select tead.id from target_data.t_etl_area_domain as tead where tead.export_connection = _id		union all
	select tesp.id from target_data.t_etl_sub_population as tesp where tesp.export_connection = _id
	);

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_etl_try_delete_export_connection(integer) IS
'Function provides test if it is possible to delete records from c_export_connection table.';

GRANT EXECUTE ON FUNCTION target_data.fn_etl_try_delete_export_connection(integer) TO public;
-- </function>



-- <function name="fn_etl_delete_export_connection" schema="target_data" src="functions/etl/fn_etl_delete_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_etl_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_etl_delete_export_connection(_id integer)
RETURNS void
AS
$$
BEGIN

	IF _id IS NULL THEN 
		RAISE EXCEPTION 'Error: 01: fn_etl_delete_export_connection: Parameter _id must not be null.';
	END IF;

	IF NOT EXISTS (SELECT * FROM target_data.c_export_connection AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Error: 02: fn_etl_delete_export_connection: Given export connection (%) does not exist in c_export_connection table.', _id;
	END IF;
	
	DELETE FROM target_data.c_export_connection 
	WHERE id  = _id;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_etl_delete_export_connection(integer) IS
'Function delete record from c_export_connection.';

GRANT EXECUTE ON FUNCTION target_data.fn_etl_delete_export_connection(integer) TO public;
-- </function>