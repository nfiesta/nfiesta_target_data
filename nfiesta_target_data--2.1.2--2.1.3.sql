--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



--------------------------------------------------------------------;
-- Table: c_export_connection [renaming to t_export_connection]
--------------------------------------------------------------------;
ALTER TABLE target_data.c_export_connection RENAME COLUMN description TO comment;
COMMENT ON COLUMN target_data.c_export_connection.comment IS 'Comment of connection.';
UPDATE target_data.c_export_connection SET comment = ''::text WHERE comment IS NULL;
ALTER TABLE target_data.c_export_connection ALTER COLUMN comment SET NOT NULL;

ALTER TABLE target_data.t_etl_target_variable DROP CONSTRAINT fkey__t_etl_target_variable__c_export_connection;
ALTER TABLE target_data.t_etl_area_domain DROP CONSTRAINT fkey__t_etl_area_domain__c_export_connection;
ALTER TABLE target_data.t_etl_sub_population DROP CONSTRAINT fkey__t_etl_sub_population__c_export_connection;

DROP INDEX target_data.fki__t_etl_target_variable__c_export_connection;
DROP INDEX target_data.fki__t_etl_area_domain__c_export_connection;
DROP INDEX target_data.fki__t_etl_sub_population__c_export_connection;

ALTER TABLE target_data.c_export_connection RENAME TO t_export_connection;

ALTER TABLE target_data.t_etl_target_variable
  ADD CONSTRAINT fkey__t_etl_target_variable__t_export_connection FOREIGN KEY (export_connection)
      REFERENCES target_data.t_export_connection (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_etl_target_variable__t_export_connection ON target_data.t_etl_target_variable IS
'Foreign key to table t_export_connection.';

CREATE INDEX fki__t_etl_target_variable__t_export_connection ON target_data.t_etl_target_variable USING btree (export_connection);
COMMENT ON INDEX target_data.fki__t_etl_target_variable__t_export_connection IS
'BTree index on foreign key fkey__t_etl_target_variable__t_export_connection.';

ALTER TABLE target_data.t_etl_area_domain
  ADD CONSTRAINT fkey__t_etl_area_domain__t_export_connection FOREIGN KEY (export_connection)
      REFERENCES target_data.t_export_connection (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_etl_area_domain__t_export_connection ON target_data.t_etl_area_domain IS
'Foreign key to table t_export_connection.';

CREATE INDEX fki__t_etl_area_domain__t_export_connection ON target_data.t_etl_area_domain USING btree (export_connection);
COMMENT ON INDEX target_data.fki__t_etl_area_domain__t_export_connection IS
'BTree index on foreign key fkey__t_etl_area_domain__t_export_connection.';

ALTER TABLE target_data.t_etl_sub_population
  ADD CONSTRAINT fkey__t_etl_sub_population__t_export_connection FOREIGN KEY (export_connection)
      REFERENCES target_data.t_export_connection (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_etl_sub_population__t_export_connection ON target_data.t_etl_sub_population IS
'Foreign key to table t_export_connection.';

CREATE INDEX fki__t_etl_sub_population__t_export_connection ON target_data.t_etl_sub_population USING btree (export_connection);
COMMENT ON INDEX target_data.fki__t_etl_sub_population__t_export_connection IS
'BTree index on foreign key fkey__t_etl_sub_population__t_export_connection.';
--------------------------------------------------------------------;



DROP FUNCTION IF EXISTS target_data.fn_etl_get_export_connections();
DROP FUNCTION IF EXISTS target_data.fn_etl_save_export_connection(varchar, varchar, integer, text);
DROP FUNCTION IF EXISTS target_data.fn_etl_update_export_connection(integer, varchar, varchar, integer, text);
DROP FUNCTION IF EXISTS target_data.fn_etl_try_delete_export_connection(integer);
DROP FUNCTION IF EXISTS target_data.fn_etl_delete_export_connection(integer);



-- <function name="fn_etl_get_export_connections" schema="target_data" src="functions/etl/fn_etl_get_export_connections.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_export_connections
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_export_connections() CASCADE;

create or replace function target_data.fn_etl_get_export_connections()
returns table
(
	id				integer,
	host			character varying,
	dbname			character varying,
	port			integer,
	comment			text
)
as
$$
begin
	return query
	select t.id, t.host, t.dbname, t.port, t.comment
	from target_data.t_export_connection as t
	order by t.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_export_connections() is
'Function returns records from t_export_connection table.';

grant execute on function target_data.fn_etl_get_export_connections() to public;
-- </function>



-- <function name="fn_etl_save_export_connection" schema="target_data" src="functions/etl/fn_etl_save_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_save_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_save_export_connection(varchar, varchar, integer, text) CASCADE;

create or replace function target_data.fn_etl_save_export_connection
(
	_host				varchar,
	_dbname				varchar,
	_port				integer,
	_comment			text
)
returns integer
as
$$
declare
		_id		integer;
begin
	if _host is null or _dbname is null or _port is null or _comment is null
	then
		raise exception 'Error 01: fn_etl_save_export_connection: Mandatory input of host, dbname, port or comment is null (%, %, %, %).', _host, _dbname, _port, _comment;
	end if; 

	insert into target_data.t_export_connection(host, dbname, port, comment)
	select _host, _dbname, _port, _comment
	returning id
	into _id;

	return _id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_save_export_connection(varchar, varchar, integer, text) is
'Function inserts a record into table t_export_connection based on given parameters.';

grant execute on function target_data.fn_etl_save_export_connection(varchar, varchar, integer, text) to public;
-- </function>



-- <function name="fn_etl_update_export_connection" schema="target_data" src="functions/etl/fn_etl_update_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_update_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_etl_update_export_connection(integer, varchar, varchar, integer, text);

CREATE OR REPLACE FUNCTION target_data.fn_etl_update_export_connection
(
	_id integer,
	_host varchar,
	_dbname varchar, 
	_port integer, 
	_comment text
)
RETURNS void
AS
$$
BEGIN
	
	IF _id IS NULL THEN 
		RAISE EXCEPTION 'Error: 01: fn_etl_update_export_connection: Parameter _id must not be null.';
	END IF;

	IF NOT EXISTS (SELECT * FROM target_data.t_export_connection AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Error: 02: fn_etl_update_export_connection: Given export connection (%) does not exist in t_export_connection table.', _id;
	END IF;
	
	IF _host IS NULL THEN 
		RAISE EXCEPTION 'Error: 03: fn_etl_update_export_connection: Parameter host must be given.';
	END IF;

	IF _dbname IS NULL THEN 
		RAISE EXCEPTION 'Error: 04: fn_etl_update_export_connection: Parameter dbname must be given.';
	END IF;

	IF _port IS NULL THEN 
		RAISE EXCEPTION 'Error: 05: fn_etl_update_export_connection: Parameter port must be given.';
	END IF;

	IF _comment IS NULL THEN 
		RAISE EXCEPTION 'Error: 06: fn_etl_update_export_connection: Parameter comment must be given.';
	END IF;		

	UPDATE target_data.t_export_connection 
	SET 	host = _host, dbname = _dbname, 
			port = _port, comment = _comment
	WHERE t_export_connection.id = _id;	

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_etl_update_export_connection(integer, varchar, varchar, integer, text) IS
'Function provides update in t_export_connection table.';

GRANT EXECUTE ON FUNCTION target_data.fn_etl_update_export_connection(integer, varchar, varchar, integer, text) TO public;
-- </function>



-- <function name="fn_etl_try_delete_export_connection" schema="target_data" src="functions/etl/fn_etl_try_delete_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_try_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_try_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_etl_try_delete_export_connection(_id integer)
RETURNS boolean
AS
$$
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.t_export_connection)
	THEN RAISE EXCEPTION 'Error 01: fn_etl_try_delete_export_connection: Given export connection does not exist in table t_export_connection (%)', _id;
	END IF;

	return not exists (
	select tetv.id from target_data.t_etl_target_variable as tetv where tetv.export_connection = _id	union all
	select tead.id from target_data.t_etl_area_domain as tead where tead.export_connection = _id		union all
	select tesp.id from target_data.t_etl_sub_population as tesp where tesp.export_connection = _id
	);

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_etl_try_delete_export_connection(integer) IS
'Function provides test if it is possible to delete records from t_export_connection table.';

GRANT EXECUTE ON FUNCTION target_data.fn_etl_try_delete_export_connection(integer) TO public;
-- </function>



-- <function name="fn_etl_delete_export_connection" schema="target_data" src="functions/etl/fn_etl_delete_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_etl_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_etl_delete_export_connection(_id integer)
RETURNS void
AS
$$
BEGIN

	IF _id IS NULL THEN 
		RAISE EXCEPTION 'Error: 01: fn_etl_delete_export_connection: Parameter _id must not be null.';
	END IF;

	IF NOT EXISTS (SELECT * FROM target_data.t_export_connection AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Error: 02: fn_etl_delete_export_connection: Given export connection (%) does not exist in t_export_connection table.', _id;
	END IF;
	
	DELETE FROM target_data.t_export_connection 
	WHERE id  = _id;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_etl_delete_export_connection(integer) IS
'Function delete record from t_export_connection.';

GRANT EXECUTE ON FUNCTION target_data.fn_etl_delete_export_connection(integer) TO public;
-- </function>