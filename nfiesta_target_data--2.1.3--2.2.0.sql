--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP FUNCTION target_data.fn_get_target_variable(integer) CASCADE;
-- <function name="fn_get_target_variable" schema="target_data" src="functions/fn_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_target_variable(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_target_variable(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer[],
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
state_or_change		integer,
soc_label		character varying(200),
soc_description		text,
areal_or_population	integer
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		WITH 
		w_objects AS (
			SELECT t2.ldsity_object_group, array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			GROUP BY t2.ldsity_object_group
			),
		w_core AS (
			SELECT 	t5.target_variable, 
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = false) AS ldsity_objects_pozit,
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = true) AS ldsity_objects_negat
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		),
		w_target_variable AS (
			SELECT 	t2.ldsity_object_group, t1.target_variable, t3.label, t3.description, t3.label_en, t3.description_en, t3.state_or_change, 
				t4.label AS soc_label, t4.description AS soc_description
			FROM w_core AS t1
			INNER JOIN w_objects AS t2
			ON t2.ldsity_objects = t1.ldsity_objects_pozit OR (t2.ldsity_objects @> t1.ldsity_objects_pozit AND t2.ldsity_objects @> t1.ldsity_objects_negat)
			INNER JOIN target_data.c_target_variable AS t3
			ON t1.target_variable = t3.id
			INNER JOIN target_data.c_state_or_change AS t4
			ON t3.state_or_change = t4.id
		),
		w_agg_200 AS (
			SELECT 	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, 
				t2.version,
				t5.areal_or_population,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = false) AS ldsity_pozit,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = true) AS ldsity_negat,
				count(*) OVER (PARTITION BY t1.target_variable) AS no_of_contrib
			FROM w_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.target_variable = t2.target_variable AND t2.ldsity_object_type = 100
			INNER JOIN target_data.c_ldsity AS t4
			ON t2.ldsity = t4.id
			INNER JOIN target_data.c_ldsity_object AS t5
			ON t4.ldsity_object = t5.id
			LEFT JOIN target_data.cm_ldsity2target_variable AS t3
			ON t1.target_variable = t3.target_variable AND t3.ldsity_object_type = 200 AND t2.use_negative = t3.use_negative
			GROUP BY t1.target_variable, t1.ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, t2.version, t5.areal_or_population
		), w_agg_id AS (
			-- query with one contribution which can be divided by other contributions
			(SELECT array_agg(t1.target_variable) AS ids, t1.ldsity_object_group,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib = 1
			GROUP BY t1.ldsity, t1.ldsity_object_group, 
				t1.area_domain_category, t1.sub_population_category,
				t1.state_or_change, t1.soc_label, t1.soc_description, t1.areal_or_population,
				t1.ldsity_object_type, CASE WHEN t1.state_or_change != 200 THEN t1.version END
			)
			UNION ALL
			-- query with more than one contribution, can not be divided
			(SELECT array_agg(DISTINCT t1.target_variable) AS ids, t1.ldsity_object_group, 
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib > 1
			GROUP BY t1.ldsity_object_group, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type, CASE WHEN t1.state_or_change != 200 THEN t1.version END
			)
		)
		SELECT ids AS id, t1.ldsity_object_group, t1.label[1], t1.description[1], t1.label_en[1], t1.description_en[1],
			t1.state_or_change, t1.soc_label, t1.soc_description,
			t1.areal_or_population[1]
		FROM w_agg_id AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		WITH 
		w_objects AS (
			SELECT t2.ldsity_object_group, array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			WHERE t1.id = _ldsity_object_group
			GROUP BY t2.ldsity_object_group
			),
		w_core AS (
			SELECT 	t5.target_variable,
 				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = false) AS ldsity_objects_pozit,
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = true) AS ldsity_objects_negat
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		),
		w_target_variable AS (
			SELECT 	t2.ldsity_object_group, t1.target_variable, t3.label, t3.description, t3.label_en, t3.description_en, t3.state_or_change, 
				t4.label AS soc_label, t4.description AS soc_description
			FROM w_core AS t1
			INNER JOIN w_objects AS t2
			ON t2.ldsity_objects = t1.ldsity_objects_pozit OR (t2.ldsity_objects @> t1.ldsity_objects_pozit AND t2.ldsity_objects @> t1.ldsity_objects_pozit)
			INNER JOIN target_data.c_target_variable AS t3
			ON t1.target_variable = t3.id
			INNER JOIN target_data.c_state_or_change AS t4
			ON t3.state_or_change = t4.id
		),
		w_agg_200 AS (
			SELECT 	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category,
				t2.version,
				t5.areal_or_population,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = false) AS ldsity_pozit,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = true) AS ldsity_negat,
				count(*) OVER (PARTITION BY t1.target_variable) AS no_of_contrib
			FROM w_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.target_variable = t2.target_variable AND t2.ldsity_object_type = 100
			INNER JOIN target_data.c_ldsity AS t4
			ON t2.ldsity = t4.id
			INNER JOIN target_data.c_ldsity_object AS t5
			ON t4.ldsity_object = t5.id
			LEFT JOIN target_data.cm_ldsity2target_variable AS t3
			ON t1.target_variable = t3.target_variable AND t3.ldsity_object_type = 200 AND t2.use_negative = t3.use_negative
			GROUP BY t1.target_variable, t1.ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, t2.version, t5.areal_or_population
		), w_agg_id AS (
			-- query with one contribution which can be divided by other contributions
			(SELECT array_agg(t1.target_variable) AS ids, t1.ldsity_object_group,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib = 1
			GROUP BY t1.ldsity, t1.ldsity_object_group, 
				t1.area_domain_category, t1.sub_population_category,
				t1.state_or_change, t1.soc_label, t1.soc_description, t1.areal_or_population,
				t1.ldsity_object_type, CASE WHEN t1.state_or_change != 200 THEN t1.version END
			)
			UNION ALL
			-- query with more than one contribution, can not be divided
			(SELECT array_agg(DISTINCT t1.target_variable) AS ids, t1.ldsity_object_group, 
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description 
			FROM w_agg_200 AS t1
			WHERE no_of_contrib > 1
			GROUP BY t1.ldsity_object_group, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type, CASE WHEN t1.state_or_change != 200 THEN t1.version END
			)
		)
		SELECT ids AS id, t1.ldsity_object_group, t1.label[1], t1.description[1], t1.label_en[1], t1.description_en[1],
			t1.state_or_change, t1.soc_label, t1.soc_description,
			t1.areal_or_population[1]
		FROM w_agg_id AS t1;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_target_variable(integer) IS
'Function returns records from c_target_variable table, optionally for given ldsity_object_group.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_target_variable(integer) TO public;

-- </function>

-- <function name="fn_check_classification_rules" schema="target_data" src="functions/fn_check_classification_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rules(_ldsity integer, _ldsity_object integer, _rules text[], _panel_refyearset integer DEFAULT NULL::integer, _adc integer[][] DEFAULT NULL::integer[][], _spc integer[][] DEFAULT NULL::integer[][])
--boolean
RETURNS TABLE (
result		boolean,
message_id	integer,
message		text
) 
AS
$$
DECLARE
_ldsity_objects		integer[];
_adc_rule_test		integer[];
_spc_rule_test		integer[];
_table_name4rule	varchar;
_rule			text;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_cases			varchar[];
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_q_all			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
_exist_test		boolean;
_exist_test_all		boolean[];
--_qrule			text;
_adc4loop2d		integer[][];
_spc4loop2d		integer[][];
_adc4loop		integer[];
_spc4loop		integer[];
adcv			integer;
spcv			integer;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	IF NOT array[_ldsity_object] <@ (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]))
	THEN
		RAISE EXCEPTION 'Given ldsity object (%) is not found within the hierarchy of ldsity contribution (%).', _ldsity_object, _ldsity;
	END IF;

	FOREACH _rule IN ARRAY _rules
	LOOP
		SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
		INTO _test;

		IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
		THEN
			_exist_test := true; _rule = true;
		ELSE 	_exist_test := false;
		END IF;

		_exist_test_all := _exist_test_all || _exist_test;

		_test_all := _test_all || array[_test];

		_case := 'CASE WHEN '||_rule||' THEN true ELSE false END';

		_cases := _cases || _case;
		--raise notice '%', _test_all;
	END LOOP;

	IF _test_all @> array[false]
	THEN
		RAISE EXCEPTION 'One or more given rules has an invalid syntax.';
	END IF;

	IF _exist_test_all @> array[true] AND _exist_test_all @> array[false]
	THEN
		RAISE EXCEPTION 'In array of given rules there is an EXISTS or NOT EXISTS rule but there are also another rules, this rules have to be alone within one sub population.';
	END IF;

	IF _exist_test_all @> array[true] AND _exist_test_all != ARRAY[true,true]
	THEN
		RAISE EXCEPTION 'The given array of rules has more or less then 2 rules (%). If this is a special case of EXISTS rule, then its counterpart NOT EXISTS has to be entered and nothing else.',_rules;
	END IF;

	_ldsity_objects := (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]));


	IF _adc IS NOT NULL AND array_length(_rules,1) != array_length(_adc,1)
	THEN
		RAISE EXCEPTION 'Given array of area domain categories must be the same length as the given array of rules.';
	END IF;

	IF _spc IS NOT NULL AND array_length(_rules,1) != array_length(_spc,1)
	THEN
		RAISE EXCEPTION 'Given array of sub population categories must be the same length as the given array of rules.';
	END IF;

-- get id from cm_adc2classification_rule id for each adc
	WITH RECURSIVE w_cm_ids AS (
		SELECT
				t2.id AS object_id, t1.id, t1.area_domain_category, t3.array_id, t3.id AS ordinality
			FROM
				target_data.cm_adc2classification_rule AS t1
			INNER JOIN
				unnest(_ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
			ON
			   t1.ldsity_object = t2.ldsity_object
			RIGHT JOIN
				(
				SELECT  adc, id, ceil(id/(count(*) OVER())::numeric * array_length(_adc,1)) AS array_id
				FROM  unnest(_adc) WITH ORDINALITY AS t1(adc, id)
				) AS t3
			ON
				t1.area_domain_category = t3.adc
			ORDER BY t2.id DESC, t3.array_id ASC
			-- because some rule can be met on more than one object, the rules from the object which is hierarchically higher will be picked
			LIMIT array_length(_adc,1) * array_length(_adc,2)
			),
	w_hier AS (
		SELECT 	t2.id AS cat_id, t2.ordinality, t2.array_id, 
			t2.id AS cm_id, t3.id AS cm_id_sup,
			t1.variable_superior, 2 AS id_sup, t1.variable, 1 AS id,
 			CASE WHEN t1.variable_superior IS NOT NULL THEN array[t1.variable_superior,t1.variable] ELSE array[t1.variable] END AS array_variable,
			CASE WHEN t3.id IS NOT NULL THEN array[t3.id, t2.id] ELSE array[t2.id] END AS cm_id_array_variable
		FROM target_data.t_adc_hierarchy AS t1
		RIGHT JOIN
			w_cm_ids AS t2
		ON t1.variable = t2.area_domain_category
		LEFT JOIN
			target_data.cm_adc2classification_rule AS t3
		ON t1.variable_superior = t3.area_domain_category
		LEFT JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		UNION ALL
		SELECT 	t2.cat_id, t2.ordinality, t2.array_id, 
			t2.cm_id, t2.cm_id_sup,
			t1.variable_superior, id_sup + 1, t1.variable, t2.id + 1, 
			array_prepend(t1.variable_superior, array_variable),
			array_prepend(t3.id, cm_id_array_variable)
		FROM target_data.t_adc_hierarchy AS t1
		INNER JOIN w_hier AS t2
		ON t1.variable = t2.variable_superior
		INNER JOIN target_data.cm_adc2classification_rule AS t3
		ON t1.variable_superior = t3.area_domain_category
		INNER JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
	), w_nulls AS (
		SELECT cat_id, ordinality, array_id, t1.array_variable[array_length(t1.array_variable,1)] AS cat, 
			array_variable, cm_id_array_variable,
			max(array_length(array_variable,1)) OVER () - array_length(array_variable,1) AS nulls2app,
			max(array_length(array_variable,1)) OVER () AS max_len
		FROM w_hier AS t1 
		LEFT JOIN target_data.t_adc_hierarchy AS t2 
		ON t1.variable_superior = t2.variable 
		WHERE t2.variable_superior IS NULL
	), w_append AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS var,
			cm_id_array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS cm_var,
			array_fill(NULL::int, ARRAY[max_len]) AS null_array, count(*) OVER(PARTITION BY cat) AS total
		FROM w_nulls
	), w_cat AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_agg(var) AS var_agg, 
			array_agg(cm_var) AS cm_var_agg, 
			null_array
		FROM w_append
		GROUP BY cat_id, cat, ordinality, array_id, null_array
	), w_cat_agg AS (
		SELECT cat_id, ordinality, array_id, cat, 
			var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS nulls2app,
			cm_var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS cm_nulls2app
		FROM w_cat
	), w_agg3d AS (
		SELECT
			array_id, 
			array_agg(nulls2app ORDER BY ordinality) AS cats,
			array_agg(cm_nulls2app ORDER BY ordinality) AS cm_ids
		FROM w_cat_agg
		GROUP BY array_id
	)
	SELECT 	--array_agg(cats ORDER BY array_id),
		array_agg(cm_ids ORDER BY array_id)
	FROM w_agg3d
	INTO _adc;

--raise notice '%', _ldsity_objects;
	WITH RECURSIVE w_cm_ids AS (
		SELECT
				t2.id AS object_id, t1.id, t1.sub_population_category, t3.array_id, t3.id AS ordinality
			FROM
				target_data.cm_spc2classification_rule AS t1
			INNER JOIN
				unnest(_ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
			ON
			   t1.ldsity_object = t2.ldsity_object
			RIGHT JOIN
				(
				SELECT  spc, id, ceil(id/(count(*) OVER())::numeric * array_length(_spc,1)) AS array_id
				FROM  unnest(_spc) WITH ORDINALITY AS t1(spc, id)
				) AS t3
			ON
				t1.sub_population_category = t3.spc
			ORDER BY t2.id DESC, t3.array_id ASC
			-- because some rule can be met on more than one object, the rules from the object which is hierarchically higher will be picked
			LIMIT array_length(_spc,1) * array_length(_spc,2)
			),
	w_hier AS (
		SELECT 	t2.id AS cat_id, t2.ordinality, t2.array_id, 
			t2.id AS cm_id, t3.id AS cm_id_sup,
			t1.variable_superior, 2 AS id_sup, t1.variable, 1 AS id,
  			CASE WHEN t3.id IS NOT NULL THEN array[t1.variable_superior,t1.variable] ELSE array[t1.variable] END AS array_variable,
			CASE WHEN t3.id IS NOT NULL THEN array[t3.id, t2.id] ELSE array[t2.id] END AS cm_id_array_variable
		FROM target_data.t_spc_hierarchy AS t1
		RIGHT JOIN
			w_cm_ids AS t2
		ON t1.variable = t2.sub_population_category
		LEFT JOIN
			target_data.cm_spc2classification_rule AS t3
		ON 	t1.variable_superior = t3.sub_population_category AND
			t3.classification_rule NOT IN ('EXISTS','NOT EXISTS')
		LEFT JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		UNION ALL
		SELECT 	t2.cat_id, t2.ordinality, t2.array_id, 
			t2.cm_id, t2.cm_id_sup,
			t1.variable_superior, id_sup + 1, t1.variable, t2.id + 1, 
			array_prepend(t1.variable_superior, array_variable),
			array_prepend(t3.id, cm_id_array_variable)
		FROM target_data.t_spc_hierarchy AS t1
		INNER JOIN w_hier AS t2
		ON t1.variable = t2.variable_superior
		INNER JOIN target_data.cm_spc2classification_rule AS t3
		ON t1.variable_superior = t3.sub_population_category
		INNER JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		WHERE t3.classification_rule NOT IN ('EXISTS','NOT EXISTS')
	), w_nulls AS (
		SELECT cat_id, ordinality, array_id, t1.array_variable[array_length(t1.array_variable,1)] AS cat, 
			array_variable, cm_id_array_variable,
			max(array_length(array_variable,1)) OVER () - array_length(array_variable,1) AS nulls2app,
			max(array_length(array_variable,1)) OVER () AS max_len
		FROM w_hier AS t1 
		LEFT JOIN target_data.t_spc_hierarchy AS t2 
		ON t1.variable_superior = t2.variable 
		WHERE t2.variable_superior IS NULL
	), w_append AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS var,
			cm_id_array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS cm_var,
			array_fill(NULL::int, ARRAY[max_len]) AS null_array, count(*) OVER(PARTITION BY cat) AS total
		FROM w_nulls
	), w_cat AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_agg(var) AS var_agg, 
			array_agg(cm_var) AS cm_var_agg, 
			null_array
		FROM w_append
		GROUP BY cat_id, cat, ordinality, array_id, null_array
	), w_cat_agg AS (
		SELECT cat_id, ordinality, array_id, cat, 
			var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS nulls2app,
			cm_var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS cm_nulls2app
		FROM w_cat
	), w_agg3d AS (
		SELECT
			array_id, 
			array_agg(nulls2app ORDER BY ordinality) AS cats,
			array_agg(cm_nulls2app ORDER BY ordinality) AS cm_ids
		FROM w_cat_agg
		GROUP BY array_id
	)
	SELECT 	--array_agg(cats ORDER BY array_id),
		array_agg(cm_ids ORDER BY array_id)
	FROM w_agg3d
	INTO _spc;


-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			(SELECT current_database()::information_schema.sql_identifier AS table_catalog,
				x.tblschema::information_schema.sql_identifier AS table_schema,
				x.tblname::information_schema.sql_identifier AS table_name,
				x.colname::information_schema.sql_identifier AS column_name,
				current_database()::information_schema.sql_identifier AS constraint_catalog,
				x.cstrschema::information_schema.sql_identifier AS constraint_schema,
				x.cstrname::information_schema.sql_identifier AS constraint_name
			FROM 
				(SELECT DISTINCT nr.nspname,
					r.relname,
					r.relowner,
					a.attname,
					nc.nspname,
					c.conname
				FROM pg_namespace nr,
					pg_class r,
					pg_attribute a,
					pg_depend d,
					pg_namespace nc,
					pg_constraint c
			 	WHERE 
					nr.oid = r.relnamespace AND r.oid = a.attrelid AND 
					d.refclassid = 'pg_class'::regclass::oid AND d.refobjid = r.oid AND 
					d.refobjsubid = a.attnum AND d.classid = 'pg_constraint'::regclass::oid AND 
					d.objid = c.oid AND c.connamespace = nc.oid AND c.contype = 'c'::"char" AND 
					(r.relkind = ANY (ARRAY['r'::"char", 'p'::"char"])) AND NOT a.attisdropped
				UNION ALL
				SELECT nr.nspname,
					r.relname,
					r.relowner,
					a.attname,
					nc.nspname,
					c.conname
			   	FROM pg_namespace nr,
					pg_class r,
					pg_attribute a,
					pg_namespace nc,
					pg_constraint c
				WHERE 
					nr.oid = r.relnamespace AND r.oid = a.attrelid AND nc.oid = c.connamespace AND r.oid =
					CASE c.contype
					WHEN 'f'::"char" THEN c.confrelid
					ELSE c.conrelid
					END AND (a.attnum = ANY (
						CASE c.contype
						WHEN 'f'::"char" THEN c.confkey
				    		ELSE c.conkey
						END)
					) AND NOT a.attisdropped AND (c.contype = ANY (ARRAY['p'::"char", 'u'::"char", 'f'::"char"])) AND 
					(r.relkind = ANY (ARRAY['r'::"char", 'p'::"char"]))
				) AS x(tblschema, tblname, tblowner, colname, cstrschema, cstrname)
			) AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	_table_name4rule := (SELECT table_name FROM target_data.c_ldsity_object WHERE id = _ldsity_object);

	FOR i IN 1..array_length(_cases,1)
	LOOP
		--raise notice 'i %', i;
		CASE
		WHEN _adc IS NOT NULL
		THEN

			-- 4D array, for each category build 2D array - each array within is an unique combination of categories/rules
			-- and each object must belong only into one combination
			_adc4loop2d :=  
					(WITH w_un AS (
						SELECT	adc,
							id AS ordinality,
							ceil(id/(count(*) OVER())::numeric * array_length(_adc[i:i],2) * array_length(_adc[i:i],3)) AS array_id
						FROM 
							unnest(_adc[i:i][:]) WITH ORDINALITY AS t(adc,id)
					), w_dist AS (
						SELECT array_agg(adc ORDER BY ordinality) AS adc, array_id
						FROM w_un
						GROUP BY array_id
					)
					SELECT array_agg(adc ORDER BY array_id) 
					FROM w_dist);
			--raise notice 'adc %', _adc;
			--raise notice 'adc4loop2d %', _adc4loop2d;
			FOR y IN 1..array_length(_adc4loop2d,1)
			LOOP
				-- for 2d array, because slicing will result again in 2D array even with one element/dimension
				-- it is necessary to unnest it and aggregate again -> 1D array
				_adc4loop :=  (SELECT array_agg(adc) FROM unnest(_adc4loop2d[y:y][:]) AS t(adc));

				--raise notice 'y %, adc4loop %', y, _adc4loop;
				IF cardinality(array_remove(_adc4loop,NULL)) > 0 OR y = 1
				THEN
					--raise notice 'y %, spc4loop %', y, _spc4loop;
					FOR adcv IN SELECT generate_subscripts(_adc4loop,1)
					LOOP
					--raise notice 'adcv %', adcv;
					IF adcv = 1 OR _adc4loop[adcv] IS NOT NULL THEN
						WITH w_tables AS (
						SELECT
							t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
								CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
								CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',i, ' AS rule_number, ', _cases[i],' AS rul ') ELSE '' END,
								' FROM ', t1.table_name, 
								CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
								') AS ', _table_names_ws[t1.id]) AS table_select
						FROM
							unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
						LEFT JOIN
							(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
							FROM
								(SELECT t4.table_name, t3.classification_rule 
								FROM target_data.c_ldsity AS t1,
									unnest(t1.area_domain_category || _adc4loop[adcv]) WITH ORDINALITY AS t2(rule, id)
								LEFT JOIN target_data.cm_adc2classification_rule AS t3
								ON t2.rule = t3.id
								INNER JOIN target_data.c_ldsity_object AS t4
								ON t3.ldsity_object = t4.id
								WHERE t1.id = _ldsity
								UNION ALL
								SELECT t4.table_name, t3.classification_rule 
								FROM target_data.c_ldsity AS t1,
									unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
								LEFT JOIN target_data.cm_spc2classification_rule AS t3
								ON t2.rule = t3.id
								INNER JOIN target_data.c_ldsity_object AS t4
								ON t3.ldsity_object = t4.id
								WHERE t1.id = _ldsity
								UNION ALL
								SELECT t2.table_name, t2.filter 
								FROM target_data.c_ldsity AS t1
								INNER JOIN target_data.c_ldsity_object AS t2
								ON t1.ldsity_object = t2.id
								WHERE t1.id = _ldsity
								) AS t1
							GROUP BY table_name
							) AS t2
						ON t1.table_name = t2.table_name
					)
					SELECT array_agg(table_select ORDER BY id)
					FROM w_tables
					INTO _table_selects;

					SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
						_table_names_ws[1], '.', _primary_keys[1], '
						FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
						INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
						INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
						INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
						_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
					INTO _table1;

					_join_all := NULL; _join := NULL;

					FOR i IN 2..array_length(_table_selects,1) 
					LOOP
						SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
						INTO _join;

						_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
						--raise notice '%', _join_all;
					END LOOP;

					--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
					_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

					_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
			--raise notice '%', _table1;
					_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL ' ELSE '' END), _q);

					END IF;
					END LOOP;
				END IF;
			END LOOP;
			--_qrule := concat(CASE WHEN _qrule IS NOT NULL THEN concat(_qrule, ' UNION ALL ') ELSE '' END, _q_all);
			--raise notice '%',_q_all;
		WHEN _spc IS NOT NULL
		THEN
			-- 4D array, for each category build 2D array - each array within is an unique combination of categories/rules
			-- and each object must belong only into one combination
			_spc4loop2d :=  
					(WITH w_un AS (
						SELECT	spc,
							id AS ordinality,
							ceil(id/(count(*) OVER())::numeric * array_length(_spc[i:i],2) * array_length(_spc[i:i],3)) AS array_id
						FROM 
							unnest(_spc[i:i][:]) WITH ORDINALITY AS t(spc,id)
					), w_dist AS (
						SELECT array_agg(spc ORDER BY ordinality) AS spc, array_id
						FROM w_un
						GROUP BY array_id
					)
					SELECT array_agg(spc ORDER BY array_id) 
					FROM w_dist);
			--raise notice 'spc %', _spc;
			--raise notice 'spc4loop2d %', _spc4loop2d;
			FOR y IN 1..array_length(_spc4loop2d,1)
			LOOP
				-- for 2d array, because slicing will result again in 2D array even with one element/dimension
				-- it is necessary to unnest it and aggregate again -> 1D array
				_spc4loop :=  (SELECT array_agg(spc) FROM unnest(_spc4loop2d[y:y][:]) AS t(spc));
				--raise notice 'i = %, spc = %, spc4loop = %', i, _spc, _spc4loop;
				IF cardinality(array_remove(_spc4loop,NULL)) > 0 OR y = 1
				THEN
					--raise notice 'y %, spc4loop %', y, _spc4loop;
					FOR spcv IN SELECT generate_subscripts(_spc4loop,1)
					LOOP

					IF spcv = 1 OR _spc4loop[spcv] IS NOT NULL THEN
					--raise notice 'spcv %', _spc4loop[spcv];
						WITH w_tables AS (
						SELECT
							t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
								CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
								CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',i, ' AS rule_number, ', _cases[i],' AS rul ') ELSE '' END,
								' FROM ', t1.table_name, 
								CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
								') AS ', _table_names_ws[t1.id]) AS table_select
						FROM
							unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
						LEFT JOIN
							(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
							FROM
								(SELECT t4.table_name, t3.classification_rule 
								FROM target_data.c_ldsity AS t1,
									unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
								LEFT JOIN target_data.cm_adc2classification_rule AS t3
								ON t2.rule = t3.id
								INNER JOIN target_data.c_ldsity_object AS t4
								ON t3.ldsity_object = t4.id
								WHERE t1.id = _ldsity
								UNION ALL
								SELECT t4.table_name, t3.classification_rule 
								FROM target_data.c_ldsity AS t1,
									unnest(t1.sub_population_category || _spc4loop[spcv]) WITH ORDINALITY AS t2(rule, id)
								LEFT JOIN target_data.cm_spc2classification_rule AS t3
								ON t2.rule = t3.id
								INNER JOIN target_data.c_ldsity_object AS t4
								ON t3.ldsity_object = t4.id
								WHERE t1.id = _ldsity
								UNION ALL
								SELECT t2.table_name, t2.filter 
								FROM target_data.c_ldsity AS t1
								INNER JOIN target_data.c_ldsity_object AS t2
								ON t1.ldsity_object = t2.id
								WHERE t1.id = _ldsity
								) AS t1
							GROUP BY table_name
							) AS t2
						ON t1.table_name = t2.table_name
					)
					SELECT array_agg(table_select ORDER BY id)
					FROM w_tables
					INTO _table_selects;

					SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
						_table_names_ws[1], '.', _primary_keys[1], '
						FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
						INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
						INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
						INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
						_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
					INTO _table1;

					_join_all := NULL; _join := NULL;

					FOR i IN 2..array_length(_table_selects,1) 
					LOOP
						SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
						INTO _join;

						_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
						--raise notice '%', _join_all;
					END LOOP;

					--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
					_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

					_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
			--raise notice '%', _table1;
					_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL ' ELSE '' END), _q);
					--raise notice 'loop spcs, rule %, spc %', i, spcv;
					--raise notice '%', _q_all; 

					END IF;
					END LOOP;
				END IF;
			END LOOP;

			--_qrule := concat(concat(_qrule, CASE WHEN _qrule IS NOT NULL THEN ' UNION ALL ' ELSE '' END), _q_all);
		ELSE
			WITH w_tables AS (
				SELECT
					t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
						CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
						CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',i, ' AS rule_number, ', _cases[i],' AS rul ') ELSE '' END,
						' FROM ', t1.table_name, 
						CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
						') AS ', _table_names_ws[t1.id]) AS table_select
				FROM
					unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
				LEFT JOIN
					(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
					FROM
						(SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_adc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_spc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t2.table_name, t2.filter 
						FROM target_data.c_ldsity AS t1
						INNER JOIN target_data.c_ldsity_object AS t2
						ON t1.ldsity_object = t2.id
						WHERE t1.id = _ldsity
						) AS t1
					GROUP BY table_name
					) AS t2
				ON t1.table_name = t2.table_name
			)
			SELECT array_agg(table_select ORDER BY id)
			FROM w_tables
			INTO _table_selects;

			SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
				_table_names_ws[1], '.', _primary_keys[1], '
				FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
				INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
				INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
				INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
				_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
			INTO _table1;

			_join_all := NULL; _join := NULL;

			FOR i IN 2..array_length(_table_selects,1) 
			LOOP
				SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
				INTO _join;

				_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
				--raise notice '%', _join_all;
			END LOOP;

			--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
			_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

			_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
	--raise notice '%', _table1;
			_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL ' ELSE '' END), _q);

			--_qrule := _q_all;
		END CASE;
	END LOOP;

	--raise notice '%',_q_all;

	EXECUTE
	'WITH w AS (' || _q_all || '),
	w2 AS (SELECT
			id, rul, sum(CASE WHEN rul=true THEN 1 ELSE 0 END) AS no_of_rules_met
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
		GROUP BY id, rul
	), w_test AS (
		SELECT no_of_rules_met::int, count(*)::int AS no_of_objects
		FROM w2
		GROUP BY no_of_rules_met
	)
	SELECT
		array_agg(no_of_rules_met ORDER BY no_of_rules_met) AS no_of_rules_met,
		array_agg(no_of_objects ORDER BY no_of_rules_met) AS no_of_objects
	FROM
		w_test
	'
	INTO _no_of_rules_met, _no_of_objects;

--raise notice '%, %', _no_of_rules_met, _no_of_objects;

	_total := (SELECT sum(nob) FROM unnest(_no_of_objects) AS t(nob));

--return query select true, _no_of_rules_met, _no_of_objects;

	-- if both are NULL 
IF _no_of_rules_met IS NULL AND _no_of_objects IS NULL
THEN	
	RETURN QUERY 
		SELECT false, 0, concat('There were no records on which the rules could be tested - given combination of panels and reference year sets probably does not correspond with the local density contribution availability.')::text;
ELSE
-- if one rule given - all records have 1 rule met
	IF array_length(_rules,1) = 1
	THEN
		IF array_length(_no_of_rules_met,1) != 1
		THEN
			RETURN QUERY 
			SELECT false, 1, concat('Only one rule was given, so all records has to result into true. There were only ', _no_of_objects[2], ' number of objects successfully categorized from ', _total, ' overall total.')::text;
		ELSE
			IF _no_of_rules_met[1] = 0
			THEN
				RETURN QUERY
				SELECT false, 2, concat('Only one rule was given, and none of the ', _no_of_objects[1], ' number of objects was successfully categorized.')::text;
			ELSE
			--raise notice '%, %', _no_of_rules_met, _no_of_objects;
				RETURN QUERY 
				SELECT true, 3, concat('Only one rule was given and every object was successfully categorized by it. All from ', _no_of_objects[1], ' number of objects resulted in ', _total, ' successful records.')::text;
			END IF;
		END IF;
	END IF;


-- if 2 or more rules given - all records have 1 rule met and 0 rule met
	-- both numbers must be the same (logically if 1 object belogns to some rule, basically also fails to the other rules)

	IF array_length(_rules,1) > 1
	THEN
		-- there are objects with 2 or more rules met (intersection between rules, can be from 0,1,2 or 1,2)
		IF EXISTS(SELECT * FROM unnest(_no_of_rules_met) AS t(nor) WHERE nor >= 2)
		THEN
			IF _exist_test_all = array[true, true]
			THEN
				RETURN QUERY
				SELECT true, 8, concat('Special case of EXISTS rules, implicitly true for all records.')::text;
			ELSE
				RETURN QUERY 
				SELECT false, 4, concat('Some of the objects were categorized by more than one rule, there is an intersection between the rule conditions.')::text;
			END IF;
		ELSE
			-- there are objects which are not covered by rules (only 0,1)
			IF array_length(_no_of_rules_met,1) = 2
			THEN
				IF _no_of_objects[1] != _no_of_objects[2]
				THEN
					RETURN QUERY 
					SELECT false, 5, concat('Some of the objects (', _no_of_objects[1] - _no_of_objects[2], ' from total of ', _no_of_objects[1], ') were not successfully categorized by any of the rules. Consider editing hierarchy.')::text;
				ELSE
					-- the rest, where 0,1, both the same number of objects
					IF _no_of_objects[1] = _no_of_objects[2]
					THEN
						RETURN QUERY
						SELECT true, 6, concat('Every object (', _no_of_objects[1], ') was succesfully categorized without any intersection.')::text;
					ELSE
						--raise notice '%, %', _no_of_rules_met, _no_of_objects;
						RAISE EXCEPTION '01: Not known state of classification rules.';
					END IF;
				END IF;
			ELSE
				IF array_length(_no_of_rules_met,1) = 1 AND _no_of_rules_met[1] = 0
				THEN
					RETURN QUERY 
					SELECT false, 7, concat('None of the ', _no_of_objects[1], ' was successfully categorized by any of the rules.')::text;
				ELSE
					RAISE EXCEPTION '02: Not known state of classification rules.';
				END IF;
			END IF;
		END IF;
	END IF;
END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[], integer[]) IS
'Function checks syntax in text field with classification rules for given ldsity contribution and ldsity object within its hierarchy. Optionally also for given panel x reference year set combination and/or  area domain/sub population category/ies.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[],  integer[]) TO public;

-- </function>
