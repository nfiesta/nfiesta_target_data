--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP FUNCTION target_data.fn_get_target_variable(integer) CASCADE;

-- <function name="fn_get_target_variable" schema="target_data" src="functions/fn_get_target_variable.sql">
--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_target_variable(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_target_variable(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer[],
core			boolean[],
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
state_or_change		integer,
soc_label		character varying(200),
soc_description		text,
areal_or_population	integer
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		WITH 
		w_objects AS (
			SELECT t2.ldsity_object_group, array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			GROUP BY t2.ldsity_object_group
			),
		w_core AS (
			SELECT 	t5.target_variable, 
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = false) AS ldsity_objects_pozit,
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = true) AS ldsity_objects_negat
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		),
		w_target_variable AS (
			SELECT 	t2.ldsity_object_group, t1.target_variable, t3.label, t3.description, t3.label_en, t3.description_en, t3.state_or_change, 
				t4.label AS soc_label, t4.description AS soc_description
			FROM w_core AS t1
			INNER JOIN w_objects AS t2
			ON t2.ldsity_objects = t1.ldsity_objects_pozit AND CASE WHEN t1.ldsity_objects_negat IS NOT NULL THEN t2.ldsity_objects = t1.ldsity_objects_negat ELSE true END
			INNER JOIN target_data.c_target_variable AS t3
			ON t1.target_variable = t3.id
			INNER JOIN target_data.c_state_or_change AS t4
			ON t3.state_or_change = t4.id
		),
		w_agg_200 AS (
			SELECT 	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, 
				t2.version,
				t5.areal_or_population,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = false) AS ldsity_pozit,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = true) AS ldsity_negat,
				count(*) OVER (PARTITION BY t1.target_variable) AS no_of_contrib
			FROM w_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.target_variable = t2.target_variable AND t2.ldsity_object_type = 100
			INNER JOIN target_data.c_ldsity AS t4
			ON t2.ldsity = t4.id
			INNER JOIN target_data.c_ldsity_object AS t5
			ON t4.ldsity_object = t5.id
			LEFT JOIN target_data.cm_ldsity2target_variable AS t3
			ON t1.target_variable = t3.target_variable AND t3.ldsity_object_type = 200 AND t2.use_negative = t3.use_negative
			GROUP BY t1.target_variable, t1.ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, t2.version, t5.areal_or_population
		), w_agg_core AS (
			SELECT	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type, t1.ldsity, t1.area_domain_category, t1.sub_population_category,
				t1.version, t1.areal_or_population, t1.ldsity_pozit, t1.ldsity_negat, t1.no_of_contrib,
				CASE WHEN t1.ldsity_pozit IS NOT NULL OR t1.ldsity_negat IS NOT NULL THEN false ELSE true END AS core
			FROM w_agg_200 AS t1
		), w_agg_id AS (
			-- query with one contribution which can be divided by other contributions
			(SELECT array_agg(t1.target_variable) AS ids, t1.ldsity_object_group,
				array_agg(t1.core ORDER BY t1.target_variable) AS core,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_core AS t1
			WHERE no_of_contrib = 1
			GROUP BY t1.ldsity, t1.ldsity_object_group, 
				t1.area_domain_category, t1.sub_population_category,
				t1.state_or_change, t1.soc_label, t1.soc_description, t1.areal_or_population,
				t1.ldsity_object_type, CASE WHEN t1.state_or_change != 200 THEN t1.version END
			)
			UNION ALL
			-- query with more than one contribution, can not be divided
			(SELECT array_agg(DISTINCT t1.target_variable) AS ids, t1.ldsity_object_group, 
				array_agg(DISTINCT t1.core) AS core,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_core AS t1
			WHERE no_of_contrib > 1
			GROUP BY t1.ldsity_object_group, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type, CASE WHEN t1.state_or_change != 200 THEN t1.version END
			)
		)
		SELECT ids AS id, t1.core, t1.ldsity_object_group, t1.label[1], t1.description[1], t1.label_en[1], t1.description_en[1],
			t1.state_or_change, t1.soc_label, t1.soc_description,
			t1.areal_or_population[1]
		FROM w_agg_id AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		WITH 
		w_objects AS (
			SELECT t2.ldsity_object_group, array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			WHERE t1.id = _ldsity_object_group
			GROUP BY t2.ldsity_object_group
			),
		w_core AS (
			SELECT 	t5.target_variable,
 				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = false) AS ldsity_objects_pozit,
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = true) AS ldsity_objects_negat
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		),
		w_target_variable AS (
			SELECT 	t2.ldsity_object_group, t1.target_variable, t3.label, t3.description, t3.label_en, t3.description_en, t3.state_or_change, 
				t4.label AS soc_label, t4.description AS soc_description
			FROM w_core AS t1
			INNER JOIN w_objects AS t2
			ON t2.ldsity_objects = t1.ldsity_objects_pozit AND CASE WHEN t1.ldsity_objects_negat IS NOT NULL THEN t2.ldsity_objects = t1.ldsity_objects_negat ELSE true END
			INNER JOIN target_data.c_target_variable AS t3
			ON t1.target_variable = t3.id
			INNER JOIN target_data.c_state_or_change AS t4
			ON t3.state_or_change = t4.id
		),
		w_agg_200 AS (
			SELECT 	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category,
				t2.version,
				t5.areal_or_population,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = false) AS ldsity_pozit,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = true) AS ldsity_negat,
				count(*) OVER (PARTITION BY t1.target_variable) AS no_of_contrib
			FROM w_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.target_variable = t2.target_variable AND t2.ldsity_object_type = 100
			INNER JOIN target_data.c_ldsity AS t4
			ON t2.ldsity = t4.id
			INNER JOIN target_data.c_ldsity_object AS t5
			ON t4.ldsity_object = t5.id
			LEFT JOIN target_data.cm_ldsity2target_variable AS t3
			ON t1.target_variable = t3.target_variable AND t3.ldsity_object_type = 200 AND t2.use_negative = t3.use_negative
			GROUP BY t1.target_variable, t1.ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, t2.version, t5.areal_or_population
		), w_agg_core AS (
			SELECT	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type, t1.ldsity, t1.area_domain_category, t1.sub_population_category,
				t1.version, t1.areal_or_population, t1.ldsity_pozit, t1.ldsity_negat, t1.no_of_contrib,
				CASE WHEN t1.ldsity_pozit IS NOT NULL OR t1.ldsity_negat IS NOT NULL THEN false ELSE true END AS core
			FROM w_agg_200 AS t1
		), w_agg_id AS (
			-- query with one contribution which can be divided by other contributions
			(SELECT array_agg(t1.target_variable) AS ids, t1.ldsity_object_group,
				array_agg(t1.core ORDER BY t1.target_variable) AS core,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_core AS t1
			WHERE no_of_contrib = 1
			GROUP BY t1.ldsity, t1.ldsity_object_group, 
				t1.area_domain_category, t1.sub_population_category,
				t1.state_or_change, t1.soc_label, t1.soc_description, t1.areal_or_population,
				t1.ldsity_object_type, CASE WHEN t1.state_or_change != 200 THEN t1.version END
			)
			UNION ALL
			-- query with more than one contribution, can not be divided
			(SELECT array_agg(DISTINCT t1.target_variable) AS ids, t1.ldsity_object_group, 
				array_agg(DISTINCT t1.core) AS core,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description 
			FROM w_agg_core AS t1
			WHERE no_of_contrib > 1
			GROUP BY t1.ldsity_object_group, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type, CASE WHEN t1.state_or_change != 200 THEN t1.version END
			)
		)
		SELECT ids AS id, t1.core, t1.ldsity_object_group, t1.label[1], t1.description[1], t1.label_en[1], t1.description_en[1],
			t1.state_or_change, t1.soc_label, t1.soc_description,
			t1.areal_or_population[1]
		FROM w_agg_id AS t1;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_target_variable(integer) IS
'Function returns records from c_target_variable table, optionally for given ldsity_object_group.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_target_variable(integer) TO public;

-- </function>
