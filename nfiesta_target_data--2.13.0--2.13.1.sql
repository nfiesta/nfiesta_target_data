--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_import_get_adc_hierarchies" schema="target_data" src="functions/import/fn_import_get_adc_hierarchies.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_adc_hierarchies
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_adc_hierarchies(integer, integer, integer, boolean, integer[]) CASCADE;

create or replace function target_data.fn_import_get_adc_hierarchies
(
	_id								integer,
	_variable_superior__etl_id		integer,
	_variable__etl_id				integer,
	_dependent						boolean,
	_etl_id							integer[] default null::integer[]
)
returns table
(
	id					integer,
	etl_id				integer,
	variable_superior	integer,
	variable			integer,
	dependent			boolean
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_adc_hierarchies: Input argument _id must not be NULL!';
		end if;
	
		if _variable_superior__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_adc_hierarchies: Input argument _variable_superior__etl_id must not be NULL!';
		end if;
	
		if _variable__etl_id is null
		then
			raise exception 'Error 03: fn_import_get_adc_hierarchies: Input argument _variable__etl_id must not be NULL!';
		end if;
	
		if _dependent is null
		then
			raise exception 'Error 04: fn_import_get_adc_hierarchies: Input argument _dependent must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 05: fn_import_get_adc_hierarchies: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		select
				_id as id,
				tah.id as etl_id,
				tah.variable_superior,
				tah.variable,
				tah.dependent
		from
				target_data.t_adc_hierarchy as tah
		where
				tah.id not in (select unnest(_etl_id))
		and		tah.variable_superior = _variable_superior__etl_id
		and		tah.variable = _variable__etl_id
		and		tah.dependent = _dependent;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_adc_hierarchies(integer, integer, integer, boolean, integer[]) is
'Function returns records from table t_adc_hierarchy.';

grant execute on function target_data.fn_import_get_adc_hierarchies(integer, integer, integer, boolean, integer[]) to public;
-- </function>



-- <function name="fn_import_get_adc2classification_rules" schema="target_data" src="functions/import/fn_import_get_adc2classification_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_adc2classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_adc2classification_rules(integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_adc2classification_rules
(
	_adc2classification_rule		integer,
	_area_domain_category__etl_id	integer,
	_ldsity_object__etl_id			integer,
	_etl_id							integer[] default null::integer[]
)
returns table
(
	adc2classification_rule					integer,
	ldsity_object__id						integer,
	ldsity_object__label					varchar,
	ldsity_object__description				text,
	ldsity_object__label_en					varchar,
	ldsity_object__description_en			text,
	area_domain_category__id				integer,
	area_domain_category__label				varchar,
	area_domain_category__description		text,
	area_domain_category__label_en			varchar,
	area_domain_category__description_en	text,
	etl_id									integer,
	classification_rule						text
)
as
$$
declare
begin
		if _adc2classification_rule is null
		then
			raise exception 'Error 01: fn_import_get_adc2classification_rules: Input argument _adc2classification_rule must not be NULL!';
		end if;
	
		if _area_domain_category__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_adc2classification_rules: Input argument _area_domain_category__etl_id must not be NULL!';
		end if;
	
		if _ldsity_object__etl_id is null
		then
			raise exception 'Error 03: fn_import_get_adc2classification_rules: Input argument _ldsity_object__etl_id must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 04: fn_import_get_adc2classification_rules: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		with
		w1 as	(
				select
						_adc2classification_rule as adc2classification_rule,
						cacr.id as etl_id,
						cacr.area_domain_category as area_domain_category__id,
						cacr.ldsity_object as ldsity_object__id,
						cacr.classification_rule
				from
						target_data.cm_adc2classification_rule as cacr
				where
						cacr.area_domain_category = _area_domain_category__etl_id
				and
						cacr.ldsity_object = _ldsity_object__etl_id
				and
						cacr.id not in (select unnest(_etl_id))
				)
		select
				w1.adc2classification_rule,
				w1.ldsity_object__id,
				clo.label as ldsity_object__label,
				clo.description as ldsity_object__description,
				clo.label_en as ldsity_object__label_en,
				clo.description_en as ldsity_object__description_en,
				w1.area_domain_category__id,
				cadc.label as area_domain_category__label,
				cadc.description as area_domain_category__description,
				cadc.label_en as area_domain_category__label_en,
				cadc.description_en as area_domain_category__description_en,
				w1.etl_id,
				w1.classification_rule
		from
				w1
				inner join target_data.c_area_domain_category as cadc on w1.area_domain_category__id = cadc.id
				inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
		order 
				by w1.ldsity_object__id, w1.area_domain_category__id, w1.adc2classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_adc2classification_rules(integer, integer, integer, integer[]) is
'Function returns records from table cm_adc2classification_rule.';

grant execute on function target_data.fn_import_get_adc2classification_rules(integer, integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_adc2classrule2panel_refyearsets" schema="target_data" src="functions/import/fn_import_get_adc2classrule2panel_refyearsets.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_adc2classrule2panel_refyearsets
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_adc2classrule2panel_refyearsets(integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_adc2classrule2panel_refyearsets
(
	_id										integer,
	_adc2classification_rule__etl_id		integer,
	_refyearset2panel						integer,
	_etl_id									integer[] default null::integer[]
)
returns table
(
	id							integer,
	etl_id						integer,
	adc2classification_rule		integer,
	refyearset2panel			integer
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_adc2classrule2panel_refyearsets: Input argument _id must not be NULL!';
		end if;
	
		if _adc2classification_rule__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_adc2classrule2panel_refyearsets: Input argument _adc2classification_rule__etl_id must not be NULL!';
		end if;
	
		if _refyearset2panel is null
		then
			raise exception 'Error 03: fn_import_get_adc2classrule2panel_refyearsets: Input argument _refyearset2panel must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 04: fn_import_get_adc2classrule2panel_refyearsets: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		select
				_id as id,
				cacpr.id as etl_id,
				cacpr.adc2classification_rule,
				cacpr.refyearset2panel
		from
				target_data.cm_adc2classrule2panel_refyearset as cacpr
		where
				cacpr.id not in (select unnest(_etl_id))
		and		cacpr.adc2classification_rule = _adc2classification_rule__etl_id
		and		cacpr.refyearset2panel = _refyearset2panel;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_adc2classrule2panel_refyearsets(integer, integer, integer, integer[]) is
'Function returns records from table cm_adc2classrule2panel_refyearset.';

grant execute on function target_data.fn_import_get_adc2classrule2panel_refyearsets(integer, integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_area_domain_categories" schema="target_data" src="functions/import/fn_import_get_area_domain_categories.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_area_domain_categories
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_area_domain_categories(integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_area_domain_categories
(
	_area_domain_category	integer,
	_area_domain__etl_id	integer,
	_etl_id					integer[] default null::integer[]
)
returns table
(
	area_domain_category	integer,
	etl_id					integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text
)
as
$$
declare
begin
		if _area_domain_category is null
		then
			raise exception 'Error 01: fn_import_get_area_domain_categories: Input argument _area_domain_category must not be NULL!';
		end if;
	
		if _area_domain__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_area_domain_categories: Input argument _area_domain__etl_id must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 03: fn_import_get_area_domain_categories: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		select
				_area_domain_category as area_domain_category,
				cadc.id as etl_id,
				cadc.label,
				cadc.description,
				cadc.label_en,
				cadc.description_en
		from 
				target_data.c_area_domain_category as cadc
		where
				cadc.area_domain = _area_domain__etl_id
		and
				cadc.id not in (select unnest(_etl_id))
		order
				by cadc.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_area_domain_categories(integer, integer, integer[]) is
'Function returns records from table c_area_domain_category.';

grant execute on function target_data.fn_import_get_area_domain_categories(integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_area_domains" schema="target_data" src="functions/import/fn_import_get_area_domains.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_area_domains
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_area_domains(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_area_domains
(
	_area_domain		integer,
	_etl_id				integer[] default null::integer[]
)
returns table
(
	area_domain			integer,
	etl_id				integer,
	label				varchar,
	description			text,
	label_en			varchar,
	description_en		text
)
as
$$
declare
begin
		if _area_domain is null
		then
			raise exception 'Error 01: fn_import_get_area_domains: Input argument _area_domain must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 02: fn_import_get_area_domains: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		select
				_area_domain as area_domain,
				cad.id as etl_id,
				cad.label,
				cad.description,
				cad.label_en,
				cad.description_en
		from 
				target_data.c_area_domain as cad
		where
				cad.id not in (select unnest(_etl_id))
		order
				by cad.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_area_domains(integer, integer[]) is
'Function returns records from table c_area_domain.';

grant execute on function target_data.fn_import_get_area_domains(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_areal_or_populations" schema="target_data" src="functions/import/fn_import_get_areal_or_populations.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_areal_or_populations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_areal_or_populations(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_areal_or_populations
(
	_id			integer,
	_etl_id		integer[] default null::integer[]
)
returns table
(
	id				integer,
	etl_id			integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_areal_or_populations: Input argument _id must not be null!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 02: fn_import_get_areal_or_populations: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;

		if _etl_id is null
		then
			return query
			select
					_id as id,
					caop.id as etl_id,
					caop.label,
					caop.description,
					caop.label as label_en,
					caop.description as description_en
			from
					target_data.c_areal_or_population as caop
			order
					by caop.id;
		else
			return query
			select
					_id as id,
					caop.id as etl_id,
					caop.label,
					caop.description,
					caop.label as label_en,
					caop.description as description_en
			from
					target_data.c_areal_or_population as caop
			where
					caop.id not in (select unnest(_etl_id))
			order
					by caop.id;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_areal_or_populations(integer, integer[]) is
'Function returns records from table c_areal_or_population for given input arguments.';

grant execute on function target_data.fn_import_get_areal_or_populations(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_definition_variants" schema="target_data" src="functions/import/fn_import_get_definition_variants.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_definition_variants
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_definition_variants(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_definition_variants
(
	_definition_variant		integer,
	_etl_id					integer[] default null::integer[]
)
returns table
(
	definition_variant		integer,
	etl_id					integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text
)
as
$$
declare
begin
		if _definition_variant is null
		then
			raise exception 'Error 01: fn_import_get_definition_variants: Input argument _definition_variant must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 02: fn_import_get_definition_variants: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		select
				_definition_variant as definition_variant,
				cdv.id as etl_id,
				cdv.label,
				cdv.description,
				cdv.label_en,
				cdv.description_en
		from 
				target_data.c_definition_variant as cdv
		where
				cdv.id not in (select unnest(_etl_id))
		order
				by cdv.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_definition_variants(integer, integer[]) is
'Function returns records from table c_definition_variant.';

grant execute on function target_data.fn_import_get_definition_variants(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_ldsities" schema="target_data" src="functions/import/fn_import_get_ldsities.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_ldsities
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_ldsities(integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_ldsities
(
	_ldsity_object	integer,
	_ldsity			integer,
	_etl_id			integer[] default null::integer[]
)
returns table
(
	ldsity											integer,
	id												integer,
	label											varchar,
	description										text,
	label_en										varchar,
	description_en									text,
	ldsity_object__id								integer,
	ldsity_object__label							varchar,
	ldsity_object__description						text,
	ldsity_object__label_en							varchar,
	ldsity_object__description_en					text,
	column_expression								text,
	unit_of_measure__id								integer,
	unit_of_measure__label							varchar,
	unit_of_measure__description					text,
	unit_of_measure__label_en						varchar,
	unit_of_measure__description_en					text,	
	area_domain_category__id						integer[],
	area_domain_category__classification_rule		text[],
	sub_population_category__id						integer[],
	sub_population_category__classification_rule	text[],
	definition_variant__id							integer[],
	definition_variant__label						varchar[],
	definition_variant__description					text[],
	definition_variant__label_en					varchar[],
	definition_variant__description_en				text[]
)
as
$$
declare
begin
		if _ldsity_object is null
		then
			raise exception 'Error 01: fn_import_get_ldsities: Input argument _ldsity_object must not be NULL!';
		end if;

		if _ldsity is null
		then
			raise exception 'Error 02: fn_import_get_ldsities: Input argument _ldsity must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 03: fn_import_get_ldsities: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		with
		w1 as	(
				select
						_ldsity as ldsity,
						cl.id,
						cl.label,
						cl.description,
						cl.label_en,
						cl.description_en,
						cl.ldsity_object as ldsity_object__id,
						cl.column_expression,
						cl.unit_of_measure as unit_of_measure__id,
						cl.area_domain_category as area_domain_category__id,
						cl.sub_population_category as sub_population_category__id,
						cl.definition_variant as definition_variant__id
				from 
						target_data.c_ldsity as cl
				where
						cl.ldsity_object = _ldsity_object
				and 
						cl.id not in (select unnest(_etl_id))
				)
		,w2 as	(
				select
						w1.*,
						clo.label as ldsity_object__label,
						clo.description as ldsity_object__description,
						clo.label_en as ldsity_object__label_en,
						clo.description_en as ldsity_object__description_en,
						cuom.label as unit_of_measure__label,
						cuom.description as unit_of_measure__description,
						cuom.label_en as unit_of_measure__label_en,
						cuom.description_en as unit_of_measure__description_en,
						-------------------------------------------------------
						case
							when w1.area_domain_category__id is null then null::text[]
							else	(
									select array_agg(cacr.classification_rule order by cacr.id)
									from target_data.cm_adc2classification_rule as cacr
									where cacr.id in (select unnest(w1.area_domain_category__id))
									)
						end as area_domain_category__classification_rule,
						-------------------------------------------------------
						case
							when w1.sub_population_category__id is null then null::text[]
							else	(
									select array_agg(cscr.classification_rule order by cscr.id)
									from target_data.cm_spc2classification_rule as cscr
									where cscr.id in (select unnest(w1.sub_population_category__id))
									)
						end as sub_population_category__classification_rule,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::varchar[]
							else	(
									select array_agg(cdv_1.label order by cdv_1.id)
									from target_data.c_definition_variant as cdv_1
									where cdv_1.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__label,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::text[]
							else	(
									select array_agg(cdv_2.description order by cdv_2.id)
									from target_data.c_definition_variant as cdv_2
									where cdv_2.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__description,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::varchar[]
							else	(
									select array_agg(cdv_3.label_en order by cdv_3.id)
									from target_data.c_definition_variant as cdv_3
									where cdv_3.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__label_en,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::text[]
							else	(
									select array_agg(cdv_4.description_en order by cdv_4.id)
									from target_data.c_definition_variant as cdv_4
									where cdv_4.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__description_en
				from
						w1
						inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
						left join target_data.c_unit_of_measure as cuom on w1.unit_of_measure__id = cuom.id
				)
		select 
				w2.ldsity,
				w2.id,
				w2.label,
				w2.description,
				w2.label_en,
				w2.description_en,
				w2.ldsity_object__id,
				w2.ldsity_object__label,
				w2.ldsity_object__description,
				w2.ldsity_object__label_en,
				w2.ldsity_object__description_en,
				w2.column_expression,
				w2.unit_of_measure__id,
				w2.unit_of_measure__label,
				w2.unit_of_measure__description,
				w2.unit_of_measure__label_en,
				w2.unit_of_measure__description_en,
				w2.area_domain_category__id,
				w2.area_domain_category__classification_rule,
				w2.sub_population_category__id,
				w2.sub_population_category__classification_rule,
				w2.definition_variant__id,
				w2.definition_variant__label,
				w2.definition_variant__description,
				w2.definition_variant__label_en,
				w2.definition_variant__description_en
		from
				w2
		order
				by w2.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_ldsities(integer, integer, integer[]) is
'Function returns records from table c_ldsity.';

grant execute on function target_data.fn_import_get_ldsities(integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_ldsity_objects" schema="target_data" src="functions/import/fn_import_get_ldsity_objects.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_ldsity_objects
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_ldsity_objects(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_ldsity_objects
(
	_ldsity_object	integer,
	_etl_id			integer[] default null::integer[]
)
returns table
(
	ldsity_object			integer,
	id						integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	table_name				varchar,
	upper_object			integer,
	areal_or_population		integer,
	column4upper_object		varchar,
	filter 					text
)
as
$$
declare
begin
		if _ldsity_object is null
		then
			raise exception 'Error 01: fn_import_get_ldsity_objects: Input argument _ldsity_object must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 02: fn_import_get_ldsity_objects: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		select
				_ldsity_object as ldsity_object,
				clo.id,
				clo.label,
				clo.description,
				clo.label_en,
				clo.description_en,
				clo.table_name,
				clo.upper_object,
				clo.areal_or_population,
				clo.column4upper_object,
				clo.filter
		from 
				target_data.c_ldsity_object as clo
		where
				clo.id not in (select unnest(_etl_id))
		order
				by clo.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_ldsity_objects(integer, integer[]) is
'Function returns records from table c_ldsity_object.';

grant execute on function target_data.fn_import_get_ldsity_objects(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_ldsity2panel_refyearset_versions" schema="target_data" src="functions/import/fn_import_get_ldsity2panel_refyearset_versions.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_ldsity2panel_refyearset_versions
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_ldsity2panel_refyearset_versions(integer, integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_ldsity2panel_refyearset_versions
(
	_id						integer,
	_etl_ldsity__etl_id		integer,
	_refyearset2panel		integer,
	_etl_version__etl_id	integer,
	_etl_id					integer[] default null::integer[]
)
returns table
(
	id					integer,
	etl_id				integer,
	ldsity				integer,
	refyearset2panel	integer,
	version				integer
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_ldsity2panel_refyearset_versions: Input argument _id must not be NULL!';
		end if;
	
		if _etl_ldsity__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_ldsity2panel_refyearset_versions: Input argument _etl_ldsity__etl_id must not be NULL!';
		end if;
	
		if _refyearset2panel is null
		then
			raise exception 'Error 03: fn_import_get_ldsity2panel_refyearset_versions: Input argument _refyearset2panel must not be NULL!';
		end if;

		if _etl_version__etl_id is null
		then
			raise exception 'Error 04: fn_import_get_ldsity2panel_refyearset_versions: Input argument _etl_version__etl_id must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 05: fn_import_get_ldsity2panel_refyearset_versions: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		select
				_id as id,
				cl2prv.id as etl_id,
				cl2prv.ldsity,
				cl2prv.refyearset2panel,
				cl2prv.version
		from
				target_data.cm_ldsity2panel_refyearset_version as cl2prv
		where
				cl2prv.id not in (select unnest(_etl_id))
		and		cl2prv.ldsity = _etl_ldsity__etl_id
		and		cl2prv.refyearset2panel = _refyearset2panel
		and		cl2prv.version = _etl_version__etl_id
		
		order by cl2prv.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_ldsity2panel_refyearset_versions(integer, integer, integer, integer, integer[]) is
'Function returns records from table cm_ldsity2panel_refyearset_version.';

grant execute on function target_data.fn_import_get_ldsity2panel_refyearset_versions(integer, integer, integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_spc_hierarchies" schema="target_data" src="functions/import/fn_import_get_spc_hierarchies.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_spc_hierarchies
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_spc_hierarchies(integer, integer, integer, boolean, integer[]) CASCADE;

create or replace function target_data.fn_import_get_spc_hierarchies
(
	_id								integer,
	_variable_superior__etl_id		integer,
	_variable__etl_id				integer,
	_dependent						boolean,
	_etl_id							integer[] default null::integer[]
)
returns table
(
	id					integer,
	etl_id				integer,
	variable_superior	integer,
	variable			integer,
	dependent			boolean
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_spc_hierarchies: Input argument _id must not be NULL!';
		end if;
	
		if _variable_superior__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_spc_hierarchies: Input argument _variable_superior__etl_id must not be NULL!';
		end if;
	
		if _variable__etl_id is null
		then
			raise exception 'Error 03: fn_import_get_spc_hierarchies: Input argument _variable__etl_id must not be NULL!';
		end if;
	
		if _dependent is null
		then
			raise exception 'Error 04: fn_import_get_spc_hierarchies: Input argument _dependent must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 05: fn_import_get_spc_hierarchies: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;
	
		return query
		select
				_id as id,
				tsh.id as etl_id,
				tsh.variable_superior,
				tsh.variable,
				tsh.dependent
		from
				target_data.t_spc_hierarchy as tsh
		where
				tsh.id not in (select unnest(_etl_id))
		and		tsh.variable_superior = _variable_superior__etl_id
		and		tsh.variable = _variable__etl_id
		and		tsh.dependent = _dependent;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_spc_hierarchies(integer, integer, integer, boolean, integer[]) is
'Function returns records from table t_spc_hierarchy.';

grant execute on function target_data.fn_import_get_spc_hierarchies(integer, integer, integer, boolean, integer[]) to public;
-- </function>



-- <function name="fn_import_get_spc2classification_rules" schema="target_data" src="functions/import/fn_import_get_spc2classification_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_spc2classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_spc2classification_rules(integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_spc2classification_rules
(
	_spc2classification_rule			integer,
	_sub_population_category__etl_id	integer,
	_ldsity_object__etl_id				integer,
	_etl_id								integer[] default null::integer[]
)
returns table
(
	spc2classification_rule					integer,
	ldsity_object__id						integer,
	ldsity_object__label					varchar,
	ldsity_object__description				text,
	ldsity_object__label_en					varchar,
	ldsity_object__description_en			text,
	sub_population_category__id				integer,
	sub_population_category__label			varchar,
	sub_population_category__description	text,
	sub_population_category__label_en		varchar,
	sub_population_category__description_en	text,
	etl_id									integer,
	classification_rule						text
)
as
$$
declare
begin
		if _spc2classification_rule is null
		then
			raise exception 'Error 01: fn_import_get_spc2classification_rules: Input argument _spc2classification_rule must not be NULL!';
		end if;
	
		if _sub_population_category__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_spc2classification_rules: Input argument _sub_population_category__etl_id must not be NULL!';
		end if;
	
		if _ldsity_object__etl_id is null
		then
			raise exception 'Error 03: fn_import_get_spc2classification_rules: Input argument _ldsity_object__etl_id must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 04: fn_import_get_spc2classification_rules: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;	
	
		return query
		with
		w1 as	(
				select
						_spc2classification_rule as spc2classification_rule,
						cscr.id as etl_id,
						cscr.sub_population_category as sub_population_category__id,
						cscr.ldsity_object as ldsity_object__id,
						cscr.classification_rule
				from
						target_data.cm_spc2classification_rule as cscr
				where
						cscr.sub_population_category = _sub_population_category__etl_id
				and
						cscr.ldsity_object = _ldsity_object__etl_id
				and
						cscr.id not in (select unnest(_etl_id))
				)
		select
				w1.spc2classification_rule,
				w1.ldsity_object__id,
				clo.label as ldsity_object__label,
				clo.description as ldsity_object__description,
				clo.label_en as ldsity_object__label_en,
				clo.description_en as ldsity_object__description_en,
				w1.sub_population_category__id,
				cspc.label as sub_population_category__label,
				cspc.description as sub_population_category__description,
				cspc.label_en as sub_population_category__label_en,
				cspc.description_en as sub_population_category__description_en,
				w1.etl_id,
				w1.classification_rule
		from
				w1
				inner join target_data.c_sub_population_category as cspc on w1.sub_population_category__id = cspc.id
				inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
		order 
				by w1.ldsity_object__id, w1.sub_population_category__id, w1.spc2classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_spc2classification_rules(integer, integer, integer, integer[]) is
'Function returns records from table cm_spc2classification_rule.';

grant execute on function target_data.fn_import_get_spc2classification_rules(integer, integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_spc2classrule2panel_refyearsets" schema="target_data" src="functions/import/fn_import_get_spc2classrule2panel_refyearsets.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_spc2classrule2panel_refyearsets
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_spc2classrule2panel_refyearsets(integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_spc2classrule2panel_refyearsets
(
	_id										integer,
	_spc2classification_rule__etl_id		integer,
	_refyearset2panel						integer,
	_etl_id									integer[] default null::integer[]
)
returns table
(
	id							integer,
	etl_id						integer,
	spc2classification_rule		integer,
	refyearset2panel			integer
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_spc2classrule2panel_refyearsets: Input argument _id must not be NULL!';
		end if;
	
		if _spc2classification_rule__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_spc2classrule2panel_refyearsets: Input argument _spc2classification_rule__etl_id must not be NULL!';
		end if;
	
		if _refyearset2panel is null
		then
			raise exception 'Error 03: fn_import_get_spc2classrule2panel_refyearsets: Input argument _refyearset2panel must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 04: fn_import_get_spc2classrule2panel_refyearsets: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;	
	
		return query
		select
				_id as id,
				cacpr.id as etl_id,
				cacpr.spc2classification_rule,
				cacpr.refyearset2panel
		from
				target_data.cm_spc2classrule2panel_refyearset as cacpr
		where
				cacpr.id not in (select unnest(_etl_id))
		and		cacpr.spc2classification_rule = _spc2classification_rule__etl_id
		and		cacpr.refyearset2panel = _refyearset2panel;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_spc2classrule2panel_refyearsets(integer, integer, integer, integer[]) is
'Function returns records from table cm_spc2classrule2panel_refyearset.';

grant execute on function target_data.fn_import_get_spc2classrule2panel_refyearsets(integer, integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_sub_population_categories" schema="target_data" src="functions/import/fn_import_get_sub_population_categories.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_sub_population_categories
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_sub_population_categories(integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_sub_population_categories
(
	_sub_population_category	integer,
	_sub_population__etl_id		integer,
	_etl_id						integer[] default null::integer[]
)
returns table
(
	sub_population_category	integer,
	etl_id					integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text
)
as
$$
declare
begin
		if _sub_population_category is null
		then
			raise exception 'Error 01: fn_import_get_sub_population_categories: Input argument _sub_population_category must not be NULL!';
		end if;
	
		if _sub_population__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_sub_population_categories: Input argument _sub_population__etl_id must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 03: fn_import_get_sub_population_categories: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;	
	
		return query
		select
				_sub_population_category as sub_population_category,
				cadc.id as etl_id,
				cadc.label,
				cadc.description,
				cadc.label_en,
				cadc.description_en
		from 
				target_data.c_sub_population_category as cadc
		where
				cadc.sub_population = _sub_population__etl_id
		and
				cadc.id not in (select unnest(_etl_id))
		order
				by cadc.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_sub_population_categories(integer, integer, integer[]) is
'Function returns records from table c_sub_population_category.';

grant execute on function target_data.fn_import_get_sub_population_categories(integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_sub_populations" schema="target_data" src="functions/import/fn_import_get_sub_populations.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_sub_populations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_sub_populations(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_sub_populations
(
	_sub_population		integer,
	_etl_id				integer[] default null::integer[]
)
returns table
(
	sub_population		integer,
	etl_id				integer,
	label				varchar,
	description			text,
	label_en			varchar,
	description_en		text
)
as
$$
declare
begin
		if _sub_population is null
		then
			raise exception 'Error 01: fn_import_get_sub_populations: Input argument _sub_population must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 02: fn_import_get_sub_populations: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;	
	
		return query
		select
				_sub_population as sub_population,
				cad.id as etl_id,
				cad.label,
				cad.description,
				cad.label_en,
				cad.description_en
		from 
				target_data.c_sub_population as cad
		where
				cad.id not in (select unnest(_etl_id))
		order
				by cad.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_sub_populations(integer, integer[]) is
'Function returns records from table c_sub_population.';

grant execute on function target_data.fn_import_get_sub_populations(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_unit_of_measures" schema="target_data" src="functions/import/fn_import_get_unit_of_measures.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_unit_of_measures
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_unit_of_measures(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_unit_of_measures
(
	_unit_of_measure	integer,
	_etl_id				integer[] default null::integer[]
)
returns table
(
	unit_of_measure			integer,
	etl_id					integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text
)
as
$$
declare
begin
		if _unit_of_measure is null
		then
			raise exception 'Error 01: fn_import_get_unit_of_measures: Input argument _unit_of_measure must not be NULL!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 02: fn_import_get_unit_of_measures: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;	
	
		return query
		select
				_unit_of_measure as unit_of_measure,
				cuom.id as etl_id,
				cuom.label,
				cuom.description,
				cuom.label_en,
				cuom.description_en
		from 
				target_data.c_unit_of_measure as cuom
		where
				cuom.id not in (select unnest(_etl_id))
		order
				by cuom.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_unit_of_measures(integer, integer[]) is
'Function returns records from table c_unit_of_measure.';

grant execute on function target_data.fn_import_get_unit_of_measures(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_get_versions" schema="target_data" src="functions/import/fn_import_get_versions.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_versions
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_versions(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_versions
(
	_id			integer,
	_etl_id		integer[] default null::integer[]
)
returns table
(
	id				integer,
	etl_id			integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_versions: Input argument _id must not be null!';
		end if;

		if _etl_id is not null
		then
			if	(
					select count(t.*) > 0 from
					(select unnest(_etl_id) as etl_id) as t
					where t.etl_id is null
				)
			then
				raise exception 'Error 02: fn_import_get_versions: Input argument _etl_id is not null and contains NULL value!';
			end if;
		end if;	

		if _etl_id is null
		then
			return query
			select
					_id as id,
					cv.id as etl_id,
					cv.label,
					cv.description,
					cv.label_en,
					cv.description_en
			from
					target_data.c_version as cv
			order
					by cv.id;
		else
			return query
			select
					_id as id,
					cv.id as etl_id,
					cv.label,
					cv.description,
					cv.label_en,
					cv.description_en
			from
					target_data.c_version as cv
			where
					cv.id not in (select unnest(_etl_id))
			order
					by cv.id;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_versions(integer, integer[]) is
'Function returns records from table c_version for given input arguments.';

grant execute on function target_data.fn_import_get_versions(integer, integer[]) to public;
-- </function>

