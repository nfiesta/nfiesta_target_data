--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



---------------------------------------------------------------------------------------------------
-- t_etl_target_variable
---------------------------------------------------------------------------------------------------   
alter table target_data.t_etl_target_variable add column etl_user name;
comment on column target_data.t_etl_target_variable.etl_user IS 'Identifier of ETL current user for given target variable.';

alter table target_data.t_etl_target_variable add column etl_time timestamptz;
comment on column target_data.t_etl_target_variable.etl_time IS 'Identifier of ETL time of given target variable.';
    
alter table target_data.t_etl_target_variable alter column etl_user set default current_user collate "C";
alter table target_data.t_etl_target_variable alter column etl_time set default now();
---------------------------------------------------------------------------------------------------		



---------------------------------------------------------------------------------------------------
-- t_etl_area_domain
---------------------------------------------------------------------------------------------------
alter table target_data.t_etl_area_domain add column etl_user name;
comment on column target_data.t_etl_area_domain.etl_user IS 'Identifier of ETL current user for given area domain.';

alter table target_data.t_etl_area_domain add column etl_time timestamptz;
comment on column target_data.t_etl_area_domain.etl_time IS 'Identifier of ETL time of given area domain.';
    
alter table target_data.t_etl_area_domain alter column etl_user set default current_user collate "C";
alter table target_data.t_etl_area_domain alter column etl_time set default now();
---------------------------------------------------------------------------------------------------



---------------------------------------------------------------------------------------------------
-- t_etl_sub_population
---------------------------------------------------------------------------------------------------
alter table target_data.t_etl_sub_population add column etl_user name;
comment on column target_data.t_etl_sub_population.etl_user IS 'Identifier of ETL current user for given sub population.';

alter table target_data.t_etl_sub_population add column etl_time timestamptz;
comment on column target_data.t_etl_sub_population.etl_time IS 'Identifier of ETL time of given sub population.';
    
alter table target_data.t_etl_sub_population alter column etl_user set default current_user collate "C";
alter table target_data.t_etl_sub_population alter column etl_time set default now();
---------------------------------------------------------------------------------------------------
		


--------------------------------------------------------------------------------------------------
-- t_etl_log
---------------------------------------------------------------------------------------------------
alter table target_data.t_etl_log add column etl_user name;
comment on column target_data.t_etl_log.etl_user IS 'Identifier of ETL current user for given log.';

alter table target_data.t_etl_log alter column etl_user set default current_user collate "C";
---------------------------------------------------------------------------------------------------