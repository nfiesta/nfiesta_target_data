--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_etl_get_area_domains4update" schema="target_data" src="functions/etl/fn_etl_get_area_domains4update.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domains4update
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domains4update(integer) CASCADE;

create or replace function target_data.fn_etl_get_area_domains4update
(
	_export_connection	integer
)
returns json
as
$$
declare
		_res	json;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_area_domains4update: Input argument _export_connection must not be null!';
		end if;
		-------------------------------------------------------------
		with
		w1 as	(
				select
						tead.id,
						tead.export_connection,
						tead.area_domain,
						tead.etl_id,
						array_length(tead.area_domain,1) as count_elements
				from
						target_data.t_etl_area_domain as tead where tead.export_connection = _export_connection
				)
		,w2 as	(
				select
						w1.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,w1.count_elements) as res ) as t) as id_order
				from
						w1
				)
		,w3 as	(
				select
						w2.id,
						unnest(w2.area_domain) as area_domain,
						unnest(w2.id_order) as id_order
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						cad.label,
						cad.description,
						cad.label_en,
						cad.description_en
				from
						w3 inner join target_data.c_area_domain as cad on w3.area_domain = cad.id
				)
		,w5 as	(
				select
						w4.id,
						array_agg(w4.area_domain order by w4.id_order) as area_domain,
						array_agg(w4.id_order order by w4.id_order) as id_order,
						array_agg(w4.label order by w4.id_order) as label,
						array_agg(w4.description order by w4.id_order) as description,
						array_agg(w4.label_en order by w4.id_order) as label_en,
						array_agg(w4.description_en order by w4.id_order) as description_en
				from
						w4 group by w4.id
				)
		,w6 as	(
				select
						w1.id,
						w1.area_domain,
						w1.etl_id,
						array_to_string(w5.label,';') as label,
						array_to_string(w5.description,';') as description,
						array_to_string(w5.label_en,';') as label_en,
						array_to_string(w5.description_en,';') as description_en
				from
						w1 inner join w5 on w1.id = w5.id
				)
		,w7 as	(
				select
						json_build_object
							(
							'area_domain',w6.etl_id,
							'label',w6.label,
							'description',w6.description,
							'label_en',w6.label_en,
							'description_en',w6.description_en
							) as res
				from w6
				)
		select json_agg(w7.res) from w7
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domains4update(integer) IS
'The funcions returs list of area domains that had already been ETLed for given export connection.';

grant execute on function target_data.fn_etl_get_area_domains4update(integer) to public;
-- </function>



-- <function name="fn_etl_get_sub_populations4update" schema="target_data" src="functions/etl/fn_etl_get_sub_populations4update.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_populations4update
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_sub_populations4update(integer) CASCADE;

create or replace function target_data.fn_etl_get_sub_populations4update
(
	_export_connection	integer
)
returns json
as
$$
declare
		_res	json;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sub_populations4update: Input argument _export_connection must not be null!';
		end if;
		-------------------------------------------------------------
		with
		w1 as	(
				select
						tesp.id,
						tesp.export_connection,
						tesp.sub_population,
						tesp.etl_id,
						array_length(tesp.sub_population,1) as count_elements
				from
						target_data.t_etl_sub_population as tesp where tesp.export_connection = _export_connection
				)
		,w2 as	(
				select
						w1.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,w1.count_elements) as res ) as t) as id_order
				from
						w1
				)
		,w3 as	(
				select
						w2.id,
						unnest(w2.sub_population) as sub_population,
						unnest(w2.id_order) as id_order
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						csp.label,
						csp.description,
						csp.label_en,
						csp.description_en
				from
						w3 inner join target_data.c_sub_population as csp on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.id,
						array_agg(w4.sub_population order by w4.id_order) as sub_population,
						array_agg(w4.id_order order by w4.id_order) as id_order,
						array_agg(w4.label order by w4.id_order) as label,
						array_agg(w4.description order by w4.id_order) as description,
						array_agg(w4.label_en order by w4.id_order) as label_en,
						array_agg(w4.description_en order by w4.id_order) as description_en
				from
						w4 group by w4.id
				)
		,w6 as	(
				select
						w1.id,
						w1.sub_population,
						w1.etl_id,
						array_to_string(w5.label,';') as label,
						array_to_string(w5.description,';') as description,
						array_to_string(w5.label_en,';') as label_en,
						array_to_string(w5.description_en,';') as description_en
				from
						w1 inner join w5 on w1.id = w5.id
				)
		,w7 as	(
				select
						json_build_object
							(
							'sub_population',w6.etl_id,
							'label',w6.label,
							'description',w6.description,
							'label_en',w6.label_en,
							'description_en',w6.description_en
							) as res
				from w6
				)
		select json_agg(w7.res) from w7
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_sub_populations4update(integer) IS
'The funcions returs list of sub populations that had already been ETLed for given export connection.';

grant execute on function target_data.fn_etl_get_sub_populations4update(integer) to public;
-- </function>



-- <function name="fn_etl_get_list_of_area_domains4update" schema="target_data" src="functions/etl/fn_etl_get_list_of_area_domains4update.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_list_of_area_domains4update
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_list_of_area_domains4update(integer, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_list_of_area_domains4update
(
	_export_connection	integer,
	_area_domain_target	integer[]
)
returns table
(
	etl_area_domain		integer,
	area_domain_target	integer,
	label				varchar,
	description			text,
	label_en			varchar,
	description_en		text
)
as
$$
declare
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_list_of_area_domains4update: Input argument _export_connection must not be null!';
		end if;

		if _area_domain_target is null
		then
			raise exception 'Error 02: fn_etl_get_list_of_area_domains4update: Input argument _area_domain_target must not be null!';
		end if;		
		-------------------------------------------------------------
		return query
		with
		w1 as	(
				select
						tead.id,
						tead.export_connection,
						tead.area_domain,
						tead.etl_id,
						array_length(tead.area_domain,1) as count_elements
				from
						target_data.t_etl_area_domain as tead
				where
						tead.export_connection = _export_connection
				and
						tead.etl_id in (select unnest(_area_domain_target))
				)
		,w2 as	(
				select
						w1.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,w1.count_elements) as res ) as t) as id_order
				from
						w1
				)
		,w3 as	(
				select
						w2.id,
						unnest(w2.area_domain) as area_domain,
						unnest(w2.id_order) as id_order
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						cad.label,
						cad.description,
						cad.label_en,
						cad.description_en
				from
						w3 inner join target_data.c_area_domain as cad on w3.area_domain = cad.id
				)
		,w5 as	(
				select
						w4.id,
						array_agg(w4.area_domain order by w4.id_order) as area_domain,
						array_agg(w4.id_order order by w4.id_order) as id_order,
						array_agg(w4.label order by w4.id_order) as label,
						array_agg(w4.description order by w4.id_order) as description,
						array_agg(w4.label_en order by w4.id_order) as label_en,
						array_agg(w4.description_en order by w4.id_order) as description_en
				from
						w4 group by w4.id
				)
		,w6 as	(
				select
						w1.id,
						w1.area_domain,
						w1.etl_id,
						array_to_string(w5.label,';') as label,
						array_to_string(w5.description,';') as description,
						array_to_string(w5.label_en,';') as label_en,
						array_to_string(w5.description_en,';') as description_en
				from
						w1 inner join w5 on w1.id = w5.id
				)
		select
				w6.id as etl_area_domain,
				w6.etl_id as area_domain_target,
				w6.label::varchar,
				w6.description,
				w6.label_en::varchar,
				w6.description_en
		from
				w6;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_list_of_area_domains4update(integer, integer[]) IS
'The funcions returs list of area domains that had already been ETLed and some of their label or description is difference between source and target DB for given export connection.';

grant execute on function target_data.fn_etl_get_list_of_area_domains4update(integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_list_of_sub_populations4update" schema="target_data" src="functions/etl/fn_etl_get_list_of_sub_populations4update.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_list_of_sub_populations4update
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_list_of_sub_populations4update(integer, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_list_of_sub_populations4update
(
	_export_connection		integer,
	_sub_population_target	integer[]
)
returns table
(
	etl_sub_population		integer,
	sub_population_target	integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text
)
as
$$
declare
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_list_of_sub_populations4update: Input argument _export_connection must not be null!';
		end if;

		if _export_connection is null
		then
			raise exception 'Error 02: fn_etl_get_list_of_sub_populations4update: Input argument _sub_population_target must not be null!';
		end if;
		-------------------------------------------------------------
		return query
		with
		w1 as	(
				select
						tesp.id,
						tesp.export_connection,
						tesp.sub_population,
						tesp.etl_id,
						array_length(tesp.sub_population,1) as count_elements
				from
						target_data.t_etl_sub_population as tesp
				where
						tesp.export_connection = _export_connection
				and
						tesp.etl_id in (select unnest(_sub_population_target))
				)
		,w2 as	(
				select
						w1.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,w1.count_elements) as res ) as t) as id_order
				from
						w1
				)
		,w3 as	(
				select
						w2.id,
						unnest(w2.sub_population) as sub_population,
						unnest(w2.id_order) as id_order
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						csp.label,
						csp.description,
						csp.label_en,
						csp.description_en
				from
						w3 inner join target_data.c_sub_population as csp on w3.sub_population = csp.id
				)
		,w5 as	(
				select
						w4.id,
						array_agg(w4.sub_population order by w4.id_order) as sub_population,
						array_agg(w4.id_order order by w4.id_order) as id_order,
						array_agg(w4.label order by w4.id_order) as label,
						array_agg(w4.description order by w4.id_order) as description,
						array_agg(w4.label_en order by w4.id_order) as label_en,
						array_agg(w4.description_en order by w4.id_order) as description_en
				from
						w4 group by w4.id
				)
		,w6 as	(
				select
						w1.id,
						w1.sub_population,
						w1.etl_id,
						array_to_string(w5.label,';') as label,
						array_to_string(w5.description,';') as description,
						array_to_string(w5.label_en,';') as label_en,
						array_to_string(w5.description_en,';') as description_en
				from
						w1 inner join w5 on w1.id = w5.id
				)
		select
				w6.id as etl_sub_population,
				w6.etl_id as sub_population_target,
				w6.label::varchar,
				w6.description,
				w6.label_en::varchar,
				w6.description_en
		from
				w6;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_list_of_sub_populations4update(integer, integer[]) IS
'The funcions returs list of sub populations that had already been ETLed and some of their label or description is difference between source and target DB for given export connection.';

grant execute on function target_data.fn_etl_get_list_of_sub_populations4update(integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_area_domain_categories4update" schema="target_data" src="functions/etl/fn_etl_get_area_domain_categories4update.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_categories4update
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domain_categories4update(integer,integer) CASCADE;

create or replace function target_data.fn_etl_get_area_domain_categories4update
(
	_etl_area_domain	integer,
	_area_domain_target	integer
)
returns json
as
$$
declare
		_res	json;
begin
		if _etl_area_domain is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_categories4update: Input argument _etl_area_domain must not be null!';
		end if;

		if _area_domain_target is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_categories4update: Input argument _area_domain_target must not be null!';
		end if;
		-------------------------------------------------------------
		with
		w1 as	(
				select
						teadc.id,
						teadc.area_domain_category,
						teadc.etl_id,
						teadc.etl_area_domain,
						array_length(teadc.area_domain_category,1) as count_elements
				from
						target_data.t_etl_area_domain_category as teadc where teadc.etl_area_domain = _etl_area_domain
				)
		,w2 as	(
				select
						w1.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,w1.count_elements) as res ) as t) as id_order
				from
						w1
				)
		,w3 as	(
				select
						w2.id,
						unnest(w2.area_domain_category) as area_domain_category,
						unnest(w2.id_order) as id_order
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						cadc.label,
						cadc.description,
						cadc.label_en,
						cadc.description_en
				from
						w3 inner join target_data.c_area_domain_category as cadc on w3.area_domain_category = cadc.id
				)		
		,w5 as	(
				select
						w4.id,
						array_agg(w4.area_domain_category order by w4.id_order) as area_domain_category,
						array_agg(w4.id_order order by w4.id_order) as id_order,
						array_agg(w4.label order by w4.id_order) as label,
						array_agg(w4.description order by w4.id_order) as description,
						array_agg(w4.label_en order by w4.id_order) as label_en,
						array_agg(w4.description_en order by w4.id_order) as description_en
				from
						w4 group by w4.id
				)
		,w6 as	(
				select
						w1.id,
						w1.area_domain_category,
						w1.etl_id,
						array_to_string(w5.label,';') as label,
						array_to_string(w5.description,';') as description,
						array_to_string(w5.label_en,';') as label_en,
						array_to_string(w5.description_en,';') as description_en
				from
						w1 inner join w5 on w1.id = w5.id
				)
		,w7 as	(
				select
						json_build_object
							(
							'area_domain',_area_domain_target,
							'area_domain_category',w6.etl_id,
							'label',w6.label,
							'description',w6.description,
							'label_en',w6.label_en,
							'description_en',w6.description_en
							) as res
				from w6
				)
		select json_agg(w7.res) from w7
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domain_categories4update(integer, integer) IS
'The funcions returs list of area domain categories that had already been ETLed for given area domain.';

grant execute on function target_data.fn_etl_get_area_domain_categories4update(integer, integer) to public;
-- </function>



-- <function name="fn_etl_get_sub_population_categories4update" schema="target_data" src="functions/etl/fn_etl_get_sub_population_categories4update.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_categories4update
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_sub_population_categories4update(integer,integer) CASCADE;

create or replace function target_data.fn_etl_get_sub_population_categories4update
(
	_etl_sub_population		integer,
	_sub_population_target	integer
)
returns json
as
$$
declare
		_res	json;
begin
		if _etl_sub_population is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_categories4update: Input argument _etl_sub_population must not be null!';
		end if;

		if _sub_population_target is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_categories4update: Input argument _sub_population_target must not be null!';
		end if;
		-------------------------------------------------------------
		with
		w1 as	(
				select
						tespc.id,
						tespc.sub_population_category,
						tespc.etl_id,
						tespc.etl_sub_population,
						array_length(tespc.sub_population_category,1) as count_elements
				from
						target_data.t_etl_sub_population_category as tespc where tespc.etl_sub_population = _etl_sub_population
				)
		,w2 as	(
				select
						w1.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,w1.count_elements) as res ) as t) as id_order
				from
						w1
				)
		,w3 as	(
				select
						w2.id,
						unnest(w2.sub_population_category) as sub_population_category,
						unnest(w2.id_order) as id_order
				from
						w2
				)
		,w4 as	(
				select
						w3.*,
						cspc.label,
						cspc.description,
						cspc.label_en,
						cspc.description_en
				from
						w3 inner join target_data.c_sub_population_category as cspc on w3.sub_population_category = cspc.id
				)		
		,w5 as	(
				select
						w4.id,
						array_agg(w4.sub_population_category order by w4.id_order) as sub_population_category,
						array_agg(w4.id_order order by w4.id_order) as id_order,
						array_agg(w4.label order by w4.id_order) as label,
						array_agg(w4.description order by w4.id_order) as description,
						array_agg(w4.label_en order by w4.id_order) as label_en,
						array_agg(w4.description_en order by w4.id_order) as description_en
				from
						w4 group by w4.id
				)
		,w6 as	(
				select
						w1.id,
						w1.sub_population_category,
						w1.etl_id,
						array_to_string(w5.label,';') as label,
						array_to_string(w5.description,';') as description,
						array_to_string(w5.label_en,';') as label_en,
						array_to_string(w5.description_en,';') as description_en
				from
						w1 inner join w5 on w1.id = w5.id
				)
		,w7 as	(
				select
						json_build_object
							(
							'sub_population',_sub_population_target,
							'sub_population_category',w6.etl_id,
							'label',w6.label,
							'description',w6.description,
							'label_en',w6.label_en,
							'description_en',w6.description_en
							) as res
				from w6
				)
		select json_agg(w7.res) from w7
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_sub_population_categories4update(integer, integer) IS
'The funcions returs list of sub population categories that had already been ETLed for given sub population.';

grant execute on function target_data.fn_etl_get_sub_population_categories4update(integer, integer) to public;
-- </function>



-- <function name="fn_etl_get_area_domain_categories4area_domains" schema="target_data" src="functions/etl/fn_etl_get_area_domain_categories4area_domains.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_categories4area_domains
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domain_categories4area_domains(integer) CASCADE;

create or replace function target_data.fn_etl_get_area_domain_categories4area_domains
(
	_export_connection	integer
)
returns json
as
$$
declare
		_res	json;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_categories4area_domains: Input argument _export_connection must not be null!';
		end if;
		-------------------------------------------------------------
		with
		w1 as	(
				select
						tead.id as etl_area_domain__id,
						tead.etl_id as etl_area_domain__etl_id
				from
						target_data.t_etl_area_domain as tead where export_connection = _export_connection
				)
		,w2 as	(
				select
						w1.*,
						teadc.id as etl_area_domain_category__id,
						teadc.area_domain_category as etl_area_domain_category__adc,
						teadc.etl_id as etl_area_domain_category__etl_id,
						array_length(teadc.area_domain_category,1) as count_elements
				from
						w1
						inner join target_data.t_etl_area_domain_category as teadc
						on w1.etl_area_domain__id = teadc.etl_area_domain
				)
		,w3 as	(
				select
						w2.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,w2.count_elements) as res ) as t) as id_order
				from
						w2
				)			
		,w4 as	(
				select
						w3.etl_area_domain__etl_id,
						w3.etl_area_domain_category__etl_id,
						unnest(w3.etl_area_domain_category__adc) as etl_area_domain_category__adc,
						unnest(w3.id_order) as id_order
				from
						w3
				)				
		,w5 as	(
				select
						w4.*,
						cadc.label,
						cadc.description,
						cadc.label_en,
						cadc.description_en
				from
						w4 inner join (select * from target_data.c_area_domain_category where id in (select w4.etl_area_domain_category__adc from w4)) as cadc
						on w4.etl_area_domain_category__adc = cadc.id
				)			
		,w6 as	(
				select
						w5.etl_area_domain__etl_id,
						w5.etl_area_domain_category__etl_id,
						array_agg(w5.label order by w5.id_order) as label,
						array_agg(w5.description order by w5.id_order) as description,
						array_agg(w5.label_en order by w5.id_order) as label_en,
						array_agg(w5.description_en order by w5.id_order) as description_en
				from
						w5 group by w5.etl_area_domain__etl_id, w5.etl_area_domain_category__etl_id
				)			
		,w7 as	(
				select
						w6.etl_area_domain__etl_id,
						w6.etl_area_domain_category__etl_id,
						array_to_string(w6.label,';') as label,
						array_to_string(w6.description,';') as description,
						array_to_string(w6.label_en,';') as label_en,
						array_to_string(w6.description_en,';') as description_en
				from
						w6
				)
		,w8 as	(
				select
						json_build_object
							(
							'area_domain',w7.etl_area_domain__etl_id,
							'area_domain_category',w7.etl_area_domain_category__etl_id,
							'label',w7.label,
							'description',w7.description,
							'label_en',w7.label_en,
							'description_en',w7.description_en
							) as res
				from w7
				)
		select json_agg(w8.res) from w8
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domain_categories4area_domains(integer) IS
'The funcions returs JSON of area domain categories that had already been ETLed for given export connection.';

grant execute on function target_data.fn_etl_get_area_domain_categories4area_domains(integer) to public;
-- </function>



-- <function name="fn_etl_get_sub_population_categories4sub_populations" schema="target_data" src="functions/etl/fn_etl_get_sub_population_categories4sub_populations.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_categories4sub_populations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_sub_population_categories4sub_populations(integer) CASCADE;

create or replace function target_data.fn_etl_get_sub_population_categories4sub_populations
(
	_export_connection	integer
)
returns json
as
$$
declare
		_res	json;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_categories4sub_populations: Input argument _export_connection must not be null!';
		end if;
		-------------------------------------------------------------
		with
		w1 as	(
				select
						tesp.id as etl_sub_population__id,
						tesp.etl_id as etl_sub_population__etl_id
				from
						target_data.t_etl_sub_population as tesp where export_connection = _export_connection
				)
		,w2 as	(
				select
						w1.*,
						tespc.id as etl_sub_population_category__id,
						tespc.sub_population_category as etl_sub_population_category__spc,
						tespc.etl_id as etl_sub_population_category__etl_id,
						array_length(tespc.sub_population_category,1) as count_elements
				from
						w1
						inner join target_data.t_etl_sub_population_category as tespc
						on w1.etl_sub_population__id = tespc.etl_sub_population
				)
		,w3 as	(
				select
						w2.*,
						(select array_agg(t.res order by t.res) from (select generate_series(1,w2.count_elements) as res ) as t) as id_order
				from
						w2
				)			
		,w4 as	(
				select
						w3.etl_sub_population__etl_id,
						w3.etl_sub_population_category__etl_id,
						unnest(w3.etl_sub_population_category__spc) as etl_sub_population_category__spc,
						unnest(w3.id_order) as id_order
				from
						w3
				)				
		,w5 as	(
				select
						w4.*,
						cspc.label,
						cspc.description,
						cspc.label_en,
						cspc.description_en
				from
						w4 inner join (select * from target_data.c_sub_population_category where id in (select w4.etl_sub_population_category__spc from w4)) as cspc
						on w4.etl_sub_population_category__spc = cspc.id
				)			
		,w6 as	(
				select
						w5.etl_sub_population__etl_id,
						w5.etl_sub_population_category__etl_id,
						array_agg(w5.label order by w5.id_order) as label,
						array_agg(w5.description order by w5.id_order) as description,
						array_agg(w5.label_en order by w5.id_order) as label_en,
						array_agg(w5.description_en order by w5.id_order) as description_en
				from
						w5 group by w5.etl_sub_population__etl_id, w5.etl_sub_population_category__etl_id
				)			
		,w7 as	(
				select
						w6.etl_sub_population__etl_id,
						w6.etl_sub_population_category__etl_id,
						array_to_string(w6.label,';') as label,
						array_to_string(w6.description,';') as description,
						array_to_string(w6.label_en,';') as label_en,
						array_to_string(w6.description_en,';') as description_en
				from
						w6
				)
		,w8 as	(
				select
						json_build_object
							(
							'sub_population',w7.etl_sub_population__etl_id,
							'sub_population_category',w7.etl_sub_population_category__etl_id,
							'label',w7.label,
							'description',w7.description,
							'label_en',w7.label_en,
							'description_en',w7.description_en
							) as res
				from w7
				)
		select json_agg(w8.res) from w8
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_sub_population_categories4sub_populations(integer) IS
'The funcions returs JSON of sub population categories that had already been ETLed for given export connection.';

grant execute on function target_data.fn_etl_get_sub_population_categories4sub_populations(integer) to public;
-- </function>

