--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



DROP FUNCTION IF EXISTS target_data.fn_etl_try_delete_export_connection(integer) CASCADE;
DROP FUNCTION target_data.fn_etl_delete_export_connection(integer) CASCADE;



-- <function name="fn_etl_try_delete_export_connection" schema="target_data" src="functions/etl/fn_etl_try_delete_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_try_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_try_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_etl_try_delete_export_connection(_id integer)
RETURNS integer
AS
$$
DECLARE
	_check_etl_data	integer;
	_check_user		integer;
BEGIN
	if _id is null
	then
		RAISE EXCEPTION 'Error 01: fn_etl_try_delete_export_connection: Input argument _id must not be NULL!';
	end if;

	IF NOT EXISTS (SELECT t.* FROM target_data.t_export_connection as t where t.id = _id)
	THEN RAISE EXCEPTION 'Error 02: fn_etl_try_delete_export_connection: Given export connection (_id = %) does not exist in t_export_connection table!', _id;
	END IF;

	with
	w1 as	(
			select tetv.id from target_data.t_etl_target_variable as tetv where tetv.export_connection = _id	union all
			select tead.id from target_data.t_etl_area_domain as tead where tead.export_connection = _id		union all
			select tesp.id from target_data.t_etl_sub_population as tesp where tesp.export_connection = _id
			)
	select count(w1.*) from w1
	into _check_etl_data;

	with
	w1 as	(
			select r.rolname as username,r1.rolname as "role"
			from pg_catalog.pg_roles r join pg_catalog.pg_auth_members m
			on (m.member = r.oid)
			join pg_roles r1 on (m.roleid=r1.oid)                                  
			where r.rolcanlogin and r.rolname = current_user
			order by 1
			)
	select count(*) from w1 where w1.role = 'app_nfiesta_mng'
	into _check_user;

	if _check_etl_data > 0 and _check_user = 0
	then
		raise exception 'Error 03: fn_etl_try_delete_export_connection: NOT ALLOWED delete given export connection. Given export connection (_id = %) contains ETL data and current user is not member in role "app_nfiesta_mng".',_id;
	end if;

	return _check_etl_data;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_etl_try_delete_export_connection(integer) IS
'The function provides test if it is possible to delete record from t_export_connection table and delete ETL datas.';

GRANT EXECUTE ON FUNCTION target_data.fn_etl_try_delete_export_connection(integer) TO public;
-- </function>



-- <function name="fn_etl_delete_etl_data4export_connection" schema="target_data" src="functions/etl/fn_etl_delete_etl_data4export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_delete_export_connefn_etl_delete_etl_data4export_connectionction
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_etl_delete_etl_data4export_connection(integer) CASCADE;

create or replace function target_data.fn_etl_delete_etl_data4export_connection
(
	_export_connection integer
)
returns text
as
$$
declare
	_res text
begin
	-------------------------------------------------------
	if _export_connection is null
	then 
		raise exception 'Error: 01: fn_etl_delete_etl_data4export_connection: Input argument _id must not be NULL!';
	end if;
	-------------------------------------------------------
	if not exists (select t1.* from target_data.t_export_connection as t1 where t1.id = _export_connection)
	then
		raise exception 'Error: 02: fn_etl_delete_etl_data4export_connection: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
	end if;
	-------------------------------------------------------
	delete from target_data.t_etl_area_domain_category where etl_area_domain in (select id from target_data.t_etl_area_domain where export_connection = _export_connection);
	delete from target_data.t_etl_area_domain where export_connection = _export_connection;
	delete from target_data.t_etl_sub_population_category where etl_sub_population in (select id from target_data.t_etl_sub_population where export_connection = _export_connection);
	delete from target_data.t_etl_sub_population where export_connection = _export_connection;
	delete from target_data.t_etl_log where etl_target_variable	in (select id from target_data.t_etl_target_variable where export_connection = _export_connection);
	delete from target_data.t_etl_target_variable where export_connection = _export_connection;	
	delete from target_data.t_export_connection where id = _export_connection;
	-------------------------------------------------------
	_res := concat('The ETL datas for export connection ID = ',_export_connection,' was successfully deleted.');
	return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_delete_etl_data4export_connection(integer) is
'The function deletes all records from ETL tables for given export_connection.';

grant execute on function target_data.fn_etl_delete_etl_data4export_connection(integer) to app_nfiesta_mng;
-- </function>



-- <function name="fn_etl_delete_export_connection" schema="target_data" src="functions/etl/fn_etl_delete_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_etl_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_etl_delete_export_connection(_id integer)
RETURNS text
AS
$$
DECLARE
	_check_etl_data	integer;
	_check_user		integer;
	_res			text;
BEGIN

	IF _id IS NULL THEN 
		RAISE EXCEPTION 'Error: 01: fn_etl_delete_export_connection: Input argument _id must not be NULL!';
	END IF;

	IF NOT EXISTS (SELECT t1.* FROM target_data.t_export_connection AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Error: 02: fn_etl_delete_export_connection: Given export connection (%) does not exist in t_export_connection table.', _id;
	END IF;

	with
	w1 as	(
			select tetv.id from target_data.t_etl_target_variable as tetv where tetv.export_connection = _id	union all
			select tead.id from target_data.t_etl_area_domain as tead where tead.export_connection = _id		union all
			select tesp.id from target_data.t_etl_sub_population as tesp where tesp.export_connection = _id
			)
	select count(w1.*) from w1
	into _check_etl_data;

	with
	w1 as	(
			select r.rolname as username,r1.rolname as "role"
			from pg_catalog.pg_roles r join pg_catalog.pg_auth_members m
			on (m.member = r.oid)
			join pg_roles r1 on (m.roleid=r1.oid)                                  
			where r.rolcanlogin and r.rolname = current_user
			order by 1
			)
	select count(*) from w1 where w1.role = 'app_nfiesta_mng'
	into _check_user;

	if _check_etl_data > 0 and _check_user = 0
	then
		raise exception 'Error 03: fn_etl_delete_export_connection: NOT ALLOWED delete given export connection. Given export connection (_id = %) contains ETL data and current user is not member in role "app_nfiesta_mng".',_id;
	end if;	
	
	if _check_etl_data = 0 and _check_user = 0
	then
		DELETE FROM target_data.t_export_connection WHERE id  = _id;
		_res := concat('The export connection ID = ',_id,' was successfully deleted from t_export_connection.');
	else
		_res := target_data.fn_etl_delete_etl_data4export_connection(_id);
	end if;

	return _res;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_etl_delete_export_connection(integer) IS
'The function delete record from t_export_connection and delete ETL datas for give export connection.';

GRANT EXECUTE ON FUNCTION target_data.fn_etl_delete_export_connection(integer) TO public;
-- </function>

