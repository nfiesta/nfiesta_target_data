--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_get_classification_rule_id4category" schema="target_data" src="functions/fn_get_classification_rule_id4category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule_id4category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule_id4category(integer[], integer[], integer[], integer[], boolean[], boolean[]) CASCADE;

create or replace function target_data.fn_get_classification_rule_id4category
(
	_area_domain_category		integer[],
	_area_domain_object			integer[],
	_sub_population_category 	integer[],
	_sub_population_object		integer[],
	_use_negative_ad			boolean[],
	_use_negative_sp			boolean[]
)
returns table
(
adc2classification_rule				integer[],
spc2classification_rule				integer[]
)
as
$$
declare
		_id_adc2classification_rule		integer[];
		_id_ad_i						integer;
		_id_spc2classification_rule		integer[];
		_id_sp_i						integer;
begin
		if	_area_domain_category is null
		then
			raise exception 'Error 01: fn_get_classification_rule_id4category: Input argument _area_domain_category must not be NULL!';
		end if;
		
		if	_sub_population_category is null
		then
			raise exception 'Error 02: fn_get_classification_rule_id4category: Input argument _sub_population_category must not be NULL!';
		end if;
	
		---------------------------------------------------------------------------------
		if _area_domain_category = array[0]
		then
			_id_adc2classification_rule := null::integer[];
		else
			for i in 1..array_length(_area_domain_category,1)
			loop
				select id from target_data.cm_adc2classification_rule
				where area_domain_category = _area_domain_category[i]
				and ldsity_object = _area_domain_object[i]
				and use_negative = _use_negative_ad[i]
				into _id_ad_i;
			
				if _area_domain_category[i] is distinct from 0 and _id_ad_i is null
				then
					raise exception 'Error 03: fn_get_classification_rule_id4category: For _area_domain_category = %, _area_domain_object = % and _use_negative = % not found any record in table cm_adc2classification_rule',_area_domain_category[i],_area_domain_object[i],_use_negative_ad[i];
				end if;
			
				if i = 1
				then
					if _id_ad_i is null
					then
						_id_adc2classification_rule := null::integer[];
					else
						_id_adc2classification_rule := array[_id_ad_i];
					end if;
				else
					if _id_ad_i is null
					then
						_id_adc2classification_rule := _id_adc2classification_rule;
					else
						_id_adc2classification_rule := _id_adc2classification_rule || array[_id_ad_i];
					end if;
				end if;
			
			end loop;
		end if;
		---------------------------------------------------------------------------------
		if _sub_population_category = array[0]
		then
			_id_spc2classification_rule := null::integer[];
		else
			for i in 1..array_length(_sub_population_category,1)
			loop
				select id from target_data.cm_spc2classification_rule
				where sub_population_category = _sub_population_category[i]
				and ldsity_object = _sub_population_object[i]
				and use_negative = _use_negative_sp[i]
				into _id_sp_i;
			
				if _sub_population_category[i] is distinct from 0 and _id_sp_i is null
				then
					raise exception 'Error 04: fn_get_classification_rule_id4category: For _sub_population_category = %, _sub_population_object = % and _use_negative = % not found any record in table cm_spc2classification_rule',_sub_population_category[i],_sub_population_object[i],_use_negative_sp[i];
				end if;			
			
				if i = 1
				then
					if _id_sp_i is null
					then
						_id_spc2classification_rule := null::integer[];
					else
						_id_spc2classification_rule := array[_id_sp_i];
					end if;
				else
					if _id_sp_i is null
					then
						_id_spc2classification_rule := _id_spc2classification_rule;
					else
						_id_spc2classification_rule := _id_spc2classification_rule || array[_id_sp_i];
					end if;
				end if;
			
			end loop;
		end if;
		---------------------------------------------------------------------------------

		return query select _id_adc2classification_rule, _id_spc2classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;		

comment on function target_data.fn_get_classification_rule_id4category(integer[], integer[], integer[], integer[], boolean[], boolean[]) is
'The function for the area domain or sub population category returns the database identifier of their classifiaction_rules.';

grant execute on function target_data.fn_get_classification_rule_id4category(integer[], integer[], integer[], integer[], boolean[], boolean[]) to public;
-- </function>

