--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_delete_ldsity_values" schema="target_data" src="functions/fn_delete_ldsity_values.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_ldsity_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_ldsity_values(integer, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_ldsity_values(_target_variable integer DEFAULT NULL::int, _area_domain integer DEFAULT NULL::int, _sub_population integer DEFAULT NULL::int)
RETURNS void
AS
$$
BEGIN
	IF _target_variable IS NOT NULL AND NOT EXISTS (SELECT * FROM target_data.c_target_variable WHERE id = _target_variable)
	THEN RAISE EXCEPTION 'Given parameter _target_variable (%) does not exist in table c_target_variable!', _target_variable;
	END IF;

	IF _area_domain IS NOT NULL AND NOT EXISTS (SELECT * FROM target_data.c_area_domain WHERE id = _area_domain)
	THEN RAISE EXCEPTION 'Given parameter _area_domain (%) does not exist in table c_area_domain!', _area_domain;
	END IF;

	IF _sub_population IS NOT NULL AND NOT EXISTS (SELECT * FROM target_data.c_sub_population WHERE id = _sub_population)
	THEN RAISE EXCEPTION 'Given parameter _sub_population (%) does not exist in table c_sub_population!', _sub_population;
	END IF;

	WITH w AS (
		-- there can be available datasets without any records in t_ldsity_values
		-- that's why first delete from t_available_datasets to get all possible ids
		DELETE FROM target_data.t_available_datasets
		WHERE id IN 
			(SELECT id
			FROM target_data.t_available_datasets
			WHERE categorization_setup IN 
				(SELECT categorization_setup 
				FROM target_data.cm_ldsity2target2categorization_setup AS t1
				LEFT JOIN target_data.cm_ldsity2target_variable AS t2
				ON t1.ldsity2target_variable = t2.id
				LEFT JOIN target_data.cm_adc2classification_rule AS t3
				ON t1.adc2classification_rule @> array[t3.id]
				LEFT JOIN target_data.cm_spc2classification_rule AS t4
				ON t1.spc2classification_rule @> array[t4.id]
				WHERE 	CASE WHEN _target_variable IS NOT NULL THEN t2.target_variable = _target_variable ELSE true END
					AND
					CASE WHEN _area_domain IS NOT NULL THEN t3.area_domain_category IN
						(SELECT id FROM target_data.c_area_domain_category 
						WHERE area_domain = _area_domain) ELSE true END
					AND
					CASE WHEN _sub_population IS NOT NULL THEN t4.sub_population_category IN
						(SELECT id FROM target_data.c_sub_population_category 
						WHERE sub_population = _sub_population) ELSE true END
				)
			)
		RETURNING id
	)
	DELETE FROM target_data.t_ldsity_values
	WHERE available_datasets IN (SELECT id FROM w);
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_ldsity_values(integer, integer, integer) IS
'Function deletes records from t_ldsity_values and t_available_datasets table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_ldsity_values(integer, integer, integer) TO public;

-- </function>

-- <function name="fn_delete_area_domain" schema="target_data" src="functions/fn_delete_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_delete_area_domain(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_area_domain(_id integer)
RETURNS void
AS
$$
BEGIN
	WITH w AS (
		DELETE FROM target_data.cm_ldsity2target2categorization_setup
		WHERE categorization_setup IN
			(SELECT categorization_setup 
			FROM target_data.cm_ldsity2target2categorization_setup AS t1
			INNER JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.adc2classification_rule @> array[t2.id]
			WHERE t2.area_domain_category IN
					(SELECT id FROM target_data.c_area_domain_category 
					WHERE area_domain = _id)
			)
		RETURNING categorization_setup
	)
	DELETE FROM target_data.t_categorization_setup
	WHERE id IN 
		(SELECT categorization_setup
		FROM w);

	WITH w AS (
		SELECT target_variable
		FROM target_data.cm_ldsity2target_variable AS t1
		INNER JOIN target_data.cm_adc2classification_rule AS t2
		ON t1.area_domain_category @> array[t2.id]
		WHERE t2.area_domain_category IN
				(SELECT id FROM target_data.c_area_domain_category 
				WHERE area_domain = _id)
		),
	-- delete all contributions belonging to the target_variable
	w_del AS (
		DELETE FROM target_data.cm_ldsity2target_variable
		WHERE target_variable IN (SELECT target_variable FROM w)
		RETURNING target_variable
	)
	DELETE FROM target_data.c_target_variable
	WHERE id IN (SELECT target_variable FROM w_del);

	DELETE FROM target_data.cm_adc2classrule2panel_refyearset 
	WHERE adc2classification_rule IN 
		(SELECT id FROM target_data.cm_adc2classification_rule 
		WHERE area_domain_category IN
			(SELECT id FROM target_data.c_area_domain_category 
			WHERE area_domain = _id)); 		
				
	DELETE FROM target_data.cm_adc2classification_rule 
	WHERE area_domain_category IN 
		(SELECT id FROM target_data.c_area_domain_category 
		WHERE area_domain = _id);

	IF EXISTS (SELECT * FROM target_data.t_adc_hierarchy
		WHERE variable_superior IN (SELECT id FROM target_data.c_area_domain_category
			WHERE area_domain = _id) AND
			dependent = true
		)
	THEN 
		RAISE EXCEPTION 'Given area domain cannot be deleted, because some of its categories are present in table t_adc_hierarchy in the superior position. It can destroy the dependency between the categories. You have to delete inferior categories (area domain) first.';
	END IF;

	DELETE FROM target_data.t_adc_hierarchy
	WHERE	variable IN (SELECT id FROM target_data.c_area_domain_category
			WHERE area_domain = _id) OR
		variable_superior IN (SELECT id FROM target_data.c_area_domain_category
			WHERE area_domain = _id);
	
	DELETE FROM target_data.c_area_domain_category 
	WHERE area_domain = _id;
	
	DELETE FROM target_data.c_area_domain 
	WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_area_domain(integer) IS
'Function deletes records from c_area_domain, c_area_domain_category, cm_adc2classification_rule and cm_adc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_area_domain(integer) TO public;

-- </function>

-- <function name="fn_delete_area_domain_and_values" schema="target_data" src="functions/fn_delete_area_domain_and_values.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_area_domain_and_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_area_domain_and_values(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_area_domain_and_values(_id integer)
RETURNS void
AS
$$
BEGIN
	PERFORM target_data.fn_delete_ldsity_values(NULL::int, _id, NULL::int);

	PERFORM target_data.fn_delete_area_domain(_id);
END;
$$
LANGUAGE plpgsql
VOLATILE
STRICT
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_area_domain_and_values(integer) IS
'Function deletes records from all tables which can be influenced by the given area domain.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_area_domain_and_values(integer) TO public;

-- </function>

-- <function name="fn_delete_sub_population" schema="target_data" src="functions/fn_delete_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_delete_sub_population(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_sub_population(_id integer)
RETURNS void
AS
$$
BEGIN
	WITH w AS (
		DELETE FROM target_data.cm_ldsity2target2categorization_setup
		WHERE categorization_setup IN 
			(SELECT categorization_setup 
			FROM target_data.cm_ldsity2target2categorization_setup AS t1
			INNER JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.spc2classification_rule @> array[t2.id]
			WHERE t2.sub_population_category IN
					(SELECT id FROM target_data.c_sub_population_category 
					WHERE sub_population = _id)
			)
		RETURNING categorization_setup
	)
	DELETE FROM target_data.t_categorization_setup
	WHERE id IN 
		(SELECT categorization_setup
		FROM w);

	WITH w AS (
		SELECT target_variable
		FROM target_data.cm_ldsity2target_variable AS t1
		INNER JOIN target_data.cm_spc2classification_rule AS t2
		ON t1.sub_population_category @> array[t2.id]
		WHERE t2.sub_population_category IN
				(SELECT id FROM target_data.c_sub_population_category 
				WHERE sub_population = _id)
		),
	-- delete all contributions belonging to the target_variable
	w_del AS (
		DELETE FROM target_data.cm_ldsity2target_variable
		WHERE target_variable IN (SELECT target_variable FROM w)
		RETURNING target_variable
	)
	DELETE FROM target_data.c_target_variable
	WHERE id IN (SELECT target_variable FROM w_del);

	DELETE FROM target_data.cm_spc2classrule2panel_refyearset 
	WHERE spc2classification_rule IN 
		(SELECT id FROM target_data.cm_spc2classification_rule 
		WHERE sub_population_category IN
			(SELECT id FROM target_data.c_sub_population_category 
			WHERE sub_population = _id)); 
				
	DELETE FROM target_data.cm_spc2classification_rule 
	WHERE sub_population_category IN 
		(SELECT id FROM target_data.c_sub_population_category 
		WHERE sub_population = _id);

	IF EXISTS (SELECT * FROM target_data.t_spc_hierarchy
		WHERE variable_superior IN (SELECT id FROM target_data.c_sub_population_category
			WHERE sub_population = _id) AND 
			dependent = true
		)
	THEN 
		RAISE EXCEPTION 'Given sub population cannot be deleted, because some of its categories are present in table t_spc_hierarchy in the superior position. It can destroy the dependency between the categories. You have to delete inferior categories (sub population) first.';
	END IF;

	DELETE FROM target_data.t_spc_hierarchy
	WHERE 	variable IN (SELECT id FROM target_data.c_sub_population_category
			WHERE sub_population = _id) OR 
		variable_superior IN (SELECT id FROM target_data.c_sub_population_category
			WHERE sub_population = _id);

	DELETE FROM target_data.c_sub_population_category 
	WHERE sub_population = _id;
	
	DELETE FROM target_data.c_sub_population
	WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_sub_population(integer) IS
'Function deletes records from c_sub_population, c_sub_population_category, cm_spc2classification_rule and cm_spc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_sub_population(integer) TO public;

-- </function>

-- <function name="fn_delete_sub_population_and_values" schema="target_data" src="functions/fn_delete_sub_population_and_values.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_sub_population_and_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_sub_population_and_values(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_sub_population_and_values(_id integer)
RETURNS void
AS
$$
BEGIN
	PERFORM target_data.fn_delete_ldsity_values(NULL::int, _id, NULL::int);

	PERFORM target_data.fn_delete_sub_population(_id);
END;
$$
LANGUAGE plpgsql
VOLATILE
STRICT
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_sub_population_and_values(integer) IS
'Function deletes records from all tables which can be influenced by the given sub population.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_sub_population_and_values(integer) TO public;

-- </function>
