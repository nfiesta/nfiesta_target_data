--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_test_categorization_setup_input" schema="target_data" src="functions/fn_test_categorization_setup_input.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_test_categorization_setup_input
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_test_categorization_setup_input(integer, integer[], integer[], integer[][], integer[], integer[][]) CASCADE;

create or replace function target_data.fn_test_categorization_setup_input 
(
	_target_variable		integer,
	_target_variable_cm		integer[],
	_area_domain			integer[],
	_area_domain_object		integer[][],
	_sub_population			integer[],
	_sub_population_object	integer[][]
)
returns boolean
as
$$
declare
		_array_id							integer[];
		_array_id4check						integer[];
		_lot_100							integer;
		_lot_200							integer;
		_variant							integer;
		_target_variable_cm_order			integer[];
		_raise_notice_value_ad				integer default 0;
		_raise_notice_value_sp				integer default 0;
		_check_input_ad_all					integer;
		_check_input_ad_null				integer;
		_check_input_ad						integer[];
		_check_input_ad_object				integer[];
		_check_input_ad_errors				integer[];
		_check_input_sp_all					integer;
		_check_input_sp_null				integer;
		_check_input_sp						integer[];
		_check_input_sp_object				integer[];
		_check_input_sp_errors				integer[];
		_ldsity_object_type_ad				integer;
		_ldsity_object_type_sp				integer;
		_raise_notice_text_ad				text;
		_raise_notice_text_sp				text;
		_raise_notice_text					text;
		_raise_notice_value					integer;
		_result								boolean;
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_test_categorization_setup_input : The input argument _target_variable must not be NULL !';
		end if;

		if _target_variable_cm is null
		then
			raise exception 'Error 02: fn_test_categorization_setup_input : The input argument _target_variable_cm must not be NULL !';
		end if;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where target_variable = _target_variable
		into
				_array_id;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where id in (select unnest(_target_variable_cm))
		into
				_array_id4check;			

		if _array_id != _array_id4check
			then raise exception 'Error 03: fn_test_categorization_setup_input: The internal argument _array_id = % and internal argument _array_id4check = % are not the same!',_array_id, _array_id4check;
		end if;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 04: fn_test_categorization_setup_input: This variant is not implemented yet!';
		end if;

		------------------------------------------------------------------
		-- ordering input argument _target_variable_cm => order by ldsity_object_type and id
		------------------------------------------------------------------	
		with
		w1 as	(
				select
						row_number() over() as id4order,
						t.id as target_variable_cm
				from
						(
						select
								id,
								ldsity_object_type
						from
								target_data.cm_ldsity2target_variable cltv
						where
								id in (select unnest(_target_variable_cm))
						order
								by ldsity_object_type, id
						) as t
				order
						by ldsity_object_type, id
				)
		select
				array_agg(w1.target_variable_cm order by w1.id4order) as target_variable_cm
		from
				w1
		into
				_target_variable_cm_order;
		------------------------------------------------------------------
		-- check passed object for input area domains
		------------------------------------------------------------------
		if _area_domain is not null
		then
			if array_length(_target_variable_cm_order,1) != array_length(_area_domain_object,1)
			then
				_raise_notice_text_ad := 'Error 05: fn_test_categorization_setup_input: For function is not passed the same number of elements in the field for area domain and in the field for area domain object!';
				_raise_notice_value_ad := 1;
			end if;

			if _raise_notice_value_ad = 0
			then
				for i in 1..array_length(_target_variable_cm_order,1)
				loop
					with
					w1 as	(
							select
									_area_domain as area_domain,
									_area_domain_object[array_position(_target_variable_cm, _target_variable_cm_order[i]):array_position(_target_variable_cm, _target_variable_cm_order[i])] as area_domain_object
							)
					,w2 as	(
							select
									unnest(w1.area_domain) as area_domain,
									unnest(w1.area_domain_object) as area_domain_object
							from
									w1
							)
					,w3 as (select 1 as id4join, count(w2.*) as count_all from w2)
					,w4 as (select 1 as id4join, count(w2.*) as count_null from w2 where w2.area_domain_object is null)
					,w5 as	(
							select
									1 as id4join,
									array_agg(w2.area_domain order by w2.area_domain) as area_domain,
									array_agg(w2.area_domain_object order by w2.area_domain) as area_domain_object
							from
									w2
							)
					select
							w3.count_all,
							w4.count_null,
							w5.area_domain,
							w5.area_domain_object
					from
							w3
							inner join w4 on w3.id4join = w4.id4join
							inner join w5 on w3.id4join = w5.id4join
					into
						_check_input_ad_all,
						_check_input_ad_null,
						_check_input_ad,
						_check_input_ad_object;		

					select ldsity_object_type from target_data.cm_ldsity2target_variable
					where id = _target_variable_cm_order[i]
					into _ldsity_object_type_ad;

					if _check_input_ad_all = 1
					then
						if	(
							(_variant in (1,2)) or
							(_variant in (3,4) and _ldsity_object_type_ad = 100)
							)
						then
							if _check_input_ad_null = 1
							then
								_raise_notice_text_ad := concat('Error 06: fn_test_categorization_setup_input: For some of area domains is not defined a rule!');
								_raise_notice_value_ad := 1;
							end if;
						else
							-- _variant is (3,4) and _ldsity_object_type_ad is 200
							if _check_input_ad_null is distinct from 1
							then
								_raise_notice_text_ad := concat('Error 07: fn_test_categorization_setup_input: For some of local densities of type "division" must not be passed a rule!');
								_raise_notice_value_ad := 1;
							end if;
						end if;
					else
						if	(
							(_variant in (1,2)) or
							(_variant in (3,4) and _ldsity_object_type_ad = 100)
							)
						then
							if _check_input_ad_null > 0
							then
								with
								w1 as	(
										select
												unnest(_check_input_ad) as id,
												unnest(_check_input_ad_object) as res
										)
								select array_agg(w1.id order by w1.id) from w1 where res is null
								into _check_input_ad_errors;

								_raise_notice_text_ad := concat('Error 08: fn_test_categorization_setup_input: For some of area domains is not defined a rule!');
								_raise_notice_value_ad := 1;
							end if;
						else
							-- _variant is (3,4) and _ldsity_object_type_ad is 200 
							if _check_input_ad_all is distinct from _check_input_ad_null
							then
								_raise_notice_text_ad := concat('Error 09: fn_test_categorization_setup_input: For some of local densities of type "division" must not be passed a rule!');
								_raise_notice_value_ad := 1;
							end if;
						end if;
					end if;
				end loop;
			end if;
		end if;
		------------------------------------------------------------------
		-- check passed object for input sub populations
		------------------------------------------------------------------
		if _sub_population is not null
		then
			if array_length(_target_variable_cm_order,1) != array_length(_sub_population_object,1)
			then
				_raise_notice_text_sp := 'Error 10: fn_test_categorization_setup_input: For function is not passed the same number of elements in the field for sub population and in the field for sub population object!';
				_raise_notice_value_sp := 1;
			end if;

			if _raise_notice_value_sp = 0
			then
				for i in 1..array_length(_target_variable_cm_order,1)
				loop
					with
					w1 as	(
							select
									_sub_population as sub_population,
									_sub_population_object[array_position(_target_variable_cm, _target_variable_cm_order[i]):array_position(_target_variable_cm, _target_variable_cm_order[i])] as sub_population_object
							)
					,w2 as	(
							select
									unnest(w1.sub_population) as sub_population,
									unnest(w1.sub_population_object) as sub_population_object
							from
									w1
							)
					,w3 as (select 1 as id4join, count(w2.*) as count_all from w2)
					,w4 as (select 1 as id4join, count(w2.*) as count_null from w2 where w2.sub_population_object is null)
					,w5 as	(
							select
									1 as id4join,
									array_agg(w2.sub_population order by w2.sub_population) as sub_population,
									array_agg(w2.sub_population_object order by w2.sub_population) as sub_population_object
							from
									w2
							)
					select
							w3.count_all,
							w4.count_null,
							w5.sub_population,
							w5.sub_population_object
					from
							w3
							inner join w4 on w3.id4join = w4.id4join
							inner join w5 on w3.id4join = w5.id4join
					into
						_check_input_sp_all,
						_check_input_sp_null,
						_check_input_sp,
						_check_input_sp_object;

					select ldsity_object_type from target_data.cm_ldsity2target_variable
					where id = _target_variable_cm_order[i]
					into _ldsity_object_type_sp;

					if _check_input_sp_all = 1
					then
						if	(
							(_variant in (1,2)) or									
							(_variant in (3,4) and _ldsity_object_type_sp = 200)	
							)
						then
							if _check_input_sp_null = 1
							then
								_raise_notice_text_sp := concat('Error 11: fn_test_categorization_setup_input: For some of sub populations is not defined a rule!');
								_raise_notice_value_sp := 1;
							end if;
						else
							-- _variant is (3,4) and _ldsity_object_type_sp is 100 
							if _check_input_sp_null is distinct from 1
							then
								_raise_notice_text_sp := concat('Error 12: fn_test_categorization_setup_input: For local density of type "core" must not be passed a rule!');
								_raise_notice_value_sp := 1;
							end if;
						end if;
					else
						if	(
							(_variant in (1,2)) or
							(_variant in (3,4) and _ldsity_object_type_sp = 200)
							)
						then
							if _check_input_sp_null > 0
							then
								with
								w1 as	(
										select
												unnest(_check_input_sp) as id,
												unnest(_check_input_sp_object) as res
										)
								select array_agg(w1.id order by w1.id) from w1 where res is null
								into _check_input_sp_errors;

								_raise_notice_text_sp := concat('Error 13: fn_test_categorization_setup_input: For some of sub populations is not defined a rule!');
								_raise_notice_value_sp := 1;
							end if;
						else
							-- _variant is (3,4) and _ldsity_object_type_sp is 100
							if _check_input_sp_all is distinct from _check_input_sp_null
							then
								_raise_notice_text_sp := concat('Error 14: fn_test_categorization_setup_input: For local density of type "core" must not be passed a rule!');
								_raise_notice_value_sp := 1;
							end if;
						end if;
					end if;
				end loop;
			end if;
		end if;
		------------------------------------------------------------------
		------------------------------------------------------------------
		------------------------------------------------------------------
		_raise_notice_value := _raise_notice_value_ad + _raise_notice_value_sp;
		------------------------------------------------------------------
		if _raise_notice_value = 0
		then
			_result := true;
		else
			if _raise_notice_value_ad = 0 and _raise_notice_value_sp = 0 then _raise_notice_text = ''::text; end if;
			if _raise_notice_value_ad = 1 and _raise_notice_value_sp = 0 then _raise_notice_text = _raise_notice_text_ad; end if;
			if _raise_notice_value_ad = 0 and _raise_notice_value_sp = 1 then _raise_notice_text = _raise_notice_text_sp; end if;
			if _raise_notice_value_ad = 1 and _raise_notice_value_sp = 1 then _raise_notice_text = _raise_notice_text_ad || ' '::text || _raise_notice_text_sp; end if;

			raise notice '%',_raise_notice_text;

			_result := false;
		end if;

		return _result;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_test_categorization_setup_input(integer,integer[],integer[],integer[][],integer[],integer[][]) IS
'The function tests input arguments for fn_get_categorization_setup function.';

grant execute on function target_data.fn_test_categorization_setup_input(integer,integer[],integer[],integer[][],integer[],integer[][]) to public;
-- </function>
