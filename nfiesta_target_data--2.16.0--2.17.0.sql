--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_system_utilization" schema="extschema" src="functions/fn_system_utilization.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- Function: target_data.fn_system_utilization()

-- drop function if exists target_data.fn_system_utilization();

CREATE OR REPLACE FUNCTION target_data.fn_system_utilization()
  RETURNS json
AS $$
  import json
  import os

  avgs = os.getloadavg()
  data = {}
  load = {}
  load['1_min'] = avgs[0]
  load['5_min'] = avgs[1]
  load['15_min'] = avgs[2]
  
  data['load'] = load
  data['cpu_count'] = os.cpu_count()
  
  data['connection_limit'] = plpy.execute('SELECT rolconnlimit FROM pg_roles where rolname = CURRENT_USER;')[0]['rolconnlimit']
  return json.dumps(data)
$$ LANGUAGE plpython3u;


COMMENT ON FUNCTION target_data.fn_system_utilization() IS 'Function reporting system utilization. Number of CPU cores. User cnnection limit.';
--------------TEST
/*
select jsonb_pretty(target_data.fn_system_utilization()::jsonb);
*/

-- </function>
