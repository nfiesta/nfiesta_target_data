--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_etl_get_area_domain_json" schema="target_data" src="functions/etl/fn_etl_get_area_domain_json.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_json
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domain_json(integer[], integer, boolean) CASCADE;

create or replace function target_data.fn_etl_get_area_domain_json
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_etl						boolean default null::boolean
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
		_cond						text;
		_res						json;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_json: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_json: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;		

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
			
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.adc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.adc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			with
			w1 as	(
					select
							0 as id,
							0 as export_connection,
							array[0] as area_domain,
							''::varchar as type_label,
							''::text as type_description,
							''::varchar as type_label_en,
							''::text as type_description_en,
							array[0] as area_domain_category,
							''::varchar as category_label,
							''::text as category_description,
							''::varchar as category_label_en,
							''::text as category_description_en,							
							0 as etl_id,
							0 as id_t_etl_ad,
							null::boolean as atomic
					)
			,w2 as	(
					select w1.id, w1.export_connection,
					w1.area_domain, w1.type_label, w1.type_description, w1.type_label_en, w1.type_description_en,
					w1.area_domain_category, w1.category_label, w1.category_description, w1.category_label_en, w1.category_description_en,
					w1.etl_id, w1.id_t_etl_ad, w1.atomic
					from w1 where w1.id is distinct from 0
					)
			select json_agg(
			json_build_object
				(
				'id',w2.id,
				'area_domain',w2.area_domain,
				'label_en_type',w2.type_label_en,
				'area_domain_category',w2.area_domain_category,
				'label_en_category',w2.category_label_en
				))
			from w2 into _res;
		else
			if _etl is null
			then
				_cond := 'TRUE'::text;
			else
				if _etl = true
				then
					_cond := 'w10.etl_id is not null'::text;
				else
					_cond := 'w10.etl_id is null'::text;
				end if;
			end if;
		
			execute
			'
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.adc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).label as type_label,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).description as type_description,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).label_en as type_label_en,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).description_en as type_description_en,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).id_category,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).label as category_label,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).description as category_description,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).label_en as category_label_en,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).description_en as category_description_en
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest($2))								
														)
					and
							t.adc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.type_label, w1.type_description, w1.type_label_en, w1.type_description_en, w1.id_category, w1.category_label, w1.category_description, w1.category_label_en, w1.category_description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select array[cad.id] as id_type, cad.label as type_label, cad.description as type_description, cad.label_en as  type_label_en, cad.description_en as type_description_en
					from target_data.c_area_domain as cad
					where cad.id in (select w4.id_type[1] from w4)
					)
			,w6 as (
					select
							w5.*,
							array[t.id] as id_category,
							t.label as category_label,
							t.description as category_description,
							t.label_en as category_label_en,
							t.description_en as category_description_en
					from
							w5
					inner join
							(
							select cadc.* from target_data.c_area_domain_category as cadc
							where cadc.area_domain in (select w5.id_type[1] from w5)
							) as t 
					on
							w5.id_type[1] = t.area_domain
					)			
			,w7 as	(
					select w2.id_type, w2.type_label, w2.type_description, w2.type_label_en, w2.type_description_en, w2.id_category, w2.category_label, w2.category_description, w2.category_label_en, w2.category_description_en from w2 union all
					select w6.id_type, w6.type_label, w6.type_description, w6.type_label_en, w6.type_description_en, w6.id_category, w6.category_label, w6.category_description, w6.category_label_en, w6.category_description_en from w6
					)
			,w8 as	(
					select
							$1 as export_connection,
							w7.id_type as area_domain,
							w7.type_label,
							w7.type_description,
							w7.type_label_en,
							w7.type_description_en,
							w7.id_category as area_domain_category,
							w7.category_label,
							w7.category_description,
							w7.category_label_en,
							w7.category_description_en							
					from
							w7
					)
			,w9 as	(
					select
							w8.export_connection,
							w8.area_domain,
							w8.type_label,
							w8.type_description,
							w8.type_label_en,
							w8.type_description_en,
							w8.area_domain_category,
							w8.category_label,
							w8.category_description,
							w8.category_label_en,
							w8.category_description_en,								
							t.etl_id,
							t.id as id_t_etl_ad
					from
							w8 left join target_data.t_etl_area_domain as t
					on
							w8.export_connection = t.export_connection
					and	
							target_data.fn_etl_array_compare(w8.area_domain,t.area_domain)
					order
							by w8.area_domain, w8.area_domain_category
					)
			,w10 as (
					select 
							a.*,
							b.category_json
					from
							(select distinct w9.export_connection, w9.area_domain, w9. type_label, w9.type_description, w9.type_label_en, w9.type_description_en, w9.etl_id, w9.id_t_etl_ad from w9) as a
					inner join
							(
							select
									w9.area_domain,
									json_agg(json_build_object(''area_domain_category'',w9.area_domain_category,''label_en_category'',w9.category_label_en)) as category_json
							from
									w9 group by w9.area_domain
							) as b
					on
							a.area_domain = b.area_domain
					order
							by a.area_domain
					)
			,w11 as (
					select
							(row_number() over ())::integer as id_order,
							w10.export_connection,
							w10.area_domain,
							w10.type_label,
							w10.type_description,
							w10.type_label_en,
							w10.type_description_en,							
							w10.etl_id,
							w10.id_t_etl_ad,
							w10.category_json,
							case when array_length(w10.area_domain,1) = 1 then true else false end as atomic
					from
							w10 where '|| _cond ||'
					)
			select json_agg(
			json_build_object
				(
				''id'',w11.id_order,
				''area_domain'',w11.area_domain,
				''label_en_type'',w11.type_label_en,
				''category'',w11.category_json
				))
			from w11
			'
			using _export_connection, _categorization_setups
			into _res;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domain_json(integer[], integer, boolean) is
'Function returns records for ETL c_area_domain table in JSON format.';

grant execute on function target_data.fn_etl_get_area_domain_json(integer[], integer, boolean) to public;
-- </function>



-- <function name="fn_etl_get_sub_population_json" schema="target_data" src="functions/etl/fn_etl_get_sub_population_json.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_json
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_sub_population_json(integer[], integer, boolean) CASCADE;

create or replace function target_data.fn_etl_get_sub_population_json
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_etl						boolean default null::boolean
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
		_cond						text;
		_res						json;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_json: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_json: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;		

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
			
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.spc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))
													)
				and
						t.spc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			with
			w1 as	(
					select
							0 as id,
							0 as export_connection,
							array[0] as sub_population,
							''::varchar as type_label,
							''::text as type_description,
							''::varchar as type_label_en,
							''::text as type_description_en,
							array[0] as sub_population_category,
							''::varchar as category_label,
							''::text as category_description,
							''::varchar as category_label_en,
							''::text as category_description_en,							
							0 as etl_id,
							0 as id_t_etl_sp,
							null::boolean as atomic
					)
			,w2 as	(
					select w1.id, w1.export_connection,
					w1.sub_population, w1.type_label, w1.type_description, w1.type_label_en, w1.type_description_en,
					w1.sub_population_category, w1.category_label, w1.category_description, w1.category_label_en, w1.category_description_en,
					w1.etl_id, w1.id_t_etl_sp, w1.atomic
					from w1 where w1.id is distinct from 0
					)
			select json_agg(
			json_build_object
				(
				'id',w2.id,
				'sub_population',w2.sub_population,
				'label_en_type',w2.type_label_en,
				'sub_population_category',w2.sub_population_category,
				'label_en_category',w2.category_label_en
				))
			from w2 into _res;
		else
			if _etl is null
			then
				_cond := 'TRUE'::text;
			else
				if _etl = true
				then
					_cond := 'w10.etl_id is not null'::text;
				else
					_cond := 'w10.etl_id is null'::text;
				end if;
			end if;
		
			execute
			'
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.spc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).label as type_label,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).description as type_description,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).label_en as type_label_en,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).description_en as type_description_en,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).id_category,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).label as category_label,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).description as category_description,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).label_en as category_label_en,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).description_en as category_description_en
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest($2))								
														)
					and
							t.spc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.type_label, w1.type_description, w1.type_label_en, w1.type_description_en, w1.id_category, w1.category_label, w1.category_description, w1.category_label_en, w1.category_description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select array[csp.id] as id_type, csp.label as type_label, csp.description as type_description, csp.label_en as  type_label_en, csp.description_en as type_description_en
					from target_data.c_sub_population as csp
					where csp.id in (select w4.id_type[1] from w4)
					)
			,w6 as (
					select
							w5.*,
							array[t.id] as id_category,
							t.label as category_label,
							t.description as category_description,
							t.label_en as category_label_en,
							t.description_en as category_description_en
					from
							w5
					inner join
							(
							select cspc.* from target_data.c_sub_population_category as cspc
							where cspc.sub_population in (select w5.id_type[1] from w5)
							) as t 
					on
							w5.id_type[1] = t.sub_population
					)			
			,w7 as	(
					select w2.id_type, w2.type_label, w2.type_description, w2.type_label_en, w2.type_description_en, w2.id_category, w2.category_label, w2.category_description, w2.category_label_en, w2.category_description_en from w2 union all
					select w6.id_type, w6.type_label, w6.type_description, w6.type_label_en, w6.type_description_en, w6.id_category, w6.category_label, w6.category_description, w6.category_label_en, w6.category_description_en from w6
					)
			,w8 as	(
					select
							$1 as export_connection,
							w7.id_type as sub_population,
							w7.type_label,
							w7.type_description,
							w7.type_label_en,
							w7.type_description_en,
							w7.id_category as sub_population_category,
							w7.category_label,
							w7.category_description,
							w7.category_label_en,
							w7.category_description_en							
					from
							w7
					)
			,w9 as	(
					select
							w8.export_connection,
							w8.sub_population,
							w8.type_label,
							w8.type_description,
							w8.type_label_en,
							w8.type_description_en,
							w8.sub_population_category,
							w8.category_label,
							w8.category_description,
							w8.category_label_en,
							w8.category_description_en,								
							t.etl_id,
							t.id as id_t_etl_sp
					from
							w8 left join target_data.t_etl_sub_population as t
					on
							w8.export_connection = t.export_connection
					and	
							target_data.fn_etl_array_compare(w8.sub_population,t.sub_population)
					order
							by w8.sub_population, w8.sub_population_category
					)
			,w10 as (
					select 
							a.*,
							b.category_json
					from
							(select distinct w9.export_connection, w9.sub_population, w9. type_label, w9.type_description, w9.type_label_en, w9.type_description_en, w9.etl_id, w9.id_t_etl_sp from w9) as a
					inner join
							(
							select
									w9.sub_population,
									json_agg(json_build_object(''sub_population_category'',w9.sub_population_category,''label_en_category'',w9.category_label_en)) as category_json
							from
									w9 group by w9.sub_population
							) as b
					on
							a.sub_population = b.sub_population
					order
							by a.sub_population
					)
			,w11 as (
					select
							(row_number() over ())::integer as id_order,
							w10.export_connection,
							w10.sub_population,
							w10.type_label,
							w10.type_description,
							w10.type_label_en,
							w10.type_description_en,							
							w10.etl_id,
							w10.id_t_etl_sp,
							w10.category_json,
							case when array_length(w10.sub_population,1) = 1 then true else false end as atomic
					from
							w10 where '|| _cond ||'
					)
			select json_agg(
			json_build_object
				(
				''id'',w11.id_order,
				''sub_population'',w11.sub_population,
				''label_en_type'',w11.type_label_en,
				''category'',w11.category_json
				))
			from w11
			'
			using _export_connection, _categorization_setups
			into _res;
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_sub_population_json(integer[], integer, boolean) is
'Function returns records for ETL c_sub_population table in JSON format.';

grant execute on function target_data.fn_etl_get_sub_population_json(integer[], integer, boolean) to public;
-- </function>



-- <function name="fn_etl_get_area_domain" schema="target_data" src="functions/etl/fn_etl_get_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domain(integer[], integer, json, boolean) CASCADE;

create or replace function target_data.fn_etl_get_area_domain
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_auto_information			json,
	_etl						boolean default null::boolean
)
returns table
(
	id							integer,
	export_connection			integer,
	area_domain					integer[],
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	etl_id						integer,
	id_t_etl_ad					integer,
	atomic						boolean,
	auto_transfer				boolean,
	auto_pair					boolean,
	auto						boolean
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
		_cond						text;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;

		if _auto_information is null
		then
			raise exception 'Error 03: fn_etl_get_area_domain: Input argument _auto_information must not be NULL!';
		end if;			

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;		

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
			
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.adc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.adc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			return query
			with w as	(
						select
								0 as id,
								0 as export_connection,
								array[0] as area_domain,
								''::varchar as label,
								''::text as description,
								''::varchar as label_en,
								''::text as description_en,
								0 as etl_id,
								0 as id_t_etl_ad,
								null::boolean as atomic,
								null::boolean as auto_transfer,
								null::boolean as auto_pair,
								null::boolean as auto
						)
			select w.id, w.export_connection, w.area_domain, w.label, w.description, w.label_en,
			w.description_en, w.etl_id, w.id_t_etl_ad, w.atomic, w.auto_transfer, w.auto_pair, w.auto
			from w where w.id is distinct from 0;		
		else
			if _etl is null
			then
				_cond := 'TRUE'::text;
			else
				if _etl = true
				then
					_cond := 'w10.etl_id is not null'::text;
				else
					_cond := 'w10.etl_id is null'::text;
				end if;
			end if;
		
			return query execute
			'
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.adc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).label as type_label,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).description as type_description,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).label_en as type_label_en,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).description_en as type_description_en,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).id_category,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).label as category_label,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).description as category_description,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).label_en as category_label_en,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).description_en as category_description_en
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest($2))								
														)
					and
							t.adc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.type_label, w1.type_description, w1.type_label_en, w1.type_description_en, w1.id_category, w1.category_label, w1.category_description, w1.category_label_en, w1.category_description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select array[cad.id] as id_type, cad.label as type_label, cad.description as type_description, cad.label_en as  type_label_en, cad.description_en as type_description_en
					from target_data.c_area_domain as cad
					where cad.id in (select w4.id_type[1] from w4)
					)
			,w6 as (
					select
							w5.*,
							array[t.id] as id_category,
							t.label as category_label,
							t.description as category_description,
							t.label_en as category_label_en,
							t.description_en as category_description_en
					from
							w5
					inner join
							(
							select cadc.* from target_data.c_area_domain_category as cadc
							where cadc.area_domain in (select w5.id_type[1] from w5)
							) as t 
					on
							w5.id_type[1] = t.area_domain
					)			
			,w7 as	(
					select w2.id_type, w2.type_label, w2.type_description, w2.type_label_en, w2.type_description_en, w2.id_category, w2.category_label, w2.category_description, w2.category_label_en, w2.category_description_en from w2 union all
					select w6.id_type, w6.type_label, w6.type_description, w6.type_label_en, w6.type_description_en, w6.id_category, w6.category_label, w6.category_description, w6.category_label_en, w6.category_description_en from w6
					)
			,w8 as	(
					select
							$1 as export_connection,
							w7.id_type as area_domain,
							w7.type_label,
							w7.type_description,
							w7.type_label_en,
							w7.type_description_en,
							w7.id_category as area_domain_category,
							w7.category_label,
							w7.category_description,
							w7.category_label_en,
							w7.category_description_en							
					from
							w7
					)
			,w9 as	(
					select
							w8.export_connection,
							w8.area_domain,
							w8.type_label,
							w8.type_description,
							w8.type_label_en,
							w8.type_description_en,
							w8.area_domain_category,
							w8.category_label,
							w8.category_description,
							w8.category_label_en,
							w8.category_description_en,								
							t.etl_id,
							t.id as id_t_etl_ad
					from
							w8 left join target_data.t_etl_area_domain as t
					on
							w8.export_connection = t.export_connection
					and	
							target_data.fn_etl_array_compare(w8.area_domain,t.area_domain)
					order
							by w8.area_domain, w8.area_domain_category
					)
			,w10 as (
					select 
							a.*,
							b.category_json
					from
							(select distinct w9.export_connection, w9.area_domain, w9. type_label, w9.type_description, w9.type_label_en, w9.type_description_en, w9.etl_id, w9.id_t_etl_ad from w9) as a
					inner join
							(
							select
									w9.area_domain,
									json_agg(json_build_object(''area_domain_category'',w9.area_domain_category,''label_en_category'',w9.category_label_en)) as category_json
							from
									w9 group by w9.area_domain
							) as b
					on
							a.area_domain = b.area_domain
					order
							by a.area_domain
					)
			,w11 as (
					select
							(row_number() over ())::integer as id_order,
							w10.export_connection,
							w10.area_domain,
							w10.type_label,
							w10.type_description,
							w10.type_label_en,
							w10.type_description_en,							
							w10.etl_id,
							w10.id_t_etl_ad,
							w10.category_json,
							case when array_length(w10.area_domain,1) = 1 then true else false end as atomic
					from
							w10 where '|| _cond ||'
					)
			----------------------------------------
			,w12 as	(
					select $3 as res
					)
			,w13 as	(
					select json_array_elements(w12.res) as auto_information from w12
					)
			,w14 as	(
					select
							(w13.auto_information->>''id'')::integer as id,
							(w13.auto_information->>''auto_transfer'')::boolean as auto_transfer,
							(w13.auto_information->>''auto_pair'')::boolean as auto_pair
					from
							w13
					)
			----------------------------------------
			,w15 as	(
					select
							w11.*,
							w14.auto_transfer,
							w14.auto_pair
					from
							w11 inner join w14 on w11.id_order = w14.id
					)
			select
					w15.id_order,
					w15.export_connection,
					w15.area_domain,
					w15.type_label,
					w15.type_description,
					w15.type_label_en,
					w15.type_description_en,
					w15.etl_id,
					w15.id_t_etl_ad,
					w15.atomic,
					w15.auto_transfer,
					w15.auto_pair,
					case when (w15.auto_transfer = true or w15.auto_pair = true) then true else false end as auto
			from
					w15 order by w15.id_order
			' using _export_connection, _categorization_setups, _auto_information;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domain(integer[], integer, json, boolean) is
'Function returns records for ETL c_area_domain table.';

grant execute on function target_data.fn_etl_get_area_domain(integer[], integer, json, boolean) to public;
-- </function>



-- <function name="fn_etl_get_sub_population" schema="target_data" src="functions/etl/fn_etl_get_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_sub_population(integer[], integer, json, boolean) CASCADE;

create or replace function target_data.fn_etl_get_sub_population
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_auto_information			json,
	_etl						boolean default null::boolean
)
returns table
(
	id							integer,
	export_connection			integer,
	sub_population					integer[],
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	etl_id						integer,
	id_t_etl_sp					integer,
	atomic						boolean,
	auto_transfer				boolean,
	auto_pair					boolean,
	auto						boolean
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
		_cond						text;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;

		if _auto_information is null
		then
			raise exception 'Error 03: fn_etl_get_sub_population: Input argument _auto_information must not be NULL!';
		end if;			

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;		

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
			
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.spc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.spc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			return query
			with w as	(
						select
								0 as id,
								0 as export_connection,
								array[0] as sub_population,
								''::varchar as label,
								''::text as description,
								''::varchar as label_en,
								''::text as description_en,
								0 as etl_id,
								0 as id_t_etl_sp,
								null::boolean as atomic,
								null::boolean as auto_transfer,
								null::boolean as auto_pair,
								null::boolean as auto
						)
			select w.id, w.export_connection, w.sub_population, w.label, w.description, w.label_en,
			w.description_en, w.etl_id, w.id_t_etl_sp, w.atomic, w.auto_transfer, w.auto_pair, w.auto
			from w where w.id is distinct from 0;		
		else
			if _etl is null
			then
				_cond := 'TRUE'::text;
			else
				if _etl = true
				then
					_cond := 'w10.etl_id is not null'::text;
				else
					_cond := 'w10.etl_id is null'::text;
				end if;
			end if;
		
			return query execute
			'
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.spc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).label as type_label,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).description as type_description,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).label_en as type_label_en,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).description_en as type_description_en,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).id_category,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).label as category_label,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).description as category_description,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).label_en as category_label_en,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).description_en as category_description_en
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest($2))								
														)
					and
							t.spc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.type_label, w1.type_description, w1.type_label_en, w1.type_description_en, w1.id_category, w1.category_label, w1.category_description, w1.category_label_en, w1.category_description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select array[csp.id] as id_type, csp.label as type_label, csp.description as type_description, csp.label_en as  type_label_en, csp.description_en as type_description_en
					from target_data.c_sub_population as csp
					where csp.id in (select w4.id_type[1] from w4)
					)
			,w6 as (
					select
							w5.*,
							array[t.id] as id_category,
							t.label as category_label,
							t.description as category_description,
							t.label_en as category_label_en,
							t.description_en as category_description_en
					from
							w5
					inner join
							(
							select cspc.* from target_data.c_sub_population_category as cspc
							where cspc.sub_population in (select w5.id_type[1] from w5)
							) as t 
					on
							w5.id_type[1] = t.sub_population
					)			
			,w7 as	(
					select w2.id_type, w2.type_label, w2.type_description, w2.type_label_en, w2.type_description_en, w2.id_category, w2.category_label, w2.category_description, w2.category_label_en, w2.category_description_en from w2 union all
					select w6.id_type, w6.type_label, w6.type_description, w6.type_label_en, w6.type_description_en, w6.id_category, w6.category_label, w6.category_description, w6.category_label_en, w6.category_description_en from w6
					)
			,w8 as	(
					select
							$1 as export_connection,
							w7.id_type as sub_population,
							w7.type_label,
							w7.type_description,
							w7.type_label_en,
							w7.type_description_en,
							w7.id_category as sub_population_category,
							w7.category_label,
							w7.category_description,
							w7.category_label_en,
							w7.category_description_en							
					from
							w7
					)
			,w9 as	(
					select
							w8.export_connection,
							w8.sub_population,
							w8.type_label,
							w8.type_description,
							w8.type_label_en,
							w8.type_description_en,
							w8.sub_population_category,
							w8.category_label,
							w8.category_description,
							w8.category_label_en,
							w8.category_description_en,								
							t.etl_id,
							t.id as id_t_etl_sp
					from
							w8 left join target_data.t_etl_sub_population as t
					on
							w8.export_connection = t.export_connection
					and	
							target_data.fn_etl_array_compare(w8.sub_population,t.sub_population)
					order
							by w8.sub_population, w8.sub_population_category
					)
			,w10 as (
					select 
							a.*,
							b.category_json
					from
							(select distinct w9.export_connection, w9.sub_population, w9. type_label, w9.type_description, w9.type_label_en, w9.type_description_en, w9.etl_id, w9.id_t_etl_sp from w9) as a
					inner join
							(
							select
									w9.sub_population,
									json_agg(json_build_object(''sub_population_category'',w9.sub_population_category,''label_en_category'',w9.category_label_en)) as category_json
							from
									w9 group by w9.sub_population
							) as b
					on
							a.sub_population = b.sub_population
					order
							by a.sub_population
					)
			,w11 as (
					select
							(row_number() over ())::integer as id_order,
							w10.export_connection,
							w10.sub_population,
							w10.type_label,
							w10.type_description,
							w10.type_label_en,
							w10.type_description_en,							
							w10.etl_id,
							w10.id_t_etl_sp,
							w10.category_json,
							case when array_length(w10.sub_population,1) = 1 then true else false end as atomic
					from
							w10 where '|| _cond ||'
					)
			----------------------------------------
			,w12 as	(
					select $3 as res
					)
			,w13 as	(
					select json_array_elements(w12.res) as auto_information from w12
					)
			,w14 as	(
					select
							(w13.auto_information->>''id'')::integer as id,
							(w13.auto_information->>''auto_transfer'')::boolean as auto_transfer,
							(w13.auto_information->>''auto_pair'')::boolean as auto_pair
					from
							w13
					)
			----------------------------------------
			,w15 as	(
					select
							w11.*,
							w14.auto_transfer,
							w14.auto_pair
					from
							w11 inner join w14 on w11.id_order = w14.id
					)
			select
					w15.id_order,
					w15.export_connection,
					w15.sub_population,
					w15.type_label,
					w15.type_description,
					w15.type_label_en,
					w15.type_description_en,
					w15.etl_id,
					w15.id_t_etl_sp,
					w15.atomic,
					w15.auto_transfer,
					w15.auto_pair,
					case when (w15.auto_transfer = true or w15.auto_pair = true) then true else false end as auto
			from
					w15 order by w15.id_order
			' using _export_connection, _categorization_setups, _auto_information;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_sub_population(integer[], integer, json, boolean) is
'Function returns records for ETL c_sub_population table.';

grant execute on function target_data.fn_etl_get_sub_population(integer[], integer, json, boolean) to public;
-- </function>



-- <function name="fn_etl_get_area_domain_category" schema="target_data" src="functions/etl/fn_etl_get_area_domain_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domain_category(integer[], integer, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_area_domain_category
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_area_domain				integer[]
)
returns table
(
	id							integer,
	area_domain					integer[],
	label_en_type				varchar,
	area_domain_category		integer[],
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	t_etl_area_domain__id		integer,
	t_etl_area_domain__etl_id	integer
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_category: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_category: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;	
	
		if _area_domain is null
		then
			raise exception 'Error 03: fn_etl_get_area_domain_category: Input argument _area_domain must not be NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable 
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;					

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------

		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.adc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type,
						(target_data.fn_get_category4classification_rule_id('adc',t.adc2classification_rule)).id_category
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.adc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type, w.id_category from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			raise exception 'Error 04: fn_etl_get_area_domain_category: If exists area_domain than must exists any area_domain_category!'; 		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.adc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label as label_type,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_en_type,
							(target_data.fn_get_category4classification_rule_id('adc',t.adc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.adc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label_type, w1.label_en_type, w1.id_category, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select
							a.id_type,
							a.label_type,
							a.label_en_type,
							array[b.id] as id_category,
							b.label,
							b.description,
							b.label_en,
							b.description_en
					from
							(
							select cad.id, array[cad.id] as id_type, cad.label as label_type, cad.label_en as label_en_type
							from target_data.c_area_domain as cad where cad.id in (select w4.id_type[1] from w4)
							) as a
					inner
					join	target_data.c_area_domain_category as b on a.id = b.area_domain
					)
			,w6 as	(
					select w2.id_type, w2.label_type, w2.label_en_type, w2.id_category, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label_type, w5.label_en_type, w5.id_category, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select
							(row_number() over ())::integer as id_order,
							w6.id_type,
							w6.label_en_type,
							w6.id_category as area_domain_category,
							w6.label,
							w6.description,
							w6.label_en,
							w6.description_en
					from
							w6 where target_data.fn_array_compare(w6.id_type,_area_domain)
					)
			select
					w7.id_order,
					w7.id_type as area_domain,
					w7.label_en_type,
					w7.area_domain_category,
					w7.label,
					w7.description,
					w7.label_en,
					w7.description_en,
					(select tead.id from target_data.t_etl_area_domain as tead where tead.export_connection = _export_connection and target_data.fn_array_compare(tead.area_domain,w7.id_type)) as t_etl_area_domain__id,
					(select tead.etl_id from target_data.t_etl_area_domain as tead where tead.export_connection = _export_connection and target_data.fn_array_compare(tead.area_domain,w7.id_type)) as t_etl_area_domain__etl_id
			from
					w7 order by w7.id_order;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domain_category(integer[], integer, integer[]) is
'Function returns records for ETL c_area_domain_category table for given area domain types.';

grant execute on function target_data.fn_etl_get_area_domain_category(integer[], integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_sub_population_category" schema="target_data" src="functions/etl/fn_etl_get_sub_population_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_sub_population_category(integer[], integer, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_sub_population_category
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_sub_population				integer[]
)
returns table
(
	id								integer,
	sub_population					integer[],
	label_en_type					varchar,
	sub_population_category			integer[],
	label							varchar,
	description						text,
	label_en						varchar,
	description_en					text,
	t_etl_sub_population__id		integer,
	t_etl_sub_population__etl_id	integer
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_category: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_category: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;	
	
		if _sub_population is null
		then
			raise exception 'Error 03: fn_etl_get_sub_population_category: Input argument _sub_population must not be NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable 
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;					

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------

		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.spc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type,
						(target_data.fn_get_category4classification_rule_id('spc',t.spc2classification_rule)).id_category
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.spc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type, w.id_category from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			raise exception 'Error 04: fn_etl_get_sub_population_category: If exists sub_population than must exists any sub_population_category!'; 		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.spc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label as label_type,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_en_type,
							(target_data.fn_get_category4classification_rule_id('spc',t.spc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.spc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label_type, w1.label_en_type, w1.id_category, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select
							a.id_type,
							a.label_type,
							a.label_en_type,
							array[b.id] as id_category,
							b.label,
							b.description,
							b.label_en,
							b.description_en
					from
							(
							select csp.id, array[csp.id] as id_type, csp.label as label_type, csp.label_en as label_en_type
							from target_data.c_sub_population as csp where csp.id in (select w4.id_type[1] from w4)
							) as a
					inner
					join	target_data.c_sub_population_category as b on a.id = b.sub_population
					)
			,w6 as	(
					select w2.id_type, w2.label_type, w2.label_en_type, w2.id_category, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label_type, w5.label_en_type, w5.id_category, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select
							(row_number() over ())::integer as id_order,
							w6.id_type,
							w6.label_en_type,
							w6.id_category as sub_population_category,
							w6.label,
							w6.description,
							w6.label_en,
							w6.description_en
					from
							w6 where target_data.fn_array_compare(w6.id_type,_sub_population)
					)
			select
					w7.id_order,
					w7.id_type as sub_population,
					w7.label_en_type,
					w7.sub_population_category,
					w7.label,
					w7.description,
					w7.label_en,
					w7.description_en,
					(select tesp.id from target_data.t_etl_sub_population as tesp where tesp.export_connection = _export_connection and target_data.fn_array_compare(tesp.sub_population,w7.id_type)) as t_etl_sub_population__id,
					(select tesp.etl_id from target_data.t_etl_sub_population as tesp where tesp.export_connection = _export_connection and target_data.fn_array_compare(tesp.sub_population,w7.id_type)) as t_etl_sub_population__etl_id
			from
					w7 order by w7.id_order;

		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_sub_population_category(integer[], integer, integer[]) is
'Function returns records for ETL c_sub_population_category table for given sub population types.';

grant execute on function target_data.fn_etl_get_sub_population_category(integer[], integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_area_domain_category_json" schema="target_data" src="functions/etl/fn_etl_get_area_domain_category_json.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_category_json
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domain_category_json(integer[], integer, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_area_domain_category_json
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_area_domain				integer[]
)
returns json
as
$$
declare
		_res json;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_category_json: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_category_json: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;	
	
		if _area_domain is null
		then
			raise exception 'Error 03: fn_etl_get_area_domain_category_json: Input argument _area_domain must not be NULL!';
		end if;
	
		with
		w1 as	(
				select
						id,
						area_domain,
						label_en_type,
						area_domain_category,
						label,
						description,
						label_en,
						description_en			
				from
						target_data.fn_etl_get_area_domain_category(_refyearset2panel_mapping,_id_t_etl_target_variable,_area_domain)
				)
		select
				json_agg(
				json_build_object
				(
				'id',w1.id,
				'label_en_type',w1.label_en_type,
				'area_domain_category',w1.area_domain_category,
				'label_en',w1.label_en
				)
				) from w1
		into _res;
	
		if _res is null
		then
			raise exception 'Error 04: fn_etl_get_area_domain_category_json: Output argument _res must not be NULL!';
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domain_category_json(integer[], integer, integer[]) is
'Function returns records for ETL c_area_domain_category table for given area domain types in JSON format.';

grant execute on function target_data.fn_etl_get_area_domain_category_json(integer[], integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_sub_population_category_json" schema="target_data" src="functions/etl/fn_etl_get_sub_population_category_json.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_category_json
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_sub_population_category_json(integer[], integer, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_sub_population_category_json
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_sub_population				integer[]
)
returns json
as
$$
declare
		_res json;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_category_json: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_category_json: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;	
	
		if _sub_population is null
		then
			raise exception 'Error 03: fn_etl_get_sub_population_category_json: Input argument _sub_population must not be NULL!';
		end if;
	
		with
		w1 as	(
				select
						id,
						sub_population,
						label_en_type,
						sub_population_category,
						label,
						description,
						label_en,
						description_en			
				from
						target_data.fn_etl_get_sub_population_category(_refyearset2panel_mapping,_id_t_etl_target_variable,_sub_population)				
				)
		select
				json_agg(
				json_build_object
				(
				'id',w1.id,
				'label_en_type',w1.label_en_type,
				'sub_population_category',w1.sub_population_category,
				'label_en',w1.label_en
				)
				) from w1
		into _res;
	
		if _res is null
		then
			raise exception 'Error 04: fn_etl_get_sub_population_category_json: Output argument _res must not be NULL!';
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_sub_population_category_json(integer[], integer, integer[]) is
'Function returns records for ETL c_sub_population_category table for given sub population types in JSON format.';

grant execute on function target_data.fn_etl_get_sub_population_category_json(integer[], integer, integer[]) to public;
-- </function>

