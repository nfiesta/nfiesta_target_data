--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


DROP FUNCTION target_data.fn_get_area_domain(integer) CASCADE;

-- <function name="fn_get_area_domain" schema="target_data" src="functions/fn_get_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_area_domain(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_area_domain(_id integer DEFAULT NULL::integer, classification_type integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
classification_type 	integer
)
AS
$$
BEGIN
	IF _classification_type IS NOT NULL
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_classification_type AS t1 WHERE t1.id = _classification_type)
		THEN RAISE EXCEPTION 'Given classification type does not exist in table c_classification_type (%)', _classification_type;
		END IF;
	END IF;

	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.classification_type
		FROM target_data.c_area_domain AS t1
		WHERE CASE WHEN _classification_type IS NOT NULL THEN t1.classification_type = _classification_type ELSE true END;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.classification_type
		FROM target_data.c_area_domain AS t1
		WHERE t1.id = _id AND CASE WHEN _classification_type IS NOT NULL THEN t1.classification_type = _classification_type ELSE true END;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_area_domain(integer, integer) IS
'Function returns records from c_area_domain table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_area_domain(integer, integer) TO public;

-- </function>


DROP FUNCTION target_data.fn_get_sub_population(integer) CASCADE;

-- <function name="fn_get_sub_population" schema="target_data" src="functions/fn_get_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_sub_population(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_sub_population(_id integer DEFAULT NULL::integer, _classification_type integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
classification_type	integer
)
AS
$$
BEGIN
	IF _classification_type IS NOT NULL
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_classification_type AS t1 WHERE t1.id = _classification_type)
		THEN RAISE EXCEPTION 'Given classification type does not exist in table c_classification_type (%)', _classification_type;
		END IF;
	END IF;

	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.classification_type
		FROM target_data.c_sub_population AS t1
		WHERE CASE WHEN _classification_type IS NOT NULL THEN t1.classification_type = _classification_type ELSE true END;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given subpopulation does not exist in table c_sub_population (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.classification_type
		FROM target_data.c_sub_population AS t1
		WHERE t1.id = _id AND CASE WHEN _classification_type IS NOT NULL THEN t1.classification_type = _classification_type ELSE true END;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_sub_population(integer, integer) IS
'Function returns records from c_sub_population table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_sub_population(integer, integer) TO public;

-- </function>

-- <function name="fn_get_classification_type" schema="target_data" src="functions/fn_get_classification_type.sql">
--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_type
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_type(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_classification_type(_target_variable integer DEFAULT NULL::integer)
RETURNS TABLE (
id		integer,
target_variable	integer,
label		character varying(200),
description	text
)
AS
$$
DECLARE
	_state_or_change integer;
BEGIN
	IF _target_variable IS NULL
	THEN
		RETURN QUERY
		SELECT DISTINCT t1.id, NULL::integer, t1.label, t1.description
		FROM target_data.c_classification_type AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
		THEN RAISE EXCEPTION 'Given target_variable does not exist in table c_target_variable (%)', _target_variable;
		END IF;

		_state_or_change := (SELECT state_or_change FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable);

		RETURN QUERY
		SELECT t1.id, _target_variable AS target_variable, t1.label, t1.description
		FROM target_data.c_classification_type AS t1
		WHERE 	CASE 
			WHEN _state_or_change = 100 THEN t1.id = 100 		-- status variable can be classified only by standard rules
			WHEN _state_or_change = 200 THEN t1.id IN (100,200)	-- change variable can be classified by both types
			WHEN _state_or_change = 300 THEN t1.id = 100		-- dynamic variable can be classified also by both types, but
			ELSE true						-- from its context change rules can't be created (since it is missing negative contributions)
			END;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_classification_type(integer) IS
'Function returns records from c_classification_type table, optionally for given target_variable.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_classification_type(integer) TO public;

-- </function>
