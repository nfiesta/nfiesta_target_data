--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- adjust of uniques
ALTER TABLE target_data.c_area_domain DROP CONSTRAINT c_area_domain_description_en_key;
ALTER TABLE target_data.c_area_domain DROP CONSTRAINT c_area_domain_label_en_key;
ALTER TABLE target_data.c_area_domain DROP CONSTRAINT ukey__c_area_domain__description;
ALTER TABLE target_data.c_area_domain DROP CONSTRAINT ukey__c_area_domain__label;

ALTER TABLE target_data.c_sub_population DROP CONSTRAINT c_sub_population_description_en_key;
ALTER TABLE target_data.c_sub_population DROP CONSTRAINT c_sub_population_label_en_key;
ALTER TABLE target_data.c_sub_population DROP CONSTRAINT ukey__c_sub_population__description;
ALTER TABLE target_data.c_sub_population DROP CONSTRAINT ukey__c_sub_population__label;

ALTER TABLE target_data.c_area_domain ADD CONSTRAINT ukey__c_area_domain__description_ct UNIQUE (description, classification_type);
ALTER TABLE target_data.c_area_domain ADD CONSTRAINT ukey__c_area_domain__label_ct UNIQUE (label, classification_type);
ALTER TABLE target_data.c_area_domain ADD CONSTRAINT ukey__c_area_domain__description_en_ct UNIQUE (description, classification_type);
ALTER TABLE target_data.c_area_domain ADD CONSTRAINT ukey__c_area_domain__label_en_ct UNIQUE (label, classification_type);

ALTER TABLE target_data.c_sub_population ADD CONSTRAINT ukey__c_sub_population__description_ct UNIQUE (description, classification_type);
ALTER TABLE target_data.c_sub_population ADD CONSTRAINT ukey__c_sub_population__label_ct UNIQUE (label, classification_type);
ALTER TABLE target_data.c_sub_population ADD CONSTRAINT ukey__c_sub_population__description_en_ct UNIQUE (description, classification_type);
ALTER TABLE target_data.c_sub_population ADD CONSTRAINT ukey__c_sub_population__label_en_ct UNIQUE (label, classification_type);

-- functions
DROP FUNCTION target_data.fn_get_area_domain(integer, integer) CASCADE;

-- <function name="fn_get_area_domain" schema="target_data" src="functions/fn_get_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_area_domain(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_area_domain(_id integer DEFAULT NULL::integer, _target_variable integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
classification_type 	integer
)
AS
$$
DECLARE
	_state_or_change integer;
BEGIN
	IF _target_variable IS NOT NULL
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
		THEN RAISE EXCEPTION 'Given classification type does not exist in table c_target_variable (%)', _target_variable;
		END IF;
	
		_state_or_change := (SELECT state_or_change FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable);
	END IF;

	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.classification_type
		FROM target_data.c_area_domain AS t1
		WHERE CASE WHEN _target_variable IS NOT NULL THEN 
			CASE 
			WHEN _state_or_change = 100 THEN t1.classification_type = 100 		-- status variable can be classified only by standard rules
			WHEN _state_or_change = 200 THEN t1.classification_type IN (100,200)	-- change variable can be classified by both types
			WHEN _state_or_change = 300 THEN t1.classification_type IN (100,200)	-- dynamic variable can be classified also by both types
			ELSE true
			END
		ELSE true END;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.classification_type
		FROM target_data.c_area_domain AS t1
		WHERE t1.id = _id AND 
		CASE WHEN _target_variable IS NOT NULL THEN 
			CASE 
			WHEN _state_or_change = 100 THEN t1.classification_type = 100 		-- status variable can be classified only by standard rules
			WHEN _state_or_change = 200 THEN t1.classification_type IN (100,200)	-- change variable can be classified by both types
			WHEN _state_or_change = 300 THEN t1.classification_type IN (100,200)	-- dynamic variable can be classified also by both types
			ELSE true
			END
		ELSE true END;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_area_domain(integer, integer) IS
'Function returns records from c_area_domain table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_area_domain(integer, integer) TO public;

-- </function>


DROP FUNCTION target_data.fn_get_sub_population(integer, integer) CASCADE;

-- <function name="fn_get_sub_population" schema="target_data" src="functions/fn_get_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_sub_population(integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_sub_population(_id integer DEFAULT NULL::integer, _target_variable integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
classification_type	integer
)
AS
$$
DECLARE
	_state_or_change integer;
BEGIN
	IF _target_variable IS NOT NULL
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
		THEN RAISE EXCEPTION 'Given classification type does not exist in table c_target_variable (%)', _target_variable;
		END IF;
	
		_state_or_change := (SELECT state_or_change FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable);
	END IF;
	
	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.classification_type
		FROM target_data.c_sub_population AS t1
		WHERE CASE WHEN _target_variable IS NOT NULL THEN 
			CASE 
			WHEN _state_or_change = 100 THEN t1.classification_type = 100 		-- status variable can be classified only by standard rules
			WHEN _state_or_change = 200 THEN t1.classification_type IN (100,200)	-- change variable can be classified by both types
			WHEN _state_or_change = 300 THEN t1.classification_type IN (100,200)	-- dynamic variable can be classified also by both types
			ELSE true
			END
		ELSE true END;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given subpopulation does not exist in table c_sub_population (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.classification_type
		FROM target_data.c_sub_population AS t1
		WHERE t1.id = _id AND 
		CASE WHEN _target_variable IS NOT NULL THEN 
			CASE 
			WHEN _state_or_change = 100 THEN t1.classification_type = 100 		-- status variable can be classified only by standard rules
			WHEN _state_or_change = 200 THEN t1.classification_type IN (100,200)	-- change variable can be classified by both types
			WHEN _state_or_change = 300 THEN t1.classification_type IN (100,200)	-- dynamic variable can be classified also by both types
			ELSE true
			END
		ELSE true END;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_sub_population(integer, integer) IS
'Function returns records from c_sub_population table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_sub_population(integer, integer) TO public;

-- </function>

