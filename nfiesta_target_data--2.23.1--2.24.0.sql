--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


-- <function name="fn_etl_get_area_domain_category" schema="target_data" src="functions/etl/fn_etl_get_area_domain_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domain_category(integer[], integer, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_area_domain_category
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_area_domain				integer[]
)
returns table
(
	id							integer,
	area_domain					integer[],
	label_en_type				varchar,
	area_domain_category		integer[],
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	t_etl_area_domain__id		integer,
	t_etl_area_domain__etl_id	integer
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_category: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_category: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;	
	
		if _area_domain is null
		then
			raise exception 'Error 03: fn_etl_get_area_domain_category: Input argument _area_domain must not be NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable 
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;					

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------

		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.adc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type,
						(target_data.fn_get_category4classification_rule_id('adc',t.adc2classification_rule)).id_category
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.adc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type, w.id_category from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			raise exception 'Error 04: fn_etl_get_area_domain_category: If exists area_domain than must exists any area_domain_category!'; 		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.adc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label as label_type,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_en_type,
							(target_data.fn_get_category4classification_rule_id('adc',t.adc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.adc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label_type, w1.label_en_type, w1.id_category, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select
							a.id_type,
							a.label_type,
							a.label_en_type,
							array[b.id] as id_category,
							b.label,
							b.description,
							b.label_en,
							b.description_en
					from
							(
							select cad.id, array[cad.id] as id_type, cad.label as label_type, cad.label_en as label_en_type
							from target_data.c_area_domain as cad where cad.id in (select w4.id_type[1] from w4)
							) as a
					inner
					join	target_data.c_area_domain_category as b on a.id = b.area_domain
					)
			,w6 as	(
					select w2.id_type, w2.label_type, w2.label_en_type, w2.id_category, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label_type, w5.label_en_type, w5.id_category, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select
							(row_number() over (order by w6.id_category))::integer as id_order,
							w6.id_type,
							w6.label_en_type,
							w6.id_category as area_domain_category,
							w6.label,
							w6.description,
							w6.label_en,
							w6.description_en
					from
							w6 where target_data.fn_array_compare(w6.id_type,_area_domain)
					)
			select
					w7.id_order,
					w7.id_type as area_domain,
					w7.label_en_type,
					w7.area_domain_category,
					w7.label,
					w7.description,
					w7.label_en,
					w7.description_en,
					(select tead.id from target_data.t_etl_area_domain as tead where tead.export_connection = _export_connection and target_data.fn_array_compare(tead.area_domain,w7.id_type)) as t_etl_area_domain__id,
					(select tead.etl_id from target_data.t_etl_area_domain as tead where tead.export_connection = _export_connection and target_data.fn_array_compare(tead.area_domain,w7.id_type)) as t_etl_area_domain__etl_id
			from
					w7 order by w7.id_order;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domain_category(integer[], integer, integer[]) is
'Function returns records for ETL c_area_domain_category table for given area domain types.';

grant execute on function target_data.fn_etl_get_area_domain_category(integer[], integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_sub_population_category" schema="target_data" src="functions/etl/fn_etl_get_sub_population_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_sub_population_category(integer[], integer, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_sub_population_category
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_sub_population				integer[]
)
returns table
(
	id								integer,
	sub_population					integer[],
	label_en_type					varchar,
	sub_population_category			integer[],
	label							varchar,
	description						text,
	label_en						varchar,
	description_en					text,
	t_etl_sub_population__id		integer,
	t_etl_sub_population__etl_id	integer
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_category: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_category: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;	
	
		if _sub_population is null
		then
			raise exception 'Error 03: fn_etl_get_sub_population_category: Input argument _sub_population must not be NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable 
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;					

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------

		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.spc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type,
						(target_data.fn_get_category4classification_rule_id('spc',t.spc2classification_rule)).id_category
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.spc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type, w.id_category from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			raise exception 'Error 04: fn_etl_get_sub_population_category: If exists sub_population than must exists any sub_population_category!'; 		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.spc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label as label_type,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_en_type,
							(target_data.fn_get_category4classification_rule_id('spc',t.spc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.spc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label_type, w1.label_en_type, w1.id_category, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select
							a.id_type,
							a.label_type,
							a.label_en_type,
							array[b.id] as id_category,
							b.label,
							b.description,
							b.label_en,
							b.description_en
					from
							(
							select csp.id, array[csp.id] as id_type, csp.label as label_type, csp.label_en as label_en_type
							from target_data.c_sub_population as csp where csp.id in (select w4.id_type[1] from w4)
							) as a
					inner
					join	target_data.c_sub_population_category as b on a.id = b.sub_population
					)
			,w6 as	(
					select w2.id_type, w2.label_type, w2.label_en_type, w2.id_category, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label_type, w5.label_en_type, w5.id_category, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select
							(row_number() over (order by w6.id_category))::integer as id_order,
							w6.id_type,
							w6.label_en_type,
							w6.id_category as sub_population_category,
							w6.label,
							w6.description,
							w6.label_en,
							w6.description_en
					from
							w6 where target_data.fn_array_compare(w6.id_type,_sub_population)
					)
			select
					w7.id_order,
					w7.id_type as sub_population,
					w7.label_en_type,
					w7.sub_population_category,
					w7.label,
					w7.description,
					w7.label_en,
					w7.description_en,
					(select tesp.id from target_data.t_etl_sub_population as tesp where tesp.export_connection = _export_connection and target_data.fn_array_compare(tesp.sub_population,w7.id_type)) as t_etl_sub_population__id,
					(select tesp.etl_id from target_data.t_etl_sub_population as tesp where tesp.export_connection = _export_connection and target_data.fn_array_compare(tesp.sub_population,w7.id_type)) as t_etl_sub_population__etl_id
			from
					w7 order by w7.id_order;

		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_sub_population_category(integer[], integer, integer[]) is
'Function returns records for ETL c_sub_population_category table for given sub population types.';

grant execute on function target_data.fn_etl_get_sub_population_category(integer[], integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_area_domain_category_json" schema="target_data" src="functions/etl/fn_etl_get_area_domain_category_json.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_area_domain_category_json
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_area_domain_category_json(integer[], integer, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_area_domain_category_json
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_area_domain				integer[]
)
returns json
as
$$
declare
		_res json;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_area_domain_category_json: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_area_domain_category_json: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;	
	
		if _area_domain is null
		then
			raise exception 'Error 03: fn_etl_get_area_domain_category_json: Input argument _area_domain must not be NULL!';
		end if;
	
		with
		w1 as	(
				select
						id,
						area_domain,
						label_en_type,
						area_domain_category,
						label,
						description,
						label_en,
						description_en			
				from
						target_data.fn_etl_get_area_domain_category(_refyearset2panel_mapping,_id_t_etl_target_variable,_area_domain)
				)
		select
				json_agg(
				json_build_object
				(
				'id',w1.id,
				'label_en_type',w1.label_en_type,
				'area_domain_category',w1.area_domain_category,
				'label_en',w1.label_en,
				'label',w1.label
				)
				) from w1
		into _res;
	
		if _res is null
		then
			raise exception 'Error 04: fn_etl_get_area_domain_category_json: Output argument _res must not be NULL!';
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_area_domain_category_json(integer[], integer, integer[]) is
'Function returns records for ETL c_area_domain_category table for given area domain types in JSON format.';

grant execute on function target_data.fn_etl_get_area_domain_category_json(integer[], integer, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_sub_population_category_json" schema="target_data" src="functions/etl/fn_etl_get_sub_population_category_json.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_sub_population_category_json
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_sub_population_category_json(integer[], integer, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_sub_population_category_json
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_sub_population				integer[]
)
returns json
as
$$
declare
		_res json;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_sub_population_category_json: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_sub_population_category_json: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;	
	
		if _sub_population is null
		then
			raise exception 'Error 03: fn_etl_get_sub_population_category_json: Input argument _sub_population must not be NULL!';
		end if;
	
		with
		w1 as	(
				select
						id,
						sub_population,
						label_en_type,
						sub_population_category,
						label,
						description,
						label_en,
						description_en			
				from
						target_data.fn_etl_get_sub_population_category(_refyearset2panel_mapping,_id_t_etl_target_variable,_sub_population)				
				)
		select
				json_agg(
				json_build_object
				(
				'id',w1.id,
				'label_en_type',w1.label_en_type,
				'sub_population_category',w1.sub_population_category,
				'label_en',w1.label_en,
				'label',w1.label
				)
				) from w1
		into _res;
	
		if _res is null
		then
			raise exception 'Error 04: fn_etl_get_sub_population_category_json: Output argument _res must not be NULL!';
		end if;
	
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_sub_population_category_json(integer[], integer, integer[]) is
'Function returns records for ETL c_sub_population_category table for given sub population types in JSON format.';

grant execute on function target_data.fn_etl_get_sub_population_category_json(integer[], integer, integer[]) to public;
-- </function>
