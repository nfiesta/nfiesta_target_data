--
-- Copyright 2025 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <view name="v_variable_hierarchy_internal" schema="target_data" src="views/v_variable_hierarchy.sql">
--
-- Copyright 2017, 2025 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--alter extension nfiesta_target_data drop view target_data.v_variable_hierarchy;
DROP MATERIALIZED VIEW target_data.v_variable_hierarchy_internal CASCADE;

CREATE MATERIALIZED VIEW target_data.v_variable_hierarchy_internal AS
with recursive w_categorization_setups_start AS (
		select
		t_categorization_setup.id as cs_id,
		c_target_variable.id as tv_id,
		max(spc_id) as spc_id, max(sp_id) as sp_id, max(adc_id) as adc_id, max(ad_id) as ad_id,
		max(c_target_variable.label) as tv_label, max(spc_label) as spc_label, max(sp_label) as sp_label, max(adc_label) as adc_label, max(ad_label) as ad_label
	from target_data.t_categorization_setup
	inner join target_data.cm_ldsity2target2categorization_setup ON cm_ldsity2target2categorization_setup.categorization_setup = t_categorization_setup.id
	inner join target_data.cm_ldsity2target_variable ON cm_ldsity2target_variable.id = cm_ldsity2target2categorization_setup.ldsity2target_variable
	inner join target_data.c_target_variable ON c_target_variable.id = cm_ldsity2target_variable.target_variable
	, lateral (	select 
					array_agg(c_sub_population_category.label order by spc2clr.n) as spc_label,
					array_agg(c_sub_population.label order by spc2clr.n) as sp_label,
					array_agg(c_sub_population_category.id order by spc2clr.n) as spc_id,
					array_agg(c_sub_population.id order by spc2clr.n) as sp_id
				from unnest(cm_ldsity2target2categorization_setup.spc2classification_rule) with ordinality as spc2clr(spc2classification_rule, n)
				inner join target_data.cm_spc2classification_rule on (spc2clr.spc2classification_rule = cm_spc2classification_rule.id)
				inner join target_data.c_sub_population_category ON c_sub_population_category.id = cm_spc2classification_rule.sub_population_category
				inner join target_data.c_sub_population ON c_sub_population.id = c_sub_population_category.sub_population
			  ) as spc
	, lateral (	select 
					array_agg(c_area_domain_category.label order by adc2clr.n) as adc_label,
					array_agg(c_area_domain.label order by adc2clr.n) as ad_label,
					array_agg(c_area_domain_category.id order by adc2clr.n) as adc_id,
					array_agg(c_area_domain.id order by adc2clr.n) as ad_id
				from unnest(cm_ldsity2target2categorization_setup.adc2classification_rule) with ordinality as adc2clr(adc2classification_rule, n)
				inner join target_data.cm_adc2classification_rule on (adc2clr.adc2classification_rule = cm_adc2classification_rule.id)
				inner join target_data.c_area_domain_category ON c_area_domain_category.id = cm_adc2classification_rule.area_domain_category
				inner join target_data.c_area_domain ON c_area_domain.id = c_area_domain_category.area_domain
			  ) as adc
	group by 	t_categorization_setup.id, c_target_variable.id
	order by t_categorization_setup.id
),
w_categorization_setups AS (
	SELECT 	cs_id, tv_id, adc_id, ad_id, spc_id, sp_id, tv_label, spc_label, sp_label, adc_label, ad_label,
		adc_id is null AS adc_is_null, spc_id is null AS spc_is_null,
		coalesce(array_length(adc_id,1),0) AS adc_id_length,
		coalesce(array_length(spc_id,1),0) AS spc_id_length
	FROM
		w_categorization_setups_start
)
, w_cat_sup_init AS (
	select 
		cs_id, tv_id, adc_id, spc_id, adc_is_null, spc_is_null,
		ad_id, sp_id, tv_label, ad_label, adc_label, sp_label, spc_label,
		adc_id_length
	from w_categorization_setups
	where 
		--spc_id_length = 0 AND 
		adc_id_length = 0
)
--select * from w_cat_sup_init;
,w_cat_sup_init2 AS (
	select 
		cs_id, tv_id, adc_id, spc_id, adc_is_null, spc_is_null,
		ad_id, sp_id, tv_label, ad_label, adc_label, sp_label, spc_label,
		adc_id_length
	from w_categorization_setups
	where 
		--spc_id_length = 0 AND 
		adc_id_length = 1
),
w_cat_sup as (
	select 
		cs_sup.cs_id, cs_inf.cs_id AS inf_cs_id, cs_sup.tv_id, cs_sup.spc_id, cs_sup.spc_is_null, 
		cs_sup.adc_id,
		cs_sup.adc_id_length, 
		cs_inf.adc_id AS inf_adc_id, 
		cs_inf.adc_id_length AS inf_adc_id_length,
		cs_sup.ad_label AS ad_label_sup, cs_inf.ad_label AS ad_label_inf, cs_sup.adc_label AS adc_label_sup, cs_inf.adc_label AS adc_label_inf,
		cs_sup.sp_label AS sp_label_sup, cs_inf.sp_label AS sp_label_inf, cs_sup.spc_label AS spc_label_sup, cs_inf.spc_label AS spc_label_inf,
		1 AS iter
	from w_cat_sup_init AS cs_sup
	INNER JOIN w_cat_sup_init2 AS cs_inf
	ON
		cs_sup.tv_id = cs_inf.tv_id AND
		(cs_sup.spc_id = cs_inf.spc_id  OR (cs_sup.spc_is_null AND cs_inf.spc_is_null))
	UNION ALL
	select 
		cs_sup.inf_cs_id, cs_inf.cs_id, cs_sup.tv_id, cs_sup.spc_id, cs_sup.spc_is_null, 
		cs_sup.inf_adc_id AS adc_id, 
		cs_sup.inf_adc_id_length AS adc_id_length, 
		cs_inf.adc_id AS inf_adc_id, 
		cs_inf.adc_id_length AS inf_adc_id_length,
		cs_sup.ad_label_sup, cs_inf.ad_label AS ad_label_inf, cs_sup.adc_label_sup, cs_inf.adc_label AS adc_label_inf,
		cs_sup.sp_label_sup, cs_inf.sp_label AS sp_label_inf, cs_sup.spc_label_sup, cs_inf.spc_label AS spc_label_inf,
		iter + 1 AS iter
	from 
		(SELECT distinct inf_cs_id, tv_id, spc_id, spc_is_null,
			inf_adc_id, inf_adc_id_length, 
			ad_label_sup, adc_label_sup,
			sp_label_sup, spc_label_sup,
			iter
		FROM w_cat_sup) AS cs_sup
	INNER JOIN
		w_categorization_setups AS cs_inf
	ON 
		cs_sup.tv_id = cs_inf.tv_id AND
		(cs_sup.spc_id = cs_inf.spc_id  OR (cs_sup.spc_is_null AND cs_inf.spc_is_null)) AND
		(cs_sup.inf_adc_id != cs_inf.adc_id AND cs_sup.inf_adc_id <@ cs_inf.adc_id)
	WHERE (cs_inf.adc_id_length = cs_sup.inf_adc_id_length + 1)

)
,w_cat_sup_sp_init AS (
	select 
		cs_id, tv_id, adc_id, spc_id, adc_is_null, spc_is_null,
		ad_id, sp_id, tv_label, ad_label, adc_label, sp_label, spc_label,
		spc_id_length
	from w_categorization_setups
	where 
		--spc_id_length = 0 AND 
		spc_id_length = 0
),
w_cat_sup_sp_init2 AS (
	select 
		cs_id, tv_id, adc_id, spc_id, adc_is_null, spc_is_null,
		ad_id, sp_id, tv_label, ad_label, adc_label, sp_label, spc_label,
		spc_id_length
	from w_categorization_setups
	where 
		--spc_id_length = 0 AND 
		spc_id_length = 1
),
w_cat_sup_sp as (
	select 
		cs_sup.cs_id, cs_inf.cs_id AS inf_cs_id, cs_sup.tv_id, cs_sup.adc_id, cs_sup.adc_is_null, 
		cs_sup.spc_id,
		cs_sup.spc_id_length, 
		cs_inf.spc_id AS inf_spc_id, 
		cs_inf.spc_id_length AS inf_spc_id_length,
 		cs_sup.ad_label AS ad_label_sup, cs_inf.ad_label AS ad_label_inf, cs_sup.adc_label AS adc_label_sup, cs_inf.adc_label AS adc_label_inf,
		cs_sup.sp_label AS sp_label_sup, cs_inf.sp_label AS sp_label_inf, cs_sup.spc_label AS spc_label_sup, cs_inf.spc_label AS spc_label_inf,
		1 AS iter
	from w_cat_sup_sp_init AS cs_sup
	INNER JOIN w_cat_sup_sp_init2 AS cs_inf
	ON
		cs_sup.tv_id = cs_inf.tv_id AND
		(cs_sup.adc_id = cs_inf.adc_id  OR (cs_sup.adc_is_null AND cs_inf.adc_is_null))
	UNION ALL
	select 
		cs_sup.inf_cs_id, cs_inf.cs_id, cs_sup.tv_id, cs_sup.adc_id, cs_sup.adc_is_null, 
		cs_sup.inf_spc_id AS spc_id, 
		cs_sup.inf_spc_id_length AS spc_id_length, 
		cs_inf.spc_id AS inf_spc_id, 
		cs_inf.spc_id_length AS inf_spc_id_length,
		cs_sup.ad_label_sup, cs_inf.ad_label AS ad_label_inf, cs_sup.adc_label_sup, cs_inf.adc_label AS adc_label_inf,
		cs_sup.sp_label_sup, cs_inf.sp_label AS sp_label_inf, cs_sup.spc_label_sup, cs_inf.spc_label AS spc_label_inf,

		iter + 1 AS iter
	from 
		(SELECT distinct inf_cs_id, tv_id, adc_id, adc_is_null,
			inf_spc_id, inf_spc_id_length, 
			ad_label_sup, adc_label_sup,
			sp_label_sup, spc_label_sup,
			iter
		FROM w_cat_sup_sp) AS cs_sup
	INNER JOIN
		w_categorization_setups AS cs_inf
	ON 
		cs_sup.tv_id = cs_inf.tv_id AND
		(cs_sup.adc_id = cs_inf.adc_id  OR (cs_sup.adc_is_null AND cs_inf.adc_is_null)) AND
		(cs_sup.inf_spc_id != cs_inf.spc_id AND cs_sup.inf_spc_id <@ cs_inf.spc_id)
	WHERE (cs_inf.spc_id_length = cs_sup.inf_spc_id_length + 1)

)
SELECT 	cs_id AS cs_id_sup, inf_cs_id AS cs_id_inf,
	tv_id,
	adc_label_sup,
	ad_label_inf,
	array_to_string(adc_label_inf, '~') AS adc_label_inf,
	spc_label_sup,
	sp_label_inf,
	array_to_string(spc_label_inf, '~') AS spc_label_inf
FROM w_cat_sup
UNION ALL
SELECT 	cs_id AS cs_id_sup, inf_cs_id AS cs_id_inf,
	tv_id,
 	adc_label_sup,
	ad_label_inf,
	array_to_string(adc_label_inf, '~') AS adc_label_inf,
	spc_label_sup,
	sp_label_inf,
	array_to_string(spc_label_inf, '~') AS spc_label_inf
FROM w_cat_sup_sp;

ALTER TABLE target_data.v_variable_hierarchy_internal OWNER TO app_nfiesta;
GRANT SELECT ON TABLE 				target_data.v_variable_hierarchy_internal TO PUBLIC;
GRANT SELECT ON TABLE				target_data.v_variable_hierarchy_internal TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	target_data.v_variable_hierarchy_internal TO app_nfiesta_mng;

---------------------------------------

--DROP VIEW target_data.v_variable_hierarchy;
CREATE VIEW target_data.v_variable_hierarchy AS
	with w_data as (
		select
			tad_sup.panel,
			tad_sup.reference_year_set,
			cs_id_sup, spc_label_sup, adc_label_sup,
			cs_id_inf, sp_label_inf, ad_label_inf, spc_label_inf, adc_label_inf
		from target_data.v_variable_hierarchy_internal
		inner join target_data.t_available_datasets as tad_sup on (tad_sup.categorization_setup = v_variable_hierarchy_internal.cs_id_sup)
		inner join target_data.t_available_datasets as tad_inf on (tad_inf.categorization_setup = v_variable_hierarchy_internal.cs_id_inf)
		where ((tad_sup.panel = tad_inf.panel) and (tad_sup.reference_year_set = tad_inf.reference_year_set))
	)
	select
		panel,
		reference_year_set,
		cs_id_sup as node, 
		array_agg(cs_id_inf 		order by cs_id_inf) AS edges,
		spc_label_sup, adc_label_sup,
		array_agg(spc_label_inf 	order by cs_id_inf) AS spc_label_inf,
		array_agg(adc_label_inf		order by cs_id_inf) AS adc_label_inf
	from w_data
	group by panel, reference_year_set, cs_id_sup, spc_label_sup, adc_label_sup, sp_label_inf, ad_label_inf
	order by panel, reference_year_set, cs_id_sup
;

ALTER TABLE target_data.v_variable_hierarchy OWNER TO app_nfiesta;
GRANT SELECT ON TABLE 				target_data.v_variable_hierarchy TO PUBLIC;
GRANT SELECT ON TABLE				target_data.v_variable_hierarchy TO app_nfiesta;
GRANT SELECT ON TABLE	 			target_data.v_variable_hierarchy TO app_nfiesta_mng;

--------------------------------------automatic view refresh (https://stackoverflow.com/a/29447328)

CREATE OR REPLACE FUNCTION target_data.tg_refresh_v_variable_hierarchy()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW /*CONCURRENTLY */target_data.v_variable_hierarchy_internal;
    RETURN NULL;
END;
$$;

--target_data.t_categorization_setup
drop trigger if exists trg__t_categorization_setup__refresh_v_variable_hierarchy ON target_data.t_categorization_setup;
CREATE TRIGGER trg__t_categorization_setup__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.t_categorization_setup
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_ldsity2target2categorization_setup
drop trigger if exists trg__cm_ldsity2target2categorization_setup__refresh_v_variable_hierarchy ON target_data.cm_ldsity2target2categorization_setup;
CREATE TRIGGER trg__cm_ldsity2target2categorization_setup__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_ldsity2target2categorization_setup
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_ldsity2target_variable
drop trigger if exists trg__cm_ldsity2target_variable__refresh_v_variable_hierarchy ON target_data.cm_ldsity2target_variable;
CREATE TRIGGER trg__cm_ldsity2target_variable__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_ldsity2target_variable
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_target_variable
drop trigger if exists trg__c_target_variable__refresh_v_variable_hierarchy ON target_data.c_target_variable;
CREATE TRIGGER trg__c_target_variable__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_target_variable
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_spc2classification_rule
drop trigger if exists trg__cm_spc2classification_rule__refresh_v_variable_hierarchy ON target_data.cm_spc2classification_rule;
CREATE TRIGGER trg__cm_spc2classification_rule__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_spc2classification_rule
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_sub_population_category
drop trigger if exists trg__c_sub_population_category__refresh_v_variable_hierarchy ON target_data.c_sub_population_category;
CREATE TRIGGER trg__c_sub_population_category__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_sub_population_category
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_sub_population
drop trigger if exists trg__c_sub_population__refresh_v_variable_hierarchy ON target_data.c_sub_population;
CREATE TRIGGER trg__c_sub_population__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_sub_population
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_adc2classification_rule
drop trigger if exists trg__cm_adc2classification_rule__refresh_v_variable_hierarchy ON target_data.cm_adc2classification_rule;
CREATE TRIGGER trg__cm_adc2classification_rule__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_adc2classification_rule
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_area_domain_category
drop trigger if exists trg__c_area_domain_category__refresh_v_variable_hierarchy ON target_data.c_area_domain_category;
CREATE TRIGGER trg__c_area_domain_category__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_area_domain_category
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_area_domain
drop trigger if exists trg__c_area_domain__refresh_v_variable_hierarchy ON target_data.c_area_domain;
CREATE TRIGGER trg__c_area_domain__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_area_domain
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

-- </view>

