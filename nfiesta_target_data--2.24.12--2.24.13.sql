--
-- Copyright 2025 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <view name="v_variable_hierarchy_internal" schema="target_data" src="views/v_variable_hierarchy.sql">
--
-- Copyright 2017, 2025 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--alter extension nfiesta_target_data drop view target_data.v_variable_hierarchy;
DROP MATERIALIZED VIEW target_data.v_variable_hierarchy_internal CASCADE;

CREATE MATERIALIZED VIEW target_data.v_variable_hierarchy_internal AS
with recursive w_categorization_setups_start AS (
		select
		t_categorization_setup.id as cs_id,
		c_target_variable.id as tv_id,
		max(spc_id) as spc_id, max(sp_id) as sp_id, max(adc_id) as adc_id, max(ad_id) as ad_id,
		max(c_target_variable.label) as tv_label, max(spc_label) as spc_label, max(sp_label) as sp_label, max(adc_label) as adc_label, max(ad_label) as ad_label
	from target_data.t_categorization_setup
	inner join target_data.cm_ldsity2target2categorization_setup ON cm_ldsity2target2categorization_setup.categorization_setup = t_categorization_setup.id
	inner join target_data.cm_ldsity2target_variable ON cm_ldsity2target_variable.id = cm_ldsity2target2categorization_setup.ldsity2target_variable
	inner join target_data.c_target_variable ON c_target_variable.id = cm_ldsity2target_variable.target_variable
	, lateral (	select 
					array_agg(c_sub_population_category.label order by spc2clr.n) as spc_label,
					array_agg(c_sub_population.label order by spc2clr.n) as sp_label,
					array_agg(c_sub_population_category.id order by spc2clr.n) as spc_id,
					array_agg(c_sub_population.id order by spc2clr.n) as sp_id
				from unnest(cm_ldsity2target2categorization_setup.spc2classification_rule) with ordinality as spc2clr(spc2classification_rule, n)
				inner join target_data.cm_spc2classification_rule on (spc2clr.spc2classification_rule = cm_spc2classification_rule.id)
				inner join target_data.c_sub_population_category ON c_sub_population_category.id = cm_spc2classification_rule.sub_population_category
				inner join target_data.c_sub_population ON c_sub_population.id = c_sub_population_category.sub_population
			  ) as spc
	, lateral (	select 
					array_agg(c_area_domain_category.label order by adc2clr.n) as adc_label,
					array_agg(c_area_domain.label order by adc2clr.n) as ad_label,
					array_agg(c_area_domain_category.id order by adc2clr.n) as adc_id,
					array_agg(c_area_domain.id order by adc2clr.n) as ad_id
				from unnest(cm_ldsity2target2categorization_setup.adc2classification_rule) with ordinality as adc2clr(adc2classification_rule, n)
				inner join target_data.cm_adc2classification_rule on (adc2clr.adc2classification_rule = cm_adc2classification_rule.id)
				inner join target_data.c_area_domain_category ON c_area_domain_category.id = cm_adc2classification_rule.area_domain_category
				inner join target_data.c_area_domain ON c_area_domain.id = c_area_domain_category.area_domain
			  ) as adc
	group by 	t_categorization_setup.id, c_target_variable.id
	order by t_categorization_setup.id
),
w_categorization_setups AS (
	SELECT 	cs_id, tv_id, adc_id, ad_id, spc_id, sp_id, tv_label, spc_label, sp_label, adc_label, ad_label,
		adc_id is null AS adc_is_null, spc_id is null AS spc_is_null,
		coalesce(array_length(adc_id,1),0) AS adc_id_length,
		coalesce(array_length(spc_id,1),0) AS spc_id_length
	FROM
		w_categorization_setups_start
)
, w_cat_sup_init AS (
	select 
		cs_id, tv_id, adc_id, spc_id, adc_is_null, spc_is_null,
		ad_id, sp_id, tv_label, ad_label, adc_label, sp_label, spc_label,
		adc_id_length
	from w_categorization_setups
	where 
		--spc_id_length = 0 AND 
		adc_id_length = 0
)
--select * from w_cat_sup_init;
,w_cat_sup_init2 AS (
	select 
		cs_id, tv_id, adc_id, spc_id, adc_is_null, spc_is_null,
		ad_id, sp_id, tv_label, ad_label, adc_label, sp_label, spc_label,
		adc_id_length
	from w_categorization_setups
	where 
		--spc_id_length = 0 AND 
		adc_id_length = 1
),
w_cat_sup as (
	select 
		cs_sup.cs_id, cs_inf.cs_id AS inf_cs_id, cs_sup.tv_id, cs_sup.spc_id, cs_sup.spc_is_null, 
		cs_sup.adc_id,
		cs_sup.adc_id_length, 
		cs_inf.adc_id AS inf_adc_id, 
		cs_inf.adc_id_length AS inf_adc_id_length,
		cs_inf.ad_id AS ad_id_inf,
		cs_inf.sp_id AS sp_id_inf,
		cs_sup.ad_label AS ad_label_sup, cs_inf.ad_label AS ad_label_inf, cs_sup.adc_label AS adc_label_sup, cs_inf.adc_label AS adc_label_inf,
		cs_sup.sp_label AS sp_label_sup, cs_inf.sp_label AS sp_label_inf, cs_sup.spc_label AS spc_label_sup, cs_inf.spc_label AS spc_label_inf,
		1 AS iter
	from w_cat_sup_init AS cs_sup
	INNER JOIN w_cat_sup_init2 AS cs_inf
	ON
		cs_sup.tv_id = cs_inf.tv_id AND
		(cs_sup.spc_id = cs_inf.spc_id  OR (cs_sup.spc_is_null AND cs_inf.spc_is_null))
	UNION ALL
	select 
		cs_sup.inf_cs_id, cs_inf.cs_id, cs_sup.tv_id, cs_sup.spc_id, cs_sup.spc_is_null, 
		cs_sup.inf_adc_id AS adc_id, 
		cs_sup.inf_adc_id_length AS adc_id_length, 
		cs_inf.adc_id AS inf_adc_id, 
		cs_inf.adc_id_length AS inf_adc_id_length,
		cs_inf.ad_id AS ad_id_inf,
		cs_inf.sp_id AS sp_id_inf,
		cs_sup.ad_label_sup, cs_inf.ad_label AS ad_label_inf, cs_sup.adc_label_sup, cs_inf.adc_label AS adc_label_inf,
		cs_sup.sp_label_sup, cs_inf.sp_label AS sp_label_inf, cs_sup.spc_label_sup, cs_inf.spc_label AS spc_label_inf,
		iter + 1 AS iter
	from 
		(SELECT distinct inf_cs_id, tv_id, spc_id, spc_is_null,
			inf_adc_id, inf_adc_id_length, 
			ad_label_sup, adc_label_sup,
			sp_label_sup, spc_label_sup,
			iter
		FROM w_cat_sup) AS cs_sup
	INNER JOIN
		w_categorization_setups AS cs_inf
	ON 
		cs_sup.tv_id = cs_inf.tv_id AND
		(cs_sup.spc_id = cs_inf.spc_id  OR (cs_sup.spc_is_null AND cs_inf.spc_is_null)) AND
		(cs_sup.inf_adc_id != cs_inf.adc_id AND cs_sup.inf_adc_id <@ cs_inf.adc_id)
	WHERE (cs_inf.adc_id_length = cs_sup.inf_adc_id_length + 1)

)
,w_cat_sup_sp_init AS (
	select 
		cs_id, tv_id, adc_id, spc_id, adc_is_null, spc_is_null,
		ad_id, sp_id, tv_label, ad_label, adc_label, sp_label, spc_label,
		spc_id_length
	from w_categorization_setups
	where 
		--spc_id_length = 0 AND 
		spc_id_length = 0
),
w_cat_sup_sp_init2 AS (
	select 
		cs_id, tv_id, adc_id, spc_id, adc_is_null, spc_is_null,
		ad_id, sp_id, tv_label, ad_label, adc_label, sp_label, spc_label,
		spc_id_length
	from w_categorization_setups
	where 
		--spc_id_length = 0 AND 
		spc_id_length = 1
),
w_cat_sup_sp as (
	select 
		cs_sup.cs_id, cs_inf.cs_id AS inf_cs_id, cs_sup.tv_id, cs_sup.adc_id, cs_sup.adc_is_null, 
		cs_sup.spc_id,
		cs_sup.spc_id_length, 
		cs_inf.spc_id AS inf_spc_id, 
		cs_inf.spc_id_length AS inf_spc_id_length,
		cs_inf.ad_id AS ad_id_inf,
		cs_inf.sp_id AS sp_id_inf,
 		cs_sup.ad_label AS ad_label_sup, cs_inf.ad_label AS ad_label_inf, cs_sup.adc_label AS adc_label_sup, cs_inf.adc_label AS adc_label_inf,
		cs_sup.sp_label AS sp_label_sup, cs_inf.sp_label AS sp_label_inf, cs_sup.spc_label AS spc_label_sup, cs_inf.spc_label AS spc_label_inf,
		1 AS iter
	from w_cat_sup_sp_init AS cs_sup
	INNER JOIN w_cat_sup_sp_init2 AS cs_inf
	ON
		cs_sup.tv_id = cs_inf.tv_id AND
		(cs_sup.adc_id = cs_inf.adc_id  OR (cs_sup.adc_is_null AND cs_inf.adc_is_null))
	UNION ALL
	select 
		cs_sup.inf_cs_id, cs_inf.cs_id, cs_sup.tv_id, cs_sup.adc_id, cs_sup.adc_is_null, 
		cs_sup.inf_spc_id AS spc_id, 
		cs_sup.inf_spc_id_length AS spc_id_length, 
		cs_inf.spc_id AS inf_spc_id, 
		cs_inf.spc_id_length AS inf_spc_id_length,
		cs_inf.ad_id AS ad_id_inf,
		cs_inf.sp_id AS sp_id_inf,
		cs_sup.ad_label_sup, cs_inf.ad_label AS ad_label_inf, cs_sup.adc_label_sup, cs_inf.adc_label AS adc_label_inf,
		cs_sup.sp_label_sup, cs_inf.sp_label AS sp_label_inf, cs_sup.spc_label_sup, cs_inf.spc_label AS spc_label_inf,

		iter + 1 AS iter
	from 
		(SELECT distinct inf_cs_id, tv_id, adc_id, adc_is_null,
			inf_spc_id, inf_spc_id_length, 
			ad_label_sup, adc_label_sup,
			sp_label_sup, spc_label_sup,
			iter
		FROM w_cat_sup_sp) AS cs_sup
	INNER JOIN
		w_categorization_setups AS cs_inf
	ON 
		cs_sup.tv_id = cs_inf.tv_id AND
		(cs_sup.adc_id = cs_inf.adc_id  OR (cs_sup.adc_is_null AND cs_inf.adc_is_null)) AND
		(cs_sup.inf_spc_id != cs_inf.spc_id AND cs_sup.inf_spc_id <@ cs_inf.spc_id)
	WHERE (cs_inf.spc_id_length = cs_sup.inf_spc_id_length + 1)

)
SELECT 	cs_id AS cs_id_sup, inf_cs_id AS cs_id_inf,
	tv_id,
	adc_label_sup,
	coalesce(ad_id_inf,array[0]) AS ad_id_inf,
	ad_label_inf,
	array_to_string(adc_label_inf, '~') AS adc_label_inf,
	spc_label_sup,
	coalesce(sp_id_inf,array[0]) AS sp_id_inf,
	sp_label_inf,
	array_to_string(spc_label_inf, '~') AS spc_label_inf
FROM w_cat_sup
UNION ALL
SELECT 	cs_id AS cs_id_sup, inf_cs_id AS cs_id_inf,
	tv_id,
 	adc_label_sup,
	coalesce(ad_id_inf,array[0]) AS ad_id_inf,
	ad_label_inf,
	array_to_string(adc_label_inf, '~') AS adc_label_inf,
	spc_label_sup,
	coalesce(sp_id_inf,array[0]) AS sp_id_inf,
	sp_label_inf,
	array_to_string(spc_label_inf, '~') AS spc_label_inf
FROM w_cat_sup_sp;

ALTER TABLE target_data.v_variable_hierarchy_internal OWNER TO app_nfiesta;
GRANT SELECT ON TABLE 				target_data.v_variable_hierarchy_internal TO PUBLIC;
GRANT SELECT ON TABLE				target_data.v_variable_hierarchy_internal TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	target_data.v_variable_hierarchy_internal TO app_nfiesta_mng;

---------------------------------------

--DROP VIEW target_data.v_variable_hierarchy;
CREATE VIEW target_data.v_variable_hierarchy AS
	with w_data as (
		select
			tad_sup.panel,
			tad_sup.reference_year_set,
			cs_id_sup, spc_label_sup, adc_label_sup,
			cs_id_inf, sp_label_inf, ad_label_inf, spc_label_inf, adc_label_inf
		from target_data.v_variable_hierarchy_internal
		inner join target_data.t_available_datasets as tad_sup on (tad_sup.categorization_setup = v_variable_hierarchy_internal.cs_id_sup)
		inner join target_data.t_available_datasets as tad_inf on (tad_inf.categorization_setup = v_variable_hierarchy_internal.cs_id_inf)
		where ((tad_sup.panel = tad_inf.panel) and (tad_sup.reference_year_set = tad_inf.reference_year_set))
	)
	select
		panel,
		reference_year_set,
		cs_id_sup as node, 
		array_agg(cs_id_inf 		order by cs_id_inf) AS edges,
		spc_label_sup, adc_label_sup,
		array_agg(spc_label_inf 	order by cs_id_inf) AS spc_label_inf,
		array_agg(adc_label_inf		order by cs_id_inf) AS adc_label_inf
	from w_data
	group by panel, reference_year_set, cs_id_sup, spc_label_sup, adc_label_sup, sp_label_inf, ad_label_inf
	order by panel, reference_year_set, cs_id_sup
;

ALTER TABLE target_data.v_variable_hierarchy OWNER TO app_nfiesta;
GRANT SELECT ON TABLE 				target_data.v_variable_hierarchy TO PUBLIC;
GRANT SELECT ON TABLE				target_data.v_variable_hierarchy TO app_nfiesta;
GRANT SELECT ON TABLE	 			target_data.v_variable_hierarchy TO app_nfiesta_mng;

--------------------------------------automatic view refresh (https://stackoverflow.com/a/29447328)

CREATE OR REPLACE FUNCTION target_data.tg_refresh_v_variable_hierarchy()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW /*CONCURRENTLY */target_data.v_variable_hierarchy_internal;
    RETURN NULL;
END;
$$;

--target_data.t_categorization_setup
drop trigger if exists trg__t_categorization_setup__refresh_v_variable_hierarchy ON target_data.t_categorization_setup;
CREATE TRIGGER trg__t_categorization_setup__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.t_categorization_setup
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_ldsity2target2categorization_setup
drop trigger if exists trg__cm_ldsity2target2categorization_setup__refresh_v_variable_hierarchy ON target_data.cm_ldsity2target2categorization_setup;
CREATE TRIGGER trg__cm_ldsity2target2categorization_setup__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_ldsity2target2categorization_setup
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_ldsity2target_variable
drop trigger if exists trg__cm_ldsity2target_variable__refresh_v_variable_hierarchy ON target_data.cm_ldsity2target_variable;
CREATE TRIGGER trg__cm_ldsity2target_variable__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_ldsity2target_variable
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_target_variable
drop trigger if exists trg__c_target_variable__refresh_v_variable_hierarchy ON target_data.c_target_variable;
CREATE TRIGGER trg__c_target_variable__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_target_variable
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_spc2classification_rule
drop trigger if exists trg__cm_spc2classification_rule__refresh_v_variable_hierarchy ON target_data.cm_spc2classification_rule;
CREATE TRIGGER trg__cm_spc2classification_rule__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_spc2classification_rule
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_sub_population_category
drop trigger if exists trg__c_sub_population_category__refresh_v_variable_hierarchy ON target_data.c_sub_population_category;
CREATE TRIGGER trg__c_sub_population_category__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_sub_population_category
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_sub_population
drop trigger if exists trg__c_sub_population__refresh_v_variable_hierarchy ON target_data.c_sub_population;
CREATE TRIGGER trg__c_sub_population__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_sub_population
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_adc2classification_rule
drop trigger if exists trg__cm_adc2classification_rule__refresh_v_variable_hierarchy ON target_data.cm_adc2classification_rule;
CREATE TRIGGER trg__cm_adc2classification_rule__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_adc2classification_rule
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_area_domain_category
drop trigger if exists trg__c_area_domain_category__refresh_v_variable_hierarchy ON target_data.c_area_domain_category;
CREATE TRIGGER trg__c_area_domain_category__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_area_domain_category
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_area_domain
drop trigger if exists trg__c_area_domain__refresh_v_variable_hierarchy ON target_data.c_area_domain;
CREATE TRIGGER trg__c_area_domain__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_area_domain
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

-- </view>

-- <function name="fn_check_ldsity_values" schema="target_data" src="functions/fn_check_ldsity_values.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_check_ldsity_values
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_ldsity_values(integer[], integer[]);

CREATE OR REPLACE FUNCTION target_data.fn_check_ldsity_values(_available_datasets integer[], _plots integer[] DEFAULT NULL::int[]) RETURNS void AS $src$
DECLARE
	_cs int[];
	--_vars int[];
	_panels int[];
	_refyearsets int[];
	_available_datasets_dist int[];
	_errp json;
BEGIN
	--raise notice '%', _available_datasets;

	select array_agg(distinct id), array_agg(distinct categorization_setup), array_agg(distinct panel), array_agg(distinct reference_year_set) 
	from target_data.t_available_datasets 
	where t_available_datasets.id = ANY(_available_datasets)
	into _available_datasets_dist, _cs, _panels, _refyearsets;

--raise notice '%, %, %', _cs, _refyearsets, _available_datasets_dist;

	if _cs is not null
	then
		--raise notice '%		fn_check_ldsity_values -- UPDATE CHECK START -- select target_data.fn_add_plot_target_attr(%, %, %, %, %);', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', _vars, _plots, _refyearsets, 1e-6, true;
		with w_err as (
			select target_data.fn_add_plot_target_attr(_cs, _plots, _panels, _refyearsets, 1e-6, true) as fn_err
		)
		, w_err_t as (
			select (fn_err).* from w_err
		)
		, w_err_json as (
			select 
				json_build_object(
					'plot'					, plot,
					'panel'		, panel,
					'reference_year_set'	, reference_year_set,
					'variable'				, variable,
					'ldsity'				, ldsity,
					'ldsity_sum'			, ldsity_sum,
					'variables_def'			, variables_def,
					'variables_found'		, variables_found,
					'diff'					, diff
				) as errj
			from w_err_t
		)
		select json_agg(errj) into _errp from w_err_json;
		--raise notice '%		fn_check_ldsity_values -- UPDATE CHECK STOP', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague';
		IF 
			(json_array_length(_errp) > 0) 
			THEN RAISE EXCEPTION 'fn_check_ldsity_values -- plot level local densities are not additive:
				checked categorization setups: %, checked plots: %, 
				found err plots: 
				%', _cs, _plots, jsonb_pretty(_errp::jsonb);
		END IF;
	else
		RAISE WARNING 'fn_check_ldsity_values -- additivity check was skiped: 
				* 0 rows edited/all rows were deleted (next line displays NULL)
				* corresponding variable hierarchy not found (next line displays array)
				available_datasets: %', _available_datasets_dist;
	end if;
        RETURN;
END;
$src$ LANGUAGE plpgsql;

comment on function target_data.fn_check_ldsity_values IS 'Function for checking additivity. Launched by triggers on either t_ldsity_values or t_available_datasets. Triggers on insert and update on t_available_datasets dropped due to performance problem. Will be replaced by more advanced concept of additivity checks in https://gitlab.com/nfiesta/nfiesta_pg/-/issues/133.';


-- </function>

DROP FUNCTION target_data.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean);
-- <function name="fn_add_plot_target_attr" schema="target_data" src="functions/additivity/fn_add_plot_target_attr.sql">
--
-- Copyright 2017, 2025 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_add_plot_target_attr
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_add_plot_target_attr(integer[], integer[], integer[], integer[], double precision, boolean);

CREATE OR REPLACE FUNCTION target_data.fn_add_plot_target_attr
(
	IN variables integer[],
	IN plots integer[] default NULL,
	IN panels integer[] default NULL,
	IN refyearsets integer[] default NULL,
	IN min_diff double precision default 0.0,
	IN include_null_diff boolean default true
)
  RETURNS TABLE
(
	plot			integer,
	panel			integer,
	reference_year_set	integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision	
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text_v text;
	_array_text_p text;
	_array_text_r text;
	_array_text_pan text;

BEGIN
	--------------------------------QUERY--------------------------------
	--_array_text_v := concat(quote_literal(variables::text), '::integer[]');

	if plots is not null then
		_array_text_p := concat('AND t8.gid = ANY (', quote_literal(plots::text), '::integer[])');
	else
		_array_text_p := '';
	end if;

	if panels is not null then
		_array_text_pan := concat('AND t3.panel = ANY (', quote_literal(panels::text), '::integer[])');
	else
		_array_text_pan := '';
	end if;

	if refyearsets is not null then
		_array_text_r := concat(' AND t3.reference_year_set = ANY (', quote_literal(refyearsets::text), '::integer[])');
	else
		_array_text_r := '';
	end if;

	_complete_query :=
	'
	with
	w_cat_setups AS (
		SELECT DISTINCT cs_id_sup, ad_id_inf, sp_id_inf --, cs_id_inf
		FROM target_data.v_variable_hierarchy_internal
		WHERE 
			(array[cs_id_sup] <@ $1 OR
			array[cs_id_inf] <@ $1)
	),
	w_plot_var as materialized
					(
		SELECT
			t2.cs_id_sup AS node, 
			t2.cs_id_inf AS edge_def,
			t2.ad_id_inf, t2.sp_id_inf,
			t3.panel, 
			t3.reference_year_set,
			t8.gid AS plot,
			coalesce(t10.value,0) AS value_sup,
			coalesce(t9.value,0) AS value_inf
		FROM
				w_cat_setups AS t1
		INNER JOIN	target_data.v_variable_hierarchy_internal AS t2 	ON t1.cs_id_sup = t2.cs_id_sup AND t1.ad_id_inf = t2.ad_id_inf AND t1.sp_id_inf = t2.sp_id_inf
		INNER JOIN	target_data.t_available_datasets AS t3			ON t2.cs_id_inf = t3.categorization_setup 
		INNER JOIN 	target_data.t_available_datasets AS t4			ON t2.cs_id_sup = t4.categorization_setup AND t3.panel = t4.panel AND t3.reference_year_set = t4.reference_year_set
		INNER JOIN 	sdesign.t_panel AS t5 					ON t4.panel = t5.id
		INNER JOIN 	sdesign.cm_cluster2panel_mapping AS t6 			ON t5.id = t6.panel
		INNER JOIN 	sdesign.t_cluster AS t7					ON t6.cluster = t7.id
		INNER JOIN 	sdesign.f_p_plot AS t8					ON t7.id = t8.cluster
		LEFT JOIN
			target_data.t_ldsity_values AS t9				ON t3.id = t9.available_datasets AND t8.gid = t9.plot AND t9.is_latest
		LEFT JOIN
			target_data.t_ldsity_values AS t10				ON t4.id = t10.available_datasets AND t8.gid = t10.plot AND t10.is_latest
		WHERE true
			' || _array_text_p || '
			' || _array_text_r || '
			' || _array_text_pan || '
		--GROUP BY t2.cs_id_sup, t3.panel, t3.reference_year_set, t6.value
	)
	,w_edge_sum as	(
		SELECT
			plot,
			panel,
			reference_year_set,
			node,
			value_sup AS node_sum,
			array_agg(edge_def ORDER BY edge_def) AS edges_def,
			array_agg(edge_def ORDER BY edge_def) AS edges_found,
			sum(value_inf) AS edges_sum
		FROM w_plot_var
		GROUP BY plot, panel, reference_year_set, node, ad_id_inf, sp_id_inf, value_sup
	)
	,w_diff as		(
					select
						plot,
						panel,
						reference_year_set,
						node		as variable,
						node_sum	as ldsity,
						edges_sum	as ldsity_sum,
						edges_def	as variables_def,
						edges_found	as variables_found,
						case
							when (abs(edges_sum) >  1e-12) and (abs(node_sum) <= 1e-12) then 1.0
							when (abs(edges_sum) <= 1e-12) and (abs(node_sum) <= 1e-12) then 0
							else abs( (node_sum - edges_sum) / node_sum )
						end as diff
					from w_edge_sum
					)
	select * from w_diff
	where
		(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
		or
		(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
	order by diff desc
	;
	';
	--raise notice '%		fn_add_plot_target_attr ', TimeOfDay(); --, _complete_query;
	RETURN QUERY EXECUTE _complete_query USING variables;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
  
comment on function target_data.fn_add_plot_target_attr(integer[], integer[], integer[], integer[], double precision, boolean) is
'Function showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Array of plots -- plots (FKEY to f_p_plot.gid). If NULL all available plots are checked.
 * Array of reference year sets -- reference year sets (FKEY to t_reference_year_set.id). If NULL all available reference year sets are checked.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Reference year set. FKEY to t_reference_year_set.id.
 * Target total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION target_data.fn_add_plot_target_attr(integer[], integer[], integer[], integer[], double precision, boolean) TO PUBLIC;

-- </function>
