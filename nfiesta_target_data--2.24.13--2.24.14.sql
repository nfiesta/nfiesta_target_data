--
-- Copyright 2025 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <view name="v_variable_hierarchy_internal" schema="target_data" src="views/v_variable_hierarchy.sql">
--
-- Copyright 2017, 2025 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_etl_check_area_domain" schema="target_data" src="functions/etl/fn_etl_check_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_area_domain(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_check_area_domain
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns table
(
	id						integer,
	export_connection		integer,
	area_domain				integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_ad				integer,
	atomic					boolean
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/					

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
			
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.adc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.adc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			return query
			with w as	(
						select
								0 as id,
								0 as export_connection,
								array[0] as area_domain,
								''::varchar as label,
								''::text as description,
								''::varchar as label_en,
								''::text as description_en,
								0 as etl_id,
								0 as id_t_etl_ad,
								null::boolean as atomic
						)
			select w.id, w.export_connection, w.area_domain, w.label, w.description, w.label_en, w.description_en, w.etl_id, w.id_t_etl_ad, w.atomic
			from w
			where w.id is distinct from 0;		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.adc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.adc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)		
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select array[cad.id] as id_type, cad.label, cad.description, cad.label_en, cad.description_en
					from target_data.c_area_domain as cad where cad.id in (select w4.id_type[1] from w4)
					)
			,w6 as	(
					select w2.id_type, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select
							_export_connection as export_connection,
							w6.id_type as area_domain,
							w6.label,
							w6.description,
							w6.label_en,
							w6.description_en
					from
							w6
					)
			,w8 as	(
					select
							w7.export_connection,
							w7.area_domain,
							w7.label,
							w7.description,
							w7.label_en,
							w7.description_en,
							t.etl_id,
							t.id as id_t_etl_ad
					from
							w7 left join target_data.t_etl_area_domain as t
					on
							w7.export_connection = t.export_connection
					and	
							--target_data.fn_etl_array_compare(w7.area_domain,t.area_domain)
							(w7.area_domain @> t.area_domain and w7.area_domain <@ t.area_domain)
					order
							by w7.area_domain
					)
			select
					(row_number() over ())::integer as id_order,
					w8.export_connection,
					w8.area_domain,
					w8.label,
					w8.description,
					w8.label_en,
					w8.description_en,
					w8.etl_id,
					w8.id_t_etl_ad,
					case when array_length(w8.area_domain,1) = 1 then true else false end as atomic
			from
					w8;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_area_domain(integer[], integer) is
'Function returns records for ETL c_area_domain table.';

grant execute on function target_data.fn_etl_check_area_domain(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_check_area_domain_category" schema="target_data" src="functions/etl/fn_etl_check_area_domain_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_area_domain_category(integer[], integer, integer) CASCADE;

create or replace function target_data.fn_etl_check_area_domain_category
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_id_t_etl_ad				integer default null::integer
)
returns table
(
	id						integer,
	export_connection		integer,
	etl_id_area_domain		integer,
	label_area_domain		varchar,
	label_en_area_domain	varchar,
	area_domain_category	integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_adc			integer,
	id_t_etl_ad				integer
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
		_cond_id_t_etl_ad			text;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_category: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain_category: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable 
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/					

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------

		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.adc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type,
						(target_data.fn_get_category4classification_rule_id('adc',t.adc2classification_rule)).id_category
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.adc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type, w.id_category from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			raise exception 'Error 03: fn_etl_check_area_domain_category: If exists area_domain than must exists any area_domain_category!'; 		
		else
			if _id_t_etl_ad is null
			then
				_cond_id_t_etl_ad := '';
			else
				_cond_id_t_etl_ad := 'where w9.id_t_etl_ad = $3';
			end if;

			return query execute
			'
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.adc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).label as label_type,
							(target_data.fn_get_category_type4classification_rule_id(''adc'', t.adc2classification_rule)).label_en as label_en_type,
							(target_data.fn_get_category4classification_rule_id(''adc'',t.adc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest($1))								
														)
					and
							t.adc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label_type, w1.label_en_type, w1.id_category, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select
							a.id_type,
							a.label_type,
							a.label_en_type,
							array[b.id] as id_category,
							b.label,
							b.description,
							b.label_en,
							b.description_en
					from
							(
							select cad.id, array[cad.id] as id_type, cad.label as label_type, cad.label_en as label_en_type
							from target_data.c_area_domain as cad where cad.id in (select w4.id_type[1] from w4)
							) as a
					inner
					join	target_data.c_area_domain_category as b on a.id = b.area_domain
					)
			,w6 as	(
					select w2.id_type, w2.label_type, w2.label_en_type, w2.id_category, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label_type, w5.label_en_type, w5.id_category, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select $2 as export_connection, w6.* from w6
					)
			,w8 as	(
					select w7.*, tead.etl_id as etl_area_domain, tead.id as id_t_etl_ad
					from w7 inner join target_data.t_etl_area_domain as tead
					on w7.export_connection = tead.export_connection
					--and target_data.fn_etl_array_compare(w7.id_type,tead.area_domain)
					and (w7.id_type @> tead.area_domain and w7.id_type <@ tead.area_domain)
					)
			,w9 as	(
					select w8.*, a.etl_id, a.id from w8
					left join
								(
								select * from target_data.t_etl_area_domain_category
								where etl_area_domain in (select distinct w8.id_t_etl_ad from w8)
								) as a
					--on target_data.fn_etl_array_compare(w8.id_category,a.area_domain_category)
					on (w8.id_category @> a.area_domain_category and w8.id_category <@ a.area_domain_category)
					order
							by w8.id_category
					)
			select
					(row_number() over ())::integer as id_order,
					w9.export_connection,
					w9.etl_area_domain,
					w9.label_type as label_area_domain,
					w9.label_en_type as label_en_area_domian,
					w9.id_category as area_domain_category,
					w9.label,
					w9.description,
					w9.label_en,
					w9.description_en,
					w9.etl_id,
					w9.id as id_t_etl_adc,
					w9.id_t_etl_ad
			from
					w9 '||_cond_id_t_etl_ad||'
			'
			using _categorization_setups, _export_connection, _id_t_etl_ad;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_area_domain_category(integer[], integer, integer) is
'Function returns records for ETL c_area_domain_category table.';

grant execute on function target_data.fn_etl_check_area_domain_category(integer[], integer, integer) to public;
-- </function>



-- <function name="fn_etl_check_sub_population" schema="target_data" src="functions/etl/fn_etl_check_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_sub_population(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_check_sub_population
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns table
(
	id						integer,
	export_connection		integer,
	sub_population			integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_sp				integer,
	atomic					boolean
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
	
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.spc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.spc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			return query
			with w as	(
						select
								0 as id,
								0 as export_connection,
								array[0] as sub_population,
								''::varchar as label,
								''::text as description,
								''::varchar as label_en,
								''::text as description_en,
								0 as etl_id,
								0 as id_t_etl_sp,
								null::boolean as atomic
						)
			select w.id, w.export_connection, w.sub_population, w.label, w.description, w.label_en, w.description_en, w.etl_id, w.id_t_etl_sp, w.atomic
			from w
			where w.id is distinct from 0;		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.spc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.spc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)		
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select array[csp.id] as id_type, csp.label, csp.description, csp.label_en, csp.description_en
					from target_data.c_sub_population as csp where csp.id in (select w4.id_type[1] from w4)
					)
			,w6 as	(
					select w2.id_type, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select
							_export_connection as export_connection,
							w6.id_type as sub_population,
							w6.label,
							w6.description,
							w6.label_en,
							w6.description_en
					from
							w6
					)
			,w8 as	(
					select
							w7.export_connection,
							w7.sub_population,
							w7.label,
							w7.description,
							w7.label_en,
							w7.description_en,
							t.etl_id,
							t.id as id_t_etl_sp
					from
							w7 left join target_data.t_etl_sub_population as t
					on
							w7.export_connection = t.export_connection
					and	
							--target_data.fn_etl_array_compare(w7.sub_population,t.sub_population)
							(w7.sub_population @> t.sub_population and w7.sub_population <@ t.sub_population)
					order
							by w7.sub_population
					)
			select
					(row_number() over ())::integer as id_order,
					w8.export_connection,
					w8.sub_population,
					w8.label,
					w8.description,
					w8.label_en,
					w8.description_en,
					w8.etl_id,
					w8.id_t_etl_sp,
					case when array_length(w8.sub_population,1) = 1 then true else false end as atomic
			from
					w8;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_sub_population(integer[], integer) is
'Function returns records for ETL c_sub_population table.';

grant execute on function target_data.fn_etl_check_sub_population(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_check_sub_population_category" schema="target_data" src="functions/etl/fn_etl_check_sub_population_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_sub_population_category(integer[], integer, integer) CASCADE;

create or replace function target_data.fn_etl_check_sub_population_category
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer,
	_id_t_etl_sp				integer default null::integer
)
returns table
(
	id						integer,
	export_connection		integer,
	etl_id_sub_population	integer,
	label_sub_population	varchar,
	label_en_sub_population	varchar,
	sub_population_category	integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_spc			integer,
	id_t_etl_sp				integer
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
		_cond_id_t_etl_sp			text;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population_category: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population_category: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------

		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.spc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type,
						(target_data.fn_get_category4classification_rule_id('spc',t.spc2classification_rule)).id_category
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))							
													)
				and
						t.spc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type, w.id_category from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			raise exception 'Error 04: fn_etl_check_sub_population_category: If exists sub_population than must exists any sub_population_category!'; 		
		else
			if _id_t_etl_sp is null
			then
				_cond_id_t_etl_sp := '';
			else
				_cond_id_t_etl_sp := 'where w9.id_t_etl_sp = $3';
			end if;

			return query execute
			'
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.spc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).label as label_type,
							(target_data.fn_get_category_type4classification_rule_id(''spc'', t.spc2classification_rule)).label_en as label_en_type,
							(target_data.fn_get_category4classification_rule_id(''spc'',t.spc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest($1))								
														)
					and
							t.spc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label_type, w1.label_en_type, w1.id_category, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select
							a.id_type,
							a.label_type,
							a.label_en_type,
							array[b.id] as id_category,
							b.label,
							b.description,
							b.label_en,
							b.description_en
					from
							(
							select csp.id, array[csp.id] as id_type, csp.label as label_type, csp.label_en as label_en_type
							from target_data.c_sub_population as csp where csp.id in (select w4.id_type[1] from w4)
							) as a
					inner
					join	target_data.c_sub_population_category as b on a.id = b.sub_population
					)
			,w6 as	(
					select w2.id_type, w2.label_type, w2.label_en_type, w2.id_category, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label_type, w5.label_en_type, w5.id_category, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select $2 as export_connection, w6.* from w6
					)
			,w8 as	(
					select w7.*, tesp.etl_id as etl_sub_population, tesp.id as id_t_etl_sp
					from w7 inner join target_data.t_etl_sub_population as tesp
					on w7.export_connection = tesp.export_connection
					--and target_data.fn_etl_array_compare(w7.id_type,tesp.sub_population)
					and (w7.id_type @> tesp.sub_population and w7.id_type <@ tesp.sub_population)
					)
			,w9 as	(
					select w8.*, a.etl_id, a.id from w8
					left join
								(
								select * from target_data.t_etl_sub_population_category
								where etl_sub_population in (select distinct w8.id_t_etl_sp from w8)
								) as a
					--on target_data.fn_etl_array_compare(w8.id_category,a.sub_population_category)
					on (w8.id_category @> a.sub_population_category and w8.id_category <@ a.sub_population_category)
					order
							by w8.id_category
					)
			select
					(row_number() over ())::integer as id_order,
					w9.export_connection,
					w9.etl_sub_population,
					w9.label_type as label_sub_population,
					w9.label_en_type as label_en_sub_population,
					w9.id_category as sub_population_category,
					w9.label,
					w9.description,
					w9.label_en,
					w9.description_en,
					w9.etl_id,
					w9.id as id_t_etl_spc,
					w9.id_t_etl_sp
			from
					w9 '||_cond_id_t_etl_sp||'
			'
			using _categorization_setups, _export_connection, _id_t_etl_sp;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_sub_population_category(integer[], integer, integer) is
'Function returns records for ETL c_sub_population_category table.';

grant execute on function target_data.fn_etl_check_sub_population_category(integer[], integer, integer) to public;
-- </function>



-- <function name="fn_etl_get_variables" schema="target_data" src="functions/etl/fn_etl_get_variables.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_variables
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_variables(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_get_variables
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns json
as
$$
declare
		_country_array							integer[];
		_res_country							varchar;
		_export_connection						integer;
		_target_variable						integer;
		_etl_id									integer;
		_res									json;

		_core_and_division						boolean;
		_check_variables						integer;
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_variables: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_variables: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;

		select array_agg(tss.country order by tss.country) from sdesign.t_strata_set as tss where tss.id in
		(select strata_set from sdesign.t_stratum where id in
		(select stratum from sdesign.t_panel
		where id in ((select panel from sdesign.cm_refyearset2panel_mapping where id in (select unnest(array[_refyearset2panel_mapping]))))))
		into _country_array;
	
		if array_length(_country_array,1) is distinct from 1
		then
			raise exception 'Error 03: fn_etl_get_variables: Input argument _refyearset2panel_mapping contains values for two or more different countries!';
		end if;
	
		select label from sdesign.c_country where id = _country_array[1]
		into _res_country;
	
		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w_etl_ad as		(
						select * from target_data.t_etl_area_domain	where export_connection = _export_connection
						)
		,w_etl_adc as	(
						select * from target_data.t_etl_area_domain_category where etl_area_domain in (select id from w_etl_ad)
						)
		,w_etl_sp as	(
						select * from target_data.t_etl_sub_population where export_connection = _export_connection
						)
		,w_etl_spc as	(
						select * from target_data.t_etl_sub_population_category where etl_sub_population in (select id from w_etl_sp)
						)				
		,w1 as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups for target variable that is reduced by
				 -- combination of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(-- list of IDs of available datasets from t_ldsity_values table
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select w2.* from w2 where w2.id in (select w3.available_datasets from w3)
				)
		,w5 as	(
				select
						w4.panel,
						w4.reference_year_set,
						array_agg(w4.categorization_setup order by w4.categorization_setup) as categorization_setup
				from
						w4 group by w4.panel, w4.reference_year_set
				)
		,w6 as	(
				select
						w5.*,
						target_data.fn_etl_supplement_categorization_setups(w5.categorization_setup) as categorization_setup_supplement
				from
						w5
				)
		,w7 as	(
				select
						w6.panel,
						w6.reference_year_set,
						unnest(w6.categorization_setup_supplement) as categorization_setup_supplement
				from
						w6
				)
		,w8 as	(
				select
					id,
					ldsity2target_variable,
					categorization_setup,
					_core_and_division as core_and_division,				
					(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type as id_spt_orig,
					(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type as id_adt_orig,
					(target_data.fn_get_category4classification_rule_id('spc', t.spc2classification_rule)).id_category as id_spc_orig,
					(target_data.fn_get_category4classification_rule_id('adc', t.adc2classification_rule)).id_category as id_adc_orig,
					(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_spt_orig,
					(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_adt_orig,
					(target_data.fn_get_category4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_spc_orig,
					(target_data.fn_get_category4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_adc_orig
				from
					target_data.cm_ldsity2target2categorization_setup as t
				where
					ldsity2target_variable
				in
						(
						select id from target_data.cm_ldsity2target_variable
						where target_variable = _target_variable
						)
				and categorization_setup in (select distinct w7.categorization_setup_supplement from w7) -- vyber kategorizacnich setupu po supplementu
				)
		,w9 as	(
				select
						w8.*,
						cltv.ldsity_object_type as core_or_division
				from
						w8
						inner join target_data.cm_ldsity2target_variable as cltv on w8.ldsity2target_variable = cltv.id
				)
		,w10a as	(select * from w9 where core_and_division = false)
		,w10b as	(select * from w9 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w9 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, label_spt_orig, label_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, label_adt_orig, label_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni,
							w11a.label_spt_orig as label_spt_orig_doplneni,
							w11a.label_spc_orig as label_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni,
							w11b.label_adt_orig as label_adt_orig_doplneni,
							w11b.label_adc_orig as label_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					label_spt_orig as label_spt, 
					label_adt_orig as label_adt,
					label_spc_orig as label_spc,
					label_adc_orig as label_adc,
					core_or_division
					from w10a
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					label_spt_orig as label_spt, 
					label_adt_orig_doplneni as label_adt,
					label_spc_orig as label_spc,
					label_adc_orig_doplneni as label_adc,
					core_or_division
					from w12b
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					label_spt_orig_doplneni as label_spt,
					label_adt_orig as label_adt,
					label_spc_orig_doplneni as label_spc,
					label_adc_orig as label_adc,
					core_or_division
					from w12a
					)
		,w14 as	(
				select distinct categorization_setup, id_spt, id_adt, id_spc, id_adc, label_spt, label_adt, label_spc, label_adc from w13 order by categorization_setup
				)
		,w15a as	(
					select t1.id_spt, w_etl_sp.etl_id as etl_id_spt from
					(select distinct id_spt from w14 where id_spt is not null) as t1
					inner join w_etl_sp
					on (t1.id_spt @> w_etl_sp.sub_population and t1.id_spt <@ w_etl_sp.sub_population)
					)
		,w15b as	(
					select t2.id_adt, w_etl_ad.etl_id as etl_id_adt from
					(select distinct id_adt from w14 where id_adt is not null) as t2
					inner join w_etl_ad
					on (t2.id_adt @> w_etl_ad.area_domain and t2.id_adt <@ w_etl_ad.area_domain)
					)
		,w15c as	(
					select t3.id_spc, w_etl_spc.etl_id as etl_id_spc from
					(select distinct id_spc from w14 where id_spc is not null) as t3
					inner join w_etl_spc
					on (t3.id_spc @> w_etl_spc.sub_population_category and t3.id_spc <@ w_etl_spc.sub_population_category)
					)
		,w15d as	(
					select t4.id_adc, w_etl_adc.etl_id as etl_id_adc from
					(select distinct id_adc from w14 where id_adc is not null) as t4
					inner join w_etl_adc
					on (t4.id_adc @> w_etl_adc.area_domain_category and t4.id_adc <@ w_etl_adc.area_domain_category)
					)
		,w15 as	(
				select
						w14.*,
						w15a.etl_id_spt,
						w15b.etl_id_adt,
						w15c.etl_id_spc,
						w15d.etl_id_adc
				from
						w14
						left join w15a on w14.id_spt = w15a.id_spt
						left join w15b on w14.id_adt = w15b.id_adt
						left join w15c on w14.id_spc = w15c.id_spc
						left join w15d on w14.id_adc = w15d.id_adc
				)
		,w16 as	(
				select
						categorization_setup,
						label_spt,
						label_spc,
						label_adt,
						label_adc,
						etl_id_spt,
						etl_id_spc,
						etl_id_adt,
						etl_id_adc,		
						case when etl_id_spt is null then 0 else etl_id_spt end as etl_id_spt__not_null,
						case when etl_id_spc is null then 0 else etl_id_spc end as etl_id_spc__not_null,
						case when etl_id_adt is null then 0 else etl_id_adt end as etl_id_adt__not_null,
						case when etl_id_adc is null then 0 else etl_id_adc end as etl_id_adc__not_null
				from
						w15
				)
		,w17 as	(
				select
						w16.*,
						---------------------------------------------------------------------------
						case
							when label_spt is     null and etl_id_spt is     null then true
							when label_spt is     null and etl_id_spt is not null then false -- this variant is impossibe to be
							when label_spt is not null and etl_id_spt is     null then false
							when label_spt is not null and etl_id_spt is not null then true
						end
							as check_spt,
						---------------------------------------------------------------------------
						case
							when label_spc is     null and etl_id_spc is     null then true
							when label_spc is     null and etl_id_spc is not null then false -- this variant is impossibe to be
							when label_spc is not null and etl_id_spc is     null then false
							when label_spc is not null and etl_id_spc is not null then true
						end
							as check_spc,
						---------------------------------------------------------------------------
						case
							when label_adt is     null and etl_id_adt is     null then true
							when label_adt is     null and etl_id_adt is not null then false -- this variant is impossibe to be
							when label_adt is not null and etl_id_adt is     null then false
							when label_adt is not null and etl_id_adt is not null then true
						end
							as check_adt,
						---------------------------------------------------------------------------
						case
							when label_adc is     null and etl_id_adc is     null then true
							when label_adc is     null and etl_id_adc is not null then false -- this variant is impossibe to be
							when label_adc is not null and etl_id_adc is     null then false
							when label_adc is not null and etl_id_adc is not null then true
						end
							as check_adc
				from
						w16
				)
		,w18 as	(
				select
						w17.*,
						case
							when (check_spt = false or check_spc = false or check_adt = false or check_adc = false)
							then
								false
							else
								true
						end
							as check_variables
				from
						w17
				)
		,w19 as	(
				select
						w7.*,
						tp.panel as panel__label,
						trys.reference_year_set as reference_year_set__label
				from
						w7
						inner join sdesign.t_panel as tp on w7.panel = tp.id
						inner join sdesign.t_reference_year_set as trys on w7.reference_year_set = trys.id
				)
		,w20 as	(
				select
						categorization_setup_supplement,
						array_agg(panel__label order by panel, reference_year_set) as panel__label,
						array_agg(reference_year_set__label order by panel, reference_year_set) as reference_year_set__label
				from
						w19 group by categorization_setup_supplement
				)
		,w21 as	(
				select w20.*, w18.* from w20 inner join w18
				on w20.categorization_setup_supplement = w18.categorization_setup
				)
		,w22 as	(
				select
						unnest(panel__label) as panel__label,
						unnest(reference_year_set__label) as reference_year_set__label,
						etl_id_spt__not_null,
						etl_id_spc__not_null,
						etl_id_adt__not_null,
						etl_id_adc__not_null,
						check_variables
				from
						w21
				)
		select
				json_build_object
				(
				'country', _res_country,
				'target_variable', _etl_id,
				'variables',json_agg(json_build_object(
					'panel', w22.panel__label,
					'reference_year_set', w22.reference_year_set__label,
					'sub_population_etl_id', w22.etl_id_spt__not_null,
					'sub_population_category_etl_id', w22.etl_id_spc__not_null,
					'area_domain_etl_id', w22.etl_id_adt__not_null,
					'area_domain_category_etl_id', w22.etl_id_adc__not_null,
					'check_variables',w22.check_variables))
				)
		from w22
		into _res;

		-----------------------------------------------------------------------
		-- check that all records of check_variables in internal argument _res
		-- in element variables are TRUE
		with
		w1 as	(
				select json_array_elements(_res->'variables') as s
				)
		,w2 as	(
				select
						(s->>'check_variables')::boolean as check_variables
				from
						w1
				)
		select count(w2.*) from w2 where w2.check_variables = false
		into _check_variables;

		if _check_variables > 0
		then
			raise exception 'Error 04: fn_etl_get_variables: For input aruguments: _refyearset2panel_mapping = % and _id_t_etl_target_variable = % was not ETL yet some of area domain type, area domain category, sub population type or sub population category into target DB!',_refyearset2panel_mapping, _id_t_etl_target_variable;
		end if;
		-----------------------------------------------------------------------

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_variables(integer[], integer) is
'Function returns variables for given input arguments.';

grant execute on function target_data.fn_etl_get_variables(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_export_variable" schema="target_data" src="functions/etl/fn_etl_export_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_variable(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_variable
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_categorization_setups		integer[];
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_variable: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_variable: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 04: fn_etl_export_variable: Internal argument _categorization_setups must not by NULL!';
		end if;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);

		with
		w1 as	(
				select
						id,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest(_categorization_setups))
				)
		,w2 as	(
				select
						w1.*,
						cltv.ldsity_object_type as core_or_division
				from
						w1
						inner join target_data.cm_ldsity2target_variable as cltv on w1.ldsity2target_variable = cltv.id
				)
		-------------------------
		,w10a as	(select * from w2 where core_and_division = false)
		,w10b as	(select * from w2 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w2 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w10a
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					core_or_division
					from w12b
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w12a
					)
		-------------------------
		,w4 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w13 order by categorization_setup
				)
		,w5 as	(
				select * from w4 where id_spt is null and id_spc is null and id_adt is null and id_adc is null -- choosing of row without distinction
				)
		,w6 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from (select * from w4 where categorization_setup is distinct from (select categorization_setup from w5)) as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on (t.id_spt @> tesp.sub_population and t.id_spt <@ tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on (t.id_adt @> tead.area_domain and t.id_adt <@ tead.area_domain)
				
				)
		,w7 as	(
				select
						w6.*,
						tespc.etl_id as etl_id_spc,
						teadc.etl_id as etl_id_adc
				from w6
				
				left join	target_data.t_etl_sub_population_category as tespc
				on  w6.id_sub_population = tespc.etl_sub_population
				and (w6.id_spc @> tespc.sub_population_category and w6.id_spc <@ tespc.sub_population_category)
				
				left join	target_data.t_etl_area_domain_category as teadc
				on  w6.id_area_domain = teadc.etl_area_domain
				and (w6.id_adc @> teadc.area_domain_category and w6.id_adc <@ teadc.area_domain_category)
				)
		,w8 as	(
				select categorization_setup, _etl_id as etl_id_tv, 0 as etl_id_spc, 0 as etl_id_adc from w5 -- row without distinction 
				union all
				select
						categorization_setup,
						_etl_id as etl_id_tv,
						case when etl_id_spc is null then 0 else etl_id_spc end as etl_id_spc,
						case when etl_id_adc is null then 0 else etl_id_adc end as etl_id_adc
				from
						w7 order by categorization_setup
				)
		select
			json_agg(json_build_object(
				'target_variable', w8.etl_id_tv,
				'sub_population_category', w8.etl_id_spc, 
				'area_domain_category', w8.etl_id_adc))
		from
				w8 into _res;
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_variable(integer[], integer) is
'Function returns records for ETL t_variable table.';

grant execute on function target_data.fn_etl_export_variable(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_export_variable_hierarchy" schema="target_data" src="functions/etl/fn_etl_export_variable_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_variable_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_variable_hierarchy(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_variable_hierarchy
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_categorization_setups		integer[];
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_variable_hierarchy: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_variable_hierarchy: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_etl_export_variable_hierarchy: Internal argument _categorization_setups must not by NULL!';
		end if;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);

		-------------------------------------------------------------
		refresh materialized view target_data.v_variable_hierarchy_internal;
		-------------------------------------------------------------
		
		with
		w_etl_ad as		(
						select * from target_data.t_etl_area_domain	where export_connection = _export_connection
						)
		,w_etl_adc as	(
						select * from target_data.t_etl_area_domain_category where etl_area_domain in (select id from w_etl_ad)
						)
		,w_etl_sp as	(
						select * from target_data.t_etl_sub_population where export_connection = _export_connection
						)
		,w_etl_spc as	(
						select * from target_data.t_etl_sub_population_category where etl_sub_population in (select id from w_etl_sp)
						)
		,w1 as	(
				select
						id,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest(_categorization_setups))
				)
		,w2 as	(
				select
						w1.*,
						cltv.ldsity_object_type as core_or_division
				from
						w1
						inner join target_data.cm_ldsity2target_variable as cltv on w1.ldsity2target_variable = cltv.id
				)
		-------------------------
		,w10a as	(select * from w2 where core_and_division = false)
		,w10b as	(select * from w2 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w2 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w10a
					union all			
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					core_or_division
					from w12b
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w12a
					)
		-------------------------
		,w4 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w13 order by categorization_setup
				)
		,w5 as	(	
				select
						node as variable_superior,
						panel, reference_year_set,
						unnest(edges) as variable
				from
						target_data.v_variable_hierarchy
				where
						node in (select unnest(_categorization_setups))
				)
		,w6 as	(
				select
						_etl_id as etl_id_target_variable,
						w5.variable,
						w5.panel, w5.reference_year_set,
						w5.variable_superior,
						w4b.id_spt,
						w4a.id_spt as id_spt_sup,
						w4b.id_spc,
						w4a.id_spc as id_spc_sup,
						w4b.id_adt,
						w4a.id_adt as id_adt_sup,
						w4b.id_adc,
						w4a.id_adc as id_adc_sup
				from		w5
				left join	w4 as w4a	on w5.variable_superior = w4a.categorization_setup
				left join	w4 as w4b	on w5.variable = w4b.categorization_setup
				)
		,w15a as	(
					select t1.id_spt, w_etl_sp.id as id_sub_population_podrizene from
					(select distinct id_spt from w6 where id_spt is not null) as t1
					inner join w_etl_sp
					on (t1.id_spt @> w_etl_sp.sub_population and t1.id_spt <@ w_etl_sp.sub_population)
					)
		,w15b as	(
					select t2.id_spt_sup, w_etl_sp.id as id_sub_population_nadrizene from
					(select distinct id_spt_sup from w6 where id_spt_sup is not null) as t2
					inner join w_etl_sp
					on (t2.id_spt_sup @> w_etl_sp.sub_population and t2.id_spt_sup <@ w_etl_sp.sub_population)
					)
		,w15c as	(
					select t3.id_adt, w_etl_ad.id as id_area_domain_podrizene from
					(select distinct id_adt from w6 where id_adt is not null) as t3
					inner join w_etl_ad
					on (t3.id_adt @> w_etl_ad.area_domain and t3.id_adt <@ w_etl_ad.area_domain)
					)
		,w15d as	(
					select t4.id_adt_sup, w_etl_ad.id as id_area_domain_nadrizene from
					(select distinct id_adt_sup from w6 where id_adt_sup is not null) as t4
					inner join w_etl_ad
					on (t4.id_adt_sup @> w_etl_ad.area_domain and t4.id_adt_sup <@ w_etl_ad.area_domain)
					)					
		,w7a as	(
				select
						w6.*,
						w15a.id_sub_population_podrizene,
						w15b.id_sub_population_nadrizene,
						w15c.id_area_domain_podrizene,
						w15d.id_area_domain_nadrizene
				from
						w6
						left join w15a on w6.id_spt = w15a.id_spt
						left join w15b on w6.id_spt_sup = w15b.id_spt_sup
						left join w15c on w6.id_adt = w15c.id_adt
						left join w15d on w6.id_adt_sup = w15d.id_adt_sup
				)
		,w16a as	(
					select t1.id_sub_population_podrizene, t1.id_spc, w_etl_spc.etl_id as etl_id_spc from
					(select distinct id_sub_population_podrizene, id_spc from w7a where id_spc is not null) as t1
					inner join w_etl_spc on t1.id_sub_population_podrizene = w_etl_spc.etl_sub_population
					and (t1.id_spc @> w_etl_spc.sub_population_category and t1.id_spc <@ w_etl_spc.sub_population_category)
					)
		,w16b as	(
					select t2.id_sub_population_nadrizene, t2.id_spc_sup, w_etl_spc.etl_id as etl_id_spc_sup from
					(select distinct id_sub_population_nadrizene, id_spc_sup from w7a where id_spc_sup is not null) as t2
					inner join w_etl_spc on t2.id_sub_population_nadrizene = w_etl_spc.etl_sub_population
					and (t2.id_spc_sup @> w_etl_spc.sub_population_category and t2.id_spc_sup <@ w_etl_spc.sub_population_category)
					)
		,w16c as	(
					select t3.id_area_domain_podrizene, t3.id_adc, w_etl_adc.etl_id as etl_id_adc from
					(select distinct id_area_domain_podrizene, id_adc from w7a where id_adc is not null) as t3
					inner join w_etl_adc on t3.id_area_domain_podrizene = w_etl_adc.etl_area_domain
					and (t3.id_adc @> w_etl_adc.area_domain_category and t3.id_adc <@ w_etl_adc.area_domain_category)
					)
		,w16d as	(
					select t4.id_area_domain_nadrizene, t4.id_adc_sup, w_etl_adc.etl_id as etl_id_adc_sup from
					(select distinct id_area_domain_nadrizene, id_adc_sup from w7a where id_adc_sup is not null) as t4
					inner join w_etl_adc on t4.id_area_domain_nadrizene = w_etl_adc.etl_area_domain
					and (t4.id_adc_sup @> w_etl_adc.area_domain_category and t4.id_adc_sup <@ w_etl_adc.area_domain_category)
					)			
		,w8a as	(
				select
						w7a.*,
						w16a.etl_id_spc,
						w16b.etl_id_spc_sup,
						w16c.etl_id_adc,
						w16d.etl_id_adc_sup
				from
						w7a
						left join w16a on w7a.id_sub_population_podrizene = w16a.id_sub_population_podrizene and w7a.id_spc = w16a.id_spc
						left join w16b on w7a.id_sub_population_nadrizene = w16b.id_sub_population_nadrizene and w7a.id_spc_sup = w16b.id_spc_sup
						left join w16c on w7a.id_area_domain_podrizene = w16c.id_area_domain_podrizene and w7a.id_adc = w16c.id_adc
						left join w16d on w7a.id_area_domain_nadrizene = w16d.id_area_domain_nadrizene and w7a.id_adc_sup = w16d.id_adc_sup
				)			
		,w9 as	(
				select
						etl_id_target_variable as etl_id_tv,
						panel,
						reference_year_set,
						case when etl_id_spc is null then 0 else etl_id_spc end				as etl_id_spc,
						case when etl_id_spc_sup is null then 0 else etl_id_spc_sup end 	as etl_id_spc_sup,
						case when etl_id_adc is null then 0 else etl_id_adc end				as etl_id_adc,
						case when etl_id_adc_sup is null then 0 else etl_id_adc_sup end		as etl_id_adc_sup
				from
						w8a
				)
		select
			json_agg(json_build_object(
				'target_variable',			w9.etl_id_tv,
				'country',					(select c_country.label
										from sdesign.c_country
										inner join sdesign.t_strata_set on (t_strata_set.country = c_country.id)
										inner join sdesign.t_stratum on (t_stratum.strata_set = t_strata_set.id)
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'strata_set',					(select t_strata_set.strata_set
										from sdesign.t_strata_set
										inner join sdesign.t_stratum on (t_stratum.strata_set = t_strata_set.id)
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'stratum',					(select t_stratum.stratum
										from sdesign.t_stratum
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'reference_year_set',				(select reference_year_set from sdesign.t_reference_year_set where t_reference_year_set.id = w9.reference_year_set),
				'panel',					(select panel from sdesign.t_panel where t_panel.id = w9.panel),
				'sub_population_category',		w9.etl_id_spc,
				'sub_population_category_superior',	w9.etl_id_spc_sup,
				'area_domain_category',			w9.etl_id_adc,
				'area_domain_category_superior',	w9.etl_id_adc_sup)
				order by w9.etl_id_tv, w9.panel, w9.reference_year_set, w9.etl_id_spc, w9.etl_id_spc_sup, w9.etl_id_adc, w9.etl_id_adc_sup
			)
		from w9
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_variable_hierarchy(integer[], integer) is
'Function returns records for ETL t_variable_hierarchy table.';

grant execute on function target_data.fn_etl_export_variable_hierarchy(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_export_ldsity_values" schema="target_data" src="functions/etl/fn_etl_export_ldsity_values.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_ldsity_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_ldsity_values(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_ldsity_values
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_available_datasets			integer[];
		_categorization_setups		integer[];
		_res_available_datasets		json;
		_res_ldsity_values			json;
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_ldsity_values: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_ldsity_values: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;			


		-------------------------------------------------------------------------------------------
		-- LDSITY VALUES => get available data sets and categorization setups FROM t_ldsity_values
		-------------------------------------------------------------------------------------------
		with
		w1 as	(-- all categorization setups for input target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups reduced by combinations of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(-- list of IDs of available datasets from t_ldsity_values table
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		select array_agg(w3.available_datasets order by w3.available_datasets) from w3
		into _available_datasets;
		
		with
		w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select unnest(_available_datasets))
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_etl_export_ldsity_values: Internal argument _categorization_setups must not by NULL!';
		end if;
		-------------------------------------------------------------------------------------------


		-------------------------------------------------------------
		-- AVAILABLE DATASETS => get all available data sets
		-------------------------------------------------------------
		with		
		w1a as	(
				select
						cmltv.id as id_cm_ldsity2target2categorization_setup,
						cmltv.ldsity2target_variable,
						cmltv.adc2classification_rule,
						cmltv.spc2classification_rule,
						cmltv.categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', cmltv.spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', cmltv.spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', cmltv.adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', cmltv.adc2classification_rule)).id_category as id_adc_orig			
				from
						target_data.cm_ldsity2target2categorization_setup as cmltv
				where
						ldsity2target_variable in
							(
							select cm.id from target_data.cm_ldsity2target_variable as cm
							where target_variable = _target_variable
							)	
				)
		,w2a as	(-- list of categorization setups reduced by combinations of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select distinct w1a.categorization_setup from w1a)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(
				select
						w2a.id as id_available_datasets,
						w2a.panel,
						w2a.reference_year_set,
						w2a.ldsity_threshold,
						w1a.*
				from
						w2a inner join w1a on w2a.categorization_setup = w1a.categorization_setup
				)
		,w4 as	(
				select
						w3.*,
						cltv.ldsity_object_type as core_or_division
				from
						w3
						inner join target_data.cm_ldsity2target_variable as cltv on w3.ldsity2target_variable = cltv.id
				)
		-------------------------
		,w10a as	(select * from w4 where core_and_division = false)
		,w10b as	(select * from w4 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w4 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id_available_datasets, panel, reference_year_set, id_cm_ldsity2target2categorization_setup, ldsity2target_variable, ldsity_threshold,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w10a
					union all			
					select id_available_datasets, panel, reference_year_set, id_cm_ldsity2target2categorization_setup, ldsity2target_variable, ldsity_threshold,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					core_or_division
					from w12b
					union all
					select id_available_datasets, panel, reference_year_set, id_cm_ldsity2target2categorization_setup, ldsity2target_variable, ldsity_threshold,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w12a
					)
		,w6 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w13
				)
		,w7 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from w6 as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on (t.id_spt @> tesp.sub_population and t.id_spt <@ tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on (t.id_adt @> tead.area_domain and t.id_adt <@ tead.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						case when tespc.etl_id is null then 0 else tespc.etl_id end as etl_id_spc,
						case when teadc.etl_id is null then 0 else teadc.etl_id end as etl_id_adc
				from w7
				
				left join target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population = tespc.etl_sub_population
				and (w7.id_spc @> tespc.sub_population_category and w7.id_spc <@ tespc.sub_population_category)
				
				left join target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain = teadc.etl_area_domain
				and (w7.id_adc @> teadc.area_domain_category and w7.id_adc <@ teadc.area_domain_category)
				)
		,w9 as	(
				select distinct t.t_panel__id, t.t_panel__panel, t.t_stratum__id, t.t_cluster_configuration__id, t.t_cluster_configuration__cluster_configuration,
				t.t_stratum__stratum, t.t_strata_set__id, t.t_strata_set__strata_set, t.c_country__id, t.c_country__label,
				t.t_reference_year_set__id, t.t_reference_year_set__reference_year_set, t.t_inventory_campaign__id, t.t_inventory_campaign__inventory_campaign
				from
						(
						select		
								t_panel.id as t_panel__id,
								t_panel.panel as t_panel__panel,
								t_cluster.id as t_cluster__id,
								t_cluster.cluster as t_cluster__cluster,
								f_p_plot.gid as f_p_plot__gid,
								f_p_plot.plot as f_p_plot__plot,
								t_cluster_configuration.id as t_cluster_configuration__id,
								t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
								t_stratum.id as t_stratum__id,
								t_stratum.stratum as t_stratum__stratum,
								t_strata_set.id as t_strata_set__id,
								t_strata_set.strata_set as t_strata_set__strata_set,
								c_country.id as c_country__id,
								c_country.label as c_country__label,
								t_reference_year_set.id as t_reference_year_set__id,
								t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
								t_inventory_campaign.id as t_inventory_campaign__id,
								t_inventory_campaign.inventory_campaign as t_inventory_campaign__inventory_campaign
						from
									sdesign.cm_refyearset2panel_mapping
						inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
						inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
						inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
						inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
						inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
						inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
						inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
						inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
						inner join	sdesign.c_country on t_strata_set.country = c_country.id
						inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
						inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
						where
								cm_refyearset2panel_mapping.id in (select unnest(_refyearset2panel_mapping))
						) as t
				)
		,w10 as	(
				select
						categorization_setup,
						array_agg(panel order by panel, reference_year_set) as panel,
						array_agg(reference_year_set order by panel, reference_year_set) as reference_year_set,
						array_agg(ldsity_threshold order by panel, reference_year_set) as ldsity_threshold
				from
						w13 group by categorization_setup
				)
		,w11 as	(
				select
						w10.*,
						w8.etl_id_spc,
						w8.etl_id_adc
				from
						w10
						inner join w8 on w10.categorization_setup = w8.categorization_setup
				)
		,w12 as	(
				select
						unnest(w11.panel) as panel,
						unnest(w11.reference_year_set) as reference_year_set,
						unnest(w11.ldsity_threshold) as ldsity_threshold,
						etl_id_spc,
						etl_id_adc
				from
						w11
				)		
		,w14 as	(
				select
						_etl_id as etl_id_tv,
						w9.c_country__label as country,
						w9.t_strata_set__strata_set as strata_set,
						w9.t_stratum__stratum as stratum,
						w9.t_panel__panel as panel,
						w9.t_reference_year_set__reference_year_set as reference_year_set,
						w9.t_cluster_configuration__cluster_configuration as cluster_configuration,
						w9.t_inventory_campaign__inventory_campaign as inventory_campaign,
						w12.ldsity_threshold,
						w12.etl_id_spc,
						w12.etl_id_adc
				from
						w12
						inner join w9 on w12.panel = w9.t_panel__id and w12.reference_year_set = w9.t_reference_year_set__id
				)
		,w15 as	(
				select
						distinct
						w14.etl_id_tv,
						w14.country,
						w14.strata_set,
						w14.stratum,
						w14.panel,
						w14.reference_year_set,
						w14.cluster_configuration,
						w14.inventory_campaign,
						w14.etl_id_spc,
						w14.etl_id_adc,
						w14.ldsity_threshold
				from
						w14
				)		
		,w16 as	(
				select
						w15.etl_id_tv,
						w15.country,
						w15.strata_set,
						w15.stratum,
						w15.reference_year_set,
						w15.panel,
						w15.cluster_configuration,
						w15.inventory_campaign,
						w15.etl_id_spc,
						w15.etl_id_adc,
						w15.ldsity_threshold
				from
						w15
				order
				by		w15.country,
						w15.strata_set,
						w15.stratum,
						w15.reference_year_set,
						w15.panel,
						w15.cluster_configuration,
						w15.inventory_campaign,
						w15.etl_id_tv,
						w15.etl_id_spc,
						w15.etl_id_adc 
				)
		select
			json_agg(json_build_object(
				'target_variable',   		w16.etl_id_tv,
				'country',					w16.country,
				'strata_set',				w16.strata_set,
				'stratum',					w16.stratum,
				'reference_year_set',		w16.reference_year_set,
				'panel',					w16.panel,
				'cluster_configuration',	w16.cluster_configuration,
				'inventory_campaign',		w16.inventory_campaign,
				'sub_population_category',	w16.etl_id_spc,
				'area_domain_category',		w16.etl_id_adc,
				'ldsity_threshold',			w16.ldsity_threshold
				))
		from
				w16 into _res_available_datasets;
		-------------------------------------------------------------


		-------------------------------------------------------------
		-- LDSITY VALUES => get ldsity values
		-------------------------------------------------------------
		with
			w3 as	(
				select
						id as id_cm_ldsity2target2categorization_setup,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig				
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest(_categorization_setups))
				)
		,w4 as	(
				select
						w3.*,
						cltv.ldsity_object_type as core_or_division
				from
						w3
						inner join target_data.cm_ldsity2target_variable as cltv on w3.ldsity2target_variable = cltv.id
				)
		-------------------------
		,w10a as	(select * from w4 where core_and_division = false)
		,w10b as	(select * from w4 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w4 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id_cm_ldsity2target2categorization_setup, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w10a
					union all
					select id_cm_ldsity2target2categorization_setup, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					core_or_division
					from w12b
					union all
					select id_cm_ldsity2target2categorization_setup, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w12a
					)
		-------------------------
		,w6 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w13
				)
		,w7 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from w6 as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on (t.id_spt @> tesp.sub_population and t.id_spt <@ tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(t.id_adt,tead.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						case when tespc.etl_id is null then 0 else tespc.etl_id end as etl_id_spc,
						case when teadc.etl_id is null then 0 else teadc.etl_id end as etl_id_adc
				from w7
				
				left join target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population = tespc.etl_sub_population
				and (w7.id_spc @> tespc.sub_population_category and w7.id_spc <@ tespc.sub_population_category)
				
				left join target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain = teadc.etl_area_domain
				and (w7.id_adc @> teadc.area_domain_category and w7.id_adc <@ teadc.area_domain_category)
				)
		,w9 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory_campaign as t_inventory_campaign__inventory_campaign
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_refyearset2panel_mapping))
				)
		---------
		,w1a as	(
				select
						tad.id as id_available_datasets,
						tad.panel,
						tad.reference_year_set,
						tad.categorization_setup
				from
						target_data.t_available_datasets as tad
				where
						tad.id in (select unnest(_available_datasets))
				)
		,w1 as	(
				select
						w1a.*,
						w8.etl_id_spc,
						w8.etl_id_adc
				from
						w1a
						inner join w8 on w1a.categorization_setup = w8.categorization_setup
				)
		,w2 as	(
				select t1.*, w1.*
				from target_data.t_ldsity_values as t1
				inner join w1 on t1.available_datasets = w1.id_available_datasets
				where t1.is_latest = true
				)
		---------
		,w10 as	(
				select
						_etl_id as etl_id_tv,
						w9.c_country__label as country,
						w9.t_strata_set__strata_set as strata_set,
						w9.t_stratum__stratum as stratum,
						w9.t_panel__panel as panel,
						w9.t_reference_year_set__reference_year_set as reference_year_set,
						w9.t_cluster__cluster as cluster,
						w9.t_cluster_configuration__cluster_configuration as cluster_configuration,
						w9.t_inventory_campaign__inventory_campaign as inventory_campaign,
						w9.f_p_plot__plot as plot,
						w2.etl_id_spc,
						w2.etl_id_adc,
						w2.value
				from
						w2
						inner join w9
						on w2.plot = w9.f_p_plot__gid and w2.panel = w9.t_panel__id and w2.reference_year_set = w9.t_reference_year_set__id
				)
		,w11 as	(
				select
						w10.etl_id_tv,
						w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster,
						w10.cluster_configuration,
						w10.inventory_campaign,						
						w10.plot,
						w10.etl_id_spc,
						w10.etl_id_adc,
						w10.value
				from
						w10
				order
				by		w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster,
						w10.cluster_configuration,
						w10.inventory_campaign,
						w10.etl_id_tv,
						w10.plot,
						w10.etl_id_spc,
						w10.etl_id_adc 
				)
		select
			json_agg(json_build_object(
				'target_variable',   		w11.etl_id_tv,
				'country',					w11.country,
				'strata_set',				w11.strata_set,
				'stratum',					w11.stratum,
				'reference_year_set',		w11.reference_year_set,
				'panel',					w11.panel,
				'cluster',					w11.cluster,
				'cluster_configuration',	w11.cluster_configuration,
				'inventory_campaign',		w11.inventory_campaign,
				'plot',						w11.plot,
				'sub_population_category',	w11.etl_id_spc,
				'area_domain_category',		w11.etl_id_adc,
				'value',					w11.value))
		from
				w11 into _res_ldsity_values;
		-------------------------------------------------------------


		-------------------------------------------------------------
		-- JOIN elements
		-------------------------------------------------------------
		select json_build_object
			(
				'available_datasets',_res_available_datasets,
				'ldsity_values',_res_ldsity_values
			)
		into _res;
		-------------------------------------------------------------
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_ldsity_values(integer[], integer) is
'Function returns records for ETL t_target_data and t_available_datasets table.';

grant execute on function target_data.fn_etl_export_ldsity_values(integer[], integer) to public;
-- </function>