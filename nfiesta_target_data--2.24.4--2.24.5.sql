--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP FUNCTION target_data.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean);

-- <function name="fn_add_plot_target_attr" schema="target_data" src="functions/additivity/fn_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_add_plot_target_attr
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean);

CREATE OR REPLACE FUNCTION target_data.fn_add_plot_target_attr
(
	IN variables integer[],
	IN plots integer[] default NULL,
	IN refyearsets integer[] default NULL,
	IN min_diff double precision default 0.0,
	IN include_null_diff boolean default true
)
  RETURNS TABLE
(
	plot			integer,
	panel			integer,
	reference_year_set	integer,
	variable		integer,
	ldsity			double precision,
	ldsity_sum		double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff			double precision	
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text_v text;
	_array_text_p text;
	_array_text_r text;

BEGIN
	--------------------------------QUERY--------------------------------
	_array_text_v := concat(quote_literal(variables::text), '::integer[]');

	if plots is not null then
		_array_text_p := concat(' AND f_p_plot.gid = ANY (', quote_literal(plots::text), '::integer[])');
	else
		_array_text_p := '';
	end if;

	if refyearsets is not null then
		_array_text_r := concat(' AND t_available_datasets.reference_year_set = ANY (', quote_literal(refyearsets::text), '::integer[])');
	else
		_array_text_r := '';
	end if;

	_complete_query :=
	'
	with
	w_plot_var as materialized
					(
					select
							f_p_plot.gid as plot,
							t_available_datasets.categorization_setup as variable,
							t_available_datasets.panel,
							t_available_datasets.reference_year_set,
							coalesce(t_ldsity_values.value, 0) as value
					from sdesign.f_p_plot
					inner join sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
					inner join sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
					inner join sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
					inner join target_data.t_available_datasets on (t_panel.id = t_available_datasets.panel) -- select * from target_data.t_available_datasets
					inner join target_data.t_categorization_setup on (t_available_datasets.categorization_setup = t_categorization_setup.id)
					left join target_data.t_ldsity_values on (
						f_p_plot.gid = t_ldsity_values.plot
						and t_available_datasets.id = t_ldsity_values.available_datasets
						and t_ldsity_values.is_latest)
					WHERE t_available_datasets.categorization_setup = ANY (' || _array_text_v || ')	
							' || _array_text_p || '
							' || _array_text_r || '
					)
	,w_node_sum as	(
					select
						plot,
						w_plot_var.panel,
						w_plot_var.reference_year_set,
						w_plot_var.variable,
						value as node_sum,
						v_variable_hierarchy.node,
						v_variable_hierarchy.edges as edges_def
					from w_plot_var
					inner join target_data.v_variable_hierarchy on (v_variable_hierarchy.node = w_plot_var.variable and 
											v_variable_hierarchy.panel = w_plot_var.panel and
											v_variable_hierarchy.reference_year_set = w_plot_var.reference_year_set)
					where (v_variable_hierarchy.edges <@ ' || _array_text_v || ')
					)
	,w_edge_sum as	(
					select
						w_node_sum.plot,
						w_node_sum.panel,
						w_node_sum.reference_year_set,
						w_node_sum.node,
						w_node_sum.node_sum,
						w_node_sum.edges_def,
						array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
						sum(w_plot_var.value) as edges_sum
					from w_node_sum
					left join w_plot_var on (
						w_plot_var.plot = w_node_sum.plot
						and w_plot_var.panel = w_node_sum.panel
						and w_plot_var.reference_year_set = w_node_sum.reference_year_set
						and w_plot_var.variable = any(w_node_sum.edges_def))
					group by w_node_sum.plot, w_node_sum.panel, w_node_sum.reference_year_set,
						w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
					)
	,w_diff as		(
					select
						plot,
						panel,
						reference_year_set,
						node		as variable,
						node_sum	as ldsity,
						edges_sum	as ldsity_sum,
						edges_def	as variables_def,
						edges_found	as variables_found,
						case
							when (abs(edges_sum) >  1e-12) and (abs(node_sum) <= 1e-12) then 1.0
							when (abs(edges_sum) <= 1e-12) and (abs(node_sum) <= 1e-12) then 0
							else abs( (node_sum - edges_sum) / node_sum )
						end as diff
					from w_edge_sum
					)
	select * from w_diff
	where
		(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
		or
		(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
	order by diff desc
	;
	';
	--raise notice '%		fn_add_plot_target_attr -- %', TimeOfDay(), _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
  
comment on function target_data.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean) is
'Function showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Array of plots -- plots (FKEY to f_p_plot.gid). If NULL all available plots are checked.
 * Array of reference year sets -- reference year sets (FKEY to t_reference_year_set.id). If NULL all available reference year sets are checked.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Reference year set. FKEY to t_reference_year_set.id.
 * Target total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION target_data.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean) TO PUBLIC;

-- </function>

-- <function name="fn_check_t_ldsity_values" schema="target_data" src="functions/fn_check_t_ldsity_values.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_check_t_ldsity_values
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_t_ldsity_values();

CREATE OR REPLACE FUNCTION target_data.fn_check_t_ldsity_values() RETURNS TRIGGER AS $src$
    DECLARE
		_cs int[];
		_vars int[];
		_plots int[];
		_refyearsets int[];
		_available_datasets int[];
		_errp json;
    BEGIN
        IF (TG_OP = 'DELETE' or TG_OP = 'UPDATE' or TG_OP = 'INSERT') THEN
			IF (TG_TABLE_NAME = 't_ldsity_values') THEN
				with w_vars as (
						select array_prepend(node, edges) as vs 
					from trans_table
					inner join target_data.t_available_datasets on trans_table.available_datasets = t_available_datasets.id
					inner join target_data.v_variable_hierarchy 
						on ((t_available_datasets.categorization_setup = v_variable_hierarchy.node 
							or t_available_datasets.categorization_setup = any (v_variable_hierarchy.edges))
						and (t_available_datasets.panel = v_variable_hierarchy.panel
							and t_available_datasets.reference_year_set = v_variable_hierarchy.reference_year_set)
					)
				)
				, w_vars_un as (
						select unnest(vs) as v from w_vars
				)
				select array_agg(distinct v) into _vars from w_vars_un;
				select array_agg(distinct categorization_setup) into _cs from trans_table inner join target_data.t_available_datasets on trans_table.available_datasets = t_available_datasets.id;
				select array_agg(distinct plot) into _plots from trans_table;
				select array_agg(distinct reference_year_set) into _refyearsets from target_data.t_available_datasets where id in (select distinct available_datasets from trans_table);
				select array_agg(distinct available_datasets) into _available_datasets from trans_table;
			ELSIF (TG_TABLE_NAME = 't_available_datasets') THEN
                                with w_vars as (
                                                select array_prepend(node, edges) as vs
                                        from trans_table
                                        inner join target_data.v_variable_hierarchy
                                                on ((trans_table.categorization_setup = v_variable_hierarchy.node
                                                        or trans_table.categorization_setup = any (v_variable_hierarchy.edges))
						and (trans_table.panel = v_variable_hierarchy.panel
							and trans_table.reference_year_set = v_variable_hierarchy.reference_year_set)
					)
                                )
                                , w_vars_un as (
                                                select unnest(vs) as v from w_vars
                                )
                                select array_agg(distinct v) into _vars from w_vars_un;
				select array_agg(distinct categorization_setup) into _cs from trans_table;
				select array_agg(distinct f_p_plot.gid) into _plots
					from trans_table
					inner join sdesign.t_panel on (trans_table.panel = t_panel.id)
					inner join sdesign.cm_cluster2panel_mapping on (t_panel.id = cm_cluster2panel_mapping.panel)
					inner join sdesign.t_cluster on (cm_cluster2panel_mapping.cluster = t_cluster.id)
					inner join sdesign.f_p_plot on (t_cluster.id = f_p_plot.cluster);
				select array_agg(distinct reference_year_set) into _refyearsets from target_data.t_available_datasets where id in (select distinct id from trans_table);
				select array_agg(distinct id) into _available_datasets from trans_table;
			ELSE
				RAISE EXCEPTION 'fn_check_t_ldsity_values -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
			END IF;

			if _vars is not null
			then
				--raise notice '%		fn_check_t_ldsity_values -- UPDATE CHECK START -- select target_data.fn_add_plot_target_attr(%, %, %, %, %);', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', _vars, _plots, _refyearsets, 1e-6, true;
				with w_err as (
					select target_data.fn_add_plot_target_attr(_vars, _plots, _refyearsets, 1e-6, true) as fn_err
				)
				, w_err_t as (
					select (fn_err).* from w_err
				)
				, w_err_json as (
					select 
						json_build_object(
							'plot'					, plot,
							'panel'		, panel,
							'reference_year_set'	, reference_year_set,
							'variable'				, variable,
							'ldsity'				, ldsity,
							'ldsity_sum'			, ldsity_sum,
							'variables_def'			, variables_def,
							'variables_found'		, variables_found,
							'diff'					, diff
						) as errj
					from w_err_t
				)
				select json_agg(errj) into _errp from w_err_json;
				--raise notice '%		fn_check_t_ldsity_values -- UPDATE CHECK STOP', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague';
				IF 
					(json_array_length(_errp) > 0) 
					THEN RAISE EXCEPTION 'fn_check_t_ldsity_values -- % -- % -- plot level local densities are not additive:
						checked categorization setups: %, variables in additivity set: %, checked plots: %, 
						found err plots: 
						%', TG_TABLE_NAME, TG_OP, _cs, _vars, _plots, jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE WARNING 'fn_check_t_ldsity_values -- % -- % -- additivity check was skiped: 
						* 0 rows edited (next line displays NULL)
						* corresponding variable hierarchy not found (next line displays array)
						trans_table available_datasets: %', TG_TABLE_NAME, TG_OP, _available_datasets;
			end if;
	ELSE
		RAISE EXCEPTION 'fn_check_t_ldsity_values -- trigger operation not known: %', TG_OP;
	END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$src$ LANGUAGE plpgsql;

comment on function target_data.fn_check_t_ldsity_values IS 'Trigger function for checking additivity. Launched by triggers on either t_ldsity_values or t_available_datasets. Triggers on insert and update on t_available_datasets dropped due to performance problem. Will be replaced by more advanced concept of additivity checks in https://gitlab.com/nfiesta/nfiesta_pg/-/issues/133.';

drop trigger if exists trg__ldsity_values__ins ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__ins
    AFTER INSERT ON target_data.t_ldsity_values
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__ldsity_values__upd ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__upd
    AFTER UPDATE ON target_data.t_ldsity_values
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__ldsity_values__del ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__del
    AFTER DELETE ON target_data.t_ldsity_values
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

/*
triggers dropped due to prefromance problem
drop trigger if exists trg__available_datasets__ins ON target_data.t_available_datasets;
CREATE TRIGGER trg__available_datasets__ins
    AFTER INSERT ON target_data.t_available_datasets
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__available_datasets__upd ON target_data.t_available_datasets;
CREATE TRIGGER trg__available_datasets__upd
    AFTER UPDATE ON target_data.t_available_datasets
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();
*/

drop trigger if exists trg__available_datasets__del ON target_data.t_available_datasets;
CREATE TRIGGER trg__available_datasets__del
    AFTER DELETE ON target_data.t_available_datasets
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

-- </function>

