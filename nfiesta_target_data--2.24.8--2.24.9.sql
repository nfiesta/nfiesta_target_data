--
-- Copyright 2024 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


-- <function name="fn_etl_get_variables" schema="target_data" src="functions/etl/fn_etl_get_variables.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_variables
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_variables(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_get_variables
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns json
as
$$
declare
		_country_array							integer[];
		_res_country							varchar;
		_export_connection						integer;
		_target_variable						integer;
		_etl_id									integer;
		_res									json;

		_core_and_division						boolean;
		_check_variables						integer;
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_variables: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_variables: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;

		select array_agg(tss.country order by tss.country) from sdesign.t_strata_set as tss where tss.id in
		(select strata_set from sdesign.t_stratum where id in
		(select stratum from sdesign.t_panel
		where id in ((select panel from sdesign.cm_refyearset2panel_mapping where id in (select unnest(array[_refyearset2panel_mapping]))))))
		into _country_array;
	
		if array_length(_country_array,1) is distinct from 1
		then
			raise exception 'Error 03: fn_etl_get_variables: Input argument _refyearset2panel_mapping contains values for two or more different countries!';
		end if;
	
		select label from sdesign.c_country where id = _country_array[1]
		into _res_country;
	
		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w_etl_ad as		(
						select * from target_data.t_etl_area_domain	where export_connection = _export_connection
						)
		,w_etl_adc as	(
						select * from target_data.t_etl_area_domain_category where etl_area_domain in (select id from w_etl_ad)
						)
		,w_etl_sp as	(
						select * from target_data.t_etl_sub_population where export_connection = _export_connection
						)
		,w_etl_spc as	(
						select * from target_data.t_etl_sub_population_category where etl_sub_population in (select id from w_etl_sp)
						)				
		,w1 as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups for target variable that is reduced by
				 -- combination of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(-- list of IDs of available datasets from t_ldsity_values table
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select w2.* from w2 where w2.id in (select w3.available_datasets from w3)
				)
		,w5 as	(
				select
						w4.panel,
						w4.reference_year_set,
						array_agg(w4.categorization_setup order by w4.categorization_setup) as categorization_setup
				from
						w4 group by w4.panel, w4.reference_year_set
				)
		,w6 as	(
				select
						w5.*,
						target_data.fn_etl_supplement_categorization_setups(w5.categorization_setup) as categorization_setup_supplement
				from
						w5
				)
		,w7 as	(
				select
						w6.panel,
						w6.reference_year_set,
						unnest(w6.categorization_setup_supplement) as categorization_setup_supplement
				from
						w6
				)
		,w8 as	(
				select
					id,
					ldsity2target_variable,
					categorization_setup,
					_core_and_division as core_and_division,				
					(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type as id_spt_orig,
					(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type as id_adt_orig,
					(target_data.fn_get_category4classification_rule_id('spc', t.spc2classification_rule)).id_category as id_spc_orig,
					(target_data.fn_get_category4classification_rule_id('adc', t.adc2classification_rule)).id_category as id_adc_orig,
					(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_spt_orig,
					(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_adt_orig,
					(target_data.fn_get_category4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_spc_orig,
					(target_data.fn_get_category4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_adc_orig
				from
					target_data.cm_ldsity2target2categorization_setup as t
				where
					ldsity2target_variable
				in
						(
						select id from target_data.cm_ldsity2target_variable
						where target_variable = _target_variable
						)
				and categorization_setup in (select distinct w7.categorization_setup_supplement from w7)
				)
		,w9 as	(
				select
						row_number() over() as id4join,
						w7.*,
						w8.*,
						cltv.ldsity_object_type as core_or_division
				from
						w7
						inner join w8 on w7.categorization_setup_supplement = w8.categorization_setup
						inner join target_data.cm_ldsity2target_variable as cltv on w8.ldsity2target_variable = cltv.id
				)
		,w10a as	(select * from w9 where core_and_division = false)
		,w10b as	(select * from w9 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w9 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, label_spt_orig, label_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, label_adt_orig, label_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni,
							w11a.label_spt_orig as label_spt_orig_doplneni,
							w11a.label_spc_orig as label_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni,
							w11b.label_adt_orig as label_adt_orig_doplneni,
							w11b.label_adc_orig as label_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id4join, panel, reference_year_set, categorization_setup_supplement, id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					label_spt_orig as label_spt, 
					label_adt_orig as label_adt,
					label_spc_orig as label_spc,
					label_adc_orig as label_adc,
					core_or_division
					from w10a
					union all
					select id4join, panel, reference_year_set, categorization_setup_supplement, id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					label_spt_orig as label_spt, 
					label_adt_orig_doplneni as label_adt,
					label_spc_orig as label_spc,
					label_adc_orig_doplneni as label_adc,
					core_or_division
					from w12b
					union all
					select id4join, panel, reference_year_set, categorization_setup_supplement, id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					label_spt_orig_doplneni as label_spt,
					label_adt_orig as label_adt,
					label_spc_orig_doplneni as label_spc,
					label_adc_orig as label_adc,
					core_or_division
					from w12a
					)
		,w14 as	(
				select distinct panel, reference_year_set, categorization_setup, id_spt, id_adt, id_spc, id_adc, label_spt, label_adt, label_spc, label_adc from w13 order by categorization_setup
				)
		,w15a as	(
					select t1.id_spt, w_etl_sp.etl_id as etl_id_spt from
					(select distinct id_spt from w14 where id_spt is not null) as t1
					inner join w_etl_sp	on target_data.fn_etl_array_compare(t1.id_spt,w_etl_sp.sub_population)
					)
		,w15b as	(
					select t2.id_adt, w_etl_ad.etl_id as etl_id_adt from
					(select distinct id_adt from w14 where id_adt is not null) as t2
					inner join w_etl_ad	on target_data.fn_etl_array_compare(t2.id_adt,w_etl_ad.area_domain)
					)
		,w15c as	(
					select t3.id_spc, w_etl_spc.etl_id as etl_id_spc from
					(select distinct id_spc from w14 where id_spc is not null) as t3
					inner join w_etl_spc on target_data.fn_etl_array_compare(t3.id_spc,w_etl_spc.sub_population_category)
					)
		,w15d as	(
					select t4.id_adc, w_etl_adc.etl_id as etl_id_adc from
					(select distinct id_adc from w14 where id_adc is not null) as t4
					inner join w_etl_adc on target_data.fn_etl_array_compare(t4.id_adc,w_etl_adc.area_domain_category)
					)
		,w15 as	(
				select
						w14.*,
						w15a.etl_id_spt,
						w15b.etl_id_adt,
						w15c.etl_id_spc,
						w15d.etl_id_adc,
						tp.panel as panel__label,
						trys.reference_year_set as reference_year_set__label
				from
						w14
						left join w15a on w14.id_spt = w15a.id_spt
						left join w15b on w14.id_adt = w15b.id_adt
						left join w15c on w14.id_spc = w15c.id_spc
						left join w15d on w14.id_adc = w15d.id_adc
						inner join sdesign.t_panel as tp on w14.panel = tp.id
						inner join sdesign.t_reference_year_set as trys on w14.reference_year_set = trys.id
				)
		,w16 as	(
				select
						w15.panel__label,
						w15.reference_year_set__label,
						label_spt,
						label_spc,
						label_adt,
						label_adc,
						etl_id_spt,
						etl_id_spc,
						etl_id_adt,
						etl_id_adc,		
						case when etl_id_spt is null then 0 else etl_id_spt end as etl_id_spt__not_null,
						case when etl_id_spc is null then 0 else etl_id_spc end as etl_id_spc__not_null,
						case when etl_id_adt is null then 0 else etl_id_adt end as etl_id_adt__not_null,
						case when etl_id_adc is null then 0 else etl_id_adc end as etl_id_adc__not_null
				from
						w15
				)
		,w17 as	(
				select
						w16.*,
						---------------------------------------------------------------------------
						case
							when label_spt is     null and etl_id_spt is     null then true
							when label_spt is     null and etl_id_spt is not null then false -- this variant is impossibe to be
							when label_spt is not null and etl_id_spt is     null then false
							when label_spt is not null and etl_id_spt is not null then true
						end
							as check_spt,
						---------------------------------------------------------------------------
						case
							when label_spc is     null and etl_id_spc is     null then true
							when label_spc is     null and etl_id_spc is not null then false -- this variant is impossibe to be
							when label_spc is not null and etl_id_spc is     null then false
							when label_spc is not null and etl_id_spc is not null then true
						end
							as check_spc,
						---------------------------------------------------------------------------
						case
							when label_adt is     null and etl_id_adt is     null then true
							when label_adt is     null and etl_id_adt is not null then false -- this variant is impossibe to be
							when label_adt is not null and etl_id_adt is     null then false
							when label_adt is not null and etl_id_adt is not null then true
						end
							as check_adt,
						---------------------------------------------------------------------------
						case
							when label_adc is     null and etl_id_adc is     null then true
							when label_adc is     null and etl_id_adc is not null then false -- this variant is impossibe to be
							when label_adc is not null and etl_id_adc is     null then false
							when label_adc is not null and etl_id_adc is not null then true
						end
							as check_adc
				from
						w16
				)
		,w18 as	(
				select
						w17.*,
						case
							when (check_spt = false or check_spc = false or check_adt = false or check_adc = false)
							then
								false
							else
								true
						end
							as check_variables
				from
						w17
				)
		select
				json_build_object
				(
				'country', _res_country,
				'target_variable', _etl_id,
				'variables',json_agg(json_build_object(
					'panel', w18.panel__label,
					'reference_year_set', w18.reference_year_set__label,
					'sub_population_etl_id', w18.etl_id_spt__not_null,
					'sub_population_category_etl_id', w18.etl_id_spc__not_null,
					'area_domain_etl_id', w18.etl_id_adt__not_null,
					'area_domain_category_etl_id', w18.etl_id_adc__not_null,
					'check_variables',w18.check_variables))
				)
		from w18
		into _res;

		-----------------------------------------------------------------------
		-- check that all records of check_variables in internal argument _res
		-- in element variables are TRUE
		with
		w1 as	(
				select json_array_elements(_res->'variables') as s
				)
		,w2 as	(
				select
						(s->>'check_variables')::boolean as check_variables
				from
						w1
				)
		select count(w2.*) from w2 where w2.check_variables = false
		into _check_variables;

		if _check_variables > 0
		then
			raise exception 'Error 04: fn_etl_get_variables: For input aruguments: _refyearset2panel_mapping = % and _id_t_etl_target_variable = % was not ETL yet some of area domain type, area domain category, sub population type or sub population category into target DB!',_refyearset2panel_mapping, _id_t_etl_target_variable;
		end if;
		-----------------------------------------------------------------------

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_variables(integer[], integer) is
'Function returns variables for given input arguments.';

grant execute on function target_data.fn_etl_get_variables(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_export_variable" schema="target_data" src="functions/etl/fn_etl_export_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_variable(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_variable
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_categorization_setups		integer[];
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_variable: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_variable: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 04: fn_etl_export_variable: Internal argument _categorization_setups must not by NULL!';
		end if;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);

		with
		w1 as	(
				select
						id,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest(_categorization_setups))
				)
		,w2 as	(
				select
						w1.*,
						cltv.ldsity_object_type as core_or_division
				from
						w1
						inner join target_data.cm_ldsity2target_variable as cltv on w1.ldsity2target_variable = cltv.id
				)
		-------------------------
		,w10a as	(select * from w2 where core_and_division = false)
		,w10b as	(select * from w2 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w2 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w10a
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					core_or_division
					from w12b
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w12a
					)
		-------------------------
		,w4 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w13 order by categorization_setup
				)
		,w5 as	(
				select * from w4 where id_spt is null and id_spc is null and id_adt is null and id_adc is null -- choosing of row without distinction
				)
		,w6 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from (select * from w4 where categorization_setup is distinct from (select categorization_setup from w5)) as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(t.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(t.id_adt,tead.area_domain)
				
				)
		,w7 as	(
				select
						w6.*,
						tespc.etl_id as etl_id_spc,
						teadc.etl_id as etl_id_adc
				from w6
				
				left join	target_data.t_etl_sub_population_category as tespc
				on  w6.id_sub_population = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w6.id_spc,tespc.sub_population_category)
				
				left join	target_data.t_etl_area_domain_category as teadc
				on  w6.id_area_domain = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w6.id_adc,teadc.area_domain_category)
				)
		,w8 as	(
				select categorization_setup, _etl_id as etl_id_tv, 0 as etl_id_spc, 0 as etl_id_adc from w5 -- row without distinction 
				union all
				select
						categorization_setup,
						_etl_id as etl_id_tv,
						case when etl_id_spc is null then 0 else etl_id_spc end as etl_id_spc,
						case when etl_id_adc is null then 0 else etl_id_adc end as etl_id_adc
				from
						w7 order by categorization_setup
				)
		select
			json_agg(json_build_object(
				'target_variable', w8.etl_id_tv,
				'sub_population_category', w8.etl_id_spc, 
				'area_domain_category', w8.etl_id_adc))
		from
				w8 into _res;
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_variable(integer[], integer) is
'Function returns records for ETL t_variable table.';

grant execute on function target_data.fn_etl_export_variable(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_export_variable_hierarchy" schema="target_data" src="functions/etl/fn_etl_export_variable_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_variable_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_variable_hierarchy(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_variable_hierarchy
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_categorization_setups		integer[];
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_variable_hierarchy: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_variable_hierarchy: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_etl_export_variable_hierarchy: Internal argument _categorization_setups must not by NULL!';
		end if;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);

		-------------------------------------------------------------
		refresh materialized view target_data.v_variable_hierarchy_internal;
		-------------------------------------------------------------
		
		with
		w_etl_ad as		(
						select * from target_data.t_etl_area_domain	where export_connection = _export_connection
						)
		,w_etl_adc as	(
						select * from target_data.t_etl_area_domain_category where etl_area_domain in (select id from w_etl_ad)
						)
		,w_etl_sp as	(
						select * from target_data.t_etl_sub_population where export_connection = _export_connection
						)
		,w_etl_spc as	(
						select * from target_data.t_etl_sub_population_category where etl_sub_population in (select id from w_etl_sp)
						)
		,w1 as	(
				select
						id,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in	(select unnest(_categorization_setups))
				)
		,w2 as	(
				select
						w1.*,
						cltv.ldsity_object_type as core_or_division
				from
						w1
						inner join target_data.cm_ldsity2target_variable as cltv on w1.ldsity2target_variable = cltv.id
				)
		-------------------------
		,w10a as	(select * from w2 where core_and_division = false)
		,w10b as	(select * from w2 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w2 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w10a
					union all			
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					core_or_division
					from w12b
					union all
					select id, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w12a
					)
		-------------------------
		,w4 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w13 order by categorization_setup
				)
		,w5 as	(	
				select
						node as variable_superior,
						panel, reference_year_set,
						unnest(edges) as variable
				from
						target_data.v_variable_hierarchy
				where
						node in (select unnest(_categorization_setups))
				)
		,w6 as	(
				select
						_etl_id as etl_id_target_variable,
						w5.variable,
						w5.panel, w5.reference_year_set,
						w5.variable_superior,
						w4b.id_spt,
						w4a.id_spt as id_spt_sup,
						w4b.id_spc,
						w4a.id_spc as id_spc_sup,
						w4b.id_adt,
						w4a.id_adt as id_adt_sup,
						w4b.id_adc,
						w4a.id_adc as id_adc_sup
				from		w5
				left join	w4 as w4a	on w5.variable_superior = w4a.categorization_setup
				left join	w4 as w4b	on w5.variable = w4b.categorization_setup
				)
		,w15a as	(
					select t1.id_spt, w_etl_sp.id as id_sub_population_podrizene from
					(select distinct id_spt from w6 where id_spt is not null) as t1
					inner join w_etl_sp	on target_data.fn_etl_array_compare(t1.id_spt,w_etl_sp.sub_population)
					)
		,w15b as	(
					select t2.id_spt_sup, w_etl_sp.id as id_sub_population_nadrizene from
					(select distinct id_spt_sup from w6 where id_spt_sup is not null) as t2
					inner join w_etl_sp	on target_data.fn_etl_array_compare(t2.id_spt_sup,w_etl_sp.sub_population)
					)
		,w15c as	(
					select t3.id_adt, w_etl_ad.id as id_area_domain_podrizene from
					(select distinct id_adt from w6 where id_adt is not null) as t3
					inner join w_etl_ad	on target_data.fn_etl_array_compare(t3.id_adt,w_etl_ad.area_domain)
					)
		,w15d as	(
					select t4.id_adt_sup, w_etl_ad.id as id_area_domain_nadrizene from
					(select distinct id_adt_sup from w6 where id_adt_sup is not null) as t4
					inner join w_etl_ad	on target_data.fn_etl_array_compare(t4.id_adt_sup,w_etl_ad.area_domain)
					)					
		,w7a as	(
				select
						w6.*,
						w15a.id_sub_population_podrizene,
						w15b.id_sub_population_nadrizene,
						w15c.id_area_domain_podrizene,
						w15d.id_area_domain_nadrizene
				from
						w6
						left join w15a on w6.id_spt = w15a.id_spt
						left join w15b on w6.id_spt_sup = w15b.id_spt_sup
						left join w15c on w6.id_adt = w15c.id_adt
						left join w15d on w6.id_adt_sup = w15d.id_adt_sup
				)
		,w16a as	(
					select t1.id_sub_population_podrizene, t1.id_spc, w_etl_spc.etl_id as etl_id_spc from
					(select distinct id_sub_population_podrizene, id_spc from w7a where id_spc is not null) as t1
					inner join w_etl_spc on t1.id_sub_population_podrizene = w_etl_spc.etl_sub_population
					and target_data.fn_etl_array_compare(t1.id_spc,w_etl_spc.sub_population_category)
					)
		,w16b as	(
					select t2.id_sub_population_nadrizene, t2.id_spc_sup, w_etl_spc.etl_id as etl_id_spc_sup from
					(select distinct id_sub_population_nadrizene, id_spc_sup from w7a where id_spc_sup is not null) as t2
					inner join w_etl_spc on t2.id_sub_population_nadrizene = w_etl_spc.etl_sub_population
					and target_data.fn_etl_array_compare(t2.id_spc_sup,w_etl_spc.sub_population_category)
					)
		,w16c as	(
					select t3.id_area_domain_podrizene, t3.id_adc, w_etl_adc.etl_id as etl_id_adc from
					(select distinct id_area_domain_podrizene, id_adc from w7a where id_adc is not null) as t3
					inner join w_etl_adc on t3.id_area_domain_podrizene = w_etl_adc.etl_area_domain
					and target_data.fn_etl_array_compare(t3.id_adc,w_etl_adc.area_domain_category)
					)
		,w16d as	(
					select t4.id_area_domain_nadrizene, t4.id_adc_sup, w_etl_adc.etl_id as etl_id_adc_sup from
					(select distinct id_area_domain_nadrizene, id_adc_sup from w7a where id_adc_sup is not null) as t4
					inner join w_etl_adc on t4.id_area_domain_nadrizene = w_etl_adc.etl_area_domain
					and target_data.fn_etl_array_compare(t4.id_adc_sup,w_etl_adc.area_domain_category)
					)			
		,w8a as	(
				select
						w7a.*,
						w16a.etl_id_spc,
						w16b.etl_id_spc_sup,
						w16c.etl_id_adc,
						w16d.etl_id_adc_sup
				from
						w7a
						left join w16a on w7a.id_sub_population_podrizene = w16a.id_sub_population_podrizene and w7a.id_spc = w16a.id_spc
						left join w16b on w7a.id_sub_population_nadrizene = w16b.id_sub_population_nadrizene and w7a.id_spc_sup = w16b.id_spc_sup
						left join w16c on w7a.id_area_domain_podrizene = w16c.id_area_domain_podrizene and w7a.id_adc = w16c.id_adc
						left join w16d on w7a.id_area_domain_nadrizene = w16d.id_area_domain_nadrizene and w7a.id_adc_sup = w16d.id_adc_sup
				)			
		,w9 as	(
				select
						etl_id_target_variable as etl_id_tv,
						panel,
						reference_year_set,
						case when etl_id_spc is null then 0 else etl_id_spc end				as etl_id_spc,
						case when etl_id_spc_sup is null then 0 else etl_id_spc_sup end 	as etl_id_spc_sup,
						case when etl_id_adc is null then 0 else etl_id_adc end				as etl_id_adc,
						case when etl_id_adc_sup is null then 0 else etl_id_adc_sup end		as etl_id_adc_sup
				from
						w8a
				)
		select
			json_agg(json_build_object(
				'target_variable',			w9.etl_id_tv,
				'country',					(select c_country.label
										from sdesign.c_country
										inner join sdesign.t_strata_set on (t_strata_set.country = c_country.id)
										inner join sdesign.t_stratum on (t_stratum.strata_set = t_strata_set.id)
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'strata_set',					(select t_strata_set.strata_set
										from sdesign.t_strata_set
										inner join sdesign.t_stratum on (t_stratum.strata_set = t_strata_set.id)
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'stratum',					(select t_stratum.stratum
										from sdesign.t_stratum
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'reference_year_set',				(select reference_year_set from sdesign.t_reference_year_set where t_reference_year_set.id = w9.reference_year_set),
				'panel',					(select panel from sdesign.t_panel where t_panel.id = w9.panel),
				'sub_population_category',		w9.etl_id_spc,
				'sub_population_category_superior',	w9.etl_id_spc_sup,
				'area_domain_category',			w9.etl_id_adc,
				'area_domain_category_superior',	w9.etl_id_adc_sup)
				order by w9.etl_id_tv, w9.panel, w9.reference_year_set, w9.etl_id_spc, w9.etl_id_spc_sup, w9.etl_id_adc, w9.etl_id_adc_sup
			)
		from w9
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_variable_hierarchy(integer[], integer) is
'Function returns records for ETL t_variable_hierarchy table.';

grant execute on function target_data.fn_etl_export_variable_hierarchy(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_export_ldsity_values" schema="target_data" src="functions/etl/fn_etl_export_ldsity_values.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_ldsity_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_ldsity_values(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_ldsity_values
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_available_datasets			integer[];
		_categorization_setups		integer[];
		_res_available_datasets		json;
		_res_ldsity_values			json;
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_ldsity_values: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_ldsity_values: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;			


		-------------------------------------------------------------------------------------------
		-- LDSITY VALUES => get available data sets and categorization setups FROM t_ldsity_values
		-------------------------------------------------------------------------------------------
		with
		w1 as	(-- all categorization setups for input target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups reduced by combinations of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(-- list of IDs of available datasets from t_ldsity_values table
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		select array_agg(w3.available_datasets order by w3.available_datasets) from w3
		into _available_datasets;
		
		with
		w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select unnest(_available_datasets))
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_etl_export_ldsity_values: Internal argument _categorization_setups must not by NULL!';
		end if;
		-------------------------------------------------------------------------------------------


		-------------------------------------------------------------
		-- AVAILABLE DATASETS => get all available data sets
		-------------------------------------------------------------
		with		
		w1a as	(
				select
						cmltv.id as id_cm_ldsity2target2categorization_setup,
						cmltv.ldsity2target_variable,
						cmltv.adc2classification_rule,
						cmltv.spc2classification_rule,
						cmltv.categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', cmltv.spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', cmltv.spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', cmltv.adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', cmltv.adc2classification_rule)).id_category as id_adc_orig			
				from
						target_data.cm_ldsity2target2categorization_setup as cmltv
				where
						ldsity2target_variable in
							(
							select cm.id from target_data.cm_ldsity2target_variable as cm
							where target_variable = _target_variable
							)	
				)
		,w2a as	(-- list of categorization setups reduced by combinations of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select distinct w1a.categorization_setup from w1a)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(
				select
						w2a.id as id_available_datasets,
						w2a.panel,
						w2a.reference_year_set,
						w2a.ldsity_threshold,
						w1a.*
				from
						w2a inner join w1a on w2a.categorization_setup = w1a.categorization_setup
				)
		,w4 as	(
				select
						w3.*,
						cltv.ldsity_object_type as core_or_division
				from
						w3
						inner join target_data.cm_ldsity2target_variable as cltv on w3.ldsity2target_variable = cltv.id
				)
		-------------------------
		,w10a as	(select * from w4 where core_and_division = false)
		,w10b as	(select * from w4 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w4 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id_available_datasets, panel, reference_year_set, id_cm_ldsity2target2categorization_setup, ldsity2target_variable, ldsity_threshold,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w10a
					union all			
					select id_available_datasets, panel, reference_year_set, id_cm_ldsity2target2categorization_setup, ldsity2target_variable, ldsity_threshold,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					core_or_division
					from w12b
					union all
					select id_available_datasets, panel, reference_year_set, id_cm_ldsity2target2categorization_setup, ldsity2target_variable, ldsity_threshold,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w12a
					)
		,w6 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w13
				)
		,w7 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from w6 as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(t.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(t.id_adt,tead.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						case when tespc.etl_id is null then 0 else tespc.etl_id end as etl_id_spc,
						case when teadc.etl_id is null then 0 else teadc.etl_id end as etl_id_adc
				from w7
				
				left join target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc,tespc.sub_population_category)
				
				left join target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc,teadc.area_domain_category)
				)
		,w9 as	(
				select distinct t.t_panel__id, t.t_panel__panel, t.t_stratum__id, t.t_cluster_configuration__id, t.t_cluster_configuration__cluster_configuration,
				t.t_stratum__stratum, t.t_strata_set__id, t.t_strata_set__strata_set, t.c_country__id, t.c_country__label,
				t.t_reference_year_set__id, t.t_reference_year_set__reference_year_set, t.t_inventory_campaign__id, t.t_inventory_campaign__inventory_campaign
				from
						(
						select		
								t_panel.id as t_panel__id,
								t_panel.panel as t_panel__panel,
								t_cluster.id as t_cluster__id,
								t_cluster.cluster as t_cluster__cluster,
								f_p_plot.gid as f_p_plot__gid,
								f_p_plot.plot as f_p_plot__plot,
								t_cluster_configuration.id as t_cluster_configuration__id,
								t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
								t_stratum.id as t_stratum__id,
								t_stratum.stratum as t_stratum__stratum,
								t_strata_set.id as t_strata_set__id,
								t_strata_set.strata_set as t_strata_set__strata_set,
								c_country.id as c_country__id,
								c_country.label as c_country__label,
								t_reference_year_set.id as t_reference_year_set__id,
								t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
								t_inventory_campaign.id as t_inventory_campaign__id,
								t_inventory_campaign.inventory_campaign as t_inventory_campaign__inventory_campaign
						from
									sdesign.cm_refyearset2panel_mapping
						inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
						inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
						inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
						inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
						inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
						inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
						inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
						inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
						inner join	sdesign.c_country on t_strata_set.country = c_country.id
						inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
						inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
						where
								cm_refyearset2panel_mapping.id in (select unnest(_refyearset2panel_mapping))
						) as t
				)
		,w10 as	(
				select
						_etl_id as etl_id_tv,
						w9.c_country__label as country,
						w9.t_strata_set__strata_set as strata_set,
						w9.t_stratum__stratum as stratum,
						w9.t_panel__panel as panel,
						w9.t_reference_year_set__reference_year_set as reference_year_set,
						w9.t_cluster_configuration__cluster_configuration as cluster_configuration,
						w9.t_inventory_campaign__inventory_campaign as inventory_campaign,
						w8.etl_id_spc,
						w8.etl_id_adc,
						w13.ldsity_threshold
				from
						w13
				inner join	w8 	on w13.categorization_setup = w8.categorization_setup
				inner join	w9	on w13.panel = w9.t_panel__id and w13.reference_year_set = w9.t_reference_year_set__id
				)
		,w11 as	(
				select
						distinct
						w10.etl_id_tv,
						w10.country,
						w10.strata_set,
						w10.stratum,
						w10.panel,
						w10.reference_year_set,
						w10.cluster_configuration,
						w10.inventory_campaign,
						w10.etl_id_spc,
						w10.etl_id_adc,
						w10.ldsity_threshold
				from
						w10
				)		
		,w12 as	(
				select
						w11.etl_id_tv,
						w11.country,
						w11.strata_set,
						w11.stratum,
						w11.reference_year_set,
						w11.panel,
						w11.cluster_configuration,
						w11.inventory_campaign,
						w11.etl_id_spc,
						w11.etl_id_adc,
						w11.ldsity_threshold
				from
						w11
				order
				by		w11.country,
						w11.strata_set,
						w11.stratum,
						w11.reference_year_set,
						w11.panel,
						w11.cluster_configuration,
						w11.inventory_campaign,
						w11.etl_id_tv,
						w11.etl_id_spc,
						w11.etl_id_adc 
				)
		select
			json_agg(json_build_object(
				'target_variable',   		w12.etl_id_tv,
				'country',					w12.country,
				'strata_set',				w12.strata_set,
				'stratum',					w12.stratum,
				'reference_year_set',		w12.reference_year_set,
				'panel',					w12.panel,
				'cluster_configuration',	w12.cluster_configuration,
				'inventory_campaign',		w12.inventory_campaign,
				'sub_population_category',	w12.etl_id_spc,
				'area_domain_category',		w12.etl_id_adc,
				'ldsity_threshold',			w12.ldsity_threshold
				))
		from
				w12 into _res_available_datasets;
		-------------------------------------------------------------


		-------------------------------------------------------------
		-- LDSITY VALUES => get ldsity values
		-------------------------------------------------------------
		with
		w1 as	(
				select
						tad.id as id_available_datasets,
						tad.panel,
						tad.reference_year_set,
						tad.categorization_setup
				from
						target_data.t_available_datasets as tad
				where
						tad.id in (select unnest(_available_datasets))
				)
		,w2 as	(
				select t1.*, w1.*
				from target_data.t_ldsity_values as t1
				inner join w1 on t1.available_datasets = w1.id_available_datasets
				where t1.is_latest = true
				)
		,w3 as	(
				select
						id as id_cm_ldsity2target2categorization_setup,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig				
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest(_categorization_setups))
				)
		,w4 as	(
				select
						w3.*,
						cltv.ldsity_object_type as core_or_division
				from
						w3
						inner join target_data.cm_ldsity2target_variable as cltv on w3.ldsity2target_variable = cltv.id
				)
		-------------------------
		,w10a as	(select * from w4 where core_and_division = false)
		,w10b as	(select * from w4 where core_and_division = true and core_or_division = 200)
		,w10c as	(select * from w4 where core_and_division = true and core_or_division = 100)
		,w11a as	(select distinct id_spt_orig, id_spc_orig, categorization_setup from w10b)
		,w11b as	(select distinct id_adt_orig, id_adc_orig, categorization_setup from w10c)
		,w12a as	(
					select
							w10c.*,
							w11a.id_spt_orig as id_spt_orig_doplneni,
							w11a.id_spc_orig as id_spc_orig_doplneni
					from
							w10c left join w11a on w10c.categorization_setup = w11a.categorization_setup
					)
		,w12b as	(
					select
							w10b.*,
							w11b.id_adt_orig as id_adt_orig_doplneni,
							w11b.id_adc_orig as id_adc_orig_doplneni
					from
							w10b left join w11b on w10b.categorization_setup = w11b.categorization_setup
					)
		,w13 as 	(
					select id_cm_ldsity2target2categorization_setup, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w10a
					union all
					select id_cm_ldsity2target2categorization_setup, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig as id_spt,
					id_adt_orig_doplneni as id_adt,
					id_spc_orig as id_spc,
					id_adc_orig_doplneni as id_adc,
					core_or_division
					from w12b
					union all
					select id_cm_ldsity2target2categorization_setup, ldsity2target_variable,
					categorization_setup, core_and_division,
					id_spt_orig_doplneni as id_spt,
					id_adt_orig as id_adt,
					id_spc_orig_doplneni as id_spc,
					id_adc_orig as id_adc,
					core_or_division
					from w12a
					)
		-------------------------
		,w6 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w13
				)
		,w7 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from w6 as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(t.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(t.id_adt,tead.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						case when tespc.etl_id is null then 0 else tespc.etl_id end as etl_id_spc,
						case when teadc.etl_id is null then 0 else teadc.etl_id end as etl_id_adc
				from w7
				
				left join target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc,tespc.sub_population_category)
				
				left join target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc,teadc.area_domain_category)
				)
		,w9 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory_campaign as t_inventory_campaign__inventory_campaign
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_refyearset2panel_mapping))
				)
		,w10 as	(
				select
						_etl_id as etl_id_tv,
						w9.c_country__label as country,
						w9.t_strata_set__strata_set as strata_set,
						w9.t_stratum__stratum as stratum,
						w9.t_panel__panel as panel,
						w9.t_reference_year_set__reference_year_set as reference_year_set,
						w9.t_cluster__cluster as cluster,
						w9.t_cluster_configuration__cluster_configuration as cluster_configuration,
						w9.t_inventory_campaign__inventory_campaign as inventory_campaign,
						w9.f_p_plot__plot as plot,
						w8.etl_id_spc,
						w8.etl_id_adc,
						w2.value
				from
						w2
				inner join	w8 	on w2.categorization_setup = w8.categorization_setup
				inner join	w9	on w2.plot = w9.f_p_plot__gid
								and w2.panel = w9.t_panel__id
								and w2.reference_year_set = w9.t_reference_year_set__id
				)
		,w11 as	(
				select
						w10.etl_id_tv,
						w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster,
						w10.cluster_configuration,
						w10.inventory_campaign,						
						w10.plot,
						w10.etl_id_spc,
						w10.etl_id_adc,
						w10.value
				from
						w10
				order
				by		w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster,
						w10.cluster_configuration,
						w10.inventory_campaign,
						w10.etl_id_tv,
						w10.plot,
						w10.etl_id_spc,
						w10.etl_id_adc 
				)
		select
			json_agg(json_build_object(
				'target_variable',   		w11.etl_id_tv,
				'country',					w11.country,
				'strata_set',				w11.strata_set,
				'stratum',					w11.stratum,
				'reference_year_set',		w11.reference_year_set,
				'panel',					w11.panel,
				'cluster',					w11.cluster,
				'cluster_configuration',	w11.cluster_configuration,
				'inventory_campaign',		w11.inventory_campaign,
				'plot',						w11.plot,
				'sub_population_category',	w11.etl_id_spc,
				'area_domain_category',		w11.etl_id_adc,
				'value',					w11.value))
		from
				w11 into _res_ldsity_values;
		-------------------------------------------------------------


		-------------------------------------------------------------
		-- JOIN elements
		-------------------------------------------------------------
		select json_build_object
			(
				'available_datasets',_res_available_datasets,
				'ldsity_values',_res_ldsity_values
			)
		into _res;
		-------------------------------------------------------------
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_ldsity_values(integer[], integer) is
'Function returns records for ETL t_target_data and t_available_datasets table.';

grant execute on function target_data.fn_etl_export_ldsity_values(integer[], integer) to public;
-- </function>

