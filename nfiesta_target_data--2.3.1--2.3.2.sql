--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP MATERIALIZED VIEW target_data.v_variable_hierarchy;
-- <view name="v_variable_hierarchy" schema="target_data" src="views/v_variable_hierarchy.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--alter extension nfiesta_target_data drop view target_data.v_variable_hierarchy;
--DROP MATERIALIZED VIEW target_data.v_variable_hierarchy;

CREATE MATERIALIZED VIEW target_data.v_variable_hierarchy AS
with w_categorization_setups as (
		select
		t_categorization_setup.id as cs_id,
		t_available_datasets.panel,
		t_available_datasets.reference_year_set,
		c_target_variable.id as tv_id 
		--, array_agg(distinct spc_id) filter (where spc_id is not null)
		, max(spc_id) as spc_id, max(sp_id) as sp_id, max(adc_id) as adc_id, max(ad_id) as ad_id
		, max(c_target_variable.label) as tv_label, max(spc_label) as spc_label, max(sp_label) as sp_label, max(adc_label) as adc_label, max(ad_label) as ad_label
	from target_data.t_categorization_setup
	inner join target_data.t_available_datasets on (t_available_datasets.categorization_setup = t_categorization_setup.id)
	inner join target_data.cm_ldsity2target2categorization_setup ON cm_ldsity2target2categorization_setup.categorization_setup = t_categorization_setup.id
	inner join target_data.cm_ldsity2target_variable ON cm_ldsity2target_variable.id = cm_ldsity2target2categorization_setup.ldsity2target_variable
	inner join target_data.c_target_variable ON c_target_variable.id = cm_ldsity2target_variable.target_variable
	, lateral (	select 
					array_agg(c_sub_population_category.label order by spc2clr.n) as spc_label,
					array_agg(c_sub_population.label order by spc2clr.n) as sp_label,
					array_agg(c_sub_population_category.id order by spc2clr.n) as spc_id,
					array_agg(c_sub_population.id order by spc2clr.n) as sp_id
				from unnest(cm_ldsity2target2categorization_setup.spc2classification_rule) with ordinality as spc2clr(spc2classification_rule, n)
				inner join target_data.cm_spc2classification_rule on (spc2clr.spc2classification_rule = cm_spc2classification_rule.id)
				inner join target_data.c_sub_population_category ON c_sub_population_category.id = cm_spc2classification_rule.sub_population_category
				inner join target_data.c_sub_population ON c_sub_population.id = c_sub_population_category.sub_population
			  ) as spc
	, lateral (	select 
					array_agg(c_area_domain_category.label order by adc2clr.n) as adc_label,
					array_agg(c_area_domain.label order by adc2clr.n) as ad_label,
					array_agg(c_area_domain_category.id order by adc2clr.n) as adc_id,
					array_agg(c_area_domain.id order by adc2clr.n) as ad_id
				from unnest(cm_ldsity2target2categorization_setup.adc2classification_rule) with ordinality as adc2clr(adc2classification_rule, n)
				inner join target_data.cm_adc2classification_rule on (adc2clr.adc2classification_rule = cm_adc2classification_rule.id)
				inner join target_data.c_area_domain_category ON c_area_domain_category.id = cm_adc2classification_rule.area_domain_category
				inner join target_data.c_area_domain ON c_area_domain.id = c_area_domain_category.area_domain
			  ) as adc
	group by 	t_categorization_setup.id, c_target_variable.id,
				t_available_datasets.panel, 
				t_available_datasets.reference_year_set
	order by t_categorization_setup.id
)
, w_sup_inf_pairs as (
	select 
		cs_sup.cs_id as cs_id_sup, cs_sup.panel, cs_sup.reference_year_set,
		cs_inf.cs_id as cs_id_inf
		, cs_inf.sp_label as sp_label_inf, cs_inf.ad_label as ad_label_inf
		, cs_sup.tv_label as tv_label_sup, cs_sup.spc_label as spc_label_sup, cs_sup.adc_label as adc_label_sup
		, cs_inf.tv_label as tv_label_inf, cs_inf.spc_label as spc_label_inf, cs_inf.adc_label as adc_label_inf
	from w_categorization_setups 		as cs_sup
	inner join w_categorization_setups 	as cs_inf on (cs_sup.tv_id = cs_inf.tv_id) and
			(cs_sup.panel = cs_inf.panel and cs_sup.reference_year_set = cs_inf.reference_year_set) and
			(
				((cs_sup.adc_id = cs_inf.adc_id OR cs_sup.adc_id IS NULL AND cs_inf.adc_id IS NULL) AND
					((cs_sup.spc_id != cs_inf.spc_id AND cs_sup.spc_id <@ cs_inf.spc_id AND (array_length(cs_inf.spc_id,1) = array_length(cs_sup.spc_id,1) + 1)) OR
					 (cs_sup.spc_id IS NULL AND cs_inf.spc_id IS NOT NULL AND array_length(cs_inf.spc_id,1) = 1)))
				OR
				((cs_sup.spc_id = cs_inf.spc_id OR cs_sup.spc_id IS NULL AND cs_inf.spc_id IS NULL) AND
					((cs_sup.adc_id != cs_inf.adc_id AND cs_sup.adc_id <@ cs_inf.adc_id AND (array_length(cs_inf.adc_id,1) = array_length(cs_sup.adc_id,1) + 1)) OR
					 (cs_sup.adc_id IS NULL AND cs_inf.adc_id IS NOT NULL AND array_length(cs_inf.adc_id,1) = 1))))
	order by cs_sup.cs_id, cs_inf.cs_id
)
select 
	panel, reference_year_set,
	cs_id_sup as node,
	array_agg(cs_id_inf order by cs_id_inf) AS edges
	--, tv_label_sup
	, spc_label_sup, adc_label_sup
	--, array_agg(tv_label_inf order by cs_id_inf) AS tv_label_inf
	, array_agg(array_to_string(spc_label_inf, '~') order by cs_id_inf) AS spc_label_inf
	, array_agg(array_to_string(adc_label_inf, '~') order by cs_id_inf) AS adc_label_inf
from w_sup_inf_pairs
group by panel, reference_year_set, cs_id_sup, tv_label_sup, spc_label_sup, adc_label_sup
	, sp_label_inf, ad_label_inf
order by panel, reference_year_set, cs_id_sup
;

GRANT SELECT ON TABLE target_data.v_variable_hierarchy TO PUBLIC;

--------------------------------------automatic view refresh (https://stackoverflow.com/a/29447328)

CREATE OR REPLACE FUNCTION target_data.tg_refresh_v_variable_hierarchy()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW /*CONCURRENTLY */target_data.v_variable_hierarchy;
    RETURN NULL;
END;
$$;

ALTER TABLE target_data.v_variable_hierarchy OWNER TO app_nfiesta;
GRANT SELECT ON TABLE 				target_data.v_variable_hierarchy TO PUBLIC;
GRANT SELECT ON TABLE				 target_data.v_variable_hierarchy TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	 target_data.v_variable_hierarchy TO app_nfiesta_mng;

--target_data.t_categorization_setup
drop trigger if exists trg__t_categorization_setup__refresh_v_variable_hierarchy ON target_data.t_categorization_setup;
CREATE TRIGGER trg__t_categorization_setup__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.t_categorization_setup
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_ldsity2target2categorization_setup
drop trigger if exists trg__cm_ldsity2target2categorization_setup__refresh_v_variable_hierarchy ON target_data.cm_ldsity2target2categorization_setup;
CREATE TRIGGER trg__cm_ldsity2target2categorization_setup__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_ldsity2target2categorization_setup
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_ldsity2target_variable
drop trigger if exists trg__cm_ldsity2target_variable__refresh_v_variable_hierarchy ON target_data.cm_ldsity2target_variable;
CREATE TRIGGER trg__cm_ldsity2target_variable__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_ldsity2target_variable
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_target_variable
drop trigger if exists trg__c_target_variable__refresh_v_variable_hierarchy ON target_data.c_target_variable;
CREATE TRIGGER trg__c_target_variable__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_target_variable
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_spc2classification_rule
drop trigger if exists trg__cm_spc2classification_rule__refresh_v_variable_hierarchy ON target_data.cm_spc2classification_rule;
CREATE TRIGGER trg__cm_spc2classification_rule__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_spc2classification_rule
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_sub_population_category
drop trigger if exists trg__c_sub_population_category__refresh_v_variable_hierarchy ON target_data.c_sub_population_category;
CREATE TRIGGER trg__c_sub_population_category__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_sub_population_category
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_sub_population
drop trigger if exists trg__c_sub_population__refresh_v_variable_hierarchy ON target_data.c_sub_population;
CREATE TRIGGER trg__c_sub_population__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_sub_population
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_adc2classification_rule
drop trigger if exists trg__cm_adc2classification_rule__refresh_v_variable_hierarchy ON target_data.cm_adc2classification_rule;
CREATE TRIGGER trg__cm_adc2classification_rule__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_adc2classification_rule
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_area_domain_category
drop trigger if exists trg__c_area_domain_category__refresh_v_variable_hierarchy ON target_data.c_area_domain_category;
CREATE TRIGGER trg__c_area_domain_category__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_area_domain_category
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_area_domain
drop trigger if exists trg__c_area_domain__refresh_v_variable_hierarchy ON target_data.c_area_domain;
CREATE TRIGGER trg__c_area_domain__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_area_domain
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

-- </function>

-- <function name="fn_check_t_ldsity_values" schema="target_data" src="functions/fn_check_t_ldsity_values.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_check_t_ldsity_values
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_t_ldsity_values();

CREATE OR REPLACE FUNCTION target_data.fn_check_t_ldsity_values() RETURNS TRIGGER AS $src$
    DECLARE
		_cs int[];
		_vars int[];
		_plots int[];
		_refyearsets int[];
		_available_datasets int[];
		_errp json;
    BEGIN
        IF (TG_OP = 'DELETE' or TG_OP = 'UPDATE' or TG_OP = 'INSERT') THEN
			IF (TG_TABLE_NAME = 't_ldsity_values') THEN
				with w_vars as (
						select array_prepend(node, edges) as vs 
					from trans_table
					inner join target_data.t_available_datasets on trans_table.available_datasets = t_available_datasets.id
					inner join target_data.v_variable_hierarchy 
						on ((t_available_datasets.categorization_setup = v_variable_hierarchy.node 
							or t_available_datasets.categorization_setup = any (v_variable_hierarchy.edges))
						and (t_available_datasets.panel = v_variable_hierarchy.panel
							and t_available_datasets.reference_year_set = v_variable_hierarchy.reference_year_set)
					)
				)
				, w_vars_un as (
						select unnest(vs) as v from w_vars
				)
				select array_agg(distinct v) into _vars from w_vars_un;
				select array_agg(distinct categorization_setup) into _cs from trans_table inner join target_data.t_available_datasets on trans_table.available_datasets = t_available_datasets.id;
				select array_agg(distinct plot) into _plots from trans_table;
				select array_agg(distinct reference_year_set) into _refyearsets from target_data.t_available_datasets where id in (select distinct available_datasets from trans_table);
				select array_agg(distinct available_datasets) into _available_datasets from trans_table;
			ELSIF (TG_TABLE_NAME = 't_available_datasets') THEN
                                with w_vars as (
                                                select array_prepend(node, edges) as vs
                                        from trans_table
                                        inner join target_data.v_variable_hierarchy
                                                on ((trans_table.categorization_setup = v_variable_hierarchy.node
                                                        or trans_table.categorization_setup = any (v_variable_hierarchy.edges))
						and (trans_table.panel = v_variable_hierarchy.panel
							and trans_table.reference_year_set = v_variable_hierarchy.reference_year_set)
					)
                                )
                                , w_vars_un as (
                                                select unnest(vs) as v from w_vars
                                )
                                select array_agg(distinct v) into _vars from w_vars_un;
				select array_agg(distinct categorization_setup) into _cs from trans_table;
				select array_agg(distinct f_p_plot.gid) into _plots
					from trans_table
					inner join sdesign.t_panel on (trans_table.panel = t_panel.id)
					inner join sdesign.cm_cluster2panel_mapping on (t_panel.id = cm_cluster2panel_mapping.panel)
					inner join sdesign.t_cluster on (cm_cluster2panel_mapping.cluster = t_cluster.id)
					inner join sdesign.f_p_plot on (t_cluster.id = f_p_plot.cluster);
				select array_agg(distinct reference_year_set) into _refyearsets from target_data.t_available_datasets where id in (select distinct id from trans_table);
				select array_agg(distinct id) into _available_datasets from trans_table;
			ELSE
				RAISE EXCEPTION 'fn_check_t_ldsity_values -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
			END IF;

			if _vars is not null
			then
				--raise notice '%		fn_check_t_ldsity_values -- UPDATE CHECK START -- select target_data.fn_add_plot_target_attr(%, %, %, %, %);', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', _vars, _plots, _refyearsets, 1e-6, true;
				with w_err as (
					select target_data.fn_add_plot_target_attr(_vars, _plots, _refyearsets, 1e-6, true) as fn_err
				)
				, w_err_t as (
					select (fn_err).* from w_err
				)
				, w_err_json as (
					select 
						json_build_object(
							'plot'					, plot,
							'reference_year_set'	, reference_year_set,
							'variable'				, variable,
							'ldsity'				, ldsity,
							'ldsity_sum'			, ldsity_sum,
							'variables_def'			, variables_def,
							'variables_found'		, variables_found,
							'diff'					, diff
						) as errj
					from w_err_t
				)
				select json_agg(errj) into _errp from w_err_json;
				--raise notice '%		fn_check_t_ldsity_values -- UPDATE CHECK STOP', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague';
				IF 
					(json_array_length(_errp) > 0) 
					THEN RAISE EXCEPTION 'fn_check_t_ldsity_values -- % -- % -- plot level local densities are not additive:
						checked categorization setups: %, variables in additivity set: %, checked plots: %, 
						found err plots: 
						%', TG_TABLE_NAME, TG_OP, _cs, _vars, _plots, jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE WARNING 'fn_check_t_ldsity_values -- % -- % -- additivity check was skiped: 
						* 0 rows edited (next line displays NULL)
						* corresponding variable hierarchy not found (next line displays array)
						trans_table available_datasets: %', TG_TABLE_NAME, TG_OP, _available_datasets;
			end if;
	ELSE
		RAISE EXCEPTION 'fn_check_t_ldsity_values -- trigger operation not known: %', TG_OP;
	END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$src$ LANGUAGE plpgsql;

drop trigger if exists trg__ldsity_values__ins ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__ins
    AFTER INSERT ON target_data.t_ldsity_values
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__ldsity_values__upd ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__upd
    AFTER UPDATE ON target_data.t_ldsity_values
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__ldsity_values__del ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__del
    AFTER DELETE ON target_data.t_ldsity_values
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__available_datasets__ins ON target_data.t_available_datasets;
CREATE TRIGGER trg__available_datasets__ins
    AFTER INSERT ON target_data.t_available_datasets
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__available_datasets__upd ON target_data.t_available_datasets;
CREATE TRIGGER trg__available_datasets__upd
    AFTER UPDATE ON target_data.t_available_datasets
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__available_datasets__del ON target_data.t_available_datasets;
CREATE TRIGGER trg__available_datasets__del
    AFTER DELETE ON target_data.t_available_datasets
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

-- </function>

-- <function name="fn_add_plot_target_attr" schema="target_data" src="functions/additivity/fn_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_add_plot_target_attr
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean);

CREATE OR REPLACE FUNCTION target_data.fn_add_plot_target_attr
(
	IN variables integer[],
	IN plots integer[] default NULL,
	IN refyearsets integer[] default NULL,
	IN min_diff double precision default 0.0,
	IN include_null_diff boolean default true
)
  RETURNS TABLE
(
	plot				integer,
	reference_year_set	integer,
	variable			integer,
	ldsity				double precision,
	ldsity_sum			double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff				double precision	
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text_v text;
	_array_text_p text;
	_array_text_r text;

BEGIN
	--------------------------------QUERY--------------------------------
	_array_text_v := concat(quote_literal(variables::text), '::integer[]');

	if plots is not null then
		_array_text_p := concat(' AND f_p_plot.gid = ANY (', quote_literal(plots::text), '::integer[])');
	else
		_array_text_p := '';
	end if;

	if refyearsets is not null then
		_array_text_r := concat(' AND t_available_datasets.reference_year_set = ANY (', quote_literal(refyearsets::text), '::integer[])');
	else
		_array_text_r := '';
	end if;

	_complete_query :=
	'
	with
	w_plot_var as not materialized
					(
					select
							f_p_plot.gid as plot,
							t_available_datasets.categorization_setup as variable,
							t_available_datasets.panel,
							t_available_datasets.reference_year_set,
							coalesce(t_ldsity_values.value, 0) as value
					from sdesign.f_p_plot
					inner join sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
					inner join sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
					inner join sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
					inner join target_data.t_available_datasets on (t_panel.id = t_available_datasets.panel) -- select * from target_data.t_available_datasets
					inner join target_data.t_categorization_setup on (t_available_datasets.categorization_setup = t_categorization_setup.id)
					left join target_data.t_ldsity_values on (
						f_p_plot.gid = t_ldsity_values.plot
						and t_available_datasets.id = t_ldsity_values.available_datasets
						and t_ldsity_values.is_latest)
					WHERE t_available_datasets.categorization_setup = ANY (' || _array_text_v || ')	
							' || _array_text_p || '
							' || _array_text_r || '
					)
	,w_node_sum as	(
					select
						plot,
						w_plot_var.reference_year_set,
						w_plot_var.variable,
						value as node_sum,
						v_variable_hierarchy.node,
						v_variable_hierarchy.edges as edges_def
					from w_plot_var
					inner join target_data.v_variable_hierarchy on (v_variable_hierarchy.node = w_plot_var.variable and 
											v_variable_hierarchy.panel = w_plot_var.panel and
											v_variable_hierarchy.reference_year_set = w_plot_var.reference_year_set)
					where (v_variable_hierarchy.edges <@ ' || _array_text_v || ')
					)
	,w_edge_sum as	(
					select
						w_node_sum.plot,
						w_node_sum.reference_year_set,
						w_node_sum.node,
						w_node_sum.node_sum,
						w_node_sum.edges_def,
						array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
						sum(w_plot_var.value) as edges_sum
					from w_node_sum
					left join w_plot_var on (
						w_plot_var.plot = w_node_sum.plot
						and w_plot_var.reference_year_set = w_node_sum.reference_year_set
						and w_plot_var.variable = any(w_node_sum.edges_def))
					group by w_node_sum.plot, w_node_sum.reference_year_set,
						w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
					)
	,w_diff as		(
					select
						plot,
						reference_year_set,
						node		as variable,
						node_sum	as ldsity,
						edges_sum	as ldsity_sum,
						edges_def	as variables_def,
						edges_found	as variables_found,
						case
							when edges_sum != 0.0 and node_sum = 0.0 then 1.0
							when node_sum = 0.0 and edges_sum = 0.0 then 0
							else abs( (node_sum - edges_sum) / node_sum )
						end as diff
					from w_edge_sum
					)
	select * from w_diff
	where
		(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
		or
		(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
	order by diff desc
	;
	';
	--raise notice '%		fn_add_plot_target_attr -- %', TimeOfDay(), _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
  
comment on function target_data.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean) is
'Function showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Array of plots -- plots (FKEY to f_p_plot.gid). If NULL all available plots are checked.
 * Array of reference year sets -- reference year sets (FKEY to t_reference_year_set.id). If NULL all available reference year sets are checked.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Reference year set. FKEY to t_reference_year_set.id.
 * Target total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION target_data.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean) TO PUBLIC;

-- </function>

-- <function name="fn_etl_export_variable_hierarchy" schema="target_data" src="functions/etl/fn_etl_export_variable_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_variable_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_variable_hierarchy(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_variable_hierarchy
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_categorization_setups		integer[];
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_variable_hierarchy: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_variable_hierarchy: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_etl_export_variable_hierarchy: Internal argument _categorization_setups must not by NULL!';
		end if;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);

		-------------------------------------------------------------
		refresh materialized view target_data.v_variable_hierarchy;
		-------------------------------------------------------------
		
		with
		w1 as	(
				select
						id,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in	(select unnest(_categorization_setups))
				)
		,w2 as	(
				select
						w1.*,
						cltv.ldsity_object_type as core_or_division
				from
						w1
						inner join target_data.cm_ldsity2target_variable as cltv on w1.ldsity2target_variable = cltv.id
				)
		,w3 as	(				
				select
						w2.*,
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w2a.id_spt_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w2a.id_spc_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w2a.id_adt_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w2a.id_adc_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adc
				from
						w2
				)
		,w4 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w3 order by categorization_setup
				)
		,w5 as	(	
				select
						node as variable_superior,
						panel, reference_year_set,
						unnest(edges) as variable
				from
						target_data.v_variable_hierarchy
				where
						node in (select unnest(_categorization_setups))
				)	
		,w6 as	(
				select
						_etl_id as etl_id_target_variable,
						w5.variable,
						w5.panel, w5.reference_year_set,
						w5.variable_superior,
						w4b.id_spt,
						w4a.id_spt as id_spt_sup,
						w4b.id_spc,
						w4a.id_spc as id_spc_sup,
						w4b.id_adt,
						w4a.id_adt as id_adt_sup,
						w4b.id_adc,
						w4a.id_adc as id_adc_sup
				from		w5
				left join	w4 as w4a	on w5.variable_superior = w4a.categorization_setup
				left join	w4 as w4b	on w5.variable = w4b.categorization_setup
				order by	w5.variable_superior, w5.variable
				)
		,w7 as	(
				select
						w6.*,
						tesp.id 	as id_sub_population_podrizene,
						tesp_sup.id as id_sub_population_nadrizene,
						tead.id 	as id_area_domain_podrizene,
						tead_sup.id	as id_area_domain_nadrizene
				from
						w6
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(w6.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp_sup
				on target_data.fn_etl_array_compare(w6.id_spt_sup,tesp_sup.sub_population)		
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(w6.id_adt,tead.area_domain)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead_sup
				on target_data.fn_etl_array_compare(w6.id_adt_sup,tead_sup.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						tespc.etl_id		as etl_id_spc,
						tespc_sup.etl_id	as etl_id_spc_sup,
						teadc.etl_id		as etl_id_adc,
						teadc_sup.etl_id	as etl_id_adc_sup
				from
						w7
				
				left join	target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population_podrizene = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc,tespc.sub_population_category)		
		
				left join	target_data.t_etl_sub_population_category as tespc_sup
				on  w7.id_sub_population_nadrizene = tespc_sup.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc_sup,tespc_sup.sub_population_category)			
			
				left join	target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain_podrizene = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc,teadc.area_domain_category)
		
				left join	target_data.t_etl_area_domain_category as teadc_sup
				on  w7.id_area_domain_nadrizene = teadc_sup.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc_sup,teadc_sup.area_domain_category)
				)
		,w9 as	(
				select
						etl_id_target_variable							as etl_id_tv,
						panel, reference_year_set,
						case when etl_id_spc is null then 0 else etl_id_spc end			as etl_id_spc,
						case when etl_id_spc_sup is null then 0 else etl_id_spc_sup end 	as etl_id_spc_sup,
						case when etl_id_adc is null then 0 else etl_id_adc end			as etl_id_adc,
						case when etl_id_adc_sup is null then 0 else etl_id_adc_sup end		as etl_id_adc_sup
				from
						w8
				)
		select
			json_agg(json_build_object(
				'target_variable',			w9.etl_id_tv,
				'country',					(select c_country.label
										from sdesign.c_country
										inner join sdesign.t_strata_set on (t_strata_set.country = c_country.id)
										inner join sdesign.t_stratum on (t_stratum.strata_set = t_strata_set.id)
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'strata_set',					(select t_strata_set.strata_set
										from sdesign.t_strata_set
										inner join sdesign.t_stratum on (t_stratum.strata_set = t_strata_set.id)
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'stratum',					(select t_stratum.stratum
										from sdesign.t_stratum
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'reference_year_set',				(select reference_year_set from sdesign.t_reference_year_set where t_reference_year_set.id = w9.reference_year_set),
				'panel',					(select panel from sdesign.t_panel where t_panel.id = w9.panel),
				'sub_population_category',		w9.etl_id_spc,
				'sub_population_category_superior',	w9.etl_id_spc_sup,
				'area_domain_category',			w9.etl_id_adc,
				'area_domain_category_superior',	w9.etl_id_adc_sup))
		from
				w9 into _res;
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_variable_hierarchy(integer[], integer) is
'Function returns records for ETL t_variable_hierarchy table.';

grant execute on function target_data.fn_etl_export_variable_hierarchy(integer[], integer) to public;

-- </function>
