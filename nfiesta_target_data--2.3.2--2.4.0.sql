--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations_internal(integer, boolean);
DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(integer);


-- <function name="fn_get_eligible_panel_refyearset_combinations_internal" schema="target_data" src="functions/fn_get_eligible_panel_refyearset_combinations_internal.sql">
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_eligible_panel_refyearset_combinations_internal 
--------------------------------------------------------------------------------
-- ALTER EXTENSION nfiesta_target_data DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations_internal(INT, INT [], BOOL);
-- DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations_internal(INT, INT [], BOOL);

CREATE OR REPLACE FUNCTION 
target_data.fn_get_eligible_panel_refyearset_combinations_internal(_target_variable INT, _categorization_setup INT [],  _use_negative BOOL)
RETURNS INT[] AS 
$$
DECLARE 

	_refyearset2panel4ldsity INT[]; -- set of panel and refrerence yearset combinations for which local densities are available
	_refyearset2panel4ldsity_spc INT[]; -- set of panel and reference yearset combinations for which values local densities are ETL-constrained to the specified subpopulations
	_refyearset2panel4ldsity_adc INT[]; -- set of panel and reference yearset combinations for which values local densities are ETL-constrained to the specified areal domain  
	_refyearset2panel4ldsity2target_variable_spc INT[]; -- set of panel and reference yearset combinations for which values local densities were constrained to the specified subpopulations by the user   
	_refyearset2panel4ldsity2target_variable_adc INT[]; -- set of panel and reference yearset combinations for which values local densities were constrained to the specified area domains by the user
	_refyearset2panel4spc INT[]; -- set of panel and reference yearset combinations for which values local densities can be classified using a particular set of subpopulation categories
	_refyearset2panel4adc INT[]; -- set of panel and reference yearset combinations for which values local densities can be classified using a particular set of area domain categories 
	_refyearset2panel_common INT[]; -- set of panel and reference yearset combinations for which values local densities of a target variable can be calculated
	_rules_present bool; -- logical value coding whether there are some rules defined    

BEGIN
	
	-- raising an exception on NULL input
	IF _target_variable IS NULL then 
		RAISE EXCEPTION 'Input parameter _target_variable INT not given, but it is required!';
	END IF; 

	-- raising an exception on NULL input
	IF _categorization_setup IS NULL then 
		RAISE EXCEPTION 'Input parameter _categorization_setup INT[] not given, but it is required!';
	END IF; 
	
	-- raising an exception on NULL input
	IF _use_negative IS NULL then 
		RAISE EXCEPTION 'Input parameter _use_negative INT not given, but it is required!';
	END IF; 

	-- assigning _refyearset2panel4ldsity
	WITH w_ldsity AS (
		SELECT DISTINCT 
			t2.id AS ldsity, 
			t1."version" 
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN 
			target_data.c_ldsity AS t2
			ON	
		 	t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative AND
			t1.ldsity = t2.id
	),
	w_refyearset2panel AS (
		SELECT 
			t2.refyearset2panel 
		FROM 
			w_ldsity  AS t1
		INNER JOIN 
			target_data.cm_ldsity2panel_refyearset_version AS t2
			ON 
			t2.ldsity = t1.ldsity AND 
			t2."version" = t2."version"
		GROUP BY t2.refyearset2panel, t2."version" HAVING count(*) = (SELECT count(*) FROM w_ldsity)
	)
	SELECT array_agg(DISTINCT refyearset2panel) FROM w_refyearset2panel
	INTO _refyearset2panel4ldsity;
--raise notice 'refyearset2panel4ldsity = %', _refyearset2panel4ldsity;

	-- assigning _refyearset2panel4ldsity_spc
	WITH w_ldsity_spc_rules AS (
		SELECT DISTINCT
			 unnest(t2.sub_population_category) AS spc2classification_rule 
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN 
			target_data.c_ldsity AS t2
			ON	
			t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative AND
			t1.ldsity = t2.id
	),
	w_refyearset2panel AS (
		SELECT 
			t3.refyearset2panel
		FROM
			w_ldsity_spc_rules AS t1	
		INNER JOIN
			target_data.cm_spc2classification_rule AS t2
			ON
			t1.spc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_spc2classrule2panel_refyearset AS t3 
			ON 
			t3.spc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity_spc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_ldsity_spc_rules) FROM w_refyearset2panel 
	INTO _refyearset2panel4ldsity_spc, _rules_present; 
--raise notice 'refyearset2panel4ldsity_spc = %, rules = %', _refyearset2panel4ldsity_spc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity_spc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	-- assigning _refyearset2panel4ldsity_adc
	WITH w_ldsity_adc_rules AS (
		SELECT DISTINCT
			 unnest(t2.area_domain_category) AS adc2classification_rule 
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN 
			target_data.c_ldsity AS t2
			ON	
			t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative AND
			t1.ldsity = t2.id
	),
	w_refyearset2panel AS (
		SELECT 
			t3.refyearset2panel
		FROM
			w_ldsity_adc_rules AS t1	
		INNER JOIN
			target_data.cm_adc2classification_rule AS t2
			ON
			t1.adc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_adc2classrule2panel_refyearset AS t3 
			ON 
			t3.adc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity_adc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_ldsity_adc_rules) FROM w_refyearset2panel 
	INTO _refyearset2panel4ldsity_adc, _rules_present;  
--raise notice 'refyearset2panel4ldsity_adc = %, rules = %', _refyearset2panel4ldsity_adc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity_adc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	-- assigning _refyearset2panel4ldsity2target_variable_spc
	WITH w_ldsity2target_variable_spc_rules AS (
		SELECT DISTINCT
			unnest(t1.sub_population_category) AS spc2classification_rule 
		FROM 
		 	target_data.cm_ldsity2target_variable AS t1
		WHERE
		 	t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative
	),
	w_refyearset2panel AS (
		SELECT 
			t3.refyearset2panel
		FROM
			w_ldsity2target_variable_spc_rules AS t1	
		INNER JOIN
			target_data.cm_spc2classification_rule AS t2
			ON
			t1.spc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_spc2classrule2panel_refyearset AS t3 
			ON 
			t3.spc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity2target_variable_spc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >=1 FROM w_ldsity2target_variable_spc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4ldsity2target_variable_spc, _rules_present; 
--raise notice 'refyearset2panel4ldsity2target_variable_spc = %, rules = %', _refyearset2panel4ldsity2target_variable_spc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity2target_variable_spc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;


	-- assigning _refyearset2panel4ldsity2target_variable_adc
	WITH w_ldsity2target_variable_adc_rules AS (
		SELECT DISTINCT
			unnest(t1.area_domain_category) AS adc2classification_rule
		FROM 
		 	target_data.cm_ldsity2target_variable AS t1
		WHERE
		 	t1.target_variable = _target_variable AND
		 	t1.use_negative = _use_negative
	),
	w_refyearset2panel AS (
		SELECT
			t3.refyearset2panel
		FROM
			w_ldsity2target_variable_adc_rules AS t1	
		INNER JOIN
			target_data.cm_adc2classification_rule AS t2
			ON
			t1.adc2classification_rule = t2.id
		INNER JOIN 
			target_data.cm_adc2classrule2panel_refyearset AS t3
			ON 
			t3.adc2classification_rule = t2.id 
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_ldsity2target_variable_adc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_ldsity2target_variable_adc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4ldsity2target_variable_adc, _rules_present; 
--raise notice 'refyearset2panel4ldsity2target_variable_adc = %, rules = %', _refyearset2panel4ldsity2target_variable_adc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4ldsity2target_variable_adc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	-- assigning _refyearset2panel4spc
	WITH w_spc_rules AS (
	SELECT DISTINCT 
		 unnest(t2.spc2classification_rule) AS spc_classification_rule 
	FROM 
		target_data.cm_ldsity2target_variable AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.target_variable = _target_variable AND 
		t1.use_negative = _use_negative AND
		t1.id = t2.ldsity2target_variable AND 
		ARRAY[t2.categorization_setup] <@ _categorization_setup
	),
	w_refyearset2panel AS (
		SELECT 
			refyearset2panel
		FROM 
			w_spc_rules AS t1
		INNER JOIN 
			target_data.cm_spc2classification_rule AS t2
			ON
			t1.spc_classification_rule = t2.id
		INNER JOIN 
			target_data.cm_spc2classrule2panel_refyearset AS t3
			ON 
			t3.spc2classification_rule = t2.id
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_spc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >=1 FROM w_spc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4spc, _rules_present;
--raise notice 'refyearset2panel4spc = %, rules = %', _refyearset2panel4spc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4spc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	-- assigning _refyearset2panel4adc
	WITH w_adc_rules AS (
	SELECT DISTINCT
		 unnest(t2.adc2classification_rule) AS adc_classification_rule 
	FROM 
		target_data.cm_ldsity2target_variable AS t1
	INNER JOIN 
		target_data.cm_ldsity2target2categorization_setup AS t2
		ON  
		t1.target_variable = _target_variable AND 
		t1.use_negative = _use_negative AND
		t1.id = t2.ldsity2target_variable AND 
		ARRAY[t2.categorization_setup] <@ _categorization_setup 
	),
	w_refyearset2panel AS (
		SELECT 
			refyearset2panel
		FROM 
			w_adc_rules AS t1
		INNER JOIN 
			target_data.cm_adc2classification_rule AS t2
			ON
			t1.adc_classification_rule = t2.id
		INNER JOIN 
			target_data.cm_adc2classrule2panel_refyearset AS t3
			ON 
			t3.adc2classification_rule = t2.id
		GROUP BY t3.refyearset2panel HAVING count(*) = (SELECT count(*) FROM w_adc_rules)
	)
	SELECT array_agg(refyearset2panel), (SELECT count(*) >= 1 FROM w_adc_rules) FROM w_refyearset2panel
	INTO _refyearset2panel4adc, _rules_present;
--raise notice 'refyearset2panel4adc = %, rules = %', _refyearset2panel4adc, _rules_present;

	-- if no common combinations of panel and reference yearset are found but some rules are still present, 
	-- no checking is needed anymore and the function returns NULL (no common combinations)   
	IF _refyearset2panel4adc IS NULL AND _rules_present THEN 
	RETURN NULL;
	END IF;

	WITH w_refyearset2panel_intersection AS (
		SELECT unnest(_refyearset2panel4ldsity) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity_spc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity_adc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity2target_variable_spc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4ldsity2target_variable_adc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4spc,_refyearset2panel4ldsity)) AS refyearset2panel
		INTERSECT
		SELECT unnest(coalesce(_refyearset2panel4adc,_refyearset2panel4ldsity)) AS refyearset2panel
	)
	SELECT array_agg(refyearset2panel) FROM  w_refyearset2panel_intersection INTO _refyearset2panel_common;
	
	RETURN _refyearset2panel_common;
		
END
$$ 
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER; 

COMMENT ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations_internal(INT, INT [], BOOL) IS 
'Function returns combinations of panel and reference-yearsets (ids of cm_refyearset2panel_mapping), '
'for which positive or negative contributions to local density of the given target variable and for the given ' 
'set of its categorizations can be calculated.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations_internal(INT, INT [], BOOL) TO public;

/*
-- tests
-- inspection of the list of target variables 
SELECT * FROM target_data.c_target_variable;

 -- inspection of categorization setups for given target variable (adapt the id on line 361)
 WITH w AS ( 
	SELECT 
		t1.target_variable,
		t1.ldsity,
		t2.id,
		t2.categorization_setup, 
		t2.adc2classification_rule,
		t2.spc2classification_rule,
		unnest(coalesce(t2.adc2classification_rule, ARRAY[NULL::integer])) AS adc_rule,
		unnest(coalesce(t2.spc2classification_rule, ARRAY[NULL::integer])) AS spc_rule
	FROM 
		target_data.cm_ldsity2target_variable AS t1
	INNER JOIN
		target_data.cm_ldsity2target2categorization_setup AS t2
	ON
		t1.target_variable IN (6) AND
		t1.id = t2.ldsity2target_variable
-- ORDER BY t1.target_variable, t2.categorization_setup;
),
w_domains AS (
SELECT 
	t1.target_variable,
	t1.ldsity,
	t1.categorization_setup,
	t1.adc2classification_rule,
	t1.spc2classification_rule,
	array_agg(DISTINCT t4.area_domain ORDER BY t4.area_domain ) FILTER (WHERE t4.area_domain IS NOT NULL) AS area_domain,
	array_agg(DISTINCT t5.sub_population ORDER BY t5.sub_population) FILTER (WHERE t5.sub_population IS NOT NULL) AS subpopulation
FROM 
	w AS t1 
LEFT JOIN 
	target_data.cm_adc2classification_rule AS t2
ON
	t1.adc_rule = t2.id
LEFT JOIN 
	target_data.cm_spc2classification_rule AS t3
ON
	t1.spc_rule = t3.id
LEFT JOIN 
	target_data.c_area_domain_category AS t4
ON
	t2.area_domain_category = t4.id
LEFT JOIN 
	target_data.c_sub_population_category AS t5
ON
	t3.sub_population_category = t5.id
GROUP BY t1.target_variable, t1.ldsity, t1.categorization_setup, t1.spc2classification_rule, adc2classification_rule
)
SELECT 
	area_domain,
	subpopulation,
	array_agg(categorization_setup) AS categorization_setup
FROM 
	w_domains
GROUP BY area_domain, subpopulation;

-- checking if the function returns some combinations of panels and reference yearsets for forest area   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, ARRAY[1,2,3], FALSE); 
-- {8,9,10,13,5,2,4,6,12,14}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, ARRAY[1,2,3], TRUE); 
-- no records, state variable has no negative contributions to local density

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(4, ARRAY[109,110,111,112,113,115,116,117,118,119,120,121,122,123,128,129,130,131,132,134,135,136,137,138,139,140,141,142,147,148,149,150,151,153,154,155,156,157,158,159,160,161], FALSE);
-- {4,2,13,9}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(4, ARRAY[109,110,111,112,113,115,116,117,118,119,120,121,122,123,128,129,130,131,132,134,135,136,137,138,139,140,141,142,147,148,149,150,151,153,154,155,156,157,158,159,160,161], TRUE);
-- no records, state variable has no negative contributions to local density


SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(5, ARRAY[173,179,192,198,211,217,173,179,192,198,211,217], FALSE);
-- {9,13}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(5, ARRAY[173,179,192,198,211,217,173,179,192,198,211,217], TRUE);
-- no records, state variable has no negative contributions to local density

-- checking if the function returns some combinations of panels and reference yearsets for change of forest area 
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(6, ARRAY[221,222], FALSE);
-- {8,9,10,13,5,2,4,6,12,14}

-- the same set is retrieved for negative contributions
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(6, ARRAY[221,222], TRUE);
-- {8,9,10,13,5,2,4,6,12,14}

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(2, ARRAY[23,24,25,26,27,29,30,31,32,33,34,35,36,37,40,41,42,43,44,46,47,48,49,50,51,52,53,54], FALSE);
-- {9,13,5,2,4}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(2, ARRAY[23,24,25,26,27,29,30,31,32,33,34,35,36,37,40,41,42,43,44,46,47,48,49,50,51,52,53,54], TRUE);
-- no records, state variable has no negative contributions to local density

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood and regeneration stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(3, ARRAY[73,79,90,96,73,79,90,96], FALSE);
--{9,13,5}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(3, ARRAY[73,79,90,96,73,79,90,96], TRUE);
-- no records, state variable has no negative contributions to local density

-- checking if the function raises an exception for NULL input
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(NULL, ARRAY[1,2,3], TRUE);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, NULL, TRUE);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, ARRAY[1,2,3], NULL);
*/
-- </function>

-- <function name="fn_get_eligible_panel_refyearset_combinations" schema="target_data" src="functions/fn_get_eligible_panel_refyearset_combinations.sql">
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_eligible_panel_refyearset_combinations 
--------------------------------------------------------------------------------
-- ALTER EXTENSION nfiesta_target_data DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT);
-- DROP FUNCTION IF EXISTS target_data.fn_get_eligible_panel_refyearset_combinations(INT, INT[]);

-- API function 
CREATE OR REPLACE FUNCTION 
target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable INT, _categorization_setup INT [])
RETURNS TABLE (
	refyearset2panel_mapping INT, 
	panel_label VARCHAR(20), 
	panel_description VARCHAR(120), 
	refyearset_label VARCHAR(20), 
	refyearset_description VARCHAR(120))  AS 
$$
DECLARE 

	_state_or_change INT; -- 100 state variable, 200 change variable, 300 dynamic variable
	_refyearset2panel_eligible_sum INT[]; -- eligible combinations of panel and reference yearset for positive terms of the local density     
	_refyearset2panel_eligible_subtract INT[]; -- eligible combinations of panel and reference yearset for negative terms of the local density

BEGIN
	
	-- raising an exception on NULL input for _target_variable
	IF _target_variable IS NULL then 
		RAISE EXCEPTION 'Input parameter _target_variable INT not given, but it is required!';
	END IF; 

	-- raising an exception on NULL input for _categorization_setup
	IF _categorization_setup IS NULL then 
		RAISE EXCEPTION 'Input parameter _categorization_setup INT[] not given, but it is required!';
	END IF;

	-- set the state or change variable
	SELECT state_or_change FROM target_data.c_target_variable WHERE id = _target_variable INTO _state_or_change; 	

	SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(_target_variable, _categorization_setup, FALSE) 
	INTO _refyearset2panel_eligible_sum;
	
	IF _refyearset2panel_eligible_sum IS NULL THEN 
		RETURN;
	END IF;


	IF _state_or_change = 200 THEN 
	
		SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(_target_variable, _categorization_setup, TRUE) 
		INTO _refyearset2panel_eligible_subtract;
		
		IF _refyearset2panel_eligible_subtract IS NULL THEN 
			RETURN;
		END IF;
	
		RETURN QUERY
		SELECT 
			t5.id AS refyearset2panel_mapping,
			t6.panel AS panel_label,
			t6."label" AS panel_description,
			t2.reference_year_set AS refyearset_label,
			t2."label" AS refyearset_description
		FROM 
			sdesign.cm_refyearset2panel_mapping AS t1
		INNER JOIN 
			sdesign.t_reference_year_set AS t2
			ON
			ARRAY[t1.id] <@ _refyearset2panel_eligible_sum AND
			t1.reference_year_set = t2.reference_year_set_end
		INNER JOIN
			sdesign.cm_refyearset2panel_mapping AS t3
			ON
			ARRAY[t3.id] <@ _refyearset2panel_eligible_subtract AND
			t1.panel = t3.panel
		INNER JOIN 
			sdesign.t_reference_year_set AS t4
			ON
			t3.reference_year_set = t4.reference_year_set_begin AND 
			t2.id = t4.id
		INNER JOIN
			sdesign.cm_refyearset2panel_mapping AS t5
			ON 
			t5.panel = t1.panel AND 
			t5.reference_year_set = t2.id
		INNER JOIN 
		 	sdesign.t_panel AS t6
		 	ON
		 	t1.panel = t6.id;
	
	ELSE -- state or dynamic variables with no subtraction of local density contributions
	
		RETURN QUERY
		WITH w_refyearset2panel_mapping AS (
			SELECT unnest(_refyearset2panel_eligible_sum) AS refyearset2panel_mapping
		)
		SELECT 
			t1.refyearset2panel_mapping,
			t4.panel AS panel_label,
			t4."label" AS panel_description,
			t3.reference_year_set AS refyerset_label,
			t3."label" AS refyearset_description
		FROM
			w_refyearset2panel_mapping AS t1 
		INNER JOIN	
			sdesign.cm_refyearset2panel_mapping AS t2
			ON t1.refyearset2panel_mapping = t2.id
		INNER JOIN
			sdesign.t_reference_year_set AS t3
			ON t2.reference_year_set = t3.id
		INNER JOIN
			sdesign.t_panel AS t4
			ON t2.panel = t4.id;
	END IF;
	
 RETURN;

END
$$ 
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER; 

COMMENT ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT, INT []) IS 
'Function returns combinations of panel and reference-yearsets (ids of cm_refyearset2panel_mapping), '
'for which local density of the given target variable and for the given set of its categorizations '
'can be calculated.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT, INT []) TO public;

/*
-- tests
-- inspection of the list of target variables 
SELECT * FROM target_data.c_target_variable;

-- inspection of categorization setups for given target variable (adapt the id on line 159)
 WITH w AS ( 
	SELECT 
		t1.target_variable,
		t1.ldsity,
		t2.id,
		t2.categorization_setup, 
		t2.adc2classification_rule,
		t2.spc2classification_rule,
		unnest(coalesce(t2.adc2classification_rule, ARRAY[NULL::integer])) AS adc_rule,
		unnest(coalesce(t2.spc2classification_rule, ARRAY[NULL::integer])) AS spc_rule
	FROM 
		target_data.cm_ldsity2target_variable AS t1
	INNER JOIN
		target_data.cm_ldsity2target2categorization_setup AS t2
	ON
		t1.target_variable IN (6) AND
		t1.id = t2.ldsity2target_variable
-- ORDER BY t1.target_variable, t2.categorization_setup;
),
w_domains AS (
SELECT 
	t1.target_variable,
	t1.ldsity,
	t1.categorization_setup,
	t1.adc2classification_rule,
	t1.spc2classification_rule,
	array_agg(DISTINCT t4.area_domain ORDER BY t4.area_domain ) FILTER (WHERE t4.area_domain IS NOT NULL) AS area_domain,
	array_agg(DISTINCT t5.sub_population ORDER BY t5.sub_population) FILTER (WHERE t5.sub_population IS NOT NULL) AS subpopulation
FROM 
	w AS t1 
LEFT JOIN 
	target_data.cm_adc2classification_rule AS t2
ON
	t1.adc_rule = t2.id
LEFT JOIN 
	target_data.cm_spc2classification_rule AS t3
ON
	t1.spc_rule = t3.id
LEFT JOIN 
	target_data.c_area_domain_category AS t4
ON
	t2.area_domain_category = t4.id
LEFT JOIN 
	target_data.c_sub_population_category AS t5
ON
	t3.sub_population_category = t5.id
GROUP BY t1.target_variable, t1.ldsity, t1.categorization_setup, t1.spc2classification_rule, adc2classification_rule
)
SELECT 
	area_domain,
	subpopulation,
	array_agg(categorization_setup) AS categorization_setup
FROM 
	w_domains
GROUP BY area_domain, subpopulation; 
   

-- checking if the function returns some combinations of panels and reference yearsets for forest area   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(1, ARRAY[2,3]);
-- ten records

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(1, ARRAY[1]);
-- ten records

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(4, ARRAY[109,110,111,112,113,115,116,117,118,119,120,121,122,123,128,129,130,131,132,134,135,136,137,138,139,140,141,142,147,148,149,150,151,153,154,155,156,157,158,159,160,161]);
-- four records

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(5, ARRAY[163,180,182,199,201,218,163,180,182,199,201,218]);
-- two records

-- checking if the function returns some combinations of panels and reference yearsets for change of forest area 
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(6, ARRAY[220,221,222]);
-- three records, all coresponding to change referenece yearsets

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(2, ARRAY[23,24,25,26,27,29,30,31,32,33,34,35,36,37,40,41,42,43,44,46,47,48,49,50,51,52,53,54]);
-- five records

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood and regeneration stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(3, ARRAY[73,79,90,96,73,79,90,96]);
-- three records

 -- checking if the function raises an exception for NULL input
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(NULL, ARRAY[73,79,90,96,73,79,90,96]);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(3, NULL);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(NULL, NULL);

*/

-- ALTER EXTENSION nfiesta_target_data DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT);
-- DROP FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT);

-- overloaded function which is not part of API and should be used from within API function fn_save_ldsity_values 
CREATE OR REPLACE FUNCTION 
target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable INT) -- id of target_data.c_target_variable 
RETURNS TABLE (
	categorization_setup INT, -- id of target_data.t_categorization_setup
	refyearset2panel_mapping INT [])  AS -- maximum set of records from sdesign.cm_refyearset2panel_mapping for which ldsity can be calculated using given categorization setup   
$$
BEGIN
	
	-- raising an exception on NULL input for _target_variable
	IF _target_variable IS NULL THEN 
		RAISE EXCEPTION 'Input parameter _target_variable INT not given, but it is required!';
	END IF; 

	RETURN QUERY
	WITH w_categorization_list AS (
		SELECT 
			DISTINCT t2.categorization_setup AS catsetup
		FROM 
			target_data.cm_ldsity2target_variable AS t1
		INNER JOIN
			target_data.cm_ldsity2target2categorization_setup AS t2
		ON
			t1.target_variable = _target_variable AND
			t1.id = t2.ldsity2target_variable 
	),
	w_refyearset2panel_mapping AS ( 
	SELECT 
		t1.catsetup,
		(target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable, ARRAY[t1.catsetup])).refyearset2panel_mapping AS refmap
	FROM
		w_categorization_list AS t1
	)
	SELECT 
		catsetup,
		array_agg(refmap)
	FROM 
		w_refyearset2panel_mapping
	GROUP BY catsetup;	
	
RETURN;

END
$$ 
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER; 

COMMENT ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT) IS 
'Function returns a table with all categorization setups under given target variable.'
'To each categorization setup an array of all panel and reference yearset combination '
' is attached, for which local densities can be calculated ';

GRANT EXECUTE ON FUNCTION target_data.fn_get_eligible_panel_refyearset_combinations(INT) TO public;

/* tests
  
  SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(1);
  -- 3 records
  
  SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(2);
  -- 51 records
  
    SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(4);
  -- 57 records
  
  -- test of null input
  SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(NULL);
  -- exception raised
  
 */ 

-- </function>






DROP FUNCTION IF EXISTS target_data.fn_get_categorization_setup(integer, integer[], integer[], integer[][], integer[], integer[][]);
DROP FUNCTION IF EXISTS target_data.fn_save_categorization_setup(integer, integer[], integer[], integer[][], integer[], integer[][]);



-- <function name="fn_get_categorization_setup" schema="target_data" src="functions/fn_get_categorization_setup.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_categorization_setup
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_categorization_setup(integer, integer[], integer[], integer[][], integer[], integer[][]) CASCADE;

create or replace function target_data.fn_get_categorization_setup
(
	_target_variable		integer,
	_target_variable_cm		integer[],
	_area_domain			integer[],
	_area_domain_object		integer[][],
	_sub_population			integer[],
	_sub_population_object	integer[][]
)
returns integer[]
as
$$
declare
		_array_id							integer[];
		_array_id4check						integer[];
		_lot_100							integer;
		_lot_200							integer;
		_variant							integer;	
		_area_domain4input					integer[];
		_area_domain_object4input			integer[];
		_sub_population4input				integer[];
		_sub_population_object4input		integer[];	
		_area_domain4input_text				text;
		_area_domain_object4input_text		text;
		_sub_population4input_text			text;
		_sub_population_object4input_text	text;
		_string4attr						text;
		_query_res							text;
		_s4ij_p1							text;
		_s4ij_p2							text;
		_s4ij_p3							text;
		_s4ij_p4							text;
		_s4ij_p5							text;
		_string4incomming					text;
		_string4inner_join					text;
		_array_id_200						integer[];
		_string_inner_join					text;
		_string_incomming_0					text;
		_string_incomming					text;
		_string_check_1						text;
		_string_check_2						text;
		_string_check_3						text;
		_string_check_4						text;
		_s4da_p1							text;
		_s4da_p2							text;
		_s4da_p3							text;
		_s4da_p4							text;
		_s4ia_p1							text;
		_s4ia_p2							text;
		_s4ia_p3							text;
		_s4ia_p4							text;
		_s4result_p1						text;
		_s4result_p2						text;
		_string4database_agg				text;
		_string4incomming_agg				text;	
		_string4result						text;
		_string_result						text;
		_res								text;
		_result								integer[];
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_get_categorization_setup: The input argument _target_variable must not be NULL !';
		end if;

		if _target_variable_cm is null
		then
			raise exception 'Error 02: fn_get_categorization_setup: The input argument _target_variable_cm must not be NULL !';
		end if;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where target_variable = _target_variable
		into
				_array_id;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where id in (select unnest(_target_variable_cm))
		into
				_array_id4check;			

		if _array_id != _array_id4check
			then raise exception 'Error 03: fn_get_categorization_setup: The internal argument _array_id = % and internal argument _array_id4check = % are not the same!',_array_id, _array_id4check;
		end if;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 04: fn_get_categorization_setup: This variant is not implemented yet!';
		end if;

		for bc in 1..array_length(_array_id,1)
		loop	
			-----------------------------------------
			-----------------------------------------
			if _area_domain is null and _sub_population is null
			then
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is null
			then
				if _variant in (3,4)
				then
					with
					w1 as (select unnest(_area_domain_object[1:1]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;				
				else		
					with
					w1 as (select unnest(_area_domain_object[bc:bc]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				end if;			
			
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;
			
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			------
			if _area_domain is null and _sub_population is not null
			then		
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
			
				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is not null
			then
				if _variant in (3,4)
				then
					with
					w1 as (select unnest(_area_domain_object[1:1]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				else
					with
					w1 as (select unnest(_area_domain_object[bc:bc]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				end if;				
			
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
	
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;			

				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			end if;
			-----------------------------------------
			-----------------------------------------
			
			if _area_domain4input is null
			then
				_area_domain4input_text := 'null::integer[]';
			else
				_area_domain4input_text := replace(replace(concat('array[',_area_domain4input,']'),'{',''),'}','');
			end if;
		
			if _area_domain_object4input is null
			then
				_area_domain_object4input_text := 'null::integer[]';
			else
				_area_domain_object4input_text := replace(replace(concat('array[',_area_domain_object4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population4input is null
			then
				_sub_population4input_text := 'null::integer[]';
			else
				_sub_population4input_text := replace(replace(concat('array[',_sub_population4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population_object4input is null
			then
				_sub_population_object4input_text := 'null::integer[]';
			else
				_sub_population_object4input_text := replace(replace(concat('array[',_sub_population_object4input,']'),'{',''),'}','');
			end if;
			
			_string4attr := concat('
			select
					area_domain_category,
					sub_population_category,
					adc2classification_rule,
					spc2classification_rule
			from
					target_data.fn_get_classification_rules_id4categories(',_area_domain4input_text,',',_area_domain_object4input_text,',',_sub_population4input_text,',',_sub_population_object4input_text,')
			');
		
			if bc = 1
			then
				_query_res := concat('with w_',bc,' as (',_string4attr,')');
			else
				_query_res := concat(_query_res,',w_',bc,' as (',_string4attr,')');
			end if;
		end loop;
		
		-------------------------------------------------------------
		-------------------------------------------------------------
		if _variant in (1,2)
		then
		
			_s4ij_p1 := 'select row_number() over() as group_id_poradi';
			_s4ij_p3 := ' from ';
		
			for bc in 1..array_length(_array_id,1)
			loop
				if bc = 1
				then
			
					_s4ij_p2 := concat('
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.area_domain_category
										,t',bc,'.sub_population_category
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'');
				
					_string4incomming := concat(
						'
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'
										on
											t1.area_domain_category = t',bc,'.area_domain_category and
											t1.sub_population_category = t',bc,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				end if;
			end loop;
			
				_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4;
		end if;		
		-------------------------------------------------------------
		if _variant in (3,4)
		then	
			_s4ij_p1 := concat(
				'
				select
					row_number() over() as group_id_poradi,
					a.*,
					b.*
				from
					(
					select
							',_array_id[1],' as ldsity2target_variable_',_array_id[1],',
							area_domain_category as area_domain_category_',_array_id[1],',
							adc2classification_rule as adc2classification_rule_',_array_id[1],',
							spc2classification_rule as spc2classification_rule_',_array_id[1],'
					from w_1
					) as a
				inner
				join
					(select ');
				
			_s4ij_p3 := ' from ';
		
			_s4ij_p5 := concat('
						) as b
					on
						a.area_domain_category_',_array_id[1],' = b.area_domain_category');
					
			_string4incomming := concat(
				'
				select
						1 as group_id,
						group_id_poradi,
						area_domain_category,
						sub_population_category,
						ldsity2target_variable_',_array_id[1],' as ldsity2target_variable,
						adc2classification_rule_',_array_id[1],' as adc2classification_rule,
						spc2classification_rule_',_array_id[1],' as spc2classification_rule
				from
						w_inner_join
				');					
				
			_array_id_200 := array_remove(_array_id, _array_id[1]);
			
			for bc_200 in 1..array_length(_array_id_200,1)
			loop
				if bc_200 = 1
				then				
					_s4ij_p2 := concat('
										t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.area_domain_category
										,t',bc_200,'.sub_population_category
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'');
				
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');				
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'
										on
											t1.area_domain_category = t',bc_200,'.area_domain_category and
											t1.sub_population_category = t',bc_200,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');
					
				end if;
			end loop;
			
			_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4 || _s4ij_p5;
		
		end if;
		-------------------------------------------------------------
		-------------------------------------------------------------
		_string_inner_join := concat(',w_inner_join as (',_string4inner_join,')');
		_string_incomming_0 := concat(',w_incomming_0 as (',_string4incomming,')');
		-------------------------------------------------------------
		if _variant in (3,4)
		then
			_string_incomming := concat(
				'
				,w_incomming as	(
								select
										group_id,
										group_id_poradi,
										area_domain_category,
										sub_population_category,
										ldsity2target_variable,
										adc2classification_rule,
										case when ldsity2target_variable in (',array_to_string(_array_id_200,','),') then null::integer[] else adc2classification_rule end as adc2classification_rule4insert,
										spc2classification_rule
								from
									w_incomming_0
								)
				');


			_string_check_1 := concat(
				'
				,w_check_1 as		(
									select
											a.*,
											b.id_cm_setup,
											b.categorization_setup		
									from
											w_incomming as a
									left
									join	(
											select
													t1.*,
													t2.area_domain_category
											from
												(
												select
														id as id_cm_setup,
														ldsity2target_variable,
														adc2classification_rule,
														spc2classification_rule,
														categorization_setup
												from
														target_data.cm_ldsity2target2categorization_setup
												where
														ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
												) as t1
												inner join
												(
												select
														categorization_setup,
														case when adc2classification_rule is null then array[0] else (select id_category from target_data.fn_get_category4classification_rule_id(''adc''::varchar,adc2classification_rule)) end as area_domain_category
												from
														target_data.cm_ldsity2target2categorization_setup
												where
														ldsity2target_variable = ',_array_id[1],'
												) as t2
											on t1.categorization_setup = t2.categorization_setup
											) as b
									on
											a.ldsity2target_variable = b.ldsity2target_variable and			
											((case when a.adc2classification_rule4insert is null then array[0] else a.adc2classification_rule4insert end)
											= (case when b.adc2classification_rule is null then array[0] else b.adc2classification_rule end)) and
											((case when a.spc2classification_rule is null then array[0] else a.spc2classification_rule end)
											= (case when b.spc2classification_rule is null then array[0] else b.spc2classification_rule end))

											and a.area_domain_category = b.area_domain_category				
									)
			');

			_string_check_3 := concat(
				'
				,w_check_3 as		(
									select * from w_check_2 union all
									select distinct c.group_id, c.group_id_poradi, c.area_domain_category,
									c.sub_population_category, c.ldsity2target_variable,
									c.adc2classification_rule, c.spc2classification_rule,
									c.adc2classification_rule4insert,
									c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
									from
											(
											select
													1 as group_id,
													group_id_poradi,
													area_domain_category,
													sub_population_category,
													',_array_id[1],' as ldsity2target_variable,
													adc2classification_rule,
													null::integer[] as spc2classification_rule,
													adc2classification_rule4insert,
													null::integer as id_cm_setup,
													null::integer as categorization_setup,
													check_exists_group_id_1
											from
													w_check_2 where check_exists_group_id_1 = 1
											) as c
									)				
			');
		else
			_string_incomming := concat(
				'
				,w_incomming as	(
								select
										group_id,
										group_id_poradi,
										area_domain_category,
										sub_population_category,
										ldsity2target_variable,
										adc2classification_rule,
										spc2classification_rule
								from
									w_incomming_0
								)
				');

			_string_check_1 := concat(
				'
				,w_check_1 as		(
									select
											a.*,
											b.id_cm_setup,
											b.categorization_setup		
									from
											w_incomming as a
									left
									join	(
											select
													id as id_cm_setup,
													ldsity2target_variable,
													adc2classification_rule,
													spc2classification_rule,
													categorization_setup
											from
													target_data.cm_ldsity2target2categorization_setup
											where
												ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
											) as b
									on
											a.ldsity2target_variable = b.ldsity2target_variable and			
											((case when a.adc2classification_rule is null then array[0] else a.adc2classification_rule end)
											= (case when b.adc2classification_rule is null then array[0] else b.adc2classification_rule end)) and
											((case when a.spc2classification_rule is null then array[0] else a.spc2classification_rule end)
											= (case when b.spc2classification_rule is null then array[0] else b.spc2classification_rule end))				
									)
				'
			);

			_string_check_3 := concat(
				'
				,w_check_3 as		(
									select * from w_check_2 union all
									select distinct c.group_id, c.group_id_poradi, c.area_domain_category,
									c.sub_population_category, c.ldsity2target_variable,
									c.adc2classification_rule, c.spc2classification_rule,
									c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
									from
											(
											select
													1 as group_id,
													group_id_poradi,
													area_domain_category,
													sub_population_category,
													',_array_id[1],' as ldsity2target_variable,
													adc2classification_rule,
													null::integer[] as spc2classification_rule,
													null::integer as id_cm_setup,
													null::integer as categorization_setup,
													check_exists_group_id_1
											from
													w_check_2 where check_exists_group_id_1 = 1
											) as c
									)				
			');			
		end if;
		-------------------------------------------------------------
		
		-------------------------------------------------------------
		-------------------------------------------------------------
		_s4da_p1 := 'select t0.categorization_setup';
		_s4da_p3 := ' from (select distinct categorization_setup from w_database) as t0 ';

		_s4ia_p1 := 'select t0.group_id_poradi';
		_s4ia_p3 := ' from (select distinct group_id_poradi from w_incomming) as t0 ';
	
		_s4result_p1 :=	'
						select
								a.*,
								b.categorization_setup
						from
								w_incomming_agg as a
						inner
						join	w_database_agg as b
						
						on ';	
	

		for i in 1..array_length(_array_id,1)
		loop
			if i = 1
			then
				_s4da_p2 := concat('
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');

				if _variant in (3,4)
				then				
					_s4ia_p2 := concat('
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule4insert as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');
				else
					_s4ia_p2 := concat('
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');				
				end if;								
								
				_s4da_p4 := concat(' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat('
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],' and
									((case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end)
									= (case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)) and
									((case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end)
									= (case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end))
								');
			else
				_s4da_p2 := concat(_s4da_p2,
									'
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');
								
				if _variant in (3,4)
				then				
					_s4ia_p2 := concat(_s4ia_p2,
										'
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule4insert as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');
				else
					_s4ia_p2 := concat(_s4ia_p2,
										'
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');				
				end if;								
								
				_s4da_p4 := concat(_s4da_p4,
									' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(_s4ia_p4,
									' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat(_s4result_p2,' and 
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],' and
									((case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end)
									= (case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)) and
									((case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end)
									= (case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end))
								');
			end if;
		end loop;
	
		_string4database_agg := _s4da_p1 || _s4da_p2 || _s4da_p3 || _s4da_p4;
		_string4incomming_agg := _s4ia_p1 || _s4ia_p2 || _s4ia_p3 || _s4ia_p4;
		_string4result := _s4result_p1 || _s4result_p2;
		-------------------------------------------------------------
		-------------------------------------------------------------

		_string_check_2 := 
		'
		,w_check_2 as		(
							select
									w_check_1.*,
									case
										when categorization_setup is null
											then
												case
													when	(
															select min(t.group_id) from w_check_1 as t
															where t.categorization_setup is null
															and t.group_id_poradi = w_check_1.group_id_poradi
															)
															= 1
													then 0
													else 1
												end 
											else 0
									end as check_exists_group_id_1
							from
									w_check_1
							)		
		';


		_string_check_4 :=
		'
		,w_check_4 as		(					
							select
									(select coalesce(max(id),0) from target_data.t_categorization_setup) + 
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as new_categorization_setup,
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as pozice,
									w_check_3.*
							from
									w_check_3
							where
									categorization_setup is null
							order
									by new_categorization_setup, pozice, group_id
							)		
		';

		/*
		_string_result :=
		'
		select
				t.categorization_setup,
				array_agg(t.id_cm_setup order by t.id_cm_setup) as id_cm_setup
		from	(
				select distinct w_check_3.id_cm_setup, w_check_3.categorization_setup from w_check_3
				where w_check_3.categorization_setup is not null
				) as t
		group by t.categorization_setup						
		union
		all
		select distinct w_check_4.new_categorization_setup as categorization_setup, null::integer[] as id_cm_setup
		from w_check_4
		order by categorization_setup;
		';
		*/

		_string_result :=
		'
		select array_agg(tt.categorization_setup order by tt.categorization_setup)
		from
			(					
			select distinct w_check_3.categorization_setup from w_check_3 where w_check_3.categorization_setup is not null
			union
			all
			select t.categorization_setup from (select distinct w_check_4.new_categorization_setup, null::integer as categorization_setup from w_check_4) as t
			) as tt;		
		';	


		_res := _query_res || _string_inner_join || _string_incomming_0 || _string_incomming || _string_check_1 || _string_check_2 || _string_check_3 || _string_check_4 || _string_result;
		
		execute ''||_res||'' into _result;

		return _result;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_get_categorization_setup(integer,integer[],integer[],integer[][],integer[],integer[][]) IS
'The function gets attribute configurations.';

grant execute on function target_data.fn_get_categorization_setup(integer,integer[],integer[],integer[][],integer[],integer[][]) to public;
-- </function>



-- <function name="fn_save_categorization_setup" schema="target_data" src="functions/fn_save_categorization_setup.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categorization_setup
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_categorization_setup(integer, integer[], integer[], integer[][], integer[], integer[][]) CASCADE;

create or replace function target_data.fn_save_categorization_setup
(
	_target_variable		integer,
	_target_variable_cm		integer[],
	_area_domain			integer[],
	_area_domain_object		integer[][],
	_sub_population			integer[],
	_sub_population_object	integer[][]
)
returns void
as
$$
declare
		_array_id							integer[];
		_array_id4check						integer[];
		_lot_100							integer;
		_lot_200							integer;
		_variant							integer;	
		_area_domain4input					integer[];
		_area_domain_object4input			integer[];
		_sub_population4input				integer[];
		_sub_population_object4input		integer[];	
		_area_domain4input_text				text;
		_area_domain_object4input_text		text;
		_sub_population4input_text			text;
		_sub_population_object4input_text	text;
		_string4attr						text;
		_query_res							text;
		_s4ij_p1							text;
		_s4ij_p2							text;
		_s4ij_p3							text;
		_s4ij_p4							text;
		_s4ij_p5							text;
		_string4incomming					text;
		_string4inner_join					text;
		_array_id_200						integer[];
		_string_inner_join					text;
		_string_incomming_0					text;
		_string_incomming					text;
		_string_check_1						text;
		_string_check_2						text;
		_string_check_3						text;
		_string_check_4						text;
		_string_insert_1					text;
		_string_insert_2					text;
		_s4da_p1							text;
		_s4da_p2							text;
		_s4da_p3							text;
		_s4da_p4							text;
		_s4ia_p1							text;
		_s4ia_p2							text;
		_s4ia_p3							text;
		_s4ia_p4							text;
		_s4result_p1						text;
		_s4result_p2						text;
		_string4database_agg				text;
		_string4incomming_agg				text;	
		_string4result						text;
		_string_agg_and_result				text;
		_res								text;
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_save_categorization_setup: The input argument _target_variable must not be NULL !';
		end if;

		if _target_variable_cm is null
		then
			raise exception 'Error 02: fn_save_categorization_setup: The input argument _target_variable_cm must not be NULL !';
		end if;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where target_variable = _target_variable
		into
				_array_id;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where id in (select unnest(_target_variable_cm))
		into
				_array_id4check;			

		if _array_id != _array_id4check
			then raise exception 'Error 03: fn_save_categorization_setup: The internal argument _array_id = % and internal argument _array_id4check = % are not the same!',_array_id, _array_id4check;
		end if;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 04: fn_save_categorization_setup: This variant is not implemented yet!';
		end if;

		for bc in 1..array_length(_array_id,1)
		loop	
			-----------------------------------------
			-----------------------------------------
			if _area_domain is null and _sub_population is null
			then
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is null
			then
				if _variant in (3,4)
				then
					with
					w1 as (select unnest(_area_domain_object[1:1]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;				
				else		
					with
					w1 as (select unnest(_area_domain_object[bc:bc]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				end if;			
			
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;
			
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			------
			if _area_domain is null and _sub_population is not null
			then		
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
			
				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is not null
			then
				if _variant in (3,4)
				then
					with
					w1 as (select unnest(_area_domain_object[1:1]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				else
					with
					w1 as (select unnest(_area_domain_object[bc:bc]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				end if;				
			
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
	
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;			

				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			end if;
			-----------------------------------------
			-----------------------------------------
			
			if _area_domain4input is null
			then
				_area_domain4input_text := 'null::integer[]';
			else
				_area_domain4input_text := replace(replace(concat('array[',_area_domain4input,']'),'{',''),'}','');
			end if;
		
			if _area_domain_object4input is null
			then
				_area_domain_object4input_text := 'null::integer[]';
			else
				_area_domain_object4input_text := replace(replace(concat('array[',_area_domain_object4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population4input is null
			then
				_sub_population4input_text := 'null::integer[]';
			else
				_sub_population4input_text := replace(replace(concat('array[',_sub_population4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population_object4input is null
			then
				_sub_population_object4input_text := 'null::integer[]';
			else
				_sub_population_object4input_text := replace(replace(concat('array[',_sub_population_object4input,']'),'{',''),'}','');
			end if;
			
			_string4attr := concat('
			select
					area_domain_category,
					sub_population_category,
					adc2classification_rule,
					spc2classification_rule
			from
					target_data.fn_get_classification_rules_id4categories(',_area_domain4input_text,',',_area_domain_object4input_text,',',_sub_population4input_text,',',_sub_population_object4input_text,')
			');
		
			if bc = 1
			then
				_query_res := concat('with w_',bc,' as (',_string4attr,')');
			else
				_query_res := concat(_query_res,',w_',bc,' as (',_string4attr,')');
			end if;
		end loop;
		
		-------------------------------------------------------------
		-------------------------------------------------------------
		if _variant in (1,2)
		then
		
			_s4ij_p1 := 'select row_number() over() as group_id_poradi';
			_s4ij_p3 := ' from ';
		
			for bc in 1..array_length(_array_id,1)
			loop
				if bc = 1
				then
			
					_s4ij_p2 := concat('
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.area_domain_category
										,t',bc,'.sub_population_category
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'');
				
					_string4incomming := concat(
						'
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'
										on
											t1.area_domain_category = t',bc,'.area_domain_category and
											t1.sub_population_category = t',bc,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				end if;
			end loop;
			
				_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4;
		end if;		
		-------------------------------------------------------------
		if _variant in (3,4)
		then	
			_s4ij_p1 := concat(
				'
				select
					row_number() over() as group_id_poradi,
					a.*,
					b.*
				from
					(
					select
							',_array_id[1],' as ldsity2target_variable_',_array_id[1],',
							area_domain_category as area_domain_category_',_array_id[1],',
							adc2classification_rule as adc2classification_rule_',_array_id[1],',
							spc2classification_rule as spc2classification_rule_',_array_id[1],'
					from w_1
					) as a
				inner
				join
					(select ');
				
			_s4ij_p3 := ' from ';
		
			_s4ij_p5 := concat('
						) as b
					on
						a.area_domain_category_',_array_id[1],' = b.area_domain_category');
					
			_string4incomming := concat(
				'
				select
						1 as group_id,
						group_id_poradi,
						area_domain_category,
						sub_population_category,
						ldsity2target_variable_',_array_id[1],' as ldsity2target_variable,
						adc2classification_rule_',_array_id[1],' as adc2classification_rule,
						spc2classification_rule_',_array_id[1],' as spc2classification_rule
				from
						w_inner_join
				');					
				
			_array_id_200 := array_remove(_array_id, _array_id[1]);
			
			for bc_200 in 1..array_length(_array_id_200,1)
			loop
				if bc_200 = 1
				then				
					_s4ij_p2 := concat('
										t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.area_domain_category
										,t',bc_200,'.sub_population_category
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'');
				
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');				
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'
										on
											t1.area_domain_category = t',bc_200,'.area_domain_category and
											t1.sub_population_category = t',bc_200,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');
					
				end if;
			end loop;
			
			_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4 || _s4ij_p5;
		
		end if;
		-------------------------------------------------------------
		-------------------------------------------------------------
		_string_inner_join := concat(',w_inner_join as (',_string4inner_join,')');
		_string_incomming_0 := concat(',w_incomming_0 as (',_string4incomming,')');
		-------------------------------------------------------------
		if _variant in (3,4)
		then
			_string_incomming := concat(
				'
				,w_incomming as	(
								select
										group_id,
										group_id_poradi,
										area_domain_category,
										sub_population_category,
										ldsity2target_variable,
										adc2classification_rule,
										case when ldsity2target_variable in (',array_to_string(_array_id_200,','),') then null::integer[] else adc2classification_rule end as adc2classification_rule4insert,
										spc2classification_rule
								from
									w_incomming_0
								)
				');


			_string_check_1 := concat(
				'
				,w_check_1 as		(
									select
											a.*,
											b.id_cm_setup,
											b.categorization_setup		
									from
											w_incomming as a
									left
									join	(
											select
													t1.*,
													t2.area_domain_category
											from
												(
												select
														id as id_cm_setup,
														ldsity2target_variable,
														adc2classification_rule,
														spc2classification_rule,
														categorization_setup
												from
														target_data.cm_ldsity2target2categorization_setup
												where
														ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
												) as t1
												inner join
												(
												select
														categorization_setup,
														case when adc2classification_rule is null then array[0] else (select id_category from target_data.fn_get_category4classification_rule_id(''adc''::varchar,adc2classification_rule)) end as area_domain_category
												from
														target_data.cm_ldsity2target2categorization_setup
												where
														ldsity2target_variable = ',_array_id[1],'
												) as t2
											on t1.categorization_setup = t2.categorization_setup
											) as b
									on
											a.ldsity2target_variable = b.ldsity2target_variable and			
											((case when a.adc2classification_rule4insert is null then array[0] else a.adc2classification_rule4insert end)
											= (case when b.adc2classification_rule is null then array[0] else b.adc2classification_rule end)) and
											((case when a.spc2classification_rule is null then array[0] else a.spc2classification_rule end)
											= (case when b.spc2classification_rule is null then array[0] else b.spc2classification_rule end))

											and a.area_domain_category = b.area_domain_category				
									)
			');

			_string_check_3 := concat(
				'
				,w_check_3 as		(
									select * from w_check_2 union all
									select distinct c.group_id, c.group_id_poradi, c.area_domain_category,
									c.sub_population_category, c.ldsity2target_variable,
									c.adc2classification_rule, c.spc2classification_rule,
									c.adc2classification_rule4insert,
									c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
									from
											(
											select
													1 as group_id,
													group_id_poradi,
													area_domain_category,
													sub_population_category,
													',_array_id[1],' as ldsity2target_variable,
													adc2classification_rule,
													null::integer[] as spc2classification_rule,
													adc2classification_rule4insert,
													null::integer as id_cm_setup,
													null::integer as categorization_setup,
													check_exists_group_id_1
											from
													w_check_2 where check_exists_group_id_1 = 1
											) as c
									)				
			');

			_string_insert_2 :=
			'
			,w_insert_2 as		(
								insert into target_data.cm_ldsity2target2categorization_setup
								(ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup)
								select ldsity2target_variable,adc2classification_rule4insert,spc2classification_rule,new_categorization_setup
								from w_check_4
								returning id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
								)			
			';
		else
			_string_incomming := concat(
				'
				,w_incomming as	(
								select
										group_id,
										group_id_poradi,
										area_domain_category,
										sub_population_category,
										ldsity2target_variable,
										adc2classification_rule,
										spc2classification_rule
								from
									w_incomming_0
								)
				');

			_string_check_1 := concat(
				'
				,w_check_1 as		(
									select
											a.*,
											b.id_cm_setup,
											b.categorization_setup		
									from
											w_incomming as a
									left
									join	(
											select
													id as id_cm_setup,
													ldsity2target_variable,
													adc2classification_rule,
													spc2classification_rule,
													categorization_setup
											from
													target_data.cm_ldsity2target2categorization_setup
											where
												ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
											) as b
									on
											a.ldsity2target_variable = b.ldsity2target_variable and			
											((case when a.adc2classification_rule is null then array[0] else a.adc2classification_rule end)
											= (case when b.adc2classification_rule is null then array[0] else b.adc2classification_rule end)) and
											((case when a.spc2classification_rule is null then array[0] else a.spc2classification_rule end)
											= (case when b.spc2classification_rule is null then array[0] else b.spc2classification_rule end))				
									)
				'
			);

			_string_check_3 := concat(
				'
				,w_check_3 as		(
									select * from w_check_2 union all
									select distinct c.group_id, c.group_id_poradi, c.area_domain_category,
									c.sub_population_category, c.ldsity2target_variable,
									c.adc2classification_rule, c.spc2classification_rule,
									c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
									from
											(
											select
													1 as group_id,
													group_id_poradi,
													area_domain_category,
													sub_population_category,
													',_array_id[1],' as ldsity2target_variable,
													adc2classification_rule,
													null::integer[] as spc2classification_rule,
													null::integer as id_cm_setup,
													null::integer as categorization_setup,
													check_exists_group_id_1
											from
													w_check_2 where check_exists_group_id_1 = 1
											) as c
									)				
			');

			_string_insert_2 :=
			'
			,w_insert_2 as		(
								insert into target_data.cm_ldsity2target2categorization_setup
								(ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup)
								select ldsity2target_variable,adc2classification_rule,spc2classification_rule,new_categorization_setup
								from w_check_4
								returning id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
								)			
			';			
		end if;
		-------------------------------------------------------------
		
		-------------------------------------------------------------
		-------------------------------------------------------------
		_s4da_p1 := 'select t0.categorization_setup';
		_s4da_p3 := ' from (select distinct categorization_setup from w_database) as t0 ';

		_s4ia_p1 := 'select t0.group_id_poradi';
		_s4ia_p3 := ' from (select distinct group_id_poradi from w_incomming) as t0 ';
	
		_s4result_p1 :=	'
						select
								a.*,
								b.categorization_setup
						from
								w_incomming_agg as a
						inner
						join	w_database_agg as b
						
						on ';	
	

		for i in 1..array_length(_array_id,1)
		loop
			if i = 1
			then
				_s4da_p2 := concat('
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');

				if _variant in (3,4)
				then				
					_s4ia_p2 := concat('
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule4insert as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');
				else
					_s4ia_p2 := concat('
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');				
				end if;								
								
				_s4da_p4 := concat(' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat('
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],' and
									((case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end)
									= (case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)) and
									((case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end)
									= (case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end))
								');
			else
				_s4da_p2 := concat(_s4da_p2,
									'
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');
								
				if _variant in (3,4)
				then				
					_s4ia_p2 := concat(_s4ia_p2,
										'
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule4insert as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');
				else
					_s4ia_p2 := concat(_s4ia_p2,
										'
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');				
				end if;								
								
				_s4da_p4 := concat(_s4da_p4,
									' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(_s4ia_p4,
									' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat(_s4result_p2,' and 
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],' and
									((case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end)
									= (case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)) and
									((case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end)
									= (case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end))
								');
			end if;
		end loop;
	
		_string4database_agg := _s4da_p1 || _s4da_p2 || _s4da_p3 || _s4da_p4;
		_string4incomming_agg := _s4ia_p1 || _s4ia_p2 || _s4ia_p3 || _s4ia_p4;
		_string4result := _s4result_p1 || _s4result_p2;
		-------------------------------------------------------------
		-------------------------------------------------------------

		_string_check_2 := 
		'
		,w_check_2 as		(
							select
									w_check_1.*,
									case
										when categorization_setup is null
											then
												case
													when	(
															select min(t.group_id) from w_check_1 as t
															where t.categorization_setup is null
															and t.group_id_poradi = w_check_1.group_id_poradi
															)
															= 1
													then 0
													else 1
												end 
											else 0
									end as check_exists_group_id_1
							from
									w_check_1
							)		
		';


		_string_check_4 :=
		'
		,w_check_4 as		(					
							select
									(select coalesce(max(id),0) from target_data.t_categorization_setup) + 
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as new_categorization_setup,
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as pozice,
									w_check_3.*
							from
									w_check_3
							where
									categorization_setup is null
							order
									by new_categorization_setup, pozice, group_id
							)		
		';

		_string_insert_1 :=
		'
		,w_insert_1 as		(
							insert into target_data.t_categorization_setup(id)
							select distinct new_categorization_setup from w_check_4 order by new_categorization_setup
							returning id
							)		
		';		
		
		_string_agg_and_result := concat(
		'
		,w_database as		(
							select id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
							from target_data.cm_ldsity2target2categorization_setup where ldsity2target_variable in
							(select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
							union all
							select id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
							from w_insert_2
							)
		,w_database_agg as	('||_string4database_agg||')
		,w_incomming_agg as	('||_string4incomming_agg||')
		,w_result as		('||_string4result||')
		select
			categorization_setup
		from
			w_result order by categorization_setup;
		');

		_res := _query_res || _string_inner_join || _string_incomming_0 || _string_incomming || _string_check_1 || _string_check_2 || _string_check_3 || _string_check_4 || _string_insert_1 || _string_insert_2 || _string_agg_and_result;
		
		execute ''||_res||'';
end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_save_categorization_setup(integer,integer[],integer[],integer[][],integer[],integer[][]) IS
'The function sets attribute configurations.';

grant execute on function target_data.fn_save_categorization_setup(integer,integer[],integer[],integer[][],integer[],integer[][]) to public;
-- </function>



-- <function name="fn_save_ldsity_values" schema="target_data" src="functions/fn_save_ldsity_values.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_save_ldsity_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_ldsity_values(integer[], integer, double precision) CASCADE;
	
create or replace function target_data.fn_save_ldsity_values
(
	_refyearset2panel_mapping	integer[],
	_target_variable			integer,
	_threshold					double precision default 0.0
)
returns text
as
$$
declare
		_max_categorization_setup				integer;
		_max_refyearset2panel_mapping			integer;
		_refyearset2panel_mapping_add_zero		integer[];
		_categorization_setup_add_zero			integer[];
		_refyearset2panel_mapping_without_zero	integer[];
		_categorization_setup_without_zero		integer[];
		_res_i									text;
		_res									text;
		_result									text;
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_save_ldsity_values: The input argument _refyearset2panel_mapping must not be NULL !';
		end if;
	
		if _target_variable is null
		then
			raise exception 'Error 02: fn_save_ldsity_values: The input argument _target_variable must not be NULL !';
		end if;

		if not exists (select t.* from target_data.c_target_variable as t where t.id = _target_variable)
			then raise exception 'Error: 03: fn_save_ldsity_values: Given target variable (%) does not exist in c_target_variable table.', _target_variable;
		END IF;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		with
		w1 as	(
				select * from target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable)
				)
		,w2 as	(
				select
						w1.categorization_setup,
						unnest(w1.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select distinct w2.categorization_setup, w2.refyearset2panel_mapping from w2
				)
		,w4 as	(
				select
						w3.refyearset2panel_mapping,
						array_agg(w3.categorization_setup order by w3.categorization_setup) as categorization_setup
				from
						w3 group by w3.refyearset2panel_mapping
				)
		,w5 as	(
				select * from w4 where w4.refyearset2panel_mapping in (select unnest(_refyearset2panel_mapping))
				)
		select max(array_length(w5.categorization_setup,1)) from w5
		into _max_categorization_setup;
		-----------------------------------------------------------------------
		if _max_categorization_setup is null
		then
			raise exception 'Error 04: fn_save_ldsity_values: For input argument _target_variable not exist any categorization setup!';
		end if;
		-----------------------------------------------------------------------
		with
		w1 as	(
				select * from target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable)
				)
		,w2 as	(
				select
						w1.categorization_setup,
						unnest(w1.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select distinct w2.categorization_setup, w2.refyearset2panel_mapping from w2
				)
		,w4 as	(
				select
						w3.refyearset2panel_mapping,
						array_agg(w3.categorization_setup order by w3.categorization_setup) as categorization_setup
				from
						w3 group by w3.refyearset2panel_mapping order by w3.refyearset2panel_mapping
				)
		,w5 as	(
				select * from w4 where w4.refyearset2panel_mapping in (select unnest(_refyearset2panel_mapping))
				)				
		,w6 as	(
				select
						w5.*,
						------------
						case
							when array_length(w5.categorization_setup,1) = _max_categorization_setup
							then w5.categorization_setup
							else
								w5.categorization_setup ||
								(select array_agg(t.categorization_setup) from (select 0 as categorization_setup, generate_series(1, (_max_categorization_setup - array_length(w5.categorization_setup,1)))) as t)
						end
							as categorization_setup_add_zero
				from
							w5
				)
		,w7 as	(
				select
						w6.categorization_setup_add_zero,
						array_agg(w6.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w6 group by w6.categorization_setup_add_zero
				)
		select max(array_length(w7.refyearset2panel_mapping,1)) from w7
		into _max_refyearset2panel_mapping;
		-----------------------------------------------------------------------
		if _max_refyearset2panel_mapping is null
		then
			raise exception 'Error 05: fn_save_ldsity_values: For input argument _target_variable not exist any panel refyearset combination!';
		end if;
		-----------------------------------------------------------------------
		with
		w1 as	(
				select * from target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable)
				)
		,w2 as	(
				select
						w1.categorization_setup,
						unnest(w1.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select distinct w2.categorization_setup, w2.refyearset2panel_mapping from w2
				)
		,w4 as	(
				select
						w3.refyearset2panel_mapping,
						array_agg(w3.categorization_setup order by w3.categorization_setup) as categorization_setup
				from
						w3 group by w3.refyearset2panel_mapping order by w3.refyearset2panel_mapping
				)
		,w5 as	(
				select * from w4 where w4.refyearset2panel_mapping in (select unnest(_refyearset2panel_mapping))
				)				
		,w6 as	(
				select
						w5.*,
						------------
						case
							when array_length(w5.categorization_setup,1) = _max_categorization_setup
							then w5.categorization_setup
							else
								w5.categorization_setup ||
								(select array_agg(t.categorization_setup) from (select 0 as categorization_setup, generate_series(1, (_max_categorization_setup - array_length(w5.categorization_setup,1)))) as t)
						end
							as categorization_setup_add_zero
				from
							w5
				)
		,w7 as	(
				select
						w6.categorization_setup_add_zero,
						array_agg(w6.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w6 group by w6.categorization_setup_add_zero
				)	
		,w8 as	(
				select
						w7.*,
						case
							when array_length(w7.refyearset2panel_mapping,1) = _max_refyearset2panel_mapping
							then w7.refyearset2panel_mapping
							else
								w7.refyearset2panel_mapping ||
								(select array_agg(t.refyearset2panel_mapping) from (select 0 as refyearset2panel_mapping, generate_series(1, (_max_refyearset2panel_mapping - array_length(w7.refyearset2panel_mapping,1)))) as t)
						end
							as refyearset2panel_mapping_add_zero
				from
							w7
				)
		select
				array_agg(w8.refyearset2panel_mapping_add_zero order by w8.refyearset2panel_mapping) as refyearset2panel_mapping_add_zero,
				array_agg(w8.categorization_setup_add_zero order by w8.refyearset2panel_mapping) as categorization_setup_add_zero
		from
				w8
		into
				_refyearset2panel_mapping_add_zero,
				_categorization_setup_add_zero;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		for i in 1..array_length(_refyearset2panel_mapping_add_zero,1)
		loop
			-- check that values in array _refyearset2panel_mapping_add_zero[i:i] must not be only zeroes !!
			if	(
				select count(t.res) = 0 from
				(select unnest(_refyearset2panel_mapping_add_zero[i:i]) as res) as t
				where t.res is distinct from 0
				)
			then
				raise exception 'Error 06: fn_save_ldsity_values: Array of panel refyearset combinations is an empty array!';
			end if;

			-- check that values in array _categorization_setup_add_zero[i:i] must not be only zeroes !!
			if	(
				select count(t.res) = 0 from
				(select unnest(_categorization_setup_add_zero[i:i]) as res) as t
				where t.res is distinct from 0
				)
			then
				raise exception 'Error 07: fn_save_ldsity_values: Array of categorization setups is an empty array!';
			end if;

			-- remove zeroes from array _refyearset2panel_mapping_add_zero
			with
			w1 as	(
					select unnest(_refyearset2panel_mapping_add_zero[i:i]) as refyearset2panel_mapping
					)
			select
					array_agg(w1.refyearset2panel_mapping)
			from
					w1 where w1.refyearset2panel_mapping is distinct from 0
			into
				_refyearset2panel_mapping_without_zero;

			-- remove zeroes from array _categorization_setup_add_zero
			with
			w1 as	(
					select unnest(_categorization_setup_add_zero[i:i]) as categorization_setup
					)
			select
					array_agg(w1.categorization_setup)
			from
					w1 where w1.categorization_setup is distinct from 0
			into
				_categorization_setup_without_zero;

			--raise notice '_refyearset2panel_mapping_without_zero: %',_refyearset2panel_mapping_without_zero;
			--raise notice '_categorization_setup_without_zero: %',_categorization_setup_without_zero;

			_res_i := target_data.fn_save_ldsity_values_internal
						(
							_refyearset2panel_mapping_without_zero,
							_categorization_setup_without_zero,
							_threshold
						);

			if i = 1
			then
				_res := _res_i;
			else
				_res := concat(_res,' ',_res_i);
			end if;

		end loop;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		raise notice '%',_res;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		_result := 'The ldsity values were prepared successfully.'::text;

		return _result;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_save_ldsity_values(integer[], integer, double precision) IS
'The function internally calls function fn_save_ldsity_values_internal.';

grant execute on function target_data.fn_save_ldsity_values(integer[], integer, double precision) to public;
-- </function>



-- <function name="fn_save_ldsity_values_internal" schema="target_data" src="functions/fn_save_ldsity_values_internal.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_save_ldsity_values_internal
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_ldsity_values_internal(integer[], integer[], double precision) CASCADE;
	
create or replace function target_data.fn_save_ldsity_values_internal
(
	_refyearset2panel_mapping	integer[],
	_categorization_setups		integer[],
	_threshold					double precision
)
returns text
as
$$
declare
		_array_id									integer[];
		_array_target_variable						integer[];
		_target_variable							integer;
		_lot_100									integer;
		_lot_200									integer;
		_variant									integer;
		_tv_target_variable							integer;
		_ldsity										integer;
		_ldsity_object_type							integer;
		_tv_area_domain_category					integer[];
		_tv_sub_population_category					integer[];
		--_tv_definition_variant					integer[];
		_tv_use_negative							boolean;
		_tv_version									integer;
		_state_or_change							integer;
		_ldsity_ldsity_object						integer;
		_ldsity_column_expresion					text;
		_ldsity_unit_of_measure						integer;
		_ldsity_area_domain_category				integer[];
		_ldsity_sub_population_category				integer[];
		_ldsity_definition_variant					integer[];	
		_area_domain_category						integer[];
		_sub_population_category					integer[];
		_definition_variant							integer[];	
		_area_domain_category_i						integer[];
		_area_domain_category_t						text[];	
		_sub_population_category_i					integer[];
		_sub_population_category_t					text[];
		_condition_ids								integer[];
		_condition_types							text[];
		_example_query_1							text;
		_example_query								text;
		_ldsity_objects								integer[];
		_filter_i									text;
		_filters_array								text[];	
		_example_query_i							text;
		_example_query_array						text[];
		_query_res									text;
		_column_name_i_array						text[];	
		_ii_column_new_text							text;
		_pozice_i									integer;
		_classification_rule_i						text;	
		_pozice_array								integer[];
		_classification_rule_array					text[];	
		_position_groups							integer[];
		_classification_rule_upr_ita_pozice			text[];
		_classification_rule_ita_pozice				text;
		_ldsity_objects_without_conditions			integer[];
		_string4columns								text;
		_string4inner_joins							text;	
		_position4join								integer;
		_table_name4join							text;
		_pkey_column4join							text;
		_i_column4join								text;
		_string4attr								text;
		_string4case								text;
		_position4ldsity							integer;
		_string4ads									text;
		_query_res_big								text;
		_string_var									text;	
		_string_result_1							text;	
		_id_cat_setups_array						integer[];
		_check_bez_rozliseni						integer[];
		_check_bez_rozliseni_boolean				boolean[];	
		_string_var_100								text;
		_array_id_200								integer[];	
		_string_var_200								text;
		_string_check_insert						text;	
		_max_id_tlv									integer;
		_res										text;
		_ext_version_text							text;
		_case4available_datasets_text				text;
		_categorization_setups_adc					integer[];
		_categorization_setups_spc					integer[];
		_ldsity_column_expresion_modified			text;
	
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_save_ldsity_values_internal: The input argument _refyearset2panel_mapping must not be NULL !';
		end if;
		
		if _categorization_setups is null
		then
			raise exception 'Error 02: fn_save_ldsity_values_internal: The input argument _categorization_setups must not be NULL !';
		end if;
	
		if	(
			select count(t.res) > 0 from
			(select unnest(_categorization_setups) as res) as t
			where t.res is null
			)
		then
			raise exception 'Error 03: fn_save_ldsity_values_internal: The input argument _categorization_setups must not contains NULL value!';
		end if;
	
		/*
		-------------------------------------------------------------
		-------------------------------------------------------------
		with
		w as	(
				select distinct categorization_setup
				from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(	
					select id from target_data.cm_ldsity2target_variable
					where target_variable =
								(
								select distinct target_variable from target_data.cm_ldsity2target_variable
								where id in	(
											select ldsity2target_variable
											from target_data.cm_ldsity2target2categorization_setup
											where categorization_setup in (select unnest(_categorization_setups))
											)
								)
					)
				and (
					adc2classification_rule is null
					or array_length(adc2classification_rule,1) <= 
						(select max(t.res) from
						(select array_length(adc2classification_rule,1) as res
						from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						and adc2classification_rule is not null) as t)
					)
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups_adc;

		with
		w as	(
				select distinct categorization_setup
				from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(	
					select id from target_data.cm_ldsity2target_variable
					where target_variable =
								(
								select distinct target_variable from target_data.cm_ldsity2target_variable
								where id in	(
											select ldsity2target_variable
											from target_data.cm_ldsity2target2categorization_setup
											where categorization_setup in (select unnest(_categorization_setups))
											)
								)
					)
				and (
					spc2classification_rule is null
					or array_length(spc2classification_rule,1) <= 
						(select max(t.res) from
						(select array_length(spc2classification_rule,1) as res
						from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						and spc2classification_rule is not null) as t)
					)
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups_spc;

		with
		w as	(
				select a.categorization_setup from
					(select unnest(_categorization_setups_adc) as categorization_setup) as a
				inner join
					(select unnest(_categorization_setups_spc) as categorization_setup) as b
				on a.categorization_setup = b.categorization_setup
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups;
		-------------------------------------------------------------
		-------------------------------------------------------------
		*/

		select
				array_agg(id order by ldsity_object_type, id) as id,
				array_agg(target_variable order by ldsity_object_type, id) as target_variable
		from
				target_data.cm_ldsity2target_variable
		where
				id in	(
						select ldsity2target_variable from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						)
		into
				_array_id,
				_array_target_variable;
		
		if _array_id is null
		then
			raise exception 'Error 04: fn_save_ldsity_values_internal: For input values in argument _categorization_setups = % not found any record in table cm_ldsity2target_variable!',_categorization_setups;
		end if;
		
		_target_variable := (select distinct t.res from (select unnest(_array_target_variable) as res) as t);
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 05: fn_save_ldsity_values_internal: This variant is not implemented yet!';
		end if;
	
		---------------------
		---------------------
		for bc in 1..array_length(_array_id,1)
		loop
			-- datas from table cm_ldsity2target_variable
			select
					target_variable,
					ldsity,
					ldsity_object_type,
					area_domain_category,
					sub_population_category,
					--definition_variant,
					use_negative,
					version
			from
					target_data.cm_ldsity2target_variable
			where
					id = _array_id[bc]
			into
					_tv_target_variable,
					_ldsity,
					_ldsity_object_type,
					_tv_area_domain_category,
					_tv_sub_population_category,
					--_tv_definition_variant,
					_tv_use_negative,
					_tv_version;
				
			-- datas from c_target_variable	
			select state_or_change from target_data.c_target_variable where id = _tv_target_variable
			into _state_or_change;
		
			-----------------------------------------------------------
			-- INTERMEDIATE STEP => insert values into table t_available_datasets
			-----------------------------------------------------------
			/*
			with
			w1 as	(
					select distinct reference_year_set, panel
					from target_data.fn_get_plots(_panels,_reference_year_sets,_state_or_change,_tv_use_negative)
					order by reference_year_set, panel
					)
			,w2 as	(select unnest(array[_categorization_setups]) as categorization_setup)
			,w3 as	(select w1.*, w2.* from w1, w2)
			insert into target_data.t_available_datasets(panel, reference_year_set, categorization_setup)
			select panel, reference_year_set, categorization_setup from w3
			except
			select panel, reference_year_set, categorization_setup from target_data.t_available_datasets
			where categorization_setup in (select unnest(array[_categorization_setups]))
			order by categorization_setup, panel, reference_year_set;
			*/

			with
			w1 as	(
					select distinct reference_year_set, panel
					from target_data.fn_get_plots(_refyearset2panel_mapping,_state_or_change,_tv_use_negative)
					order by reference_year_set, panel
					)
			,w2 as	(select unnest(array[_categorization_setups]) as categorization_setup)
			,w3 as	(select w1.*, w2.* from w1, w2)
			insert into target_data.t_available_datasets(panel, reference_year_set, categorization_setup)
			select panel, reference_year_set, categorization_setup from w3
			except
			select panel, reference_year_set, categorization_setup from target_data.t_available_datasets
			where categorization_setup in (select unnest(array[_categorization_setups]))
			order by categorization_setup, panel, reference_year_set;			
			-----------------------------------------------------------
			-----------------------------------------------------------
	
			-- datas from table c_ldsity
			select
					ldsity_object,
					column_expression,
					unit_of_measure,
					area_domain_category,
					sub_population_category,
					definition_variant
			from
					target_data.c_ldsity where id = _ldsity
			into
					_ldsity_ldsity_object,
					_ldsity_column_expresion,
					_ldsity_unit_of_measure,
					_ldsity_area_domain_category,
					_ldsity_sub_population_category,
					_ldsity_definition_variant;
							 
			_area_domain_category := _ldsity_area_domain_category || _tv_area_domain_category;
			_sub_population_category := _ldsity_sub_population_category || _tv_sub_population_category;
			_definition_variant := _ldsity_definition_variant; --|| _tv_definition_variant;

			-----------------------------------------
			if _area_domain_category is not null
			then
				for i in 1..array_length(_area_domain_category,1)
				loop
					if i = 1
					then
						_area_domain_category_i := array[_area_domain_category[i]];
						_area_domain_category_t := array['ad'];
					else
						_area_domain_category_i := _area_domain_category_i || array[_area_domain_category[i]];
						_area_domain_category_t := _area_domain_category_t || array['ad'];
					end if;
				end loop;
			else
				_area_domain_category_i := null::integer[];
				_area_domain_category_t := null::text[];
			end if;
			-----------------------------------------
			-----------------------------------------
			if _sub_population_category is not null
			then
				for i in 1..array_length(_sub_population_category,1)
				loop
					if i = 1
					then
						_sub_population_category_i := array[_sub_population_category[i]];
						_sub_population_category_t := array['sp'];
					else
						_sub_population_category_i := _sub_population_category_i || array[_sub_population_category[i]];
						_sub_population_category_t := _sub_population_category_t || array['sp'];
					end if;
				end loop;
			else
				_sub_population_category_i := null::integer[];
				_sub_population_category_t := null::text[];
			end if;
			-----------------------------------------
			-----------------------------------------
	
			_condition_ids := _area_domain_category_i || _sub_population_category_i;
			_condition_types := _area_domain_category_t || _sub_population_category_t;
	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if _condition_ids is null
			then
				--_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_panels,'],array[',_reference_year_sets,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');  -- pkey pro testy nahradit sloupcem plot
				_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_refyearset2panel_mapping,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');  -- pkey pro testy nahradit sloupcem plot
				_example_query := concat('w_&i& as (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#)');
			else
				--_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_panels,'],array[',_reference_year_sets,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');
				_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_refyearset2panel_mapping,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');
				_example_query := concat('w_&i& as (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#)');
			end if;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_ldsity_objects := target_data.fn_get_ldsity_objects(array[_ldsity_ldsity_object]);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					select case when filter is null then 'true' else filter end
					from target_data.c_ldsity_object
					where id = _ldsity_objects[i]
					into _filter_i;
				
					if i = 1
					then
						_filters_array := array[_filter_i];
					else
						_filters_array := _filters_array || array[_filter_i];
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					if _ldsity_objects[i] = 100
					then
						_example_query_i := _example_query_1;
					else
						_example_query_i := _example_query;
					end if;
				
					_example_query_i := replace(_example_query_i,'&i&'::text,i::text);
					_example_query_i := replace(_example_query_i,'#con4filter#',(select case when filter is null then 'true' else filter end from target_data.c_ldsity_object where id = _ldsity_objects[i]));
					_example_query_i := replace(_example_query_i,concat('#table_name#_',i),(select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]));				
				
					if i = 1
					then
						_example_query_array := array[_example_query_i];
					else
						_example_query_array := _example_query_array || _example_query_i;
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					if i = 1
					then
						_query_res := concat('with ',_example_query_array[i]);
					else
						_query_res := _query_res || concat(',',_example_query_array[i]);
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
				select array_agg(column_name order by ordinal_position) from information_schema.columns
			 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
			 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
				into _column_name_i_array;
				
				for ii in 1..array_length(_column_name_i_array,1)
				loop
					if ii = 1
					then
						_ii_column_new_text := concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i);
					else
						_ii_column_new_text := concat(_ii_column_new_text,', ',concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i));
					end if;
				end loop;
			
				_query_res := replace(_query_res,concat('#column_names_',i::text,'#'),_ii_column_new_text);
				
			end loop;	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if _condition_ids is not null
			then
				for i in 1..array_length(_condition_ids,1)
				loop
					if _condition_types[i] = 'ad'
					then			
						select array_position(_ldsity_objects,
						(select ldsity_object from target_data.cm_adc2classification_rule where id = _condition_ids[i]))
						into _pozice_i;
					
						if _pozice_i is null then raise exception 'Error 06: fn_save_ldsity_values_internal: For internal variable _condition_ids[i] = % not found position in internal variable _ldsity_objects = % !',_condition_ids[i],_ldsity_objects; end if;
									
						select case when classification_rule is null then 'true' else classification_rule end
						from target_data.cm_adc2classification_rule where id = _condition_ids[i]
						into _classification_rule_i;
					
						if _classification_rule_i is null then raise exception 'Error 07: fn_save_ldsity_values_internal: The classification rule in the cm_adc2classification_rule table was not found for the internal variable _condition_ids[i] = %!,',_condition_ids[i]; end if;
					
					end if;
				
					if _condition_types[i] = 'sp'
					then			
						select array_position(_ldsity_objects,
						(select ldsity_object from target_data.cm_spc2classification_rule where id = _condition_ids[i]))
						into _pozice_i;				
					
						if _pozice_i is null then raise exception 'Error 08: fn_save_ldsity_values_internal: For internal variable _condition_ids[i] = % not found position in internal variable _ldsity_objects = % !',_condition_ids[i],_ldsity_objects; end if;
								
						select case when classification_rule is null then 'true' else classification_rule end
						from target_data.cm_spc2classification_rule where id = _condition_ids[i]
						into _classification_rule_i;
					
						if _classification_rule_i is null then raise exception 'Error 09: fn_save_ldsity_values_internal: The classification rule in the cm_spc2classification_rule table was not found for the internal variable _condition_ids[i] = %!,',_condition_ids[i]; end if;				
					
					end if;
				
					if i = 1
					then
						_pozice_array := array[_pozice_i];
						_classification_rule_array := array[_classification_rule_i];
					else
						_pozice_array := _pozice_array || array[_pozice_i];
						_classification_rule_array := _classification_rule_array || array[_classification_rule_i];
					end if;
					
				end loop;
				
				with
				w1 as	(select unnest(_pozice_array) as pozice)
				,w2 as 	(select distinct pozice from w1)
				select array_agg(pozice order by pozice) from w2
				into _position_groups;
			
				for i in 1..array_length(_position_groups,1)
				loop
					with
					w1 as	(
							select
							unnest(_pozice_array) as pozice,
							unnest(_classification_rule_array) as classification_rule
							)
					,w2 as	(
							select
									row_number() over () as new_id,
									pozice,
									classification_rule
							from w1
							)
					,w3 as	(
							select
									row_number() over (partition by pozice order by new_id) as new_id4order,
									w2.*
							from w2
							)
					select array_agg(classification_rule order by new_id4order) as classification_rule
					from w3 where pozice = _position_groups[i]
					into _classification_rule_upr_ita_pozice;
				
					for ii in 1..array_length(_classification_rule_upr_ita_pozice,1)
					loop
						if ii = 1
						then
							_classification_rule_ita_pozice := concat(_classification_rule_upr_ita_pozice[ii]);
						else
							_classification_rule_ita_pozice := concat(_classification_rule_ita_pozice,' and ',_classification_rule_upr_ita_pozice[ii]);
						end if;
					end loop;
				
					_query_res := replace(_query_res,concat('#conditions_',_position_groups[i],'#'),concat('(',_classification_rule_ita_pozice,')'));
				
				end loop;
			
				with
				w1 as	(select * from generate_series(1,array_length(_ldsity_objects,1)) as res)
				,w2 as	(select unnest(_position_groups) as res)
				,w3 as	(select res from w1 except select res from w2)
				select array_agg(res order by res) from w3
				into _ldsity_objects_without_conditions;
				
				if _ldsity_objects_without_conditions is not null
				then
					for i in 1..array_length(_ldsity_objects_without_conditions,1)
					loop
						_query_res := replace(_query_res,concat('#conditions_',_ldsity_objects_without_conditions[i],'#'),'(true)');
					end loop;
				end if;
						
			end if;	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
				if i = 1
				then
					_string4columns := concat(',w_res as (select w_',i,'.*');
					_string4inner_joins := concat(' from w_',i);
				else
					_string4columns := concat(_string4columns,', w_',i,'.*');
				
					select array_position(_ldsity_objects, 
						(select upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]))
					into _position4join;
					
					for i in 1..array_length(_ldsity_objects,1)
					loop
							if i = _position4join
							then
								_table_name4join := (select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]);
							end if;
					end loop;
				
					with
					w as	(
							select kcu.table_schema,
							       kcu.table_name,
							       tco.constraint_name,
							       kcu.ordinal_position as position,
							       kcu.column_name as key_column
							from information_schema.table_constraints tco
							join information_schema.key_column_usage kcu 
							     on kcu.constraint_name = tco.constraint_name
							     and kcu.constraint_schema = tco.constraint_schema
							     and kcu.constraint_name = tco.constraint_name
							where tco.constraint_type = 'PRIMARY KEY'
							order by kcu.table_schema,
							         kcu.table_name,
							         position
							 )
					select key_column::text from w
					where table_schema = (substring(_table_name4join from 1 for (position('.' in _table_name4join) - 1))) 
					and table_name = (substring(_table_name4join from (position('.' in _table_name4join) + 1) for length(_table_name4join)))
					into _pkey_column4join;
					
					select column4upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]
					into _i_column4join;
				
					_string4inner_joins :=
					concat(_string4inner_joins,' inner join w_',i,' on ','w_',_position4join,'.',_pkey_column4join,'_',_position4join,' = ','w_',i,'.',_i_column4join,'_',i);
							
				end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------		
			_query_res := _query_res || _string4columns || _string4inner_joins || ')';	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if	(_variant in (3,4) and _ldsity_ldsity_object is distinct from (select id from target_data.c_ldsity_object where upper_object is null))
			then
				_string4attr := concat('
				select
					case
						when ws100.adc2classification_rule is null
								then array[0]
								else (select id_category from target_data.fn_get_category4classification_rule_id(''adc'',ws100.adc2classification_rule))
					end as area_domain_category,
					case
						when ws200.spc2classification_rule is null
								then array[0]
								else (select id_category from target_data.fn_get_category4classification_rule_id(''spc'',ws200.spc2classification_rule))
					end as sub_population_category,
					ws100.adc2classification_rule,
					ws200.spc2classification_rule,
					ws200.categorization_setup
				from			
					(		
					select
							adc2classification_rule,
							spc2classification_rule,
							categorization_setup
					from
							target_data.cm_ldsity2target2categorization_setup
					where
							categorization_setup in
													(
													select unnest($1)
													except
													select t.categorization_setup from
													(select a.*, b.* from
													(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest($1))
													and ldsity2target_variable = ',_array_id[bc],') as a
													inner join target_data.cm_spc2classification_rule as b
													on a.id_cm_spc = b.id) as t
													where t.classification_rule = ''NOT EXISTS''	
													)
					and
							ldsity2target_variable = ',_array_id[bc],'
					) as ws200
				inner join 
					(					
					select adc2classification_rule, categorization_setup from target_data.cm_ldsity2target2categorization_setup
					where ldsity2target_variable = ',_array_id[1],'
					) as ws100
				on
					ws200.categorization_setup = ws100.categorization_setup	
				');
			else			
				_string4attr := concat('
				select
						case
							when adc2classification_rule is null
									then array[0]
									else (select id_category from target_data.fn_get_category4classification_rule_id(''adc'',adc2classification_rule))
						end as area_domain_category,
						case
							when spc2classification_rule is null
									then array[0]
									else (select id_category from target_data.fn_get_category4classification_rule_id(''spc'',spc2classification_rule))
						end as sub_population_category,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest($1))
				and
						ldsity2target_variable = ',_array_id[bc],'
				');
			end if;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			-- dopleni radku bez rozliseni pokud schazi, radek je potreba u varianty 3 a 4 !!!
			if _variant in (3,4)
			then	
				for i in 1..array_length(_categorization_setups,1)
				loop
					select array_agg(id order by id)
					from target_data.cm_ldsity2target2categorization_setup
					where categorization_setup = _categorization_setups[i]
					into _id_cat_setups_array;
				
					for ii in 1..array_length(_id_cat_setups_array,1)
					loop
						if	(
							select count(*) = 1
							from target_data.cm_ldsity2target2categorization_setup
							where id = _id_cat_setups_array[ii]
							and (adc2classification_rule is null and spc2classification_rule is null)
							)
						then
							if ii = 1
							then
								_check_bez_rozliseni := array[1];
							else
								_check_bez_rozliseni := _check_bez_rozliseni || array[1];
							end if;
						else
							if ii = 1
							then
								_check_bez_rozliseni := array[0];
							else
								_check_bez_rozliseni := _check_bez_rozliseni || array[0];
							end if;
						end if;
					end loop;
				
					if		(
							select sum(t.res) = array_length(_id_cat_setups_array,1)
							from (select unnest(_check_bez_rozliseni) as res) as t
							)
					then
						if i = 1
						then
							_check_bez_rozliseni_boolean := array[true];
						else
							_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[true];
						end if;
					else
						if i = 1
						then
							_check_bez_rozliseni_boolean := array[false];
						else
							_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[false];
						end if;
					end if;	
				end loop;
			
				if	(
					select count(t.res) = 0
					from (select unnest(_check_bez_rozliseni_boolean) as res) as t
					where t.res = true
					)
				then
					with
					w1 as	(
							select * from target_data.cm_ldsity2target2categorization_setup
							where ldsity2target_variable in	(
															select distinct ldsity2target_variable
															from target_data.cm_ldsity2target2categorization_setup
															where categorization_setup in (select unnest(_categorization_setups))
															)
							and (adc2classification_rule is null and spc2classification_rule is null)
							)
					,w2 as	(
							select categorization_setup, count(categorization_setup) as pocet
							from w1 group by categorization_setup
							)
					select array[categorization_setup] || _categorization_setups
					from w2 where pocet = (select max(pocet) from w2)
					into _categorization_setups;
				
				else
					_categorization_setups := _categorization_setups;
				end if;
			end if;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_string4case := target_data.fn_get_classification_rules4categorization_setups(_categorization_setups,_array_id[bc]);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_position4ldsity := array_position(_ldsity_objects,_ldsity_ldsity_object);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_ldsity_column_expresion_modified := target_data.fn_get_modified_ldsity_column_expression(_ldsity_column_expresion,_position4ldsity,_ldsity_ldsity_object);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_string4ads := concat('
			,w_strings as	('||_string4attr||')
			,w_pre_adc as	(-- tady je cross join => zde dojde ke kazdemu zaznamu z w_res k rozjoinovani na vsechny kategorie, ktere jsou ve w_strings
							select
									w_strings.*,
									w_res.*
							from
									w_res, w_strings
							)
			,w_ads as		(
							select
									',_string4case,' as res_case,
									w_pre_adc.*
							from
									w_pre_adc
							)
			select
					res_case,
					area_domain_category,
					sub_population_category,
					adc2classification_rule,
					spc2classification_rule,
					(',case when _tv_use_negative = true then '(-1)' else '(1)' end,' * coalesce(',_ldsity_column_expresion_modified,',0.0)) as value,
					gid,
					cluster_configuration,
					reference_year_set4join,
					reference_year_set,
					panel,
					stratum,
					categorization_setup
			from
					w_ads
			');	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_query_res := _query_res || _string4ads;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if bc = 1
			then
				_query_res_big := concat('with bw_',bc,' as (',_query_res,')');
			else
				_query_res_big := concat(_query_res_big,',bw_',bc,' as (',_query_res,')');
			end if;
		
		end loop;		
		-----------------------------------------------------------------------
		-- end BIG CYCLE
		-----------------------------------------------------------------------

		_ext_version_text := (select extversion from pg_extension where extname = 'nfiesta_target_data');

		_case4available_datasets_text := target_data.fn_get_case_ad4categorization_setups(_categorization_setups);
		
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _variant in (1,2)
		then		
				for bc in 1..array_length(_array_id,1)
				loop
					if bc = 1
					then
						_string_var := concat(',w_union_all as (select ',_array_id[bc],' as ldsity2target_variable,* from bw_',bc,' where res_case = true');
					else
						_string_var := concat(_string_var,' union all select ',_array_id[bc],' as ldsity2target_variable,* from bw_',bc,' where res_case = true');
					end if;
				end loop;
			
				_string_var := concat(_string_var,')');
			
				_string_result_1 := concat(
				'
				,w_part_1 as		(
									select
											gid,
											panel,
											reference_year_set,
											categorization_setup,
											sum(value) as value
									from
											w_union_all as t1
									group
											by gid, panel, reference_year_set, categorization_setup
									)
				,w_result_1 as		(
									select
											t1.gid as plot,
											t1.panel,
											t1.reference_year_set,
											t1.categorization_setup,
											t1.value,
											',_case4available_datasets_text,' as available_datasets
									from
											w_part_1 as t1 where t1.value is distinct from 0 -- toto zajisti praci s nenulovymi hodnotama
									)
				');

		_query_res_big := _query_res_big || _string_var || _string_result_1;
	
		end if;

		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _variant in (3,4)
		then
			/*		
			for i in 1..array_length(_categorization_setups,1)
			loop
				select array_agg(id order by id)
				from target_data.cm_ldsity2target2categorization_setup
				where categorization_setup = _categorization_setups[i]
				into _id_cat_setups_array;
			
				for ii in 1..array_length(_id_cat_setups_array,1)
				loop
					if	(
						select count(*) = 1
						from target_data.cm_ldsity2target2categorization_setup
						where id = _id_cat_setups_array[ii]
						and (adc2classification_rule is null and spc2classification_rule is null)
						)
					then
						if ii = 1
						then
							_check_bez_rozliseni := array[1];
						else
							_check_bez_rozliseni := _check_bez_rozliseni || array[1];
						end if;
					else
						if ii = 1
						then
							_check_bez_rozliseni := array[0];
						else
							_check_bez_rozliseni := _check_bez_rozliseni || array[0];
						end if;
					end if;
				end loop;
			
				if		(
						select sum(t.res) = array_length(_id_cat_setups_array,1)
						from (select unnest(_check_bez_rozliseni) as res) as t
						)
				then
					if i = 1
					then
						_check_bez_rozliseni_boolean := array[true];
					else
						_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[true];
					end if;
				else
					if i = 1
					then
						_check_bez_rozliseni_boolean := array[false];
					else
						_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[false];
					end if;
				end if;	
			end loop;
		
			if	(
				select count(t.res) = 0
				from (select unnest(_check_bez_rozliseni_boolean) as res) as t
				where t.res = true
				)
			then
				with
				w1 as	(
						select * from target_data.cm_ldsity2target2categorization_setup
						where ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))
														)
						and (adc2classification_rule is null and spc2classification_rule is null)
						)
				,w2 as	(
						select categorization_setup, count(categorization_setup) as pocet
						from w1 group by categorization_setup
						)
				select array[categorization_setup] || _categorization_setups
				from w2 where pocet = (select max(pocet) from w2)
				into _categorization_setups;
			
			else
				_categorization_setups := _categorization_setups;
			end if;
			*/

			_string_var_100 := concat(',w_union_all_100 as (select ',_array_id[1],' as ldsity2target_variable,* from bw_1 where res_case = true)');
		
			_array_id_200 := array_remove(_array_id, _array_id[1]);
				
			for bc_200 in 1..array_length(_array_id_200,1)
			loop
				if bc_200 = 1
				then
					_string_var_200 := concat(',w_union_all_200 as (select ',_array_id_200[bc_200],' as ldsity2target_variable,* from bw_',bc_200 + 1,' where res_case = true');
				else
					_string_var_200 := concat(_string_var_200,' union all select ',_array_id_200[bc_200],' as ldsity2target_variable,* from bw_',bc_200 + 1,' where res_case = true');
				end if;				
			end loop;
		
			_string_var_200 := concat(_string_var_200,')');
			
			_string_result_1 := concat('
			,w_part_1 as		(
								select
										t1.*,
										',_case4available_datasets_text,' as available_datasets
								from
										w_union_all_100 as t1
								where
										value is distinct from 0.0			
								)
			,w_part_2 as		(			
								select
									t1.gid,
									t1.panel,
									t1.reference_year_set,
									t1.categorization_setup,
									t1.area_domain_category,
									t1.value,
									t2.value_sum,
									case when t2.value_sum = 0.0 then t2.value_sum else t1.value/t2.value_sum end as value4nasobeni,
									',_case4available_datasets_text,' as available_datasets
								from
									(
									select
									gid, panel, reference_year_set, categorization_setup, area_domain_category,
									sum(value) as value
									from w_union_all_200
									where value is distinct from 0.0
									group by gid, panel, reference_year_set, categorization_setup, area_domain_category
									) as t1
								inner join 
									(
									select gid, panel, reference_year_set,
									sum(value) as value_sum
									from w_union_all_200
									where (area_domain_category = array[0] and sub_population_category = array[0])
									and value is distinct from 0.0
									group by gid, panel, reference_year_set
									) as t2
								on
									t1.gid = t2.gid and
									t1.panel = t2.panel and
									t1.reference_year_set = t2.reference_year_set
							)
			,w_result as	(
							select
									t.*,
									coalesce((t.value * coef_200.value4nasobeni),0.0) as value_res
							from 
									w_part_1 as t inner join w_part_2 as coef_200
							on
									t.gid = coef_200.gid and t.available_datasets = coef_200.available_datasets
							)
			,w_not_exists as	(
								/*
								select * from w_part_1
								where gid in (select distinct gid from w_part_1 except select distinct gid from w_part_2)
								and categorization_setup in							
									(
										-- category non-sortable
										select t1.categorization_setup from
										(select a.*, b.* from
										(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
										from target_data.cm_ldsity2target2categorization_setup
										where ldsity2target_variable in (select unnest($2))
										) as a inner join target_data.cm_spc2classification_rule as b
										on a.id_cm_spc = b.id) as t1
										where t1.classification_rule = ''NOT EXISTS''
										union all
										-- category without distinction
										select t2.categorization_setup
										from	(
												select categorization_setup, count(categorization_setup) as pocet
												from target_data.cm_ldsity2target2categorization_setup
												where ldsity2target_variable in (select unnest($2))
												and (adc2classification_rule is null and spc2classification_rule is null)
												group by categorization_setup
												) as t2
										where t2.pocet = array_length($2,1)
									)
								*/

								select * from w_part_1
								where gid in (select distinct gid from w_part_1 except select distinct gid from w_part_2)
								and categorization_setup in							
									(
										-- category non-sortable
										select distinct t1.categorization_setup from
										(select a.*, b.* from
										(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
										from target_data.cm_ldsity2target2categorization_setup
										where ldsity2target_variable in (select unnest($2))
										) as a inner join target_data.cm_spc2classification_rule as b
										on a.id_cm_spc = b.id) as t1
										where t1.classification_rule = ''NOT EXISTS''
										union all
										-- categorization_setup that is a category without distinction
										select t2.categorization_setup
										from	(
												select categorization_setup, count(categorization_setup) as pocet
												from target_data.cm_ldsity2target2categorization_setup
												where ldsity2target_variable in (select unnest($2))
												and (adc2classification_rule is null and spc2classification_rule is null)
												group by categorization_setup
												) as t2
										where t2.pocet = array_length($2,1)
										union all
										-- categorization_setup for	ADC																			
										select categorization_setup
										from target_data.cm_ldsity2target2categorization_setup
										where categorization_setup in
											(
											select t.categorization_setup from 
											(select categorization_setup, count(categorization_setup) as pocet
											from target_data.cm_ldsity2target2categorization_setup
											where ldsity2target_variable in (select unnest($2))
											and spc2classification_rule is null
											group by categorization_setup) as t
											where t.pocet = array_length($2,1)
											)
										and adc2classification_rule in
										(
										-- category non-sortable for ADC
										select adc2classification_rule from
										target_data.cm_ldsity2target2categorization_setup
										where categorization_setup in 
										(select distinct t1.categorization_setup from
										(select a.*, b.* from
										(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
										from target_data.cm_ldsity2target2categorization_setup
										where ldsity2target_variable in (select unnest($2))
										) as a inner join target_data.cm_spc2classification_rule as b
										on a.id_cm_spc = b.id) as t1
										where t1.classification_rule = ''NOT EXISTS'')
										and adc2classification_rule is not null
										)
									)
								)
			,w_result_1 as	(
							select gid as plot, value_res as value, available_datasets from w_result union --all
							select gid as plot, value, available_datasets from w_not_exists
							)
			');
		
			_query_res_big := _query_res_big || _string_var_100 || _string_var_200 || _string_result_1;
		
		end if;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
			
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		_string_check_insert := replace(replace(concat
		(',w_tlv as		(
						select
								tlv.id,
								tlv.plot,
								tlv.available_datasets,
								tlv.value,
								coalesce(w_result_1.plot,tlv.plot) as plot_upr,
								coalesce(w_result_1.available_datasets,tlv.available_datasets) as available_datasets_upr,
								case
									when tlv.value is     null and w_result_1.value is not null then w_result_1.value
									when tlv.value is not null and tlv.value is distinct from 0.0 and w_result_1.value is null then 0.0
									when tlv.value is not null and tlv.value = 0.0 and w_result_1.value is null then null::double precision
									else w_result_1.value
								end
									as value_upr,
								case
									when tlv.value is     null and w_result_1.value is not null then false
									when tlv.value is not null and tlv.value is distinct from 0.0 and w_result_1.value is null then false
									when tlv.value is not null and tlv.value = 0.0 and w_result_1.value is null then null::boolean									
									else
										case
											when tlv.value = 0.0 and w_result_1.value = 0.0 then true
											when tlv.value = 0.0 and w_result_1.value is distinct from 0.0
													then
														case
															when (abs(1 - (tlv.value / w_result_1.value)) * 100.0) <= ',_threshold,' then true
															else false
														end
											when tlv.value is distinct from 0.0 and w_result_1.value = 0.0
													then
														case
															when (abs(1 - (w_result_1.value / tlv.value)) * 100.0) <= ',_threshold,'	then true
															else false
														end
											else
												case
													when (abs(1 - (tlv.value / w_result_1.value)) * 100.0) <= ',_threshold,' then true
													else false
												end
										end									
								end
									as value_identic
						from
							(
							select * from target_data.t_ldsity_values
							where available_datasets in
								(
								select
									a.id
								from
									(
									select * from target_data.t_available_datasets
									where categorization_setup in (select unnest($1))
									) as a
								inner
								join
									(
									select * from sdesign.cm_refyearset2panel_mapping
									where id in (select unnest(array[',_refyearset2panel_mapping,']))
									) as b
								on
									a.panel = b.panel and a.reference_year_set = b.reference_year_set
								)
							and is_latest = true
							) as tlv
						full outer join w_result_1
						on (tlv.plot = w_result_1.plot and tlv.available_datasets = w_result_1.available_datasets)
						)
		,w_update as	(
						update target_data.t_ldsity_values set is_latest = false where id in
						(select id from w_tlv where value_identic = false and id is not null)
						returning t_ldsity_values.plot, t_ldsity_values.available_datasets
						)
		insert into target_data.t_ldsity_values(plot,available_datasets,value,creation_time,is_latest,ext_version)
		select
				w_tlv.plot_upr as plot,
				w_tlv.available_datasets_upr as available_datasets,
				w_tlv.value_upr as value,
				now() as creation_time,
				true as is_latest,
				''',_ext_version_text,''' as ext_version
		from
				w_tlv
				left join w_update on w_tlv.plot_upr = w_update.plot and w_tlv.available_datasets_upr = w_update.available_datasets
		where
				w_tlv.value_identic = false
		order
				by w_tlv.plot_upr, w_tlv.available_datasets_upr;
		'),'{',''),'}','');	
	
	_query_res_big := _query_res_big || _string_check_insert;

	---------------------------------------------
	---------------------------------------------
	select coalesce(max(id),0) from target_data.t_ldsity_values into _max_id_tlv;
	---------------------------------------------
	---------------------------------------------
	refresh materialized view target_data.v_variable_hierarchy;
	---------------------------------------------
	---------------------------------------------
	execute ''||_query_res_big||'' using _categorization_setups, _array_id;
	---------------------------------------------
	---------------------------------------------
	_res := concat('The ',(select count(*) from target_data.t_ldsity_values where id > _max_id_tlv),' new local densities were prepared for ',(select count(t.plot) from (select distinct plot from target_data.t_ldsity_values where id > _max_id_tlv) as t),' plots.');
	---------------------------------------------
	---------------------------------------------
	return _res;
	---------------------------------------------
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_save_ldsity_values_internal(integer[], integer[], double precision) IS
'The function for the specified list of input arguments inserts data into the t_available_datasets table and inserts data into the t_ldsity_values table (aggregated local density at the plot level).';

grant execute on function target_data.fn_save_ldsity_values_internal(integer[], integer[], double precision) to public;

-- </function>