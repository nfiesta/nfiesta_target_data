--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_etl_get_target_variable" schema="target_data" src="functions/fn_etl_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(integer, character varying, boolean) CASCADE;

create or replace function target_data.fn_etl_get_target_variable
(
	_export_connection	integer,
	_national_language	character varying(2) default 'en'::character varying(2),
	_etl				boolean default null::boolean
)
returns table
(
	id							integer,
	label						varchar,
	id_etl_target_variable		integer,
	check_target_variable		boolean,
	refyearset2panel_mapping	integer[],
	metadata					json
)
as
$$
declare
		_cond			text;
		_set_column		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable: Input argument _export_connection must not be null!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_get_target_variable: Input argument _national_language must not be null!';
		end if;

		if _etl is null
		then
			_cond := 'TRUE';
		else
			if _etl = true
			then
				_cond := 'w2.check_target_variable = TRUE';
			else
				_cond := 'w2.check_target_variable = FALSE';
			end if;
		end if;

		if _national_language = 'en'
		then
			_set_column := 'w1.label_en';
		else
			_set_column := 'w1.label';
		end if;

		return query execute
		'
		with
		w1 as	(
				select 
						ctv.*,
						(target_data.fn_etl_check_target_variable($1,ctv.id)) as res
				from 
						target_data.c_target_variable as ctv order by ctv.id
				)
		,w2 as	(
				select
						w1.id,
						'|| _set_column ||' as label,
						(w1.res).id_etl_target_variable,
						(w1.res).check_target_variable,
						(w1.res).refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select
						w2.*,
						target_data.fn_etl_get_target_variable_metadata(w2.id,$2) as metadata
				from
						w2 where '|| _cond ||'
				)
		select
				w3.id,
				w3.label,
				w3.id_etl_target_variable,
				w3.check_target_variable,
				w3.refyearset2panel_mapping,
				w3.metadata
		from
				w3 order by w3.id;
		'
		using _export_connection, _national_language;

end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_target_variable(integer, character varying, boolean) IS
'The function returs target variables and informations for ETL. If input argument _etl = TRUE then function
returns target variables that was ETL and all theirs datas ar current. If input argument _etl = FALSE
then function retursn target variables that was not ETL yet or returns target variables that was ETL
and exists newer datas for ETL. If input argument _etl IS NULL (_etl = true or false) then function
returns both cases.';

grant execute on function target_data.fn_etl_get_target_variable(integer, character varying, boolean) to public;
-- </function>


