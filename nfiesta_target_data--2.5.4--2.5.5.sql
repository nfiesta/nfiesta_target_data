--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_check_cm_ldsity2target2categorization_setup" schema="target_data" src="functions/fn_check_cm_ldsity2target2categorization_setup.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_check_cm_ldsity2target2categorization_setup
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_cm_ldsity2target2categorization_setup();

create or replace function target_data.fn_check_cm_ldsity2target2categorization_setup() returns trigger as $src$
declare
	_trans_id					integer[];
	_ldsity2target_variable		integer[];
	_target_variable			integer;
	_target_variable_label_en	varchar;
	_string_1					text;
	_string_2					text;
	_string_3					text;
	_string_4					text;
	_string_5					text;
	_string_6					text;
	_string4bw1					text;
	_string4bw2					text;
	_query						text;
	_res						integer;
begin
		if (TG_OP = 'INSERT')
		then
			if (TG_TABLE_NAME = 'cm_ldsity2target2categorization_setup')
			then
				-----------------------------------------------------
				select array_agg(id order by id) from trans_table
				into _trans_id;

				select array_agg(t.ldsity2target_variable order by t.ldsity2target_variable)
				from (select distinct ldsity2target_variable from trans_table) as t
				into _ldsity2target_variable;
		
				select target_variable from target_data.cm_ldsity2target_variable
				where id in (select unnest(_ldsity2target_variable))
				into _target_variable;
			
				select label_en from target_data.c_target_variable where id = _target_variable
				into _target_variable_label_en;
				-----------------------------------------------------
			
				for i in 1..array_length(_ldsity2target_variable,1)
				loop
					if i = 1
					then
						_string_1 := concat('
						with
						w',_ldsity2target_variable[i],' as	(
								select
										categorization_setup,
										ldsity2target_variable as ldsity2target_variable_',_ldsity2target_variable[i],',
										case when adc2classification_rule is null then array[0] else adc2classification_rule end as adc2classification_rule_',_ldsity2target_variable[i],',
										case when spc2classification_rule is null then array[0] else spc2classification_rule end as spc2classification_rule_',_ldsity2target_variable[i],'
								from
										#table_name#
								where
										#cond_trans_id#
								and
										ldsity2target_variable = ',_ldsity2target_variable[i],'
								)
						');
					
						_string_2 := concat('select w',_ldsity2target_variable[i],'.*');
						_string_3 := concat(' from w',_ldsity2target_variable[i]);
						_string_4 := concat(' on bw1.ldsity2target_variable_',_ldsity2target_variable[i],' = bw2.ldsity2target_variable_',_ldsity2target_variable[i]);
						_string_5 := concat(' and (target_data.fn_array_compare(bw1.adc2classification_rule_',_ldsity2target_variable[i],',bw2.adc2classification_rule_',_ldsity2target_variable[i],'))');
						_string_6 := concat(' and (target_data.fn_array_compare(bw1.spc2classification_rule_',_ldsity2target_variable[i],',bw2.spc2classification_rule_',_ldsity2target_variable[i],'))');
					else
						_string_1 := concat(_string_1,'
						,w',_ldsity2target_variable[i],' as	(
								select
										categorization_setup,
										ldsity2target_variable as ldsity2target_variable_',_ldsity2target_variable[i],',
										case when adc2classification_rule is null then array[0] else adc2classification_rule end as adc2classification_rule_',_ldsity2target_variable[i],',
										case when spc2classification_rule is null then array[0] else spc2classification_rule end as spc2classification_rule_',_ldsity2target_variable[i],'
								from
										#table_name#
								where
										#cond_trans_id#
								and
										ldsity2target_variable = ',_ldsity2target_variable[i],'
								)
						');
					
						_string_2 := concat(_string_2,',w',_ldsity2target_variable[i],'.*');
						_string_3 := concat(_string_3,' inner join w',_ldsity2target_variable[i],' on w',_ldsity2target_variable[i] - 1,'.categorization_setup = w',_ldsity2target_variable[i],'.categorization_setup');
						_string_4 := concat(_string_4,' and bw1.ldsity2target_variable_',_ldsity2target_variable[i],' = bw2.ldsity2target_variable_',_ldsity2target_variable[i]);
						_string_5 := concat(_string_5,' and (target_data.fn_array_compare(bw1.adc2classification_rule_',_ldsity2target_variable[i],',bw2.adc2classification_rule_',_ldsity2target_variable[i],'))');
						_string_6 := concat(_string_6,' and (target_data.fn_array_compare(bw1.spc2classification_rule_',_ldsity2target_variable[i],',bw2.spc2classification_rule_',_ldsity2target_variable[i],'))');
					end if;
				end loop;
			
				_string4bw1 := replace(replace(_string_1 || _string_2 || _string_3,'#table_name#','target_data.cm_ldsity2target2categorization_setup'),'#cond_trans_id#','id not in (select unnest($1))');	-- data from cm_ldsity2target2categorization_setup without new inserted rows
				_string4bw2 := replace(replace(_string_1 || _string_2 || _string_3,'#table_name#','trans_table'),'#cond_trans_id#','TRUE');																	-- new inserted rows
			
				_query := concat( 
				'
				with
				 bw1 as (',_string4bw1,')
				,bw2 as (',_string4bw2,')
				,bw3 as	(
						select bw1.*, bw2.* from bw1 inner join bw2
						',_string_4,'
						',_string_5,'
						',_string_6,'		
						)
				select count(bw3.*) as pocet from bw3;
				'
				);
			
				execute ''||_query||'' using _trans_id into _res;
			
				if _res > 0
				then
					raise exception 'fn_check_cm_ldsity2target2categorization_setup: some of inserting rows violates the UNIQUENESS of classification rules for ldsity objects for target variable = "%"!',_target_variable_label_en;
				end if;		
			
			else
				raise exception 'fn_check_cm_ldsity2target2categorization_setup -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
			end if;
		else
			raise exception 'fn_check_cm_ldsity2target2categorization_setup -- trigger operation not known: %', TG_OP;
		end if;

	 	RETURN NULL;
end
$src$ LANGUAGE plpgsql;


drop trigger if exists trg__ldsity2target2categorization_setup__ins on target_data.cm_ldsity2target2categorization_setup;
create TRIGGER trg__ldsity2target2categorization_setup__ins
    after insert on target_data.cm_ldsity2target2categorization_setup
    referencing new table as trans_table
    for each statement execute function target_data.fn_check_cm_ldsity2target2categorization_setup();
-- </function>