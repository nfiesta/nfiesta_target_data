--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

----------------------
-- DDL
----------------------
UPDATE target_data.cm_ldsity2target_variable SET use_negative = false WHERE use_negative IS NULL;
ALTER TABLE target_data.cm_ldsity2target_variable ALTER COLUMN use_negative SET DEFAULT false;
ALTER TABLE target_data.cm_ldsity2target_variable ALTER COLUMN use_negative SET NOT NULL;

ALTER TABLE target_data.cm_adc2classification_rule ADD COLUMN use_negative boolean DEFAULT false;
ALTER TABLE target_data.cm_spc2classification_rule ADD COLUMN use_negative boolean DEFAULT false;
ALTER TABLE target_data.cm_adc2classification_rule ALTER COLUMN use_negative SET NOT NULL;
ALTER TABLE target_data.cm_spc2classification_rule ALTER COLUMN use_negative SET NOT NULL;


CREATE TABLE target_data.c_classification_type
(
  id			integer NOT NULL,
  label			character varying(200) NOT NULL,
  description		text NOT NULL
);

COMMENT ON TABLE target_data.c_classification_type IS 'Lookup table with classification type categories.';
COMMENT ON COLUMN target_data.c_classification_type.id IS 'Primary key.';
COMMENT ON COLUMN target_data.c_classification_type.label IS 'Label of the category.';
COMMENT ON COLUMN target_data.c_classification_type.description IS 'Description of the category.';

ALTER TABLE target_data.c_classification_type ADD CONSTRAINT pkey__c_classification_type_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_classification_type_id ON target_data.c_classification_type IS 'Primary key.';

INSERT INTO target_data.c_classification_type(id,label,description) VALUES
(100,'standard','Standard type. Calculation rules are connected to columns/data which are connected to the time snapshot in which they was collected/measured.'),
(200,'change or dynamic','Calculation rules are bonded with the time period between the two states. Hence the two variants of calculation rules are assumed: use_negative = true and use_negative = false. Each of them is then used for their local density contribution counterpart with equal use_negative value during the local density of change creation. The main goal is to classify the negative local density contribution (previous measurement) by the attributes which were assesed during the next measurement (e.g. reason of land use category change - forest succession).');

ALTER TABLE target_data.c_area_domain ADD COLUMN classification_type integer DEFAULT 100;
ALTER TABLE target_data.c_area_domain ALTER COLUMN classification_type SET NOT NULL;
 
ALTER TABLE target_data.c_area_domain
  ADD CONSTRAINT fkey__c_area_domain__c_classification_type FOREIGN KEY (classification_type)
      REFERENCES target_data.c_classification_type (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__c_area_domain__c_classification_type ON target_data.c_area_domain IS
'Foreign key to table c_classification_type.';

ALTER TABLE target_data.c_sub_population ADD COLUMN classification_type integer DEFAULT 100;
ALTER TABLE target_data.c_sub_population ALTER COLUMN classification_type SET NOT NULL;
 
ALTER TABLE target_data.c_sub_population
  ADD CONSTRAINT fkey__c_sub_population__c_classification_type FOREIGN KEY (classification_type)
      REFERENCES target_data.c_classification_type (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__c_sub_population__c_classification_type ON target_data.c_sub_population IS
'Foreign key to table c_classification_type.';



DROP FUNCTION target_data.fn_save_classification_rule(integer, integer, integer, text) CASCADE;
-- <function name="fn_save_classification_rule" schema="target_data" src="functions/fn_save_classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_classification_rule.sql
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_classification_rule(integer, integer, integer, text, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_classification_rule(_parent integer, _areal_or_population integer, _ldsity_object integer, _classification_rule text, _use_negative boolean DEFAULT false)
RETURNS integer
AS
$$
DECLARE
	_id integer;
BEGIN
	IF _parent IS NULL OR _ldsity_object IS NULL OR _classification_rule IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of parent indicator (%) or ldsity object (%) or classification rule (%) is null!', _parent, _ldsity_object, _classification_rule;
	END IF; 

	CASE 
	WHEN _areal_or_population = 100 THEN
		INSERT INTO target_data.cm_adc2classification_rule(area_domain_category, ldsity_object, classification_rule, use_negative)
		SELECT 
			_parent,
			_ldsity_object, _classification_rule, _use_negative
		RETURNING id
		INTO _id;

	WHEN _areal_or_population = 200 THEN

		INSERT INTO target_data.cm_spc2classification_rule(sub_population_category, ldsity_object, classification_rule, use_negative)
		SELECT 
			_parent,
			_ldsity_object, _classification_rule, _use_negative
		RETURNING id
		INTO _id;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_classification_rule(integer, integer, integer, text, boolean) IS
'Functions inserts a record into table cm_adc2conversion_string or cm_spc2conversion_string based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_classification_rule(integer, integer, integer, text, boolean) TO public;

-- </function>

DROP FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], integer[], integer[][], integer[][]) CASCADE;
-- <function name="fn_save_categories_and_rules" schema="target_data" src="functions/fn_save_categories_and_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categories_and_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_categories_and_rules(integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], boolean[], integer[], integer[][], integer[][]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_categories_and_rules(
	_ldsity_object integer,						-- id from c_ldsity_object, for which object the classification rules are valid
	_areal_or_pop integer,						-- id from c_areal_or_population
	_classification_type integer,					-- id from c_classification_type
	_label varchar(200), 						-- label of the area domain/population
	_description text, 						-- description of the area domain/population
	_label_en varchar(200), 					-- english label of the area domain/population
	_description_en text,						-- english description of the area domain/population
	_cat_label varchar(200)[], 					-- label of the area domain/population categories
	_cat_description text[],					-- description of the area domain/population categories
	_cat_label_en varchar(200)[], 					-- english label of the area domain/population categories
	_cat_description_en text[],					-- english description of the area domain/population categories
	_rules text[], 							-- text representation of the classification rules which are used to classify an object
	_use_negative boolean[],					-- if there is standard classification_type, all values are false, if 
									-- change type present then each category has to have both rules variants (with use negative true and false)
									-- currently there has to be all values sorted and cannot be mixed (e.g. an array [false, true, false, true] is prohibited)
	_panel_refyearset integer[][], 					-- an array of panel and reference year set combinations (id from table cm_refyearset2panel_mapping)
									-- in which are classification rules applicable, the second dimension is because of use_negative variant, where
									-- there is a different set of combinations for true and false variants
	_adc integer[][] DEFAULT NULL::int[][], 			-- optional area domain categories contraint (only for areal), e.g. accessible land
	_spc integer[][] DEFAULT NULL::int[][]				-- dtto for populational, e.g. living stem for age classification rule, it is an array of arrays hence
									--  more categories from one population can fit the constraint for each rule, 
									-- e.g. for rule which classifies high intensity of damage a prerequisite can be two categories:
									-- damaged by bark stripping, damaged by human etc.
)
RETURNS TABLE (
	areal_or_pop			integer,
	cat_label			varchar(200),
	cat_label_en			varchar(200),
	parent_id			integer,
	category_id			integer,	
	classification_rule		integer,
	refyearset2panel		integer
) 
AS
$$
DECLARE
_total_false integer;
_total_true integer;
BEGIN
	IF array_length(_cat_label,1) != array_length(_cat_description,1) OR
		array_length(_cat_label_en,1) != array_length(_cat_description_en,1)
	THEN
		RAISE EXCEPTION 'Given arrays of label and description (%,%) or label_en and description_en (%,%) must be of same length!', _label, _description, _label_en, _description_en;
	END IF;

	WITH w AS (
		SELECT use_negative, id
		FROM unnest(_use_negative) WITH ORDINALITY AS t(use_negative,id)
	)
	SELECT count(*)::numeric AS total
	FROM w
	WHERE use_negative = false
	INTO _total_false;
	
	WITH w AS (
		SELECT use_negative, id
		FROM unnest(_use_negative) WITH ORDINALITY AS t(use_negative,id)
	)
	SELECT count(*)::numeric AS total
	FROM w
	WHERE use_negative = true
	INTO _total_true;

	IF _classification_type = 100 THEN
		IF array_length(_rules,1) != array_length(_cat_label,1)
		THEN
			RAISE EXCEPTION 'Given arrays of rules (%) must be the same length as array of labels (%)!', _rules, _cat_label;
		END IF;

		IF _total_true > 0
		THEN 
			RAISE EXCEPTION 'If calculation type = 100 then no use_negative = true classification rules can be passed.';
		END IF;
	ELSE
		IF array_length(_rules,1) != (array_length(_cat_label,1) * 2.0) -- each category has 2 rules (use_negative true and false)
		THEN
			RAISE EXCEPTION 'Given arrays of rules (%) must have twice the length of labels array (%)!', _rules, _cat_label;
		END IF;

		IF _total_false != _total_true
		THEN 
			RAISE EXCEPTION 'Both variants of use_negative classification rules (false %, true %) has to have the same totals.', _total_false, _total_true;
		END IF;

		IF EXISTS (
			WITH w AS (
				SELECT 
					use_negative, id, ceil( round((id/(count(*) OVER())::numeric * 2),12) ) AS array_id
				FROM unnest(_use_negative) WITH ORDINALITY AS t1(use_negative, id)
			), w_test AS (
				SELECT array_id, count(DISTINCT use_negative) AS test
				FROM w
				GROUP BY array_id
			) SELECT *
			FROM w_test
			WHERE test != 1)
		THEN 
			RAISE EXCEPTION 'Given array of use_negative values must be ordered. First use_negative true or false and then the other one.';
		END IF;

	END IF;

	RETURN QUERY
	WITH w_parent AS (
		SELECT t1 AS id FROM target_data.fn_save_areal_or_pop(_areal_or_pop, _label, _description, _label_en, _description_en, _classification_type) AS t1
	), w_categories AS (
		SELECT 	t1.id AS parent_id, -- t1.label, t1.label_en,
			t2.id AS array_id,
			t6 AS category_id, t2.label AS label_cat, t4.label_en AS label_en_cat
		FROM 	w_parent AS t1,
			unnest(_cat_label) WITH ORDINALITY AS t2(label, id)
		LEFT JOIN unnest(_cat_description) WITH ORDINALITY AS t3(description, id) ON t2.id = t3.id
		LEFT JOIN unnest(_cat_label_en) WITH ORDINALITY AS t4(label_en, id) ON t3.id = t4.id
		LEFT JOIN unnest(_cat_description_en) WITH ORDINALITY AS t5(description_en, id) ON t4.id = t5.id,
			target_data.fn_save_category(_areal_or_pop, t2.label, t3.description, t4.label_en, t5.description_en, t1.id) AS t6
	), w_rules AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, t2.rule, t4 AS rule_id, t1.label_cat, t1.label_en_cat
		FROM 	w_categories AS t1
		INNER JOIN unnest(_rules) WITH ORDINALITY AS t2(rule,id) 
		ON t1.array_id = t2.id OR t1.array_id = (t2.id - array_length(_cat_label,1)) -- in case of use_negative rules, there will be array overflow (twice length)
		INNER JOIN unnest(_use_negative) WITH ORDINALITY AS t3(use_negative,id) 
		ON t1.array_id = t3.id OR t1.array_id = (t3.id - array_length(_cat_label,1)),
			target_data.fn_save_classification_rule(t1.category_id, _areal_or_pop, _ldsity_object, t2.rule, t3.use_negative) AS t4
	),
	w_rule_agg AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, t1.label_cat, t1.label_en_cat,
			array_agg(t1.rule_id ORDER BY array_id) AS rule_ids
		FROM w_rules AS t1
		GROUP BY t1.parent_id, t1.array_id, t1.category_id, t1.label_cat, t1.label_en_cat
	), w_hier_adc AS (
		INSERT INTO target_data.t_adc_hierarchy (variable_superior, variable, dependent)
		SELECT t2.adc, t1.category_id, true
		FROM	w_categories AS t1,
			(SELECT 
				adc, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_adc,1)),12) ) AS array_id
			FROM unnest(_adc) WITH ORDINALITY AS t1(adc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id AND t2.adc IS NOT NULL
	), w_hier_spc AS (
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable, dependent)
		SELECT t2.spc, t1.category_id, true
		FROM	w_categories AS t1,
			(SELECT 
				spc, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_spc,1)),12) ) AS array_id
			FROM unnest(_spc) WITH ORDINALITY AS t1(spc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id AND t2.spc IS NOT NULL
	)
	SELECT  _areal_or_pop, t1.label_cat, t1.label_en_cat,
		 t1.parent_id, t1.category_id, t2.classification_rule, t2.refyearset2panel
	FROM 
		w_rule_agg AS t1,
		target_data.fn_save_class_rule_mapping(_areal_or_pop, t1.rule_ids, _use_negative, _panel_refyearset) AS t2
	;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_categories_and_rules(integer, integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], boolean[], integer[], integer[][], integer[][]) IS
'Function inserts all necessary data into lookups/tables with area_domain/sub_population categories and its classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_categories_and_rules(integer, integer, integer, varchar(200), text, varchar(200), text, varchar(200)[], text[], varchar(200)[], text[], text[], boolean[], integer[], integer[][], integer[][]) TO public;

-- </function>

DROP FUNCTION target_data.fn_save_rules(integer, integer, integer[], text[], integer[], integer[][], integer[][]) CASCADE;
-- <function name="fn_save_rules" schema="target_data" src="functions/fn_save_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_rules(integer, integer, integer, integer[], text[], boolean[], integer[], integer[][], integer[][]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_rules(
	_ldsity_object integer,						-- id from c_ldsity_object, for which object the classification rules are valid
	_areal_or_pop integer,						-- id from c_areal_or_population
	_classification_type integer,					-- id from c_classification_type
	_category_ids integer[],					-- ids from c_area_domain_category or c_sub_population_category 
	_rules text[], 							-- text representation of the classification rules which are used to classify an object
	_use_negative boolean[],					-- if there is standard classification_type, all values are false, if 
									-- change type present then each category has to have both rules variants (with use negative true and false)
	_panel_refyearset integer[][], 					-- an array of panel and reference year set combinations (id from table cm_refyearset2panel_mapping)
									-- in which are classification rules applicable, the second dimension is because of use_negative variant, where
									-- there is a different set of combinations for true and false variants
	_adc integer[][] DEFAULT NULL::int[][], 			-- optional area domain categories contraint (only for areal), e.g. accessible land
	_spc integer[][] DEFAULT NULL::int[][]				-- dtto for populational, e.g. living stem for age classification rule, it is an array of arrays hence
									--  more categories from one population can fit the constraint for each rule, 
									-- e.g. for rule which classifies high intensity of damage a prerequisite can be two categories:
									-- damaged by bark stripping, damaged by human etc.
)
RETURNS TABLE (
	areal_or_pop			integer,
	parent_id			integer,
	category_id			integer,
	classification_rule		integer,
	refyearset2panel		integer
) 
AS
$$
DECLARE
BEGIN
	IF array_length(_rules,1) != array_length(_category_ids,1)
	THEN
		RAISE EXCEPTION 'Given arrays of rules (%) must be the same length as array of category ids (%)!', _rules, _category_ids;
	END IF;

	RETURN QUERY
	WITH w_categories AS (
		SELECT 
			t1.id AS array_id,
			t1.category_id,
			CASE WHEN _areal_or_pop = 100 THEN t2.area_domain
			WHEN _areal_or_pop = 200 THEN t3.sub_population
			END AS parent_id
		FROM
			unnest(_category_ids) WITH ORDINALITY AS t1(category_id, id)
		LEFT JOIN target_data.c_area_domain_category AS t2
		ON t2.id = t1.category_id AND _areal_or_pop = 100
		LEFT JOIN target_data.c_sub_population_category AS t3
		ON t3.id = t1.category_id AND _areal_or_pop = 200

	), w_rules AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, t2.rule, t4 AS rule_id
		FROM 	w_categories AS t1
		INNER JOIN unnest(_rules) WITH ORDINALITY AS t2(rule,id) 
		ON t1.array_id = t2.id OR t1.array_id = (t2.id - array_length(_category_ids,1)) -- in case of use_negative rules, there will be array overflow (twice length)
		INNER JOIN unnest(_use_negative) WITH ORDINALITY AS t3(use_negative,id) 
		ON t1.array_id = t3.id OR t1.array_id = (t3.id - array_length(_category_ids,1)),
			target_data.fn_save_classification_rule(t1.category_id, _areal_or_pop, _ldsity_object, t2.rule, t3.use_negative) AS t4
	),
	w_rule_agg AS (
		SELECT t1.parent_id, t1.array_id, t1.category_id, array_agg(t1.rule_id ORDER BY array_id) AS rule_ids
		FROM w_rules AS t1
		GROUP BY t1.parent_id, t1.array_id, t1.category_id
	), w_hier_adc AS (
		INSERT INTO target_data.t_adc_hierarchy (variable_superior, variable, dependent)
		SELECT t2.adc, t1.category_id, true
		FROM	w_categories AS t1,
			(SELECT 
				adc, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_adc,1)),12) ) AS array_id
			FROM unnest(_adc) WITH ORDINALITY AS t1(adc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id AND t2.adc IS NOT NULL
	), w_hier_spc AS (
		INSERT INTO target_data.t_spc_hierarchy (variable_superior, variable, dependent)
		SELECT t2.spc, t1.category_id, true
		FROM	w_categories AS t1,
			(SELECT 
				spc, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_spc,1)),12) ) AS array_id
			FROM unnest(_spc) WITH ORDINALITY AS t1(spc, id)
			) AS t2
		WHERE t1.array_id = t2.array_id AND t2.spc IS NOT NULL
	)
	SELECT  _areal_or_pop, t1.parent_id, t1.category_id, t2.classification_rule, t2.refyearset2panel
	FROM 
		w_rule_agg AS t1,
		target_data.fn_save_class_rule_mapping(_areal_or_pop, t1.rule_ids, _use_negative, _panel_refyearset) AS t2
	;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_rules(integer, integer, integer, integer[], text[], boolean[], integer[], integer[][], integer[][]) IS
'Function inserts all necessary data into lookups/tables with area_domain/sub_population categories and its classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_rules(integer, integer, integer, integer[], text[], boolean[], integer[], integer[][], integer[][]) TO public;

-- </function>

DROP FUNCTION target_data.fn_save_class_rule_mapping(integer, integer[], integer[]) CASCADE;
-- <function name="fn_save_class_rule_mapping" schema="target_data" src="functions/fn_save_class_rule_mapping.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_class_rule_mapping
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_class_rule_mapping(integer, integer[], boolean[], integer[][]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_class_rule_mapping(_areal_or_pop integer, _rules integer[], _use_negative boolean[], _panel_refyearset integer[][])
--boolean
RETURNS TABLE (
	classification_rule		integer,
	refyearset2panel		integer
) 
AS
$$
DECLARE
	_use_negative_count integer;
BEGIN
	_use_negative_count := (SELECT count(DISTINCT val) FROM unnest(_use_negative) AS t(val));

	CASE
	WHEN _areal_or_pop = 100
	THEN
		PERFORM pg_catalog.setval('target_data.cm_adc2classrule2panel_refyearset_id_seq', (SELECT max(id) FROM target_data.cm_adc2classrule2panel_refyearset), true); 
		RETURN QUERY

		WITH w_rules AS (
			SELECT
				t1.rule, t1.id, t2.use_negative, t2.array_id
			FROM
				unnest(_rules) WITH ORDINALITY AS t1(rule, id)
			INNER JOIN
				(SELECT 
					use_negative, id, ceil( round((id/(count(*) OVER())::numeric * _use_negative_count),12) ) AS array_id
				FROM unnest(_use_negative) WITH ORDINALITY AS t1(use_negative, id)
				) AS t2
			ON t1.id = t2.id
			)
		INSERT INTO target_data.cm_adc2classrule2panel_refyearset (adc2classification_rule, refyearset2panel)
		SELECT
			t1.rule, t2.panel_refyearset
		FROM
			w_rules AS t1
		INNER JOIN
			(SELECT 
				panel_refyearset, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_panel_refyearset,1)),12) ) AS array_id
			FROM unnest(_panel_refyearset) WITH ORDINALITY AS t1(panel_refyearset, id)
			) AS t2
		ON t1.array_id = t2.array_id
		RETURNING cm_adc2classrule2panel_refyearset.adc2classification_rule, cm_adc2classrule2panel_refyearset.refyearset2panel;

	WHEN _areal_or_pop = 200
	THEN
		PERFORM pg_catalog.setval('target_data.cm_spc2classrule2panel_refyearset_id_seq', (SELECT max(id) FROM target_data.cm_spc2classrule2panel_refyearset), true); 
		RETURN QUERY
		WITH w_rules AS (
			SELECT
				t1.rule, t1.id, t2.use_negative, t2.array_id
			FROM
				unnest(_rules) WITH ORDINALITY AS t1(rule, id)
			INNER JOIN
				(SELECT 
					use_negative, id, ceil( round((id/(count(*) OVER())::numeric * _use_negative_count),12) ) AS array_id
				FROM unnest(_use_negative) WITH ORDINALITY AS t1(use_negative, id)
				) AS t2
			ON t1.id = t2.id
			)
		INSERT INTO target_data.cm_spc2classrule2panel_refyearset (spc2classification_rule, refyearset2panel)
		SELECT
			t1.rule, t2.panel_refyearset
		FROM
			w_rules AS t1
		INNER JOIN
			(SELECT 
				panel_refyearset, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_panel_refyearset,1)),12) ) AS array_id
			FROM unnest(_panel_refyearset) WITH ORDINALITY AS t1(panel_refyearset, id)
			) AS t2
		ON t1.array_id = t2.array_id
		RETURNING cm_spc2classrule2panel_refyearset.spc2classification_rule, cm_spc2classrule2panel_refyearset.refyearset2panel;
	ELSE
		RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_class_rule_mapping(integer, integer[], boolean[], integer[][]) IS
'Function checks syntax in text field with classification rules.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_class_rule_mapping(integer, integer[], boolean[], integer[][]) TO public;

-- </function>

DROP FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer) CASCADE;
-- <function name="fn_get_ldsity_object4ld_object_adsp" schema="target_data" src="functions/fn_get_ldsity_object4ld_object_adsp.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--------------------------------------------------------------------------------
-- fn_get_ldsity_object4ld_object_adsp
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(
	_ldsity_object integer, 		-- ldsity object which carries the local density
	_areal_or_population integer, 		-- identificator from c_areal_or_population
	_id integer,				-- id from table c_area_domain or c_sub_population according to areal_or_population id
	_use_negative boolean DEFAULT FALSE	-- boolean value if the local density should consider negative variant of the rule
	)
RETURNS TABLE (
id			integer,			-- ldsity object used for classification, id from table c_ldsity_object
label			character varying(200),		-- label of returned ldsity object
description		text,				-- description dtto
label_en		character varying(200),		-- label_en dtto
description_en		text,				-- description_en dtto
table_name		character varying(200),		-- table of returned ldsity object used for classification
classification_rule	boolean,			-- identification if returned ldsity object has classification rule defined
use_negative		boolean				-- use_negative parameter for returned object
)
AS
$$
BEGIN
	IF _ldsity_object IS NULL
	THEN
		RAISE EXCEPTION 'Parameter _ldsity_object is mandatory and must not be null!';
	END IF;

	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
	THEN
		RAISE EXCEPTION 'Given ldsity object (%) does not exist in table c_ldsity_object!', _ldsity_object;
	END IF;

	CASE
	WHEN _areal_or_population = 100
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
		THEN
			RAISE EXCEPTION 'Given area domain (%) does not exist in table c_area_domain!', _id;
		END IF;

		RETURN QUERY
		WITH w AS (
			SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
		)
		SELECT 	t3.id, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, 
			CASE WHEN t4.ldsity_object IS NOT NULL THEN true ELSE false END AS rule,
			t4.use_negative
		FROM w AS t1,
			unnest(t1.ldsity_objects) AS t2(ldsity_object)
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		LEFT JOIN 
			(SELECT t1.area_domain, t2.ldsity_object, t2.use_negative, count(*) AS total
			FROM target_data.c_area_domain_category AS t1
			INNER JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.id = t2.area_domain_category
			WHERE t1.area_domain = _id AND t2.use_negative = _use_negative
			GROUP BY t1.area_domain, t2.ldsity_object, t2.use_negative
			) AS t4
		ON t3.id = t4.ldsity_object
		WHERE t3.areal_or_population = 100;

	WHEN _areal_or_population = 200
	THEN
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _id)
		THEN
			RAISE EXCEPTION 'Given population (%) does not exist in table c_sub_population!', _id;
		END IF;

		RETURN QUERY
		WITH w AS (
			SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
		)
		SELECT 	t3.id, t3.label, t3.description, t3.label_en, t3.description_en, t3.table_name, 
			CASE WHEN t4.ldsity_object IS NOT NULL THEN true ELSE false END AS rule,
			t4.use_negative
		FROM w AS t1,
			unnest(t1.ldsity_objects) AS t2(ldsity_object)
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		LEFT JOIN 
			(SELECT t1.sub_population, t2.ldsity_object, t2.use_negative, count(*) AS total
			FROM target_data.c_sub_population_category AS t1
			INNER JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.sub_population = _id AND t2.use_negative = _use_negative
			GROUP BY t1.sub_population, t2.ldsity_object, t2.use_negative
			) AS t4
		ON t3.id = t4.ldsity_object
		WHERE t3.areal_or_population = 200;
	ELSE
		RAISE EXCEPTION 'Not known value of areal or population (%)!', _areal_or_population;
	END CASE;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer, boolean) IS
'Function returns records from c_ldsity_object table for given area domain or population.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_ldsity_object4ld_object_adsp(integer, integer, integer, boolean) TO public;

-- </function>

DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_objects4target_variable(integer) CASCADE;
-- <function name="fn_get_ldsity_objects4target_variable" schema="target_data" src="functions/fn_get_ldsity_objects4target_variable.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_ldsity_objects4target_variable(integer) CASCADE;

create or replace function target_data.fn_get_ldsity_objects4target_variable(_target_variable integer)
returns table
(
	id_cm_ldsity2target_variable	integer,
	ldsity_object			integer,
	use_negative			boolean,
	ldsity_object_type		integer
)
AS
$$
declare
		_check_exist_id		integer[];
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_get_ldsity_objects4target_variable: The input argument _target_variable must not be NULL !';
		end if;	
	
		select array_agg(id order by id) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable
		into _check_exist_id;

		if _check_exist_id is null
		then
			raise exception 'Error 02: fn_get_ldsity_objects4target_variable: For input argument _target_variable = % not exist any record in cm_ldsity2target_variable table!',_target_variable;
		end if;

		RETURN QUERY	
		SELECT
				a.id as id_cm_ldsity2target_variable,
				b.ldsity_object,
				a.use_negative,
				a.ldsity_object_type
		FROM
				(select * from target_data.cm_ldsity2target_variable where id in (select unnest(_check_exist_id))) as a
		INNER JOIN
				target_data.c_ldsity as b on a.ldsity = b.id
		ORDER
				BY a.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_ldsity_objects4target_variable(integer) IS
'The function for the specified target variable returns a list of ldsity objects.';

grant execute on function target_data.fn_get_ldsity_objects4target_variable(integer) TO public;


-- </function>

-- <function name="fn_trg_check_c_area_domain" schema="target_data" src="functions/fn_trg_check_c_area_domain.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_c_area_domain
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_c_area_domain();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_c_area_domain() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF	(
			SELECT count(*) FROM target_data.c_area_domain_category
			WHERE area_domain = NEW.id
			) = 0
		THEN
			RAISE EXCEPTION 'For newly inserted area domain (area_domain = %), not exists any record in table c_area_domain_category!', NEW.id;
		END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_c_area_domain() IS
'This trigger function checks wheter for newly inserted record in c_area_domain table exists at least one record in c_area_domain_category table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_c_area_domain() TO public;


-- </function>

-- <function name="fn_trg_check_c_area_domain_category" schema="target_data" src="functions/fn_trg_check_c_area_domain_category.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_c_area_domain
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_c_area_domain_category();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_c_area_domain_category() 
RETURNS TRIGGER AS 
$$
DECLARE
	_class_type 	integer;
	_total		integer;
	_total_negat	integer;
BEGIN
		_class_type :=  (SELECT classification_type FROM target_data.c_area_domain WHERE id = NEW.area_domain);

		CASE WHEN _class_type = 100
		THEN
		
			SELECT count(*) 
			FROM target_data.cm_adc2classification_rule
			WHERE area_domain_category = NEW.id AND use_negative = false
			INTO _total;

			SELECT count(*) 
			FROM target_data.cm_adc2classification_rule
			WHERE area_domain_category = NEW.id AND use_negative = true
			INTO _total_negat;

			IF _total = 0
			THEN
				RAISE EXCEPTION 'For newly inserted area domain category (area_domain = %), not exists any record in table cm_adc2classification_rule!', NEW.area_domain;
			END IF;

			IF _total != 0 AND _total_negat != 0
			THEN
				RAISE EXCEPTION 'For newly inserted area domain category (area_domain = %) of standard classification type (%), there can''t be any classification rule with use_negative true!', NEW.area_domain, _class_type;
			END IF;

		WHEN _class_type = 200
		THEN
			SELECT count(*) 
			FROM target_data.cm_adc2classification_rule
			WHERE area_domain_category = NEW.id AND use_negative = false
			INTO _total;

			SELECT count(*) 
			FROM target_data.cm_adc2classification_rule
			WHERE area_domain_category = NEW.id AND use_negative = true
			INTO _total_negat;

			IF _total = 0
			THEN
				RAISE EXCEPTION 'For newly inserted area domain category (area_domain = %), not exists any record in table cm_adc2classification_rule!', NEW.area_domain;
			END IF;

			IF _total != _total_negat
			THEN
				RAISE EXCEPTION 'For newly inserted area domain category (area_domain = %) of change classification type (%), there has to be the same number of classification rules with use negative false and true!', NEW.area_domain, _class_type;
			END IF;

		ELSE

		END CASE;
	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_c_area_domain_category() IS
'This trigger function checks wheter for newly inserted record in c_area_domain_category table exists at least one record in cm_adc2classification_rule table. According to classification type there is also a check on the same number of classification rules with each use_negative variant.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_c_area_domain_category() TO public;


-- </function>

-- <function name="fn_trg_check_c_sub_population" schema="target_data" src="functions/fn_trg_check_c_sub_population.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_c_sub_population
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_c_sub_population();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_c_sub_population() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF	(
			SELECT count(*) FROM target_data.c_sub_population_category
			WHERE sub_population = NEW.id
			) = 0
		THEN
			RAISE EXCEPTION 'For newly inserted sub population (sub_population = %), not exists any record in table c_sub_population_category!', NEW.id;
		END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_c_sub_population() IS
'This trigger function checks wheter for newly inserted record in c_sub_population table exists at least one record in c_sub_population_category table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_c_sub_population() TO public;


-- </function>

-- <function name="fn_trg_check_c_sub_population_category" schema="target_data" src="functions/fn_trg_check_c_sub_population_category.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_c_sub_population
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_c_sub_population_category();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_c_sub_population_category() 
RETURNS TRIGGER AS 
$$
DECLARE
	_class_type 	integer;
	_total		integer;
	_total_negat	integer;
BEGIN
		_class_type :=  (SELECT classification_type FROM target_data.c_sub_population WHERE id = NEW.sub_population);

		CASE WHEN _class_type = 100
		THEN
		
			SELECT count(*) 
			FROM target_data.cm_spc2classification_rule
			WHERE sub_population_category = NEW.id AND use_negative = false
			INTO _total;

			SELECT count(*) 
			FROM target_data.cm_spc2classification_rule
			WHERE sub_population_category = NEW.id AND use_negative = true
			INTO _total_negat;

			IF _total = 0
			THEN
				RAISE EXCEPTION 'For newly inserted area domain category (sub_population = %), not exists any record in table cm_spc2classification_rule!', NEW.sub_population;
			END IF;

			IF _total != 0 AND _total_negat != 0
			THEN
				RAISE EXCEPTION 'For newly inserted area domain category (sub_population = %) of standard classification type (%), there can''t be any classification rule with use_negative true!', NEW.sub_population, _class_type;
			END IF;

		WHEN _class_type = 200
		THEN
			SELECT count(*) 
			FROM target_data.cm_spc2classification_rule
			WHERE sub_population_category = NEW.id AND use_negative = false
			INTO _total;

			SELECT count(*) 
			FROM target_data.cm_spc2classification_rule
			WHERE sub_population_category = NEW.id AND use_negative = true
			INTO _total_negat;

			IF _total = 0
			THEN
				RAISE EXCEPTION 'For newly inserted area domain category (sub_population = %), not exists any record in table cm_spc2classification_rule!', NEW.sub_population;
			END IF;

			IF _total != _total_negat
			THEN
				RAISE EXCEPTION 'For newly inserted area domain category (sub_population = %) of change classification type (%), there has to be the same number of classification rules with use negative false and true!', NEW.sub_population, _class_type;
			END IF;

		ELSE

		END CASE;
	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_c_sub_population_category() IS
'This trigger function checks wheter for newly inserted record in c_sub_population_category table exists at least one record in cm_spc2classification_rule table. According to classification type there is also a check on the same number of classification rules with each use_negative variant.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_c_sub_population_category() TO public;


-- </function>

-- <function name="fn_check_classification_rule" schema="target_data" src="functions/fn_check_classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rule(
	_ldsity integer,						-- id from table c_ldsity, ldsity contribution for which the rule should be tested
	_ldsity_object integer,						-- id from c_ldsity_object, for which object the classification rule are valid (must be within hierarchy of ldsity contribution)
	_rule text, 							-- text representation of the classification rule which is used to classify an object
	_panel_refyearset integer DEFAULT NULL::int,			-- panel and reference year set combination (id from table cm_refyearset2panel_mapping)
									-- in which are classification rules applicable
	_adc integer[] DEFAULT NULL::int[], 				-- optional area domain categories contraint (only for areal), e.g. accessible land
	_spc integer[] DEFAULT NULL::int[]				-- dtto for populational, e.g. living stem for age classification rule, it is an array of arrays hence
									--  more categories from one population can fit the constraint for each rule, 
									-- e.g. for rule which classifies high intensity of damage a prerequisite can be two categories:
									-- damaged by bark stripping, damaged by human etc.
)
RETURNS TABLE (
	compliance_status	boolean,
	no_of_objects		integer
) 
AS
$$
DECLARE
_ldsity_objects		integer[];
_adc_rule_test		integer[];
_spc_rule_test		integer[];
_table_name4rule	varchar;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
_exist_test		boolean;
_qall			text;
i 			integer;
_adc4loop2d		integer[][];
_spc4loop2d		integer[][];
_adc4loop		integer[];
_spc4loop		integer[];
adcv			integer;
spcv			integer;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
	INTO _test;

	IF _test = false
	THEN
		RAISE EXCEPTION 'Given rule has an invalid syntax.';
	END IF;

	IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
	THEN
		_exist_test := true; _rule = true;
	ELSE 	_exist_test := false;
	END IF;
	
	_case := '('||_rule||')';

-- test all adc/spc has to come from one area domain/sub population

	IF (SELECT count(DISTINCT t2.area_domain)
		FROM unnest(_adc) AS t1(adc),
			target_data.c_area_domain_category AS t2
		WHERE t1.adc = t2.id) > 1
	THEN 
		RAISE EXCEPTION 'Given array of area domain categories does not come from one area domain.';
	END IF;
	
	IF (SELECT count(DISTINCT t2.sub_population)
		FROM unnest(_spc) AS t1(spc),
			target_data.c_sub_population_category AS t2
		WHERE t1.spc = t2.id) > 1
	THEN 
		RAISE EXCEPTION 'Given array of sub population categories does not come from one sub population.';
	END IF;


	_ldsity_objects := (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]));

-- get id from cm_adc2classification_rule id for each adc
	WITH RECURSIVE w_cm_ids AS (
		SELECT
				t2.id AS object_id, t1.id, t1.area_domain_category, t3.array_id, t3.id AS ordinality
			FROM
				target_data.cm_adc2classification_rule AS t1
			INNER JOIN
				unnest(_ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
			ON
			   t1.ldsity_object = t2.ldsity_object
			RIGHT JOIN
				(
				SELECT  adc, id, ceil(id/(count(*) OVER())::numeric * array_length(_adc,1)) AS array_id
				FROM  unnest(_adc) WITH ORDINALITY AS t1(adc, id)
				) AS t3
			ON
				t1.area_domain_category = t3.adc
			ORDER BY t2.id DESC, t3.array_id ASC
			-- because some rule can be met on more than one object, the rules from the object which is hierarchically higher will be picked
			LIMIT array_length(_adc,1) * array_length(_adc,2)
			),
	w_hier AS (
		SELECT 	t2.id AS cat_id, t2.ordinality, t2.array_id, 
			t2.id AS cm_id, t3.id AS cm_id_sup,
			t1.variable_superior, 2 AS id_sup, t1.variable, 1 AS id,
 			CASE WHEN t3.id IS NOT NULL THEN array[t1.variable_superior,t1.variable] ELSE array[t1.variable] END AS array_variable,
			CASE WHEN t3.id IS NOT NULL THEN array[t3.id, t2.id] ELSE array[t2.id] END AS cm_id_array_variable
		FROM target_data.t_adc_hierarchy AS t1
		RIGHT JOIN
			w_cm_ids AS t2
		ON t1.variable = t2.area_domain_category
		LEFT JOIN
			target_data.cm_adc2classification_rule AS t3
		ON t1.variable_superior = t3.area_domain_category
		LEFT JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		UNION ALL
		SELECT 	t2.cat_id, t2.ordinality, t2.array_id, 
			t2.cm_id, t2.cm_id_sup,
			t1.variable_superior, id_sup + 1, t1.variable, t2.id + 1, 
			array_prepend(t1.variable_superior, array_variable),
			array_prepend(t3.id, cm_id_array_variable)
		FROM target_data.t_adc_hierarchy AS t1
		INNER JOIN w_hier AS t2
		ON t1.variable = t2.variable_superior
		INNER JOIN target_data.cm_adc2classification_rule AS t3
		ON t1.variable_superior = t3.area_domain_category
		INNER JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
	), w_nulls AS (
		SELECT cat_id, ordinality, array_id, t1.array_variable[array_length(t1.array_variable,1)] AS cat, 
			array_variable, cm_id_array_variable,
			max(array_length(array_variable,1)) OVER () - array_length(array_variable,1) AS nulls2app,
			max(array_length(array_variable,1)) OVER () AS max_len
		FROM w_hier AS t1 
		LEFT JOIN target_data.t_adc_hierarchy AS t2 
		ON t1.variable_superior = t2.variable 
		WHERE t2.variable_superior IS NULL
	), w_append AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS var,
			cm_id_array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS cm_var,
			array_fill(NULL::int, ARRAY[max_len]) AS null_array, count(*) OVER(PARTITION BY cat) AS total
		FROM w_nulls
	), w_cat AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_agg(var) AS var_agg, 
			array_agg(cm_var) AS cm_var_agg, 
			null_array
		FROM w_append
		GROUP BY cat_id, cat, ordinality, array_id, null_array
	), w_cat_agg AS (
		SELECT cat_id, ordinality, array_id, cat, 
			var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS nulls2app,
			cm_var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS cm_nulls2app
		FROM w_cat
	), w_agg3d AS (
		SELECT
			array_id, 
			array_agg(nulls2app ORDER BY ordinality) AS cats,
			array_agg(cm_nulls2app ORDER BY ordinality) AS cm_ids
		FROM w_cat_agg
		GROUP BY array_id
	)
	SELECT 	--array_agg(cats ORDER BY array_id),
		array_agg(cm_ids ORDER BY array_id)
	FROM w_agg3d
	INTO _adc;

--raise notice '%', _ldsity_objects;
	WITH RECURSIVE w_cm_ids AS (
		SELECT
				t2.id AS object_id, t1.id, t1.sub_population_category, t3.array_id, t3.id AS ordinality
			FROM
				target_data.cm_spc2classification_rule AS t1
			INNER JOIN
				unnest(_ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
			ON
			   t1.ldsity_object = t2.ldsity_object
			RIGHT JOIN
				(
				--SELECT  spc, id, ceil(id/(count(*) OVER())::double precision * array_length(array[array[401,402],array[NULL::int,NULL::int]],1)) AS array_id
				--FROM  unnest(array[array[401,402],array[NULL::int,NULL::int]]) WITH ORDINALITY AS t1(spc, id)
				SELECT  spc, id, ceil(id/(count(*) OVER())::double precision * array_length(_spc,1)) AS array_id
				FROM  unnest(_spc) WITH ORDINALITY AS t1(spc, id)
				) AS t3
			ON
				t1.sub_population_category = t3.spc
			ORDER BY t2.id DESC, t3.array_id ASC
			-- because some rule can be met on more than one object, the rules from the object which is hierarchically higher will be picked
			--LIMIT array_length(array[array[401,402],array[NULL::int,NULL::int]],1) * array_length(array[array[401,402],array[NULL::int,NULL::int]],2)
			LIMIT array_length(_spc,1) * array_length(_spc,2)
			),
	w_hier AS (
		SELECT 	t2.id AS cat_id, t2.ordinality, t2.array_id, 
			t2.id AS cm_id, t3.id AS cm_id_sup,
			t1.variable_superior, 2 AS id_sup, t1.variable, 1 AS id,
  			CASE WHEN t3.id IS NOT NULL THEN array[t1.variable_superior,t1.variable] ELSE array[t1.variable] END AS array_variable,
			CASE WHEN t3.id IS NOT NULL THEN array[t3.id, t2.id] ELSE array[t2.id] END AS cm_id_array_variable
		FROM target_data.t_spc_hierarchy AS t1
		RIGHT JOIN
			w_cm_ids AS t2
		ON t1.variable = t2.sub_population_category
		LEFT JOIN
			target_data.cm_spc2classification_rule AS t3
		ON 	t1.variable_superior = t3.sub_population_category AND
			t3.classification_rule NOT IN ('EXISTS','NOT EXISTS')
		LEFT JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		UNION ALL
		SELECT 	t2.cat_id, t2.ordinality, t2.array_id, 
			t2.cm_id, t2.cm_id_sup,
			t1.variable_superior, id_sup + 1, t1.variable, t2.id + 1, 
			array_prepend(t1.variable_superior, array_variable),
			array_prepend(t3.id, cm_id_array_variable)
		FROM target_data.t_spc_hierarchy AS t1
		INNER JOIN w_hier AS t2
		ON t1.variable = t2.variable_superior
		INNER JOIN target_data.cm_spc2classification_rule AS t3
		ON t1.variable_superior = t3.sub_population_category
		INNER JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		WHERE t3.classification_rule NOT IN ('EXISTS','NOT EXISTS')
	), w_nulls AS (
		SELECT cat_id, ordinality, array_id, t1.array_variable[array_length(t1.array_variable,1)] AS cat, 
			array_variable, cm_id_array_variable,
			max(array_length(array_variable,1)) OVER () - array_length(array_variable,1) AS nulls2app,
			max(array_length(array_variable,1)) OVER () AS max_len
		FROM w_hier AS t1 
		LEFT JOIN target_data.t_spc_hierarchy AS t2 
		ON t1.variable_superior = t2.variable 
		WHERE t2.variable_superior IS NULL
	), w_append AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS var,
			cm_id_array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS cm_var,
			array_fill(NULL::int, ARRAY[max_len]) AS null_array, count(*) OVER(PARTITION BY cat) AS total
		FROM w_nulls
	), w_cat AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_agg(var) AS var_agg, 
			array_agg(cm_var) AS cm_var_agg, 
			null_array
		FROM w_append
		GROUP BY cat_id, cat, ordinality, array_id, null_array
	), w_cat_agg AS (
		SELECT cat_id, ordinality, array_id, cat, 
			var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS nulls2app,
			cm_var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS cm_nulls2app
		FROM w_cat
	), w_agg3d AS (
		SELECT
			array_id, 
			array_agg(nulls2app ORDER BY ordinality) AS cats,
			array_agg(cm_nulls2app ORDER BY ordinality) AS cm_ids
		FROM w_cat_agg
		GROUP BY array_id
	)
	SELECT 	--array_agg(cats ORDER BY array_id),
		array_agg(cm_ids ORDER BY array_id)
	FROM w_agg3d
	INTO _spc;
-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			(SELECT current_database()::information_schema.sql_identifier AS table_catalog,
				x.tblschema::information_schema.sql_identifier AS table_schema,
				x.tblname::information_schema.sql_identifier AS table_name,
				x.colname::information_schema.sql_identifier AS column_name,
				current_database()::information_schema.sql_identifier AS constraint_catalog,
				x.cstrschema::information_schema.sql_identifier AS constraint_schema,
				x.cstrname::information_schema.sql_identifier AS constraint_name
			FROM 
				(SELECT DISTINCT nr.nspname,
					r.relname,
					r.relowner,
					a.attname,
					nc.nspname,
					c.conname
				FROM pg_namespace nr,
					pg_class r,
					pg_attribute a,
					pg_depend d,
					pg_namespace nc,
					pg_constraint c
			 	WHERE 
					nr.oid = r.relnamespace AND r.oid = a.attrelid AND 
					d.refclassid = 'pg_class'::regclass::oid AND d.refobjid = r.oid AND 
					d.refobjsubid = a.attnum AND d.classid = 'pg_constraint'::regclass::oid AND 
					d.objid = c.oid AND c.connamespace = nc.oid AND c.contype = 'c'::"char" AND 
					(r.relkind = ANY (ARRAY['r'::"char", 'p'::"char"])) AND NOT a.attisdropped
				UNION ALL
				SELECT nr.nspname,
					r.relname,
					r.relowner,
					a.attname,
					nc.nspname,
					c.conname
			   	FROM pg_namespace nr,
					pg_class r,
					pg_attribute a,
					pg_namespace nc,
					pg_constraint c
				WHERE 
					nr.oid = r.relnamespace AND r.oid = a.attrelid AND nc.oid = c.connamespace AND r.oid =
					CASE c.contype
					WHEN 'f'::"char" THEN c.confrelid
					ELSE c.conrelid
					END AND (a.attnum = ANY (
						CASE c.contype
						WHEN 'f'::"char" THEN c.confkey
				    		ELSE c.conkey
						END)
					) AND NOT a.attisdropped AND (c.contype = ANY (ARRAY['p'::"char", 'u'::"char", 'f'::"char"])) AND 
					(r.relkind = ANY (ARRAY['r'::"char", 'p'::"char"]))
				) AS x(tblschema, tblname, tblowner, colname, cstrschema, cstrname)
			) AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	_table_name4rule := (SELECT table_name FROM target_data.c_ldsity_object WHERE id = _ldsity_object);

-- _adc or _spc is filled, depends on categories (rules), if they are populational, or areal
-- one category (rule) can be defined under more than one superior category, each of them has to be checked

i := 1;
CASE WHEN _adc IS NOT NULL
THEN
		_adc4loop2d :=  
			(WITH w_un AS (
				SELECT	adc,
					id AS ordinality,
					ceil(id/(count(*) OVER())::double precision * array_length(_adc[i:i],2) * array_length(_adc[i:i],3)) AS array_id
				FROM 
					unnest(_adc[i:i][:]) WITH ORDINALITY AS t(adc,id)
			), w_dist AS (
				SELECT array_agg(adc ORDER BY ordinality) AS adc, array_id
				FROM w_un
				GROUP BY array_id
			)
			SELECT array_agg(adc ORDER BY array_id) 
			FROM w_dist);

		--raise notice 'adc %', _adc;
		--raise notice 'adc4loop2d %', _adc4loop2d;
		FOR y IN 1..array_length(_adc4loop2d,1)
		LOOP
			-- for 2d array, because slicing will result again in 2D array even with one element/dimension
			-- it is necessary to unnest it and aggregate again -> 1D array
			_adc4loop :=  (SELECT array_agg(adc) FROM unnest(_adc4loop2d[y:y][:]) AS t(adc));

			--raise notice 'y %, adc4loop %', y, _adc4loop;
			IF cardinality(array_remove(_adc4loop,NULL)) > 0 OR y = 1
			THEN
				--raise notice 'y %, spc4loop %', y, _spc4loop;
				FOR adcv IN SELECT generate_subscripts(_adc4loop,1)
				LOOP
				--raise notice 'adcv %', adcv;
				IF adcv = 1 OR _adc4loop[adcv] IS NOT NULL THEN

				WITH w_tables AS (
				SELECT
					t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
						CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
						CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',1, ' AS rule_number, ', _case,' AS rul ') ELSE '' END,
						' FROM ', t1.table_name, 
						CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
						') AS ', _table_names_ws[t1.id]) AS table_select
				FROM
					unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
				LEFT JOIN
					(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
					FROM
						(SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.area_domain_category || _adc4loop[adcv]) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_adc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_spc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t2.table_name, t2.filter 
						FROM target_data.c_ldsity AS t1
						INNER JOIN target_data.c_ldsity_object AS t2
						ON t1.ldsity_object = t2.id
						WHERE t1.id = _ldsity
						) AS t1
					GROUP BY table_name
					) AS t2
				ON t1.table_name = t2.table_name
				)
				SELECT array_agg(table_select ORDER BY id)
				FROM w_tables
				INTO _table_selects;

				SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
					_table_names_ws[1], '.', _primary_keys[1], '
					FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
					INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
					INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
					INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
					_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
				INTO _table1;

				_join_all := NULL; _join := NULL;

				FOR i IN 2..array_length(_table_selects,1) 
				LOOP
					SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
					INTO _join;

					_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
					--raise notice '%', _join_all;
				END LOOP;
				--raise notice '%', _join_all;

				--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
				_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

				_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || _join_all;

				_qall := concat(CASE WHEN _qall IS NOT NULL THEN concat(_qall, ' UNION ALL ') ELSE '' END, _q);
				END IF;
				END LOOP;
			END IF;
		END LOOP;

	WHEN _spc IS NOT NULL
	THEN
		_spc4loop2d :=  
			(WITH w_un AS (
				SELECT	spc,
					id AS ordinality,
					ceil(id/(count(*) OVER())::double precision * array_length(_spc[i:i],2) * array_length(_spc[i:i],3)) AS array_id
				FROM 
					unnest(_spc[i:i][:]) WITH ORDINALITY AS t(spc,id)
			), w_dist AS (
				SELECT array_agg(spc ORDER BY ordinality) AS spc, array_id
				FROM w_un
				GROUP BY array_id
			)
			SELECT array_agg(spc ORDER BY array_id) 
			FROM w_dist);

		--raise notice 'spc %', _spc;
		--raise notice 'spc4loop2d %', _spc4loop2d;
		FOR y IN 1..array_length(_spc4loop2d,1)
		LOOP
			-- for 2d array, because slicing will result again in 2D array even with one element/dimension
			-- it is necessary to unnest it and aggregate again -> 1D array
			_spc4loop :=  (SELECT array_agg(spc) FROM unnest(_spc4loop2d[y:y][:]) AS t(spc));
			--raise notice 'i = %, spc = %, spc4loop = %', i, _spc, _spc4loop;
			IF cardinality(array_remove(_spc4loop,NULL)) > 0 OR y = 1
			THEN
				--raise notice 'y %, spc4loop %', y, _spc4loop;
				FOR spcv IN SELECT generate_subscripts(_spc4loop,1)
				LOOP

				IF spcv = 1 OR _spc4loop[spcv] IS NOT NULL THEN
				--raise notice 'spcv %', _spc4loop[spcv];

				WITH w_tables AS (
				SELECT
					t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
						CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
						CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',1, ' AS rule_number, ', _case,' AS rul ') ELSE '' END,
						' FROM ', t1.table_name, 
						CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
						') AS ', _table_names_ws[t1.id]) AS table_select
				FROM
					unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
				LEFT JOIN
					(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
					FROM
						(SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_adc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.sub_population_category || _spc4loop[spcv]) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_spc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t2.table_name, t2.filter 
						FROM target_data.c_ldsity AS t1
						INNER JOIN target_data.c_ldsity_object AS t2
						ON t1.ldsity_object = t2.id
						WHERE t1.id = _ldsity
						) AS t1
					GROUP BY table_name
					) AS t2
				ON t1.table_name = t2.table_name
				)
				SELECT array_agg(table_select ORDER BY id)
				FROM w_tables
				INTO _table_selects;

				SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
					_table_names_ws[1], '.', _primary_keys[1], '
					FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
					INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
					INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
					INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
					_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
				INTO _table1;

				_join_all := NULL; _join := NULL;

				FOR i IN 2..array_length(_table_selects,1) 
				LOOP
					SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
					INTO _join;

					_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
					--raise notice '%', _join_all;
				END LOOP;
				--raise notice '%', _join_all;

				--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
				_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

				_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || _join_all;

				_qall := concat(CASE WHEN _qall IS NOT NULL THEN concat(_qall, ' UNION ALL ') ELSE '' END, _q);
				END IF;
				END LOOP;
			END IF;
		END LOOP;
ELSE
			WITH w_tables AS (
				SELECT
					t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
						CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
						CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',1, ' AS rule_number, ', _case,' AS rul ') ELSE '' END,
						' FROM ', t1.table_name, 
						CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
						') AS ', _table_names_ws[t1.id]) AS table_select
				FROM
					unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
				LEFT JOIN
					(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
					FROM
						(SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_adc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_spc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t2.table_name, t2.filter 
						FROM target_data.c_ldsity AS t1
						INNER JOIN target_data.c_ldsity_object AS t2
						ON t1.ldsity_object = t2.id
						WHERE t1.id = _ldsity
						) AS t1
					GROUP BY table_name
					) AS t2
				ON t1.table_name = t2.table_name
			)
			SELECT array_agg(table_select ORDER BY id)
			FROM w_tables
			INTO _table_selects;

			SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
				_table_names_ws[1], '.', _primary_keys[1], '
				FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
				INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
				INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
				INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
				_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
			INTO _table1;

			_join_all := NULL; _join := NULL;

			FOR i IN 2..array_length(_table_selects,1) 
			LOOP
				SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
				INTO _join;

				_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
				--raise notice '%', _join_all;
			END LOOP;
			--raise notice '%', _join_all;

			--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
			_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

			_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
			_qall := _q;
END CASE;

	RETURN QUERY EXECUTE
	'WITH w AS (' || _qall || '),
	w2 AS (SELECT
			id, rul
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
	)
	SELECT rul AS compliance_status, count(*)::int AS no_of_objects
	FROM w2
	GROUP BY rul';

--return query select true, _no_of_rules_met, _no_of_objects;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) IS
'Function returns number of objects classified by the rule for given ldsity and attribute_type hierarchy.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) TO public;

-- </function>

-- <function name="fn_check_classification_rules" schema="target_data" src="functions/fn_check_classification_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rules(
	_ldsity integer,						-- id from table c_ldsity, ldsity contribution for which the rules should be tested
	_ldsity_object integer,						-- id from c_ldsity_object, for which object the classification rules are valid (must be within hierarchy of ldsity contribution)
	_rules text[], 							-- text representation of the classification rules which are used to classify an object
	_panel_refyearset integer DEFAULT NULL::int,			-- an array of panel and reference year set combinations (id from table cm_refyearset2panel_mapping)
									-- in which are classification rules applicable
	_adc integer[][] DEFAULT NULL::int[][], 			-- optional area domain categories contraint (only for areal), e.g. accessible land
	_spc integer[][] DEFAULT NULL::int[][]				-- dtto for populational, e.g. living stem for age classification rule, it is an array of arrays hence
									--  more categories from one population can fit the constraint for each rule, 
									-- e.g. for rule which classifies high intensity of damage a prerequisite can be two categories:
									-- damaged by bark stripping, damaged by human etc.
)
RETURNS TABLE (
	result		boolean,
	message_id	integer,
	message		text
) 
AS
$$
DECLARE
_ldsity_objects		integer[];
_adc_rule_test		integer[];
_spc_rule_test		integer[];
_table_name4rule	varchar;
_rule			text;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_cases			varchar[];
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_q_all			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
_exist_test		boolean;
_exist_test_all		boolean[];
--_qrule			text;
_adc4loop2d		integer[][];
_spc4loop2d		integer[][];
_adc4loop		integer[];
_spc4loop		integer[];
adcv			integer;
spcv			integer;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	IF NOT array[_ldsity_object] <@ (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]))
	THEN
		RAISE EXCEPTION 'Given ldsity object (%) is not found within the hierarchy of ldsity contribution (%).', _ldsity_object, _ldsity;
	END IF;

	FOREACH _rule IN ARRAY _rules
	LOOP
		SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
		INTO _test;

		IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
		THEN
			_exist_test := true; _rule = true;
		ELSE 	_exist_test := false;
		END IF;

		_exist_test_all := _exist_test_all || _exist_test;

		_test_all := _test_all || array[_test];

		_case := 'CASE WHEN '||_rule||' THEN true ELSE false END';

		_cases := _cases || _case;
		--raise notice '%', _test_all;
	END LOOP;

	IF _test_all @> array[false]
	THEN
		RAISE EXCEPTION 'One or more given rules has an invalid syntax.';
	END IF;

	IF _exist_test_all @> array[true] AND _exist_test_all @> array[false]
	THEN
		RAISE EXCEPTION 'In array of given rules there is an EXISTS or NOT EXISTS rule but there are also another rules, this rules have to be alone within one sub population.';
	END IF;

	IF _exist_test_all @> array[true] AND _exist_test_all != ARRAY[true,true]
	THEN
		RAISE EXCEPTION 'The given array of rules has more or less then 2 rules (%). If this is a special case of EXISTS rule, then its counterpart NOT EXISTS has to be entered and nothing else.',_rules;
	END IF;

	_ldsity_objects := (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]));


	IF _adc IS NOT NULL AND array_length(_rules,1) != array_length(_adc,1)
	THEN
		RAISE EXCEPTION 'Given array of area domain categories must be the same length as the given array of rules.';
	END IF;

	IF _spc IS NOT NULL AND array_length(_rules,1) != array_length(_spc,1)
	THEN
		RAISE EXCEPTION 'Given array of sub population categories must be the same length as the given array of rules.';
	END IF;

-- get id from cm_adc2classification_rule id for each adc
	WITH RECURSIVE w_cm_ids AS (
		SELECT
				t2.id AS object_id, t1.id, t1.area_domain_category, t3.array_id, t3.id AS ordinality
			FROM
				target_data.cm_adc2classification_rule AS t1
			INNER JOIN
				unnest(_ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
			ON
			   t1.ldsity_object = t2.ldsity_object
			RIGHT JOIN
				(
				SELECT  adc, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_adc,1)),12) ) AS array_id
				FROM  unnest(_adc) WITH ORDINALITY AS t1(adc, id)
				) AS t3
			ON
				t1.area_domain_category = t3.adc
			ORDER BY t2.id DESC, t3.array_id ASC
			-- because some rule can be met on more than one object, the rules from the object which is hierarchically higher will be picked
			LIMIT array_length(_adc,1) * array_length(_adc,2)
			),
	w_hier AS (
		SELECT 	t2.id AS cat_id, t2.ordinality, t2.array_id, 
			t2.id AS cm_id, t3.id AS cm_id_sup,
			t1.variable_superior, 2 AS id_sup, t1.variable, 1 AS id,
 			CASE WHEN t1.variable_superior IS NOT NULL THEN array[t1.variable_superior,t1.variable] ELSE array[t1.variable] END AS array_variable,
			CASE WHEN t3.id IS NOT NULL THEN array[t3.id, t2.id] ELSE array[t2.id] END AS cm_id_array_variable
		FROM target_data.t_adc_hierarchy AS t1
		RIGHT JOIN
			w_cm_ids AS t2
		ON t1.variable = t2.area_domain_category
		LEFT JOIN
			target_data.cm_adc2classification_rule AS t3
		ON t1.variable_superior = t3.area_domain_category
		LEFT JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		UNION ALL
		SELECT 	t2.cat_id, t2.ordinality, t2.array_id, 
			t2.cm_id, t2.cm_id_sup,
			t1.variable_superior, id_sup + 1, t1.variable, t2.id + 1, 
			array_prepend(t1.variable_superior, array_variable),
			array_prepend(t3.id, cm_id_array_variable)
		FROM target_data.t_adc_hierarchy AS t1
		INNER JOIN w_hier AS t2
		ON t1.variable = t2.variable_superior
		INNER JOIN target_data.cm_adc2classification_rule AS t3
		ON t1.variable_superior = t3.area_domain_category
		INNER JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
	), w_nulls AS (
		SELECT cat_id, ordinality, array_id, t1.array_variable[array_length(t1.array_variable,1)] AS cat, 
			array_variable, cm_id_array_variable,
			max(array_length(array_variable,1)) OVER () - array_length(array_variable,1) AS nulls2app,
			max(array_length(array_variable,1)) OVER () AS max_len
		FROM w_hier AS t1 
		LEFT JOIN target_data.t_adc_hierarchy AS t2 
		ON t1.variable_superior = t2.variable 
		WHERE t2.variable_superior IS NULL
	), w_append AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS var,
			cm_id_array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS cm_var,
			array_fill(NULL::int, ARRAY[max_len]) AS null_array, count(*) OVER(PARTITION BY cat) AS total
		FROM w_nulls
	), w_cat AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_agg(var) AS var_agg, 
			array_agg(cm_var) AS cm_var_agg, 
			null_array
		FROM w_append
		GROUP BY cat_id, cat, ordinality, array_id, null_array
	), w_cat_agg AS (
		SELECT cat_id, ordinality, array_id, cat, 
			var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS nulls2app,
			cm_var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS cm_nulls2app
		FROM w_cat
	), w_agg3d AS (
		SELECT
			array_id, 
			array_agg(nulls2app ORDER BY ordinality) AS cats,
			array_agg(cm_nulls2app ORDER BY ordinality) AS cm_ids
		FROM w_cat_agg
		GROUP BY array_id
	)
	SELECT 	--array_agg(cats ORDER BY array_id),
		array_agg(cm_ids ORDER BY array_id)
	FROM w_agg3d
	INTO _adc;

--raise notice '%', _ldsity_objects;
	WITH RECURSIVE w_cm_ids AS (
		SELECT
				t2.id AS object_id, t1.id, t1.sub_population_category, t3.array_id, t3.id AS ordinality
			FROM
				target_data.cm_spc2classification_rule AS t1
			INNER JOIN
				unnest(_ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
			ON
			   t1.ldsity_object = t2.ldsity_object
			RIGHT JOIN
				(
				SELECT  spc, id, ceil( round((id/(count(*) OVER())::numeric * array_length(_spc,1)),12) ) AS array_id
				FROM  unnest(_spc) WITH ORDINALITY AS t1(spc, id)
				) AS t3
			ON
				t1.sub_population_category = t3.spc
			ORDER BY t2.id DESC, t3.array_id ASC
			-- because some rule can be met on more than one object, the rules from the object which is hierarchically higher will be picked
			LIMIT array_length(_spc,1) * array_length(_spc,2)
			),
	w_hier AS (
		SELECT 	t2.id AS cat_id, t2.ordinality, t2.array_id, 
			t2.id AS cm_id, t3.id AS cm_id_sup,
			t1.variable_superior, 2 AS id_sup, t1.variable, 1 AS id,
  			CASE WHEN t3.id IS NOT NULL THEN array[t1.variable_superior,t1.variable] ELSE array[t1.variable] END AS array_variable,
			CASE WHEN t3.id IS NOT NULL THEN array[t3.id, t2.id] ELSE array[t2.id] END AS cm_id_array_variable
		FROM target_data.t_spc_hierarchy AS t1
		RIGHT JOIN
			w_cm_ids AS t2
		ON t1.variable = t2.sub_population_category
		LEFT JOIN
			target_data.cm_spc2classification_rule AS t3
		ON 	t1.variable_superior = t3.sub_population_category AND
			t3.classification_rule NOT IN ('EXISTS','NOT EXISTS')
		LEFT JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		UNION ALL
		SELECT 	t2.cat_id, t2.ordinality, t2.array_id, 
			t2.cm_id, t2.cm_id_sup,
			t1.variable_superior, id_sup + 1, t1.variable, t2.id + 1, 
			array_prepend(t1.variable_superior, array_variable),
			array_prepend(t3.id, cm_id_array_variable)
		FROM target_data.t_spc_hierarchy AS t1
		INNER JOIN w_hier AS t2
		ON t1.variable = t2.variable_superior
		INNER JOIN target_data.cm_spc2classification_rule AS t3
		ON t1.variable_superior = t3.sub_population_category
		INNER JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		WHERE t3.classification_rule NOT IN ('EXISTS','NOT EXISTS')
	), w_nulls AS (
		SELECT cat_id, ordinality, array_id, t1.array_variable[array_length(t1.array_variable,1)] AS cat, 
			array_variable, cm_id_array_variable,
			max(array_length(array_variable,1)) OVER () - array_length(array_variable,1) AS nulls2app,
			max(array_length(array_variable,1)) OVER () AS max_len
		FROM w_hier AS t1 
		LEFT JOIN target_data.t_spc_hierarchy AS t2 
		ON t1.variable_superior = t2.variable 
		WHERE t2.variable_superior IS NULL
	), w_append AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS var,
			cm_id_array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS cm_var,
			array_fill(NULL::int, ARRAY[max_len]) AS null_array, count(*) OVER(PARTITION BY cat) AS total
		FROM w_nulls
	), w_cat AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_agg(var) AS var_agg, 
			array_agg(cm_var) AS cm_var_agg, 
			null_array
		FROM w_append
		GROUP BY cat_id, cat, ordinality, array_id, null_array
	), w_cat_agg AS (
		SELECT cat_id, ordinality, array_id, cat, 
			var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS nulls2app,
			cm_var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS cm_nulls2app
		FROM w_cat
	), w_agg3d AS (
		SELECT
			array_id, 
			array_agg(nulls2app ORDER BY ordinality) AS cats,
			array_agg(cm_nulls2app ORDER BY ordinality) AS cm_ids
		FROM w_cat_agg
		GROUP BY array_id
	)
	SELECT 	--array_agg(cats ORDER BY array_id),
		array_agg(cm_ids ORDER BY array_id)
	FROM w_agg3d
	INTO _spc;


-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			(SELECT current_database()::information_schema.sql_identifier AS table_catalog,
				x.tblschema::information_schema.sql_identifier AS table_schema,
				x.tblname::information_schema.sql_identifier AS table_name,
				x.colname::information_schema.sql_identifier AS column_name,
				current_database()::information_schema.sql_identifier AS constraint_catalog,
				x.cstrschema::information_schema.sql_identifier AS constraint_schema,
				x.cstrname::information_schema.sql_identifier AS constraint_name
			FROM 
				(SELECT DISTINCT nr.nspname,
					r.relname,
					r.relowner,
					a.attname,
					nc.nspname,
					c.conname
				FROM pg_namespace nr,
					pg_class r,
					pg_attribute a,
					pg_depend d,
					pg_namespace nc,
					pg_constraint c
			 	WHERE 
					nr.oid = r.relnamespace AND r.oid = a.attrelid AND 
					d.refclassid = 'pg_class'::regclass::oid AND d.refobjid = r.oid AND 
					d.refobjsubid = a.attnum AND d.classid = 'pg_constraint'::regclass::oid AND 
					d.objid = c.oid AND c.connamespace = nc.oid AND c.contype = 'c'::"char" AND 
					(r.relkind = ANY (ARRAY['r'::"char", 'p'::"char"])) AND NOT a.attisdropped
				UNION ALL
				SELECT nr.nspname,
					r.relname,
					r.relowner,
					a.attname,
					nc.nspname,
					c.conname
			   	FROM pg_namespace nr,
					pg_class r,
					pg_attribute a,
					pg_namespace nc,
					pg_constraint c
				WHERE 
					nr.oid = r.relnamespace AND r.oid = a.attrelid AND nc.oid = c.connamespace AND r.oid =
					CASE c.contype
					WHEN 'f'::"char" THEN c.confrelid
					ELSE c.conrelid
					END AND (a.attnum = ANY (
						CASE c.contype
						WHEN 'f'::"char" THEN c.confkey
				    		ELSE c.conkey
						END)
					) AND NOT a.attisdropped AND (c.contype = ANY (ARRAY['p'::"char", 'u'::"char", 'f'::"char"])) AND 
					(r.relkind = ANY (ARRAY['r'::"char", 'p'::"char"]))
				) AS x(tblschema, tblname, tblowner, colname, cstrschema, cstrname)
			) AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	_table_name4rule := (SELECT table_name FROM target_data.c_ldsity_object WHERE id = _ldsity_object);

	FOR i IN 1..array_length(_cases,1)
	LOOP
		--raise notice 'i %', i;
		CASE
		WHEN _adc IS NOT NULL
		THEN

			-- 4D array, for each category build 2D array - each array within is an unique combination of categories/rules
			-- and each object must belong only into one combination
			_adc4loop2d :=  
					(WITH w_un AS (
						SELECT	adc,
							id AS ordinality,
							ceil( round((id/(count(*) OVER())::numeric * array_length(_adc[i:i],2) * array_length(_adc[i:i],3)),12) ) AS array_id
						FROM 
							unnest(_adc[i:i][:]) WITH ORDINALITY AS t(adc,id)
					), w_dist AS (
						SELECT array_agg(adc ORDER BY ordinality) AS adc, array_id
						FROM w_un
						GROUP BY array_id
					)
					SELECT array_agg(adc ORDER BY array_id) 
					FROM w_dist);
			--raise notice 'adc %', _adc;
			--raise notice 'adc4loop2d %', _adc4loop2d;
			FOR y IN 1..array_length(_adc4loop2d,1)
			LOOP
				-- for 2d array, because slicing will result again in 2D array even with one element/dimension
				-- it is necessary to unnest it and aggregate again -> 1D array
				_adc4loop :=  (SELECT array_agg(adc) FROM unnest(_adc4loop2d[y:y][:]) AS t(adc));

				--raise notice 'y %, adc4loop %', y, _adc4loop;
				IF cardinality(array_remove(_adc4loop,NULL)) > 0 OR y = 1
				THEN
					--raise notice 'y %, spc4loop %', y, _spc4loop;
					--FOR adcv IN SELECT generate_subscripts(_adc4loop,1)
					--LOOP
					--raise notice 'adcv %', adcv;
					--IF adcv = 1 OR _adc4loop[adcv] IS NOT NULL THEN
						WITH w_tables AS (
						SELECT
							t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
								CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
								CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',i, ' AS rule_number, ', _cases[i],' AS rul ') ELSE '' END,
								' FROM ', t1.table_name, 
								CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
								') AS ', _table_names_ws[t1.id]) AS table_select
						FROM
							unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
						LEFT JOIN
							(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
							FROM
								(SELECT t4.table_name, t3.classification_rule 
								FROM target_data.c_ldsity AS t1,
									unnest(t1.area_domain_category || _adc4loop[adcv]) WITH ORDINALITY AS t2(rule, id)
								LEFT JOIN target_data.cm_adc2classification_rule AS t3
								ON t2.rule = t3.id
								INNER JOIN target_data.c_ldsity_object AS t4
								ON t3.ldsity_object = t4.id
								WHERE t1.id = _ldsity
								UNION ALL
								SELECT t4.table_name, t3.classification_rule 
								FROM target_data.c_ldsity AS t1,
									unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
								LEFT JOIN target_data.cm_spc2classification_rule AS t3
								ON t2.rule = t3.id
								INNER JOIN target_data.c_ldsity_object AS t4
								ON t3.ldsity_object = t4.id
								WHERE t1.id = _ldsity
								UNION ALL
								SELECT t2.table_name, t2.filter 
								FROM target_data.c_ldsity_object AS t2
								) AS t1
							WHERE t1.classification_rule IS NOT NULL
							GROUP BY table_name
							) AS t2
						ON t1.table_name = t2.table_name
					)
					SELECT array_agg(table_select ORDER BY id)
					FROM w_tables
					INTO _table_selects;

					SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
						_table_names_ws[1], '.', _primary_keys[1], '
						FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
						INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
						INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
						INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
						_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
					INTO _table1;

					_join_all := NULL; _join := NULL;

					FOR i IN 2..array_length(_table_selects,1) 
					LOOP
						SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
						INTO _join;

						_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
						--raise notice '%', _join_all;
					END LOOP;

					--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
					_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

					_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
			--raise notice '%', _table1;
					_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL ' ELSE '' END), _q);

					--END IF;
					--END LOOP;
				END IF;
			END LOOP;
			--_qrule := concat(CASE WHEN _qrule IS NOT NULL THEN concat(_qrule, ' UNION ALL ') ELSE '' END, _q_all);
			--raise notice '%',_q_all;
		WHEN _spc IS NOT NULL
		THEN
			-- 4D array, for each category build 2D array - each array within is an unique combination of categories/rules
			-- and each object must belong only into one combination
			_spc4loop2d :=  
					(WITH w_un AS (
						SELECT	spc,
							id AS ordinality,
							ceil( round((id/(count(*) OVER())::numeric * array_length(_spc[i:i],2) * array_length(_spc[i:i],3)),12) ) AS array_id
						FROM 
							unnest(_spc[i:i][:]) WITH ORDINALITY AS t(spc,id)
					), w_dist AS (
						SELECT array_agg(spc ORDER BY ordinality) AS spc, array_id
						FROM w_un
						GROUP BY array_id
					)
					SELECT array_agg(spc ORDER BY array_id) 
					FROM w_dist);
			--raise notice 'spc %', _spc;
			--raise notice 'spc4loop2d %', _spc4loop2d;
			FOR y IN 1..array_length(_spc4loop2d,1)
			LOOP
				-- for 2d array, because slicing will result again in 2D array even with one element/dimension
				-- it is necessary to unnest it and aggregate again -> 1D array
				_spc4loop :=  (SELECT array_agg(spc) FROM unnest(_spc4loop2d[y:y][:]) AS t(spc));
				--raise notice 'i = %, spc = %, spc4loop = %', i, _spc, _spc4loop;
				IF cardinality(array_remove(_spc4loop,NULL)) > 0 OR y = 1
				THEN
					--raise notice 'y %, spc4loop %', y, _spc4loop;
					--FOR spcv IN SELECT generate_subscripts(_spc4loop,1)
					--LOOP

					--IF spcv = 1 OR _spc4loop[spcv] IS NOT NULL THEN
					--raise notice 'spcv %', _spc4loop[spcv];
						WITH w_tables AS (
						SELECT
							t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
								CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
								CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',i, ' AS rule_number, ', _cases[i],' AS rul ') ELSE '' END,
								' FROM ', t1.table_name, 
								CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
								') AS ', _table_names_ws[t1.id]) AS table_select
						FROM
							unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
						LEFT JOIN
							(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
							FROM
								(SELECT t4.table_name, t3.classification_rule 
								FROM target_data.c_ldsity AS t1,
									unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
								LEFT JOIN target_data.cm_adc2classification_rule AS t3
								ON t2.rule = t3.id
								INNER JOIN target_data.c_ldsity_object AS t4
								ON t3.ldsity_object = t4.id
								WHERE t1.id = _ldsity
								UNION ALL
								SELECT t4.table_name, t3.classification_rule 
								FROM target_data.c_ldsity AS t1,
									unnest(t1.sub_population_category || _spc4loop[spcv]) WITH ORDINALITY AS t2(rule, id)
								LEFT JOIN target_data.cm_spc2classification_rule AS t3
								ON t2.rule = t3.id
								INNER JOIN target_data.c_ldsity_object AS t4
								ON t3.ldsity_object = t4.id
								WHERE t1.id = _ldsity
								UNION ALL
								SELECT t2.table_name, t2.filter 
								FROM target_data.c_ldsity_object AS t2
								) AS t1
							WHERE t1.classification_rule IS NOT NULL
							GROUP BY table_name
							) AS t2
						ON t1.table_name = t2.table_name
					)
					SELECT array_agg(table_select ORDER BY id)
					FROM w_tables
					INTO _table_selects;

					SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
						_table_names_ws[1], '.', _primary_keys[1], '
						FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
						INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
						INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
						INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
						_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
					INTO _table1;

					_join_all := NULL; _join := NULL;

					FOR i IN 2..array_length(_table_selects,1) 
					LOOP
						SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
						INTO _join;

						_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
						--raise notice '%', _join_all;
					END LOOP;

					--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
					_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

					_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
			--raise notice '%', _table1;
					_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL ' ELSE '' END), _q);
					--raise notice 'loop spcs, rule %, spc %', i, spcv;
					--raise notice '%', _q_all; 

					--END IF;
					--END LOOP;
				END IF;
			END LOOP;

			--_qrule := concat(concat(_qrule, CASE WHEN _qrule IS NOT NULL THEN ' UNION ALL ' ELSE '' END), _q_all);
		ELSE
			WITH w_tables AS (
				SELECT
					t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
						CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
						CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',i, ' AS rule_number, ', _cases[i],' AS rul ') ELSE '' END,
						' FROM ', t1.table_name, 
						CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
						') AS ', _table_names_ws[t1.id]) AS table_select
				FROM
					unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
				LEFT JOIN
					(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
					FROM
						(SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_adc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_spc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t2.table_name, t2.filter 
						FROM target_data.c_ldsity_object AS t2
						) AS t1
					WHERE t1.classification_rule IS NOT NULL
					GROUP BY table_name
					) AS t2
				ON t1.table_name = t2.table_name
			)
			SELECT array_agg(table_select ORDER BY id)
			FROM w_tables
			INTO _table_selects;

			SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
				_table_names_ws[1], '.', _primary_keys[1], '
				FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
				INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
				INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
				INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
				_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
			INTO _table1;

			_join_all := NULL; _join := NULL;

			FOR i IN 2..array_length(_table_selects,1) 
			LOOP
				SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
				INTO _join;

				_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
				--raise notice '%', _join_all;
			END LOOP;

			--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
			_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

			_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
	--raise notice '%', _table1;
			_q_all := concat(concat(_q_all, CASE WHEN _q_all IS NOT NULL THEN E'\nUNION ALL ' ELSE '' END), _q);

			--_qrule := _q_all;
		END CASE;
	END LOOP;

	--raise notice '%',_q_all;

	EXECUTE
	'WITH w AS (' || _q_all || '),
	w2 AS (SELECT
			id, rul, sum(CASE WHEN rul=true THEN 1 ELSE 0 END) AS no_of_rules_met
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
		GROUP BY id, rul
	), w_test AS (
		SELECT no_of_rules_met::int, count(*)::int AS no_of_objects
		FROM w2
		GROUP BY no_of_rules_met
	)
	SELECT
		array_agg(no_of_rules_met ORDER BY no_of_rules_met) AS no_of_rules_met,
		array_agg(no_of_objects ORDER BY no_of_rules_met) AS no_of_objects
	FROM
		w_test
	'
	INTO _no_of_rules_met, _no_of_objects;

--raise notice '%, %', _no_of_rules_met, _no_of_objects;

	_total := (SELECT sum(nob) FROM unnest(_no_of_objects) AS t(nob));

--return query select true, _no_of_rules_met, _no_of_objects;

	-- if both are NULL 
IF _no_of_rules_met IS NULL AND _no_of_objects IS NULL
THEN	
	RETURN QUERY 
		SELECT false, 0, concat('There were no records on which the rules could be tested - given combination of panels and reference year sets probably does not correspond with the local density contribution availability.')::text;
ELSE
-- if one rule given - all records have 1 rule met
	IF array_length(_rules,1) = 1
	THEN
		IF array_length(_no_of_rules_met,1) != 1
		THEN
			RETURN QUERY 
			SELECT false, 1, concat('Only one rule was given, so all records has to result into true. There were only ', _no_of_objects[2], ' number of objects successfully categorized from ', _total, ' overall total.')::text;
		ELSE
			IF _no_of_rules_met[1] = 0
			THEN
				RETURN QUERY
				SELECT false, 2, concat('Only one rule was given, and none of the ', _no_of_objects[1], ' number of objects was successfully categorized.')::text;
			ELSE
			--raise notice '%, %', _no_of_rules_met, _no_of_objects;
				RETURN QUERY 
				SELECT true, 3, concat('Only one rule was given and every object was successfully categorized by it. All from ', _no_of_objects[1], ' number of objects resulted in ', _total, ' successful records.')::text;
			END IF;
		END IF;
	END IF;


-- if 2 or more rules given - all records have 1 rule met and 0 rule met
	-- both numbers must be the same (logically if 1 object belongs to some rule, basically also fails to the other rules)

	IF array_length(_rules,1) > 1
	THEN
		-- there are objects with 2 or more rules met (intersection between rules, can be from 0,1,2 or 1,2)
		IF EXISTS(SELECT * FROM unnest(_no_of_rules_met) AS t(nor) WHERE nor >= 2)
		THEN
			IF _exist_test_all = array[true, true]
			THEN
				RETURN QUERY
				SELECT true, 8, concat('Special case of EXISTS rules, implicitly true for all records.')::text;
			ELSE
				RETURN QUERY 
				SELECT false, 4, concat('Some of the objects (', _no_of_objects, ') were categorized by more than one rule, there is an intersection between the rule conditions.')::text;
			END IF;
		ELSE
			-- there are objects which are not covered by rules (only 0,1)
			IF array_length(_no_of_rules_met,1) = 2
			THEN
				IF _no_of_objects[1] != _no_of_objects[2]
				THEN
					RETURN QUERY 
					SELECT false, 5, concat('Some of the objects (', _no_of_objects[1] - _no_of_objects[2], ' from total of ', _no_of_objects[1], ') were not successfully categorized by any of the rules. Consider editing hierarchy.')::text;
				ELSE
					-- the rest, where 0,1, both the same number of objects
					IF _no_of_objects[1] = _no_of_objects[2]
					THEN
						RETURN QUERY
						SELECT true, 6, concat('Every object (', _no_of_objects[1], ') was succesfully categorized without any intersection.')::text;
					ELSE
						--raise notice '%, %', _no_of_rules_met, _no_of_objects;
						RAISE EXCEPTION '01: Not known state of classification rules.';
					END IF;
				END IF;
			ELSE
				IF array_length(_no_of_rules_met,1) = 1 AND _no_of_rules_met[1] = 0
				THEN
					RETURN QUERY 
					SELECT false, 7, concat('None of the ', _no_of_objects[1], ' was successfully categorized by any of the rules.')::text;
				ELSE
					RAISE EXCEPTION '02: Not known state of classification rules.';
				END IF;
			END IF;
		END IF;
	END IF;
END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[], integer[]) IS
'Function checks syntax in text field with classification rules for given ldsity contribution and ldsity object within its hierarchy. Optionally also for given panel x reference year set combination and/or  area domain/sub population category/ies.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rules(integer, integer, text[], integer, integer[],  integer[]) TO public;

-- </function>

DROP FUNCTION target_data.fn_get_classification_rule4adc(integer, integer) CASCADE;
-- <function name="fn_get_classification_rule4adc" schema="target_data" src="functions/fn_get_classification_rule4adc.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4adc
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_classification_rule4adc(integer, integer, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_classification_rule4adc(
	_area_domain_category integer DEFAULT NULL::integer, 
	_ldsity_object integer DEFAULT NULL::integer, 
	_use_negative boolean DEFAULT NULL::boolean )
RETURNS TABLE (
id			integer,
area_domain_category	integer,
ldsity_object		integer,
classification_rule	text,
use_negative		boolean
)
AS
$$
BEGIN
	IF _area_domain_category IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.area_domain_category, t1.ldsity_object, t1.classification_rule, t1.use_negative
		FROM target_data.cm_adc2classification_rule AS t1
		WHERE
			CASE WHEN _use_negative IS NOT NULL THEN t1.use_negative = _use_negative ELSE true END
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain_category AS t1 WHERE t1.id = _area_domain_category)
		THEN RAISE EXCEPTION 'Given area_domain_category does not exist in table c_area_domain_category (%)', _area_domain_category;
		END IF;

		IF _ldsity_object IS NOT NULL
		THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
			THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
			END IF;

			RETURN QUERY
			SELECT t2.id, t1.id AS area_domain_category, t2.ldsity_object, t2.classification_rule, t2.use_negative
			FROM target_data.c_area_domain_category AS t1
			LEFT JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.id = t2.area_domain_category
			WHERE t1.id = _area_domain_category AND t2.ldsity_object = _ldsity_object AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			ORDER BY t2.id;
		ELSE
			RETURN QUERY
			SELECT t2.id, t1.id AS area_domain_category, t2.ldsity_object, t2.classification_rule, t2.use_negative
			FROM target_data.c_area_domain_category AS t1
			LEFT JOIN target_data.cm_adc2classification_rule AS t2
			ON t1.id = t2.area_domain_category
			WHERE t1.id = _area_domain_category AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			ORDER BY t2.id;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_classification_rule4adc(integer, integer, boolean) IS
'Function returns records from cm_adc2classification_rule table, optionally for given area_domain_category and/or ldsity object.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_classification_rule4adc(integer, integer, boolean) TO public;

-- </function>

DROP FUNCTION target_data.fn_get_classification_rule4spc(integer, integer) CASCADE;
-- <function name="fn_get_classification_rule4spc" schema="target_data" src="functions/fn_get_classification_rule4spc.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4spc
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_classification_rule4spc(integer, integer, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_classification_rule4spc(
	_sub_population_category integer DEFAULT NULL::integer, 
	_ldsity_object integer DEFAULT NULL::integer,
	_use_negative boolean DEFAULT NULL::boolean )
RETURNS TABLE (
id			integer,
sub_population_category	integer,
ldsity_object		integer,
classification_rule	text,
use_negative		boolean
)
AS
$$
BEGIN
	IF _sub_population_category IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.sub_population_category, t1.ldsity_object, t1.classification_rule, t1.use_negative
		FROM target_data.cm_spc2classification_rule AS t1
		WHERE
			CASE WHEN _use_negative IS NOT NULL THEN t1.use_negative = _use_negative ELSE true END
		ORDER BY t1.id;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population_category AS t1 WHERE t1.id = _sub_population_category)
		THEN RAISE EXCEPTION 'Given sub_population_category does not exist in table c_sub_population_category (%)', _sub_population_category;
		END IF;

		IF _ldsity_object IS NOT NULL
		THEN
			IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object AS t1 WHERE t1.id = _ldsity_object)
			THEN RAISE EXCEPTION 'Given ldsity object does not exist in table c_ldsity_object (%)', _ldsity_object;
			END IF;

			RETURN QUERY
			SELECT t2.id, t1.id AS sub_population_category, t2.ldsity_object, t2.classification_rule, t2.use_negative
			FROM target_data.c_sub_population_category AS t1
			LEFT JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.id = _sub_population_category AND t2.ldsity_object = _ldsity_object AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			ORDER BY t2.id;
		ELSE
			RETURN QUERY
			SELECT t2.id, t1.id AS sub_population_category, t2.ldsity_object, t2.classification_rule, t2.use_negative
			FROM target_data.c_sub_population_category AS t1
			LEFT JOIN target_data.cm_spc2classification_rule AS t2
			ON t1.id = t2.sub_population_category
			WHERE t1.id = _sub_population_category AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			ORDER BY t2.id;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_classification_rule4spc(integer, integer, boolean) IS
'Function returns records from cm_spc2classification_rule table, optionally for given sub_population_category and/or ldsity object.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_classification_rule4spc(integer, integer, boolean) TO public;

-- </function>

DROP FUNCTION target_data.fn_get_area_domain(integer) CASCADE;
-- <function name="fn_get_area_domain" schema="target_data" src="functions/fn_get_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_area_domain(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_area_domain(_id integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
classification_type 	integer
)
AS
$$
BEGIN
	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.classification_type
		FROM target_data.c_area_domain AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.classification_type
		FROM target_data.c_area_domain AS t1
		WHERE t1.id = _id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_area_domain(integer) IS
'Function returns records from c_area_domain table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_area_domain(integer) TO public;

-- </function>

DROP FUNCTION target_data.fn_get_sub_population(integer) CASCADE;
-- <function name="fn_get_sub_population" schema="target_data" src="functions/fn_get_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_sub_population(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_sub_population(_id integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
classification_type	integer
)
AS
$$
BEGIN
	IF _id IS NULL
	THEN
		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.classification_type
		FROM target_data.c_sub_population AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Given subpopulation does not exist in table c_sub_population (%)', _id;
		END IF;

		RETURN QUERY
		SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t1.classification_type
		FROM target_data.c_sub_population AS t1
		WHERE t1.id = _id;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_sub_population(integer) IS
'Function returns records from c_sub_population table.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_sub_population(integer) TO public;

-- </function>

DROP FUNCTION target_data.fn_get_area_domain_category(integer, integer, boolean) CASCADE;
-- <function name="fn_get_area_domain_category" schema="target_data" src="functions/fn_get_area_domain_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_area_domain_category(integer, integer, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_area_domain_category(
	_target_variable integer DEFAULT NULL::integer, -- target variable (id from c_target_variable)
	_ldsity integer DEFAULT NULL::integer, 		-- ldsity contribution (id from c_ldsity)
	_use_negative boolean DEFAULT NULL::boolean	-- use_negative, only with combination target_variable NOT NULL
	)
RETURNS TABLE (
id		integer,			-- id from table c_area_domain_category
target_variable integer,			-- target variable (id from c_target_variable)
ldsity		integer,			-- local density contribution (id from c_ldsity)
label		character varying(200),		-- label from c_area_domain_category
description	text,				-- description dtto
label_en	character varying(200),
description_en	text,
use_negative	boolean,
restriction	boolean
)
AS
$$
BEGIN
	IF _ldsity IS NULL
	THEN
		IF _target_variable IS NULL 
		THEN
			IF _use_negative IS NOT NULL THEN
				RAISE EXCEPTION 'Use negative can be specified only in combination with not null target variable.';
			END IF;

			RETURN QUERY
			SELECT t1.id, NULL::integer, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en, NULL::boolean, NULL::boolean
			FROM target_data.c_area_domain_category AS t1
			ORDER BY t1.id;
		ELSE
			RAISE EXCEPTION 'If target_variable is specified, ldsity has to be specified too.';
		END IF;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
		END IF;

		IF _target_variable IS NULL
		THEN
			IF _use_negative IS NOT NULL THEN
				RAISE EXCEPTION 'Use negative can be specified only in combination with not null target variable.';
			END IF;

			RETURN QUERY
			SELECT t4.id, NULL::integer, t1.id AS ldsity, t4.label, t4.description, t4.label_en, t4.description_en, NULL::boolean, false::boolean AS restriction
			FROM target_data.c_ldsity AS t1,
			unnest(t1.area_domain_category) WITH ORDINALITY AS t2(def, id)
			LEFT JOIN target_data.cm_adc2classification_rule AS t3
			ON t2.def = t3.id
			LEFT JOIN target_data.c_area_domain_category AS t4
			ON t3.area_domain_category = t4.id
			WHERE t1.id = _ldsity
			ORDER BY t2.id;
		ELSE			
			IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
			THEN RAISE EXCEPTION 'Given target_variable does not exist in table c_target_variable (%)', _target_variable;
			END IF;

			IF NOT EXISTS (SELECT * FROM target_data.cm_ldsity2target_variable AS t1 
					WHERE t1.target_variable = _target_variable AND t1.ldsity = _ldsity AND
					CASE WHEN _use_negative IS NOT NULL THEN t1.use_negative = _use_negative ELSE true END)
			THEN RAISE EXCEPTION 'Given ldsity is not part of given target_variable.';
			END IF;

			RETURN QUERY
			SELECT t5.id, t2.target_variable, t1.id AS ldsity, t5.label, t5.description, t5.label_en, t5.description_en, t2.use_negative, false::boolean AS restriction
			FROM target_data.c_ldsity AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.ldsity,
				unnest(t1.area_domain_category) WITH ORDINALITY AS t3(def, id)
			LEFT JOIN target_data.cm_adc2classification_rule AS t4
			ON t3.def = t4.id
			LEFT JOIN target_data.c_area_domain_category AS t5
			ON t4.area_domain_category = t5.id
			WHERE t1.id = _ldsity AND t2.target_variable = _target_variable AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			--ORDER BY t2.id
			UNION ALL
			SELECT t5.id, t2.target_variable, t1.id AS ldsity, t5.label, t5.description, t5.label_en, t5.description_en, t2.use_negative, true::boolean AS restriction
			FROM target_data.c_ldsity AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.ldsity,
				unnest(t2.area_domain_category) WITH ORDINALITY AS t3(def, id)
			LEFT JOIN target_data.cm_adc2classification_rule AS t4
			ON t3.def = t4.id
			LEFT JOIN target_data.c_area_domain_category AS t5
			ON t4.area_domain_category = t5.id
			WHERE t1.id = _ldsity AND t2.target_variable = _target_variable AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			--ORDER BY t2.id
			;
		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_area_domain_category(integer, integer, boolean) IS
'Function returns records from c_area_domain_category table, optionally for given target variable and ldsity.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_area_domain_category(integer, integer, boolean) TO public;

-- </function>

-- <function name="fn_delete_area_domain" schema="target_data" src="functions/fn_delete_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_area_domain(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_area_domain(_id integer)
RETURNS void
AS
$$
BEGIN
	WITH w AS (
		DELETE FROM target_data.cm_ldsity2target2categorization_setup
		WHERE categorization_setup IN 
			(SELECT categorization_setup FROM target_data.cm_ldsity2target2categorization_setup
			WHERE adc2classification_rule @> 
				array[	(SELECT id FROM target_data.cm_adc2classification_rule 
					WHERE area_domain_category IN
					(SELECT id FROM target_data.c_area_domain_category 
					WHERE area_domain = _id))
					]
			)
		RETURNING categorization_setup
	)
	DELETE FROM target_data.t_categorization_setup
	WHERE id IN 
		(SELECT categorization_setup
		FROM w);

	DELETE FROM target_data.cm_adc2classrule2panel_refyearset 
	WHERE adc2classification_rule IN 
		(SELECT id FROM target_data.cm_adc2classification_rule 
		WHERE area_domain_category IN
			(SELECT id FROM target_data.c_area_domain_category 
			WHERE area_domain = _id)); 		
				
	DELETE FROM target_data.cm_adc2classification_rule 
	WHERE area_domain_category IN 
		(SELECT id FROM target_data.c_area_domain_category 
		WHERE area_domain = _id);

	IF EXISTS (SELECT * FROM target_data.t_adc_hierarchy
		WHERE variable_superior IN (SELECT id FROM target_data.c_area_domain_category
			WHERE area_domain = _id) AND
			dependent = true
		)
	THEN 
		RAISE EXCEPTION 'Given area domain cannot be deleted, because some of its categories are present in table t_adc_hierarchy in the superior position. It can destroy the dependency between the categories. You have to delete inferior categories (area domain) first.';
	END IF;

	DELETE FROM target_data.t_adc_hierarchy
	WHERE	variable IN (SELECT id FROM target_data.c_area_domain_category
			WHERE area_domain = _id) OR
		variable_superior IN (SELECT id FROM target_data.c_area_domain_category
			WHERE area_domain = _id);
	
	DELETE FROM target_data.c_area_domain_category 
	WHERE area_domain = _id;
	
	DELETE FROM target_data.c_area_domain 
	WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_area_domain(integer) IS
'Function deletes records from c_area_domain, c_area_domain_category, cm_adc2classification_rule and cm_adc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_area_domain(integer) TO public;

-- </function>

-- <function name="fn_delete_sub_population" schema="target_data" src="functions/fn_delete_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_delete_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_delete_sub_population(character varying, text, character varying, text) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_delete_sub_population(_id integer)
RETURNS void
AS
$$
BEGIN
	WITH w AS (
		DELETE FROM target_data.cm_ldsity2target2categorization_setup
		WHERE categorization_setup IN 
			(SELECT categorization_setup FROM target_data.cm_ldsity2target2categorization_setup
			WHERE spc2classification_rule @> 
				array[	(SELECT id FROM target_data.cm_spc2classification_rule 
					WHERE sub_population_category IN
					(SELECT id FROM target_data.c_sub_population_category 
					WHERE sub_population = _id))
					]
			)
		RETURNING categorization_setup
	)
	DELETE FROM target_data.t_categorization_setup
	WHERE id IN 
		(SELECT categorization_setup
		FROM w);

	DELETE FROM target_data.cm_spc2classrule2panel_refyearset 
	WHERE spc2classification_rule IN 
		(SELECT id FROM target_data.cm_spc2classification_rule 
		WHERE sub_population_category IN
			(SELECT id FROM target_data.c_sub_population_category 
			WHERE sub_population = _id)); 
				
	DELETE FROM target_data.cm_spc2classification_rule 
	WHERE sub_population_category IN 
		(SELECT id FROM target_data.c_sub_population_category 
		WHERE sub_population = _id);

	IF EXISTS (SELECT * FROM target_data.t_spc_hierarchy
		WHERE variable_superior IN (SELECT id FROM target_data.c_sub_population_category
			WHERE sub_population = _id) AND 
			dependent = true
		)
	THEN 
		RAISE EXCEPTION 'Given sub population cannot be deleted, because some of its categories are present in table t_spc_hierarchy in the superior position. It can destroy the dependency between the categories. You have to delete inferior categories (sub population) first.';
	END IF;

	DELETE FROM target_data.t_spc_hierarchy
	WHERE 	variable IN (SELECT id FROM target_data.c_sub_population_category
			WHERE sub_population = _id) OR 
		variable_superior IN (SELECT id FROM target_data.c_sub_population_category
			WHERE sub_population = _id);

	DELETE FROM target_data.c_sub_population_category 
	WHERE sub_population = _id;
	
	DELETE FROM target_data.c_sub_population
	WHERE id = _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_delete_sub_population(integer) IS
'Function deletes records from c_sub_population, c_sub_population_category, cm_spc2classification_rule and cm_spc2classification_rule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_delete_sub_population(integer) TO public;

-- </function>

DROP FUNCTION target_data.fn_get_sub_population_category(integer, integer, boolean) CASCADE;
-- <function name="fn_get_sub_population_category" schema="target_data" src="functions/fn_get_sub_population_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_sub_population_category(integer, integer, boolean) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_sub_population_category(
	_target_variable integer DEFAULT NULL::integer, -- target variable (id from c_target_variable)
	_ldsity integer DEFAULT NULL::integer, 		-- ldsity contribution (id from c_ldsity)
	_use_negative boolean DEFAULT NULL::boolean	-- use_negative, only with combination target_variable NOT NULL
	)
RETURNS TABLE (
id		integer,			-- id from table c_sub_population_category
target_variable integer,			-- target variable (id from c_target_variable)
ldsity		integer,			-- local density contribution (id from c_ldsity)
label		character varying(200),		-- label from c_sub_population_category
description	text,				-- description dtto
label_en	character varying(200),
description_en	text,
use_negative	boolean,
restriction	boolean
)
AS
$$
BEGIN
	IF _ldsity IS NULL
	THEN
		IF _target_variable IS NULL
		THEN
			IF _use_negative IS NOT NULL THEN
				RAISE EXCEPTION 'Use negative can be specified only in combination with not null target variable.';
			END IF;

			RETURN QUERY
			SELECT t1.id, NULL::integer, NULL::integer, t1.label, t1.description, t1.label_en, t1.description_en, NULL::boolean, NULL::boolean
			FROM target_data.c_sub_population_category AS t1
			ORDER BY t1.id;
		ELSE
			RAISE EXCEPTION 'If target_variable is specified, ldsity has to be specified too.';
		END IF;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
		THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
		END IF;

		IF _target_variable IS NULL
		THEN
			IF _use_negative IS NOT NULL THEN
				RAISE EXCEPTION 'Use negative can be specified only in combination with not null target variable.';
			END IF;

			RETURN QUERY
			SELECT t4.id, NULL::integer, t1.id AS ldsity, t4.label, t4.description, t4.label_en, t4.description_en, NULL::boolean, false::boolean AS restriction
			FROM target_data.c_ldsity AS t1,
			unnest(t1.sub_population_category) WITH ORDINALITY AS t2(def, id)
			LEFT JOIN target_data.cm_spc2classification_rule AS t3
			ON t2.def = t3.id
			LEFT JOIN target_data.c_sub_population_category AS t4
			ON t3.sub_population_category = t4.id
			WHERE t1.id = _ldsity
			ORDER BY t2.id;
		ELSE
			IF NOT EXISTS (SELECT * FROM target_data.c_target_variable AS t1 WHERE t1.id = _target_variable)
			THEN RAISE EXCEPTION 'Given target_variable does not exist in table c_target_variable (%)', _target_variable;
			END IF;

			IF NOT EXISTS (SELECT * FROM target_data.cm_ldsity2target_variable AS t1 
					WHERE t1.target_variable = _target_variable AND t1.ldsity = _ldsity AND 
					CASE WHEN _use_negative IS NOT NULL THEN t1.use_negative = _use_negative ELSE true END)
			THEN RAISE EXCEPTION 'Given ldsity is not part of given target_variable.';
			END IF;

			RETURN QUERY
			SELECT t5.id, t2.target_variable, t1.id AS ldsity, t5.label, t5.description, t5.label_en, t5.description_en, t2.use_negative, false::boolean AS restriction
			FROM target_data.c_ldsity AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.ldsity,
				unnest(t1.sub_population_category) WITH ORDINALITY AS t3(def, id)
			LEFT JOIN target_data.cm_spc2classification_rule AS t4
			ON t3.def = t4.id
			LEFT JOIN target_data.c_sub_population_category AS t5
			ON t4.sub_population_category = t5.id
			WHERE t1.id = _ldsity AND t2.target_variable = _target_variable AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			--ORDER BY t2.id
			UNION ALL
			SELECT t5.id, t2.target_variable, t1.id AS ldsity, t5.label, t5.description, t5.label_en, t5.description_en, t2.use_negative, true::boolean AS restriction
			FROM target_data.c_ldsity AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.ldsity,
				unnest(t2.sub_population_category) WITH ORDINALITY AS t3(def, id)
			LEFT JOIN target_data.cm_spc2classification_rule AS t4
			ON t3.def = t4.id
			LEFT JOIN target_data.c_sub_population_category AS t5
			ON t4.sub_population_category = t5.id
			WHERE t1.id = _ldsity AND t2.target_variable = _target_variable AND
				CASE WHEN _use_negative IS NOT NULL THEN t2.use_negative = _use_negative ELSE true END
			--ORDER BY t2.id
			;

		END IF;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_sub_population_category(integer, integer, boolean) IS
'Function returns records from c_sub_population_category table, optionally for given target variable and ldsity.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_sub_population_category(integer, integer, boolean) TO public;

-- </function>

DROP FUNCTION target_data.fn_save_areal_or_pop(integer, varchar(200), text, varchar(200), text, integer) CASCADE;
-- <function name="fn_save_areal_or_pop" schema="target_data" src="functions/fn_save_areal_or_pop.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_areal_or_pop
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_save_areal_or_pop(integer, varchar(200), text, varchar(200), text, integer, integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_save_areal_or_pop(_areal_or_population integer, _label varchar(200), _description text, _label_en varchar(200), _description_en text, _classification_type integer DEFAULT 100, _id integer DEFAULT NULL::integer)
RETURNS integer
AS
$$
BEGIN
	IF _label IS NULL OR _description IS NULL OR _label_en IS NULL OR _description_en IS NULL
	THEN
		RAISE EXCEPTION 'Mandatory input of label or description or label_en or descrioption_en is null (%, %, %, %).', _label, _description, _label_en, _description_en;
	END IF; 

	IF _id IS NULl
	THEN
		CASE 
		WHEN _areal_or_population = 100 THEN
			INSERT INTO target_data.c_area_domain(label, description, label_en, description_en, classification_type)
			SELECT _label, _description, _label_en, _description_en, _classification_type
			RETURNING id
			INTO _id;

		WHEN _areal_or_population = 200 THEN
			INSERT INTO target_data.c_sub_population(label, description, label_en, description_en, classification_type)
			SELECT _label, _description, _label_en, _description_en, _classification_type
			RETURNING id
			INTO _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	ELSE
		CASE WHEN _areal_or_population = 100 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_area_domain AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain does not exist in table c_area_domain (%)', _id;
			END IF;

			UPDATE target_data.c_area_domain
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_area_domain.id = _id;

		WHEN _areal_or_population = 200 THEN

			IF NOT EXISTS (SELECT * FROM target_data.c_sub_population AS t1 WHERE t1.id = _id)
			THEN RAISE EXCEPTION 'Given area domain does not exist in table c_sub_population (%)', _id;
			END IF;

			UPDATE target_data.c_sub_population
			SET 	label = _label, description = _description, 
				label_en = _label_en, description_en = _description_en
			WHERE c_sub_population.id = _id;
		ELSE
			RAISE EXCEPTION 'Given areal_or_population parameter not known (%)', _areal_or_population;
		END CASE;
	END IF;

	RETURN _id;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_save_areal_or_pop(integer, character varying, text, character varying, text, integer, integer) IS
'Functions inserts a record into table c_area_domain or c_sub_population based on given parameters.';

GRANT EXECUTE ON FUNCTION target_data.fn_save_areal_or_pop(integer, character varying, text, character varying, text, integer, integer) TO public;

-- </function>




CREATE CONSTRAINT TRIGGER trg__c_area_domain__ins
    AFTER INSERT OR UPDATE ON target_data.c_area_domain
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION target_data.fn_trg_check_c_area_domain();

CREATE CONSTRAINT TRIGGER trg__c_area_domain_category__ins
    AFTER INSERT OR UPDATE ON target_data.c_area_domain_category
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION target_data.fn_trg_check_c_area_domain_category();

CREATE CONSTRAINT TRIGGER trg__c_sub_population__ins
    AFTER INSERT OR UPDATE ON target_data.c_sub_population
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION target_data.fn_trg_check_c_sub_population();

CREATE CONSTRAINT TRIGGER trg__c_sub_population_category__ins
    AFTER INSERT OR UPDATE ON target_data.c_sub_population_category
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE FUNCTION target_data.fn_trg_check_c_sub_population_category();



