--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule_id4category(integer[], integer[], integer[], integer[]) CASCADE;
-- <function name="fn_get_classification_rule_id4category" schema="target_data" src="functions/fn_get_classification_rule_id4category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule_id4category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule_id4category(integer[], integer[], integer[], integer[], boolean[], boolean[]) CASCADE;

create or replace function target_data.fn_get_classification_rule_id4category
(
	_area_domain_category		integer[],
	_area_domain_object			integer[],
	_sub_population_category 	integer[],
	_sub_population_object		integer[],
	_use_negative_ad			boolean[],
	_use_negative_sp			boolean[]
)
returns table
(
adc2classification_rule				integer[],
spc2classification_rule				integer[]
)
as
$$
declare
		_id_adc2classification_rule		integer[];
		_id_ad_i						integer;
		_id_spc2classification_rule		integer[];
		_id_sp_i						integer;
begin
		if	_area_domain_category is null
		then
			raise exception 'Error 01: fn_get_classification_rule_id4category: Input argument _area_domain_category must not be NULL!';
		end if;
		
		if	_sub_population_category is null
		then
			raise exception 'Error 02: fn_get_classification_rule_id4category: Input argument _sub_population_category must not be NULL!';
		end if;
	
		---------------------------------------------------------------------------------
		if _area_domain_category = array[0]
		then
			_id_adc2classification_rule := null::integer[];
		else
			for i in 1..array_length(_area_domain_category,1)
			loop
				select id from target_data.cm_adc2classification_rule
				where area_domain_category = _area_domain_category[i]
				and ldsity_object = _area_domain_object[i]
				and use_negative = _use_negative_ad[i]
				into _id_ad_i;
			
				if _area_domain_category[i] is distinct from 0 and _id_ad_i is null
				then
					raise exception 'Error 03: fn_get_classification_rule_id4category: For _area_domain_category = %, _area_domain_object = % and _use_negative = % not found any record in table cm_adc2classification_rule',_area_domain_category[i],_area_domain_object[i],_use_negative;
				end if;
			
				if i = 1
				then
					if _id_ad_i is null
					then
						_id_adc2classification_rule := null::integer[];
					else
						_id_adc2classification_rule := array[_id_ad_i];
					end if;
				else
					if _id_ad_i is null
					then
						_id_adc2classification_rule := _id_adc2classification_rule;
					else
						_id_adc2classification_rule := _id_adc2classification_rule || array[_id_ad_i];
					end if;
				end if;
			
			end loop;
		end if;
		---------------------------------------------------------------------------------
		if _sub_population_category = array[0]
		then
			_id_spc2classification_rule := null::integer[];
		else
			for i in 1..array_length(_sub_population_category,1)
			loop
				select id from target_data.cm_spc2classification_rule
				where sub_population_category = _sub_population_category[i]
				and ldsity_object = _sub_population_object[i]
				and use_negative = _use_negative_sp[i]
				into _id_sp_i;
			
				if _sub_population_category[i] is distinct from 0 and _id_sp_i is null
				then
					raise exception 'Error 04: fn_get_classification_rule_id4category: For _sub_population_category = %, _sub_population_object = % and _use_negative = % not found any record in table cm_spc2classification_rule',_sub_population_category[i],_sub_population_object[i],_use_negative;
				end if;			
			
				if i = 1
				then
					if _id_sp_i is null
					then
						_id_spc2classification_rule := null::integer[];
					else
						_id_spc2classification_rule := array[_id_sp_i];
					end if;
				else
					if _id_sp_i is null
					then
						_id_spc2classification_rule := _id_spc2classification_rule;
					else
						_id_spc2classification_rule := _id_spc2classification_rule || array[_id_sp_i];
					end if;
				end if;
			
			end loop;
		end if;
		---------------------------------------------------------------------------------

		return query select _id_adc2classification_rule, _id_spc2classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;		

comment on function target_data.fn_get_classification_rule_id4category(integer[], integer[], integer[], integer[], boolean[], boolean[]) is
'The function for the area domain or sub population category returns the database identifier of their classifiaction_rules.';

grant execute on function target_data.fn_get_classification_rule_id4category(integer[], integer[], integer[], integer[], boolean[], boolean[]) to public;
-- </function>



DROP FUNCTION IF EXISTS target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[]) CASCADE;
-- <function name="fn_get_classification_rules_id4categories" schema="target_data" src="functions/fn_get_classification_rules_id4categories.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rules_id4categories
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[], boolean[], boolean[]) CASCADE;

create or replace function target_data.fn_get_classification_rules_id4categories
(
	_area_domain integer[],
	_area_domain_object integer[],
	_sub_population integer[],
	_sub_population_object integer[],
	_use_negative_ad boolean[],
	_use_negative_sp boolean[]
)
returns table
(
	area_domain_category integer[],
	sub_population_category integer[],
	adc2classification_rule integer[],
	spc2classification_rule integer[]
) as
$$
declare
		_string_ad_w1			text;
		_string_ad_w3			text;
		_string_columns_ad		text;
		_string_columns_ad_w3	text;
	
		_string_sp_w2			text;
		_string_sp_w3			text;
		_string_columns_sp		text;
		_string_columns_sp_w3	text;
	
		_query_res				text;
begin
		-------------------------------
		-------------------------------
		if
			_area_domain is null
		then
			/*
			_string_ad_w1 := 'select 0 as adc_1';
			_string_ad_w3 := 'array[adc_1] as area_domain_category';
			*/

			_string_ad_w1 := 'select array[0] as area_domain, array[0] as area_domain_category';
		else
			/*		
			for i in 1..array_length(_area_domain,1)
			loop
					if i = 1
					then
						_string_columns_ad := concat('unnest(attr_',i,') as adc_',i);
						_string_columns_ad_w3 := concat('adc_',i);
					else
						_string_columns_ad := concat(_string_columns_ad,', ',concat('unnest(attr_',i,') as adc_',i));
						_string_columns_ad_w3 := concat(_string_columns_ad_w3,', adc_',i);
					end if;
			end loop;
		
			_string_ad_w1 := replace(replace(concat('select ',_string_columns_ad,' from target_data.fn_get_attribute_domain(''ad''::varchar,array[',_area_domain,'])'),'{',''),'}','');
			_string_ad_w3 := concat('array[',_string_columns_ad_w3,'] as area_domain_category');
			*/

			_string_ad_w1 := replace(replace(concat('select attribute_domain as area_domain, category as area_domain_category from target_data.fn_get_attribute_domain(''ad''::varchar,array[',_area_domain,'])'),'{',''),'}','');
		end if;
				
		-------------------------------
		-------------------------------
		
		-------------------------------
		-------------------------------
		if
			_sub_population is null
		then
			/*
			_string_sp_w2 := 'select 0 as spc_1';
			_string_sp_w3 := 'array[spc_1] as sub_population_category';
			*/

			_string_sp_w2 := 'select array[0] as sub_population_domain, array[0] as sub_population_category';
		else
			/*
			for i in 1..array_length(_sub_population,1)
			loop
					if i = 1
					then
						_string_columns_sp := concat('unnest(attr_',i,') as spc_',i);
						_string_columns_sp_w3 := concat('spc_',i);
					else
						_string_columns_sp := concat(_string_columns_sp,', ',concat('unnest(attr_',i,') as spc_',i));
						_string_columns_sp_w3 := concat(_string_columns_sp_w3,', spc_',i);
					end if;
			end loop;
		
			_string_sp_w2 := replace(replace(concat('select ',_string_columns_sp,' from target_data.fn_get_attribute_domain(''sp''::varchar,array[',_sub_population,'])'),'{',''),'}','');
			_string_sp_w3 := concat('array[',_string_columns_sp_w3,'] as sub_population_category');
			*/

			_string_sp_w2 := replace(replace(concat('select attribute_domain as sub_population_domain, category as sub_population_category from target_data.fn_get_attribute_domain(''sp''::varchar,array[',_sub_population,'])'),'{',''),'}','');
		end if;
		
		-------------------------------
		-------------------------------	

		_query_res := concat
		('
		with
		w1 as	(',_string_ad_w1,'),
		w2 as	(',_string_sp_w2,'),
		w_data_ad as (
			select
		 		$3 				as domain_orig, 
				w1.area_domain			as domain_reorder,
				$1 				as object_orig
			from w1 limit 1
		)
		, w_unnest_ad as (
			select
				domain_orig.id, domain_orig.item as domain_orig, 
		 		domain_reorder.item as domain_reorder,
		 		object_orig.item as object_orig
			from w_data_ad as w_data,
					unnest(w_data.domain_orig)		with ordinality as domain_orig(item, id)
			join 	unnest(w_data.domain_reorder) 	with ordinality as domain_reorder(item, id) 	on domain_orig.id = domain_reorder.id
			join 	unnest(w_data.object_orig) 		with ordinality as object_orig(item, id) 		on object_orig.id = domain_reorder.id
		)
		, w_sort_ad as (
			select
				w_unnest.object_orig,
				array_position(w_data.domain_reorder, w_unnest.domain_orig)
			from w_data_ad as w_data, w_unnest_ad as w_unnest
		)
		, w_reorder_ad as (
			select 
				array_agg(object_orig order by array_position) as object_reorder 
			from w_sort_ad as w_sort
		),
		w_data_sp as (
			select
		 		$4 				as domain_orig,
		 		w2.sub_population_domain	as domain_reorder,
		 		$2 				as object_orig
			from w2 limit 1
		)
		, w_unnest_sp as (
			select
				domain_orig.id, domain_orig.item as domain_orig, 
		 		domain_reorder.item as domain_reorder, 
		 		object_orig.item as object_orig
			from w_data_sp as w_data,
					unnest(w_data.domain_orig)		with ordinality as domain_orig(item, id)
			join 	unnest(w_data.domain_reorder) 	with ordinality as domain_reorder(item, id) 	on domain_orig.id = domain_reorder.id
			join 	unnest(w_data.object_orig) 		with ordinality as object_orig(item, id) 		on object_orig.id = domain_reorder.id
		)
		, w_sort_sp as (
			select
				w_unnest.object_orig,
				array_position(w_data.domain_reorder, w_unnest.domain_orig)
			from w_data_sp as w_data, w_unnest_sp as w_unnest
		)
		, w_reorder_sp as (
			select 
				array_agg(object_orig order by array_position) as object_reorder 
			from w_sort_sp as w_sort
		),
		w3 as	(select w1.area_domain_category, w2.sub_population_category from w1, w2),
		w4 as	(
				select
					area_domain_category,
					sub_population_category,
					(
					target_data.fn_get_classification_rule_id4category
						(
						area_domain_category,
						w_reorder_ad.object_reorder,
						sub_population_category,
						w_reorder_sp.object_reorder,
						$5,
						$6
						)
					) as res
				from
					w3, w_reorder_ad, w_reorder_sp
				)
		select
				area_domain_category,
				sub_population_category,
				(res).adc2classification_rule,
				(res).spc2classification_rule
		from
				w4 order by area_domain_category, sub_population_category;
		');
		return query execute ''||_query_res||'' using _area_domain_object, _sub_population_object, _area_domain, _sub_population, _use_negative_ad, _use_negative_sp;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[], boolean[], boolean[]) is
'The function for the area domain or sub population returns a combination of attribute classifications and database identifiers of their classifiaction_rules.';

grant execute on function target_data.fn_get_classification_rules_id4categories(integer[], integer[], integer[], integer[], boolean[], boolean[]) to public;
-- </function>



-- <function name="fn_get_categorization_setup" schema="target_data" src="functions/fn_get_categorization_setup.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_categorization_setup
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_categorization_setup(integer, integer[], integer[], integer[][], integer[], integer[][]) CASCADE;

create or replace function target_data.fn_get_categorization_setup
(
	_target_variable		integer,
	_target_variable_cm		integer[],
	_area_domain			integer[],
	_area_domain_object		integer[][],
	_sub_population			integer[],
	_sub_population_object	integer[][]
)
returns integer[]
as
$$
declare
		_array_id							integer[];
		_array_id4check						integer[];
		_lot_100							integer;
		_lot_200							integer;
		_variant							integer;	
		_area_domain4input					integer[];
		_area_domain_object4input			integer[];
		_sub_population4input				integer[];
		_sub_population_object4input		integer[];
		_area_domain4input_text				text;
		_area_domain_object4input_text		text;
		_sub_population4input_text			text;
		_sub_population_object4input_text	text;
		_string4attr						text;
		_query_res							text;
		_s4ij_p1							text;
		_s4ij_p2							text;
		_s4ij_p3							text;
		_s4ij_p4							text;
		_s4ij_p5							text;
		_string4incomming					text;
		_string4inner_join					text;
		_array_id_200						integer[];
		_string_inner_join					text;
		_string_incomming_0					text;
		_string_incomming					text;
		_string_check_1						text;
		_string_check_2						text;
		_string_check_3						text;
		_string_check_4						text;
		/*
		_s4da_p1							text;
		_s4da_p2							text;
		_s4da_p3							text;
		_s4da_p4							text;
		_s4ia_p1							text;
		_s4ia_p2							text;
		_s4ia_p3							text;
		_s4ia_p4							text;
		_s4result_p1						text;
		_s4result_p2						text;
		_string4database_agg				text;
		_string4incomming_agg				text;
		_string4result						text;
		*/
		_string_result						text;
		_res								text;
		_result								integer[];

		_use_negative_id_bc					boolean;
		_use_negative_ad_4input_text		text;
		_classification_type_ad				integer;
		_use_negative_ad_4input_text_i		text;
		_use_negative_ad_4input_text_join	text;
		_use_negative_sp_4input_text		text;
		_classification_type_sp				integer;
		_use_negative_sp_4input_text_i		text;
		_use_negative_sp_4input_text_join	text;		
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_get_categorization_setup: The input argument _target_variable must not be NULL !';
		end if;

		if _target_variable_cm is null
		then
			raise exception 'Error 02: fn_get_categorization_setup: The input argument _target_variable_cm must not be NULL !';
		end if;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where target_variable = _target_variable
		into
				_array_id;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where id in (select unnest(_target_variable_cm))
		into
				_array_id4check;			

		if _array_id != _array_id4check
			then raise exception 'Error 03: fn_get_categorization_setup: The internal argument _array_id = % and internal argument _array_id4check = % are not the same!',_array_id, _array_id4check;
		end if;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 04: fn_get_categorization_setup: This variant is not implemented yet!';
		end if;

		for bc in 1..array_length(_array_id,1)
		loop	
			-----------------------------------------
			-----------------------------------------
			if _area_domain is null and _sub_population is null
			then
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is null
			then
				if _variant in (3,4)
				then
					with
					w1 as (select unnest(_area_domain_object[1:1]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;				
				else		
					with
					w1 as (select unnest(_area_domain_object[bc:bc]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				end if;			
			
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;
			
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			------
			if _area_domain is null and _sub_population is not null
			then		
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
			
				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is not null
			then
				if _variant in (3,4)
				then
					with
					w1 as (select unnest(_area_domain_object[1:1]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				else
					with
					w1 as (select unnest(_area_domain_object[bc:bc]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				end if;				
			
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
	
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;			

				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			end if;
			-----------------------------------------
			-----------------------------------------
			if _area_domain4input is null
			then
				_area_domain4input_text := 'null::integer[]';
			else
				_area_domain4input_text := replace(replace(concat('array[',_area_domain4input,']'),'{',''),'}','');
			end if;
		
			if _area_domain_object4input is null
			then
				_area_domain_object4input_text := 'null::integer[]';
			else
				_area_domain_object4input_text := replace(replace(concat('array[',_area_domain_object4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population4input is null
			then
				_sub_population4input_text := 'null::integer[]';
			else
				_sub_population4input_text := replace(replace(concat('array[',_sub_population4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population_object4input is null
			then
				_sub_population_object4input_text := 'null::integer[]';
			else
				_sub_population_object4input_text := replace(replace(concat('array[',_sub_population_object4input,']'),'{',''),'}','');
			end if;

			-----------------------------------------
			-----------------------------------------
			-----------------------------------------
			_use_negative_id_bc := (select cmlv.use_negative from target_data.cm_ldsity2target_variable as cmlv where cmlv.id = _array_id[bc]);

			if _use_negative_id_bc is null
			then
				raise exception 'Error 05: fn_get_categorization_setup: For target_variable = % and target_variable_cm = % is value use_negative NULL!',_target_variable, _array_id[bc];
			end if;

			if _use_negative_id_bc = FALSE
			then
				if _area_domain4input is null or _area_domain_object4input is null
				then
					_use_negative_ad_4input_text := 'null::boolean[]';
				else
					_use_negative_ad_4input_text := (select replace(replace(concat('array[',(select array_agg(t.res) from (select 'false' as res from generate_series(1,array_length(_area_domain4input,1))) as t),']'),'{',''),'}',''));
				end if;

				if _sub_population4input is null or _sub_population_object4input is null
				then
					_use_negative_sp_4input_text := 'null::boolean[]';
				else
					_use_negative_sp_4input_text := (select replace(replace(concat('array[',(select array_agg(t.res) from (select 'false' as res from generate_series(1,array_length(_sub_population4input,1))) as t),']'),'{',''),'}',''));
				end if;
			else
				-- _use_negative_id_bc = TRUE

				-- AD --
				if _area_domain4input is null
				then
					_use_negative_ad_4input_text := 'null::boolean[]';
				else
					-- get classification_type for AD
					for ict in 1..array_length(_area_domain4input,1)
					loop
						select cad.classification_type from target_data.c_area_domain as cad
						where cad.id = _area_domain4input[ict]
						into _classification_type_ad;

						if _classification_type_ad is null
						then
							raise exception 'Error 06: fn_get_categorization_setup: For	target_variable = %, target_variable_cm = % and area_domain = % not exists any value classification_type in c_area_domain table!',_target_variable, _array_id[bc], _area_domain4input[ict];
						end if;

						if _classification_type_ad = 100
						then
							_use_negative_ad_4input_text_i := 'false';
						else
							_use_negative_ad_4input_text_i := 'true';
						end if;

						if ict = 1
						then
							_use_negative_ad_4input_text_join := _use_negative_ad_4input_text_i;
						else
							_use_negative_ad_4input_text_join := concat(_use_negative_ad_4input_text_join,',',_use_negative_ad_4input_text_i);
						end if;
					end loop;

					_use_negative_ad_4input_text := concat('array[',_use_negative_ad_4input_text_join,']');

				end if;
				
				-- SP --
				if _sub_population4input is null
				then
					_use_negative_sp_4input_text := 'null::boolean[]';
				else
					-- get classification_type for SP
					for ict in 1..array_length(_sub_population4input,1)
					loop
						select csp.classification_type from target_data.c_sub_population as csp
						where csp.id = _sub_population4input[ict]
						into _classification_type_sp;

						if _classification_type_sp is null
						then
							raise exception 'Error 07: fn_get_categorization_setup: For	target_variable = %, target_variable_cm = % and sub_population = % not exists any value classification_type in c_sub_population table!',_target_variable, _array_id[bc], _sub_population4input[ict];
						end if;

						if _classification_type_sp = 100
						then
							_use_negative_sp_4input_text_i := 'false';
						else
							_use_negative_sp_4input_text_i := 'true';
						end if;

						if ict = 1
						then
							_use_negative_sp_4input_text_join := _use_negative_sp_4input_text_i;
						else
							_use_negative_sp_4input_text_join := concat(_use_negative_sp_4input_text_join,',',_use_negative_sp_4input_text_i);
						end if;
					end loop;

					_use_negative_sp_4input_text := concat('array[',_use_negative_sp_4input_text_join,']');

				end if;
					
			end if;
			-----------------------------------------
			-----------------------------------------
			-----------------------------------------
			
			_string4attr := concat('
			select
					area_domain_category,
					sub_population_category,
					adc2classification_rule,
					spc2classification_rule
			from
					target_data.fn_get_classification_rules_id4categories
					(
						',_area_domain4input_text,',
						',_area_domain_object4input_text,',
						',_sub_population4input_text,',
						',_sub_population_object4input_text,',
						',_use_negative_ad_4input_text,',
						',_use_negative_sp_4input_text,'
					)
			');
		
			if bc = 1
			then
				_query_res := concat('with w_',bc,' as (',_string4attr,')');
			else
				_query_res := concat(_query_res,',w_',bc,' as (',_string4attr,')');
			end if;
		end loop;
		
		-------------------------------------------------------------
		-------------------------------------------------------------
		if _variant in (1,2)
		then
		
			_s4ij_p1 := 'select row_number() over() as group_id_poradi';
			_s4ij_p3 := ' from ';
		
			for bc in 1..array_length(_array_id,1)
			loop
				if bc = 1
				then
			
					_s4ij_p2 := concat('
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.area_domain_category
										,t',bc,'.sub_population_category
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'');
				
					_string4incomming := concat(
						'
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'
										on
											t1.area_domain_category = t',bc,'.area_domain_category and
											t1.sub_population_category = t',bc,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				end if;
			end loop;
			
				_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4;
		end if;		
		-------------------------------------------------------------
		if _variant in (3,4)
		then	
			_s4ij_p1 := concat(
				'
				select
					row_number() over() as group_id_poradi,
					a.*,
					b.*
				from
					(
					select
							',_array_id[1],' as ldsity2target_variable_',_array_id[1],',
							area_domain_category as area_domain_category_',_array_id[1],',
							adc2classification_rule as adc2classification_rule_',_array_id[1],',
							spc2classification_rule as spc2classification_rule_',_array_id[1],'
					from w_1
					) as a
				inner
				join
					(select ');
				
			_s4ij_p3 := ' from ';
		
			_s4ij_p5 := concat('
						) as b
					on
						a.area_domain_category_',_array_id[1],' = b.area_domain_category');
					
			_string4incomming := concat(
				'
				select
						1 as group_id,
						group_id_poradi,
						area_domain_category,
						sub_population_category,
						ldsity2target_variable_',_array_id[1],' as ldsity2target_variable,
						adc2classification_rule_',_array_id[1],' as adc2classification_rule,
						spc2classification_rule_',_array_id[1],' as spc2classification_rule
				from
						w_inner_join
				');					
				
			_array_id_200 := array_remove(_array_id, _array_id[1]);
			
			for bc_200 in 1..array_length(_array_id_200,1)
			loop
				if bc_200 = 1
				then				
					_s4ij_p2 := concat('
										t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.area_domain_category
										,t',bc_200,'.sub_population_category
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'');
				
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');				
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'
										on
											t1.area_domain_category = t',bc_200,'.area_domain_category and
											t1.sub_population_category = t',bc_200,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');
					
				end if;
			end loop;
			
			_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4 || _s4ij_p5;
		
		end if;
		-------------------------------------------------------------
		-------------------------------------------------------------
		_string_inner_join := concat(',w_inner_join as (',_string4inner_join,')');
		_string_incomming_0 := concat(',w_incomming_0 as (',_string4incomming,')');
		-------------------------------------------------------------
		if _variant in (3,4)
		then
			_string_incomming := concat(
				'
				,w_incomming as	(
								select
										group_id,
										group_id_poradi,
										------------------------------------
										area_domain_category,
										case
											when (array_remove(area_domain_category,0)) = array[]::integer[]
											then array[0]
											else (array_remove(area_domain_category,0))
										end as area_domain_category_upr,
										------------------------------------
										sub_population_category,
										case
											when (array_remove(sub_population_category,0)) = array[]::integer[]
											then array[0]
											else (array_remove(sub_population_category,0))
										end as sub_population_category_upr,
										------------------------------------
										ldsity2target_variable,
										------------------------------------
										case
											when adc2classification_rule is null
											then array[0]
											else adc2classification_rule
										end as adc2classification_rule,
										case
											when spc2classification_rule is null
											then array[0]
											else spc2classification_rule
										end as spc2classification_rule,
										------------------------------------
										case
											when ldsity2target_variable in (',array_to_string(_array_id_200,','),')
											then null::integer[]
											else adc2classification_rule
										end as adc2classification_rule4insert,
										spc2classification_rule as spc2classification_rule4insert
								from
									w_incomming_0
								)
				');


			_string_check_1 := concat(
				'
				,w_check_1 as		(
									select
											a.*,
											b.id_cm_setup,
											b.categorization_setup		
									from
											w_incomming as a
									left
									join	(								
											select
													tt1.*
											from
														(
														select
																t1.*,
																----------------------
																t2.adc2classification_rule4join
																----------------------
														from
															(
																	select
																		z1.*,
																		-----------------------
																		case
																			when z2.sub_population_category4join is null
																			then array[0]
																			else z2.sub_population_category4join
																		end as sub_population_category4join
																		-----------------------
																	from
																		(
																		select
																				id as id_cm_setup,
																				ldsity2target_variable,
																				-------------------------------------
																				case
																					when adc2classification_rule is null
																					then array[0]
																					else adc2classification_rule
																				end as adc2classification_rule,
																				-------------------------------------
																				spc2classification_rule,
																				categorization_setup
																		from
																				target_data.cm_ldsity2target2categorization_setup
																		where
																				ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
																		) as z1
																	left join
																		(
																		select
																			distinct categorization_setup, sub_population_category4join
																		from
																			(
																			select
																					categorization_setup,
																					case when spc2classification_rule is null then array[0]
																					else (select id_category from target_data.fn_get_category4classification_rule_id(''spc''::varchar,spc2classification_rule))
																					end as sub_population_category4join
																			from
																					target_data.cm_ldsity2target2categorization_setup
																			where
																					ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
																			) as t
																		where
																				t.sub_population_category4join is distinct from array[0]
																		) as z2
																	on
																		z1.categorization_setup = z2.categorization_setup
															) as t1
															inner join
															(
															select
																	categorization_setup,
																	case when adc2classification_rule is null then array[0] else adc2classification_rule end as adc2classification_rule4join
															from
																	target_data.cm_ldsity2target2categorization_setup
															where
																	ldsity2target_variable = ',_array_id[1],'
															) as t2
														on t1.categorization_setup = t2.categorization_setup
														)
															as tt1
											inner join
														w_incomming as tt2
											on
													tt1.ldsity2target_variable = tt2.ldsity2target_variable
												and	(target_data.fn_array_compare(tt1.adc2classification_rule4join,tt2.adc2classification_rule))
												and	(target_data.fn_array_compare(tt1.sub_population_category4join,tt2.sub_population_category_upr))
											) as b
									on	
											a.ldsity2target_variable = b.ldsity2target_variable
										and	(target_data.fn_array_compare(a.adc2classification_rule,b.adc2classification_rule4join))
										and	(target_data.fn_array_compare(a.sub_population_category_upr,b.sub_population_category4join))		
									)
			');

			_string_check_3 := concat(
				'
				,w_check_3 as		(
									select * from w_check_2 union all
									select distinct c.group_id, c.group_id_poradi,
									------------------------
									c.area_domain_category,
									c.area_domain_category_upr,
									c.sub_population_category,
									c.sub_population_category_upr,
									--------------------------
									c.ldsity2target_variable,
									c.adc2classification_rule,
									c.spc2classification_rule,
									c.adc2classification_rule4insert,
									c.spc2classification_rule4insert,
									c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
									from
											(
											select
													1 as group_id,
													group_id_poradi,
													--------------------------
													area_domain_category,
													area_domain_category_upr,
													sub_population_category,
													sub_population_category_upr,
													---------------------------
													',_array_id[1],' as ldsity2target_variable,
													adc2classification_rule,
													null::integer[] as spc2classification_rule,
													adc2classification_rule4insert,
													spc2classification_rule4insert,
													null::integer as id_cm_setup,
													null::integer as categorization_setup,
													check_exists_group_id_1
											from
													w_check_2 where check_exists_group_id_1 = 1
											) as c
									)				
			');
		else
			_string_incomming := concat(
				'
				,w_incomming as	(
								select
										group_id,
										group_id_poradi,
										area_domain_category,
										sub_population_category,
										ldsity2target_variable,
										adc2classification_rule,
										spc2classification_rule
								from
									w_incomming_0
								)
				');

			_string_check_1 := concat(
				'
				,w_check_1 as		(
									select
											a.*,
											b.id_cm_setup,
											b.categorization_setup
											-----------------------
											--,b.adc2classification_rule as adc2classification_rule_original,
											--b.spc2classification_rule as spc2classification_rule_original
									from
											w_incomming as a
									left
									join	(
											select
													t1.*
											from
														(
														select
																id as id_cm_setup,
																ldsity2target_variable,
																adc2classification_rule,
																spc2classification_rule,
																categorization_setup
														from
																target_data.cm_ldsity2target2categorization_setup
														where
																ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
														) as t1
											inner
											join		w_incomming as t2
												
														on	t1.ldsity2target_variable = t2.ldsity2target_variable
														and	(											
																target_data.fn_array_compare
																(
																	(case when t1.adc2classification_rule is null then array[0] else t1.adc2classification_rule end),
																	(case when t2.adc2classification_rule is null then array[0] else t2.adc2classification_rule end)
																)
															)
														and	(											
																target_data.fn_array_compare
																(
																	(case when t1.spc2classification_rule is null then array[0] else t1.spc2classification_rule end),
																	(case when t2.spc2classification_rule is null then array[0] else t2.spc2classification_rule end)
																)
															)
											) as b
									on
											a.ldsity2target_variable = b.ldsity2target_variable
											and			
											(												
												target_data.fn_array_compare
												(
													(case when a.adc2classification_rule is null then array[0] else a.adc2classification_rule end),
													(case when b.adc2classification_rule is null then array[0] else b.adc2classification_rule end)
												)
											)
											and
											(												
												target_data.fn_array_compare
												(
													(case when a.spc2classification_rule is null then array[0] else a.spc2classification_rule end),
													(case when b.spc2classification_rule is null then array[0] else b.spc2classification_rule end)
												)
											)	
									)
				'
			);

			_string_check_3 := concat(
				'
				,w_check_3 as		(
									select * from w_check_2 union all
									select distinct c.group_id, c.group_id_poradi, c.area_domain_category,
									c.sub_population_category, c.ldsity2target_variable,
									c.adc2classification_rule, c.spc2classification_rule,
									c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
									from
											(
											select
													1 as group_id,
													group_id_poradi,
													area_domain_category,
													sub_population_category,
													',_array_id[1],' as ldsity2target_variable,
													adc2classification_rule,
													null::integer[] as spc2classification_rule,
													null::integer as id_cm_setup,
													null::integer as categorization_setup,
													check_exists_group_id_1
											from
													w_check_2 where check_exists_group_id_1 = 1
											) as c
									)				
			');			
		end if;
		-------------------------------------------------------------
		
		/*
		-------------------------------------------------------------
		-------------------------------------------------------------
		_s4da_p1 := 'select t0.categorization_setup';
		_s4da_p3 := ' from (select distinct categorization_setup from w_database) as t0 ';

		_s4ia_p1 := 'select t0.group_id_poradi';
		_s4ia_p3 := ' from (select distinct group_id_poradi from w_incomming) as t0 ';
	
		_s4result_p1 :=	'
						select
								a.*,
								b.categorization_setup
						from
								w_incomming_agg as a
						inner
						join	w_database_agg as b
						
						on ';	
	

		for i in 1..array_length(_array_id,1)
		loop
			if i = 1
			then
				_s4da_p2 := concat('
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');

				if _variant in (3,4)
				then				
					_s4ia_p2 := concat('
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule4insert as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');
				else
					_s4ia_p2 := concat('
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');				
				end if;								
								
				_s4da_p4 := concat(' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat('
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],' and
									((case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end)
									= (case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)) and
									((case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end)
									= (case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end))
								');
			else
				_s4da_p2 := concat(_s4da_p2,
									'
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');
								
				if _variant in (3,4)
				then				
					_s4ia_p2 := concat(_s4ia_p2,
										'
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule4insert as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');
				else
					_s4ia_p2 := concat(_s4ia_p2,
										'
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');				
				end if;								
								
				_s4da_p4 := concat(_s4da_p4,
									' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(_s4ia_p4,
									' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat(_s4result_p2,' and 
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],' and
									((case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end)
									= (case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)) and
									((case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end)
									= (case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end))
								');
			end if;
		end loop;
	
		_string4database_agg := _s4da_p1 || _s4da_p2 || _s4da_p3 || _s4da_p4;
		_string4incomming_agg := _s4ia_p1 || _s4ia_p2 || _s4ia_p3 || _s4ia_p4;
		_string4result := _s4result_p1 || _s4result_p2;
		-------------------------------------------------------------
		-------------------------------------------------------------
		*/

		_string_check_2 := 
		'
		,w_check_2 as		(
							select
									w_check_1.*,
									case
										when categorization_setup is null
											then
												case
													when	(
															select min(t.group_id) from w_check_1 as t
															where t.categorization_setup is null
															and t.group_id_poradi = w_check_1.group_id_poradi
															)
															= 1
													then 0
													else 1
												end 
											else 0
									end as check_exists_group_id_1
							from
									w_check_1
							)		
		';


		_string_check_4 :=
		'
		,w_check_4 as		(					
							select
									(select coalesce(max(id),0) from target_data.t_categorization_setup) + 
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as new_categorization_setup,
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as pozice,
									w_check_3.*
							from
									w_check_3
							where
									categorization_setup is null
							order
									by new_categorization_setup, pozice, group_id
							)		
		';


		_string_result :=
		'
		select array_agg(tt.categorization_setup order by tt.categorization_setup)
		from
			(					
			select distinct w_check_3.categorization_setup from w_check_3 where w_check_3.categorization_setup is not null
			union
			all
			select t.categorization_setup from (select distinct w_check_4.new_categorization_setup, null::integer as categorization_setup from w_check_4) as t
			) as tt;		
		';	


		_res := _query_res || _string_inner_join || _string_incomming_0 || _string_incomming || _string_check_1 || _string_check_2 || _string_check_3 || _string_check_4 || _string_result;
		
		execute ''||_res||'' into _result;

		return _result;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_get_categorization_setup(integer,integer[],integer[],integer[][],integer[],integer[][]) IS
'The function gets attribute configurations.';

grant execute on function target_data.fn_get_categorization_setup(integer,integer[],integer[],integer[][],integer[],integer[][]) to public;
-- </function>



-- <function name="fn_save_categorization_setup" schema="target_data" src="functions/fn_save_categorization_setup.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_save_categorization_setup
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_categorization_setup(integer, integer[], integer[], integer[][], integer[], integer[][]) CASCADE;

create or replace function target_data.fn_save_categorization_setup
(
	_target_variable		integer,
	_target_variable_cm		integer[],
	_area_domain			integer[],
	_area_domain_object		integer[][],
	_sub_population			integer[],
	_sub_population_object	integer[][]
)
returns void
as
$$
declare
		_array_id							integer[];
		_array_id4check						integer[];
		_lot_100							integer;
		_lot_200							integer;
		_variant							integer;	
		_area_domain4input					integer[];
		_area_domain_object4input			integer[];
		_sub_population4input				integer[];
		_sub_population_object4input		integer[];	
		_area_domain4input_text				text;
		_area_domain_object4input_text		text;
		_sub_population4input_text			text;
		_sub_population_object4input_text	text;
		_string4attr						text;
		_query_res							text;
		_s4ij_p1							text;
		_s4ij_p2							text;
		_s4ij_p3							text;
		_s4ij_p4							text;
		_s4ij_p5							text;
		_string4incomming					text;
		_string4inner_join					text;
		_array_id_200						integer[];
		_string_inner_join					text;
		_string_incomming_0					text;
		_string_incomming					text;
		_string_check_1						text;
		_string_check_2						text;
		_string_check_3						text;
		_string_check_4						text;
		_string_insert_1					text;
		_string_insert_2					text;
		/*
		_s4da_p1							text;
		_s4da_p2							text;
		_s4da_p3							text;
		_s4da_p4							text;
		_s4ia_p1							text;
		_s4ia_p2							text;
		_s4ia_p3							text;
		_s4ia_p4							text;
		_s4result_p1						text;
		_s4result_p2						text;
		_string4database_agg				text;
		_string4incomming_agg				text;	
		_string4result						text;
		_string_agg_and_result				text;
		*/
		_res								text;

		_use_negative_id_bc					boolean;
		_use_negative_ad_4input_text		text;
		_classification_type_ad				integer;
		_use_negative_ad_4input_text_i		text;
		_use_negative_ad_4input_text_join	text;
		_use_negative_sp_4input_text		text;
		_classification_type_sp				integer;
		_use_negative_sp_4input_text_i		text;
		_use_negative_sp_4input_text_join	text;		
begin
		if _target_variable is null
		then
			raise exception 'Error 01: fn_save_categorization_setup: The input argument _target_variable must not be NULL !';
		end if;

		if _target_variable_cm is null
		then
			raise exception 'Error 02: fn_save_categorization_setup: The input argument _target_variable_cm must not be NULL !';
		end if;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where target_variable = _target_variable
		into
				_array_id;
	
		select
				array_agg(id order by ldsity_object_type, id) as id
		from
				target_data.cm_ldsity2target_variable where id in (select unnest(_target_variable_cm))
		into
				_array_id4check;			

		if _array_id != _array_id4check
			then raise exception 'Error 03: fn_save_categorization_setup: The internal argument _array_id = % and internal argument _array_id4check = % are not the same!',_array_id, _array_id4check;
		end if;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 04: fn_save_categorization_setup: This variant is not implemented yet!';
		end if;

		for bc in 1..array_length(_array_id,1)
		loop	
			-----------------------------------------
			-----------------------------------------
			if _area_domain is null and _sub_population is null
			then
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is null
			then
				if _variant in (3,4)
				then
					with
					w1 as (select unnest(_area_domain_object[1:1]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;				
				else		
					with
					w1 as (select unnest(_area_domain_object[bc:bc]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				end if;			
			
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;
			
				_sub_population4input := null::integer[];
				_sub_population_object4input := null::integer[];
			end if;
			------
			if _area_domain is null and _sub_population is not null
			then		
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
			
				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			
				_area_domain4input := null::integer[];
				_area_domain_object4input := null::integer[];
			end if;
			-------
			if _area_domain is not null and _sub_population is not null
			then
				if _variant in (3,4)
				then
					with
					w1 as (select unnest(_area_domain_object[1:1]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				else
					with
					w1 as (select unnest(_area_domain_object[bc:bc]) as res),
					w2 as (select row_number() over() as new_id, res from w1),
					w3 as (select new_id, case when res is null then 0 else res end as res from w2)
					select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
					into _area_domain_object4input;
				end if;				
			
				with
				w1 as (select unnest(_sub_population_object[bc:bc]) as res),
				w2 as (select row_number() over() as new_id, res from w1),
				w3 as (select new_id, case when res is null then 0 else res end as res from w2)
				select array_agg(w3.res order by w3.new_id) from w3 where w3.res is distinct from 0
				into _sub_population_object4input;
	
				if _area_domain_object4input is null
				then
					_area_domain4input := null::integer[];
				else
					_area_domain4input := _area_domain;
				end if;			

				if _sub_population_object4input is null
				then
					_sub_population4input := null::integer[];
				else
					_sub_population4input := _sub_population;
				end if;			
			end if;
			-----------------------------------------
			-----------------------------------------
			
			if _area_domain4input is null
			then
				_area_domain4input_text := 'null::integer[]';
			else
				_area_domain4input_text := replace(replace(concat('array[',_area_domain4input,']'),'{',''),'}','');
			end if;
		
			if _area_domain_object4input is null
			then
				_area_domain_object4input_text := 'null::integer[]';
			else
				_area_domain_object4input_text := replace(replace(concat('array[',_area_domain_object4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population4input is null
			then
				_sub_population4input_text := 'null::integer[]';
			else
				_sub_population4input_text := replace(replace(concat('array[',_sub_population4input,']'),'{',''),'}','');
			end if;
		
			if _sub_population_object4input is null
			then
				_sub_population_object4input_text := 'null::integer[]';
			else
				_sub_population_object4input_text := replace(replace(concat('array[',_sub_population_object4input,']'),'{',''),'}','');
			end if;

			-----------------------------------------
			-----------------------------------------
			-----------------------------------------
			_use_negative_id_bc := (select cmlv.use_negative from target_data.cm_ldsity2target_variable as cmlv where cmlv.id = _array_id[bc]);

			if _use_negative_id_bc is null
			then
				raise exception 'Error 05: fn_get_categorization_setup: For target_variable = % and target_variable_cm = % is value use_negative NULL!',_target_variable, _array_id[bc];
			end if;

			if _use_negative_id_bc = FALSE
			then
				if _area_domain4input is null or _area_domain_object4input is null
				then
					_use_negative_ad_4input_text := 'null::boolean[]';
				else
					_use_negative_ad_4input_text := (select replace(replace(concat('array[',(select array_agg(t.res) from (select 'false' as res from generate_series(1,array_length(_area_domain4input,1))) as t),']'),'{',''),'}',''));
				end if;

				if _sub_population4input is null or _sub_population_object4input is null
				then
					_use_negative_sp_4input_text := 'null::boolean[]';
				else
					_use_negative_sp_4input_text := (select replace(replace(concat('array[',(select array_agg(t.res) from (select 'false' as res from generate_series(1,array_length(_sub_population4input,1))) as t),']'),'{',''),'}',''));
				end if;
			else
				-- _use_negative_id_bc = TRUE

				-- AD --
				if _area_domain4input is null
				then
					_use_negative_ad_4input_text := 'null::boolean[]';
				else
					-- get classification_type for AD
					for ict in 1..array_length(_area_domain4input,1)
					loop
						select cad.classification_type from target_data.c_area_domain as cad
						where cad.id = _area_domain4input[ict]
						into _classification_type_ad;

						if _classification_type_ad is null
						then
							raise exception 'Error 06: fn_get_categorization_setup: For	target_variable = %, target_variable_cm = % and area_domain = % not exists any value classification_type in c_area_domain table!',_target_variable, _array_id[bc], _area_domain4input[ict];
						end if;

						if _classification_type_ad = 100
						then
							_use_negative_ad_4input_text_i := 'false';
						else
							_use_negative_ad_4input_text_i := 'true';
						end if;

						if ict = 1
						then
							_use_negative_ad_4input_text_join := _use_negative_ad_4input_text_i;
						else
							_use_negative_ad_4input_text_join := concat(_use_negative_ad_4input_text_join,',',_use_negative_ad_4input_text_i);
						end if;
					end loop;

					_use_negative_ad_4input_text := concat('array[',_use_negative_ad_4input_text_join,']');

				end if;
				
				-- SP --
				if _sub_population4input is null
				then
					_use_negative_sp_4input_text := 'null::boolean[]';
				else
					-- get classification_type for SP
					for ict in 1..array_length(_sub_population4input,1)
					loop
						select csp.classification_type from target_data.c_sub_population as csp
						where csp.id = _sub_population4input[ict]
						into _classification_type_sp;

						if _classification_type_sp is null
						then
							raise exception 'Error 07: fn_get_categorization_setup: For	target_variable = %, target_variable_cm = % and sub_population = % not exists any value classification_type in c_sub_population table!',_target_variable, _array_id[bc], _sub_population4input[ict];
						end if;

						if _classification_type_sp = 100
						then
							_use_negative_sp_4input_text_i := 'false';
						else
							_use_negative_sp_4input_text_i := 'true';
						end if;

						if ict = 1
						then
							_use_negative_sp_4input_text_join := _use_negative_sp_4input_text_i;
						else
							_use_negative_sp_4input_text_join := concat(_use_negative_sp_4input_text_join,',',_use_negative_sp_4input_text_i);
						end if;
					end loop;

					_use_negative_sp_4input_text := concat('array[',_use_negative_sp_4input_text_join,']');

				end if;
					
			end if;
			-----------------------------------------
			-----------------------------------------
			-----------------------------------------			
			
			_string4attr := concat('
			select
					area_domain_category,
					sub_population_category,
					adc2classification_rule,
					spc2classification_rule
			from
					target_data.fn_get_classification_rules_id4categories(',_area_domain4input_text,',',_area_domain_object4input_text,',',_sub_population4input_text,',',_sub_population_object4input_text,',',_use_negative_ad_4input_text,',',_use_negative_sp_4input_text,')
			');
		
			if bc = 1
			then
				_query_res := concat('with w_',bc,' as (',_string4attr,')');
			else
				_query_res := concat(_query_res,',w_',bc,' as (',_string4attr,')');
			end if;
		end loop;
		
		-------------------------------------------------------------
		-------------------------------------------------------------
		if _variant in (1,2)
		then
		
			_s4ij_p1 := 'select row_number() over() as group_id_poradi';
			_s4ij_p3 := ' from ';
		
			for bc in 1..array_length(_array_id,1)
			loop
				if bc = 1
				then
			
					_s4ij_p2 := concat('
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.area_domain_category
										,t',bc,'.sub_population_category
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'');
				
					_string4incomming := concat(
						'
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc,'.ldsity2target_variable as ldsity2target_variable_',_array_id[bc],'
										,t',bc,'.adc2classification_rule as adc2classification_rule_',_array_id[bc],'
										,t',bc,'.spc2classification_rule as spc2classification_rule_',_array_id[bc],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id[bc],' as ldsity2target_variable,* from w_',bc,') as t',bc,'
										on
											t1.area_domain_category = t',bc,'.area_domain_category and
											t1.sub_population_category = t',bc,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id[bc],' as ldsity2target_variable,
								adc2classification_rule_',_array_id[bc],' as adc2classification_rule,
								spc2classification_rule_',_array_id[bc],' as spc2classification_rule
						from
								w_inner_join
						');
				
				end if;
			end loop;
			
				_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4;
		end if;		
		-------------------------------------------------------------
		if _variant in (3,4)
		then	
			_s4ij_p1 := concat(
				'
				select
					row_number() over() as group_id_poradi,
					a.*,
					b.*
				from
					(
					select
							',_array_id[1],' as ldsity2target_variable_',_array_id[1],',
							area_domain_category as area_domain_category_',_array_id[1],',
							adc2classification_rule as adc2classification_rule_',_array_id[1],',
							spc2classification_rule as spc2classification_rule_',_array_id[1],'
					from w_1
					) as a
				inner
				join
					(select ');
				
			_s4ij_p3 := ' from ';
		
			_s4ij_p5 := concat('
						) as b
					on
						a.area_domain_category_',_array_id[1],' = b.area_domain_category');
					
			_string4incomming := concat(
				'
				select
						1 as group_id,
						group_id_poradi,
						area_domain_category,
						sub_population_category,
						ldsity2target_variable_',_array_id[1],' as ldsity2target_variable,
						adc2classification_rule_',_array_id[1],' as adc2classification_rule,
						spc2classification_rule_',_array_id[1],' as spc2classification_rule
				from
						w_inner_join
				');					
				
			_array_id_200 := array_remove(_array_id, _array_id[1]);
			
			for bc_200 in 1..array_length(_array_id_200,1)
			loop
				if bc_200 = 1
				then				
					_s4ij_p2 := concat('
										t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.area_domain_category
										,t',bc_200,'.sub_population_category
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat('(select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'');
				
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');				
				
				else
					_s4ij_p2 := concat(_s4ij_p2,
										'
										,t',bc_200,'.ldsity2target_variable as ldsity2target_variable_',_array_id_200[bc_200],'
										,t',bc_200,'.adc2classification_rule as adc2classification_rule_',_array_id_200[bc_200],'
										,t',bc_200,'.spc2classification_rule as spc2classification_rule_',_array_id_200[bc_200],'
										');
									
					_s4ij_p4 := concat(_s4ij_p4,
										' inner join (select ',_array_id_200[bc_200],' as ldsity2target_variable,* from w_',bc_200+1,') as t',bc_200,'
										on
											t1.area_domain_category = t',bc_200,'.area_domain_category and
											t1.sub_population_category = t',bc_200,'.sub_population_category 
										');
									
					_string4incomming := concat(_string4incomming,
						' union all
						select
								',bc_200+1,' as group_id,
								group_id_poradi,
								area_domain_category,
								sub_population_category,
								ldsity2target_variable_',_array_id_200[bc_200],' as ldsity2target_variable,
								adc2classification_rule_',_array_id_200[bc_200],' as adc2classification_rule,
								spc2classification_rule_',_array_id_200[bc_200],' as spc2classification_rule
						from
								w_inner_join
						');
					
				end if;
			end loop;
			
			_string4inner_join := _s4ij_p1 || _s4ij_p2 || _s4ij_p3 || _s4ij_p4 || _s4ij_p5;
		
		end if;
		-------------------------------------------------------------
		-------------------------------------------------------------
		_string_inner_join := concat(',w_inner_join as (',_string4inner_join,')');
		_string_incomming_0 := concat(',w_incomming_0 as (',_string4incomming,')');
		-------------------------------------------------------------
		if _variant in (3,4)
		then
			_string_incomming := concat(
				'
				,w_incomming as	(
								select
										group_id,
										group_id_poradi,
										------------------------------------
										area_domain_category,
										case
											when (array_remove(area_domain_category,0)) = array[]::integer[]
											then array[0]
											else (array_remove(area_domain_category,0))
										end as area_domain_category_upr,
										------------------------------------
										sub_population_category,
										case
											when (array_remove(sub_population_category,0)) = array[]::integer[]
											then array[0]
											else (array_remove(sub_population_category,0))
										end as sub_population_category_upr,
										------------------------------------
										ldsity2target_variable,
										------------------------------------
										case
											when adc2classification_rule is null
											then array[0]
											else adc2classification_rule
										end as adc2classification_rule,
										case
											when spc2classification_rule is null
											then array[0]
											else spc2classification_rule
										end as spc2classification_rule,
										------------------------------------
										case
											when ldsity2target_variable in (',array_to_string(_array_id_200,','),')
											then null::integer[]
											else adc2classification_rule
										end as adc2classification_rule4insert,
										spc2classification_rule as spc2classification_rule4insert
								from
									w_incomming_0
								)
				');


			_string_check_1 := concat(
				'
				,w_check_1 as		(
									select
											a.*,
											b.id_cm_setup,
											b.categorization_setup		
									from
											w_incomming as a
									left
									join	(								
											select
													tt1.*
											from
														(
														select
																t1.*,
																----------------------
																t2.adc2classification_rule4join
																----------------------
														from
															(
																	select
																		z1.*,
																		-----------------------
																		case
																			when z2.sub_population_category4join is null
																			then array[0]
																			else z2.sub_population_category4join
																		end as sub_population_category4join
																		-----------------------
																	from
																		(
																		select
																				id as id_cm_setup,
																				ldsity2target_variable,
																				-------------------------------------
																				case
																					when adc2classification_rule is null
																					then array[0]
																					else adc2classification_rule
																				end as adc2classification_rule,
																				-------------------------------------
																				spc2classification_rule,
																				categorization_setup
																		from
																				target_data.cm_ldsity2target2categorization_setup
																		where
																				ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
																		) as z1
																	left join
																		(
																		select
																			distinct categorization_setup, sub_population_category4join
																		from
																			(
																			select
																					categorization_setup,
																					case when spc2classification_rule is null then array[0]
																					else (select id_category from target_data.fn_get_category4classification_rule_id(''spc''::varchar,spc2classification_rule))
																					end as sub_population_category4join
																			from
																					target_data.cm_ldsity2target2categorization_setup
																			where
																					ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
																			) as t
																		where
																				t.sub_population_category4join is distinct from array[0]
																		) as z2
																	on
																		z1.categorization_setup = z2.categorization_setup
															) as t1
															inner join
															(
															select
																	categorization_setup,
																	case when adc2classification_rule is null then array[0] else adc2classification_rule end as adc2classification_rule4join
															from
																	target_data.cm_ldsity2target2categorization_setup
															where
																	ldsity2target_variable = ',_array_id[1],'
															) as t2
														on t1.categorization_setup = t2.categorization_setup
														)
															as tt1
											inner join
														w_incomming as tt2
											on
													tt1.ldsity2target_variable = tt2.ldsity2target_variable
												and	(target_data.fn_array_compare(tt1.adc2classification_rule4join,tt2.adc2classification_rule))
												and	(target_data.fn_array_compare(tt1.sub_population_category4join,tt2.sub_population_category_upr))
											) as b
									on	
											a.ldsity2target_variable = b.ldsity2target_variable
										and	(target_data.fn_array_compare(a.adc2classification_rule,b.adc2classification_rule4join))
										and	(target_data.fn_array_compare(a.sub_population_category_upr,b.sub_population_category4join))				
									)
			');

			_string_check_3 := concat(
				'
				,w_check_3 as		(
									select * from w_check_2 union all
									select distinct c.group_id, c.group_id_poradi,
									------------------------
									c.area_domain_category,
									c.area_domain_category_upr,
									c.sub_population_category,
									c.sub_population_category_upr,
									--------------------------
									c.ldsity2target_variable,
									c.adc2classification_rule,
									c.spc2classification_rule,
									c.adc2classification_rule4insert,
									c.spc2classification_rule4insert,
									c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
									from
											(
											select
													1 as group_id,
													group_id_poradi,
													--------------------------
													area_domain_category,
													area_domain_category_upr,
													sub_population_category,
													sub_population_category_upr,
													---------------------------
													',_array_id[1],' as ldsity2target_variable,
													adc2classification_rule,
													null::integer[] as spc2classification_rule,
													adc2classification_rule4insert,
													spc2classification_rule4insert,
													null::integer as id_cm_setup,
													null::integer as categorization_setup,
													check_exists_group_id_1
											from
													w_check_2 where check_exists_group_id_1 = 1
											) as c
									)				
			');

			_string_insert_2 :=
			'
			insert into target_data.cm_ldsity2target2categorization_setup
			(ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup)
			select
					w_check_4.ldsity2target_variable,
					w_check_4.adc2classification_rule4insert,
					w_check_4.spc2classification_rule4insert,
					w_check_4.new_categorization_setup
			from
					w_check_4
			order
					by
						w_check_4.new_categorization_setup,
						w_check_4.pozice,
						w_check_4.group_id;
			';
		else
			_string_incomming := concat(
				'
				,w_incomming as	(
								select
										group_id,
										group_id_poradi,
										area_domain_category,
										sub_population_category,
										ldsity2target_variable,
										adc2classification_rule,
										spc2classification_rule
								from
									w_incomming_0
								)
				');

			_string_check_1 := concat(
				'
				,w_check_1 as		(
									select
											a.*,
											b.id_cm_setup,
											b.categorization_setup
											-----------------------
											--,b.adc2classification_rule as adc2classification_rule_original,
											--b.spc2classification_rule as spc2classification_rule_original
									from
											w_incomming as a
									left
									join	(
											select
													t1.*
											from
														(
														select
																id as id_cm_setup,
																ldsity2target_variable,
																adc2classification_rule,
																spc2classification_rule,
																categorization_setup
														from
																target_data.cm_ldsity2target2categorization_setup
														where
																ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
														) as t1
											inner
											join		w_incomming as t2
												
														on	t1.ldsity2target_variable = t2.ldsity2target_variable
														and	(											
																target_data.fn_array_compare
																(
																	(case when t1.adc2classification_rule is null then array[0] else t1.adc2classification_rule end),
																	(case when t2.adc2classification_rule is null then array[0] else t2.adc2classification_rule end)
																)
															)
														and	(											
																target_data.fn_array_compare
																(
																	(case when t1.spc2classification_rule is null then array[0] else t1.spc2classification_rule end),
																	(case when t2.spc2classification_rule is null then array[0] else t2.spc2classification_rule end)
																)
															)
											) as b
									on
											a.ldsity2target_variable = b.ldsity2target_variable
											and			
											(												
												target_data.fn_array_compare
												(
													(case when a.adc2classification_rule is null then array[0] else a.adc2classification_rule end),
													(case when b.adc2classification_rule is null then array[0] else b.adc2classification_rule end)
												)
											)
											and
											(												
												target_data.fn_array_compare
												(
													(case when a.spc2classification_rule is null then array[0] else a.spc2classification_rule end),
													(case when b.spc2classification_rule is null then array[0] else b.spc2classification_rule end)
												)
											)
									)
				'
			);

			_string_check_3 := concat(
				'
				,w_check_3 as		(
									select * from w_check_2 union all
									select distinct c.group_id, c.group_id_poradi, c.area_domain_category,
									c.sub_population_category, c.ldsity2target_variable,
									c.adc2classification_rule, c.spc2classification_rule,
									c.id_cm_setup, c.categorization_setup, c.check_exists_group_id_1
									from
											(
											select
													1 as group_id,
													group_id_poradi,
													area_domain_category,
													sub_population_category,
													',_array_id[1],' as ldsity2target_variable,
													adc2classification_rule,
													null::integer[] as spc2classification_rule,
													null::integer as id_cm_setup,
													null::integer as categorization_setup,
													check_exists_group_id_1
											from
													w_check_2 where check_exists_group_id_1 = 1
											) as c
									)				
			');

			_string_insert_2 :=
			'
			insert into target_data.cm_ldsity2target2categorization_setup
			(ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup)
			select
					w_check_4.ldsity2target_variable,
					w_check_4.adc2classification_rule,
					w_check_4.spc2classification_rule,
					w_check_4.new_categorization_setup
			from
					w_check_4
			order
					by
						w_check_4.new_categorization_setup,
						w_check_4.pozice,
						w_check_4.group_id;
			';			
		end if;
		-------------------------------------------------------------
		
		/*
		-------------------------------------------------------------
		-------------------------------------------------------------
		_s4da_p1 := 'select t0.categorization_setup';
		_s4da_p3 := ' from (select distinct categorization_setup from w_database) as t0 ';

		_s4ia_p1 := 'select t0.group_id_poradi';
		_s4ia_p3 := ' from (select distinct group_id_poradi from w_incomming) as t0 ';
	
		_s4result_p1 :=	'
						select
								a.*,
								b.categorization_setup
						from
								w_incomming_agg as a
						inner
						join	w_database_agg as b
						
						on ';	
	

		for i in 1..array_length(_array_id,1)
		loop
			if i = 1
			then
				_s4da_p2 := concat('
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');

				if _variant in (3,4)
				then				
					_s4ia_p2 := concat('
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule4insert as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');
				else
					_s4ia_p2 := concat('
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');				
				end if;								
								
				_s4da_p4 := concat(' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat('
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],'
									and			
									(												
										target_data.fn_array_compare
										(
											(case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end),
											(case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)
										)
									)
									and
									(												
										target_data.fn_array_compare
										(
											(case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end),
											(case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end)
										)
									)
								');
			else
				_s4da_p2 := concat(_s4da_p2,
									'
									,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
									,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
									,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
									');
								
				if _variant in (3,4)
				then				
					_s4ia_p2 := concat(_s4ia_p2,
										'
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule4insert as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');
				else
					_s4ia_p2 := concat(_s4ia_p2,
										'
										,t',i,'.ldsity2target_variable as ldsity2target_variable_',_array_id[i],'
										,t',i,'.adc2classification_rule as adc2classification_rule_',_array_id[i],'
										,t',i,'.spc2classification_rule as spc2classification_rule_',_array_id[i],'
										');				
				end if;								
								
				_s4da_p4 := concat(_s4da_p4,
									' inner join (select * from w_database where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.categorization_setup = t',i,'.categorization_setup 
									');
								
				_s4ia_p4 := concat(_s4ia_p4,
									' inner join (select * from w_incomming where ldsity2target_variable = ',_array_id[i],') as t',i,'
									on
										t0.group_id_poradi = t',i,'.group_id_poradi 
									');
								
				_s4result_p2 := concat(_s4result_p2,' and 
									a.ldsity2target_variable_',_array_id[i],' = b.ldsity2target_variable_',_array_id[i],'
									and			
									(												
										target_data.fn_array_compare
										(
											(case when a.adc2classification_rule_',_array_id[i],' is null then array[0] else a.adc2classification_rule_',_array_id[i],' end),
											(case when b.adc2classification_rule_',_array_id[i],' is null then array[0] else b.adc2classification_rule_',_array_id[i],' end)
										)
									)
									and
									(												
										target_data.fn_array_compare
										(
											(case when a.spc2classification_rule_',_array_id[i],' is null then array[0] else a.spc2classification_rule_',_array_id[i],' end),
											(case when b.spc2classification_rule_',_array_id[i],' is null then array[0] else b.spc2classification_rule_',_array_id[i],' end)
										)
									)									
								');
			end if;
		end loop;
	
		_string4database_agg := _s4da_p1 || _s4da_p2 || _s4da_p3 || _s4da_p4;
		_string4incomming_agg := _s4ia_p1 || _s4ia_p2 || _s4ia_p3 || _s4ia_p4;
		_string4result := _s4result_p1 || _s4result_p2;
		-------------------------------------------------------------
		-------------------------------------------------------------
		*/

		_string_check_2 := 
		'
		,w_check_2 as		(
							select
									w_check_1.*,
									case
										when categorization_setup is null
											then
												case
													when	(
															select min(t.group_id) from w_check_1 as t
															where t.categorization_setup is null
															and t.group_id_poradi = w_check_1.group_id_poradi
															)
															= 1
													then 0
													else 1
												end 
											else 0
									end as check_exists_group_id_1
							from
									w_check_1
							)		
		';


		_string_check_4 :=
		'
		,w_check_4 as		(					
							select
									(select coalesce(max(id),0) from target_data.t_categorization_setup) + 
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as new_categorization_setup,
									array_position((select array_agg(t.group_id_poradi order by t.group_id_poradi) as group_id_array from
									(select distinct group_id_poradi from w_check_3 where categorization_setup is null) as t),group_id_poradi) as pozice,
									w_check_3.*
							from
									w_check_3
							where
									categorization_setup is null
							order
									by new_categorization_setup, pozice, group_id
							)		
		';

		_string_insert_1 :=
		'
		,w_insert_1 as		(
							insert into target_data.t_categorization_setup(id)
							select distinct new_categorization_setup from w_check_4 order by new_categorization_setup
							returning id
							)		
		';		
		
		/*
		_string_agg_and_result := concat(
		'
		,w_database as		(
							select id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
							from target_data.cm_ldsity2target2categorization_setup where ldsity2target_variable in
							(select id from target_data.cm_ldsity2target_variable where target_variable = ',_target_variable,')
							union all
							select id,ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup
							from w_insert_2
							)
		,w_database_agg as	('||_string4database_agg||')
		,w_incomming_agg as	('||_string4incomming_agg||')
		,w_result as		('||_string4result||')
		select
			categorization_setup
		from
			w_result order by categorization_setup;
		');
		*/

		_res := _query_res || _string_inner_join || _string_incomming_0 || _string_incomming || _string_check_1 || _string_check_2 || _string_check_3 || _string_check_4 || _string_insert_1 || _string_insert_2 /*|| _string_agg_and_result*/;
		
		execute ''||_res||'';
end;
$$
language plpgsql
volatile
cost 100
security invoker;

COMMENT ON FUNCTION target_data.fn_save_categorization_setup(integer,integer[],integer[],integer[][],integer[],integer[][]) IS
'The function sets attribute configurations.';

grant execute on function target_data.fn_save_categorization_setup(integer,integer[],integer[],integer[][],integer[],integer[][]) to public;
-- </function>



-- <function name="fn_trg_check_cm_adc2classification_rule" schema="target_data" src="functions/fn_trg_check_cm_adc2classification_rule.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_adc2classification_rule
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_adc2classification_rule();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_adc2classification_rule() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF NOT(new.classification_rule = any(array['EXISTS','NOT EXISTS']))
		THEN
			IF	(
				SELECT count(*) FROM target_data.cm_adc2classrule2panel_refyearset
				WHERE adc2classification_rule = new.id
				) = 0
			THEN
				RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_adc2classification_rule: For new inserted classification rule: [area_domain_category = %, ldsity_object = %, classification_rule = "%", use_negative = %], not exists any record in table cm_adc2classrule2panel_refyearset!', new.area_domain_category, new.ldsity_object, new.classification_rule, new.use_negative;
			END IF;
		END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_adc2classification_rule() IS
'This trigger function controls that for new inserted record into cm_adc2classification_rule table exists at least one record in cm_adc2classrule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_adc2classification_rule() TO public;
-- </function>



-- <function name="fn_trg_check_cm_adc2classification_rule" schema="target_data" src="functions/fn_trg_check_cm_adc2classification_rule.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_spc2classification_rule
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_spc2classification_rule();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_spc2classification_rule() 
RETURNS TRIGGER AS 
$$
DECLARE
BEGIN
		IF NOT(new.classification_rule = any(array['EXISTS','NOT EXISTS']))
		THEN
			IF	(
				SELECT count(*) FROM target_data.cm_spc2classrule2panel_refyearset
				WHERE spc2classification_rule = new.id
				) = 0
			THEN
				RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_spc2classification_rule: For new inserted classification rule: [sub_population_category = %, ldsity_object = %, classification_rule = "%", use_negative = %], not exists any record in table cm_adc2classrule2panel_refyearset!', new.sub_population_category, new.ldsity_object, new.classification_rule, new.use_negative;
			END IF;
		END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_spc2classification_rule() IS
'This trigger function controls that for new inserted record into cm_spc2classification_rule table exists at least one record in cm_spc2classrule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_spc2classification_rule() TO public;
-- </function>


