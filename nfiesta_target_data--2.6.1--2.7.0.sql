--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_import_data" schema="target_data" src="functions/fn_import_data.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_data(character varying) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_import_data(_schema character varying)
RETURNS text
AS
$$
DECLARE
		_res	text;
BEGIN
	-- Data for Name: c_ldsity_object; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity_object(id, label, description, label_en, description_en, table_name, upper_object, areal_or_population, column4upper_object, filter)
	select id, label, description, label_en, description_en, table_name, upper_object, areal_or_population, column4upper_object, filter from '||_schema||'.c_ldsity_object order by id';

	-- Data for Name: c_unit_of_measure; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_unit_of_measure(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_unit_of_measure order by id';

	-- Data for Name: c_definition_variant; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_definition_variant(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_definition_variant order by id';	

	-- Data for Name: c_area_domain; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_area_domain order by id';

	-- Data for Name: c_area_domain_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_area_domain_category(id, label, description, label_en, description_en, area_domain)
	select id, label, description, label_en, description_en, area_domain from '||_schema||'.c_area_domain_category order by id';

	-- Data for Name: cm_adc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_adc2classification_rule(area_domain_category, ldsity_object, classification_rule)
	select area_domain_category, ldsity_object, classification_rule from '||_schema||'.cm_adc2classification_rule order by id';	

	-- Data for Name: c_sub_population; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_sub_population order by id';

	-- Data for Name: c_sub_population_category; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_sub_population_category(id, label, description, label_en, description_en, sub_population)
	select id, label, description, label_en, description_en, sub_population from '||_schema||'.c_sub_population_category order by id';

	-- Data for Name: cm_spc2classification_rule; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_spc2classification_rule(sub_population_category, ldsity_object, classification_rule)
	select sub_population_category, ldsity_object, classification_rule from '||_schema||'.cm_spc2classification_rule order by id';	

	-- Data for Name: c_ldsity; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_ldsity(id, label, description, label_en, description_en, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant)
	select id, label, description, label_en, description_en, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant from '||_schema||'.c_ldsity order by id';

	-- Data for Name: t_adc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_adc_hierarchy(variable_superior, variable, dependent)
	select variable_superior, variable, dependent from '||_schema||'.t_adc_hierarchy order by id';

	-- Data for Name: t_spc_hierarchy; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.t_spc_hierarchy(variable_superior, variable, dependent)
	select variable_superior, variable, dependent from '||_schema||'.t_spc_hierarchy order by id';

	-- Data for Name: c_version; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.c_version(id, label, description, label_en, description_en)
	select id, label, description, label_en, description_en from '||_schema||'.c_version order by id';	

	-- Data for Name: cm_ldsity2panel_refyearset_version; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_ldsity2panel_refyearset_version(ldsity, refyearset2panel, version)
	select ldsity, refyearset2panel, version from '||_schema||'.cm_ldsity2panel_refyearset_version order by id';

	-- Data for Name: cm_adc2classrule2panel_refyearset; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_adc2classrule2panel_refyearset(adc2classification_rule, refyearset2panel)
	select adc2classification_rule, refyearset2panel from '||_schema||'.cm_adc2classrule2panel_refyearset order by id';	

	-- Data for Name: cm_spc2classrule2panel_refyearset; Type: TABLE DATA; Schema: target_data; Owner: vagrant
	EXECUTE 'insert into target_data.cm_spc2classrule2panel_refyearset(spc2classification_rule, refyearset2panel)
	select spc2classification_rule, refyearset2panel from '||_schema||'.cm_spc2classrule2panel_refyearset order by id';		

	-- Name: c_ldsity_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_ldsity_id_seq', (select max(id) from target_data.c_ldsity), true);

	-- Name: c_ldsity_object_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_ldsity_object_id_seq', (select max(id) from target_data.c_ldsity_object), true);	

	-- Name: c_unit_of_measure_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_unit_of_measure_id_seq', (select max(id) from target_data.c_unit_of_measure), true);

	-- Name: c_definition_variant_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_definition_variant_id_seq', (select max(id) from target_data.c_definition_variant), true);

	-- Name: c_area_domain_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_area_domain_id_seq', (select max(id) from target_data.c_area_domain), true);

	-- Name: c_sub_population_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_sub_population_id_seq', (select max(id) from target_data.c_sub_population), true);

	-- Name: c_area_domain_category_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_area_domain_category_id_seq', (select max(id) from target_data.c_area_domain_category), true);

	-- Name: c_sub_population_category_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_sub_population_category_id_seq', (select max(id) from target_data.c_sub_population_category), true);

	-- Name: c_version_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.c_version_id_seq', (select max(id) from target_data.c_version), true);

	-- Name: cm_ldsity2panel_refyearset_version_id_seq; Type: SEQUENCE SET; Schema: target_data; Owner: vagrant
	PERFORM pg_catalog.setval('target_data.cm_ldsity2panel_refyearset_version_id_seq', (select max(id) from target_data.cm_ldsity2panel_refyearset_version), true);

	_res := 'Import is comleted.';

	return _res;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_import_data(character varying) IS
'The function imports data from source tables into tables that are in the target_data schema.';
-- </function>



-- <function name="fn_import_get_areal_or_populations" schema="target_data" src="functions/import/fn_import_get_areal_or_populations.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_areal_or_populations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_areal_or_populations(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_areal_or_populations
(
	_id			integer,
	_etl_id		integer[] default null::integer[]
)
returns table
(
	id				integer,
	etl_id			integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_areal_or_populations: Input argument _id must not be null!';
		end if;

		if _etl_id is null
		then
			return query
			select
					_id as id,
					caop.id as etl_id,
					caop.label,
					caop.description,
					caop.label as label_en,
					caop.description as description_en
			from
					target_data.c_areal_or_population as caop
			order
					by caop.id;
		else
			return query
			select
					_id as id,
					caop.id as etl_id,
					caop.label,
					caop.description,
					caop.label as label_en,
					caop.description as description_en
			from
					target_data.c_areal_or_population as caop
			where
					caop.id not in (select unnest(_etl_id))
			order
					by caop.id;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_areal_or_populations(integer, integer[]) is
'Function returns records from table c_areal_or_population for given input arguments.';

grant execute on function target_data.fn_import_get_areal_or_populations(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_check_areal_or_population" schema="target_data" src="functions/import/fn_import_check_areal_or_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_areal_or_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_areal_or_population(integer, varchar) CASCADE;

create or replace function target_data.fn_import_check_areal_or_population
(
	_id			integer,
	_label_en	varchar
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text
)
as
$$
declare
		_res_etl_id				integer;
		_res_label				character varying;
		_res_description		text;
		_res_label_en			character varying;
		_res_description_en		text;
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_check_areal_or_population: Input argument _id must not be NULL!';
		end if; 
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_import_check_areal_or_population: Input argument _label_en must not be NULL!';
		end if;
	
		select
				caop.id,
				caop.label,
				caop.description,
				caop.label as label_en,
				caop.description as description_en
		from
				target_data.c_areal_or_population as caop
		where
				caop.label = _label_en
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query select _id, _res_etl_id, _res_label, _res_description, _res_label_en, _res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_areal_or_population(integer, varchar) is
'Function returns record ID from table c_areal_or_population based on given parameters.';

grant execute on function target_data.fn_import_check_areal_or_population(integer, varchar) to public;
-- </function>


/*
-- <function name="fn_import_save_areal_or_population" schema="target_data" src="functions/import/fn_import_save_areal_or_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_areal_or_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_areal_or_population(integer, varchar, text, varchar, text) CASCADE;

create or replace function target_data.fn_import_save_areal_or_population
(
	_id					integer,
	_label				varchar,
	_description		text,
	_label_en			varchar,
	_description_en		text
)
returns table
(
	id		integer,
	etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_import_save_areal_or_population: Input argument _id must not by NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_import_save_areal_or_population: Input argument _label must not by NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_import_save_areal_or_population: Input argument _description must not by NULL!';
	end if;

	insert into target_data.c_areal_or_population(label, description)
	select _label, _description
	returning c_areal_or_population.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_areal_or_population(integer, varchar, text, varchar, text) is
'Function inserts a record into table c_areal_or_population based on given parameters.';

grant execute on function target_data.fn_import_save_areal_or_population(integer, varchar, text, varchar, text) to public;
-- </function>
*/


-- <function name="fn_import_get_ldsity_objects" schema="target_data" src="functions/import/fn_import_get_ldsity_objects.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_ldsity_objects
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_ldsity_objects(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_ldsity_objects
(
	_ldsity_object	integer,
	_etl_id			integer[] default null::integer[]
)
returns table
(
	ldsity_object			integer,
	id						integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	table_name				varchar,
	upper_object			integer,
	areal_or_population		integer,
	column4upper_object		varchar,
	filter 					text
)
as
$$
declare
begin
		if _ldsity_object is null
		then
			raise exception 'Error 01: fn_import_get_ldsity_objects: Input argument _ldsity_object must not be NULL!';
		end if;
	
		return query
		select
				_ldsity_object as ldsity_object,
				clo.id,
				clo.label,
				clo.description,
				clo.label_en,
				clo.description_en,
				clo.table_name,
				clo.upper_object,
				clo.areal_or_population,
				clo.column4upper_object,
				clo.filter
		from 
				target_data.c_ldsity_object as clo
		where
				clo.id not in (select unnest(_etl_id))
		order
				by clo.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_ldsity_objects(integer, integer[]) is
'Function returns records from table c_ldsity_object.';

grant execute on function target_data.fn_import_get_ldsity_objects(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_check_ldsity_object" schema="target_data" src="functions/import/fn_import_check_ldsity_object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_ldsity_object(integer, character varying) CASCADE;

create or replace function target_data.fn_import_check_ldsity_object
(
	_ldsity_object	integer,
	_label_en		character varying
)
returns table
(
	ldsity_object					integer,
	id								integer,
	label							character varying,
	description						text,
	label_en						character varying,
	description_en					text,
	areal_or_population__label		character varying,
	areal_or_population__label_en	character varying
)
as
$$
declare
		_res_etl_id							integer;
		_res_label							character varying;
		_res_description					text;
		_res_label_en						character varying;
		_res_description_en					text;
		_res_areal_or_population__label		character varying;
		_res_areal_or_population__label_en	character varying;
begin
		if _ldsity_object is null
		then
			raise exception 'Error 01: fn_import_check_ldsity_object: Input argument _ldsity_object must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_import_check_ldsity_object: Input argument _label_en must not be NULL!';
		end if;
	
		with
		w1 as	(
				select
						clo.id,
						clo.label,
						clo.description,
						clo.label_en,
						clo.description_en,
						clo.areal_or_population
				from
						target_data.c_ldsity_object as clo
				where
						clo.label_en = _label_en
				)
		select
				w1.id,
				w1.label,
				w1.description,
				w1.label_en,
				w1.description_en,
				caop.label as areal_or_population__label,
				caop.label as areal_or_population__label_en
		from 
				w1
				inner join target_data.c_areal_or_population as caop
				on w1.areal_or_population = caop.id
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en,
				_res_areal_or_population__label,
				_res_areal_or_population__label_en;

		return query
		select
				_ldsity_object,
				_res_etl_id, 
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en,
				_res_areal_or_population__label,
				_res_areal_or_population__label_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_ldsity_object(integer, character varying) is
'Function returns record ID from table c_ldsity_object based on given parameters.';

grant execute on function target_data.fn_import_check_ldsity_object(integer, character varying) to public;
-- </function>



-- <function name="fn_import_save_ldsity_object" schema="target_data" src="functions/import/fn_import_save_ldsity_object.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_ldsity_object
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_ldsity_object(integer, varchar, text, varchar, text, varchar, integer, integer, varchar, text) CASCADE;

create or replace function target_data.fn_import_save_ldsity_object
(
	_ldsity_object			integer,
	_label					varchar,
	_description			text,
	_label_en				varchar,
	_description_en			text,
	_table_name				varchar,
	_upper_object			integer,
	_areal_or_population	integer,
	_column4upper_object	varchar,
	_filter					text
)
returns integer
as
$$
declare
		_etl_id		integer;
begin
	if _ldsity_object is null
	then
		raise exception 'Error 01: fn_import_save_ldsity_object: Input argument _ldsity_object must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_import_save_ldsity_object: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_import_save_ldsity_object: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_import_save_ldsity_object: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_import_save_ldsity_object: Input argument _description_en must not be NULL!';
	end if;

	if _table_name is null
	then
		raise exception 'Error 06: fn_import_save_ldsity_object: Input argument _table_name must not be NULL!';
	end if;

	if _areal_or_population is null
	then
		raise exception 'Error 07: fn_import_save_ldsity_object: Input argument _areal_or_population must not be NULL!';
	end if;

	insert into target_data.c_ldsity_object
	(
		label, description, table_name, upper_object, areal_or_population, column4upper_object, label_en, description_en, filter
	)
	select
			_label, _description, _table_name, _upper_object, _areal_or_population, _column4upper_object, _label_en, _description_en, _filter
	returning
			c_ldsity_object.id
	into
			_etl_id;

	return _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_ldsity_object(integer, varchar, text, varchar, text, varchar, integer, integer, varchar, text) is
'Function inserts a record into table c_ldsity_object based on given parameters.';

grant execute on function target_data.fn_import_save_ldsity_object(integer, varchar, text, varchar, text, varchar, integer, integer, varchar, text) to public;
-- </function>



-- <function name="fn_import_get_unit_of_measures" schema="target_data" src="functions/import/fn_import_get_unit_of_measures.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_unit_of_measures
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_unit_of_measures(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_unit_of_measures
(
	_unit_of_measure	integer,
	_etl_id				integer[] default null::integer[]
)
returns table
(
	unit_of_measure			integer,
	etl_id					integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text
)
as
$$
declare
begin
		if _unit_of_measure is null
		then
			raise exception 'Error 01: fn_import_get_unit_of_measures: Input argument _unit_of_measure must not be NULL!';
		end if;
	
		return query
		select
				_unit_of_measure as unit_of_measure,
				cuom.id as etl_id,
				cuom.label,
				cuom.description,
				cuom.label_en,
				cuom.description_en
		from 
				target_data.c_unit_of_measure as cuom
		where
				cuom.id not in (select unnest(_etl_id))
		order
				by cuom.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_unit_of_measures(integer, integer[]) is
'Function returns records from table c_unit_of_measure.';

grant execute on function target_data.fn_import_get_unit_of_measures(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_check_unit_of_measure" schema="target_data" src="functions/import/fn_import_check_unit_of_measure.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_unit_of_measure
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_unit_of_measure(integer, character varying) CASCADE;

create or replace function target_data.fn_import_check_unit_of_measure
(
	_unit_of_measure	integer,
	_label_en			character varying
)
returns table
(
	unit_of_measure		integer,
	etl_id				integer,
	label				character varying,
	description			text,
	label_en			character varying,
	description_en		text
)
as
$$
declare
		_res_etl_id							integer;
		_res_label							character varying;
		_res_description					text;
		_res_label_en						character varying;
		_res_description_en					text;
begin
		if _unit_of_measure is null
		then
			raise exception 'Error 01: fn_import_check_unit_of_measure: Input argument _unit_of_measure must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_import_check_unit_of_measure: Input argument _label_en must not be NULL!';
		end if;
	
		select
				cuom.id as etl_id,
				cuom.label,
				cuom.description,
				cuom.label_en,
				cuom.description_en
		from 
				target_data.c_unit_of_measure as cuom
		where
				cuom.label_en = _label_en
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query
		select
				_unit_of_measure,
				_res_etl_id, 
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_unit_of_measure(integer, character varying) is
'Function returns record ID from table c_unit_of_measure based on given parameters.';

grant execute on function target_data.fn_import_check_unit_of_measure(integer, character varying) to public;
-- </function>



-- <function name="fn_import_save_unit_of_measure" schema="target_data" src="functions/import/fn_import_save_unit_of_measure.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_unit_of_measure
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_unit_of_measure(integer, varchar, text, varchar, text) CASCADE;

create or replace function target_data.fn_import_save_unit_of_measure
(
	_unit_of_measure		integer,
	_label					varchar,
	_description			text,
	_label_en				varchar,
	_description_en			text
)
returns table
(
	unit_of_measure		integer,
	etl_id				integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _unit_of_measure is null
	then
		raise exception 'Error 01: fn_import_save_unit_of_measure: Input argument _unit_of_measure must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_import_save_unit_of_measure: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_import_save_unit_of_measure: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_import_save_unit_of_measure: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_import_save_unit_of_measure: Input argument _description_en must not be NULL!';
	end if;

	insert into target_data.c_unit_of_measure(label, description, label_en, description_en)
	select _label, _description, _label_en, _description_en
	returning c_unit_of_measure.id
	into _etl_id;

	return query select _unit_of_measure, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_unit_of_measure(integer, varchar, text, varchar, text) is
'Function inserts a record into table c_unit_of_measure based on given parameters.';

grant execute on function target_data.fn_import_save_unit_of_measure(integer, varchar, text, varchar, text) to public;
-- </function



-- <function name="fn_import_get_definition_variants" schema="target_data" src="functions/import/fn_import_get_definition_variants.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_definition_variants
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_definition_variants(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_definition_variants
(
	_definition_variant		integer,
	_etl_id					integer[] default null::integer[]
)
returns table
(
	definition_variant		integer,
	etl_id					integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text
)
as
$$
declare
begin
		if _definition_variant is null
		then
			raise exception 'Error 01: fn_import_get_definition_variants: Input argument _definition_variant must not be NULL!';
		end if;
	
		return query
		select
				_definition_variant as definition_variant,
				cdv.id as etl_id,
				cdv.label,
				cdv.description,
				cdv.label_en,
				cdv.description_en
		from 
				target_data.c_definition_variant as cdv
		where
				cdv.id not in (select unnest(_etl_id))
		order
				by cdv.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_definition_variants(integer, integer[]) is
'Function returns records from table c_definition_variant.';

grant execute on function target_data.fn_import_get_definition_variants(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_check_definition_variant" schema="target_data" src="functions/import/fn_import_check_definition_variant.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_definition_variant
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_definition_variant(integer, character varying) CASCADE;

create or replace function target_data.fn_import_check_definition_variant
(
	_definition_variant		integer,
	_label_en				character varying
)
returns table
(
	definition_variant	integer,
	etl_id				integer,
	label				character varying,
	description			text,
	label_en			character varying,
	description_en		text
)
as
$$
declare
		_res_etl_id							integer;
		_res_label							character varying;
		_res_description					text;
		_res_label_en						character varying;
		_res_description_en					text;
begin
		if _definition_variant is null
		then
			raise exception 'Error 01: fn_import_check_definition_variant: Input argument _definition_variant must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_import_check_definition_variant: Input argument _label_en must not be NULL!';
		end if;
	
		select
				cdv.id as etl_id,
				cdv.label,
				cdv.description,
				cdv.label_en,
				cdv.description_en
		from 
				target_data.c_definition_variant as cdv
		where
				cdv.label_en = _label_en
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query
		select
				_definition_variant,
				_res_etl_id, 
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_definition_variant(integer, character varying) is
'Function returns record ID from table c_definition_variant based on given parameters.';

grant execute on function target_data.fn_import_check_definition_variant(integer, character varying) to public;
-- </function>



-- <function name="fn_import_save_definition_variant" schema="target_data" src="functions/import/fn_import_save_definition_variant.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_definition_variant
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_definition_variant(integer, varchar, text, varchar, text) CASCADE;

create or replace function target_data.fn_import_save_definition_variant
(
	_definition_variant		integer,
	_label					varchar,
	_description			text,
	_label_en				varchar,
	_description_en			text
)
returns table
(
	definition_variant	integer,
	etl_id				integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _definition_variant is null
	then
		raise exception 'Error 01: fn_import_save_definition_variant: Input argument _definition_variant must not by NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_import_save_definition_variant: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_import_save_definition_variant: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_import_save_definition_variant: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_import_save_definition_variant: Input argument _description_en must not be NULL!';
	end if;

	insert into target_data.c_definition_variant(label, description, label_en, description_en)
	select _label, _description, _label_en, _description_en
	returning c_definition_variant.id
	into _etl_id;

	return query select _definition_variant, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_definition_variant(integer, varchar, text, varchar, text) is
'Function inserts a record into table c_definition_variant based on given parameters.';

grant execute on function target_data.fn_import_save_definition_variant(integer, varchar, text, varchar, text) to public;
-- </function>



-- <function name="fn_import_get_area_domains" schema="target_data" src="functions/import/fn_import_get_area_domains.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_area_domains
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_area_domains(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_area_domains
(
	_area_domain		integer,
	_etl_id				integer[] default null::integer[]
)
returns table
(
	area_domain			integer,
	etl_id				integer,
	label				varchar,
	description			text,
	label_en			varchar,
	description_en		text
)
as
$$
declare
begin
		if _area_domain is null
		then
			raise exception 'Error 01: fn_import_get_area_domains: Input argument _area_domain must not be NULL!';
		end if;
	
		return query
		select
				_area_domain as area_domain,
				cad.id as etl_id,
				cad.label,
				cad.description,
				cad.label_en,
				cad.description_en
		from 
				target_data.c_area_domain as cad
		where
				cad.id not in (select unnest(_etl_id))
		order
				by cad.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_area_domains(integer, integer[]) is
'Function returns records from table c_area_domain.';

grant execute on function target_data.fn_import_get_area_domains(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_check_area_domain" schema="target_data" src="functions/import/fn_import_check_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_area_domain(integer, character varying) CASCADE;

create or replace function target_data.fn_import_check_area_domain
(
	_area_domain		integer,
	_label_en			character varying
)
returns table
(
	area_domain			integer,
	etl_id				integer,
	label				character varying,
	description			text,
	label_en			character varying,
	description_en		text
)
as
$$
declare
		_res_etl_id							integer;
		_res_label							character varying;
		_res_description					text;
		_res_label_en						character varying;
		_res_description_en					text;
begin
		if _area_domain is null
		then
			raise exception 'Error 01: fn_import_check_area_domain: Input argument _area_domain must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_import_check_area_domain: Input argument _label_en must not be NULL!';
		end if;
	
		select
				cad.id as etl_id,
				cad.label,
				cad.description,
				cad.label_en,
				cad.description_en
		from 
				target_data.c_area_domain as cad
		where
				cad.label_en = _label_en
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query
		select
				_area_domain,
				_res_etl_id, 
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_area_domain(integer, character varying) is
'Function returns record ID from table c_area_domain based on given parameters.';


grant execute on function target_data.fn_import_check_area_domain(integer, character varying) to public;
-- </function>



-- <function name="fn_import_save_area_domain" schema="target_data" src="functions/import/fn_import_save_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_area_domain(integer, varchar, text, varchar, text) CASCADE;

create or replace function target_data.fn_import_save_area_domain
(
	_area_domain			integer,
	_label					varchar,
	_description			text,
	_label_en				varchar,
	_description_en			text
)
returns table
(
	area_domain			integer,
	etl_id				integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _area_domain is null
	then
		raise exception 'Error 01: fn_import_save_area_domain: Input argument _area_domain must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_import_save_area_domain: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_import_save_area_domain: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_import_save_area_domain: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_import_save_area_domain: Input argument _description_en must not be NULL!';
	end if;

	insert into target_data.c_area_domain(label, description, label_en, description_en)
	select _label, _description, _label_en, _description_en
	returning c_area_domain.id
	into _etl_id;

	return query select _area_domain, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_area_domain(integer, varchar, text, varchar, text) is
'Function inserts a record into table c_area_domain based on given parameters.';

grant execute on function target_data.fn_import_save_area_domain(integer, varchar, text, varchar, text) to public;
-- </function>



-- <function name="fn_import_get_sub_populations" schema="target_data" src="functions/import/fn_import_get_sub_populations.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_sub_populations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_sub_populations(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_sub_populations
(
	_sub_population		integer,
	_etl_id				integer[] default null::integer[]
)
returns table
(
	sub_population		integer,
	etl_id				integer,
	label				varchar,
	description			text,
	label_en			varchar,
	description_en		text
)
as
$$
declare
begin
		if _sub_population is null
		then
			raise exception 'Error 01: fn_import_get_sub_populations: Input argument _sub_population must not be NULL!';
		end if;
	
		return query
		select
				_sub_population as sub_population,
				cad.id as etl_id,
				cad.label,
				cad.description,
				cad.label_en,
				cad.description_en
		from 
				target_data.c_sub_population as cad
		where
				cad.id not in (select unnest(_etl_id))
		order
				by cad.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_sub_populations(integer, integer[]) is
'Function returns records from table c_sub_population.';

grant execute on function target_data.fn_import_get_sub_populations(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_check_sub_population" schema="target_data" src="functions/import/fn_import_check_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_sub_population(integer, character varying) CASCADE;

create or replace function target_data.fn_import_check_sub_population
(
	_sub_population		integer,
	_label_en			character varying
)
returns table
(
	sub_population		integer,
	etl_id				integer,
	label				character varying,
	description			text,
	label_en			character varying,
	description_en		text
)
as
$$
declare
		_res_etl_id							integer;
		_res_label							character varying;
		_res_description					text;
		_res_label_en						character varying;
		_res_description_en					text;
begin
		if _sub_population is null
		then
			raise exception 'Error 01: fn_import_check_sub_population: Input argument _sub_population must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_import_check_sub_population: Input argument _label_en must not be NULL!';
		end if;
	
		select
				cad.id as etl_id,
				cad.label,
				cad.description,
				cad.label_en,
				cad.description_en
		from 
				target_data.c_sub_population as cad
		where
				cad.label_en = _label_en
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query
		select
				_sub_population,
				_res_etl_id, 
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_sub_population(integer, character varying) is
'Function returns record ID from table c_sub_population based on given parameters.';

grant execute on function target_data.fn_import_check_sub_population(integer, character varying) to public;
-- </function>



-- <function name="fn_import_save_sub_population" schema="target_data" src="functions/import/fn_import_save_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_sub_population(integer, varchar, text, varchar, text) CASCADE;

create or replace function target_data.fn_import_save_sub_population
(
	_sub_population			integer,
	_label					varchar,
	_description			text,
	_label_en				varchar,
	_description_en			text
)
returns table
(
	sub_population		integer,
	etl_id				integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _sub_population is null
	then
		raise exception 'Error 01: fn_import_save_sub_population: Input argument _sub_population must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_import_save_sub_population: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_import_save_sub_population: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_import_save_sub_population: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_import_save_sub_population: Input argument _description_en must not be NULL!';
	end if;

	insert into target_data.c_sub_population(label, description, label_en, description_en)
	select _label, _description, _label_en, _description_en
	returning c_sub_population.id
	into _etl_id;

	return query select _sub_population, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_sub_population(integer, varchar, text, varchar, text) is
'Function inserts a record into table c_sub_population based on given parameters.';

grant execute on function target_data.fn_import_save_sub_population(integer, varchar, text, varchar, text) to public;
-- </function>



-- <function name="fn_import_get_area_domain_categories" schema="target_data" src="functions/import/fn_import_get_area_domain_categories.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_area_domain_categories
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_area_domain_categories(integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_area_domain_categories
(
	_area_domain_category	integer,
	_area_domain__etl_id	integer,
	_etl_id					integer[] default null::integer[]
)
returns table
(
	area_domain_category	integer,
	etl_id					integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text
)
as
$$
declare
begin
		if _area_domain_category is null
		then
			raise exception 'Error 01: fn_import_get_area_domain_categories: Input argument _area_domain_category must not be NULL!';
		end if;
	
		if _area_domain__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_area_domain_categories: Input argument _area_domain__etl_id must not be NULL!';
		end if;
	
		return query
		select
				_area_domain_category as area_domain_category,
				cadc.id as etl_id,
				cadc.label,
				cadc.description,
				cadc.label_en,
				cadc.description_en
		from 
				target_data.c_area_domain_category as cadc
		where
				cadc.area_domain = _area_domain__etl_id
		and
				cadc.id not in (select unnest(_etl_id))
		order
				by cadc.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_area_domain_categories(integer, integer, integer[]) is
'Function returns records from table c_area_domain_category.';

grant execute on function target_data.fn_import_get_area_domain_categories(integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_check_area_domain_category" schema="target_data" src="functions/import/fn_import_check_area_domain_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_area_domain_category(integer, integer, character varying) CASCADE;

create or replace function target_data.fn_import_check_area_domain_category
(
	_area_domain_category		integer,
	_area_domain__etl_id		integer,
	_label_en					character varying
)
returns table
(
	area_domain_category		integer,
	area_domain__etl_id			integer,
	area_domain__label			character varying,
	area_domain__description	text,
	area_domain__label_en		character varying,
	area_domain__description_en text,
	etl_id						integer,
	label						character varying,
	description					text,
	label_en					character varying,
	description_en				text
)
as
$$
declare
		_area_domain__label					character varying;
		_area_domain__description			text;
		_area_domain__label_en				character varying;
		_area_domain__description_en 		text;
		_res_etl_id							integer;
		_res_label							character varying;
		_res_description					text;
		_res_label_en						character varying;
		_res_description_en					text;
begin
		if _area_domain_category is null
		then
			raise exception 'Error 01: fn_import_check_area_domain_category: Input argument _area_domain_category must not be NULL!';
		end if;
	
		if _area_domain__etl_id is null
		then
			raise exception 'Error 02: fn_import_check_area_domain_category: Input argument _area_domain__etl_id must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 03: fn_import_check_area_domain_category: Input argument _label_en must not be NULL!';
		end if;
	
		with
		w1 as	(select cad.* from target_data.c_area_domain as cad where cad.id = _area_domain__etl_id)
		,w2 as	(
				select
						cadc.id as etl_id,
						cadc.label,
						cadc.description,
						cadc.label_en,
						cadc.description_en,
						cadc.area_domain
				from 
						target_data.c_area_domain_category as cadc
				where
						cadc.area_domain = _area_domain__etl_id
				and
						cadc.label_en = _label_en
				)
		select
				w1.label as area_domain__label,
				w1.description as area_domain__description,
				w1.label_en as area_domain__label_en,
				w1.description_en as area_domain__description_en,
				w2.etl_id,
				w2.label,
				w2.description,
				w2.label_en,
				w2.description_en
		from
				w1 left join w2 on w1.id = w2.area_domain
		into
				_area_domain__label,
				_area_domain__description,
				_area_domain__label_en,
				_area_domain__description_en,
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query
		select
				_area_domain_category,
				_area_domain__etl_id,
				_area_domain__label,
				_area_domain__description,
				_area_domain__label_en,
				_area_domain__description_en,
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_area_domain_category(integer, integer, character varying) is
'Function returns record ID from table c_area_domain based on given parameters.';

grant execute on function target_data.fn_import_check_area_domain_category(integer, integer, character varying) to public;
-- </function>



-- <function name="fn_import_save_area_domain_category" schema="target_data" src="functions/import/fn_import_save_area_domain_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_area_domain_category(integer, integer, varchar, text, varchar, text) CASCADE;

create or replace function target_data.fn_import_save_area_domain_category
(
	_area_domain_category		integer,
	_area_domain__etl_id		integer,
	_label						varchar,
	_description				text,
	_label_en					varchar,
	_description_en				text
)
returns table
(
	area_domain_category		integer,
	etl_id						integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _area_domain_category is null
	then
		raise exception 'Error 01: fn_import_save_area_domain_category: Input argument _area_domain_category must not be NULL!';
	end if;

	if _area_domain__etl_id is null
	then
		raise exception 'Error 02: fn_import_save_area_domain_category: Input argument _area_domain__etl_id must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 03: fn_import_save_area_domain_category: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 04: fn_import_save_area_domain_category: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 05: fn_import_save_area_domain_category: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 06: fn_import_save_area_domain_category: Input argument _description_en must not be NULL!';
	end if;

	insert into target_data.c_area_domain_category(label, description, area_domain, label_en, description_en)
	select _label, _description, _area_domain__etl_id, _label_en, _description_en
	returning c_area_domain_category.id
	into _etl_id;

	return query select _area_domain_category, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_area_domain_category(integer, integer, varchar, text, varchar, text) is
'Function inserts a record into table c_area_domain_category based on given parameters.';

grant execute on function target_data.fn_import_save_area_domain_category(integer, integer, varchar, text, varchar, text) to public;
-- </function>



-- <function name="fn_import_get_sub_population_categories" schema="target_data" src="functions/import/fn_import_get_sub_population_categories.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_sub_population_categories
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_sub_population_categories(integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_sub_population_categories
(
	_sub_population_category	integer,
	_sub_population__etl_id		integer,
	_etl_id						integer[] default null::integer[]
)
returns table
(
	sub_population_category	integer,
	etl_id					integer,
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text
)
as
$$
declare
begin
		if _sub_population_category is null
		then
			raise exception 'Error 01: fn_import_get_sub_population_categories: Input argument _sub_population_category must not be NULL!';
		end if;
	
		if _sub_population__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_sub_population_categories: Input argument _sub_population__etl_id must not be NULL!';
		end if;
	
		return query
		select
				_sub_population_category as sub_population_category,
				cadc.id as etl_id,
				cadc.label,
				cadc.description,
				cadc.label_en,
				cadc.description_en
		from 
				target_data.c_sub_population_category as cadc
		where
				cadc.sub_population = _sub_population__etl_id
		and
				cadc.id not in (select unnest(_etl_id))
		order
				by cadc.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_sub_population_categories(integer, integer, integer[]) is
'Function returns records from table c_sub_population_category.';

grant execute on function target_data.fn_import_get_sub_population_categories(integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_check_sub_population_category" schema="target_data" src="functions/import/fn_import_check_sub_population_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_sub_population_category(integer, integer, character varying) CASCADE;

create or replace function target_data.fn_import_check_sub_population_category
(
	_sub_population_category		integer,
	_sub_population__etl_id			integer,
	_label_en						character varying
)
returns table
(
	sub_population_category			integer,
	sub_population__etl_id			integer,
	sub_population__label			character varying,
	sub_population__description		text,
	sub_population__label_en		character varying,
	sub_population__description_en 	text,
	etl_id							integer,
	label							character varying,
	description						text,
	label_en						character varying,
	description_en					text
)
as
$$
declare
		_sub_population__label					character varying;
		_sub_population__description			text;
		_sub_population__label_en				character varying;
		_sub_population__description_en 		text;
		_res_etl_id								integer;
		_res_label								character varying;
		_res_description						text;
		_res_label_en							character varying;
		_res_description_en						text;
begin
		if _sub_population_category is null
		then
			raise exception 'Error 01: fn_import_check_sub_population_category: Input argument _sub_population_category must not be NULL!';
		end if;
	
		if _sub_population__etl_id is null
		then
			raise exception 'Error 02: fn_import_check_sub_population_category: Input argument _sub_population__etl_id must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 03: fn_import_check_sub_population_category: Input argument _label_en must not be NULL!';
		end if;
	
		with
		w1 as	(select cad.* from target_data.c_sub_population as cad where cad.id = _sub_population__etl_id)
		,w2 as	(
				select
						cadc.id as etl_id,
						cadc.label,
						cadc.description,
						cadc.label_en,
						cadc.description_en,
						cadc.sub_population
				from 
						target_data.c_sub_population_category as cadc
				where
						cadc.sub_population = _sub_population__etl_id
				and
						cadc.label_en = _label_en
				)
		select
				w1.label as sub_population__label,
				w1.description as sub_population__description,
				w1.label_en as sub_population__label_en,
				w1.description_en as sub_population__description_en,
				w2.etl_id,
				w2.label,
				w2.description,
				w2.label_en,
				w2.description_en
		from
				w1 left join w2 on w1.id = w2.sub_population
		into
				_sub_population__label,
				_sub_population__description,
				_sub_population__label_en,
				_sub_population__description_en,
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query
		select
				_sub_population_category,
				_sub_population__etl_id,
				_sub_population__label,
				_sub_population__description,
				_sub_population__label_en,
				_sub_population__description_en,
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_sub_population_category(integer, integer, character varying) is
'Function returns record ID from table c_sub_population based on given parameters.';

grant execute on function target_data.fn_import_check_sub_population_category(integer, integer, character varying) to public;
-- </function>



-- <function name="fn_import_save_sub_population_category" schema="target_data" src="functions/import/fn_import_save_sub_population_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_sub_population_category(integer, integer, varchar, text, varchar, text) CASCADE;

create or replace function target_data.fn_import_save_sub_population_category
(
	_sub_population_category		integer,
	_sub_population__etl_id		integer,
	_label						varchar,
	_description				text,
	_label_en					varchar,
	_description_en				text
)
returns table
(
	sub_population_category		integer,
	etl_id						integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _sub_population_category is null
	then
		raise exception 'Error 01: fn_import_save_sub_population_category: Input argument _sub_population_category must not be NULL!';
	end if;

	if _sub_population__etl_id is null
	then
		raise exception 'Error 02: fn_import_save_sub_population_category: Input argument _sub_population__etl_id must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 03: fn_import_save_sub_population_category: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 04: fn_import_save_sub_population_category: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 05: fn_import_save_sub_population_category: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 06: fn_import_save_sub_population_category: Input argument _description_en must not be NULL!';
	end if;

	insert into target_data.c_sub_population_category(label, description, sub_population, label_en, description_en)
	select _label, _description, _sub_population__etl_id, _label_en, _description_en
	returning c_sub_population_category.id
	into _etl_id;

	return query select _sub_population_category, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_sub_population_category(integer, integer, varchar, text, varchar, text) is
'Function inserts a record into table c_sub_population_category based on given parameters.';

grant execute on function target_data.fn_import_save_sub_population_category(integer, integer, varchar, text, varchar, text) to public;
-- </function>



-- <function name="fn_import_get_adc_hierarchies" schema="target_data" src="functions/import/fn_import_get_adc_hierarchies.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_adc_hierarchies
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_adc_hierarchies(integer, integer, integer, boolean, integer[]) CASCADE;

create or replace function target_data.fn_import_get_adc_hierarchies
(
	_id								integer,
	_variable_superior__etl_id		integer,
	_variable__etl_id				integer,
	_dependent						boolean,
	_etl_id							integer[] default null::integer[]
)
returns table
(
	id					integer,
	etl_id				integer,
	variable_superior	integer,
	variable			integer,
	dependent			boolean
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_adc_hierarchies: Input argument _id must not be NULL!';
		end if;
	
		if _variable_superior__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_adc_hierarchies: Input argument _variable_superior__etl_id must not be NULL!';
		end if;
	
		if _variable__etl_id is null
		then
			raise exception 'Error 03: fn_import_get_adc_hierarchies: Input argument _variable__etl_id must not be NULL!';
		end if;
	
		if _dependent is null
		then
			raise exception 'Error 04: fn_import_get_adc_hierarchies: Input argument _dependent must not be NULL!';
		end if;
	
		return query
		select
				_id as id,
				tah.id as etl_id,
				tah.variable_superior,
				tah.variable,
				tah.dependent
		from
				target_data.t_adc_hierarchy as tah
		where
				tah.id not in (select unnest(_etl_id))
		and		tah.variable_superior = _variable_superior__etl_id
		and		tah.variable = _variable__etl_id
		and		tah.dependent = _dependent;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_adc_hierarchies(integer, integer, integer, boolean, integer[]) is
'Function returns records from table t_adc_hierarchy.';

grant execute on function target_data.fn_import_get_adc_hierarchies(integer, integer, integer, boolean, integer[]) to public;
-- </function>



-- <function name="fn_import_save_adc_hierarchy" schema="target_data" src="functions/import/fn_import_save_adc_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_adc_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_adc_hierarchy(integer, integer, integer, boolean) CASCADE;

create or replace function target_data.fn_import_save_adc_hierarchy
(
	_id						integer,
	_variable_superior		integer,
	_variable				integer,
	_dependent				boolean
)
returns table
(
	id			integer,
	etl_id		integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_import_save_adc_hierarchy: Input argument _id must not be NULL!';
	end if;

	if _variable_superior is null
	then
		raise exception 'Error 02: fn_import_save_adc_hierarchy: Input argument _variable_superior must not be NULL!';
	end if;

	if _variable is null
	then
		raise exception 'Error 03: fn_import_save_adc_hierarchy: Input argument _variable must not be NULL!';
	end if;

	if _dependent is null
	then
		raise exception 'Error 04: fn_import_save_adc_hierarchy: Input argument _dependent must not be NULL!';
	end if;

	insert into target_data.t_adc_hierarchy(variable_superior, variable, dependent)
	select _variable_superior, _variable, _dependent
	returning t_adc_hierarchy.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_adc_hierarchy(integer, integer, integer, boolean) is
'Function inserts a record into table t_adc_hierarchy based on given parameters.';

grant execute on function target_data.fn_import_save_adc_hierarchy(integer, integer, integer, boolean) to public;
-- </function>



-- <function name="fn_import_get_spc_hierarchies" schema="target_data" src="functions/import/fn_import_get_spc_hierarchies.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_spc_hierarchies
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_spc_hierarchies(integer, integer, integer, boolean, integer[]) CASCADE;

create or replace function target_data.fn_import_get_spc_hierarchies
(
	_id								integer,
	_variable_superior__etl_id		integer,
	_variable__etl_id				integer,
	_dependent						boolean,
	_etl_id							integer[] default null::integer[]
)
returns table
(
	id					integer,
	etl_id				integer,
	variable_superior	integer,
	variable			integer,
	dependent			boolean
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_spc_hierarchies: Input argument _id must not be NULL!';
		end if;
	
		if _variable_superior__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_spc_hierarchies: Input argument _variable_superior__etl_id must not be NULL!';
		end if;
	
		if _variable__etl_id is null
		then
			raise exception 'Error 03: fn_import_get_spc_hierarchies: Input argument _variable__etl_id must not be NULL!';
		end if;
	
		if _dependent is null
		then
			raise exception 'Error 04: fn_import_get_spc_hierarchies: Input argument _dependent must not be NULL!';
		end if;
	
		return query
		select
				_id as id,
				tsh.id as etl_id,
				tsh.variable_superior,
				tsh.variable,
				tsh.dependent
		from
				target_data.t_spc_hierarchy as tsh
		where
				tsh.id not in (select unnest(_etl_id))
		and		tsh.variable_superior = _variable_superior__etl_id
		and		tsh.variable = _variable__etl_id
		and		tsh.dependent = _dependent;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_spc_hierarchies(integer, integer, integer, boolean, integer[]) is
'Function returns records from table t_spc_hierarchy.';

grant execute on function target_data.fn_import_get_spc_hierarchies(integer, integer, integer, boolean, integer[]) to public;
-- </function>



-- <function name="fn_import_save_spc_hierarchy" schema="target_data" src="functions/import/fn_import_save_spc_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_spc_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_spc_hierarchy(integer, integer, integer, boolean) CASCADE;

create or replace function target_data.fn_import_save_spc_hierarchy
(
	_id						integer,
	_variable_superior		integer,
	_variable				integer,
	_dependent				boolean
)
returns table
(
	id			integer,
	etl_id		integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_import_save_spc_hierarchy: Input argument _id must not be NULL!';
	end if;

	if _variable_superior is null
	then
		raise exception 'Error 02: fn_import_save_spc_hierarchy: Input argument _variable_superior must not be NULL!';
	end if;

	if _variable is null
	then
		raise exception 'Error 03: fn_import_save_spc_hierarchy: Input argument _variable must not be NULL!';
	end if;

	if _dependent is null
	then
		raise exception 'Error 04: fn_import_save_spc_hierarchy: Input argument _dependent must not be NULL!';
	end if;

	insert into target_data.t_spc_hierarchy(variable_superior, variable, dependent)
	select _variable_superior, _variable, _dependent
	returning t_spc_hierarchy.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_spc_hierarchy(integer, integer, integer, boolean) is
'Function inserts a record into table t_spc_hierarchy based on given parameters.';

grant execute on function target_data.fn_import_save_spc_hierarchy(integer, integer, integer, boolean) to public;
-- </function>



-- <function name="fn_import_get_adc2classification_rules" schema="target_data" src="functions/import/fn_import_get_adc2classification_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_adc2classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_adc2classification_rules(integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_adc2classification_rules
(
	_adc2classification_rule		integer,
	_area_domain_category__etl_id	integer,
	_ldsity_object__etl_id			integer,
	_etl_id							integer[] default null::integer[]
)
returns table
(
	adc2classification_rule					integer,
	ldsity_object__id						integer,
	ldsity_object__label					varchar,
	ldsity_object__description				text,
	ldsity_object__label_en					varchar,
	ldsity_object__description_en			text,
	area_domain_category__id				integer,
	area_domain_category__label				varchar,
	area_domain_category__description		text,
	area_domain_category__label_en			varchar,
	area_domain_category__description_en	text,
	etl_id									integer,
	classification_rule						text
)
as
$$
declare
begin
		if _adc2classification_rule is null
		then
			raise exception 'Error 01: fn_import_get_adc2classification_rules: Input argument _adc2classification_rule must not be NULL!';
		end if;
	
		if _area_domain_category__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_adc2classification_rules: Input argument _area_domain_category__etl_id must not be NULL!';
		end if;
	
		if _ldsity_object__etl_id is null
		then
			raise exception 'Error 03: fn_import_get_adc2classification_rules: Input argument _ldsity_object__etl_id must not be NULL!';
		end if;	
	
		return query
		with
		w1 as	(
				select
						_adc2classification_rule as adc2classification_rule,
						cacr.id as etl_id,
						cacr.area_domain_category as area_domain_category__id,
						cacr.ldsity_object as ldsity_object__id,
						cacr.classification_rule
				from
						target_data.cm_adc2classification_rule as cacr
				where
						cacr.area_domain_category = _area_domain_category__etl_id
				and
						cacr.ldsity_object = _ldsity_object__etl_id
				and
						cacr.id not in (select unnest(_etl_id))
				)
		select
				w1.adc2classification_rule,
				w1.ldsity_object__id,
				clo.label as ldsity_object__label,
				clo.description as ldsity_object__description,
				clo.label_en as ldsity_object__label_en,
				clo.description_en as ldsity_object__description_en,
				w1.area_domain_category__id,
				cadc.label as area_domain_category__label,
				cadc.description as area_domain_category__description,
				cadc.label_en as area_domain_category__label_en,
				cadc.description_en as area_domain_category__description_en,
				w1.etl_id,
				w1.classification_rule
		from
				w1
				inner join target_data.c_area_domain_category as cadc on w1.area_domain_category__id = cadc.id
				inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
		order 
				by w1.ldsity_object__id, w1.area_domain_category__id, w1.adc2classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_adc2classification_rules(integer, integer, integer, integer[]) is
'Function returns records from table cm_adc2classification_rule.';

grant execute on function target_data.fn_import_get_adc2classification_rules(integer, integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_check_adc2classification_rule" schema="target_data" src="functions/import/fn_import_check_adc2classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_adc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_adc2classification_rule(integer, integer, integer, text) CASCADE;

create or replace function target_data.fn_import_check_adc2classification_rule
(
	_adc2classification_rule			integer,
	_area_domain_category__etl_id		integer,
	_ldsity_object__etl_id				integer,
	_classification_rule				text
)
returns table
(	
	adc2classification_rule					integer,
	ldsity_object__id						integer,
	ldsity_object__label					varchar,
	ldsity_object__description				text,
	ldsity_object__label_en					varchar,
	ldsity_object__description_en			text,
	area_domain_category__id				integer,
	area_domain_category__label				varchar,
	area_domain_category__description		text,
	area_domain_category__label_en			varchar,
	area_domain_category__description_en	text,
	etl_id									integer,
	classification_rule						text
)
as
$$
declare
		_ldsity_object__label					character varying;
		_ldsity_object__description				text;
		_ldsity_object__label_en				character varying;
		_ldsity_object__description_en 			text;
		_area_domain_category__label			character varying;
		_area_domain_category__description		text;
		_area_domain_category__label_en			character varying;
		_area_domain_category__description_en 	text;
		_res_etl_id								integer;
		_res_classification_rule				text;
begin
		if _adc2classification_rule is null
		then
			raise exception 'Error 01: fn_import_check_adc2classification_rule: Input argument _adc2classification_rule must not be NULL!';
		end if;
	
		if _area_domain_category__etl_id is null
		then
			raise exception 'Error 02: fn_import_check_adc2classification_rule: Input argument _area_domain_category__etl_id must not be NULL!';
		end if;
	
		if _ldsity_object__etl_id is null
		then
			raise exception 'Error 03: fn_import_check_adc2classification_rule: Input argument _ldsity_object__etl_id must not be NULL!';
		end if;
	
		if _classification_rule is null
		then
			raise exception 'Error 04: fn_import_check_adc2classification_rule: Input argument _classification_rule must not be NULL!';
		end if;
	
		with
		w1 as	(
				select
						_adc2classification_rule as adc2classification_rule,
						_ldsity_object__etl_id as ldsity_object__id,
						_area_domain_category__etl_id as area_domain_category__id
				)
		,w2 as	(
				select
						w1.adc2classification_rule,
						w1.ldsity_object__id,
						clo.label as ldsity_object__label,
						clo.description as ldsity_object__description,
						clo.label_en as ldsity_object__label_en,
						clo.description_en as ldsity_object__description_en,
						w1.area_domain_category__id,
						cadc.label as area_domain_category__label,
						cadc.description as area_domain_category__description,
						cadc.label_en as area_domain_category__label_en,
						cadc.description_en as area_domain_category__description_en
				from
						w1
						inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
						inner join target_data.c_area_domain_category as cadc on w1.area_domain_category__id = cadc.id
				)
		,w3 as	(
				select cacr.* from target_data.cm_adc2classification_rule as cacr
				where cacr.area_domain_category = _area_domain_category__etl_id
				and cacr.ldsity_object = _ldsity_object__etl_id
				and cacr.classification_rule = _classification_rule
				)
		select
				w2.ldsity_object__label,
				w2.ldsity_object__description,
				w2.ldsity_object__label_en,
				w2.ldsity_object__description_en,
				w2.area_domain_category__label,
				w2.area_domain_category__description,
				w2.area_domain_category__label_en,
				w2.area_domain_category__description_en,
				w3.id as etl_id,
				w3.classification_rule
		from
				w2
				left join w3
				on w2.ldsity_object__id = w3.ldsity_object
				and w2.area_domain_category__id = w3.area_domain_category
		into
				_ldsity_object__label,
				_ldsity_object__description,
				_ldsity_object__label_en,
				_ldsity_object__description_en,
				_area_domain_category__label,
				_area_domain_category__description,
				_area_domain_category__label_en,
				_area_domain_category__description_en,
				_res_etl_id,
				_res_classification_rule;
	
		return query
		select
				_adc2classification_rule,
				_ldsity_object__etl_id,
				_ldsity_object__label,
				_ldsity_object__description,
				_ldsity_object__label_en,
				_ldsity_object__description_en,
				_area_domain_category__etl_id,
				_area_domain_category__label,
				_area_domain_category__description,
				_area_domain_category__label_en,
				_area_domain_category__description_en,
				_res_etl_id,
				_res_classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_adc2classification_rule(integer, integer, integer, text) is
'Function returns record ID from table cm_adc2classification_rule based on given parameters.';

grant execute on function target_data.fn_import_check_adc2classification_rule(integer, integer, integer, text) to public;
-- </function>



-- <function name="fn_import_save_adc2classification_rule" schema="target_data" src="functions/import/fn_import_save_adc2classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_adc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_adc2classification_rule(integer, integer, integer, text, integer) CASCADE;

create or replace function target_data.fn_import_save_adc2classification_rule
(
	_adc2classification_rule			integer,
	_area_domain_category__etl_id		integer,
	_ldsity_object__etl_id				integer,
	_classification_rule				text,
	_refyearset2panel					integer
)
returns table
(
	adc2classification_rule						integer,
	etl_id										integer,
	cm_adc2classrule2panel_refyearset__etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _adc2classification_rule is null
	then
		raise exception 'Error 01: fn_import_save_adc2classification_rule: Input argument _adc2classification_rule must not be NULL!';
	end if;

	if _area_domain_category__etl_id is null
	then
		raise exception 'Error 02: fn_import_save_adc2classification_rule: Input argument _area_domain_category__etl_id must not be NULL!';
	end if;

	if _ldsity_object__etl_id is null
	then
		raise exception 'Error 03: fn_import_save_adc2classification_rule: Input argument _ldsity_object__etl_id must not be NULL!';
	end if;

	if _classification_rule is null
	then
		raise exception 'Error 04: fn_import_save_adc2classification_rule: Input argument _classification_rule must not be NULL!';
	end if;

	return query
	with
	w1 as	(
			select
					_area_domain_category__etl_id as adc__etl_id,
					_ldsity_object__etl_id as lo__etl_id,
					_classification_rule as cr
			)
	,w2 as	(
			insert into target_data.cm_adc2classification_rule
			(area_domain_category, ldsity_object, classification_rule)
			select
					w1.adc__etl_id,
					w1.lo__etl_id,
					w1.cr
			from
					w1
			returning cm_adc2classification_rule.id
			)
	insert into target_data.cm_adc2classrule2panel_refyearset(adc2classification_rule, refyearset2panel)
	select w2.id, _refyearset2panel from w2
	returning _adc2classification_rule, cm_adc2classrule2panel_refyearset.adc2classification_rule, cm_adc2classrule2panel_refyearset.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_adc2classification_rule(integer, integer, integer, text, integer) is
'Function inserts a record into table cm_adc2classification_rule based on given parameters.';

grant execute on function target_data.fn_import_save_adc2classification_rule(integer, integer, integer, text, integer) to public;
-- </function>



-- <function name="fn_import_get_spc2classification_rules" schema="target_data" src="functions/import/fn_import_get_spc2classification_rules.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_spc2classification_rules
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_spc2classification_rules(integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_spc2classification_rules
(
	_spc2classification_rule			integer,
	_sub_population_category__etl_id	integer,
	_ldsity_object__etl_id				integer,
	_etl_id								integer[] default null::integer[]
)
returns table
(
	spc2classification_rule					integer,
	ldsity_object__id						integer,
	ldsity_object__label					varchar,
	ldsity_object__description				text,
	ldsity_object__label_en					varchar,
	ldsity_object__description_en			text,
	sub_population_category__id				integer,
	sub_population_category__label			varchar,
	sub_population_category__description	text,
	sub_population_category__label_en		varchar,
	sub_population_category__description_en	text,
	etl_id									integer,
	classification_rule						text
)
as
$$
declare
begin
		if _spc2classification_rule is null
		then
			raise exception 'Error 01: fn_import_get_spc2classification_rules: Input argument _spc2classification_rule must not be NULL!';
		end if;
	
		if _sub_population_category__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_spc2classification_rules: Input argument _sub_population_category__etl_id must not be NULL!';
		end if;
	
		if _ldsity_object__etl_id is null
		then
			raise exception 'Error 03: fn_import_get_spc2classification_rules: Input argument _ldsity_object__etl_id must not be NULL!';
		end if;	
	
		return query
		with
		w1 as	(
				select
						_spc2classification_rule as spc2classification_rule,
						cscr.id as etl_id,
						cscr.sub_population_category as sub_population_category__id,
						cscr.ldsity_object as ldsity_object__id,
						cscr.classification_rule
				from
						target_data.cm_spc2classification_rule as cscr
				where
						cscr.sub_population_category = _sub_population_category__etl_id
				and
						cscr.ldsity_object = _ldsity_object__etl_id
				and
						cscr.id not in (select unnest(_etl_id))
				)
		select
				w1.spc2classification_rule,
				w1.ldsity_object__id,
				clo.label as ldsity_object__label,
				clo.description as ldsity_object__description,
				clo.label_en as ldsity_object__label_en,
				clo.description_en as ldsity_object__description_en,
				w1.sub_population_category__id,
				cspc.label as sub_population_category__label,
				cspc.description as sub_population_category__description,
				cspc.label_en as sub_population_category__label_en,
				cspc.description_en as sub_population_category__description_en,
				w1.etl_id,
				w1.classification_rule
		from
				w1
				inner join target_data.c_sub_population_category as cspc on w1.sub_population_category__id = cspc.id
				inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
		order 
				by w1.ldsity_object__id, w1.sub_population_category__id, w1.spc2classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_spc2classification_rules(integer, integer, integer, integer[]) is
'Function returns records from table cm_spc2classification_rule.';

grant execute on function target_data.fn_import_get_spc2classification_rules(integer, integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_check_spc2classification_rule" schema="target_data" src="functions/import/fn_import_check_spc2classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_spc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_spc2classification_rule(integer, integer, integer, text) CASCADE;

create or replace function target_data.fn_import_check_spc2classification_rule
(
	_spc2classification_rule			integer,
	_sub_population_category__etl_id	integer,
	_ldsity_object__etl_id				integer,
	_classification_rule				text
)
returns table
(	
	spc2classification_rule					integer,
	ldsity_object__id						integer,
	ldsity_object__label					varchar,
	ldsity_object__description				text,
	ldsity_object__label_en					varchar,
	ldsity_object__description_en			text,
	sub_population_category__id				integer,
	sub_population_category__label			varchar,
	sub_population_category__description	text,
	sub_population_category__label_en		varchar,
	sub_population_category__description_en	text,
	etl_id									integer,
	classification_rule						text
)
as
$$
declare
		_ldsity_object__label						character varying;
		_ldsity_object__description					text;
		_ldsity_object__label_en					character varying;
		_ldsity_object__description_en 				text;
		_sub_population_category__label				character varying;
		_sub_population_category__description		text;
		_sub_population_category__label_en			character varying;
		_sub_population_category__description_en 	text;
		_res_etl_id									integer;
		_res_classification_rule					text;
begin
		if _spc2classification_rule is null
		then
			raise exception 'Error 01: fn_import_check_spc2classification_rule: Input argument _spc2classification_rule must not be NULL!';
		end if;
	
		if _sub_population_category__etl_id is null
		then
			raise exception 'Error 02: fn_import_check_spc2classification_rule: Input argument _sub_population_category__etl_id must not be NULL!';
		end if;
	
		if _ldsity_object__etl_id is null
		then
			raise exception 'Error 03: fn_import_check_spc2classification_rule: Input argument _ldsity_object__etl_id must not be NULL!';
		end if;
	
		if _classification_rule is null
		then
			raise exception 'Error 04: fn_import_check_spc2classification_rule: Input argument _classification_rule must not be NULL!';
		end if;
	
		with
		w1 as	(
				select
						_spc2classification_rule as spc2classification_rule,
						_ldsity_object__etl_id as ldsity_object__id,
						_sub_population_category__etl_id as sub_population_category__id
				)
		,w2 as	(
				select
						w1.spc2classification_rule,
						w1.ldsity_object__id,
						clo.label as ldsity_object__label,
						clo.description as ldsity_object__description,
						clo.label_en as ldsity_object__label_en,
						clo.description_en as ldsity_object__description_en,
						w1.sub_population_category__id,
						cspc.label as sub_population_category__label,
						cspc.description as sub_population_category__description,
						cspc.label_en as sub_population_category__label_en,
						cspc.description_en as sub_population_category__description_en
				from
						w1
						inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
						inner join target_data.c_sub_population_category as cspc on w1.sub_population_category__id = cspc.id
				)
		,w3 as	(
				select cacr.* from target_data.cm_spc2classification_rule as cacr
				where cacr.sub_population_category = _sub_population_category__etl_id
				and cacr.ldsity_object = _ldsity_object__etl_id
				and cacr.classification_rule = _classification_rule
				)
		select
				w2.ldsity_object__label,
				w2.ldsity_object__description,
				w2.ldsity_object__label_en,
				w2.ldsity_object__description_en,
				w2.sub_population_category__label,
				w2.sub_population_category__description,
				w2.sub_population_category__label_en,
				w2.sub_population_category__description_en,
				w3.id as etl_id,
				w3.classification_rule
		from
				w2
				left join w3
				on w2.ldsity_object__id = w3.ldsity_object
				and w2.sub_population_category__id = w3.sub_population_category
		into
				_ldsity_object__label,
				_ldsity_object__description,
				_ldsity_object__label_en,
				_ldsity_object__description_en,
				_sub_population_category__label,
				_sub_population_category__description,
				_sub_population_category__label_en,
				_sub_population_category__description_en,
				_res_etl_id,
				_res_classification_rule;
	
		return query
		select
				_spc2classification_rule,
				_ldsity_object__etl_id,
				_ldsity_object__label,
				_ldsity_object__description,
				_ldsity_object__label_en,
				_ldsity_object__description_en,
				_sub_population_category__etl_id,
				_sub_population_category__label,
				_sub_population_category__description,
				_sub_population_category__label_en,
				_sub_population_category__description_en,
				_res_etl_id,
				_res_classification_rule;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_spc2classification_rule(integer, integer, integer, text) is
'Function returns record ID from table cm_spc2classification_rule based on given parameters.';

grant execute on function target_data.fn_import_check_spc2classification_rule(integer, integer, integer, text) to public;
-- </function>



-- <function name="fn_import_save_spc2classification_rule" schema="target_data" src="functions/import/fn_import_save_spc2classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_spc2classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_spc2classification_rule(integer, integer, integer, text, integer) CASCADE;

create or replace function target_data.fn_import_save_spc2classification_rule
(
	_spc2classification_rule			integer,
	_sub_population_category__etl_id	integer,
	_ldsity_object__etl_id				integer,
	_classification_rule				text,
	_refyearset2panel					integer
)
returns table
(
	spc2classification_rule						integer,
	etl_id										integer,
	cm_spc2classrule2panel_refyearset__etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _spc2classification_rule is null
	then
		raise exception 'Error 01: fn_import_save_spc2classification_rule: Input argument _spc2classification_rule must not be NULL!';
	end if;

	if _sub_population_category__etl_id is null
	then
		raise exception 'Error 02: fn_import_save_spc2classification_rule: Input argument _sub_population_category__etl_id must not be NULL!';
	end if;

	if _ldsity_object__etl_id is null
	then
		raise exception 'Error 03: fn_import_save_spc2classification_rule: Input argument _ldsity_object__etl_id must not be NULL!';
	end if;

	if _classification_rule is null
	then
		raise exception 'Error 04: fn_import_save_spc2classification_rule: Input argument _classification_rule must not be NULL!';
	end if;

	return query
	with
	w1 as	(
			select
					_sub_population_category__etl_id as spc__etl_id,
					_ldsity_object__etl_id as lo__etl_id,
					_classification_rule as cr
			)
	,w2 as	(
			insert into target_data.cm_spc2classification_rule
			(sub_population_category, ldsity_object, classification_rule)
			select
					w1.spc__etl_id,
					w1.lo__etl_id,
					w1.cr
			from
					w1
			returning cm_spc2classification_rule.id
			)
	insert into target_data.cm_spc2classrule2panel_refyearset(spc2classification_rule, refyearset2panel)
	select w2.id, _refyearset2panel from w2
	returning _spc2classification_rule, cm_spc2classrule2panel_refyearset.spc2classification_rule, cm_spc2classrule2panel_refyearset.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_spc2classification_rule(integer, integer, integer, text, integer) is
'Function inserts a record into table cm_spc2classification_rule based on given parameters.';

grant execute on function target_data.fn_import_save_spc2classification_rule(integer, integer, integer, text, integer) to public;
-- </function>



-- <function name="fn_import_get_adc2classrule2panel_refyearsets" schema="target_data" src="functions/import/fn_import_get_adc2classrule2panel_refyearsets.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_adc2classrule2panel_refyearsets
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_adc2classrule2panel_refyearsets(integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_adc2classrule2panel_refyearsets
(
	_id										integer,
	_adc2classification_rule__etl_id		integer,
	_refyearset2panel						integer,
	_etl_id									integer[] default null::integer[]
)
returns table
(
	id							integer,
	etl_id						integer,
	adc2classification_rule		integer,
	refyearset2panel			integer
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_adc2classrule2panel_refyearsets: Input argument _id must not be NULL!';
		end if;
	
		if _adc2classification_rule__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_adc2classrule2panel_refyearsets: Input argument _adc2classification_rule__etl_id must not be NULL!';
		end if;
	
		if _refyearset2panel is null
		then
			raise exception 'Error 03: fn_import_get_adc2classrule2panel_refyearsets: Input argument _refyearset2panel must not be NULL!';
		end if;
	
		return query
		select
				_id as id,
				cacpr.id as etl_id,
				cacpr.adc2classification_rule,
				cacpr.refyearset2panel
		from
				target_data.cm_adc2classrule2panel_refyearset as cacpr
		where
				cacpr.id not in (select unnest(_etl_id))
		and		cacpr.adc2classification_rule = _adc2classification_rule__etl_id
		and		cacpr.refyearset2panel = _refyearset2panel;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_adc2classrule2panel_refyearsets(integer, integer, integer, integer[]) is
'Function returns records from table cm_adc2classrule2panel_refyearset.';

grant execute on function target_data.fn_import_get_adc2classrule2panel_refyearsets(integer, integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_save_adc2classrule2panel_refyearset" schema="target_data" src="functions/import/fn_import_save_adc2classrule2panel_refyearset.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_adc2classrule2panel_refyearset
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_adc2classrule2panel_refyearset(integer, integer, integer) CASCADE;

create or replace function target_data.fn_import_save_adc2classrule2panel_refyearset
(
	_id							integer,
	_adc2classification_rule	integer,
	_refyearset2panel			integer
)
returns table
(
	id			integer,
	etl_id		integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_import_save_adc2classrule2panel_refyearset: Input argument _id must not be NULL!';
	end if;

	if _adc2classification_rule is null
	then
		raise exception 'Error 02: fn_import_save_adc2classrule2panel_refyearset: Input argument _adc2classification_rule must not be NULL!';
	end if;

	if _refyearset2panel is null
	then
		raise exception 'Error 03: fn_import_save_adc2classrule2panel_refyearset: Input argument _refyearset2panel must not be NULL!';
	end if;

	insert into target_data.cm_adc2classrule2panel_refyearset(adc2classification_rule, refyearset2panel)
	select _adc2classification_rule, _refyearset2panel
	returning cm_adc2classrule2panel_refyearset.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_adc2classrule2panel_refyearset(integer, integer, integer) is
'Function inserts a record into table cm_adc2classrule2panel_refyearset based on given parameters.';

grant execute on function target_data.fn_import_save_adc2classrule2panel_refyearset(integer, integer, integer) to public;
-- </function>



-- <function name="fn_import_get_spc2classrule2panel_refyearsets" schema="target_data" src="functions/import/fn_import_get_spc2classrule2panel_refyearsets.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_spc2classrule2panel_refyearsets
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_spc2classrule2panel_refyearsets(integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_spc2classrule2panel_refyearsets
(
	_id										integer,
	_spc2classification_rule__etl_id		integer,
	_refyearset2panel						integer,
	_etl_id									integer[] default null::integer[]
)
returns table
(
	id							integer,
	etl_id						integer,
	spc2classification_rule		integer,
	refyearset2panel			integer
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_spc2classrule2panel_refyearsets: Input argument _id must not be NULL!';
		end if;
	
		if _spc2classification_rule__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_spc2classrule2panel_refyearsets: Input argument _spc2classification_rule__etl_id must not be NULL!';
		end if;
	
		if _refyearset2panel is null
		then
			raise exception 'Error 03: fn_import_get_spc2classrule2panel_refyearsets: Input argument _refyearset2panel must not be NULL!';
		end if;
	
		return query
		select
				_id as id,
				cacpr.id as etl_id,
				cacpr.spc2classification_rule,
				cacpr.refyearset2panel
		from
				target_data.cm_spc2classrule2panel_refyearset as cacpr
		where
				cacpr.id not in (select unnest(_etl_id))
		and		cacpr.spc2classification_rule = _spc2classification_rule__etl_id
		and		cacpr.refyearset2panel = _refyearset2panel;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_spc2classrule2panel_refyearsets(integer, integer, integer, integer[]) is
'Function returns records from table cm_spc2classrule2panel_refyearset.';

grant execute on function target_data.fn_import_get_spc2classrule2panel_refyearsets(integer, integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_save_spc2classrule2panel_refyearset" schema="target_data" src="functions/import/fn_import_save_spc2classrule2panel_refyearset.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_spc2classrule2panel_refyearset
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_spc2classrule2panel_refyearset(integer, integer, integer) CASCADE;

create or replace function target_data.fn_import_save_spc2classrule2panel_refyearset
(
	_id							integer,
	_spc2classification_rule	integer,
	_refyearset2panel			integer
)
returns table
(
	id			integer,
	etl_id		integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_import_save_spc2classrule2panel_refyearset: Input argument _id must not be NULL!';
	end if;

	if _spc2classification_rule is null
	then
		raise exception 'Error 02: fn_import_save_spc2classrule2panel_refyearset: Input argument _spc2classification_rule must not be NULL!';
	end if;

	if _refyearset2panel is null
	then
		raise exception 'Error 03: fn_import_save_spc2classrule2panel_refyearset: Input argument _refyearset2panel must not be NULL!';
	end if;

	insert into target_data.cm_spc2classrule2panel_refyearset(spc2classification_rule, refyearset2panel)
	select _spc2classification_rule, _refyearset2panel
	returning cm_spc2classrule2panel_refyearset.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_spc2classrule2panel_refyearset(integer, integer, integer) is
'Function inserts a record into table cm_spc2classrule2panel_refyearset based on given parameters.';

grant execute on function target_data.fn_import_save_spc2classrule2panel_refyearset(integer, integer, integer) to public;
-- </function>



-- <function name="fn_import_get_ldsities" schema="target_data" src="functions/import/fn_import_get_ldsities.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_ldsities
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_ldsities(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_ldsities
(
	_ldsity			integer,
	_etl_id			integer[] default null::integer[]
)
returns table
(
	ldsity											integer,
	id												integer,
	label											varchar,
	description										text,
	label_en										varchar,
	description_en									text,
	ldsity_object__id								integer,
	ldsity_object__label							varchar,
	ldsity_object__description						text,
	ldsity_object__label_en							varchar,
	ldsity_object__description_en					text,
	column_expression								text,
	unit_of_measure__id								integer,
	unit_of_measure__label							varchar,
	unit_of_measure__description					text,
	unit_of_measure__label_en						varchar,
	unit_of_measure__description_en					text,	
	area_domain_category__id						integer[],
	area_domain_category__classification_rule		text[],
	sub_population_category__id						integer[],
	sub_population_category__classification_rule	text[],
	definition_variant__id							integer[],
	definition_variant__label						varchar[],
	definition_variant__description					text[],
	definition_variant__label_en					varchar[],
	definition_variant__description_en				text[]
)
as
$$
declare
begin
		if _ldsity is null
		then
			raise exception 'Error 01: fn_import_get_ldsities: Input argument _ldsity must not be NULL!';
		end if;
	
		return query
		with
		w1 as	(
				select
						_ldsity as ldsity,
						cl.id,
						cl.label,
						cl.description,
						cl.label_en,
						cl.description_en,
						cl.ldsity_object as ldsity_object__id,
						cl.column_expression,
						cl.unit_of_measure as unit_of_measure__id,
						cl.area_domain_category as area_domain_category__id,
						cl.sub_population_category as sub_population_category__id,
						cl.definition_variant as definition_variant__id
				from 
						target_data.c_ldsity as cl
				where
						cl.id not in (select unnest(_etl_id))
				)
		,w2 as	(
				select
						w1.*,
						clo.label as ldsity_object__label,
						clo.description as ldsity_object__description,
						clo.label_en as ldsity_object__label_en,
						clo.description_en as ldsity_object__description_en,
						cuom.label as unit_of_measure__label,
						cuom.description as unit_of_measure__description,
						cuom.label_en as unit_of_measure__label_en,
						cuom.description_en as unit_of_measure__description_en,
						-------------------------------------------------------
						case
							when w1.area_domain_category__id is null then null::text[]
							else	(
									select array_agg(cacr.classification_rule order by cacr.id)
									from target_data.cm_adc2classification_rule as cacr
									where cacr.id in (select unnest(w1.area_domain_category__id))
									)
						end as area_domain_category__classification_rule,
						-------------------------------------------------------
						case
							when w1.sub_population_category__id is null then null::text[]
							else	(
									select array_agg(cscr.classification_rule order by cscr.id)
									from target_data.cm_spc2classification_rule as cscr
									where cscr.id in (select unnest(w1.sub_population_category__id))
									)
						end as sub_population_category__classification_rule,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::varchar[]
							else	(
									select array_agg(cdv_1.label order by cdv_1.id)
									from target_data.c_definition_variant as cdv_1
									where cdv_1.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__label,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::text[]
							else	(
									select array_agg(cdv_2.description order by cdv_2.id)
									from target_data.c_definition_variant as cdv_2
									where cdv_2.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__description,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::varchar[]
							else	(
									select array_agg(cdv_3.label_en order by cdv_3.id)
									from target_data.c_definition_variant as cdv_3
									where cdv_3.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__label_en,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::text[]
							else	(
									select array_agg(cdv_4.description_en order by cdv_4.id)
									from target_data.c_definition_variant as cdv_4
									where cdv_4.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__description_en
				from
						w1
						inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
						left join target_data.c_unit_of_measure as cuom on w1.unit_of_measure__id = cuom.id
				)
		select 
				w2.ldsity,
				w2.id,
				w2.label,
				w2.description,
				w2.label_en,
				w2.description_en,
				w2.ldsity_object__id,
				w2.ldsity_object__label,
				w2.ldsity_object__description,
				w2.ldsity_object__label_en,
				w2.ldsity_object__description_en,
				w2.column_expression,
				w2.unit_of_measure__id,
				w2.unit_of_measure__label,
				w2.unit_of_measure__description,
				w2.unit_of_measure__label_en,
				w2.unit_of_measure__description_en,
				w2.area_domain_category__id,
				w2.area_domain_category__classification_rule,
				w2.sub_population_category__id,
				w2.sub_population_category__classification_rule,
				w2.definition_variant__id,
				w2.definition_variant__label,
				w2.definition_variant__description,
				w2.definition_variant__label_en,
				w2.definition_variant__description_en
		from
				w2
		order
				by w2.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_ldsities(integer, integer[]) is
'Function returns records from table c_ldsity.';

grant execute on function target_data.fn_import_get_ldsities(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_check_ldsity" schema="target_data" src="functions/import/fn_import_check_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_ldsity(integer, integer, varchar) CASCADE;

create or replace function target_data.fn_import_check_ldsity
(
	_ldsity					integer,
	_ldsity_object__etl_id	integer,
	_label_en				varchar
)
returns table
(
	ldsity											integer,
	etl_id											integer,
	label											varchar,
	description										text,
	label_en										varchar,
	description_en									text,
	ldsity_object__id								integer,
	ldsity_object__label							varchar,
	ldsity_object__description						text,
	ldsity_object__label_en							varchar,
	ldsity_object__description_en					text,
	column_expression								text,
	unit_of_measure__id								integer,
	unit_of_measure__label							varchar,
	unit_of_measure__description					text,
	unit_of_measure__label_en						varchar,
	unit_of_measure__description_en					text,	
	area_domain_category__id						integer[],
	area_domain_category__classification_rule		text[],
	sub_population_category__id						integer[],
	sub_population_category__classification_rule	text[],
	definition_variant__id							integer[],
	definition_variant__label						varchar[],
	definition_variant__description					text[],
	definition_variant__label_en					varchar[],
	definition_variant__description_en				text[]
)
as
$$
declare
		_etl_id											integer;
		_ldsity_label									varchar;
		_ldsity_description								text;
		_ldsity_label_en								varchar;
		_ldsity_description_en							text;
		_ldsity_object__id								integer;
		_ldsity_object__label							varchar;
		_ldsity_object__description						text;
		_ldsity_object__label_en						varchar;
		_ldsity_object__description_en					text;
		_column_expression								text;
		_unit_of_measure__id							integer;
		_unit_of_measure__label							varchar;
		_unit_of_measure__description					text;
		_unit_of_measure__label_en						varchar;
		_unit_of_measure__description_en				text;
		_area_domain_category__id						integer[];
		_area_domain_category__classification_rule		text[];
		_sub_population_category__id					integer[];
		_sub_population_category__classification_rule	text[];
		_definition_variant__id							integer[];
		_definition_variant__label						varchar[];
		_definition_variant__description				text[];
		_definition_variant__label_en					varchar[];
		_definition_variant__description_en				text[];
begin
		if _ldsity is null
		then
			raise exception 'Error 01: fn_import_check_ldsity: Input argument _ldsity must not be NULL!';
		end if;
	
		if _ldsity_object__etl_id is null
		then
			raise exception 'Error 02: fn_import_check_ldsity: Input argument _ldsity_object__etl_id must not be NULL!';
		end if;
	
		if _label_en is null
		then
			raise exception 'Error 03: fn_import_check_ldsity: Input argument _label_en must not be NULL!';
		end if;

		with
		w1 as	(
				select
						cl.id as etl_id,
						cl.label,
						cl.description,
						cl.label_en,
						cl.description_en,
						cl.ldsity_object as ldsity_object__id,
						cl.column_expression,
						cl.unit_of_measure as unit_of_measure__id,
						cl.area_domain_category as area_domain_category__id,
						cl.sub_population_category as sub_population_category__id,
						cl.definition_variant as definition_variant__id
				from 
						target_data.c_ldsity as cl
				where
						cl.ldsity_object = _ldsity_object__etl_id
				and
						cl.label_en = _label_en
				)
		,w2 as	(
				select
						w1.*,
						clo.label as ldsity_object__label,
						clo.description as ldsity_object__description,
						clo.label_en as ldsity_object__label_en,
						clo.description_en as ldsity_object__description_en,
						cuom.label as unit_of_measure__label,
						cuom.description as unit_of_measure__description,
						cuom.label_en as unit_of_measure__label_en,
						cuom.description_en as unit_of_measure__description_en,
						-------------------------------------------------------
						case
							when w1.area_domain_category__id is null then null::text[]
							else	(
									select array_agg(cacr.classification_rule order by cacr.id)
									from target_data.cm_adc2classification_rule as cacr
									where cacr.id in (select unnest(w1.area_domain_category__id))
									)
						end as area_domain_category__classification_rule,
						-------------------------------------------------------
						case
							when w1.sub_population_category__id is null then null::text[]
							else	(
									select array_agg(cscr.classification_rule order by cscr.id)
									from target_data.cm_spc2classification_rule as cscr
									where cscr.id in (select unnest(w1.sub_population_category__id))
									)
						end as sub_population_category__classification_rule,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::varchar[]
							else	(
									select array_agg(cdv_1.label order by cdv_1.id)
									from target_data.c_definition_variant as cdv_1
									where cdv_1.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__label,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::text[]
							else	(
									select array_agg(cdv_2.description order by cdv_2.id)
									from target_data.c_definition_variant as cdv_2
									where cdv_2.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__description,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::varchar[]
							else	(
									select array_agg(cdv_3.label_en order by cdv_3.id)
									from target_data.c_definition_variant as cdv_3
									where cdv_3.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__label_en,
						-------------------------------------------------------
						case
							when w1.definition_variant__id is null then null::text[]
							else	(
									select array_agg(cdv_4.description_en order by cdv_4.id)
									from target_data.c_definition_variant as cdv_4
									where cdv_4.id in (select unnest(w1.definition_variant__id))
									)
						end as definition_variant__description_en
				from
						w1
						inner join target_data.c_ldsity_object as clo on w1.ldsity_object__id = clo.id
						left join target_data.c_unit_of_measure as cuom on w1.unit_of_measure__id = cuom.id
				)
		select 
				w2.etl_id,
				w2.label,
				w2.description,
				w2.label_en,
				w2.description_en,
				w2.ldsity_object__id,
				w2.ldsity_object__label,
				w2.ldsity_object__description,
				w2.ldsity_object__label_en,
				w2.ldsity_object__description_en,
				w2.column_expression,
				w2.unit_of_measure__id,
				w2.unit_of_measure__label,
				w2.unit_of_measure__description,
				w2.unit_of_measure__label_en,
				w2.unit_of_measure__description_en,
				w2.area_domain_category__id,
				w2.area_domain_category__classification_rule,
				w2.sub_population_category__id,
				w2.sub_population_category__classification_rule,
				w2.definition_variant__id,
				w2.definition_variant__label,
				w2.definition_variant__description,
				w2.definition_variant__label_en,
				w2.definition_variant__description_en
		from
				w2
		into
			_etl_id,
			_ldsity_label,
			_ldsity_description,
			_ldsity_label_en,
			_ldsity_description_en,
			_ldsity_object__id,
			_ldsity_object__label,
			_ldsity_object__description,
			_ldsity_object__label_en,
			_ldsity_object__description_en,
			_column_expression,
			_unit_of_measure__id,
			_unit_of_measure__label,
			_unit_of_measure__description,
			_unit_of_measure__label_en,
			_unit_of_measure__description_en,
			_area_domain_category__id,
			_area_domain_category__classification_rule,
			_sub_population_category__id,
			_sub_population_category__classification_rule,
			_definition_variant__id,
			_definition_variant__label,
			_definition_variant__description,
			_definition_variant__label_en,
			_definition_variant__description_en;
		
		return query
		select
				_ldsity,
				_etl_id,
				_ldsity_label,
				_ldsity_description,
				_ldsity_label_en,
				_ldsity_description_en,
				_ldsity_object__id,
				_ldsity_object__label,
				_ldsity_object__description,
				_ldsity_object__label_en,
				_ldsity_object__description_en,
				_column_expression,
				_unit_of_measure__id,
				_unit_of_measure__label,
				_unit_of_measure__description,
				_unit_of_measure__label_en,
				_unit_of_measure__description_en,
				_area_domain_category__id,
				_area_domain_category__classification_rule,
				_sub_population_category__id,
				_sub_population_category__classification_rule,
				_definition_variant__id,
				_definition_variant__label,
				_definition_variant__description,
				_definition_variant__label_en,
				_definition_variant__description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_ldsity(integer, integer, varchar) is
'Function returns record ID from table c_ldsity based on given parameters.';

grant execute on function target_data.fn_import_check_ldsity(integer, integer, varchar) to public;
-- </function>



-- <function name="fn_import_save_ldsity" schema="target_data" src="functions/import/fn_import_save_ldsity.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_ldsity
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_ldsity(integer,varchar,text,varchar,text,integer,text,integer,integer[],integer[],integer[]) CASCADE;

create or replace function target_data.fn_import_save_ldsity
(
	_ldsity						integer,
	_label						varchar,
	_description				text,
	_label_en					varchar,
	_description_en				text,
	_ldsity_object				integer,
	_column_expression			text,
	_unit_of_measure			integer,
	_area_domain_category		integer[],
	_sub_population_category	integer[],
	_definition_variant			integer[]
)
returns integer
as
$$
declare
		_etl_id		integer;
begin
	if _ldsity is null
	then
		raise exception 'Error 01: fn_import_save_ldsity: Input argument _ldsity must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_import_save_ldsity: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_import_save_ldsity: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_import_save_ldsity: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_import_save_ldsity: Input argument _description_en must not be NULL!';
	end if;

	if _ldsity_object is null
	then
		raise exception 'Error 06: fn_import_save_ldsity: Input argument _ldsity_object must not be NULL!';
	end if;

	if _unit_of_measure is null
	then
		raise exception 'Error 07: fn_import_save_ldsity: Input argument _unit_of_measure must not be NULL!';
	end if;

	insert into target_data.c_ldsity
	(
		label, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category,
		definition_variant, label_en, description, description_en
	)
	select
		_label, _ldsity_object, _column_expression, _unit_of_measure, _area_domain_category, _sub_population_category,
		_definition_variant, _label_en, _description, _description_en
	returning
			c_ldsity.id
	into
			_etl_id;

	return _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_ldsity(integer,varchar,text,varchar,text,integer,text,integer,integer[],integer[],integer[]) is
'Function inserts a record into table c_ldsity based on given parameters.';

grant execute on function target_data.fn_import_save_ldsity(integer,varchar,text,varchar,text,integer,text,integer,integer[],integer[],integer[]) to public;
-- </function>



-- <function name="fn_import_get_versions" schema="target_data" src="functions/import/fn_import_get_versions.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_versions
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_versions(integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_versions
(
	_id			integer,
	_etl_id		integer[] default null::integer[]
)
returns table
(
	id				integer,
	etl_id			integer,
	label			varchar,
	description		text,
	label_en		varchar,
	description_en	text
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_versions: Input argument _id must not be null!';
		end if;

		if _etl_id is null
		then
			return query
			select
					_id as id,
					cv.id as etl_id,
					cv.label,
					cv.description,
					cv.label_en,
					cv.description_en
			from
					target_data.c_version as cv
			order
					by cv.id;
		else
			return query
			select
					_id as id,
					cv.id as etl_id,
					cv.label,
					cv.description,
					cv.label_en,
					cv.description_en
			from
					target_data.c_version as cv
			where
					cv.id not in (select unnest(_etl_id))
			order
					by cv.id;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_versions(integer, integer[]) is
'Function returns records from table c_version for given input arguments.';

grant execute on function target_data.fn_import_get_versions(integer, integer[]) to public;
-- </function>



-- <function name="fn_import_check_version" schema="target_data" src="functions/import/fn_import_check_version.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_check_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_check_version(integer, varchar) CASCADE;

create or replace function target_data.fn_import_check_version
(
	_id			integer,
	_label_en	varchar
)
returns table
(
	id				integer,
	etl_id			integer,
	label			character varying,
	description		text,
	label_en		character varying,
	description_en	text
)
as
$$
declare
		_res_etl_id				integer;
		_res_label				character varying;
		_res_description		text;
		_res_label_en			character varying;
		_res_description_en		text;
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_check_version: Input argument _id must not be NULL!';
		end if; 
	
		if _label_en is null
		then
			raise exception 'Error 02: fn_import_check_version: Input argument _label_en must not be NULL!';
		end if;
	
		select
				cv.id,
				cv.label,
				cv.description,
				cv.label as label_en,
				cv.description as description_en
		from
				target_data.c_version as cv
		where
				cv.label_en = _label_en
		into
				_res_etl_id,
				_res_label,
				_res_description,
				_res_label_en,
				_res_description_en;

		return query select _id, _res_etl_id, _res_label, _res_description, _res_label_en, _res_description_en;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_check_version(integer, varchar) is
'Function returns record ID from table c_version based on given parameters.';

grant execute on function target_data.fn_import_check_version(integer, varchar) to public;
-- </function>



-- <function name="fn_import_save_version" schema="target_data" src="functions/import/fn_import_save_version.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_version(integer, varchar, text, varchar, text) CASCADE;

create or replace function target_data.fn_import_save_version
(
	_id					integer,
	_label				varchar,
	_description		text,
	_label_en			varchar,
	_description_en		text
)
returns table
(
	id		integer,
	etl_id	integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_import_save_version: Input argument _id must not be NULL!';
	end if;

	if _label is null
	then
		raise exception 'Error 02: fn_import_save_version: Input argument _label must not be NULL!';
	end if;

	if _description is null
	then
		raise exception 'Error 03: fn_import_save_version: Input argument _description must not be NULL!';
	end if;

	if _label_en is null
	then
		raise exception 'Error 04: fn_import_save_version: Input argument _label_en must not be NULL!';
	end if;

	if _description_en is null
	then
		raise exception 'Error 05: fn_import_save_version: Input argument _description_en must not be NULL!';
	end if;

	insert into target_data.c_version(label, description, label_en, description_en)
	select _label, _description, _label_en, _description_en
	returning c_version.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_version(integer, varchar, text, varchar, text) is
'Function inserts a record into table c_version based on given parameters.';

grant execute on function target_data.fn_import_save_version(integer, varchar, text, varchar, text) to public;
-- </function>


-- <function name="fn_import_get_ldsity2panel_refyearset_versions" schema="target_data" src="functions/import/fn_import_get_ldsity2panel_refyearset_versions.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_get_ldsity2panel_refyearset_versions
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_get_ldsity2panel_refyearset_versions(integer, integer, integer, integer, integer[]) CASCADE;

create or replace function target_data.fn_import_get_ldsity2panel_refyearset_versions
(
	_id						integer,
	_etl_ldsity__etl_id		integer,
	_refyearset2panel		integer,
	_etl_version__etl_id	integer,
	_etl_id					integer[] default null::integer[]
)
returns table
(
	id					integer,
	etl_id				integer,
	ldsity				integer,
	refyearset2panel	integer,
	version				integer
)
as
$$
declare
begin
		if _id is null
		then
			raise exception 'Error 01: fn_import_get_ldsity2panel_refyearset_versions: Input argument _id must not be NULL!';
		end if;
	
		if _etl_ldsity__etl_id is null
		then
			raise exception 'Error 02: fn_import_get_ldsity2panel_refyearset_versions: Input argument _etl_ldsity__etl_id must not be NULL!';
		end if;
	
		if _refyearset2panel is null
		then
			raise exception 'Error 03: fn_import_get_ldsity2panel_refyearset_versions: Input argument _refyearset2panel must not be NULL!';
		end if;

		if _etl_version__etl_id is null
		then
			raise exception 'Error 04: fn_import_get_ldsity2panel_refyearset_versions: Input argument _etl_version__etl_id must not be NULL!';
		end if;
	
		return query
		select
				_id as id,
				cl2prv.id as etl_id,
				cl2prv.ldsity,
				cl2prv.refyearset2panel,
				cl2prv.version
		from
				target_data.cm_ldsity2panel_refyearset_version as cl2prv
		where
				cl2prv.id not in (select unnest(_etl_id))
		and		cl2prv.ldsity = _etl_ldsity__etl_id
		and		cl2prv.refyearset2panel = _refyearset2panel
		and		cl2prv.version = _etl_version__etl_id
		
		order by cl2prv.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_get_ldsity2panel_refyearset_versions(integer, integer, integer, integer, integer[]) is
'Function returns records from table cm_ldsity2panel_refyearset_version.';

grant execute on function target_data.fn_import_get_ldsity2panel_refyearset_versions(integer, integer, integer, integer, integer[]) to public;
-- </function>



-- <function name="fn_import_save_ldsity2panel_refyearset_version" schema="target_data" src="functions/import/fn_import_save_ldsity2panel_refyearset_version.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_import_save_ldsity2panel_refyearset_version
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_import_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) CASCADE;

create or replace function target_data.fn_import_save_ldsity2panel_refyearset_version
(
	_id					integer,
	_ldsity				integer,
	_refyearset2panel	integer,
	_version			integer
)
returns table
(
	id			integer,
	etl_id		integer
)
as
$$
declare
		_etl_id		integer;
begin
	if _id is null
	then
		raise exception 'Error 01: fn_import_save_ldsity2panel_refyearset_version: Input argument _id must not be NULL!';
	end if;

	if _ldsity is null
	then
		raise exception 'Error 02: fn_import_save_ldsity2panel_refyearset_version: Input argument _ldsity must not be NULL!';
	end if;

	if _refyearset2panel is null
	then
		raise exception 'Error 03: fn_import_save_ldsity2panel_refyearset_version: Input argument _refyearset2panel must not be NULL!';
	end if;

	if _version is null
	then
		raise exception 'Error 04: fn_import_save_ldsity2panel_refyearset_version: Input argument _version must not be NULL!';
	end if;

	insert into target_data.cm_ldsity2panel_refyearset_version(ldsity, refyearset2panel, version)
	select _ldsity, _refyearset2panel, _version
	returning cm_ldsity2panel_refyearset_version.id
	into _etl_id;

	return query select _id, _etl_id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_import_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) is
'Function inserts a record into table cm_ldsity2panel_refyearset_version based on given parameters.';

grant execute on function target_data.fn_import_save_ldsity2panel_refyearset_version(integer, integer, integer, integer) to public;
-- </function>