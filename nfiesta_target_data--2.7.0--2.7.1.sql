--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


-- <function name="fn_add_plot_target_attr" schema="target_data" src="functions/additivity/fn_add_plot_target_attr.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_add_plot_target_attr
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean);

CREATE OR REPLACE FUNCTION target_data.fn_add_plot_target_attr
(
	IN variables integer[],
	IN plots integer[] default NULL,
	IN refyearsets integer[] default NULL,
	IN min_diff double precision default 0.0,
	IN include_null_diff boolean default true
)
  RETURNS TABLE
(
	plot				integer,
	reference_year_set	integer,
	variable			integer,
	ldsity				double precision,
	ldsity_sum			double precision,
	variables_def		integer[],
	variables_found		integer[],
	diff				double precision	
) AS
$BODY$
DECLARE
	_complete_query text;
	_array_text_v text;
	_array_text_p text;
	_array_text_r text;

BEGIN
	--------------------------------QUERY--------------------------------
	_array_text_v := concat(quote_literal(variables::text), '::integer[]');

	if plots is not null then
		_array_text_p := concat(' AND f_p_plot.gid = ANY (', quote_literal(plots::text), '::integer[])');
	else
		_array_text_p := '';
	end if;

	if refyearsets is not null then
		_array_text_r := concat(' AND t_available_datasets.reference_year_set = ANY (', quote_literal(refyearsets::text), '::integer[])');
	else
		_array_text_r := '';
	end if;

	_complete_query :=
	'
	with
	w_plot_var as materialized
					(
					select
							f_p_plot.gid as plot,
							t_available_datasets.categorization_setup as variable,
							t_available_datasets.panel,
							t_available_datasets.reference_year_set,
							coalesce(t_ldsity_values.value, 0) as value
					from sdesign.f_p_plot
					inner join sdesign.t_cluster on (f_p_plot.cluster = t_cluster.id)
					inner join sdesign.cm_cluster2panel_mapping on (t_cluster.id = cm_cluster2panel_mapping.cluster)
					inner join sdesign.t_panel on (cm_cluster2panel_mapping.panel = t_panel.id)
					inner join target_data.t_available_datasets on (t_panel.id = t_available_datasets.panel) -- select * from target_data.t_available_datasets
					inner join target_data.t_categorization_setup on (t_available_datasets.categorization_setup = t_categorization_setup.id)
					left join target_data.t_ldsity_values on (
						f_p_plot.gid = t_ldsity_values.plot
						and t_available_datasets.id = t_ldsity_values.available_datasets
						and t_ldsity_values.is_latest)
					WHERE t_available_datasets.categorization_setup = ANY (' || _array_text_v || ')	
							' || _array_text_p || '
							' || _array_text_r || '
					)
	,w_node_sum as	(
					select
						plot,
						w_plot_var.reference_year_set,
						w_plot_var.variable,
						value as node_sum,
						v_variable_hierarchy.node,
						v_variable_hierarchy.edges as edges_def
					from w_plot_var
					inner join target_data.v_variable_hierarchy on (v_variable_hierarchy.node = w_plot_var.variable and 
											v_variable_hierarchy.panel = w_plot_var.panel and
											v_variable_hierarchy.reference_year_set = w_plot_var.reference_year_set)
					where (v_variable_hierarchy.edges <@ ' || _array_text_v || ')
					)
	,w_edge_sum as	(
					select
						w_node_sum.plot,
						w_node_sum.reference_year_set,
						w_node_sum.node,
						w_node_sum.node_sum,
						w_node_sum.edges_def,
						array_agg(w_plot_var.variable order by w_plot_var.variable) as edges_found,
						sum(w_plot_var.value) as edges_sum
					from w_node_sum
					left join w_plot_var on (
						w_plot_var.plot = w_node_sum.plot
						and w_plot_var.reference_year_set = w_node_sum.reference_year_set
						and w_plot_var.variable = any(w_node_sum.edges_def))
					group by w_node_sum.plot, w_node_sum.reference_year_set,
						w_node_sum.node, w_node_sum.node_sum, w_node_sum.edges_def
					)
	,w_diff as		(
					select
						plot,
						reference_year_set,
						node		as variable,
						node_sum	as ldsity,
						edges_sum	as ldsity_sum,
						edges_def	as variables_def,
						edges_found	as variables_found,
						case
							when edges_sum != 0.0 and node_sum = 0.0 then 1.0
							when node_sum = 0.0 and edges_sum = 0.0 then 0
							else abs( (node_sum - edges_sum) / node_sum )
						end as diff
					from w_edge_sum
					)
	select * from w_diff
	where
		(' || include_null_diff || ' and ((diff >= ' || min_diff || ') or (diff is null)))
		or
		(not ' || include_null_diff || ' and (diff >= ' || min_diff || '))
	order by diff desc
	;
	';
	--raise notice '%		fn_add_plot_target_attr -- %', TimeOfDay(), _complete_query;
	RETURN QUERY EXECUTE _complete_query;
END;
$BODY$
  LANGUAGE plpgsql STABLE
  COST 100
  ROWS 100000;
  
comment on function target_data.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean) is
'Function showing plot level target local density attribute additivity (aggregated class estimate should be equal to sum of sub-classes estimates) of target local densities. Hierarchy between aggregated classes and its sub-classes is defined in v_variable_hierarchy.
Function input argument is:
 * Array of attributes -- variables (FKEY to t_variable.id). All variables to be checked (aggregated class together with sub-classes) need to be passed.
 * Array of plots -- plots (FKEY to f_p_plot.gid). If NULL all available plots are checked.
 * Array of reference year sets -- reference year sets (FKEY to t_reference_year_set.id). If NULL all available reference year sets are checked.
 * Minimal amount of difference [%].
 * Whether include NULL difference -- indicating defined sub-classes missing in data.
Resulting table has following columns:
 * Plot. FKEY to f_p_plot.gid.
 * Reference year set. FKEY to t_reference_year_set.id.
 * Target total attribute -- variable. FKEY to t_variable.id.
 * Aggregated class local density.
 * Sum of sub-classes local densities (belonging to aggregated class).
 * Attributes -- variables defined in hierarchy (v_variable_hierarchy). Array of FKEYs to t_variable.id.
 * Attributes -- variables found in data (t_auxiliary_data). Array of FKEYs to t_variable.id.
 * Relative difference between aggregated class local densities and sum of sub-classes local densities.';

GRANT EXECUTE ON FUNCTION target_data.fn_add_plot_target_attr(integer[], integer[], integer[], double precision, boolean) TO PUBLIC;

-- </function>
