--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_etl_get_variables" schema="target_data" src="functions/etl/fn_etl_get_variables.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_variables
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_variables(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_get_variables
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns json
as
$$
declare
		_country_array							integer[];
		_res_country							varchar;
		_export_connection						integer;
		_target_variable						integer;
		_etl_id									integer;
		_res									json;

		_core_and_division						boolean;
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_variables: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_variables: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;

		select array_agg(tss.country order by tss.country) from sdesign.t_strata_set as tss where tss.id in
		(select strata_set from sdesign.t_stratum where id in
		(select stratum from sdesign.t_panel
		where id in ((select panel from sdesign.cm_refyearset2panel_mapping where id in (select unnest(array[_refyearset2panel_mapping]))))))
		into _country_array;
	
		if array_length(_country_array,1) is distinct from 1
		then
			raise exception 'Error 03: fn_etl_get_variables: Input argument _refyearset2panel_mapping contains values for two or more different countries!';
		end if;
	
		select label from sdesign.c_country where id = _country_array[1]
		into _res_country;
	
		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w_etl_ad as		(
						select * from target_data.t_etl_area_domain	where export_connection = _export_connection
						)
		,w_etl_adc as	(
						select * from target_data.t_etl_area_domain_category where etl_area_domain in (select id from w_etl_ad)
						)
		,w_etl_sp as	(
						select * from target_data.t_etl_sub_population where export_connection = _export_connection
						)
		,w_etl_spc as	(
						select * from target_data.t_etl_sub_population_category where etl_sub_population in (select id from w_etl_sp)
						)		
		,w1 as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups for target variable that is reduced by
				 -- combination of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(-- list of IDs of available datasets from t_ldsity_values table
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select w2.* from w2 where w2.id in (select w3.available_datasets from w3)
				)
		,w5 as	(
				select
						w4.panel,
						w4.reference_year_set,
						array_agg(w4.categorization_setup order by w4.categorization_setup) as categorization_setup
				from
						w4 group by w4.panel, w4.reference_year_set
				)
		,w6 as	(
				select
						w5.*,
						target_data.fn_etl_supplement_categorization_setups(w5.categorization_setup) as categorization_setup_supplement
				from
						w5
				)
		,w7 as	(
				select
						w6.panel,
						w6.reference_year_set,
						unnest(w6.categorization_setup_supplement) as categorization_setup_supplement
				from
						w6
				)
		,w8 as	(
				select
					id,
					ldsity2target_variable,
					categorization_setup,
					_core_and_division as core_and_division,				
					(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type as id_spt_orig,
					(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type as id_adt_orig,
					(target_data.fn_get_category4classification_rule_id('spc', t.spc2classification_rule)).id_category as id_spc_orig,
					(target_data.fn_get_category4classification_rule_id('adc', t.adc2classification_rule)).id_category as id_adc_orig,
					(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_spt_orig,
					(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_adt_orig,
					(target_data.fn_get_category4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_spc_orig,
					(target_data.fn_get_category4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_adc_orig
				from
					target_data.cm_ldsity2target2categorization_setup as t
				where
					ldsity2target_variable
				in
						(
						select id from target_data.cm_ldsity2target_variable
						where target_variable = _target_variable
						)
				and categorization_setup in (select distinct w7.categorization_setup_supplement from w7)
				)		
		,w9 as	(
				select
						w7.*,
						w8.*,
						cltv.ldsity_object_type as core_or_division
				from
						w7
						inner join w8 on w7.categorization_setup_supplement = w8.categorization_setup
						inner join target_data.cm_ldsity2target_variable as cltv on w8.ldsity2target_variable = cltv.id
				)
		,w10 as	(				
				select
						w9.*,
						-------------
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w2a.id_spt_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w2a.id_spc_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w2a.id_adt_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w2a.id_adc_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adc,
						-------------
						case
							when core_and_division = false
								then label_spt_orig
								else
									case
										when core_or_division = 200
										then label_spt_orig
										else (select distinct w2a.label_spt_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 200)
									end
						end as label_spt,
						
						case
							when core_and_division = false
								then label_spc_orig
								else
									case
										when core_or_division = 200
										then label_spc_orig
										else (select distinct w2a.label_spc_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 200)
									end
						end as label_spc,
						
						case
							when core_and_division = false
								then label_adt_orig
								else
									case
										when core_or_division = 100
										then label_adt_orig
										else (select distinct w2a.label_adt_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 100)
									end
						end as label_adt,
						
						case
							when core_and_division = false
								then label_adc_orig
								else
									case
										when core_or_division = 100
										then label_adc_orig
										else (select distinct w2a.label_adc_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 100)
									end
						end as label_adc						
				from
						w9
				)
		,w11 as	(
				select distinct panel, reference_year_set, categorization_setup, id_spt, id_adt, id_spc, id_adc, label_spt, label_adt, label_spc, label_adc from w10 order by categorization_setup
				)
		,w12 as	(
				select
						w11.*,
						w_etl_ad.etl_id as etl_id_ad,
						w_etl_sp.etl_id as etl_id_sp,
						w_etl_adc.etl_id as etl_id_adc,
						w_etl_spc.etl_id as etl_id_spc,
						tp.panel as panel__label,
						trys.reference_year_set as reference_year_set__label
				from
							w11
				left join	w_etl_ad	on w11.id_adt = w_etl_ad.area_domain
				left join	w_etl_sp	on w11.id_spt= w_etl_sp.sub_population
				left join 	w_etl_adc	on w11.id_adc = w_etl_adc.area_domain_category
				left join	w_etl_spc	on w11.id_spc = w_etl_spc.sub_population_category
				
				inner join sdesign.t_panel as tp on w11.panel = tp.id
				inner join sdesign.t_reference_year_set as trys on w11.reference_year_set = trys.id 
				)
		,w13 as	(
				select
						w12.panel__label,
						w12.reference_year_set__label,
						case when label_spt is null then 'null'::varchar else label_spt end as variables_sp,
						case when label_adt is null then 'null'::varchar else label_adt end as variables_ad,
						case when label_spc is null then 'null'::varchar else label_spc end as variables_spc,
						case when label_adc is null then 'null'::varchar else label_adc end as variables_adc,					
						case when etl_id_sp is null then 0 else etl_id_sp end as etl_id_sp,
						case when etl_id_spc is null then 0 else etl_id_spc end as etl_id_spc,
						case when etl_id_ad is null then 0 else etl_id_ad end as etl_id_ad,
						case when etl_id_adc is null then 0 else etl_id_adc end as etl_id_adc
				from
						w12
				)
		select
				json_build_object
				(
				'country', _res_country,
				'target_variable', _etl_id,
				'variables',json_agg(json_build_object(
					'panel', w13.panel__label,
					'reference_year_set', w13.reference_year_set__label,
					'sub_population',  w13.variables_sp,
					'sub_population_category', w13.variables_spc,
					'area_domain',  w13.variables_ad,
					'area_domain_category', w13.variables_adc,
					'sub_population_etl_id', w13.etl_id_sp,
					'sub_population_category_etl_id', w13.etl_id_spc,
					'area_domain_etl_id', w13.etl_id_ad,
					'area_domain_category_etl_id', w13.etl_id_adc))
				)
		from w13
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_variables(integer[], integer) is
'Function returns variables for given input arguments.';

grant execute on function target_data.fn_etl_get_variables(integer[], integer) to public;
-- </function>

