--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) CASCADE;
DROP FUNCTION IF EXISTS target_data.fn_etl_get_variables_false(JSON, varchar, varchar, character varying) CASCADE;



-- <function name="fn_etl_get_target_variable" schema="target_data" src="functions/etl/fn_etl_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) CASCADE;

create or replace function target_data.fn_etl_get_target_variable
(
	_export_connection	integer,
	_national_language	character varying(2) default 'en'::character varying(2),
	_etl				boolean default null::boolean,
	_target_variable	integer default null::integer
)
returns table
(
	id							integer,
	label						varchar,
	description					text,
	label_en					varchar,
	description_en				text,
	id_etl_target_variable		integer,
	check_target_variable		boolean,
	refyearset2panel_mapping	integer[],
	metadata					json
)
as
$$
declare
		_cond_1						text;
		_cond_2						text;
		_set_column_label			text;
		_set_column_label_en		text;
		_set_column_description		text;
		_set_column_description_en	text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable: Input argument _export_connection must not be null!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_get_target_variable: Input argument _national_language must not be null!';
		end if;

		if _etl is null
		then
			_cond_1 := 'TRUE';
		else
			if _etl = true
			then
				_cond_1 := 'w2.check_target_variable = TRUE';
			else
				_cond_1 := 'w2.check_target_variable = FALSE';
			end if;
		end if;

		if _national_language = 'en'
		then
			_set_column_label := 'w1.label_en';
			_set_column_label_en := 'w1.label_en';
			_set_column_description := 'w1.description_en';
			_set_column_description_en := 'w1.description_en';			
		else
			_set_column_label := 'w1.label';
			_set_column_label_en := 'w1.label_en';
			_set_column_description := 'w1.description';
			_set_column_description_en := 'w1.description_en';
		end if;

		if _target_variable is null
		then
			_cond_2 := 'TRUE';
		else
			_cond_2 := 'ctv.id = $3';
		end if;

		return query execute
		'
		with
		w1 as	(
				select 
						ctv.*,
						(target_data.fn_etl_check_target_variable($1,ctv.id)) as res
				from 
						target_data.c_target_variable as ctv
				where
						'|| _cond_2 ||'
				order
						by ctv.id
				)
		,w2 as	(
				select
						w1.id,
						'|| _set_column_label ||' as label,
						'|| _set_column_description ||' as description,
						'|| _set_column_label_en ||' as label_en,
						'|| _set_column_description_en ||' as description_en,
						(w1.res).id_etl_target_variable,
						(w1.res).check_target_variable,
						(w1.res).refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select
						w2.*,
						target_data.fn_etl_get_target_variable_metadata(w2.id,$2) as metadata
				from
						w2 where '|| _cond_1 ||'
				)
		select
				w3.id,
				w3.label,
				w3.description,
				w3.label_en,
				w3.description_en,
				w3.id_etl_target_variable,
				w3.check_target_variable,
				w3.refyearset2panel_mapping,
				w3.metadata
		from
				w3 order by w3.id;
		'
		using _export_connection, _national_language, _target_variable;

end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) IS
'The function returs target variables and informations for ETL. If input argument _etl = TRUE then function
returns target variables that was ETL and all theirs datas ar current. If input argument _etl = FALSE
then function retursn target variables that was not ETL yet or returns target variables that was ETL
and exists newer datas for ETL. If input argument _etl IS NULL (_etl = true or false) then function
returns both cases.';

grant execute on function target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) to public;
-- </function>



-- <function name="fn_etl_get_variables_false" schema="target_data" src="functions/etl/fn_etl_get_variables_false.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_variables_false
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_variables_false(JSON, varchar, varchar, character varying) CASCADE;

create or replace function target_data.fn_etl_get_variables_false
(
	_variables				JSON,
	_panel					varchar,
	_reference_year_set		varchar,
	_national_language		character varying(2) default 'en'::character varying(2)
)
returns table
(
	sub_population				varchar,
	sub_population_category		varchar,	
	area_domain					varchar,
	area_domain_category		varchar,
	sub_population_en			varchar,
	sub_population_category_en	varchar,	
	area_domain_en				varchar,
	area_domain_category_en		varchar
)
as
$$
declare
begin		
		if _variables is null
		then
			raise exception 'Error 01: fn_etl_get_variables_false: Input argument _variables must not be NULL!';
		end if;

		if _panel is null
		then
			raise exception 'Error 02: fn_etl_get_variables_false: Input argument _panel must not be NULL!';
		end if;

		if _reference_year_set is null
		then
			raise exception 'Error 03: fn_etl_get_variables_false: Input argument _reference_year_set must not be NULL!';
		end if;		

		if _national_language = 'en'
		then
			return query
			with
				w1 as	(
						select json_array_elements(_variables) as s
						)
				,w2 as	(
						select
								(s->>'panel')::varchar							as panel,
								(s->>'reference_year_set')::varchar				as reference_year_set,
								(s->>'sub_population_en')::varchar				as sub_population,
								(s->>'sub_population_category_en')::varchar		as sub_population_category,
								(s->>'area_domain_en')::varchar					as area_domain,
								(s->>'area_domain_category_en')::varchar		as area_domain_category,
								(s->>'sub_population_en')::varchar				as sub_population_en,
								(s->>'sub_population_category_en')::varchar		as sub_population_category_en,
								(s->>'area_domain_en')::varchar					as area_domain_en,
								(s->>'area_domain_category_en')::varchar		as area_domain_category_en			
						from w1
						)
				,w3 as	(
						select w2.* from w2
						where w2.panel = _panel
						and w2.reference_year_set = _reference_year_set
						)
				select
						w3.sub_population,
						w3.sub_population_category,
						w3.area_domain,
						w3.area_domain_category,
						w3.sub_population_en,
						w3.sub_population_category_en,
						w3.area_domain_en,
						w3.area_domain_category_en
				from
						w3;
		else
			return query
			with
				w1 as	(
						select json_array_elements(_variables) as s
						)
				,w2 as	(
						select
								(s->>'panel')::varchar							as panel,
								(s->>'reference_year_set')::varchar				as reference_year_set,
								(s->>'sub_population')::varchar					as sub_population,
								(s->>'sub_population_category')::varchar		as sub_population_category,
								(s->>'area_domain')::varchar					as area_domain,
								(s->>'area_domain_category')::varchar			as area_domain_category,
								(s->>'sub_population_en')::varchar				as sub_population_en,
								(s->>'sub_population_category_en')::varchar		as sub_population_category_en,
								(s->>'area_domain_en')::varchar					as area_domain_en,
								(s->>'area_domain_category_en')::varchar		as area_domain_category_en						
						from w1
						)
				,w3 as	(
						select w2.* from w2
						where w2.panel = _panel
						and w2.reference_year_set = _reference_year_set
						)
				select
						w3.sub_population,
						w3.sub_population_category,
						w3.area_domain,
						w3.area_domain_category,
						w3.sub_population_en,
						w3.sub_population_category_en,
						w3.area_domain_en,
						w3.area_domain_category_en
				from
						w3;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_variables_false(JSON, varchar, varchar, character varying) is
'Function returns list of attribute variables that must be still implemented for given combination of panel and reference year set for selected target variable.';

grant execute on function target_data.fn_etl_get_variables_false(JSON, varchar, varchar, character varying) to public;
-- </function>



-- <function name="fn_etl_get_etl_ids_of_target_variable" schema="target_data" src="functions/etl/fn_etl_get_etl_ids_of_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_etl_ids_of_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_etl_ids_of_target_variable(integer) CASCADE;

create or replace function target_data.fn_etl_get_etl_ids_of_target_variable
(
	_export_connection	integer
)
returns integer[]
as
$$
declare
	_res integer[];
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_etl_ids_of_target_variable: Input argument _export_connection must not be NULL!';
		end if;

		if not exists (select t1.* from target_data.t_export_connection as t1 where t1.id = _export_connection)
		then
			raise exception 'Error 02: fn_etl_get_etl_ids_of_target_variable: Given export connection (%) does not exist in t_export_connection table.', _export_connection;
		end if;

		select array_agg(tetv.etl_id order by tetv.etl_id)
		from target_data.t_etl_target_variable as tetv
		where tetv.export_connection = _export_connection
		into _res;
	
		return _res;	
end;
$$

language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_etl_ids_of_target_variable(integer) is
'The function gets an array of etl_ids for target variables that was ETLed yet.';

grant execute on function target_data.fn_etl_get_etl_ids_of_target_variable(integer) to public;
-- </function>