--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_etl_export_ldsity_values" schema="target_data" src="functions/etl/fn_etl_export_ldsity_values.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_ldsity_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_ldsity_values(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_ldsity_values
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_available_datasets			integer[];
		_categorization_setups		integer[];
		_res_available_datasets		json;
		_res_ldsity_values			json;
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_ldsity_values: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_ldsity_values: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;			


		-------------------------------------------------------------------------------------------
		-- LDSITY VALUES => get available data sets and categorization setups FROM t_ldsity_values
		-------------------------------------------------------------------------------------------
		with
		w1 as	(-- all categorization setups for input target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups reduced by combinations of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(-- list of IDs of available datasets from t_ldsity_values table
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		select array_agg(w3.available_datasets order by w3.available_datasets) from w3
		into _available_datasets;
		
		with
		w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select unnest(_available_datasets))
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_etl_export_ldsity_values: Internal argument _categorization_setups must not by NULL!';
		end if;
		-------------------------------------------------------------------------------------------


		-------------------------------------------------------------
		-- AVAILABLE DATASETS => get all available data sets
		-------------------------------------------------------------
		with
		w1 as	(-- all categorization setups for input target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups reduced by combinations of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(
				select
						w2.id as id_available_datasets,
						w2.panel,
						w2.reference_year_set,
						cmltv.id as id_cm_ldsity2target2categorization_setup,
						cmltv.ldsity2target_variable,
						cmltv.adc2classification_rule,
						cmltv.spc2classification_rule,
						cmltv.categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', cmltv.spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', cmltv.spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', cmltv.adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', cmltv.adc2classification_rule)).id_category as id_adc_orig				
				from
						w2
						inner join target_data.cm_ldsity2target2categorization_setup as cmltv
						on w2.categorization_setup = cmltv.categorization_setup
				)
		,w4 as	(
				select
						w3.*,
						cltv.ldsity_object_type as core_or_division
				from
						w3
						inner join target_data.cm_ldsity2target_variable as cltv on w3.ldsity2target_variable = cltv.id
				)
		,w5 as	(				
				select
						w4.*,
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w4a.id_spt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w4a.id_spc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w4a.id_adt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w4a.id_adc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adc
				from
						w4
				)		
		,w6 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w5
				)
		,w7 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from w6 as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(t.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(t.id_adt,tead.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						case when tespc.etl_id is null then 0 else tespc.etl_id end as etl_id_spc,
						case when teadc.etl_id is null then 0 else teadc.etl_id end as etl_id_adc
				from w7
				
				left join target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc,tespc.sub_population_category)
				
				left join target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc,teadc.area_domain_category)
				)
		,w9 as	(
				select distinct t.t_panel__id, t.t_panel__panel, t.t_stratum__id, t.t_cluster_configuration__id, t.t_cluster_configuration__cluster_configuration,
				t.t_stratum__stratum, t.t_strata_set__id, t.t_strata_set__strata_set, t.c_country__id, t.c_country__label,
				t.t_reference_year_set__id, t.t_reference_year_set__reference_year_set, t.t_inventory_campaign__id, t.t_inventory_campaign__inventory
				from
						(
						select		
								t_panel.id as t_panel__id,
								t_panel.panel as t_panel__panel,
								t_cluster.id as t_cluster__id,
								t_cluster.cluster as t_cluster__cluster,
								f_p_plot.gid as f_p_plot__gid,
								f_p_plot.plot as f_p_plot__plot,
								t_cluster_configuration.id as t_cluster_configuration__id,
								t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
								t_stratum.id as t_stratum__id,
								t_stratum.stratum as t_stratum__stratum,
								t_strata_set.id as t_strata_set__id,
								t_strata_set.strata_set as t_strata_set__strata_set,
								c_country.id as c_country__id,
								c_country.label as c_country__label,
								t_reference_year_set.id as t_reference_year_set__id,
								t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
								t_inventory_campaign.id as t_inventory_campaign__id,
								t_inventory_campaign.inventory as t_inventory_campaign__inventory
						from
									sdesign.cm_refyearset2panel_mapping
						inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
						inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
						inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
						inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
						inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
						inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
						inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
						inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
						inner join	sdesign.c_country on t_strata_set.country = c_country.id
						inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
						inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
						where
								cm_refyearset2panel_mapping.id in (select unnest(_refyearset2panel_mapping))
						) as t
				)
		,w10 as	(
				select
						_etl_id as etl_id_tv,
						w9.c_country__label as country,
						w9.t_strata_set__strata_set as strata_set,
						w9.t_stratum__stratum as stratum,
						w9.t_panel__panel as panel,
						w9.t_reference_year_set__reference_year_set as reference_year_set,
						w9.t_cluster_configuration__cluster_configuration as cluster_configuration,
						w9.t_inventory_campaign__inventory as inventory_campaign,
						w8.etl_id_spc,
						w8.etl_id_adc
				from
						w5
				inner join	w8 	on w5.categorization_setup = w8.categorization_setup
				inner join	w9	on w5.panel = w9.t_panel__id
								and w5.reference_year_set = w9.t_reference_year_set__id
				)				
		,w11 as	(
				select
						w10.etl_id_tv,
						w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster_configuration,
						w10.inventory_campaign,
						w10.etl_id_spc,
						w10.etl_id_adc
				from
						w10
				order
				by		w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster_configuration,
						w10.inventory_campaign,
						w10.etl_id_tv,
						w10.etl_id_spc,
						w10.etl_id_adc 
				)
		select
			json_agg(json_build_object(
				'target_variable',   		w11.etl_id_tv,
				'country',					w11.country,
				'strata_set',				w11.strata_set,
				'stratum',					w11.stratum,
				'reference_year_set',		w11.reference_year_set,
				'panel',					w11.panel,
				'cluster_configuration',	w11.cluster_configuration,
				'inventory_campaign',		w11.inventory_campaign,
				'sub_population_category',	w11.etl_id_spc,
				'area_domain_category',		w11.etl_id_adc
				))
		from
				w11 into _res_available_datasets;
		-------------------------------------------------------------


		-------------------------------------------------------------
		-- LDSITY VALUES => get ldsity values
		-------------------------------------------------------------
		with
		w1 as	(
				select
						tad.id as id_available_datasets,
						tad.panel,
						tad.reference_year_set,
						tad.categorization_setup
				from
						target_data.t_available_datasets as tad
				where
						tad.id in (select unnest(_available_datasets))
				)
		,w2 as	(
				select t1.*, w1.*
				from target_data.t_ldsity_values as t1
				inner join w1 on t1.available_datasets = w1.id_available_datasets
				where t1.is_latest = true
				)
		,w3 as	(
				select
						id as id_cm_ldsity2target2categorization_setup,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig				
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest(_categorization_setups))
				)
		,w4 as	(
				select
						w3.*,
						cltv.ldsity_object_type as core_or_division
				from
						w3
						inner join target_data.cm_ldsity2target_variable as cltv on w3.ldsity2target_variable = cltv.id
				)
		,w5 as	(				
				select
						w4.*,
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w4a.id_spt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w4a.id_spc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w4a.id_adt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w4a.id_adc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adc
				from
						w4
				)
		,w6 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w5
				)
		,w7 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from w6 as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(t.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(t.id_adt,tead.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						case when tespc.etl_id is null then 0 else tespc.etl_id end as etl_id_spc,
						case when teadc.etl_id is null then 0 else teadc.etl_id end as etl_id_adc
				from w7
				
				left join target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc,tespc.sub_population_category)
				
				left join target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc,teadc.area_domain_category)
				)
		,w9 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory as t_inventory_campaign__inventory
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_refyearset2panel_mapping))
				)
		,w10 as	(
				select
						_etl_id as etl_id_tv,
						w9.c_country__label as country,
						w9.t_strata_set__strata_set as strata_set,
						w9.t_stratum__stratum as stratum,
						w9.t_panel__panel as panel,
						w9.t_reference_year_set__reference_year_set as reference_year_set,
						w9.t_cluster__cluster as cluster,
						w9.t_cluster_configuration__cluster_configuration as cluster_configuration,
						w9.t_inventory_campaign__inventory as inventory_campaign,
						w9.f_p_plot__plot as plot,
						w8.etl_id_spc,
						w8.etl_id_adc,
						w2.value
				from
						w2
				inner join	w8 	on w2.categorization_setup = w8.categorization_setup
				inner join	w9	on w2.plot = w9.f_p_plot__gid
								and w2.panel = w9.t_panel__id
								and w2.reference_year_set = w9.t_reference_year_set__id
				)
		,w11 as	(
				select
						w10.etl_id_tv,
						w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster,
						w10.cluster_configuration,
						w10.inventory_campaign,						
						w10.plot,
						w10.etl_id_spc,
						w10.etl_id_adc,
						w10.value
				from
						w10
				order
				by		w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster,
						w10.cluster_configuration,
						w10.inventory_campaign,
						w10.etl_id_tv,
						w10.plot,
						w10.etl_id_spc,
						w10.etl_id_adc 
				)
		select
			json_agg(json_build_object(
				'target_variable',   		w11.etl_id_tv,
				'country',					w11.country,
				'strata_set',				w11.strata_set,
				'stratum',					w11.stratum,
				'reference_year_set',		w11.reference_year_set,
				'panel',					w11.panel,
				'cluster',					w11.cluster,
				'cluster_configuration',	w11.cluster_configuration,
				'inventory_campaign',		w11.inventory_campaign,
				'plot',						w11.plot,
				'sub_population_category',	w11.etl_id_spc,
				'area_domain_category',		w11.etl_id_adc,
				'value',					w11.value))
		from
				w11 into _res_ldsity_values;
		-------------------------------------------------------------


		-------------------------------------------------------------
		-- JOIN elements
		-------------------------------------------------------------
		select json_build_object
			(
				'available_datasets',_res_available_datasets,
				'ldsity_values',_res_ldsity_values
			)
		into _res;
		-------------------------------------------------------------
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_ldsity_values(integer[], integer) is
'Function returns records for ETL t_target_data and t_available_datasets table.';

grant execute on function target_data.fn_etl_export_ldsity_values(integer[], integer) to public;
-- </function>