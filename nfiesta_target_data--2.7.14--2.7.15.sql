--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



DROP FUNCTION IF EXISTS target_data.fn_etl_get_export_connections(integer) CASCADE;



-- <function name="fn_etl_get_export_connections" schema="target_data" src="functions/etl/fn_etl_get_export_connections.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_export_connections
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_export_connections() CASCADE;

create or replace function target_data.fn_etl_get_export_connections()
returns table
(
	id				integer,
	host			character varying,
	dbname			character varying,
	port			integer,
	comment			text
)
as
$$
begin
	return query
	select t.id, t.host, t.dbname, t.port, t.comment
	from target_data.t_export_connection as t
	order by t.id;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_export_connections() is
'The function returns records from t_export_connection table.';

grant execute on function target_data.fn_etl_get_export_connections() to public;
-- </function>



-- <function name="fn_etl_update_export_connection" schema="target_data" src="functions/etl/fn_etl_update_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_update_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_etl_update_export_connection(integer, text);

CREATE OR REPLACE FUNCTION target_data.fn_etl_update_export_connection
(
	_id integer,
	_comment text
)
RETURNS void
AS
$$
BEGIN
	if _id is null
	then
		RAISE EXCEPTION 'Error 01: fn_etl_update_export_connection: Input argument _id must not be NULL!';
	end if;

	IF NOT EXISTS (SELECT t1.* FROM target_data.t_export_connection as t1 where t1.id = _id)
	THEN RAISE EXCEPTION 'Error 02: fn_etl_update_export_connection: Given export connection (id = %) does not exist in t_export_connection table!', _id;
	END IF;

	if _comment is null
	then
		RAISE EXCEPTION 'Error 03: fn_etl_update_export_connection: Input argument _comment must not be NULL!';
	end if;

	update target_data.t_export_connection set comment = _comment where id = _id;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_etl_update_export_connection(integer, text) IS
'The function provides update attribute "comment" in t_export_connection table.';

GRANT EXECUTE ON FUNCTION target_data.fn_etl_update_export_connection(integer, text) TO public;
-- </function>



-- <function name="fn_etl_try_delete_export_connection" schema="target_data" src="functions/etl/fn_etl_try_delete_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_try_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_try_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_etl_try_delete_export_connection(_id integer)
RETURNS boolean
AS
$$
BEGIN
	if _id is null
	then
		RAISE EXCEPTION 'Error 01: fn_etl_try_delete_export_connection: Input argument _id must not be NULL!';
	end if;

	IF NOT EXISTS (SELECT t.* FROM target_data.t_export_connection as t where t.id = _id)
	THEN RAISE EXCEPTION 'Error 02: fn_etl_try_delete_export_connection: Given export connection (_id = %) does not exist in t_export_connection table!', _id;
	END IF;

	return not exists (
	select tetv.id from target_data.t_etl_target_variable as tetv where tetv.export_connection = _id	union all
	select tead.id from target_data.t_etl_area_domain as tead where tead.export_connection = _id		union all
	select tesp.id from target_data.t_etl_sub_population as tesp where tesp.export_connection = _id
	);

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_etl_try_delete_export_connection(integer) IS
'The function provides test if it is possible to delete record from t_export_connection table.';

GRANT EXECUTE ON FUNCTION target_data.fn_etl_try_delete_export_connection(integer) TO public;
-- </function>



-- <function name="fn_etl_delete_export_connection" schema="target_data" src="functions/etl/fn_etl_delete_export_connection.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_delete_export_connection
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_etl_delete_export_connection(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_etl_delete_export_connection(_id integer)
RETURNS void
AS
$$
BEGIN

	IF _id IS NULL THEN 
		RAISE EXCEPTION 'Error: 01: fn_etl_delete_export_connection: Input argument _id must not be NULL!';
	END IF;

	IF NOT EXISTS (SELECT t1.* FROM target_data.t_export_connection AS t1 WHERE t1.id = _id)
		THEN RAISE EXCEPTION 'Error: 02: fn_etl_delete_export_connection: Given export connection (%) does not exist in t_export_connection table.', _id;
	END IF;
	
	DELETE FROM target_data.t_export_connection 
	WHERE id  = _id;

END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_etl_delete_export_connection(integer) IS
'The function delete record from t_export_connection.';

GRANT EXECUTE ON FUNCTION target_data.fn_etl_delete_export_connection(integer) TO public;
-- </function>