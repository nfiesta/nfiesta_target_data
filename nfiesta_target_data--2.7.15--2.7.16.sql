--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) CASCADE;
DROP FUNCTION IF EXISTS target_data.fn_etl_check_area_domain(integer[], integer) CASCADE;
DROP FUNCTION IF EXISTS target_data.fn_etl_check_sub_population(integer[], integer) CASCADE;


-- <function name="fn_etl_get_target_variable" schema="target_data" src="functions/etl/fn_etl_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) CASCADE;

create or replace function target_data.fn_etl_get_target_variable
(
	_export_connection	integer,
	_national_language	character varying(2) default 'en'::character varying(2),
	_etl				boolean default null::boolean,
	_target_variable	integer default null::integer
)
returns table
(
	id								integer,
	label							varchar,
	description						text,
	label_en						varchar,
	description_en					text,
	id_etl_target_variable			integer,
	check_target_variable			boolean,
	refyearset2panel_mapping		integer[],
	check_atomic_area_domains		boolean,
	check_atomic_sub_populations	boolean,
	metadata						json
)
as
$$
declare
		_check_atomic_ad			integer;
		_check_atomic_sp			integer;
		_check_atomic				integer;
		_cond_1						text;
		_cond_2						text;
		_target_variable4etl		integer[];
		_set_column_label			text;
		_set_column_label_en		text;
		_set_column_description		text;
		_set_column_description_en	text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable: Input argument _export_connection must not be null!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_get_target_variable: Input argument _national_language must not be null!';
		end if;

		-------------------------------------------------------------
		with
		w1 as	(
				select unnest(tead.area_domain) as area_domain from target_data.t_etl_area_domain as tead
				where tead.export_connection = _export_connection
				)
		,w2 as	(
				select tead2.area_domain[1] from target_data.t_etl_area_domain as tead2
				where tead2.export_connection = _export_connection
				and array_length(tead2.area_domain,1) = 1
				)
		,w3 as	(
				select w1.area_domain from w1 except
				select w2.area_domain from w2
				)
		select count(w3.*) from w3
		into _check_atomic_ad;
		-------------------------------------------------------------
		with
		w1 as	(
				select unnest(tesp.sub_population) as sub_population from target_data.t_etl_sub_population as tesp
				where tesp.export_connection = _export_connection
				)
		,w2 as	(
				select tesp2.sub_population[1] from target_data.t_etl_sub_population as tesp2
				where tesp2.export_connection = _export_connection
				and array_length(tesp2.sub_population,1) = 1
				)
		,w3 as	(
				select w1.sub_population from w1 except
				select w2.sub_population from w2
				)
		select count(w3.*) from w3
		into _check_atomic_sp;	
		-------------------------------------------------------------
		_check_atomic := _check_atomic_ad + _check_atomic_sp;
		-------------------------------------------------------------
		if _etl is null
		then
			_cond_1 := 'TRUE';
		else
			if _etl = true
			then
				if _check_atomic = 0
				then
					_cond_1 := 'w2a.check_target_variable = TRUE';
				else
					_cond_1 := 'w16.check_target_variable = TRUE';
				end if;
			else
				if _check_atomic = 0
				then
					_cond_1 := 'w2a.check_target_variable = FALSE';
				else
					_cond_1 := 'w16.check_target_variable = FALSE';
				end if;
			end if;
		end if;
		-------------------------------------------------------------
		if _national_language = 'en'
		then
			_set_column_label := 'w1a.label_en';
			_set_column_label_en := 'w1a.label_en';
			_set_column_description := 'w1a.description_en';
			_set_column_description_en := 'w1a.description_en';			
		else
			_set_column_label := 'w1a.label';
			_set_column_label_en := 'w1a.label_en';
			_set_column_description := 'w1a.description';
			_set_column_description_en := 'w1a.description_en';
		end if;
		-------------------------------------------------------------
		if _target_variable is null
		then
			_cond_2 := 'TRUE';
		else
			_cond_2 := 'ctv.id = $3';
		end if;
		-------------------------------------------------------------
		with
		w1 as	(
				select
						cltv.id as id_ldsity2target_variable,
						cltv.target_variable
				from
						target_data.cm_ldsity2target_variable cltv
				where
						target_variable in (select ctv.id from target_data.c_target_variable as ctv)		
				)
		,w2 as	(
				select
						w1.*,
						cltcs.*,
						tad.id as id_available_datasets
				from
						w1
						inner join target_data.cm_ldsity2target2categorization_setup as cltcs
						on w1.id_ldsity2target_variable = cltcs.ldsity2target_variable
						
						inner join target_data.t_available_datasets as tad
						on cltcs.categorization_setup = tad.categorization_setup
				)
		,w3 as	(
				select distinct w2.target_variable from w2
				where w2.id_available_datasets in
					(select distinct tlv.available_datasets from target_data.t_ldsity_values as tlv
					where tlv.available_datasets in (select distinct w2.id_available_datasets from w2)
					and tlv.is_latest = true)
				)
		select array_agg(w3.target_variable order by w3.target_variable) from w3
		into _target_variable4etl;
		-------------------------------------------------------------
		if _check_atomic = 0
		then
			return query execute
			'
			with
			w1a as	(
					select 
							ctv.*,
							(target_data.fn_etl_check_target_variable($1,ctv.id)) as res
					from 
							target_data.c_target_variable as ctv
					where
							ctv.id in (select unnest($4))
					and
							'|| _cond_2 ||'
					order
							by ctv.id
					)
			,w2a as	(
					select
							w1a.id,
							'|| _set_column_label ||' as label,
							'|| _set_column_description ||' as description,
							'|| _set_column_label_en ||' as label_en,
							'|| _set_column_description_en ||' as description_en,
							(w1a.res).id_etl_target_variable,
							(w1a.res).check_target_variable,
							(w1a.res).refyearset2panel_mapping							
					from
							w1a
					)
			,w3a as	(
					select
							w2a.*,
							target_data.fn_etl_get_target_variable_metadata(w2a.id,$2) as metadata
					from
							w2a where '|| _cond_1 ||'
					)	
			select
					w3a.id,
					w3a.label,
					w3a.description,
					w3a.label_en,
					w3a.description_en,
					w3a.id_etl_target_variable,
					w3a.check_target_variable,
					w3a.refyearset2panel_mapping,
					null::boolean as check_atomic_area_domains,
					null::boolean as check_atomic_sub_populations,
					w3a.metadata
			from
					w3a order by w3a.id;			
			'
			using _export_connection, _national_language, _target_variable, _target_variable4etl;
		else
			return query execute
			'
			with
			w1a as	(
					select 
							ctv.*,
							(target_data.fn_etl_check_target_variable($1,ctv.id)) as res
					from 
							target_data.c_target_variable as ctv
					where
							ctv.id in (select unnest($4))
					and
							'|| _cond_2 ||'
					order
							by ctv.id
					)
			,w2a as	(
					select
							w1a.id,
							'|| _set_column_label ||' as label,
							'|| _set_column_description ||' as description,
							'|| _set_column_label_en ||' as label_en,
							'|| _set_column_description_en ||' as description_en,
							(w1a.res).id_etl_target_variable,
							(w1a.res).check_target_variable,
							(w1a.res).refyearset2panel_mapping
					from
							w1a
					)
			,w3a as	(
					select
							w2a.id,
							w2a.label,
							w2a.description,
							w2a.label_en,
							w2a.description_en,
							-----------------------------------
							w2a.id_etl_target_variable,
							w2a.check_target_variable,
							w2a.refyearset2panel_mapping,
							-----------------------------------
							case
								when w2a.id_etl_target_variable is null
								then
									false
								else
									case
										when w2a.check_target_variable = true
										then true
										else false
									end
							end as do_checks4atrribute
							-----------------------------------
					from
							w2a
					)
			-------------------
			-------------------
			,w1 as	(
					select
							t2.etl_target_variable,
							array_agg(t2.refyearset2panel_mapping order by t2.refyearset2panel_mapping) as refyearset2panel_mapping4checks4attribute
					from
							(
							select
									tel.*
							from
									(select * from target_data.t_etl_log where etl_target_variable in (select w3a.id_etl_target_variable from w3a where w3a.do_checks4atrribute = true)) as tel
							inner
							join	(
									select
											etl_target_variable,
											refyearset2panel_mapping,
											max(etl_time) as etl_time_max
									from
											target_data.t_etl_log
									where
											etl_target_variable in (select w3a.id_etl_target_variable from w3a where w3a.do_checks4atrribute = true)
									group
											by etl_target_variable, refyearset2panel_mapping
									) as t1
							on
									tel.etl_target_variable = t1.etl_target_variable
							and		tel.refyearset2panel_mapping = t1.refyearset2panel_mapping
							and		tel.etl_time = t1.etl_time_max
							)
							as t2
					group
							by t2.etl_target_variable
					)
			,w2 as	(
					select
							w1.*,
							tetv.target_variable
					from
							w1
							inner join target_data.t_etl_target_variable as tetv on w1.etl_target_variable = tetv.id
					)
			,w3 as	(
					select
							target_variable,
							unnest(refyearset2panel_mapping4checks4attribute) as refyearset2panel_mapping4checks4attribute
					from
							w2
					)		
			,w4 as	(
					select
							w3.*,
							crpm.panel,
							crpm.reference_year_set
					from
							w3
							inner join sdesign.cm_refyearset2panel_mapping as crpm on w3.refyearset2panel_mapping4checks4attribute = crpm.id
					)
					-----------------
			,w5 as	(
					select
							t1.target_variable,
							cltcs.ldsity2target_variable,
							cltcs.adc2classification_rule,
							cltcs.spc2classification_rule,
							cltcs.categorization_setup,
							case
								when cltcs.adc2classification_rule is null then null::integer[]
								else (select id_type from target_data.fn_get_category_type4classification_rule_id(''adc'', cltcs.adc2classification_rule))
							end as adc_type,
							case
								when cltcs.spc2classification_rule is null then null::integer[]
								else (select id_type from target_data.fn_get_category_type4classification_rule_id(''spc'', cltcs.spc2classification_rule))
							end as spc_type
					from
							(
							select id, target_variable from target_data.cm_ldsity2target_variable
							where target_variable in (select distinct target_variable from w2)
							) as t1
					inner join target_data.cm_ldsity2target2categorization_setup as cltcs
					on t1.id = cltcs.ldsity2target_variable
					)
			,w6 as	(
					select
							w4.target_variable,
							w4.refyearset2panel_mapping4checks4attribute,
							w4.panel,
							w4.reference_year_set,
							w5.ldsity2target_variable,
							w5.adc2classification_rule,
							w5.spc2classification_rule,
							w5.adc_type,
							w5.spc_type,
							tad.id as available_datasets_id
					from
							w4
							inner join w5 on w4.target_variable = w5.target_variable
							inner join target_data.t_available_datasets as tad
							on w4.panel = tad.panel
							and w4.reference_year_set = tad.reference_year_set 
							and w5.categorization_setup = tad.categorization_setup
					)
			,w7 as	(
					select w6.* from w6 where w6.available_datasets_id in
						(
						select distinct tlv.available_datasets from target_data.t_ldsity_values as tlv
						where tlv.available_datasets in (select distinct w6.available_datasets_id from w6)
						and is_latest = true
						)
					)
			,w8 as	(
					select
							t4.target_variable,
							count(t4.adc_type) as adc_type_pocet
					from
							(
							select t3.* from
								(
								select distinct t2.target_variable, t2.adc_type from
								(select t1.target_variable, unnest(t1.adc_type) as adc_type
								from (select distinct target_variable, adc_type from w7 where adc_type is not null) as t1) as t2
								) as t3
							where t3.adc_type not in	(
														select area_domain[1] as area_domain
														from target_data.t_etl_area_domain as tead
														where tead.export_connection = $1
														and array_length(area_domain,1) = 1
														)
							) as t4
					group
							by t4.target_variable
					)
			,w9 as	(
					select
							t4.target_variable,
							count(t4.spc_type) as spc_type_pocet
					from
							(
							select t3.* from
								(
								select distinct t2.target_variable, t2.spc_type from
								(select t1.target_variable, unnest(t1.spc_type) as spc_type
								from (select distinct target_variable, spc_type from w7 where spc_type is not null) as t1) as t2
								) as t3
							where t3.spc_type not in	(
														select sub_population[1] as sub_population
														from target_data.t_etl_sub_population as tesp
														where tesp.export_connection = $1
														and array_length(sub_population,1) = 1
														)
							) as t4
					group
							by t4.target_variable
					)
			,w10 as	(
					select w8.*, tetv.id as id_etl_target_variable from w8
					inner join (select * from target_data.t_etl_target_variable where export_connection = 6) as tetv
					on w8.target_variable = tetv.target_variable
					)
			,w11 as	(
					select w9.*, tetv.id as id_etl_target_variable from w9
					inner join (select * from target_data.t_etl_target_variable where export_connection = 6) as tetv
					on w9.target_variable = tetv.target_variable
					)
			,w12 as	(
					select
							w1.*,
							case when w10.adc_type_pocet is null then 0 else w10.adc_type_pocet end as res_of_check_ad,
							case when w11.spc_type_pocet is null then 0 else w11.spc_type_pocet end as res_of_check_sp
					from
							w1
							left join w10 on w1.etl_target_variable = w10.id_etl_target_variable
							left join w11 on w1.etl_target_variable = w11.id_etl_target_variable
					)
			,w14 as	(
					select
							w3a.*,
							w12.*
					from
							w3a
							left join w12 on w3a.id_etl_target_variable = w12.etl_target_variable
					)
			,w15 as	(
					select
							w14.id,
							w14.label,
							w14.description,
							w14.label_en,
							w14.description_en,
							w14.id_etl_target_variable,
							w14.check_target_variable,
							---------------------------------------------
							case
								when w14.id_etl_target_variable is not null and w14.check_target_variable = true
								then
									case
										when w14.res_of_check_ad = 0 and w14.res_of_check_sp = 0
										then true
										else false
									end							
								else
									w14.check_target_variable
							end
								as check_target_variable4output,
							---------------------------------------------
							case
								when w14.id_etl_target_variable is not null and w14.check_target_variable = true
								then
									w14.refyearset2panel_mapping4checks4attribute	
								else
									w14.refyearset2panel_mapping
							end
								as refyearset2panel_mapping,						
							---------------------------------------------
							case
								when w14.id_etl_target_variable is not null and w14.check_target_variable = true
								then
									case
										when w14.res_of_check_ad = 0
										then true
										else false
									end							
								else
									null::boolean
							end
								as check_atomic_area_domains,
							---------------------------------------------
							case
								when w14.id_etl_target_variable is not null and w14.check_target_variable = true
								then
									case
										when w14.res_of_check_sp = 0
										then true
										else false
									end							
								else
									null::boolean
							end
								as check_atomic_sub_populations
							---------------------------------------------					
					from
							w14
					)
			,w16 as	(
					select
							w15.id,
							w15.label,
							w15.description,
							w15.label_en,
							w15.description_en,
							w15.id_etl_target_variable,
							w15.check_target_variable4output as check_target_variable,
							w15.refyearset2panel_mapping,
							w15.check_atomic_area_domains,
							w15.check_atomic_sub_populations
					from
							w15
					)
			,w17 as	(
					select
							w16.*,
							target_data.fn_etl_get_target_variable_metadata(w16.id,$2) as metadata
					from
							w16 where '|| _cond_1 ||'
					)
			select
					w17.id,
					w17.label,
					w17.description,
					w17.label_en,
					w17.description_en,
					w17.id_etl_target_variable,
					w17.check_target_variable,
					w17.refyearset2panel_mapping,
					w17.check_atomic_area_domains,
					w17.check_atomic_sub_populations,
					w17.metadata
			from
					w17 order by w17.id;
			'
			using _export_connection, _national_language, _target_variable, _target_variable4etl;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) IS
'The function returs target variables and informations for ETL. If input argument _etl = TRUE then function
returns target variables that was ETL and all theirs datas are current and all their atomic types are ETL-forged.
If input argument _etl = FALSE then function returns target variables that was not ETL yet or returns target variables
that was ETL and exists newer datas for ETL or returns target variables that was ETL and all theirs datas are current
but all their atomic types were not ETL-forged. If input argument _etl IS NULL (_etl = true or false) then function
returns all cases.';

grant execute on function target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) to public;
-- </function>



-- <function name="fn_etl_check_area_domain" schema="target_data" src="functions/etl/fn_etl_check_area_domain.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_area_domain
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_area_domain(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_check_area_domain
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns table
(
	id						integer,
	export_connection		integer,
	area_domain				integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_ad				integer,
	atomic					boolean
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/					

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
			
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.adc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.adc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			return query
			with w as	(
						select
								0 as id,
								0 as export_connection,
								array[0] as area_domain,
								''::varchar as label,
								''::text as description,
								''::varchar as label_en,
								''::text as description_en,
								0 as etl_id,
								0 as id_t_etl_ad,
								null::boolean as atomic
						)
			select w.id, w.export_connection, w.area_domain, w.label, w.description, w.label_en, w.description_en, w.etl_id, w.id_t_etl_ad, w.atomic
			from w
			where w.id is distinct from 0;		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.adc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.adc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)		
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select array[cad.id] as id_type, cad.label, cad.description, cad.label_en, cad.description_en
					from target_data.c_area_domain as cad where cad.id in (select w4.id_type[1] from w4)
					)
			,w6 as	(
					select w2.id_type, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select
							_export_connection as export_connection,
							w6.id_type as area_domain,
							w6.label,
							w6.description,
							w6.label_en,
							w6.description_en
					from
							w6
					)
			,w8 as	(
					select
							w7.export_connection,
							w7.area_domain,
							w7.label,
							w7.description,
							w7.label_en,
							w7.description_en,
							t.etl_id,
							t.id as id_t_etl_ad
					from
							w7 left join target_data.t_etl_area_domain as t
					on
							w7.export_connection = t.export_connection
					and	
							target_data.fn_etl_array_compare(w7.area_domain,t.area_domain)
					order
							by w7.area_domain
					)
			select
					(row_number() over ())::integer as id_order,
					w8.export_connection,
					w8.area_domain,
					w8.label,
					w8.description,
					w8.label_en,
					w8.description_en,
					w8.etl_id,
					w8.id_t_etl_ad,
					case when array_length(w8.area_domain,1) = 1 then true else false end as atomic
			from
					w8;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_area_domain(integer[], integer) is
'Function returns records for ETL c_area_domain table.';

grant execute on function target_data.fn_etl_check_area_domain(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_check_sub_population" schema="target_data" src="functions/etl/fn_etl_check_sub_population.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_sub_population
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_sub_population(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_check_sub_population
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns table
(
	id						integer,
	export_connection		integer,
	sub_population			integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_sp				integer,
	atomic					boolean
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;		

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------
	
		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.spc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.spc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			return query
			with w as	(
						select
								0 as id,
								0 as export_connection,
								array[0] as sub_population,
								''::varchar as label,
								''::text as description,
								''::varchar as label_en,
								''::text as description_en,
								0 as etl_id,
								0 as id_t_etl_sp,
								null::boolean as atomic
						)
			select w.id, w.export_connection, w.sub_population, w.label, w.description, w.label_en, w.description_en, w.etl_id, w.id_t_etl_sp, w.atomic
			from w
			where w.id is distinct from 0;		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.spc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.spc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)		
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select array[csp.id] as id_type, csp.label, csp.description, csp.label_en, csp.description_en
					from target_data.c_sub_population as csp where csp.id in (select w4.id_type[1] from w4)
					)
			,w6 as	(
					select w2.id_type, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select
							_export_connection as export_connection,
							w6.id_type as sub_population,
							w6.label,
							w6.description,
							w6.label_en,
							w6.description_en
					from
							w6
					)
			,w8 as	(
					select
							w7.export_connection,
							w7.sub_population,
							w7.label,
							w7.description,
							w7.label_en,
							w7.description_en,
							t.etl_id,
							t.id as id_t_etl_sp
					from
							w7 left join target_data.t_etl_sub_population as t
					on
							w7.export_connection = t.export_connection
					and	
							target_data.fn_etl_array_compare(w7.sub_population,t.sub_population)
					order
							by w7.sub_population
					)
			select
					(row_number() over ())::integer as id_order,
					w8.export_connection,
					w8.sub_population,
					w8.label,
					w8.description,
					w8.label_en,
					w8.description_en,
					w8.etl_id,
					w8.id_t_etl_sp,
					case when array_length(w8.sub_population,1) = 1 then true else false end as atomic
			from
					w8;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_sub_population(integer[], integer) is
'Function returns records for ETL c_sub_population table.';

grant execute on function target_data.fn_etl_check_sub_population(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_check_area_domain_category" schema="target_data" src="functions/etl/fn_etl_check_area_domain_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_area_domain_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_area_domain_category(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_check_area_domain_category
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns table
(
	id						integer,
	export_connection		integer,
	etl_id_area_domain		integer,
	label_area_domain		varchar,
	label_en_area_domain	varchar,
	area_domain_category	integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_adc			integer,
	id_t_etl_ad				integer
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_area_domain_category: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_area_domain_category: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable 
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/					

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------

		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.adc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type,
						(target_data.fn_get_category4classification_rule_id('adc',t.adc2classification_rule)).id_category
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))								
													)
				and
						t.adc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type, w.id_category from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			raise exception 'Error 03: fn_etl_check_area_domain_category: If exists area_domain than must exists any area_domain_category!'; 		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.adc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label as label_type,
							(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_en_type,
							(target_data.fn_get_category4classification_rule_id('adc',t.adc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.adc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label_type, w1.label_en_type, w1.id_category, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select
							a.id_type,
							a.label_type,
							a.label_en_type,
							array[b.id] as id_category,
							b.label,
							b.description,
							b.label_en,
							b.description_en
					from
							(
							select cad.id, array[cad.id] as id_type, cad.label as label_type, cad.label_en as label_en_type
							from target_data.c_area_domain as cad where cad.id in (select w4.id_type[1] from w4)
							) as a
					inner
					join	target_data.c_area_domain_category as b on a.id = b.area_domain
					)
			,w6 as	(
					select w2.id_type, w2.label_type, w2.label_en_type, w2.id_category, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label_type, w5.label_en_type, w5.id_category, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select _export_connection as export_connection, w6.* from w6
					)
			,w8 as	(
					select w7.*, tead.etl_id as etl_area_domain, tead.id as id_t_etl_ad
					from w7 inner join target_data.t_etl_area_domain as tead
					on w7.export_connection = tead.export_connection
					and target_data.fn_etl_array_compare(w7.id_type,tead.area_domain)
					)
			,w9 as	(
					select w8.*, a.etl_id, a.id from w8
					left join
								(
								select * from target_data.t_etl_area_domain_category
								where etl_area_domain in (select distinct w8.id_t_etl_ad from w8)
								) as a
					on target_data.fn_etl_array_compare(w8.id_category,a.area_domain_category)
					order
							by w8.id_category
					)
			select
					(row_number() over ())::integer as id_order,
					w9.export_connection,
					w9.etl_area_domain,
					w9.label_type as label_area_domain,
					w9.label_en_type as label_en_area_domian,
					w9.id_category as area_domain_category,
					w9.label,
					w9.description,
					w9.label_en,
					w9.description_en,
					w9.etl_id,
					w9.id as id_t_etl_adc,
					w9.id_t_etl_ad
			from
					w9;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_area_domain_category(integer[], integer) is
'Function returns records for ETL c_area_domain_category table.';

grant execute on function target_data.fn_etl_check_area_domain_category(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_check_sub_population_category" schema="target_data" src="functions/etl/fn_etl_check_sub_population_category.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_sub_population_category
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_sub_population_category(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_check_sub_population_category
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns table
(
	id						integer,
	export_connection		integer,
	etl_id_sub_population	integer,
	label_sub_population	varchar,
	label_en_sub_population	varchar,
	sub_population_category	integer[],
	label					varchar,
	description				text,
	label_en				varchar,
	description_en			text,
	etl_id					integer,
	id_t_etl_spc			integer,
	id_t_etl_sp				integer
)
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_categorization_setups		integer[];
		_check_count				integer;
begin		
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_check_sub_population_category: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_sub_population_category: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable;
	
		/*
		with
		w as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(
					select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		select array_agg(w.categorization_setup order by w.categorization_setup) from w
		into _categorization_setups;
		*/

		-------------------------------------------------------------------------------------------
		-- variant where categorization setups are finding from table t_ldsity_values and
		-- list of categorization setups is reduced by combination of panels and reference year sets
		-------------------------------------------------------------------------------------------
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);
		-------------------------------------------------------------------------------------------
		-------------------------------------------------------------------------------------------

		with
		w as	(
				select
						t.id,
						t.ldsity2target_variable,
						t.spc2classification_rule,
						t.categorization_setup,
						(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type,
						(target_data.fn_get_category4classification_rule_id('spc',t.spc2classification_rule)).id_category
				from
						target_data.cm_ldsity2target2categorization_setup as t
				where
						t.ldsity2target_variable in	(
													select distinct ldsity2target_variable
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest(_categorization_setups))							
													)
				and
						t.spc2classification_rule is not null
				)
		select count(t.*) from (select distinct w.id_type, w.id_category from w) as t
		into _check_count;
			
		if _check_count = 0
		then
			raise exception 'Error 04: fn_etl_check_sub_population_category: If exists sub_population than must exists any sub_population_category!'; 		
		else
			return query
			with
			w1 as	(
					select
							t.id,
							t.ldsity2target_variable,
							t.spc2classification_rule,
							t.categorization_setup,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label as label_type,
							(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_en_type,
							(target_data.fn_get_category4classification_rule_id('spc',t.spc2classification_rule)).*
					from
							target_data.cm_ldsity2target2categorization_setup as t
					where
							ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))								
														)
					and
							t.spc2classification_rule is not null
					)
			,w2 as	(
					select distinct w1.id_type, w1.label_type, w1.label_en_type, w1.id_category, w1.label, w1.description, w1.label_en, w1.description_en from w1
					)
			,w3 as	(
					select distinct t.id_type from (select unnest(w2.id_type) as id_type from w2) as t
					)
			,w4 as	(
					select array[w3.id_type] as id_type from w3 except
					select w2.id_type from w2
					)
			,w5 as	(
					select
							a.id_type,
							a.label_type,
							a.label_en_type,
							array[b.id] as id_category,
							b.label,
							b.description,
							b.label_en,
							b.description_en
					from
							(
							select csp.id, array[csp.id] as id_type, csp.label as label_type, csp.label_en as label_en_type
							from target_data.c_sub_population as csp where csp.id in (select w4.id_type[1] from w4)
							) as a
					inner
					join	target_data.c_sub_population_category as b on a.id = b.sub_population
					)
			,w6 as	(
					select w2.id_type, w2.label_type, w2.label_en_type, w2.id_category, w2.label, w2.description, w2.label_en, w2.description_en from w2 union all
					select w5.id_type, w5.label_type, w5.label_en_type, w5.id_category, w5.label, w5.description, w5.label_en, w5.description_en from w5
					)
			,w7 as	(
					select _export_connection as export_connection, w6.* from w6
					)
			,w8 as	(
					select w7.*, tesp.etl_id as etl_sub_population, tesp.id as id_t_etl_sp
					from w7 inner join target_data.t_etl_sub_population as tesp
					on w7.export_connection = tesp.export_connection
					and target_data.fn_etl_array_compare(w7.id_type,tesp.sub_population)
					)
			,w9 as	(
					select w8.*, a.etl_id, a.id from w8
					left join
								(
								select * from target_data.t_etl_sub_population_category
								where etl_sub_population in (select distinct w8.id_t_etl_sp from w8)
								) as a
					on target_data.fn_etl_array_compare(w8.id_category,a.sub_population_category)
					order
							by w8.id_category
					)
			select
					(row_number() over ())::integer as id_order,
					w9.export_connection,
					w9.etl_sub_population,
					w9.label_type as label_sub_population,
					w9.label_en_type as label_en_sub_population,
					w9.id_category as sub_population_category,
					w9.label,
					w9.description,
					w9.label_en,
					w9.description_en,
					w9.etl_id,
					w9.id as id_t_etl_spc,
					w9.id_t_etl_sp
			from
					w9;
		end if;			
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_sub_population_category(integer[], integer) is
'Function returns records for ETL c_sub_population_category table.';

grant execute on function target_data.fn_etl_check_sub_population_category(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_export_area_domain_category_mapping" schema="target_data" src="functions/etl/fn_etl_export_area_domain_category_mapping.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_area_domain_category_mapping
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_area_domain_category_mapping(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_area_domain_category_mapping
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_check_ad		integer;
		_check_adc		integer;
		_res			json;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_area_domain_category_mapping: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_area_domain_category_mapping: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;

		select count(*) from target_data.fn_etl_check_area_domain(_refyearset2panel_mapping,_id_t_etl_target_variable)
		into _check_ad;

		if _check_ad = 0
		then
				with
				w1 as	(
						select
								0 as etl_id_adc_non_atomic,
								0 as etl_id_adc_atomic
						)
				select json_agg(json_build_object(
						'area_domain_category_non_atomic',w1.etl_id_adc_non_atomic,
						'area_domain_category_atomic',w1.etl_id_adc_atomic))
				from
						w1 where w1.etl_id_adc_non_atomic > 0
				into
						_res;
		else
			select count(*) from target_data.fn_etl_check_area_domain_category(_refyearset2panel_mapping,_id_t_etl_target_variable)
			where etl_id is null
			into _check_adc;

			if _check_adc = 0
			then
				with
				w1 as	(
						select
								id,
								etl_id_area_domain,
								area_domain_category,
								etl_id,
								id_t_etl_adc,
								id_t_etl_ad
						from
								target_data.fn_etl_check_area_domain_category(_refyearset2panel_mapping,_id_t_etl_target_variable)
						)
				,w2 as	(
						select area_domain_category, etl_id from w1 where array_length(area_domain_category,1) > 1
						)
				,w3 as	(
						select area_domain_category[1] as adc_atomic, etl_id from w1 where array_length(area_domain_category,1) = 1
						)
				,w4 as	(
						select
								t1.etl_id as etl_id_adc_non_atomic,
								w3.etl_id as etl_id_adc_atomic
						from
								(select etl_id, unnest(area_domain_category) as adc_non_atomic from w2) as t1
								inner join w3 on t1.adc_non_atomic = w3.adc_atomic
						)
				select json_agg(json_build_object(
						'area_domain_category_non_atomic',w4.etl_id_adc_non_atomic,
						'area_domain_category_atomic',w4.etl_id_adc_atomic))
				from
						w4
				into
						_res;					
			else
				raise exception 'Error 03: fn_etl_export_area_domain_category_mapping: For some of area domain category not exists ETL ID!';
			end if;

		end if;
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_area_domain_category_mapping(integer[], integer) is
'Function returns records for ETL cm_area_domain_category table.';

grant execute on function target_data.fn_etl_export_area_domain_category_mapping(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_export_sub_population_category_mapping" schema="target_data" src="functions/etl/fn_etl_export_sub_population_category_mapping.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_sub_population_category_mapping
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_sub_population_category_mapping(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_sub_population_category_mapping
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_check_sp		integer;
		_check_spc		integer;
		_res			json;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_sub_population_category_mapping: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;
	
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_sub_population_category_mapping: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;

		select count(*) from target_data.fn_etl_check_sub_population(_refyearset2panel_mapping,_id_t_etl_target_variable)
		into _check_sp;

		if _check_sp = 0
		then
				with
				w1 as	(
						select
								0 as etl_id_spc_non_atomic,
								0 as etl_id_spc_atomic
						)
				select json_agg(json_build_object(
						'sub_population_category_non_atomic',w1.etl_id_spc_non_atomic,
						'sub_population_category_atomic',w1.etl_id_spc_atomic))
				from
						w1 where w1.etl_id_spc_non_atomic > 0
				into
						_res;
		else
			select count(*) from target_data.fn_etl_check_sub_population_category(_refyearset2panel_mapping,_id_t_etl_target_variable)
			where etl_id is null
			into _check_spc;

			if _check_spc = 0
			then
				with
				w1 as	(
						select
								id,
								etl_id_sub_population,
								sub_population_category,
								etl_id,
								id_t_etl_spc,
								id_t_etl_sp
						from
								target_data.fn_etl_check_sub_population_category(_refyearset2panel_mapping,_id_t_etl_target_variable)
						)
				,w2 as	(
						select sub_population_category, etl_id from w1 where array_length(sub_population_category,1) > 1
						)
				,w3 as	(
						select sub_population_category[1] as spc_atomic, etl_id from w1 where array_length(sub_population_category,1) = 1
						)
				,w4 as	(
						select
								t1.etl_id as etl_id_spc_non_atomic,
								w3.etl_id as etl_id_spc_atomic
						from
								(select etl_id, unnest(sub_population_category) as spc_non_atomic from w2) as t1
								inner join w3 on t1.spc_non_atomic = w3.spc_atomic
						)
				select json_agg(json_build_object(
						'sub_population_category_non_atomic',w4.etl_id_spc_non_atomic,
						'sub_population_category_atomic',w4.etl_id_spc_atomic))
				from
						w4
				into
						_res;					
			else
				raise exception 'Error 03: fn_etl_export_sub_population_category_mapping: For some of sub population category not exists ETL ID!';
			end if;

		end if;
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_sub_population_category_mapping(integer[], integer) is
'Function returns records for ETL cm_sub_population_category table.';

grant execute on function target_data.fn_etl_export_sub_population_category_mapping(integer[], integer) to public;
-- </function>