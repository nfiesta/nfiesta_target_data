--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_check_classification_rule" schema="target_data" src="functions/fn_check_classification_rule.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_check_classification_rule
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_check_classification_rule(
	_ldsity integer,						-- id from table c_ldsity, ldsity contribution for which the rule should be tested
	_ldsity_object integer,						-- id from c_ldsity_object, for which object the classification rule are valid (must be within hierarchy of ldsity contribution)
	_rule text, 							-- text representation of the classification rule which is used to classify an object
	_panel_refyearset integer DEFAULT NULL::int,			-- panel and reference year set combination (id from table cm_refyearset2panel_mapping)
									-- in which are classification rules applicable
	_adc integer[] DEFAULT NULL::int[], 				-- optional area domain categories contraint (only for areal), e.g. accessible land
	_spc integer[] DEFAULT NULL::int[]				-- dtto for populational, e.g. living stem for age classification rule, it is an array of arrays hence
									--  more categories from one population can fit the constraint for each rule, 
									-- e.g. for rule which classifies high intensity of damage a prerequisite can be two categories:
									-- damaged by bark stripping, damaged by human etc.
)
RETURNS TABLE (
	compliance_status	boolean,
	no_of_objects		integer
) 
AS
$$
DECLARE
_ldsity_objects		integer[];
_adc_rule_test		integer[];
_spc_rule_test		integer[];
_table_name4rule	varchar;
_test			boolean;
_test_all		boolean[];
_case			varchar;
_table1			varchar;
_table_names		varchar[];
_table_selects		varchar[];
_table_names_ws		varchar[];
_primary_keys		varchar[];
_column_names		varchar[];
_join			text;
_join_all		text;
_pkey			varchar;
_q			text;
_no_of_rules_met	integer[];
_no_of_objects		integer[];
_total			integer;
_exist_test		boolean;
_qall			text;
i 			integer;
_adc4loop2d		integer[][];
_spc4loop2d		integer[][];
_adc4loop		integer[];
_spc4loop		integer[];
adcv			integer;
spcv			integer;
BEGIN
	IF NOT EXISTS (SELECT * FROM target_data.c_ldsity AS t1 WHERE t1.id = _ldsity)
	THEN RAISE EXCEPTION 'Given ldsity does not exist in table c_ldsity (%)', _ldsity;
	END IF;

	SELECT * FROM target_data.fn_check_classification_rule_syntax(_ldsity_object, _rule)
	INTO _test;

	IF _test = false
	THEN
		RAISE EXCEPTION 'Given rule has an invalid syntax.';
	END IF;

	IF _rule = 'EXISTS' OR _rule = 'NOT EXISTS'
	THEN
		_exist_test := true; _rule = true;
	ELSE 	_exist_test := false;
	END IF;
	
	_case := '('||_rule||')';

-- test all adc/spc has to come from one area domain/sub population

	IF (SELECT count(DISTINCT t2.area_domain)
		FROM unnest(_adc) AS t1(adc),
			target_data.c_area_domain_category AS t2
		WHERE t1.adc = t2.id) > 1
	THEN 
		RAISE EXCEPTION 'Given array of area domain categories does not come from one area domain.';
	END IF;
	
	IF (SELECT count(DISTINCT t2.sub_population)
		FROM unnest(_spc) AS t1(spc),
			target_data.c_sub_population_category AS t2
		WHERE t1.spc = t2.id) > 1
	THEN 
		RAISE EXCEPTION 'Given array of sub population categories does not come from one sub population.';
	END IF;


	_ldsity_objects := (SELECT target_data.fn_get_ldsity_objects(array[_ldsity_object]));

-- get id from cm_adc2classification_rule id for each adc
	WITH RECURSIVE w_cm_ids AS (
		SELECT
				t2.id AS object_id, t1.id, t1.area_domain_category, t3.array_id, t3.id AS ordinality
			FROM
				target_data.cm_adc2classification_rule AS t1
			INNER JOIN
				unnest(_ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
			ON
			   t1.ldsity_object = t2.ldsity_object
			RIGHT JOIN
				(
				SELECT  adc, id, ceil(id/(count(*) OVER())::numeric * array_length(_adc,1)) AS array_id
				FROM  unnest(_adc) WITH ORDINALITY AS t1(adc, id)
				) AS t3
			ON
				t1.area_domain_category = t3.adc
			ORDER BY t2.id DESC, t3.array_id ASC
			-- because some rule can be met on more than one object, the rules from the object which is hierarchically higher will be picked
			LIMIT array_length(_adc,1) * array_length(_adc,2)
			),
	w_hier AS (
		SELECT 	t2.id AS cat_id, t2.ordinality, t2.array_id, 
			t2.id AS cm_id, t3.id AS cm_id_sup,
			t1.variable_superior, 2 AS id_sup, t1.variable, 1 AS id,
 			CASE WHEN t3.id IS NOT NULL THEN array[t1.variable_superior,t1.variable] ELSE array[t1.variable] END AS array_variable,
			CASE WHEN t3.id IS NOT NULL THEN array[t3.id, t2.id] ELSE array[t2.id] END AS cm_id_array_variable
		FROM target_data.t_adc_hierarchy AS t1
		RIGHT JOIN
			w_cm_ids AS t2
		ON t1.variable = t2.area_domain_category
		LEFT JOIN
			target_data.cm_adc2classification_rule AS t3
		ON t1.variable_superior = t3.area_domain_category
		LEFT JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		UNION ALL
		SELECT 	t2.cat_id, t2.ordinality, t2.array_id, 
			t2.cm_id, t2.cm_id_sup,
			t1.variable_superior, id_sup + 1, t1.variable, t2.id + 1, 
			array_prepend(t1.variable_superior, array_variable),
			array_prepend(t3.id, cm_id_array_variable)
		FROM target_data.t_adc_hierarchy AS t1
		INNER JOIN w_hier AS t2
		ON t1.variable = t2.variable_superior
		INNER JOIN target_data.cm_adc2classification_rule AS t3
		ON t1.variable_superior = t3.area_domain_category
		INNER JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
	), w_nulls AS (
		SELECT cat_id, ordinality, array_id, t1.array_variable[array_length(t1.array_variable,1)] AS cat, 
			array_variable, cm_id_array_variable,
			max(array_length(array_variable,1)) OVER () - array_length(array_variable,1) AS nulls2app,
			max(array_length(array_variable,1)) OVER () AS max_len
		FROM w_hier AS t1 
		--LEFT JOIN target_data.t_adc_hierarchy AS t2 
		--ON t1.variable_superior = t2.variable 
		--WHERE t2.variable_superior IS NULL
	), w_append AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS var,
			cm_id_array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS cm_var,
			array_fill(NULL::int, ARRAY[max_len]) AS null_array, count(*) OVER(PARTITION BY cat) AS total
		FROM w_nulls
	), w_cat AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_agg(var) AS var_agg, 
			array_agg(cm_var) AS cm_var_agg, 
			null_array
		FROM w_append
		GROUP BY cat_id, cat, ordinality, array_id, null_array
	), w_cat_agg AS (
		SELECT cat_id, ordinality, array_id, cat, 
			var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS nulls2app,
			cm_var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS cm_nulls2app
		FROM w_cat
	), w_agg3d AS (
		SELECT
			array_id, 
			array_agg(nulls2app ORDER BY ordinality) AS cats,
			array_agg(cm_nulls2app ORDER BY ordinality) AS cm_ids
		FROM w_cat_agg
		GROUP BY array_id
	)
	SELECT 	--array_agg(cats ORDER BY array_id),
		array_agg(cm_ids ORDER BY array_id)
	FROM w_agg3d
	INTO _adc;

--raise notice '%', _ldsity_objects;
	WITH RECURSIVE w_cm_ids AS (
		SELECT
				t2.id AS object_id, t1.id, t1.sub_population_category, t3.array_id, t3.id AS ordinality
			FROM
				target_data.cm_spc2classification_rule AS t1
			INNER JOIN
				unnest(_ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
			ON
			   t1.ldsity_object = t2.ldsity_object
			RIGHT JOIN
				(
				--SELECT  spc, id, ceil(id/(count(*) OVER())::double precision * array_length(array[array[401,402],array[NULL::int,NULL::int]],1)) AS array_id
				--FROM  unnest(array[array[401,402],array[NULL::int,NULL::int]]) WITH ORDINALITY AS t1(spc, id)
				SELECT  spc, id, ceil(id/(count(*) OVER())::double precision * array_length(_spc,1)) AS array_id
				FROM  unnest(_spc) WITH ORDINALITY AS t1(spc, id)
				) AS t3
			ON
				t1.sub_population_category = t3.spc
			ORDER BY t2.id DESC, t3.array_id ASC
			-- because some rule can be met on more than one object, the rules from the object which is hierarchically higher will be picked
			--LIMIT array_length(array[array[401,402],array[NULL::int,NULL::int]],1) * array_length(array[array[401,402],array[NULL::int,NULL::int]],2)
			LIMIT array_length(_spc,1) * array_length(_spc,2)
			),
	w_hier AS (
		SELECT 	t2.id AS cat_id, t2.ordinality, t2.array_id, 
			t2.id AS cm_id, t3.id AS cm_id_sup,
			t1.variable_superior, 2 AS id_sup, t1.variable, 1 AS id,
  			CASE WHEN t3.id IS NOT NULL THEN array[t1.variable_superior,t1.variable] ELSE array[t1.variable] END AS array_variable,
			CASE WHEN t3.id IS NOT NULL THEN array[t3.id, t2.id] ELSE array[t2.id] END AS cm_id_array_variable
		FROM target_data.t_spc_hierarchy AS t1
		RIGHT JOIN
			w_cm_ids AS t2
		ON t1.variable = t2.sub_population_category
		LEFT JOIN
			target_data.cm_spc2classification_rule AS t3
		ON 	t1.variable_superior = t3.sub_population_category AND
			t3.classification_rule NOT IN ('EXISTS','NOT EXISTS')
		LEFT JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		UNION ALL
		SELECT 	t2.cat_id, t2.ordinality, t2.array_id, 
			t2.cm_id, t2.cm_id_sup,
			t1.variable_superior, id_sup + 1, t1.variable, t2.id + 1, 
			array_prepend(t1.variable_superior, array_variable),
			array_prepend(t3.id, cm_id_array_variable)
		FROM target_data.t_spc_hierarchy AS t1
		INNER JOIN w_hier AS t2
		ON t1.variable = t2.variable_superior
		INNER JOIN target_data.cm_spc2classification_rule AS t3
		ON t1.variable_superior = t3.sub_population_category
		INNER JOIN unnest(_ldsity_objects) WITH ORDINALITY AS t4(ldsity_object, id)
		ON t3.ldsity_object = t4.ldsity_object
		WHERE t3.classification_rule NOT IN ('EXISTS','NOT EXISTS')
	), w_nulls AS (
		SELECT cat_id, ordinality, array_id, t1.array_variable[array_length(t1.array_variable,1)] AS cat, 
			array_variable, cm_id_array_variable,
			max(array_length(array_variable,1)) OVER () - array_length(array_variable,1) AS nulls2app,
			max(array_length(array_variable,1)) OVER () AS max_len
		FROM w_hier AS t1 
		--LEFT JOIN target_data.t_spc_hierarchy AS t2 
		--ON t1.variable_superior = t2.variable 
		--WHERE t2.variable_superior IS NULL
	), w_append AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS var,
			cm_id_array_variable || array_fill(NULL::int, ARRAY[nulls2app]) AS cm_var,
			array_fill(NULL::int, ARRAY[max_len]) AS null_array, count(*) OVER(PARTITION BY cat) AS total
		FROM w_nulls
	), w_cat AS (
		SELECT cat_id, ordinality, array_id, cat, 
			array_agg(var) AS var_agg, 
			array_agg(cm_var) AS cm_var_agg, 
			null_array
		FROM w_append
		GROUP BY cat_id, cat, ordinality, array_id, null_array
	), w_cat_agg AS (
		SELECT cat_id, ordinality, array_id, cat, 
			var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS nulls2app,
			cm_var_agg || concat('{',replace(repeat(null_array::text, max(array_length(var_agg,1)) OVER () - array_length(var_agg,1)),'}{','},{'),'}')::integer[][] AS cm_nulls2app
		FROM w_cat
	), w_agg3d AS (
		SELECT
			array_id, 
			array_agg(nulls2app ORDER BY ordinality) AS cats,
			array_agg(cm_nulls2app ORDER BY ordinality) AS cm_ids
		FROM w_cat_agg
		GROUP BY array_id
	)
	SELECT 	--array_agg(cats ORDER BY array_id),
		array_agg(cm_ids ORDER BY array_id)
	FROM w_agg3d
	INTO _spc;
-- construct from part of the query
	WITH w AS (
		SELECT	target_data.fn_get_ldsity_objects(array[_ldsity_object]) AS ldsity_objects
	), w_all AS (
		SELECT
			t3.id, t2.ldsity_object, t3.table_name, t3.column4upper_object AS column_name, 
			t3.filter, t5.column_name AS primary_key
		FROM
			w AS t1,
			unnest(t1.ldsity_objects) WITH ORDINALITY AS t2(ldsity_object, id)
		INNER JOIN
			target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		INNER JOIN
			information_schema.table_constraints AS t4
		ON t4.table_schema = split_part(t3.table_name,'.',1) AND t4.table_name = split_part(t3.table_name,'.',2) AND
			t4.constraint_type = 'PRIMARY KEY'
		INNER JOIN
			(SELECT current_database()::information_schema.sql_identifier AS table_catalog,
				x.tblschema::information_schema.sql_identifier AS table_schema,
				x.tblname::information_schema.sql_identifier AS table_name,
				x.colname::information_schema.sql_identifier AS column_name,
				current_database()::information_schema.sql_identifier AS constraint_catalog,
				x.cstrschema::information_schema.sql_identifier AS constraint_schema,
				x.cstrname::information_schema.sql_identifier AS constraint_name
			FROM 
				(SELECT DISTINCT nr.nspname,
					r.relname,
					r.relowner,
					a.attname,
					nc.nspname,
					c.conname
				FROM pg_namespace nr,
					pg_class r,
					pg_attribute a,
					pg_depend d,
					pg_namespace nc,
					pg_constraint c
			 	WHERE 
					nr.oid = r.relnamespace AND r.oid = a.attrelid AND 
					d.refclassid = 'pg_class'::regclass::oid AND d.refobjid = r.oid AND 
					d.refobjsubid = a.attnum AND d.classid = 'pg_constraint'::regclass::oid AND 
					d.objid = c.oid AND c.connamespace = nc.oid AND c.contype = 'c'::"char" AND 
					(r.relkind = ANY (ARRAY['r'::"char", 'p'::"char"])) AND NOT a.attisdropped
				UNION ALL
				SELECT nr.nspname,
					r.relname,
					r.relowner,
					a.attname,
					nc.nspname,
					c.conname
			   	FROM pg_namespace nr,
					pg_class r,
					pg_attribute a,
					pg_namespace nc,
					pg_constraint c
				WHERE 
					nr.oid = r.relnamespace AND r.oid = a.attrelid AND nc.oid = c.connamespace AND r.oid =
					CASE c.contype
					WHEN 'f'::"char" THEN c.confrelid
					ELSE c.conrelid
					END AND (a.attnum = ANY (
						CASE c.contype
						WHEN 'f'::"char" THEN c.confkey
				    		ELSE c.conkey
						END)
					) AND NOT a.attisdropped AND (c.contype = ANY (ARRAY['p'::"char", 'u'::"char", 'f'::"char"])) AND 
					(r.relkind = ANY (ARRAY['r'::"char", 'p'::"char"]))
				) AS x(tblschema, tblname, tblowner, colname, cstrschema, cstrname)
			) AS t5
		ON t4.table_schema = t5.table_schema AND t4.table_name = t5.table_name AND t4.constraint_name = t5.constraint_name
	)
	SELECT	array_agg(table_name ORDER BY id) AS table_name,
		array_agg(split_part(table_name,'.',2) ORDER BY id) AS table_name_ws,
		array_agg(primary_key ORDER BY id) AS primary_key,
		array_agg(column_name ORDER BY id) AS column_name
	FROM w_all
	INTO _table_names, _table_names_ws, _primary_keys, _column_names;

	IF _table_names IS NULL
	THEN
		RAISE EXCEPTION 'There area some incosistencies between metadata and the real situation in tables with local density contributions. Array of table names (%) is NULL.', _table_names;
	END IF;

	_table_name4rule := (SELECT table_name FROM target_data.c_ldsity_object WHERE id = _ldsity_object);

-- _adc or _spc is filled, depends on categories (rules), if they are populational, or areal
-- one category (rule) can be defined under more than one superior category, each of them has to be checked

i := 1;
CASE WHEN _adc IS NOT NULL
THEN
		_adc4loop2d :=  
			(WITH w_un AS (
				SELECT	adc,
					id AS ordinality,
					ceil(id/(count(*) OVER())::double precision * array_length(_adc[i:i],2) * array_length(_adc[i:i],3)) AS array_id
				FROM 
					unnest(_adc[i:i][:]) WITH ORDINALITY AS t(adc,id)
			), w_dist AS (
				SELECT array_agg(adc ORDER BY ordinality) AS adc, array_id
				FROM w_un
				GROUP BY array_id
			)
			SELECT array_agg(adc ORDER BY array_id) 
			FROM w_dist);

		--raise notice 'adc %', _adc;
		--raise notice 'adc4loop2d %', _adc4loop2d;
		FOR y IN 1..array_length(_adc4loop2d,1)
		LOOP
			-- for 2d array, because slicing will result again in 2D array even with one element/dimension
			-- it is necessary to unnest it and aggregate again -> 1D array
			_adc4loop :=  (SELECT array_agg(adc) FROM unnest(_adc4loop2d[y:y][:]) AS t(adc));

			--raise notice 'y %, adc4loop %', y, _adc4loop;
			IF cardinality(array_remove(_adc4loop,NULL)) > 0 OR y = 1
			THEN
				--raise notice 'y %, spc4loop %', y, _spc4loop;
				FOR adcv IN SELECT generate_subscripts(_adc4loop,1)
				LOOP
				--raise notice 'adcv %', adcv;
				IF adcv = 1 OR _adc4loop[adcv] IS NOT NULL THEN

				WITH w_tables AS (
				SELECT
					t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
						CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
						CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',1, ' AS rule_number, ', _case,' AS rul ') ELSE '' END,
						' FROM ', t1.table_name, 
						CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
						') AS ', _table_names_ws[t1.id]) AS table_select
				FROM
					unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
				LEFT JOIN
					(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
					FROM
						(SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.area_domain_category || _adc4loop[adcv]) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_adc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_spc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t2.table_name, t2.filter 
						FROM target_data.c_ldsity_object AS t2
						) AS t1
					WHERE t1.classification_rule IS NOT NULL
					GROUP BY table_name
					) AS t2
				ON t1.table_name = t2.table_name
				)
				SELECT array_agg(table_select ORDER BY id)
				FROM w_tables
				INTO _table_selects;

				SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
					_table_names_ws[1], '.', _primary_keys[1], '
					FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
					INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
					INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
					INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
					_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
				INTO _table1;

				_join_all := NULL; _join := NULL;

				FOR i IN 2..array_length(_table_selects,1) 
				LOOP
					SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
					INTO _join;

					_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
					--raise notice '%', _join_all;
				END LOOP;
				--raise notice '%', _join_all;

				--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
				_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

				_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || _join_all;

				_qall := concat(CASE WHEN _qall IS NOT NULL THEN concat(_qall, ' UNION ALL ') ELSE '' END, _q);
				END IF;
				END LOOP;
			END IF;
		END LOOP;

	WHEN _spc IS NOT NULL
	THEN
		_spc4loop2d :=  
			(WITH w_un AS (
				SELECT	spc,
					id AS ordinality,
					ceil(id/(count(*) OVER())::double precision * array_length(_spc[i:i],2) * array_length(_spc[i:i],3)) AS array_id
				FROM 
					unnest(_spc[i:i][:]) WITH ORDINALITY AS t(spc,id)
			), w_dist AS (
				SELECT array_agg(spc ORDER BY ordinality) AS spc, array_id
				FROM w_un
				GROUP BY array_id
			)
			SELECT array_agg(spc ORDER BY array_id) 
			FROM w_dist);

		--raise notice 'spc %', _spc;
		--raise notice 'spc4loop2d %', _spc4loop2d;
		FOR y IN 1..array_length(_spc4loop2d,1)
		LOOP
			-- for 2d array, because slicing will result again in 2D array even with one element/dimension
			-- it is necessary to unnest it and aggregate again -> 1D array
			_spc4loop :=  (SELECT array_agg(spc) FROM unnest(_spc4loop2d[y:y][:]) AS t(spc));
			--raise notice 'i = %, spc = %, spc4loop = %', i, _spc, _spc4loop;
			IF cardinality(array_remove(_spc4loop,NULL)) > 0 OR y = 1
			THEN
				--raise notice 'y %, spc4loop %', y, _spc4loop;
				FOR spcv IN SELECT generate_subscripts(_spc4loop,1)
				LOOP

				IF spcv = 1 OR _spc4loop[spcv] IS NOT NULL THEN
				--raise notice 'spcv %', _spc4loop[spcv];

				WITH w_tables AS (
				SELECT
					t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
						CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
						CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',1, ' AS rule_number, ', _case,' AS rul ') ELSE '' END,
						' FROM ', t1.table_name, 
						CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
						') AS ', _table_names_ws[t1.id]) AS table_select
				FROM
					unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
				LEFT JOIN
					(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
					FROM
						(SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_adc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.sub_population_category || _spc4loop[spcv]) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_spc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t2.table_name, t2.filter 
						FROM target_data.c_ldsity_object AS t2
						) AS t1
					WHERE t1.classification_rule IS NOT NULL
					GROUP BY table_name
					) AS t2
				ON t1.table_name = t2.table_name
				)
				SELECT array_agg(table_select ORDER BY id)
				FROM w_tables
				INTO _table_selects;

				SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
					_table_names_ws[1], '.', _primary_keys[1], '
					FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
					INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
					INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
					INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
					_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
				INTO _table1;

				_join_all := NULL; _join := NULL;

				FOR i IN 2..array_length(_table_selects,1) 
				LOOP
					SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
					INTO _join;

					_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
					--raise notice '%', _join_all;
				END LOOP;
				--raise notice '%', _join_all;

				--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
				_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

				_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || _join_all;

				_qall := concat(CASE WHEN _qall IS NOT NULL THEN concat(_qall, ' UNION ALL ') ELSE '' END, _q);
				END IF;
				END LOOP;
			END IF;
		END LOOP;
ELSE
			WITH w_tables AS (
				SELECT
					t1.id, 	concat('(SELECT ', CASE WHEN t1.id = 1 THEN 'plot, reference_year_set, ' ELSE '' END, _primary_keys[t1.id],' AS id',
						CASE WHEN _column_names[t1.id] IS NOT NULL THEN concat(',',_column_names[t1.id]) ELSE '' END,
						CASE WHEN t1.table_name = _table_name4rule THEN concat(' ,',1, ' AS rule_number, ', _case,' AS rul ') ELSE '' END,
						' FROM ', t1.table_name, 
						CASE WHEN constraints IS NOT NULL THEN concat(' WHERE ', constraints) ELSE '' END,
						') AS ', _table_names_ws[t1.id]) AS table_select
				FROM
					unnest(_table_names) WITH ORDINALITY AS t1(table_name, id)
				LEFT JOIN
					(SELECT table_name, string_agg(concat('(',classification_rule,')'),' AND ') AS constraints
					FROM
						(SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.area_domain_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_adc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t4.table_name, t3.classification_rule 
						FROM target_data.c_ldsity AS t1,
							unnest(t1.sub_population_category) WITH ORDINALITY AS t2(rule, id)
						LEFT JOIN target_data.cm_spc2classification_rule AS t3
						ON t2.rule = t3.id
						INNER JOIN target_data.c_ldsity_object AS t4
						ON t3.ldsity_object = t4.id
						WHERE t1.id = _ldsity
						UNION ALL
						SELECT t2.table_name, t2.filter 
						FROM target_data.c_ldsity_object AS t2
						) AS t1
					WHERE t1.classification_rule IS NOT NULL
					GROUP BY table_name
					) AS t2
				ON t1.table_name = t2.table_name
			)
			SELECT array_agg(table_select ORDER BY id)
			FROM w_tables
			INTO _table_selects;

			SELECT concat(' FROM (SELECT t5.id AS panel_refyearset, ', CASE WHEN _table_names[1] = _table_name4rule THEN 'rule_number, rul, ' ELSE '' END, 
				_table_names_ws[1], '.', _primary_keys[1], '
				FROM ', _table_selects[1], ' INNER JOIN sdesign.f_p_plot AS t2 ON ', _table_names_ws[1],'.plot = t2.gid 
				INNER JOIN sdesign.t_cluster AS t3 ON t2.cluster = t3.id
				INNER JOIN sdesign.cm_cluster2panel_mapping AS t4 ON t3.id = t4.cluster
				INNER JOIN sdesign.cm_refyearset2panel_mapping AS t5 ON t4.panel = t5.panel AND ', 
				_table_names_ws[1],'.reference_year_set = t5.reference_year_set) AS ', _table_names_ws[1]) 
			INTO _table1;

			_join_all := NULL; _join := NULL;

			FOR i IN 2..array_length(_table_selects,1) 
			LOOP
				SELECT concat(' INNER JOIN ', _table_selects[i], ' ON ', _table_names_ws[i-1],'.',_primary_keys[i-1], '=', _table_names_ws[i],'.',_column_names[i])
				INTO _join;

				_join_all := concat(concat(_join_all, case when _join_all IS NOT NULL THEN E'\n' ELSE '' END),_join);
				--raise notice '%', _join_all;
			END LOOP;
			--raise notice '%', _join_all;

			--_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.' || _primary_keys[array_length(_primary_keys,1)] || ' AS id';
			_pkey := _table_names_ws[array_length(_table_names_ws,1)] || '.id, ';

			_q := 'SELECT ' || _pkey || 'panel_refyearset, rule_number, rul' || _table1 || CASE WHEN _join_all IS NOT NULL THEN _join_all ELSE '' END;
			_qall := _q;
END CASE;

	RETURN QUERY EXECUTE
	'WITH w AS MATERIALIZED (' || _qall || '),
	w2 AS (SELECT
			id, rul
		FROM
			w
		' || CASE WHEN _panel_refyearset IS NOT NULL THEN concat('WHERE panel_refyearset = ',_panel_refyearset) ELSE '' END ||'
	)
	SELECT rul AS compliance_status, count(*)::int AS no_of_objects
	FROM w2
	GROUP BY rul';

--return query select true, _no_of_rules_met, _no_of_objects;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) IS
'Function returns number of objects classified by the rule for given ldsity and attribute_type hierarchy.';

GRANT EXECUTE ON FUNCTION target_data.fn_check_classification_rule(integer, integer, text, integer, integer[], integer[]) TO public;

-- </function>

