--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



---------------------------------------------------------------------------------------------------
-- t_available_datasets => add column ldsity_threshold and last_change
---------------------------------------------------------------------------------------------------
alter table target_data.t_available_datasets add COLUMN ldsity_threshold double precision;
comment on column target_data.t_available_datasets.ldsity_threshold IS 'Saved threshold for given combination panel, reference_year_set and categorization_setup.';

alter table target_data.t_available_datasets add COLUMN last_change timestamptz;
comment on column target_data.t_available_datasets.last_change IS 'Value of last change for given panel, reference_year_set and categorization_setup. This value is change after insert, after update or after check additivity of local densities.';
---------------------------------------------------------------------------------------------------



-- <function name="fn_save_ldsity_values_internal" schema="target_data" src="functions/fn_save_ldsity_values_internal.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_save_ldsity_values_internal
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_ldsity_values_internal(integer[], integer[], double precision) CASCADE;
	
create or replace function target_data.fn_save_ldsity_values_internal
(
	_refyearset2panel_mapping	integer[],
	_categorization_setups		integer[],
	_threshold					double precision
)
returns text
as
$$
declare
		_array_id									integer[];
		_array_target_variable						integer[];
		_target_variable							integer;
		_lot_100									integer;
		_lot_200									integer;
		_variant									integer;
		_tv_target_variable							integer;
		_ldsity										integer;
		_ldsity_object_type							integer;
		_tv_area_domain_category					integer[];
		_tv_sub_population_category					integer[];
		--_tv_definition_variant					integer[];
		_tv_use_negative							boolean;
		_tv_version									integer;
		_state_or_change							integer;
		_ldsity_ldsity_object						integer;
		_ldsity_column_expresion					text;
		_ldsity_unit_of_measure						integer;
		_ldsity_area_domain_category				integer[];
		_ldsity_sub_population_category				integer[];
		_ldsity_definition_variant					integer[];	
		_area_domain_category						integer[];
		_sub_population_category					integer[];
		_definition_variant							integer[];	
		_area_domain_category_i						integer[];
		_area_domain_category_t						text[];	
		_sub_population_category_i					integer[];
		_sub_population_category_t					text[];
		_condition_ids								integer[];
		_condition_types							text[];
		_example_query_1							text;
		_example_query								text;
		_ldsity_objects								integer[];
		_filter_i									text;
		_filters_array								text[];	
		_example_query_i							text;
		_example_query_array						text[];
		_query_res									text;
		_column_name_i_array						text[];	
		_ii_column_new_text							text;
		_pozice_i									integer;
		_classification_rule_i						text;	
		_pozice_array								integer[];
		_classification_rule_array					text[];	
		_position_groups							integer[];
		_classification_rule_upr_ita_pozice			text[];
		_classification_rule_ita_pozice				text;
		_ldsity_objects_without_conditions			integer[];
		_string4columns								text;
		_string4inner_joins							text;	
		_position4join								integer;
		_table_name4join							text;
		_pkey_column4join							text;
		_i_column4join								text;
		_string4attr								text;
		_string4case								text;
		_position4ldsity							integer;
		_string4ads									text;
		_query_res_big								text;
		_string_var									text;	
		_string_result_1							text;	
		_id_cat_setups_array						integer[];
		_check_bez_rozliseni						integer[];
		_check_bez_rozliseni_boolean				boolean[];	
		_string_var_100								text;
		_array_id_200								integer[];	
		_string_var_200								text;
		_string_check_insert						text;	
		_max_id_tlv									integer;
		_res										text;
		_ext_version_text							text;
		_case4available_datasets_text				text;
		_categorization_setups_adc					integer[];
		_categorization_setups_spc					integer[];
		_ldsity_column_expresion_modified			text;
		_last_change								timestamptz;
	
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_save_ldsity_values_internal: The input argument _refyearset2panel_mapping must not be NULL !';
		end if;
		
		if _categorization_setups is null
		then
			raise exception 'Error 02: fn_save_ldsity_values_internal: The input argument _categorization_setups must not be NULL !';
		end if;
	
		if	(
			select count(t.res) > 0 from
			(select unnest(_categorization_setups) as res) as t
			where t.res is null
			)
		then
			raise exception 'Error 03: fn_save_ldsity_values_internal: The input argument _categorization_setups must not contains NULL value!';
		end if;
	
		/*
		-------------------------------------------------------------
		-------------------------------------------------------------
		with
		w as	(
				select distinct categorization_setup
				from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(	
					select id from target_data.cm_ldsity2target_variable
					where target_variable =
								(
								select distinct target_variable from target_data.cm_ldsity2target_variable
								where id in	(
											select ldsity2target_variable
											from target_data.cm_ldsity2target2categorization_setup
											where categorization_setup in (select unnest(_categorization_setups))
											)
								)
					)
				and (
					adc2classification_rule is null
					or array_length(adc2classification_rule,1) <= 
						(select max(t.res) from
						(select array_length(adc2classification_rule,1) as res
						from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						and adc2classification_rule is not null) as t)
					)
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups_adc;

		with
		w as	(
				select distinct categorization_setup
				from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(	
					select id from target_data.cm_ldsity2target_variable
					where target_variable =
								(
								select distinct target_variable from target_data.cm_ldsity2target_variable
								where id in	(
											select ldsity2target_variable
											from target_data.cm_ldsity2target2categorization_setup
											where categorization_setup in (select unnest(_categorization_setups))
											)
								)
					)
				and (
					spc2classification_rule is null
					or array_length(spc2classification_rule,1) <= 
						(select max(t.res) from
						(select array_length(spc2classification_rule,1) as res
						from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						and spc2classification_rule is not null) as t)
					)
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups_spc;

		with
		w as	(
				select a.categorization_setup from
					(select unnest(_categorization_setups_adc) as categorization_setup) as a
				inner join
					(select unnest(_categorization_setups_spc) as categorization_setup) as b
				on a.categorization_setup = b.categorization_setup
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups;
		-------------------------------------------------------------
		-------------------------------------------------------------
		*/

		select
				array_agg(id order by ldsity_object_type, id) as id,
				array_agg(target_variable order by ldsity_object_type, id) as target_variable
		from
				target_data.cm_ldsity2target_variable
		where
				id in	(
						select ldsity2target_variable from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						)
		into
				_array_id,
				_array_target_variable;
		
		if _array_id is null
		then
			raise exception 'Error 04: fn_save_ldsity_values_internal: For input values in argument _categorization_setups = % not found any record in table cm_ldsity2target_variable!',_categorization_setups;
		end if;
		
		_target_variable := (select distinct t.res from (select unnest(_array_target_variable) as res) as t);
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 05: fn_save_ldsity_values_internal: This variant is not implemented yet!';
		end if;
	
		---------------------
		---------------------
		for bc in 1..array_length(_array_id,1)
		loop
			-- datas from table cm_ldsity2target_variable
			select
					target_variable,
					ldsity,
					ldsity_object_type,
					area_domain_category,
					sub_population_category,
					--definition_variant,
					use_negative,
					version
			from
					target_data.cm_ldsity2target_variable
			where
					id = _array_id[bc]
			into
					_tv_target_variable,
					_ldsity,
					_ldsity_object_type,
					_tv_area_domain_category,
					_tv_sub_population_category,
					--_tv_definition_variant,
					_tv_use_negative,
					_tv_version;
				
			-- datas from c_target_variable	
			select state_or_change from target_data.c_target_variable where id = _tv_target_variable
			into _state_or_change;
		
			-----------------------------------------------------------
			-- INTERMEDIATE STEP => insert values into table t_available_datasets
			-----------------------------------------------------------
			/*
			with
			w1 as	(
					select distinct reference_year_set, panel
					from target_data.fn_get_plots(_panels,_reference_year_sets,_state_or_change,_tv_use_negative)
					order by reference_year_set, panel
					)
			,w2 as	(select unnest(array[_categorization_setups]) as categorization_setup)
			,w3 as	(select w1.*, w2.* from w1, w2)
			insert into target_data.t_available_datasets(panel, reference_year_set, categorization_setup)
			select panel, reference_year_set, categorization_setup from w3
			except
			select panel, reference_year_set, categorization_setup from target_data.t_available_datasets
			where categorization_setup in (select unnest(array[_categorization_setups]))
			order by categorization_setup, panel, reference_year_set;
			*/

			_last_change := now();

			with
			w1 as	(
					select distinct reference_year_set, panel
					from target_data.fn_get_plots(_refyearset2panel_mapping,_state_or_change,_tv_use_negative)
					order by reference_year_set, panel
					)
			,w2 as	(select unnest(array[_categorization_setups]) as categorization_setup)
			,w3 as	(select w1.*, w2.* from w1, w2)
			,w4 as	(-- new records for insert
					select w3.panel, w3.reference_year_set, w3.categorization_setup from w3
					except
					select tad.panel, tad.reference_year_set, tad.categorization_setup from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select unnest(array[_categorization_setups]))
					)
			,w5 as	(-- recods for update
					select w3.panel, w3.reference_year_set, w3.categorization_setup from w3
					except
					select w4.panel, w4.reference_year_set, w4.categorization_setup from w4
					)
			,w6 as	(
					update target_data.t_available_datasets
					set
						ldsity_threshold = _threshold,
						last_change = _last_change
					where
						panel in (select w5.panel from w5)
					and
						reference_year_set in (select w5.reference_year_set from w5)
					and
						categorization_setup in (select w5.categorization_setup from w5)
					)
			insert into target_data.t_available_datasets(panel, reference_year_set, categorization_setup, ldsity_threshold, last_change)
			select w4.panel, w4.reference_year_set, w4.categorization_setup, _threshold, _last_change from w4
			order by w4.categorization_setup, w4.panel, w4.reference_year_set;			
			-----------------------------------------------------------
			-----------------------------------------------------------
	
			-- datas from table c_ldsity
			select
					ldsity_object,
					column_expression,
					unit_of_measure,
					area_domain_category,
					sub_population_category,
					definition_variant
			from
					target_data.c_ldsity where id = _ldsity
			into
					_ldsity_ldsity_object,
					_ldsity_column_expresion,
					_ldsity_unit_of_measure,
					_ldsity_area_domain_category,
					_ldsity_sub_population_category,
					_ldsity_definition_variant;
							 
			_area_domain_category := _ldsity_area_domain_category || _tv_area_domain_category;
			_sub_population_category := _ldsity_sub_population_category || _tv_sub_population_category;
			_definition_variant := _ldsity_definition_variant; --|| _tv_definition_variant;

			-----------------------------------------
			if _area_domain_category is not null
			then
				for i in 1..array_length(_area_domain_category,1)
				loop
					if i = 1
					then
						_area_domain_category_i := array[_area_domain_category[i]];
						_area_domain_category_t := array['ad'];
					else
						_area_domain_category_i := _area_domain_category_i || array[_area_domain_category[i]];
						_area_domain_category_t := _area_domain_category_t || array['ad'];
					end if;
				end loop;
			else
				_area_domain_category_i := null::integer[];
				_area_domain_category_t := null::text[];
			end if;
			-----------------------------------------
			-----------------------------------------
			if _sub_population_category is not null
			then
				for i in 1..array_length(_sub_population_category,1)
				loop
					if i = 1
					then
						_sub_population_category_i := array[_sub_population_category[i]];
						_sub_population_category_t := array['sp'];
					else
						_sub_population_category_i := _sub_population_category_i || array[_sub_population_category[i]];
						_sub_population_category_t := _sub_population_category_t || array['sp'];
					end if;
				end loop;
			else
				_sub_population_category_i := null::integer[];
				_sub_population_category_t := null::text[];
			end if;
			-----------------------------------------
			-----------------------------------------
	
			_condition_ids := _area_domain_category_i || _sub_population_category_i;
			_condition_types := _area_domain_category_t || _sub_population_category_t;
	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if _condition_ids is null
			then
				--_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_panels,'],array[',_reference_year_sets,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');  -- pkey pro testy nahradit sloupcem plot
				_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_refyearset2panel_mapping,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');  -- pkey pro testy nahradit sloupcem plot
				_example_query := concat('w_&i& as (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#)');
			else
				--_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_panels,'],array[',_reference_year_sets,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');
				_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_refyearset2panel_mapping,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');
				_example_query := concat('w_&i& as (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#)');
			end if;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_ldsity_objects := target_data.fn_get_ldsity_objects(array[_ldsity_ldsity_object]);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					select case when filter is null then 'true' else filter end
					from target_data.c_ldsity_object
					where id = _ldsity_objects[i]
					into _filter_i;
				
					if i = 1
					then
						_filters_array := array[_filter_i];
					else
						_filters_array := _filters_array || array[_filter_i];
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					if _ldsity_objects[i] = 100
					then
						_example_query_i := _example_query_1;
					else
						_example_query_i := _example_query;
					end if;
				
					_example_query_i := replace(_example_query_i,'&i&'::text,i::text);
					_example_query_i := replace(_example_query_i,'#con4filter#',(select case when filter is null then 'true' else filter end from target_data.c_ldsity_object where id = _ldsity_objects[i]));
					_example_query_i := replace(_example_query_i,concat('#table_name#_',i),(select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]));				
				
					if i = 1
					then
						_example_query_array := array[_example_query_i];
					else
						_example_query_array := _example_query_array || _example_query_i;
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					if i = 1
					then
						_query_res := concat('with ',_example_query_array[i]);
					else
						_query_res := _query_res || concat(',',_example_query_array[i]);
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
				select array_agg(column_name order by ordinal_position) from information_schema.columns
			 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
			 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
				into _column_name_i_array;
				
				for ii in 1..array_length(_column_name_i_array,1)
				loop
					if ii = 1
					then
						_ii_column_new_text := concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i);
					else
						_ii_column_new_text := concat(_ii_column_new_text,', ',concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i));
					end if;
				end loop;
			
				_query_res := replace(_query_res,concat('#column_names_',i::text,'#'),_ii_column_new_text);
				
			end loop;	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if _condition_ids is not null
			then
				for i in 1..array_length(_condition_ids,1)
				loop
					if _condition_types[i] = 'ad'
					then			
						select array_position(_ldsity_objects,
						(select ldsity_object from target_data.cm_adc2classification_rule where id = _condition_ids[i]))
						into _pozice_i;
					
						if _pozice_i is null then raise exception 'Error 06: fn_save_ldsity_values_internal: For internal variable _condition_ids[i] = % not found position in internal variable _ldsity_objects = % !',_condition_ids[i],_ldsity_objects; end if;
									
						select case when classification_rule is null then 'true' else classification_rule end
						from target_data.cm_adc2classification_rule where id = _condition_ids[i]
						into _classification_rule_i;
					
						if _classification_rule_i is null then raise exception 'Error 07: fn_save_ldsity_values_internal: The classification rule in the cm_adc2classification_rule table was not found for the internal variable _condition_ids[i] = %!,',_condition_ids[i]; end if;
					
						if _classification_rule_i = any(array['EXISTS','NOT EXISTS'])
						then
							raise exception 'Error 08: fn_save_ldsity_values_internal: The ADC classification rule = % is not allowed as a reducing condition!',_classification_rule_i;
						end if;
					end if;
				
					if _condition_types[i] = 'sp'
					then			
						select array_position(_ldsity_objects,
						(select ldsity_object from target_data.cm_spc2classification_rule where id = _condition_ids[i]))
						into _pozice_i;				
					
						if _pozice_i is null then raise exception 'Error 09: fn_save_ldsity_values_internal: For internal variable _condition_ids[i] = % not found position in internal variable _ldsity_objects = % !',_condition_ids[i],_ldsity_objects; end if;
								
						select case when classification_rule is null then 'true' else classification_rule end
						from target_data.cm_spc2classification_rule where id = _condition_ids[i]
						into _classification_rule_i;
					
						if _classification_rule_i is null then raise exception 'Error 10: fn_save_ldsity_values_internal: The classification rule in the cm_spc2classification_rule table was not found for the internal variable _condition_ids[i] = %!,',_condition_ids[i]; end if;				
					
						if _classification_rule_i = any(array['EXISTS','NOT EXISTS'])
						then
							raise exception 'Error 11: fn_save_ldsity_values_internal: The SPC classification rule = % is not allowed as a reducing condition!',_classification_rule_i;
						end if;					
					end if;
				
					if i = 1
					then
						_pozice_array := array[_pozice_i];
						_classification_rule_array := array[_classification_rule_i];
					else
						_pozice_array := _pozice_array || array[_pozice_i];
						_classification_rule_array := _classification_rule_array || array[_classification_rule_i];
					end if;
					
				end loop;
				
				with
				w1 as	(select unnest(_pozice_array) as pozice)
				,w2 as 	(select distinct pozice from w1)
				select array_agg(pozice order by pozice) from w2
				into _position_groups;
			
				for i in 1..array_length(_position_groups,1)
				loop
					with
					w1 as	(
							select
							unnest(_pozice_array) as pozice,
							unnest(_classification_rule_array) as classification_rule
							)
					,w2 as	(
							select
									row_number() over () as new_id,
									pozice,
									classification_rule
							from w1
							)
					,w3 as	(
							select
									row_number() over (partition by pozice order by new_id) as new_id4order,
									w2.*
							from w2
							)
					select array_agg(classification_rule order by new_id4order) as classification_rule
					from w3 where pozice = _position_groups[i]
					into _classification_rule_upr_ita_pozice;
				
					for ii in 1..array_length(_classification_rule_upr_ita_pozice,1)
					loop
						if ii = 1
						then
							_classification_rule_ita_pozice := concat(_classification_rule_upr_ita_pozice[ii]);
						else
							_classification_rule_ita_pozice := concat(_classification_rule_ita_pozice,' and ',_classification_rule_upr_ita_pozice[ii]);
						end if;
					end loop;
				
					_query_res := replace(_query_res,concat('#conditions_',_position_groups[i],'#'),concat('(',_classification_rule_ita_pozice,')'));
				
				end loop;
			
				with
				w1 as	(select * from generate_series(1,array_length(_ldsity_objects,1)) as res)
				,w2 as	(select unnest(_position_groups) as res)
				,w3 as	(select res from w1 except select res from w2)
				select array_agg(res order by res) from w3
				into _ldsity_objects_without_conditions;
				
				if _ldsity_objects_without_conditions is not null
				then
					for i in 1..array_length(_ldsity_objects_without_conditions,1)
					loop
						_query_res := replace(_query_res,concat('#conditions_',_ldsity_objects_without_conditions[i],'#'),'(true)');
					end loop;
				end if;
						
			end if;	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
				if i = 1
				then
					_string4columns := concat(',w_res as (select w_',i,'.*');
					_string4inner_joins := concat(' from w_',i);
				else
					_string4columns := concat(_string4columns,', w_',i,'.*');
				
					select array_position(_ldsity_objects, 
						(select upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]))
					into _position4join;
					
					for i in 1..array_length(_ldsity_objects,1)
					loop
							if i = _position4join
							then
								_table_name4join := (select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]);
							end if;
					end loop;
				
					with
					w as	(
							select kcu.table_schema,
							       kcu.table_name,
							       tco.constraint_name,
							       kcu.ordinal_position as position,
							       kcu.column_name as key_column
							from information_schema.table_constraints tco
							join information_schema.key_column_usage kcu 
							     on kcu.constraint_name = tco.constraint_name
							     and kcu.constraint_schema = tco.constraint_schema
							     and kcu.constraint_name = tco.constraint_name
							where tco.constraint_type = 'PRIMARY KEY'
							order by kcu.table_schema,
							         kcu.table_name,
							         position
							 )
					select key_column::text from w
					where table_schema = (substring(_table_name4join from 1 for (position('.' in _table_name4join) - 1))) 
					and table_name = (substring(_table_name4join from (position('.' in _table_name4join) + 1) for length(_table_name4join)))
					into _pkey_column4join;
					
					select column4upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]
					into _i_column4join;
				
					_string4inner_joins :=
					concat(_string4inner_joins,' inner join w_',i,' on ','w_',_position4join,'.',_pkey_column4join,'_',_position4join,' = ','w_',i,'.',_i_column4join,'_',i);
							
				end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------		
			_query_res := _query_res || _string4columns || _string4inner_joins || ')';	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if	(_variant in (3,4) and _ldsity_ldsity_object is distinct from (select id from target_data.c_ldsity_object where upper_object is null))
			then
				_string4attr := concat('
				select
					case
						when ws100.adc2classification_rule is null
								then array[0]
								else (select id_category from target_data.fn_get_category4classification_rule_id(''adc'',ws100.adc2classification_rule))
					end as area_domain_category,
					case
						when ws200.spc2classification_rule is null
								then array[0]
								else (select id_category from target_data.fn_get_category4classification_rule_id(''spc'',ws200.spc2classification_rule))
					end as sub_population_category,
					ws100.adc2classification_rule,
					ws200.spc2classification_rule,
					ws200.categorization_setup
				from			
					(		
					select
							adc2classification_rule,
							spc2classification_rule,
							categorization_setup
					from
							target_data.cm_ldsity2target2categorization_setup
					where
							categorization_setup in
													(
													select unnest($1)
													except
													select t.categorization_setup from
													(select a.*, b.* from
													(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest($1))
													and ldsity2target_variable = ',_array_id[bc],') as a
													inner join target_data.cm_spc2classification_rule as b
													on a.id_cm_spc = b.id) as t
													where t.classification_rule = ''NOT EXISTS''	
													)
					and
							ldsity2target_variable = ',_array_id[bc],'
					) as ws200
				inner join 
					(					
					select adc2classification_rule, categorization_setup from target_data.cm_ldsity2target2categorization_setup
					where ldsity2target_variable = ',_array_id[1],'
					) as ws100
				on
					ws200.categorization_setup = ws100.categorization_setup	
				');
			else			
				_string4attr := concat('
				select
						case
							when adc2classification_rule is null
									then array[0]
									else (select id_category from target_data.fn_get_category4classification_rule_id(''adc'',adc2classification_rule))
						end as area_domain_category,
						case
							when spc2classification_rule is null
									then array[0]
									else (select id_category from target_data.fn_get_category4classification_rule_id(''spc'',spc2classification_rule))
						end as sub_population_category,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest($1))
				and
						ldsity2target_variable = ',_array_id[bc],'
				');
			end if;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			-- dopleni radku bez rozliseni pokud schazi, radek je potreba u varianty 3 a 4 !!!
			if _variant in (3,4)
			then	
				for i in 1..array_length(_categorization_setups,1)
				loop
					select array_agg(id order by id)
					from target_data.cm_ldsity2target2categorization_setup
					where categorization_setup = _categorization_setups[i]
					into _id_cat_setups_array;
				
					for ii in 1..array_length(_id_cat_setups_array,1)
					loop
						if	(
							select count(*) = 1
							from target_data.cm_ldsity2target2categorization_setup
							where id = _id_cat_setups_array[ii]
							and (adc2classification_rule is null and spc2classification_rule is null)
							)
						then
							if ii = 1
							then
								_check_bez_rozliseni := array[1];
							else
								_check_bez_rozliseni := _check_bez_rozliseni || array[1];
							end if;
						else
							if ii = 1
							then
								_check_bez_rozliseni := array[0];
							else
								_check_bez_rozliseni := _check_bez_rozliseni || array[0];
							end if;
						end if;
					end loop;
				
					if		(
							select sum(t.res) = array_length(_id_cat_setups_array,1)
							from (select unnest(_check_bez_rozliseni) as res) as t
							)
					then
						if i = 1
						then
							_check_bez_rozliseni_boolean := array[true];
						else
							_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[true];
						end if;
					else
						if i = 1
						then
							_check_bez_rozliseni_boolean := array[false];
						else
							_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[false];
						end if;
					end if;	
				end loop;
			
				if	(
					select count(t.res) = 0
					from (select unnest(_check_bez_rozliseni_boolean) as res) as t
					where t.res = true
					)
				then
					with
					w1 as	(
							select * from target_data.cm_ldsity2target2categorization_setup
							where ldsity2target_variable in	(
															select distinct ldsity2target_variable
															from target_data.cm_ldsity2target2categorization_setup
															where categorization_setup in (select unnest(_categorization_setups))
															)
							and (adc2classification_rule is null and spc2classification_rule is null)
							)
					,w2 as	(
							select categorization_setup, count(categorization_setup) as pocet
							from w1 group by categorization_setup
							)
					select array[categorization_setup] || _categorization_setups
					from w2 where pocet = (select max(pocet) from w2)
					into _categorization_setups;
				
				else
					_categorization_setups := _categorization_setups;
				end if;
			end if;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_string4case := target_data.fn_get_classification_rules4categorization_setups(_categorization_setups,_array_id[bc]);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_position4ldsity := array_position(_ldsity_objects,_ldsity_ldsity_object);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_ldsity_column_expresion_modified := target_data.fn_get_modified_ldsity_column_expression(_ldsity_column_expresion,_position4ldsity,_ldsity_ldsity_object);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_string4ads := concat('
			,w_strings as	('||_string4attr||')
			,w_pre_adc as	(-- tady je cross join => zde dojde ke kazdemu zaznamu z w_res k rozjoinovani na vsechny kategorie, ktere jsou ve w_strings
							select
									w_strings.*,
									w_res.*
							from
									w_res, w_strings
							)
			,w_ads as		(
							select
									',_string4case,' as res_case,
									w_pre_adc.*
							from
									w_pre_adc
							)
			select
					res_case,
					area_domain_category,
					sub_population_category,
					adc2classification_rule,
					spc2classification_rule,
					(',case when _tv_use_negative = true then '(-1)' else '(1)' end,' * coalesce(',_ldsity_column_expresion_modified,',0.0)) as value,
					gid,
					cluster_configuration,
					reference_year_set4join,
					reference_year_set,
					panel,
					stratum,
					categorization_setup
			from
					w_ads
			');	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_query_res := _query_res || _string4ads;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if bc = 1
			then
				_query_res_big := concat('with bw_',bc,' as (',_query_res,')');
			else
				_query_res_big := concat(_query_res_big,',bw_',bc,' as (',_query_res,')');
			end if;
		
		end loop;		
		-----------------------------------------------------------------------
		-- end BIG CYCLE
		-----------------------------------------------------------------------

		_ext_version_text := (select extversion from pg_extension where extname = 'nfiesta_target_data');

		_case4available_datasets_text := target_data.fn_get_case_ad4categorization_setups(_categorization_setups);
		
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _variant in (1,2)
		then		
				for bc in 1..array_length(_array_id,1)
				loop
					if bc = 1
					then
						_string_var := concat(',w_union_all as (select ',_array_id[bc],' as ldsity2target_variable,* from bw_',bc,' where res_case = true');
					else
						_string_var := concat(_string_var,' union all select ',_array_id[bc],' as ldsity2target_variable,* from bw_',bc,' where res_case = true');
					end if;
				end loop;
			
				_string_var := concat(_string_var,')');
			
				_string_result_1 := concat(
				'
				,w_part_1 as		(
									select
											gid,
											panel,
											reference_year_set,
											categorization_setup,
											sum(value) as value
									from
											w_union_all as t1
									group
											by gid, panel, reference_year_set, categorization_setup
									)
				,w_result_1 as		(
									select
											t1.gid as plot,
											t1.panel,
											t1.reference_year_set,
											t1.categorization_setup,
											t1.value,
											',_case4available_datasets_text,' as available_datasets
									from
											w_part_1 as t1 where t1.value is distinct from 0 -- toto zajisti praci s nenulovymi hodnotama
									)
				');

		_query_res_big := _query_res_big || _string_var || _string_result_1;
	
		end if;

		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _variant in (3,4)
		then
			/*		
			for i in 1..array_length(_categorization_setups,1)
			loop
				select array_agg(id order by id)
				from target_data.cm_ldsity2target2categorization_setup
				where categorization_setup = _categorization_setups[i]
				into _id_cat_setups_array;
			
				for ii in 1..array_length(_id_cat_setups_array,1)
				loop
					if	(
						select count(*) = 1
						from target_data.cm_ldsity2target2categorization_setup
						where id = _id_cat_setups_array[ii]
						and (adc2classification_rule is null and spc2classification_rule is null)
						)
					then
						if ii = 1
						then
							_check_bez_rozliseni := array[1];
						else
							_check_bez_rozliseni := _check_bez_rozliseni || array[1];
						end if;
					else
						if ii = 1
						then
							_check_bez_rozliseni := array[0];
						else
							_check_bez_rozliseni := _check_bez_rozliseni || array[0];
						end if;
					end if;
				end loop;
			
				if		(
						select sum(t.res) = array_length(_id_cat_setups_array,1)
						from (select unnest(_check_bez_rozliseni) as res) as t
						)
				then
					if i = 1
					then
						_check_bez_rozliseni_boolean := array[true];
					else
						_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[true];
					end if;
				else
					if i = 1
					then
						_check_bez_rozliseni_boolean := array[false];
					else
						_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[false];
					end if;
				end if;	
			end loop;
		
			if	(
				select count(t.res) = 0
				from (select unnest(_check_bez_rozliseni_boolean) as res) as t
				where t.res = true
				)
			then
				with
				w1 as	(
						select * from target_data.cm_ldsity2target2categorization_setup
						where ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))
														)
						and (adc2classification_rule is null and spc2classification_rule is null)
						)
				,w2 as	(
						select categorization_setup, count(categorization_setup) as pocet
						from w1 group by categorization_setup
						)
				select array[categorization_setup] || _categorization_setups
				from w2 where pocet = (select max(pocet) from w2)
				into _categorization_setups;
			
			else
				_categorization_setups := _categorization_setups;
			end if;
			*/

			_string_var_100 := concat(',w_union_all_100 as (select ',_array_id[1],' as ldsity2target_variable,* from bw_1 where res_case = true)');
		
			_array_id_200 := array_remove(_array_id, _array_id[1]);
				
			for bc_200 in 1..array_length(_array_id_200,1)
			loop
				if bc_200 = 1
				then
					_string_var_200 := concat(',w_union_all_200 as (select ',_array_id_200[bc_200],' as ldsity2target_variable,* from bw_',bc_200 + 1,' where res_case = true');
				else
					_string_var_200 := concat(_string_var_200,' union all select ',_array_id_200[bc_200],' as ldsity2target_variable,* from bw_',bc_200 + 1,' where res_case = true');
				end if;				
			end loop;
		
			_string_var_200 := concat(_string_var_200,')');
			
			_string_result_1 := concat('
			,w_part_1 as		(
								select
										a.gid,
										a.panel,
										a.reference_year_set,
										a.categorization_setup,
										a.available_datasets,
										sum(a.value) as value
								from
										(
										select
												t1.*,
												',_case4available_datasets_text,' as available_datasets
										from
												w_union_all_100 as t1
										where
												value is distinct from 0.0
										) as a
								group
										by a.gid, a.panel, a.reference_year_set, a.categorization_setup, a.available_datasets			
								)
			,w_part_2 as		(			
								select
									t1.gid,
									t1.panel,
									t1.reference_year_set,
									t1.categorization_setup,
									t1.area_domain_category,
									t1.value,
									t2.value_sum,
									case when t2.value_sum = 0.0 then t2.value_sum else t1.value/t2.value_sum end as value4nasobeni,
									',_case4available_datasets_text,' as available_datasets
								from
									(
									select
									gid, panel, reference_year_set, categorization_setup, area_domain_category,
									sum(value) as value
									from w_union_all_200
									where value is distinct from 0.0
									group by gid, panel, reference_year_set, categorization_setup, area_domain_category
									) as t1
								inner join 
									(
									select gid, panel, reference_year_set,
									sum(value) as value_sum
									from w_union_all_200
									where (area_domain_category = array[0] and sub_population_category = array[0])
									and value is distinct from 0.0
									group by gid, panel, reference_year_set
									) as t2
								on
									t1.gid = t2.gid and
									t1.panel = t2.panel and
									t1.reference_year_set = t2.reference_year_set
							)
			,w_result as	(
							select
									t.*,
									coalesce((t.value * coef_200.value4nasobeni),0.0) as value_res
							from 
									w_part_1 as t inner join w_part_2 as coef_200
							on
									t.gid = coef_200.gid and t.available_datasets = coef_200.available_datasets
							)
			,w_not_exists as	(
								/*
								select * from w_part_1
								where gid in (select distinct gid from w_part_1 except select distinct gid from w_part_2)
								and categorization_setup in							
									(
										-- category non-sortable
										select t1.categorization_setup from
										(select a.*, b.* from
										(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
										from target_data.cm_ldsity2target2categorization_setup
										where ldsity2target_variable in (select unnest($2))
										) as a inner join target_data.cm_spc2classification_rule as b
										on a.id_cm_spc = b.id) as t1
										where t1.classification_rule = ''NOT EXISTS''
										union all
										-- category without distinction
										select t2.categorization_setup
										from	(
												select categorization_setup, count(categorization_setup) as pocet
												from target_data.cm_ldsity2target2categorization_setup
												where ldsity2target_variable in (select unnest($2))
												and (adc2classification_rule is null and spc2classification_rule is null)
												group by categorization_setup
												) as t2
										where t2.pocet = array_length($2,1)
									)
								*/

								select * from w_part_1
								where gid in (select distinct gid from w_part_1 except select distinct gid from w_part_2)
								and categorization_setup in							
									(
										-- category non-sortable
										select distinct t1.categorization_setup from
										(select a.*, b.* from
										(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
										from target_data.cm_ldsity2target2categorization_setup
										where ldsity2target_variable in (select unnest($2))
										) as a inner join target_data.cm_spc2classification_rule as b
										on a.id_cm_spc = b.id) as t1
										where t1.classification_rule = ''NOT EXISTS''
										union all
										-- categorization_setup that is a category without distinction
										select t2.categorization_setup
										from	(
												select categorization_setup, count(categorization_setup) as pocet
												from target_data.cm_ldsity2target2categorization_setup
												where ldsity2target_variable in (select unnest($2))
												and (adc2classification_rule is null and spc2classification_rule is null)
												group by categorization_setup
												) as t2
										where t2.pocet = array_length($2,1)
										union all
										-- categorization_setup for	ADC																			
										select categorization_setup
										from target_data.cm_ldsity2target2categorization_setup
										where categorization_setup in
											(
											select t.categorization_setup from 
											(select categorization_setup, count(categorization_setup) as pocet
											from target_data.cm_ldsity2target2categorization_setup
											where ldsity2target_variable in (select unnest($2))
											and spc2classification_rule is null
											group by categorization_setup) as t
											where t.pocet = array_length($2,1)
											)
										and adc2classification_rule in
										(
										-- category non-sortable for ADC
										select adc2classification_rule from
										target_data.cm_ldsity2target2categorization_setup
										where categorization_setup in 
										(select distinct t1.categorization_setup from
										(select a.*, b.* from
										(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
										from target_data.cm_ldsity2target2categorization_setup
										where ldsity2target_variable in (select unnest($2))
										) as a inner join target_data.cm_spc2classification_rule as b
										on a.id_cm_spc = b.id) as t1
										where t1.classification_rule = ''NOT EXISTS'')
										and adc2classification_rule is not null
										)
									)
								)
			,w_result_1 as	(
							select gid as plot, value_res as value, available_datasets from w_result union --all
							select gid as plot, value, available_datasets from w_not_exists
							)
			');
		
			_query_res_big := _query_res_big || _string_var_100 || _string_var_200 || _string_result_1;
		
		end if;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
			
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		_string_check_insert := replace(replace(concat
		(',w_tlv as		(
						select
								tlv.id,
								tlv.plot,
								tlv.available_datasets,
								tlv.value,
								coalesce(w_result_1.plot,tlv.plot) as plot_upr,
								coalesce(w_result_1.available_datasets,tlv.available_datasets) as available_datasets_upr,
								case
									when tlv.value is     null and w_result_1.value is not null then w_result_1.value
									when tlv.value is not null and tlv.value is distinct from 0.0 and w_result_1.value is null then 0.0
									when tlv.value is not null and tlv.value = 0.0 and w_result_1.value is null then null::double precision
									else w_result_1.value
								end
									as value_upr,
								case
									when tlv.value is     null and w_result_1.value is not null then false
									when tlv.value is not null and tlv.value is distinct from 0.0 and w_result_1.value is null then false
									when tlv.value is not null and tlv.value = 0.0 and w_result_1.value is null then null::boolean									
									else
										case
											when tlv.value = 0.0 and w_result_1.value = 0.0 then true
											when tlv.value = 0.0 and w_result_1.value is distinct from 0.0
													then
														case
															when (abs(1 - (tlv.value / w_result_1.value)) * 100.0) <= ',_threshold,' then true
															else false
														end
											when tlv.value is distinct from 0.0 and w_result_1.value = 0.0
													then
														case
															when (abs(1 - (w_result_1.value / tlv.value)) * 100.0) <= ',_threshold,'	then true
															else false
														end
											else
												case
													when (abs(1 - (tlv.value / w_result_1.value)) * 100.0) <= ',_threshold,' then true
													else false
												end
										end									
								end
									as value_identic
						from
							(
							select * from target_data.t_ldsity_values
							where available_datasets in
								(
								select
									a.id
								from
									(
									select * from target_data.t_available_datasets
									where categorization_setup in (select unnest($1))
									) as a
								inner
								join
									(
									select * from sdesign.cm_refyearset2panel_mapping
									where id in (select unnest(array[',_refyearset2panel_mapping,']))
									) as b
								on
									a.panel = b.panel and a.reference_year_set = b.reference_year_set
								)
							and is_latest = true
							) as tlv
						full outer join w_result_1
						on (tlv.plot = w_result_1.plot and tlv.available_datasets = w_result_1.available_datasets)
						)
		,w_update as	(
						update target_data.t_ldsity_values set is_latest = false where id in
						(select id from w_tlv where value_identic = false and id is not null)
						returning t_ldsity_values.plot, t_ldsity_values.available_datasets
						)
		insert into target_data.t_ldsity_values(plot,available_datasets,value,creation_time,is_latest,ext_version)
		select
				w_tlv.plot_upr as plot,
				w_tlv.available_datasets_upr as available_datasets,
				w_tlv.value_upr as value,
				$3 as creation_time,
				true as is_latest,
				''',_ext_version_text,''' as ext_version
		from
				w_tlv
				left join w_update on w_tlv.plot_upr = w_update.plot and w_tlv.available_datasets_upr = w_update.available_datasets
		where
				w_tlv.value_identic = false
		order
				by w_tlv.plot_upr, w_tlv.available_datasets_upr;
		'),'{',''),'}','');	
	
	_query_res_big := _query_res_big || _string_check_insert;

	---------------------------------------------
	---------------------------------------------
	select coalesce(max(id),0) from target_data.t_ldsity_values into _max_id_tlv;
	---------------------------------------------
	---------------------------------------------
	execute ''||_query_res_big||'' using _categorization_setups, _array_id, _last_change;
	---------------------------------------------
	---------------------------------------------
	_res := concat('The ',(select count(*) from target_data.t_ldsity_values where id > _max_id_tlv),' new local densities were prepared for ',(select count(t.plot) from (select distinct plot from target_data.t_ldsity_values where id > _max_id_tlv) as t),' plots.');
	---------------------------------------------
	---------------------------------------------
	return _res;
	---------------------------------------------
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_save_ldsity_values_internal(integer[], integer[], double precision) IS
'The function for the specified list of input arguments inserts data into the t_available_datasets table and inserts data into the t_ldsity_values table (aggregated local density at the plot level).';

grant execute on function target_data.fn_save_ldsity_values_internal(integer[], integer[], double precision) to public;
-- </function>



-- <function name="fn_etl_check_target_variable" schema="target_data" src="functions/etl/fn_etl_check_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_check_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_check_target_variable(integer, integer) CASCADE;

create or replace function target_data.fn_etl_check_target_variable
(
	_export_connection		integer,
	_target_variable		integer
)
returns table
(
	id_etl_target_variable		integer,
	check_target_variable		boolean,
	refyearset2panel_mapping	integer[]
)
as
$$
declare
		_id_etl_target_variable		integer;
		_refyearset2panel_mapping	integer[];
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_check_target_variable: Input argument _export_connection must not by NULL!';
		end if; 
	
		if _target_variable is null
		then
			raise exception 'Error 02: fn_etl_check_target_variable: Input argument _target_variable must not by NULL!';
		end if;
	
		select id from target_data.t_etl_target_variable
		where export_connection = _export_connection
		and target_variable = _target_variable
		into _id_etl_target_variable;
	
		if _id_etl_target_variable is null
		then
			return query select
								_id_etl_target_variable as etl_target_variable,
								false as check_target_variable,
								null::integer[] as refyearset2panel_mapping;
		else
			with
			w1 as	(
					select * from target_data.t_etl_log
					where etl_target_variable = _id_etl_target_variable
					)
			,w2 as	(
					select w1.refyearset2panel_mapping, max(w1.etl_time) as max_etl_time from w1
					group by w1.refyearset2panel_mapping
					)
			,w3 as	(
					select w1.*, w2.max_etl_time from w1 inner join w2
					on w1.refyearset2panel_mapping = w2.refyearset2panel_mapping
					and w1.etl_time = w2.max_etl_time
					)
			,w4 as	(-- list of panels and reference year sets that was ETL, source is from t_etl_log
					select
							w3.id as id_etl_log,
							w3.etl_target_variable,
							w3.refyearset2panel_mapping as refyearset2panel_mapping_in_log,
							w3.etl_time,
							w3.max_etl_time,
							crpm.panel as panel_in_log,
							crpm.reference_year_set as reference_year_set_in_log
					from w3
					inner join sdesign.cm_refyearset2panel_mapping as crpm on w3.refyearset2panel_mapping = crpm.id
					)
			,w5 as	(-- list of panels and reference year sets that are in configuration for given target variable
					 -- with information about ldsity_threshold and last_change
					select tad.* from target_data.t_available_datasets as tad
					where categorization_setup in
						(
						select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
						where ldsity2target_variable in
							(
							select id from target_data.cm_ldsity2target_variable
							where target_variable = (
													select tetv.target_variable
													from target_data.t_etl_target_variable as tetv
													where tetv.id = _id_etl_target_variable
													)
							)
						)
					)
			,w6 as	(
					select w5.*, w4.* from w5 left join w4
					on w5.panel = w4.panel_in_log and w5.reference_year_set = w4.reference_year_set_in_log
					)
			,w7 as	(-- list of panels and reference year sets that were not ETL yet
					select crpm.id as refyearset2panel_mapping
					from sdesign.cm_refyearset2panel_mapping as crpm
					inner join
								(
								select distinct w6.panel, w6.reference_year_set from w6
								where w6.id_etl_log is null
								) as t
					on crpm.panel = t.panel and crpm.reference_year_set = t.reference_year_set
					)			
			,w8 as	(-- list of panels and reference year sets that were ETL but exists newer local densities
					select distinct w6.refyearset2panel_mapping_in_log as refyearset2panel_mapping
					from w6 where w6.id_etl_log is not null and w6.last_change > w6.max_etl_time
					order by w6.refyearset2panel_mapping_in_log
					)
			select
				array_agg(a.refyearset2panel_mapping order by a.refyearset2panel_mapping) as refyearset2panel_mapping
			from
				(
				select w7.refyearset2panel_mapping from w7 union all
				select w8.refyearset2panel_mapping from w8
				) as a
			into
				_refyearset2panel_mapping;

			if _refyearset2panel_mapping is null
			then
				return query select
									_id_etl_target_variable as etl_target_variable,
									true as check_target_variable,
									null::integer[] as refyearset2panel_mapping;
			else
				return query select
									_id_etl_target_variable as etl_target_variable,
									false as check_target_variable,
									_refyearset2panel_mapping as refyearset2panel_mapping;
			end if;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_check_target_variable(integer, integer) is
'Function checks that target variable and all theirs current datas was ETL (true) or not (false).';

grant execute on function target_data.fn_etl_check_target_variable(integer, integer) to public;
-- </function>



-- <function name="fn_trg_check_t_available_datasets" schema="target_data" src="functions/fn_trg_check_t_available_datasets.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_t_available_datasets
---------------------------------------------------------------------------------------------------

-- drop function target_data.fn_trg_check_t_available_datasets();

create or replace function target_data.fn_trg_check_t_available_datasets()
returns trigger as 
$$
declare
	_target_variable		integer;
	_categorization_setups	integer[];
	_check					integer;
begin
	if (TG_OP = 'INSERT') or (TG_OP = 'UPDATE')
	then
		select distinct target_variable from target_data.cm_ldsity2target_variable where id in
		(select ldsity2target_variable from target_data.cm_ldsity2target2categorization_setup
		where categorization_setup = NEW.categorization_setup)
		into _target_variable;

		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = _target_variable)
				)
		select array_agg(w1.categorization_setup) from w1
		into _categorization_setups;

		with
		w1 as	(
				select distinct ldsity_threshold, last_change from target_data.t_available_datasets
				where panel = NEW.panel
				and reference_year_set = NEW.reference_year_set
				and categorization_setup in (select unnest(_categorization_setups))
				)
		select count(w1.*) from w1
		into _check;

		if _check > 1
		then
			raise exception 'Error: 01: fn_trg_check_t_available_datasets: For combination of panel = %, reference_year_set = % and target_variable = % not exists the equal value ldsity_threshold or not exists the equal value last_change in t_available_datasets table!',NEW.panel, NEW.reference_year_set, _target_variable;
		end if;
	end if;

	return new;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_t_available_datasets() IS
'This trigger function controls that for combination of panel, reference_year_set and target_variable exists the equal value ldsity_threshold and exists the equal value last_change in t_available_datasets table';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_t_available_datasets() TO public;
-- </function>

---------------------------------------------------------------------------------------------------

alter table target_data.t_available_datasets disable trigger trg__available_datasets__del;
alter table target_data.t_available_datasets disable trigger trg__available_datasets__ins;
alter table target_data.t_available_datasets disable trigger trg__available_datasets__upd;

---------------------------------------------------------------------------------------------------
-- t_available_datasets => update column ldsity_threshold and last_change
---------------------------------------------------------------------------------------------------
with
w1 as	(
		select * from target_data.t_available_datasets
		)
,w2 as	(
		select distinct available_datasets, creation_time
		from target_data.t_ldsity_values
		where is_latest = true
		)
,w3 as	(
		select w2.available_datasets, max(w2.creation_time) as max_creation_time
		from w2 group by w2.available_datasets
		)
,w4 as	(
		select w2.* from w2 inner join w3
		on w2.available_datasets = w3.available_datasets
		and w2.creation_time = w3.max_creation_time
		)
,w5 as	(
		select w1.*, w4.creation_time from w1
		left join w4 on w1.id = w4.available_datasets
		)
,w6 as	(
		select
				t1.*,
				cltv.target_variable
		from
			(
			select * from target_data.cm_ldsity2target2categorization_setup as cltcs
			where cltcs.categorization_setup in (select w5.categorization_setup from w5)
			) as t1
			inner join target_data.cm_ldsity2target_variable as cltv
			on t1.ldsity2target_variable = cltv.id
		)
,w7 as	(
		select distinct w6.categorization_setup, w6.target_variable from w6
		)
,w8 as	(
		select w5.*, w7.target_variable from w5 inner join w7
		on w5.categorization_setup = w7.categorization_setup
		)
,w9 as	(
		select w8.* from w8 where w8.creation_time is not null
		)		
,w10 as	(
		select
				w9.panel,
				w9.reference_year_set,
				w9.target_variable,
				max(w9.creation_time) as max_creation_time
		from
				w9
		group
				by w9.panel, w9.reference_year_set, w9.target_variable
		)
,w11 as	(
		select
				w8.*,
				case
					when w10.max_creation_time is null
					then '2022-01-01 00:00:00'::timestamptz
					else w10.max_creation_time
				end as last_change4update
		from
				w8
				left join w10
				on w8.panel = w10.panel
				and w8.reference_year_set = w10.reference_year_set
				and w8.target_variable = w10.target_variable
		)
update target_data.t_available_datasets
set
	ldsity_threshold = 0.0000000001,
	last_change = w11.last_change4update
from
	w11 where w11.id = t_available_datasets.id;

alter table target_data.t_available_datasets enable trigger trg__available_datasets__del;
alter table target_data.t_available_datasets enable trigger trg__available_datasets__ins;
alter table target_data.t_available_datasets enable trigger trg__available_datasets__upd;

---------------------------------------------------------------------------------------------------
-- t_available_datasets => add trigger
---------------------------------------------------------------------------------------------------
create constraint trigger trg_t_available_datasets_after_update
after update
on target_data.t_available_datasets
deferrable initially deferred for each row
execute function target_data.fn_trg_check_t_available_datasets();

create constraint trigger trg_t_available_datasets_after_insert
after insert
on target_data.t_available_datasets
deferrable initially deferred for each row
execute function target_data.fn_trg_check_t_available_datasets();

---------------------------------------------------------------------------------------------------

alter table target_data.t_available_datasets alter column ldsity_threshold set not null;
alter table target_data.t_available_datasets alter column last_change set not null;
---------------------------------------------------------------------------------------------------



-- <function name="fn_etl_export_ldsity_values" schema="target_data" src="functions/etl/fn_etl_export_ldsity_values.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_ldsity_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_ldsity_values(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_ldsity_values
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_available_datasets			integer[];
		_categorization_setups		integer[];
		_res_available_datasets		json;
		_res_ldsity_values			json;
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_ldsity_values: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_ldsity_values: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;			


		-------------------------------------------------------------------------------------------
		-- LDSITY VALUES => get available data sets and categorization setups FROM t_ldsity_values
		-------------------------------------------------------------------------------------------
		with
		w1 as	(-- all categorization setups for input target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups reduced by combinations of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(-- list of IDs of available datasets from t_ldsity_values table
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		select array_agg(w3.available_datasets order by w3.available_datasets) from w3
		into _available_datasets;
		
		with
		w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select unnest(_available_datasets))
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_etl_export_ldsity_values: Internal argument _categorization_setups must not by NULL!';
		end if;
		-------------------------------------------------------------------------------------------


		-------------------------------------------------------------
		-- AVAILABLE DATASETS => get all available data sets
		-------------------------------------------------------------
		with
		w1 as	(-- all categorization setups for input target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups reduced by combinations of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(
				select
						w2.id as id_available_datasets,
						w2.panel,
						w2.reference_year_set,
						w2.ldsity_threshold,
						cmltv.id as id_cm_ldsity2target2categorization_setup,
						cmltv.ldsity2target_variable,
						cmltv.adc2classification_rule,
						cmltv.spc2classification_rule,
						cmltv.categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', cmltv.spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', cmltv.spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', cmltv.adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', cmltv.adc2classification_rule)).id_category as id_adc_orig				
				from
						w2
						inner join target_data.cm_ldsity2target2categorization_setup as cmltv
						on w2.categorization_setup = cmltv.categorization_setup
				)
		,w4 as	(
				select
						w3.*,
						cltv.ldsity_object_type as core_or_division
				from
						w3
						inner join target_data.cm_ldsity2target_variable as cltv on w3.ldsity2target_variable = cltv.id
				)
		,w5 as	(				
				select
						w4.*,
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w4a.id_spt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w4a.id_spc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w4a.id_adt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w4a.id_adc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adc
				from
						w4
				)		
		,w6 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w5
				)
		,w7 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from w6 as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(t.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(t.id_adt,tead.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						case when tespc.etl_id is null then 0 else tespc.etl_id end as etl_id_spc,
						case when teadc.etl_id is null then 0 else teadc.etl_id end as etl_id_adc
				from w7
				
				left join target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc,tespc.sub_population_category)
				
				left join target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc,teadc.area_domain_category)
				)
		,w9 as	(
				select distinct t.t_panel__id, t.t_panel__panel, t.t_stratum__id, t.t_cluster_configuration__id, t.t_cluster_configuration__cluster_configuration,
				t.t_stratum__stratum, t.t_strata_set__id, t.t_strata_set__strata_set, t.c_country__id, t.c_country__label,
				t.t_reference_year_set__id, t.t_reference_year_set__reference_year_set, t.t_inventory_campaign__id, t.t_inventory_campaign__inventory
				from
						(
						select		
								t_panel.id as t_panel__id,
								t_panel.panel as t_panel__panel,
								t_cluster.id as t_cluster__id,
								t_cluster.cluster as t_cluster__cluster,
								f_p_plot.gid as f_p_plot__gid,
								f_p_plot.plot as f_p_plot__plot,
								t_cluster_configuration.id as t_cluster_configuration__id,
								t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
								t_stratum.id as t_stratum__id,
								t_stratum.stratum as t_stratum__stratum,
								t_strata_set.id as t_strata_set__id,
								t_strata_set.strata_set as t_strata_set__strata_set,
								c_country.id as c_country__id,
								c_country.label as c_country__label,
								t_reference_year_set.id as t_reference_year_set__id,
								t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
								t_inventory_campaign.id as t_inventory_campaign__id,
								t_inventory_campaign.inventory as t_inventory_campaign__inventory
						from
									sdesign.cm_refyearset2panel_mapping
						inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
						inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
						inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
						inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
						inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
						inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
						inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
						inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
						inner join	sdesign.c_country on t_strata_set.country = c_country.id
						inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
						inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
						where
								cm_refyearset2panel_mapping.id in (select unnest(_refyearset2panel_mapping))
						) as t
				)
		,w10 as	(
				select
						_etl_id as etl_id_tv,
						w9.c_country__label as country,
						w9.t_strata_set__strata_set as strata_set,
						w9.t_stratum__stratum as stratum,
						w9.t_panel__panel as panel,
						w9.t_reference_year_set__reference_year_set as reference_year_set,
						w9.t_cluster_configuration__cluster_configuration as cluster_configuration,
						w9.t_inventory_campaign__inventory as inventory_campaign,
						w8.etl_id_spc,
						w8.etl_id_adc,
						w5.ldsity_threshold
				from
						w5
				inner join	w8 	on w5.categorization_setup = w8.categorization_setup
				inner join	w9	on w5.panel = w9.t_panel__id
								and w5.reference_year_set = w9.t_reference_year_set__id
				)				
		,w11 as	(
				select
						w10.etl_id_tv,
						w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster_configuration,
						w10.inventory_campaign,
						w10.etl_id_spc,
						w10.etl_id_adc,
						w10.ldsity_threshold
				from
						w10
				order
				by		w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster_configuration,
						w10.inventory_campaign,
						w10.etl_id_tv,
						w10.etl_id_spc,
						w10.etl_id_adc 
				)
		select
			json_agg(json_build_object(
				'target_variable',   		w11.etl_id_tv,
				'country',					w11.country,
				'strata_set',				w11.strata_set,
				'stratum',					w11.stratum,
				'reference_year_set',		w11.reference_year_set,
				'panel',					w11.panel,
				'cluster_configuration',	w11.cluster_configuration,
				'inventory_campaign',		w11.inventory_campaign,
				'sub_population_category',	w11.etl_id_spc,
				'area_domain_category',		w11.etl_id_adc,
				'ldsity_threshold',			w11.ldsity_threshold
				))
		from
				w11 into _res_available_datasets;
		-------------------------------------------------------------


		-------------------------------------------------------------
		-- LDSITY VALUES => get ldsity values
		-------------------------------------------------------------
		with
		w1 as	(
				select
						tad.id as id_available_datasets,
						tad.panel,
						tad.reference_year_set,
						tad.categorization_setup
				from
						target_data.t_available_datasets as tad
				where
						tad.id in (select unnest(_available_datasets))
				)
		,w2 as	(
				select t1.*, w1.*
				from target_data.t_ldsity_values as t1
				inner join w1 on t1.available_datasets = w1.id_available_datasets
				where t1.is_latest = true
				)
		,w3 as	(
				select
						id as id_cm_ldsity2target2categorization_setup,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig				
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest(_categorization_setups))
				)
		,w4 as	(
				select
						w3.*,
						cltv.ldsity_object_type as core_or_division
				from
						w3
						inner join target_data.cm_ldsity2target_variable as cltv on w3.ldsity2target_variable = cltv.id
				)
		,w5 as	(				
				select
						w4.*,
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w4a.id_spt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w4a.id_spc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w4a.id_adt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w4a.id_adc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adc
				from
						w4
				)
		,w6 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w5
				)
		,w7 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from w6 as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(t.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(t.id_adt,tead.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						case when tespc.etl_id is null then 0 else tespc.etl_id end as etl_id_spc,
						case when teadc.etl_id is null then 0 else teadc.etl_id end as etl_id_adc
				from w7
				
				left join target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc,tespc.sub_population_category)
				
				left join target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc,teadc.area_domain_category)
				)
		,w9 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory as t_inventory_campaign__inventory
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_refyearset2panel_mapping))
				)
		,w10 as	(
				select
						_etl_id as etl_id_tv,
						w9.c_country__label as country,
						w9.t_strata_set__strata_set as strata_set,
						w9.t_stratum__stratum as stratum,
						w9.t_panel__panel as panel,
						w9.t_reference_year_set__reference_year_set as reference_year_set,
						w9.t_cluster__cluster as cluster,
						w9.t_cluster_configuration__cluster_configuration as cluster_configuration,
						w9.t_inventory_campaign__inventory as inventory_campaign,
						w9.f_p_plot__plot as plot,
						w8.etl_id_spc,
						w8.etl_id_adc,
						w2.value
				from
						w2
				inner join	w8 	on w2.categorization_setup = w8.categorization_setup
				inner join	w9	on w2.plot = w9.f_p_plot__gid
								and w2.panel = w9.t_panel__id
								and w2.reference_year_set = w9.t_reference_year_set__id
				)
		,w11 as	(
				select
						w10.etl_id_tv,
						w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster,
						w10.cluster_configuration,
						w10.inventory_campaign,						
						w10.plot,
						w10.etl_id_spc,
						w10.etl_id_adc,
						w10.value
				from
						w10
				order
				by		w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster,
						w10.cluster_configuration,
						w10.inventory_campaign,
						w10.etl_id_tv,
						w10.plot,
						w10.etl_id_spc,
						w10.etl_id_adc 
				)
		select
			json_agg(json_build_object(
				'target_variable',   		w11.etl_id_tv,
				'country',					w11.country,
				'strata_set',				w11.strata_set,
				'stratum',					w11.stratum,
				'reference_year_set',		w11.reference_year_set,
				'panel',					w11.panel,
				'cluster',					w11.cluster,
				'cluster_configuration',	w11.cluster_configuration,
				'inventory_campaign',		w11.inventory_campaign,
				'plot',						w11.plot,
				'sub_population_category',	w11.etl_id_spc,
				'area_domain_category',		w11.etl_id_adc,
				'value',					w11.value))
		from
				w11 into _res_ldsity_values;
		-------------------------------------------------------------


		-------------------------------------------------------------
		-- JOIN elements
		-------------------------------------------------------------
		select json_build_object
			(
				'available_datasets',_res_available_datasets,
				'ldsity_values',_res_ldsity_values
			)
		into _res;
		-------------------------------------------------------------
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_ldsity_values(integer[], integer) is
'Function returns records for ETL t_target_data and t_available_datasets table.';

grant execute on function target_data.fn_etl_export_ldsity_values(integer[], integer) to public;
-- </function>



DROP FUNCTION IF EXISTS target_data.fn_save_ldsity_values(integer[], integer, double precision) CASCADE;



-- <function name="fn_save_ldsity_values" schema="target_data" src="functions/fn_save_ldsity_values.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_save_ldsity_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_ldsity_values(integer[], integer, double precision) CASCADE;
	
create or replace function target_data.fn_save_ldsity_values
(
	_refyearset2panel_mapping	integer[],
	_target_variable			integer,
	_threshold					double precision default 0.0000000001
)
returns text
as
$$
declare
		_max_categorization_setup				integer;
		_max_refyearset2panel_mapping			integer;
		_refyearset2panel_mapping_add_zero		integer[];
		_categorization_setup_add_zero			integer[];
		_refyearset2panel_mapping_without_zero	integer[];
		_categorization_setup_without_zero		integer[];
		_res_i									text;
		_res									text;
		_result									text;
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_save_ldsity_values: The input argument _refyearset2panel_mapping must not be NULL !';
		end if;
	
		if _target_variable is null
		then
			raise exception 'Error 02: fn_save_ldsity_values: The input argument _target_variable must not be NULL !';
		end if;

		if not exists (select t.* from target_data.c_target_variable as t where t.id = _target_variable)
			then raise exception 'Error: 03: fn_save_ldsity_values: Given target variable (%) does not exist in c_target_variable table.', _target_variable;
		END IF;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		with
		w1 as	(
				select * from target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable)
				)
		,w2 as	(
				select
						w1.categorization_setup,
						unnest(w1.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select distinct w2.categorization_setup, w2.refyearset2panel_mapping from w2
				)
		,w4 as	(
				select
						w3.refyearset2panel_mapping,
						array_agg(w3.categorization_setup order by w3.categorization_setup) as categorization_setup
				from
						w3 group by w3.refyearset2panel_mapping
				)
		,w5 as	(
				select * from w4 where w4.refyearset2panel_mapping in (select unnest(_refyearset2panel_mapping))
				)
		select max(array_length(w5.categorization_setup,1)) from w5
		into _max_categorization_setup;
		-----------------------------------------------------------------------
		if _max_categorization_setup is null
		then
			raise exception 'Error 04: fn_save_ldsity_values: For input argument _target_variable not exist any categorization setup!';
		end if;
		-----------------------------------------------------------------------
		with
		w1 as	(
				select * from target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable)
				)
		,w2 as	(
				select
						w1.categorization_setup,
						unnest(w1.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select distinct w2.categorization_setup, w2.refyearset2panel_mapping from w2
				)
		,w4 as	(
				select
						w3.refyearset2panel_mapping,
						array_agg(w3.categorization_setup order by w3.categorization_setup) as categorization_setup
				from
						w3 group by w3.refyearset2panel_mapping order by w3.refyearset2panel_mapping
				)
		,w5 as	(
				select * from w4 where w4.refyearset2panel_mapping in (select unnest(_refyearset2panel_mapping))
				)				
		,w6 as	(
				select
						w5.*,
						------------
						case
							when array_length(w5.categorization_setup,1) = _max_categorization_setup
							then w5.categorization_setup
							else
								w5.categorization_setup ||
								(select array_agg(t.categorization_setup) from (select 0 as categorization_setup, generate_series(1, (_max_categorization_setup - array_length(w5.categorization_setup,1)))) as t)
						end
							as categorization_setup_add_zero
				from
							w5
				)
		,w7 as	(
				select
						w6.categorization_setup_add_zero,
						array_agg(w6.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w6 group by w6.categorization_setup_add_zero
				)
		select max(array_length(w7.refyearset2panel_mapping,1)) from w7
		into _max_refyearset2panel_mapping;
		-----------------------------------------------------------------------
		if _max_refyearset2panel_mapping is null
		then
			raise exception 'Error 05: fn_save_ldsity_values: For input argument _target_variable not exist any panel refyearset combination!';
		end if;
		-----------------------------------------------------------------------
		with
		w1 as	(
				select * from target_data.fn_get_eligible_panel_refyearset_combinations(_target_variable)
				)
		,w2 as	(
				select
						w1.categorization_setup,
						unnest(w1.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select distinct w2.categorization_setup, w2.refyearset2panel_mapping from w2
				)
		,w4 as	(
				select
						w3.refyearset2panel_mapping,
						array_agg(w3.categorization_setup order by w3.categorization_setup) as categorization_setup
				from
						w3 group by w3.refyearset2panel_mapping order by w3.refyearset2panel_mapping
				)
		,w5 as	(
				select * from w4 where w4.refyearset2panel_mapping in (select unnest(_refyearset2panel_mapping))
				)				
		,w6 as	(
				select
						w5.*,
						------------
						case
							when array_length(w5.categorization_setup,1) = _max_categorization_setup
							then w5.categorization_setup
							else
								w5.categorization_setup ||
								(select array_agg(t.categorization_setup) from (select 0 as categorization_setup, generate_series(1, (_max_categorization_setup - array_length(w5.categorization_setup,1)))) as t)
						end
							as categorization_setup_add_zero
				from
							w5
				)
		,w7 as	(
				select
						w6.categorization_setup_add_zero,
						array_agg(w6.refyearset2panel_mapping) as refyearset2panel_mapping
				from
						w6 group by w6.categorization_setup_add_zero
				)	
		,w8 as	(
				select
						w7.*,
						case
							when array_length(w7.refyearset2panel_mapping,1) = _max_refyearset2panel_mapping
							then w7.refyearset2panel_mapping
							else
								w7.refyearset2panel_mapping ||
								(select array_agg(t.refyearset2panel_mapping) from (select 0 as refyearset2panel_mapping, generate_series(1, (_max_refyearset2panel_mapping - array_length(w7.refyearset2panel_mapping,1)))) as t)
						end
							as refyearset2panel_mapping_add_zero
				from
							w7
				)
		select
				array_agg(w8.refyearset2panel_mapping_add_zero order by w8.refyearset2panel_mapping) as refyearset2panel_mapping_add_zero,
				array_agg(w8.categorization_setup_add_zero order by w8.refyearset2panel_mapping) as categorization_setup_add_zero
		from
				w8
		into
				_refyearset2panel_mapping_add_zero,
				_categorization_setup_add_zero;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		for i in 1..array_length(_refyearset2panel_mapping_add_zero,1)
		loop
			-- check that values in array _refyearset2panel_mapping_add_zero[i:i] must not be only zeroes !!
			if	(
				select count(t.res) = 0 from
				(select unnest(_refyearset2panel_mapping_add_zero[i:i]) as res) as t
				where t.res is distinct from 0
				)
			then
				raise exception 'Error 06: fn_save_ldsity_values: Array of panel refyearset combinations is an empty array!';
			end if;

			-- check that values in array _categorization_setup_add_zero[i:i] must not be only zeroes !!
			if	(
				select count(t.res) = 0 from
				(select unnest(_categorization_setup_add_zero[i:i]) as res) as t
				where t.res is distinct from 0
				)
			then
				raise exception 'Error 07: fn_save_ldsity_values: Array of categorization setups is an empty array!';
			end if;

			-- remove zeroes from array _refyearset2panel_mapping_add_zero
			with
			w1 as	(
					select unnest(_refyearset2panel_mapping_add_zero[i:i]) as refyearset2panel_mapping
					)
			select
					array_agg(w1.refyearset2panel_mapping)
			from
					w1 where w1.refyearset2panel_mapping is distinct from 0
			into
				_refyearset2panel_mapping_without_zero;

			-- remove zeroes from array _categorization_setup_add_zero
			with
			w1 as	(
					select unnest(_categorization_setup_add_zero[i:i]) as categorization_setup
					)
			select
					array_agg(w1.categorization_setup)
			from
					w1 where w1.categorization_setup is distinct from 0
			into
				_categorization_setup_without_zero;

			--raise notice '_refyearset2panel_mapping_without_zero: %',_refyearset2panel_mapping_without_zero;
			--raise notice '_categorization_setup_without_zero: %',_categorization_setup_without_zero;

			_res_i := target_data.fn_save_ldsity_values_internal
						(
							_refyearset2panel_mapping_without_zero,
							_categorization_setup_without_zero,
							_threshold
						);

			if i = 1
			then
				_res := _res_i;
			else
				_res := concat(_res,' ',_res_i);
			end if;

		end loop;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		raise notice '%',_res;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		_result := 'The ldsity values were prepared successfully.'::text;

		return _result;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_save_ldsity_values(integer[], integer, double precision) IS
'The function internally calls function fn_save_ldsity_values_internal.';

grant execute on function target_data.fn_save_ldsity_values(integer[], integer, double precision) to public;
-- </function>
