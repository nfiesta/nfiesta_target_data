--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


-- <function name="fn_get_classification_rules4categorization_setups" schema="target_data" src="functions/fn_get_classification_rules4categorization_setups.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rules4categorization_setups
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rules4categorization_setups(integer[], integer) CASCADE;

create or replace function target_data.fn_get_classification_rules4categorization_setups
(
	_categorization_setups	integer[],
	_ldsity2target_variable	integer
)
returns text
as
$$
declare
		_ldsity									integer;
		_ldsity_object_type						integer;
		_target_variable						integer;
		_ldsity_object							integer;
		_ldsity_objects							integer[];
		_ldsity2target_variable_object_type_100	integer;
		_string4case_i							text;
		_string4case							text;
begin
		if _categorization_setups is null
		then
			raise exception 'Error 01: fn_get_classification_rules4categorization_setups: Input argument _categorization_setups must not by null !';
		end if;
	
		if _ldsity2target_variable is null
		then
			raise exception 'Error 02: fn_get_classification_rules4categorization_setups: Input argument _ldsity2target_variable must not by null !';
		end if;
	
		select
				ldsity,
				ldsity_object_type,
				target_variable
		from
				target_data.cm_ldsity2target_variable
		where
				id = _ldsity2target_variable
		into
				_ldsity,
				_ldsity_object_type,
				_target_variable;
	
		select ldsity_object from target_data.c_ldsity 
		where id = _ldsity
		into _ldsity_object;
	
		if _ldsity_object is null
		then
			raise exception 'Error 03: fn_get_classification_rules4categorization_setups: For input argument _ldsity2target_variable = % not found any ldsity_object in table c_ldsity!',_ldsity2target_variable;
		end if;
	
		_ldsity_objects := target_data.fn_get_ldsity_objects(array[_ldsity_object]);
	
		if _ldsity_object_type = 200
		then
			select id from target_data.cm_ldsity2target_variable
			where target_variable = _target_variable
			and ldsity_object_type = 100
			into _ldsity2target_variable_object_type_100;
		else
			_ldsity2target_variable_object_type_100 := null::integer;
		end if;
	
		-- ordering values in argument _categorization_setups
		with
		w as	(
				select distinct t.res from (select unnest(_categorization_setups) as res) as t
				)
		select array_agg(w.res order by w.res) from w
		into _categorization_setups;
				
		if _ldsity_object_type = 200
		then
			with
			w as	(
					select
							a.categorization_setup,
							a.adc2classification_rule,
							b.spc2classification_rule
					from
					(
					select categorization_setup, adc2classification_rule
					from target_data.cm_ldsity2target2categorization_setup
					where categorization_setup = any(_categorization_setups)
					and	ldsity2target_variable = _ldsity2target_variable_object_type_100
					) as a
					inner join
					(
					select categorization_setup, spc2classification_rule
					from target_data.cm_ldsity2target2categorization_setup
					where categorization_setup = any(_categorization_setups)
					and	ldsity2target_variable = _ldsity2target_variable
					) as b
					on a.categorization_setup = b.categorization_setup
					)
			,w1 as	(
					select
						w.categorization_setup,
						case
							when adc2classification_rule is null and spc2classification_rule is null
							then 'when area_domain_category = array[0] and sub_population_category = array[0] then true'
							
							when adc2classification_rule is not null and spc2classification_rule is null
							then concat('when area_domain_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('adc',adc2classification_rule)),',',''),
										'] and sub_population_category = array[0] then case when '
										,target_data.fn_get_classification_rule4classification_rule_id('adc',adc2classification_rule,_ldsity_objects),
										' then true else false end'
										)
										
							when adc2classification_rule is null and spc2classification_rule is not null
							then concat('when area_domain_category = array[0] and sub_population_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('spc',spc2classification_rule)),',',''),
										'] then case when '
										,target_data.fn_get_classification_rule4classification_rule_id('spc',spc2classification_rule,_ldsity_objects),
										' then true else false end'
										)
							
							when adc2classification_rule is not null and spc2classification_rule is not null
							then concat('when area_domain_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('adc',adc2classification_rule)),',',''),
										'] and sub_population_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('spc',spc2classification_rule)),',',''),
										'] then case when ('
										,target_data.fn_get_classification_rule4classification_rule_id('adc',adc2classification_rule,_ldsity_objects),
										' and '
										,target_data.fn_get_classification_rule4classification_rule_id('spc',spc2classification_rule,_ldsity_objects),
										') then true else false end'
										)		
						end as string4case												
					from
							w
					)
			,w2 as	(
					select
							w1.categorization_setup,
							concat('#DELETE#',w1.string4case) as string4case
					from
							w1
					)
			,w3 as	(
					select
							array_agg(w2.categorization_setup order by w2.categorization_setup) as categorization_setup,
							array_agg(w2.string4case order by w2.categorization_setup) as string4case
					from
							w2
					)
			select
					replace(replace(replace(concat(w3.string4case),'{"#DELETE#',' '),'","#DELETE#',' '),'"}',' ') as string4case
			from
					w3
			into
					_string4case;
		else
			with
			w1 as	(					
					select
						categorization_setup,
						case
							when adc2classification_rule is null and spc2classification_rule is null
							then 'when area_domain_category = array[0] and sub_population_category = array[0] then true'
							
							when adc2classification_rule is not null and spc2classification_rule is null
							then concat('when area_domain_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('adc',adc2classification_rule)),',',''),
										'] and sub_population_category = array[0] then case when '
										,target_data.fn_get_classification_rule4classification_rule_id('adc',adc2classification_rule,_ldsity_objects),
										' then true else false end'
										)
										
							when adc2classification_rule is null and spc2classification_rule is not null
							then concat('when area_domain_category = array[0] and sub_population_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('spc',spc2classification_rule)),',',''),
										'] then case when '
										,target_data.fn_get_classification_rule4classification_rule_id('spc',spc2classification_rule,_ldsity_objects),
										' then true else false end'
										)
							
							when adc2classification_rule is not null and spc2classification_rule is not null
							then concat('when area_domain_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('adc',adc2classification_rule)),',',''),
										'] and sub_population_category = array[',
										array_to_string((select id_category from target_data.fn_get_category4classification_rule_id('spc',spc2classification_rule)),',',''),
										'] then case when ('
										,target_data.fn_get_classification_rule4classification_rule_id('adc',adc2classification_rule,_ldsity_objects),
										' and '
										,target_data.fn_get_classification_rule4classification_rule_id('spc',spc2classification_rule,_ldsity_objects),
										') then true else false end'
										)		
						end as string4case												
					from
							target_data.cm_ldsity2target2categorization_setup
					where
							categorization_setup = any(_categorization_setups)
					and
							ldsity2target_variable = _ldsity2target_variable
					)
			,w2 as	(
					select
							w1.categorization_setup,
							concat('#DELETE#',w1.string4case) as string4case
					from
							w1
					)
			,w3 as	(
					select
							array_agg(w2.categorization_setup order by w2.categorization_setup) as categorization_setup,
							array_agg(w2.string4case order by w2.categorization_setup) as string4case
					from
							w2
					)
			select
					replace(replace(replace(concat(w3.string4case),'{"#DELETE#',' '),'","#DELETE#',' '),'"}',' ') as string4case
			from
					w3
			into
					_string4case;
		end if;
			
		_string4case := concat('case ',_string4case,' end');

		return _string4case;	
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_classification_rules4categorization_setups(integer[], integer) is
'The function gets text of classification rules for input categorization setups.';

grant execute on function target_data.fn_get_classification_rules4categorization_setups(integer[], integer) to public;
-- </function>



-- <function name="fn_get_classification_rule4classification_rule_id" schema="target_data" src="functions/fn_get_classification_rule4classification_rule_id.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_classification_rule4classification_rule_id
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) CASCADE;

create or replace function target_data.fn_get_classification_rule4classification_rule_id
(
	_classification_rule_type	character varying,
	_classification_rule_id		integer[],
	_ldsity_objects				integer[]
)	
returns text
as
$$
declare
		_table_name								text;
		_classification_rule_i					text;	
		_ldsity_object_i						integer;
		_pozice_i								integer;
		_column_name_i_array					text[];
		_classification_rule_i_pozice			text;
		_classification_rule_4case_pozice		text;
		_category								integer[];
begin
		if _classification_rule_type is null
		then
			raise exception 'Error 01: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_type must not by NULL !';
		end if;
	
		if not(_classification_rule_type = any(array['adc','spc']))
		then
			raise exception 'Error 02: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_type must be "adc" or "spc" !';
		end if;	
	
		if _classification_rule_id is null
		then
			raise exception 'Error 03: fn_get_classification_rule4classification_rule_id: Input argument _classification_rule_id must not by NULL !';
		end if;
	
		if _ldsity_objects is null
		then
			raise exception 'Error 04: fn_get_classification_rule4classification_rule_id: Input argument _ldsity_objects must not by NULL !';
		end if;	
	
		if _classification_rule_type = 'adc' then _table_name := 'cm_adc2classification_rule'; end if;
		if _classification_rule_type = 'spc' then _table_name := 'cm_spc2classification_rule'; end if;	
	
		for i in 1..array_length(_classification_rule_id,1)
		loop
				execute '
				select
						case when classification_rule is null then ''true'' else classification_rule end as classification_rule,
						ldsity_object
				from
						target_data.'||_table_name||'
				where
						id = $1'
				using	_classification_rule_id[i]
				into	_classification_rule_i,
						_ldsity_object_i;

				-----------------------------------------------------
				if 	_classification_rule_i in ('EXISTS','NOT EXISTS')
				then
					_classification_rule_i := 'TRUE';
					_pozice_i := 1;
				else
					_classification_rule_i := _classification_rule_i;
					_pozice_i := array_position(_ldsity_objects,_ldsity_object_i);
				end if;
				-----------------------------------------------------						
										
				/*
				select array_agg(column_name::text order by ordinal_position) from information_schema.columns
			 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_object_i)
			 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_object_i)
				into _column_name_i_array;
				*/

				with
				w1 as	(
						select column_name::text from information_schema.columns
			 			where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_object_i)
			 			and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_object_i)
			 			)
			 	select array_agg(w1.column_name) from w1
			 	where position(w1.column_name in _classification_rule_i) > 0
				into _column_name_i_array;

				if _column_name_i_array is not null
				then
					for ii in 1..array_length(_column_name_i_array,1)
					loop					
						_classification_rule_i := regexp_replace(_classification_rule_i,concat('(\+|\*|\-|\/| |\A|\(|\)|\=|\t)(',_column_name_i_array[ii],')(\+|\*|\-|\/| |\Z|\(|\)|\=|\:)'),concat('\1\2','_',_pozice_i,'\3'),'g'); 
					end loop;
				end if;							
								
				_classification_rule_i := concat('(',_classification_rule_i,')');
		
				if i = 1
				then
					_classification_rule_4case_pozice := _classification_rule_i;
				else
					_classification_rule_4case_pozice := concat(_classification_rule_4case_pozice,' and ',_classification_rule_i);
				end if;		
						
		end loop;
	
		_classification_rule_4case_pozice := concat('(',_classification_rule_4case_pozice,')');

		return _classification_rule_4case_pozice;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) is
'The function gets text of classification rules for their classification rule identifiers.';

grant execute on function target_data.fn_get_classification_rule4classification_rule_id(character varying, integer[], integer[]) to public;
-- </function>



-- <function name="fn_get_case_ad4categorization_setups" schema="target_data" src="functions/fn_get_case_ad4categorization_setups.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_case_ad4categorization_setups
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_get_case_ad4categorization_setups(integer[]) CASCADE;

create or replace function target_data.fn_get_case_ad4categorization_setups
(
	_categorization_setups	integer[]
)
returns text
as
$$
declare
		_id_ad_array			integer[];
		_string4case_i			text;
		_string4case			text;
begin
		if _categorization_setups is null
		then
			raise exception 'Error 01: fn_get_case_ad4categorization_setups: Input argument _categorization_setups must not by null !';
		end if;
		
		select array_agg(id order by id) from target_data.t_available_datasets
		where categorization_setup in (select unnest(_categorization_setups))
		into _id_ad_array;
	
		if _id_ad_array is null
		then
			raise exception 'Error 02: fn_get_case_ad4categorization_setups: For values in input argument _categorization_setup not found any record in table t_available_datasets !';
		end if;	

		with
		w1 as	(
				select
						id,
						concat('when (t1.categorization_setup = ',categorization_setup,' and t1.panel = ',panel,' and t1.reference_year_set = ',reference_year_set,') then ',id) as string4case
				from
						target_data.t_available_datasets
				where
						id = any(_id_ad_array)
				)
		,w2 as	(
				select
						w1.id,
						concat('#DELETE#',w1.string4case) as string4case
				from
						w1
				)
		,w3 as	(
				select
						array_agg(w2.id order by w2.id) as id,
						array_agg(w2.string4case order by w2.id) as string4case
				from
						w2
				)
		select
				replace(replace(replace(concat(w3.string4case),'{"#DELETE#',' '),'","#DELETE#',' '),'"}',' ') as string4case
		from
				w3		
		into
				_string4case;
				
		/*
		for i in 1..array_length(_id_ad_array,1)
		loop
			select
					concat('when (t1.categorization_setup = ',categorization_setup,' and t1.panel = ',panel,' and t1.reference_year_set = ',reference_year_set,') then ',id)
			from
					target_data.t_available_datasets where id = _id_ad_array[i]
			into
					_string4case_i;
			
			if i = 1
			then
					_string4case := _string4case_i;
			else
					_string4case := concat(_string4case,' ',_string4case_i);
			end if;
		
		end loop;
		*/

		_string4case := concat('case ',_string4case,' end');

		return _string4case;	
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_get_case_ad4categorization_setups(integer[]) is
'The function gets string for transforming available dataset ids for input values of categorization setups.';

grant execute on function target_data.fn_get_case_ad4categorization_setups(integer[]) to public;
-- </function>

