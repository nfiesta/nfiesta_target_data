--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="fn_trg_check_c_sub_population" schema="target_data" src="functions/fn_trg_check_c_sub_population.sql">
--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_c_sub_population
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_c_sub_population();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_c_sub_population() 
RETURNS TRIGGER AS 
$$
DECLARE
	_old_sp	integer;
	_new_sp	integer;
BEGIN
		IF	(
			SELECT count(*) FROM target_data.c_sub_population_category
			WHERE sub_population = NEW.id
			) = 0
		THEN
			RAISE EXCEPTION 'For newly inserted sub population (sub_population = %), not exists any record in table c_sub_population_category!', NEW.id;
		END IF;

		-- if the population has EXISTS/NOT EXISTS rules
		-- only one population of this kind can exist

		WITH w_old AS (
			-- find populations which already existed before new one
			SELECT t2.sub_population, t1.ldsity_object, count(t1.*) AS total
			FROM target_data.cm_spc2classification_rule AS t1
			INNER JOIN target_data.c_sub_population_category AS t2
			ON t1.sub_population_category = t2.id
			WHERE 	t2.sub_population != NEW.id AND 
				(t1.classification_rule = 'EXISTS' OR 
				t1.classification_rule = 'NOT EXISTS')
			GROUP BY t2.sub_population, t1.ldsity_object
		)
		SELECT DISTINCT sub_population
		FROM w_old
		WHERE total = 2
		INTO _old_sp;

		WITH w_new AS (
			SELECT t2.sub_population, t1.ldsity_object, count(t1.*) AS total
			FROM target_data.cm_spc2classification_rule AS t1
			INNER JOIN target_data.c_sub_population_category AS t2
			ON t1.sub_population_category = t2.id
			WHERE 	t2.sub_population = NEW.id AND 
				(t1.classification_rule = 'EXISTS' OR 
				t1.classification_rule = 'NOT EXISTS')
			GROUP BY t2.sub_population, t1.ldsity_object
		)
		SELECT DISTINCT sub_population
		FROM w_new
		WHERE total = 2
		INTO _new_sp;

		IF _old_sp IS NOT NULL AND _new_sp IS NOT NULL
		THEN
			RAISE EXCEPTION 'There is already one sub_population (id=%) with EXISTS/NOT EXISTS rules.',_old_sp;
		END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_c_sub_population() IS
'This trigger function checks whether for newly inserted record in c_sub_population table exists at least one record in c_sub_population_category table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_c_sub_population() TO public;


-- </function>

-- <function name="fn_trg_check_cm_spc2classification_rule" schema="target_data" src="functions/fn_trg_check_cm_spc2classification_rule.sql">
--
-- Copyright 2017, 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_spc2classification_rule
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_spc2classification_rule();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_spc2classification_rule() 
RETURNS TRIGGER AS 
$$
DECLARE
	_check_ids integer[];
BEGIN
		IF NOT(new.classification_rule = any(array['EXISTS','NOT EXISTS']))
		THEN
			IF	(
				SELECT count(*) FROM target_data.cm_spc2classrule2panel_refyearset
				WHERE spc2classification_rule = new.id
				) = 0
			THEN
				RAISE EXCEPTION 'Error: 01: fn_trg_check_cm_spc2classification_rule: For new inserted classification rule: [sub_population_category = %, ldsity_object = %, classification_rule = "%", use_negative = %], not exists any record in table cm_adc2classrule2panel_refyearset!', new.sub_population_category, new.ldsity_object, new.classification_rule, new.use_negative;
			END IF;
		END IF;
		
		-- if the population has EXISTS/NOT EXISTS rules
		-- then only these two rules can be there (per object)
		WITH w_old AS (
			-- find populations which already existed before new one
			SELECT 	t2.sub_population, t1.ldsity_object, 
				CASE WHEN t1.classification_rule = 'EXISTS' THEN 1
				WHEN t1.classification_rule = 'NOT EXISTS' THEN 2
				ELSE 3
				END AS typ,
				count(t1.*) AS total
			FROM target_data.cm_spc2classification_rule AS t1
			INNER JOIN target_data.c_sub_population_category AS t2
			ON t1.sub_population_category = t2.id
			INNER JOIN
				(SELECT sub_population
				FROM target_data.c_sub_population_category
				WHERE id = NEW.sub_population_category
				) AS t3
			ON 	t2.sub_population = t3.sub_population
			WHERE	t1.ldsity_object = NEW.ldsity_object AND
				(t1.classification_rule = 'EXISTS' OR 
				t1.classification_rule = 'NOT EXISTS')
			GROUP BY t2.sub_population, t1.ldsity_object, typ
		), w_check AS (
			SELECT 1 AS check_id
			FROM w_old
			WHERE total != 1
			UNION ALL
			SELECT 2 AS check_id
			FROM (
				SELECT sum(total) AS check_sum
				FROM w_old
				WHERE typ IN (1,2) AND total = 1) AS t1
			WHERE check_sum != 2
		)
		SELECT array_agg(check_id ORDER BY check_id)
		FROM w_check
		INTO _check_ids;

		IF _check_ids = '{1}'
		THEN
			RAISE EXCEPTION 'There is a duplicate of EXISTS or NOT EXISTS classification rule (trigger was fired for id %).', NEW.id;
		END IF;

		IF _check_ids = '{2}'
		THEN
			RAISE EXCEPTION 'There is a missing EXISTS or NOT EXISTS classification rule. Only one from the pair is present (trigger was fired for id %).', NEW.id;
		END IF;

		IF _check_ids = '{1,2}'
		THEN
			RAISE EXCEPTION 'There is a duplicate of EXISTS or NOT EXISTS classification rule and also one from the pair is missing (trigger was fired for id %)', NEW.id;
		END IF;

	RETURN NEW;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_spc2classification_rule() IS
'This trigger function controls that for new inserted record into cm_spc2classification_rule table exists at least one record in cm_spc2classrule2panel_refyearset table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_spc2classification_rule() TO public;


-- </function>

