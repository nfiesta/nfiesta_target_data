--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

DROP MATERIALIZED VIEW target_data.v_variable_hierarchy CASCADE;

-- <view name="v_variable_hierarchy" schema="target_data" src="views/v_variable_hierarchy.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
--alter extension nfiesta_target_data drop view target_data.v_variable_hierarchy;
--DROP MATERIALIZED VIEW target_data.v_variable_hierarchy;

CREATE MATERIALIZED VIEW target_data.v_variable_hierarchy_internal AS
with w_categorization_setups as (
		select
		t_categorization_setup.id as cs_id,
		c_target_variable.id as tv_id 
		, max(spc_id) as spc_id, max(sp_id) as sp_id, max(adc_id) as adc_id, max(ad_id) as ad_id
		, max(c_target_variable.label) as tv_label, max(spc_label) as spc_label, max(sp_label) as sp_label, max(adc_label) as adc_label, max(ad_label) as ad_label
	from target_data.t_categorization_setup
	inner join target_data.cm_ldsity2target2categorization_setup ON cm_ldsity2target2categorization_setup.categorization_setup = t_categorization_setup.id
	inner join target_data.cm_ldsity2target_variable ON cm_ldsity2target_variable.id = cm_ldsity2target2categorization_setup.ldsity2target_variable
	inner join target_data.c_target_variable ON c_target_variable.id = cm_ldsity2target_variable.target_variable
	, lateral (	select 
					array_agg(c_sub_population_category.label order by spc2clr.n) as spc_label,
					array_agg(c_sub_population.label order by spc2clr.n) as sp_label,
					array_agg(c_sub_population_category.id order by spc2clr.n) as spc_id,
					array_agg(c_sub_population.id order by spc2clr.n) as sp_id
				from unnest(cm_ldsity2target2categorization_setup.spc2classification_rule) with ordinality as spc2clr(spc2classification_rule, n)
				inner join target_data.cm_spc2classification_rule on (spc2clr.spc2classification_rule = cm_spc2classification_rule.id)
				inner join target_data.c_sub_population_category ON c_sub_population_category.id = cm_spc2classification_rule.sub_population_category
				inner join target_data.c_sub_population ON c_sub_population.id = c_sub_population_category.sub_population
			  ) as spc
	, lateral (	select 
					array_agg(c_area_domain_category.label order by adc2clr.n) as adc_label,
					array_agg(c_area_domain.label order by adc2clr.n) as ad_label,
					array_agg(c_area_domain_category.id order by adc2clr.n) as adc_id,
					array_agg(c_area_domain.id order by adc2clr.n) as ad_id
				from unnest(cm_ldsity2target2categorization_setup.adc2classification_rule) with ordinality as adc2clr(adc2classification_rule, n)
				inner join target_data.cm_adc2classification_rule on (adc2clr.adc2classification_rule = cm_adc2classification_rule.id)
				inner join target_data.c_area_domain_category ON c_area_domain_category.id = cm_adc2classification_rule.area_domain_category
				inner join target_data.c_area_domain ON c_area_domain.id = c_area_domain_category.area_domain
			  ) as adc
	group by 	t_categorization_setup.id, c_target_variable.id
	order by t_categorization_setup.id
)
, w_sup_inf_pairs as (
	select 
		cs_sup.cs_id as cs_id_sup,
		cs_inf.cs_id as cs_id_inf
		, cs_inf.sp_label as sp_label_inf, cs_inf.ad_label as ad_label_inf
		, cs_sup.tv_label as tv_label_sup, cs_sup.spc_label as spc_label_sup, cs_sup.adc_label as adc_label_sup
		, cs_inf.tv_label as tv_label_inf, cs_inf.spc_label as spc_label_inf, cs_inf.adc_label as adc_label_inf
	from w_categorization_setups 		as cs_sup
	inner join w_categorization_setups 	as cs_inf on (cs_sup.tv_id = cs_inf.tv_id) and
			(
				((cs_sup.adc_id = cs_inf.adc_id OR cs_sup.adc_id IS NULL AND cs_inf.adc_id IS NULL) AND
					((cs_sup.spc_id != cs_inf.spc_id AND cs_sup.spc_id <@ cs_inf.spc_id AND (array_length(cs_inf.spc_id,1) = array_length(cs_sup.spc_id,1) + 1)) OR
					 (cs_sup.spc_id IS NULL AND cs_inf.spc_id IS NOT NULL AND array_length(cs_inf.spc_id,1) = 1)))
				OR
				((cs_sup.spc_id = cs_inf.spc_id OR cs_sup.spc_id IS NULL AND cs_inf.spc_id IS NULL) AND
					((cs_sup.adc_id != cs_inf.adc_id AND cs_sup.adc_id <@ cs_inf.adc_id AND (array_length(cs_inf.adc_id,1) = array_length(cs_sup.adc_id,1) + 1)) OR
					 (cs_sup.adc_id IS NULL AND cs_inf.adc_id IS NOT NULL AND array_length(cs_inf.adc_id,1) = 1))))
	order by cs_sup.cs_id, cs_inf.cs_id
)
select 
	cs_id_sup, spc_label_sup, adc_label_sup
	, cs_id_inf, sp_label_inf, ad_label_inf
	, array_to_string(spc_label_inf, '~') AS spc_label_inf
	, array_to_string(adc_label_inf, '~') AS adc_label_inf
from w_sup_inf_pairs
order by cs_id_sup
;

ALTER TABLE target_data.v_variable_hierarchy_internal OWNER TO app_nfiesta;
GRANT SELECT ON TABLE 				target_data.v_variable_hierarchy_internal TO PUBLIC;
GRANT SELECT ON TABLE				target_data.v_variable_hierarchy_internal TO app_nfiesta;
GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE	target_data.v_variable_hierarchy_internal TO app_nfiesta_mng;

---------------------------------------

--DROP VIEW target_data.v_variable_hierarchy;
CREATE VIEW target_data.v_variable_hierarchy AS
	with w_data as (
		select
			tad_sup.panel,
			tad_sup.reference_year_set,
			cs_id_sup, spc_label_sup, adc_label_sup,
			cs_id_inf, sp_label_inf, ad_label_inf, spc_label_inf, adc_label_inf
		from target_data.v_variable_hierarchy_internal
		inner join target_data.t_available_datasets as tad_sup on (tad_sup.categorization_setup = v_variable_hierarchy_internal.cs_id_sup)
		inner join target_data.t_available_datasets as tad_inf on (tad_inf.categorization_setup = v_variable_hierarchy_internal.cs_id_inf)
		where ((tad_sup.panel = tad_inf.panel) and (tad_sup.reference_year_set = tad_inf.reference_year_set))
	)
	select
		panel,
		reference_year_set,
		cs_id_sup as node, 
		array_agg(cs_id_inf 		order by cs_id_inf) AS edges,
		spc_label_sup, adc_label_sup,
		array_agg(spc_label_inf 	order by cs_id_inf) AS spc_label_inf,
		array_agg(adc_label_inf		order by cs_id_inf) AS adc_label_inf
	from w_data
	group by panel, reference_year_set, cs_id_sup, spc_label_sup, adc_label_sup, sp_label_inf, ad_label_inf
	order by panel, reference_year_set, cs_id_sup
;

ALTER TABLE target_data.v_variable_hierarchy OWNER TO app_nfiesta;
GRANT SELECT ON TABLE 				target_data.v_variable_hierarchy TO PUBLIC;
GRANT SELECT ON TABLE				target_data.v_variable_hierarchy TO app_nfiesta;
GRANT SELECT ON TABLE	 			target_data.v_variable_hierarchy TO app_nfiesta_mng;

--------------------------------------automatic view refresh (https://stackoverflow.com/a/29447328)

CREATE OR REPLACE FUNCTION target_data.tg_refresh_v_variable_hierarchy()
RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    REFRESH MATERIALIZED VIEW /*CONCURRENTLY */target_data.v_variable_hierarchy_internal;
    RETURN NULL;
END;
$$;

--target_data.t_categorization_setup
drop trigger if exists trg__t_categorization_setup__refresh_v_variable_hierarchy ON target_data.t_categorization_setup;
CREATE TRIGGER trg__t_categorization_setup__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.t_categorization_setup
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_ldsity2target2categorization_setup
drop trigger if exists trg__cm_ldsity2target2categorization_setup__refresh_v_variable_hierarchy ON target_data.cm_ldsity2target2categorization_setup;
CREATE TRIGGER trg__cm_ldsity2target2categorization_setup__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_ldsity2target2categorization_setup
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_ldsity2target_variable
drop trigger if exists trg__cm_ldsity2target_variable__refresh_v_variable_hierarchy ON target_data.cm_ldsity2target_variable;
CREATE TRIGGER trg__cm_ldsity2target_variable__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_ldsity2target_variable
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_target_variable
drop trigger if exists trg__c_target_variable__refresh_v_variable_hierarchy ON target_data.c_target_variable;
CREATE TRIGGER trg__c_target_variable__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_target_variable
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_spc2classification_rule
drop trigger if exists trg__cm_spc2classification_rule__refresh_v_variable_hierarchy ON target_data.cm_spc2classification_rule;
CREATE TRIGGER trg__cm_spc2classification_rule__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_spc2classification_rule
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_sub_population_category
drop trigger if exists trg__c_sub_population_category__refresh_v_variable_hierarchy ON target_data.c_sub_population_category;
CREATE TRIGGER trg__c_sub_population_category__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_sub_population_category
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_sub_population
drop trigger if exists trg__c_sub_population__refresh_v_variable_hierarchy ON target_data.c_sub_population;
CREATE TRIGGER trg__c_sub_population__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_sub_population
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.cm_adc2classification_rule
drop trigger if exists trg__cm_adc2classification_rule__refresh_v_variable_hierarchy ON target_data.cm_adc2classification_rule;
CREATE TRIGGER trg__cm_adc2classification_rule__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.cm_adc2classification_rule
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_area_domain_category
drop trigger if exists trg__c_area_domain_category__refresh_v_variable_hierarchy ON target_data.c_area_domain_category;
CREATE TRIGGER trg__c_area_domain_category__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_area_domain_category
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

--target_data.c_area_domain
drop trigger if exists trg__c_area_domain__refresh_v_variable_hierarchy ON target_data.c_area_domain;
CREATE TRIGGER trg__c_area_domain__refresh_v_variable_hierarchy
AFTER INSERT OR UPDATE OR DELETE
ON target_data.c_area_domain
FOR EACH STATEMENT EXECUTE PROCEDURE target_data.tg_refresh_v_variable_hierarchy();

-- </view>

-- <function name="fn_save_ldsity_values_internal" schema="target_data" src="functions/fn_save_ldsity_values_internal.sql">
--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- fn_save_ldsity_values_internal
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_save_ldsity_values_internal(integer[], integer[], double precision) CASCADE;
	
create or replace function target_data.fn_save_ldsity_values_internal
(
	_refyearset2panel_mapping	integer[],
	_categorization_setups		integer[],
	_threshold					double precision
)
returns text
as
$$
declare
		_array_id									integer[];
		_array_target_variable						integer[];
		_target_variable							integer;
		_lot_100									integer;
		_lot_200									integer;
		_variant									integer;
		_tv_target_variable							integer;
		_ldsity										integer;
		_ldsity_object_type							integer;
		_tv_area_domain_category					integer[];
		_tv_sub_population_category					integer[];
		--_tv_definition_variant					integer[];
		_tv_use_negative							boolean;
		_tv_version									integer;
		_state_or_change							integer;
		_ldsity_ldsity_object						integer;
		_ldsity_column_expresion					text;
		_ldsity_unit_of_measure						integer;
		_ldsity_area_domain_category				integer[];
		_ldsity_sub_population_category				integer[];
		_ldsity_definition_variant					integer[];	
		_area_domain_category						integer[];
		_sub_population_category					integer[];
		_definition_variant							integer[];	
		_area_domain_category_i						integer[];
		_area_domain_category_t						text[];	
		_sub_population_category_i					integer[];
		_sub_population_category_t					text[];
		_condition_ids								integer[];
		_condition_types							text[];
		_example_query_1							text;
		_example_query								text;
		_ldsity_objects								integer[];
		_filter_i									text;
		_filters_array								text[];	
		_example_query_i							text;
		_example_query_array						text[];
		_query_res									text;
		_column_name_i_array						text[];	
		_ii_column_new_text							text;
		_pozice_i									integer;
		_classification_rule_i						text;	
		_pozice_array								integer[];
		_classification_rule_array					text[];	
		_position_groups							integer[];
		_classification_rule_upr_ita_pozice			text[];
		_classification_rule_ita_pozice				text;
		_ldsity_objects_without_conditions			integer[];
		_string4columns								text;
		_string4inner_joins							text;	
		_position4join								integer;
		_table_name4join							text;
		_pkey_column4join							text;
		_i_column4join								text;
		_string4attr								text;
		_string4case								text;
		_position4ldsity							integer;
		_string4ads									text;
		_query_res_big								text;
		_string_var									text;	
		_string_result_1							text;	
		_id_cat_setups_array						integer[];
		_check_bez_rozliseni						integer[];
		_check_bez_rozliseni_boolean				boolean[];	
		_string_var_100								text;
		_array_id_200								integer[];	
		_string_var_200								text;
		_string_check_insert						text;	
		_max_id_tlv									integer;
		_res										text;
		_ext_version_text							text;
		_case4available_datasets_text				text;
		_categorization_setups_adc					integer[];
		_categorization_setups_spc					integer[];
		_ldsity_column_expresion_modified			text;
	
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_save_ldsity_values_internal: The input argument _refyearset2panel_mapping must not be NULL !';
		end if;
		
		if _categorization_setups is null
		then
			raise exception 'Error 02: fn_save_ldsity_values_internal: The input argument _categorization_setups must not be NULL !';
		end if;
	
		if	(
			select count(t.res) > 0 from
			(select unnest(_categorization_setups) as res) as t
			where t.res is null
			)
		then
			raise exception 'Error 03: fn_save_ldsity_values_internal: The input argument _categorization_setups must not contains NULL value!';
		end if;
	
		/*
		-------------------------------------------------------------
		-------------------------------------------------------------
		with
		w as	(
				select distinct categorization_setup
				from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(	
					select id from target_data.cm_ldsity2target_variable
					where target_variable =
								(
								select distinct target_variable from target_data.cm_ldsity2target_variable
								where id in	(
											select ldsity2target_variable
											from target_data.cm_ldsity2target2categorization_setup
											where categorization_setup in (select unnest(_categorization_setups))
											)
								)
					)
				and (
					adc2classification_rule is null
					or array_length(adc2classification_rule,1) <= 
						(select max(t.res) from
						(select array_length(adc2classification_rule,1) as res
						from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						and adc2classification_rule is not null) as t)
					)
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups_adc;

		with
		w as	(
				select distinct categorization_setup
				from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(	
					select id from target_data.cm_ldsity2target_variable
					where target_variable =
								(
								select distinct target_variable from target_data.cm_ldsity2target_variable
								where id in	(
											select ldsity2target_variable
											from target_data.cm_ldsity2target2categorization_setup
											where categorization_setup in (select unnest(_categorization_setups))
											)
								)
					)
				and (
					spc2classification_rule is null
					or array_length(spc2classification_rule,1) <= 
						(select max(t.res) from
						(select array_length(spc2classification_rule,1) as res
						from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						and spc2classification_rule is not null) as t)
					)
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups_spc;

		with
		w as	(
				select a.categorization_setup from
					(select unnest(_categorization_setups_adc) as categorization_setup) as a
				inner join
					(select unnest(_categorization_setups_spc) as categorization_setup) as b
				on a.categorization_setup = b.categorization_setup
				)
		select array_agg(categorization_setup order by categorization_setup)
		from w into _categorization_setups;
		-------------------------------------------------------------
		-------------------------------------------------------------
		*/

		select
				array_agg(id order by ldsity_object_type, id) as id,
				array_agg(target_variable order by ldsity_object_type, id) as target_variable
		from
				target_data.cm_ldsity2target_variable
		where
				id in	(
						select ldsity2target_variable from target_data.cm_ldsity2target2categorization_setup
						where categorization_setup in (select unnest(_categorization_setups))
						)
		into
				_array_id,
				_array_target_variable;
		
		if _array_id is null
		then
			raise exception 'Error 04: fn_save_ldsity_values_internal: For input values in argument _categorization_setups = % not found any record in table cm_ldsity2target_variable!',_categorization_setups;
		end if;
		
		_target_variable := (select distinct t.res from (select unnest(_array_target_variable) as res) as t);
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 100
		into _lot_100;
	
		select count(ldsity_object_type) from target_data.cm_ldsity2target_variable
		where target_variable = _target_variable and ldsity_object_type = 200
		into _lot_200;
	
		if _lot_100 = 1 and _lot_200 = 0 then _variant = 1; end if;
		if _lot_100 > 1 and _lot_200 = 0 then _variant = 2; end if;
		if _lot_100 = 1 and _lot_200 = 1 then _variant = 3; end if;
		if _lot_100 = 1 and _lot_200 > 1 then _variant = 4; end if;
		if _lot_100 > 1 and _lot_200 > 1
		then
			raise exception 'Error 05: fn_save_ldsity_values_internal: This variant is not implemented yet!';
		end if;
	
		---------------------
		---------------------
		for bc in 1..array_length(_array_id,1)
		loop
			-- datas from table cm_ldsity2target_variable
			select
					target_variable,
					ldsity,
					ldsity_object_type,
					area_domain_category,
					sub_population_category,
					--definition_variant,
					use_negative,
					version
			from
					target_data.cm_ldsity2target_variable
			where
					id = _array_id[bc]
			into
					_tv_target_variable,
					_ldsity,
					_ldsity_object_type,
					_tv_area_domain_category,
					_tv_sub_population_category,
					--_tv_definition_variant,
					_tv_use_negative,
					_tv_version;
				
			-- datas from c_target_variable	
			select state_or_change from target_data.c_target_variable where id = _tv_target_variable
			into _state_or_change;
		
			-----------------------------------------------------------
			-- INTERMEDIATE STEP => insert values into table t_available_datasets
			-----------------------------------------------------------
			/*
			with
			w1 as	(
					select distinct reference_year_set, panel
					from target_data.fn_get_plots(_panels,_reference_year_sets,_state_or_change,_tv_use_negative)
					order by reference_year_set, panel
					)
			,w2 as	(select unnest(array[_categorization_setups]) as categorization_setup)
			,w3 as	(select w1.*, w2.* from w1, w2)
			insert into target_data.t_available_datasets(panel, reference_year_set, categorization_setup)
			select panel, reference_year_set, categorization_setup from w3
			except
			select panel, reference_year_set, categorization_setup from target_data.t_available_datasets
			where categorization_setup in (select unnest(array[_categorization_setups]))
			order by categorization_setup, panel, reference_year_set;
			*/

			with
			w1 as	(
					select distinct reference_year_set, panel
					from target_data.fn_get_plots(_refyearset2panel_mapping,_state_or_change,_tv_use_negative)
					order by reference_year_set, panel
					)
			,w2 as	(select unnest(array[_categorization_setups]) as categorization_setup)
			,w3 as	(select w1.*, w2.* from w1, w2)
			insert into target_data.t_available_datasets(panel, reference_year_set, categorization_setup)
			select panel, reference_year_set, categorization_setup from w3
			except
			select panel, reference_year_set, categorization_setup from target_data.t_available_datasets
			where categorization_setup in (select unnest(array[_categorization_setups]))
			order by categorization_setup, panel, reference_year_set;			
			-----------------------------------------------------------
			-----------------------------------------------------------
	
			-- datas from table c_ldsity
			select
					ldsity_object,
					column_expression,
					unit_of_measure,
					area_domain_category,
					sub_population_category,
					definition_variant
			from
					target_data.c_ldsity where id = _ldsity
			into
					_ldsity_ldsity_object,
					_ldsity_column_expresion,
					_ldsity_unit_of_measure,
					_ldsity_area_domain_category,
					_ldsity_sub_population_category,
					_ldsity_definition_variant;
							 
			_area_domain_category := _ldsity_area_domain_category || _tv_area_domain_category;
			_sub_population_category := _ldsity_sub_population_category || _tv_sub_population_category;
			_definition_variant := _ldsity_definition_variant; --|| _tv_definition_variant;

			-----------------------------------------
			if _area_domain_category is not null
			then
				for i in 1..array_length(_area_domain_category,1)
				loop
					if i = 1
					then
						_area_domain_category_i := array[_area_domain_category[i]];
						_area_domain_category_t := array['ad'];
					else
						_area_domain_category_i := _area_domain_category_i || array[_area_domain_category[i]];
						_area_domain_category_t := _area_domain_category_t || array['ad'];
					end if;
				end loop;
			else
				_area_domain_category_i := null::integer[];
				_area_domain_category_t := null::text[];
			end if;
			-----------------------------------------
			-----------------------------------------
			if _sub_population_category is not null
			then
				for i in 1..array_length(_sub_population_category,1)
				loop
					if i = 1
					then
						_sub_population_category_i := array[_sub_population_category[i]];
						_sub_population_category_t := array['sp'];
					else
						_sub_population_category_i := _sub_population_category_i || array[_sub_population_category[i]];
						_sub_population_category_t := _sub_population_category_t || array['sp'];
					end if;
				end loop;
			else
				_sub_population_category_i := null::integer[];
				_sub_population_category_t := null::text[];
			end if;
			-----------------------------------------
			-----------------------------------------
	
			_condition_ids := _area_domain_category_i || _sub_population_category_i;
			_condition_types := _area_domain_category_t || _sub_population_category_t;
	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if _condition_ids is null
			then
				--_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_panels,'],array[',_reference_year_sets,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');  -- pkey pro testy nahradit sloupcem plot
				_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_refyearset2panel_mapping,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');  -- pkey pro testy nahradit sloupcem plot
				_example_query := concat('w_&i& as (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter#)');
			else
				--_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_panels,'],array[',_reference_year_sets,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');
				_example_query_1 := replace(replace(concat('w_1 as (select a1.*, a2.* from (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#) as a1 inner join ','(select * from target_data.fn_get_plots(array[',_refyearset2panel_mapping,'],',_state_or_change,',',case when _tv_use_negative = true then 'true' else 'false' end,')) as a2 on a1.plot_1 = a2.gid and a1.reference_year_set_1 = a2.reference_year_set4join)'),'{',''),'}','');
				_example_query := concat('w_&i& as (select #column_names_&i&# from #table_name#_&i& where version = ',_tv_version,' and #con4filter# and #conditions_&i&#)');
			end if;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_ldsity_objects := target_data.fn_get_ldsity_objects(array[_ldsity_ldsity_object]);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					select case when filter is null then 'true' else filter end
					from target_data.c_ldsity_object
					where id = _ldsity_objects[i]
					into _filter_i;
				
					if i = 1
					then
						_filters_array := array[_filter_i];
					else
						_filters_array := _filters_array || array[_filter_i];
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					if _ldsity_objects[i] = 100
					then
						_example_query_i := _example_query_1;
					else
						_example_query_i := _example_query;
					end if;
				
					_example_query_i := replace(_example_query_i,'&i&'::text,i::text);
					_example_query_i := replace(_example_query_i,'#con4filter#',(select case when filter is null then 'true' else filter end from target_data.c_ldsity_object where id = _ldsity_objects[i]));
					_example_query_i := replace(_example_query_i,concat('#table_name#_',i),(select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]));				
				
					if i = 1
					then
						_example_query_array := array[_example_query_i];
					else
						_example_query_array := _example_query_array || _example_query_i;
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
					if i = 1
					then
						_query_res := concat('with ',_example_query_array[i]);
					else
						_query_res := _query_res || concat(',',_example_query_array[i]);
					end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
				select array_agg(column_name order by ordinal_position) from information_schema.columns
			 	where table_schema = (select substring(table_name from 1 for (position('.' in table_name) - 1)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
			 	and table_name = (select substring(table_name from (position('.' in table_name) + 1) for length(table_name)) from target_data.c_ldsity_object where id = _ldsity_objects[i])
				into _column_name_i_array;
				
				for ii in 1..array_length(_column_name_i_array,1)
				loop
					if ii = 1
					then
						_ii_column_new_text := concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i);
					else
						_ii_column_new_text := concat(_ii_column_new_text,', ',concat(_column_name_i_array[ii],' as ',_column_name_i_array[ii],'_',i));
					end if;
				end loop;
			
				_query_res := replace(_query_res,concat('#column_names_',i::text,'#'),_ii_column_new_text);
				
			end loop;	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if _condition_ids is not null
			then
				for i in 1..array_length(_condition_ids,1)
				loop
					if _condition_types[i] = 'ad'
					then			
						select array_position(_ldsity_objects,
						(select ldsity_object from target_data.cm_adc2classification_rule where id = _condition_ids[i]))
						into _pozice_i;
					
						if _pozice_i is null then raise exception 'Error 06: fn_save_ldsity_values_internal: For internal variable _condition_ids[i] = % not found position in internal variable _ldsity_objects = % !',_condition_ids[i],_ldsity_objects; end if;
									
						select case when classification_rule is null then 'true' else classification_rule end
						from target_data.cm_adc2classification_rule where id = _condition_ids[i]
						into _classification_rule_i;
					
						if _classification_rule_i is null then raise exception 'Error 07: fn_save_ldsity_values_internal: The classification rule in the cm_adc2classification_rule table was not found for the internal variable _condition_ids[i] = %!,',_condition_ids[i]; end if;
					
						if _classification_rule_i = any(array['EXISTS','NOT EXISTS'])
						then
							raise exception 'Error 08: fn_save_ldsity_values_internal: The ADC classification rule = % is not allowed as a reducing condition!',_classification_rule_i;
						end if;
					end if;
				
					if _condition_types[i] = 'sp'
					then			
						select array_position(_ldsity_objects,
						(select ldsity_object from target_data.cm_spc2classification_rule where id = _condition_ids[i]))
						into _pozice_i;				
					
						if _pozice_i is null then raise exception 'Error 09: fn_save_ldsity_values_internal: For internal variable _condition_ids[i] = % not found position in internal variable _ldsity_objects = % !',_condition_ids[i],_ldsity_objects; end if;
								
						select case when classification_rule is null then 'true' else classification_rule end
						from target_data.cm_spc2classification_rule where id = _condition_ids[i]
						into _classification_rule_i;
					
						if _classification_rule_i is null then raise exception 'Error 10: fn_save_ldsity_values_internal: The classification rule in the cm_spc2classification_rule table was not found for the internal variable _condition_ids[i] = %!,',_condition_ids[i]; end if;				
					
						if _classification_rule_i = any(array['EXISTS','NOT EXISTS'])
						then
							raise exception 'Error 11: fn_save_ldsity_values_internal: The SPC classification rule = % is not allowed as a reducing condition!',_classification_rule_i;
						end if;					
					end if;
				
					if i = 1
					then
						_pozice_array := array[_pozice_i];
						_classification_rule_array := array[_classification_rule_i];
					else
						_pozice_array := _pozice_array || array[_pozice_i];
						_classification_rule_array := _classification_rule_array || array[_classification_rule_i];
					end if;
					
				end loop;
				
				with
				w1 as	(select unnest(_pozice_array) as pozice)
				,w2 as 	(select distinct pozice from w1)
				select array_agg(pozice order by pozice) from w2
				into _position_groups;
			
				for i in 1..array_length(_position_groups,1)
				loop
					with
					w1 as	(
							select
							unnest(_pozice_array) as pozice,
							unnest(_classification_rule_array) as classification_rule
							)
					,w2 as	(
							select
									row_number() over () as new_id,
									pozice,
									classification_rule
							from w1
							)
					,w3 as	(
							select
									row_number() over (partition by pozice order by new_id) as new_id4order,
									w2.*
							from w2
							)
					select array_agg(classification_rule order by new_id4order) as classification_rule
					from w3 where pozice = _position_groups[i]
					into _classification_rule_upr_ita_pozice;
				
					for ii in 1..array_length(_classification_rule_upr_ita_pozice,1)
					loop
						if ii = 1
						then
							_classification_rule_ita_pozice := concat(_classification_rule_upr_ita_pozice[ii]);
						else
							_classification_rule_ita_pozice := concat(_classification_rule_ita_pozice,' and ',_classification_rule_upr_ita_pozice[ii]);
						end if;
					end loop;
				
					_query_res := replace(_query_res,concat('#conditions_',_position_groups[i],'#'),concat('(',_classification_rule_ita_pozice,')'));
				
				end loop;
			
				with
				w1 as	(select * from generate_series(1,array_length(_ldsity_objects,1)) as res)
				,w2 as	(select unnest(_position_groups) as res)
				,w3 as	(select res from w1 except select res from w2)
				select array_agg(res order by res) from w3
				into _ldsity_objects_without_conditions;
				
				if _ldsity_objects_without_conditions is not null
				then
					for i in 1..array_length(_ldsity_objects_without_conditions,1)
					loop
						_query_res := replace(_query_res,concat('#conditions_',_ldsity_objects_without_conditions[i],'#'),'(true)');
					end loop;
				end if;
						
			end if;	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			for i in 1..array_length(_ldsity_objects,1)
			loop
				if i = 1
				then
					_string4columns := concat(',w_res as (select w_',i,'.*');
					_string4inner_joins := concat(' from w_',i);
				else
					_string4columns := concat(_string4columns,', w_',i,'.*');
				
					select array_position(_ldsity_objects, 
						(select upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]))
					into _position4join;
					
					for i in 1..array_length(_ldsity_objects,1)
					loop
							if i = _position4join
							then
								_table_name4join := (select table_name from target_data.c_ldsity_object where id = _ldsity_objects[i]);
							end if;
					end loop;
				
					with
					w as	(
							select kcu.table_schema,
							       kcu.table_name,
							       tco.constraint_name,
							       kcu.ordinal_position as position,
							       kcu.column_name as key_column
							from information_schema.table_constraints tco
							join information_schema.key_column_usage kcu 
							     on kcu.constraint_name = tco.constraint_name
							     and kcu.constraint_schema = tco.constraint_schema
							     and kcu.constraint_name = tco.constraint_name
							where tco.constraint_type = 'PRIMARY KEY'
							order by kcu.table_schema,
							         kcu.table_name,
							         position
							 )
					select key_column::text from w
					where table_schema = (substring(_table_name4join from 1 for (position('.' in _table_name4join) - 1))) 
					and table_name = (substring(_table_name4join from (position('.' in _table_name4join) + 1) for length(_table_name4join)))
					into _pkey_column4join;
					
					select column4upper_object from target_data.c_ldsity_object where id = _ldsity_objects[i]
					into _i_column4join;
				
					_string4inner_joins :=
					concat(_string4inner_joins,' inner join w_',i,' on ','w_',_position4join,'.',_pkey_column4join,'_',_position4join,' = ','w_',i,'.',_i_column4join,'_',i);
							
				end if;
			end loop;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------		
			_query_res := _query_res || _string4columns || _string4inner_joins || ')';	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if	(_variant in (3,4) and _ldsity_ldsity_object is distinct from (select id from target_data.c_ldsity_object where upper_object is null))
			then
				_string4attr := concat('
				select
					case
						when ws100.adc2classification_rule is null
								then array[0]
								else (select id_category from target_data.fn_get_category4classification_rule_id(''adc'',ws100.adc2classification_rule))
					end as area_domain_category,
					case
						when ws200.spc2classification_rule is null
								then array[0]
								else (select id_category from target_data.fn_get_category4classification_rule_id(''spc'',ws200.spc2classification_rule))
					end as sub_population_category,
					ws100.adc2classification_rule,
					ws200.spc2classification_rule,
					ws200.categorization_setup
				from			
					(		
					select
							adc2classification_rule,
							spc2classification_rule,
							categorization_setup
					from
							target_data.cm_ldsity2target2categorization_setup
					where
							categorization_setup in
													(
													select unnest($1)
													except
													select t.categorization_setup from
													(select a.*, b.* from
													(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
													from target_data.cm_ldsity2target2categorization_setup
													where categorization_setup in (select unnest($1))
													and ldsity2target_variable = ',_array_id[bc],') as a
													inner join target_data.cm_spc2classification_rule as b
													on a.id_cm_spc = b.id) as t
													where t.classification_rule = ''NOT EXISTS''	
													)
					and
							ldsity2target_variable = ',_array_id[bc],'
					) as ws200
				inner join 
					(					
					select adc2classification_rule, categorization_setup from target_data.cm_ldsity2target2categorization_setup
					where ldsity2target_variable = ',_array_id[1],'
					) as ws100
				on
					ws200.categorization_setup = ws100.categorization_setup	
				');
			else			
				_string4attr := concat('
				select
						case
							when adc2classification_rule is null
									then array[0]
									else (select id_category from target_data.fn_get_category4classification_rule_id(''adc'',adc2classification_rule))
						end as area_domain_category,
						case
							when spc2classification_rule is null
									then array[0]
									else (select id_category from target_data.fn_get_category4classification_rule_id(''spc'',spc2classification_rule))
						end as sub_population_category,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest($1))
				and
						ldsity2target_variable = ',_array_id[bc],'
				');
			end if;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			-- dopleni radku bez rozliseni pokud schazi, radek je potreba u varianty 3 a 4 !!!
			if _variant in (3,4)
			then	
				for i in 1..array_length(_categorization_setups,1)
				loop
					select array_agg(id order by id)
					from target_data.cm_ldsity2target2categorization_setup
					where categorization_setup = _categorization_setups[i]
					into _id_cat_setups_array;
				
					for ii in 1..array_length(_id_cat_setups_array,1)
					loop
						if	(
							select count(*) = 1
							from target_data.cm_ldsity2target2categorization_setup
							where id = _id_cat_setups_array[ii]
							and (adc2classification_rule is null and spc2classification_rule is null)
							)
						then
							if ii = 1
							then
								_check_bez_rozliseni := array[1];
							else
								_check_bez_rozliseni := _check_bez_rozliseni || array[1];
							end if;
						else
							if ii = 1
							then
								_check_bez_rozliseni := array[0];
							else
								_check_bez_rozliseni := _check_bez_rozliseni || array[0];
							end if;
						end if;
					end loop;
				
					if		(
							select sum(t.res) = array_length(_id_cat_setups_array,1)
							from (select unnest(_check_bez_rozliseni) as res) as t
							)
					then
						if i = 1
						then
							_check_bez_rozliseni_boolean := array[true];
						else
							_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[true];
						end if;
					else
						if i = 1
						then
							_check_bez_rozliseni_boolean := array[false];
						else
							_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[false];
						end if;
					end if;	
				end loop;
			
				if	(
					select count(t.res) = 0
					from (select unnest(_check_bez_rozliseni_boolean) as res) as t
					where t.res = true
					)
				then
					with
					w1 as	(
							select * from target_data.cm_ldsity2target2categorization_setup
							where ldsity2target_variable in	(
															select distinct ldsity2target_variable
															from target_data.cm_ldsity2target2categorization_setup
															where categorization_setup in (select unnest(_categorization_setups))
															)
							and (adc2classification_rule is null and spc2classification_rule is null)
							)
					,w2 as	(
							select categorization_setup, count(categorization_setup) as pocet
							from w1 group by categorization_setup
							)
					select array[categorization_setup] || _categorization_setups
					from w2 where pocet = (select max(pocet) from w2)
					into _categorization_setups;
				
				else
					_categorization_setups := _categorization_setups;
				end if;
			end if;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_string4case := target_data.fn_get_classification_rules4categorization_setups(_categorization_setups,_array_id[bc]);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_position4ldsity := array_position(_ldsity_objects,_ldsity_ldsity_object);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_ldsity_column_expresion_modified := target_data.fn_get_modified_ldsity_column_expression(_ldsity_column_expresion,_position4ldsity,_ldsity_ldsity_object);
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_string4ads := concat('
			,w_strings as	('||_string4attr||')
			,w_pre_adc as	(-- tady je cross join => zde dojde ke kazdemu zaznamu z w_res k rozjoinovani na vsechny kategorie, ktere jsou ve w_strings
							select
									w_strings.*,
									w_res.*
							from
									w_res, w_strings
							)
			,w_ads as		(
							select
									',_string4case,' as res_case,
									w_pre_adc.*
							from
									w_pre_adc
							)
			select
					res_case,
					area_domain_category,
					sub_population_category,
					adc2classification_rule,
					spc2classification_rule,
					(',case when _tv_use_negative = true then '(-1)' else '(1)' end,' * coalesce(',_ldsity_column_expresion_modified,',0.0)) as value,
					gid,
					cluster_configuration,
					reference_year_set4join,
					reference_year_set,
					panel,
					stratum,
					categorization_setup
			from
					w_ads
			');	
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			_query_res := _query_res || _string4ads;
			-----------------------------------------------------------------------
			-----------------------------------------------------------------------
			if bc = 1
			then
				_query_res_big := concat('with bw_',bc,' as (',_query_res,')');
			else
				_query_res_big := concat(_query_res_big,',bw_',bc,' as (',_query_res,')');
			end if;
		
		end loop;		
		-----------------------------------------------------------------------
		-- end BIG CYCLE
		-----------------------------------------------------------------------

		_ext_version_text := (select extversion from pg_extension where extname = 'nfiesta_target_data');

		_case4available_datasets_text := target_data.fn_get_case_ad4categorization_setups(_categorization_setups);
		
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _variant in (1,2)
		then		
				for bc in 1..array_length(_array_id,1)
				loop
					if bc = 1
					then
						_string_var := concat(',w_union_all as (select ',_array_id[bc],' as ldsity2target_variable,* from bw_',bc,' where res_case = true');
					else
						_string_var := concat(_string_var,' union all select ',_array_id[bc],' as ldsity2target_variable,* from bw_',bc,' where res_case = true');
					end if;
				end loop;
			
				_string_var := concat(_string_var,')');
			
				_string_result_1 := concat(
				'
				,w_part_1 as		(
									select
											gid,
											panel,
											reference_year_set,
											categorization_setup,
											sum(value) as value
									from
											w_union_all as t1
									group
											by gid, panel, reference_year_set, categorization_setup
									)
				,w_result_1 as		(
									select
											t1.gid as plot,
											t1.panel,
											t1.reference_year_set,
											t1.categorization_setup,
											t1.value,
											',_case4available_datasets_text,' as available_datasets
									from
											w_part_1 as t1 where t1.value is distinct from 0 -- toto zajisti praci s nenulovymi hodnotama
									)
				');

		_query_res_big := _query_res_big || _string_var || _string_result_1;
	
		end if;

		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		if _variant in (3,4)
		then
			/*		
			for i in 1..array_length(_categorization_setups,1)
			loop
				select array_agg(id order by id)
				from target_data.cm_ldsity2target2categorization_setup
				where categorization_setup = _categorization_setups[i]
				into _id_cat_setups_array;
			
				for ii in 1..array_length(_id_cat_setups_array,1)
				loop
					if	(
						select count(*) = 1
						from target_data.cm_ldsity2target2categorization_setup
						where id = _id_cat_setups_array[ii]
						and (adc2classification_rule is null and spc2classification_rule is null)
						)
					then
						if ii = 1
						then
							_check_bez_rozliseni := array[1];
						else
							_check_bez_rozliseni := _check_bez_rozliseni || array[1];
						end if;
					else
						if ii = 1
						then
							_check_bez_rozliseni := array[0];
						else
							_check_bez_rozliseni := _check_bez_rozliseni || array[0];
						end if;
					end if;
				end loop;
			
				if		(
						select sum(t.res) = array_length(_id_cat_setups_array,1)
						from (select unnest(_check_bez_rozliseni) as res) as t
						)
				then
					if i = 1
					then
						_check_bez_rozliseni_boolean := array[true];
					else
						_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[true];
					end if;
				else
					if i = 1
					then
						_check_bez_rozliseni_boolean := array[false];
					else
						_check_bez_rozliseni_boolean := _check_bez_rozliseni_boolean || array[false];
					end if;
				end if;	
			end loop;
		
			if	(
				select count(t.res) = 0
				from (select unnest(_check_bez_rozliseni_boolean) as res) as t
				where t.res = true
				)
			then
				with
				w1 as	(
						select * from target_data.cm_ldsity2target2categorization_setup
						where ldsity2target_variable in	(
														select distinct ldsity2target_variable
														from target_data.cm_ldsity2target2categorization_setup
														where categorization_setup in (select unnest(_categorization_setups))
														)
						and (adc2classification_rule is null and spc2classification_rule is null)
						)
				,w2 as	(
						select categorization_setup, count(categorization_setup) as pocet
						from w1 group by categorization_setup
						)
				select array[categorization_setup] || _categorization_setups
				from w2 where pocet = (select max(pocet) from w2)
				into _categorization_setups;
			
			else
				_categorization_setups := _categorization_setups;
			end if;
			*/

			_string_var_100 := concat(',w_union_all_100 as (select ',_array_id[1],' as ldsity2target_variable,* from bw_1 where res_case = true)');
		
			_array_id_200 := array_remove(_array_id, _array_id[1]);
				
			for bc_200 in 1..array_length(_array_id_200,1)
			loop
				if bc_200 = 1
				then
					_string_var_200 := concat(',w_union_all_200 as (select ',_array_id_200[bc_200],' as ldsity2target_variable,* from bw_',bc_200 + 1,' where res_case = true');
				else
					_string_var_200 := concat(_string_var_200,' union all select ',_array_id_200[bc_200],' as ldsity2target_variable,* from bw_',bc_200 + 1,' where res_case = true');
				end if;				
			end loop;
		
			_string_var_200 := concat(_string_var_200,')');
			
			_string_result_1 := concat('
			,w_part_1 as		(
								select
										a.gid,
										a.panel,
										a.reference_year_set,
										a.categorization_setup,
										a.available_datasets,
										sum(a.value) as value
								from
										(
										select
												t1.*,
												',_case4available_datasets_text,' as available_datasets
										from
												w_union_all_100 as t1
										where
												value is distinct from 0.0
										) as a
								group
										by a.gid, a.panel, a.reference_year_set, a.categorization_setup, a.available_datasets			
								)
			,w_part_2 as		(			
								select
									t1.gid,
									t1.panel,
									t1.reference_year_set,
									t1.categorization_setup,
									t1.area_domain_category,
									t1.value,
									t2.value_sum,
									case when t2.value_sum = 0.0 then t2.value_sum else t1.value/t2.value_sum end as value4nasobeni,
									',_case4available_datasets_text,' as available_datasets
								from
									(
									select
									gid, panel, reference_year_set, categorization_setup, area_domain_category,
									sum(value) as value
									from w_union_all_200
									where value is distinct from 0.0
									group by gid, panel, reference_year_set, categorization_setup, area_domain_category
									) as t1
								inner join 
									(
									select gid, panel, reference_year_set,
									sum(value) as value_sum
									from w_union_all_200
									where (area_domain_category = array[0] and sub_population_category = array[0])
									and value is distinct from 0.0
									group by gid, panel, reference_year_set
									) as t2
								on
									t1.gid = t2.gid and
									t1.panel = t2.panel and
									t1.reference_year_set = t2.reference_year_set
							)
			,w_result as	(
							select
									t.*,
									coalesce((t.value * coef_200.value4nasobeni),0.0) as value_res
							from 
									w_part_1 as t inner join w_part_2 as coef_200
							on
									t.gid = coef_200.gid and t.available_datasets = coef_200.available_datasets
							)
			,w_not_exists as	(
								/*
								select * from w_part_1
								where gid in (select distinct gid from w_part_1 except select distinct gid from w_part_2)
								and categorization_setup in							
									(
										-- category non-sortable
										select t1.categorization_setup from
										(select a.*, b.* from
										(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
										from target_data.cm_ldsity2target2categorization_setup
										where ldsity2target_variable in (select unnest($2))
										) as a inner join target_data.cm_spc2classification_rule as b
										on a.id_cm_spc = b.id) as t1
										where t1.classification_rule = ''NOT EXISTS''
										union all
										-- category without distinction
										select t2.categorization_setup
										from	(
												select categorization_setup, count(categorization_setup) as pocet
												from target_data.cm_ldsity2target2categorization_setup
												where ldsity2target_variable in (select unnest($2))
												and (adc2classification_rule is null and spc2classification_rule is null)
												group by categorization_setup
												) as t2
										where t2.pocet = array_length($2,1)
									)
								*/

								select * from w_part_1
								where gid in (select distinct gid from w_part_1 except select distinct gid from w_part_2)
								and categorization_setup in							
									(
										-- category non-sortable
										select distinct t1.categorization_setup from
										(select a.*, b.* from
										(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
										from target_data.cm_ldsity2target2categorization_setup
										where ldsity2target_variable in (select unnest($2))
										) as a inner join target_data.cm_spc2classification_rule as b
										on a.id_cm_spc = b.id) as t1
										where t1.classification_rule = ''NOT EXISTS''
										union all
										-- categorization_setup that is a category without distinction
										select t2.categorization_setup
										from	(
												select categorization_setup, count(categorization_setup) as pocet
												from target_data.cm_ldsity2target2categorization_setup
												where ldsity2target_variable in (select unnest($2))
												and (adc2classification_rule is null and spc2classification_rule is null)
												group by categorization_setup
												) as t2
										where t2.pocet = array_length($2,1)
										union all
										-- categorization_setup for	ADC																			
										select categorization_setup
										from target_data.cm_ldsity2target2categorization_setup
										where categorization_setup in
											(
											select t.categorization_setup from 
											(select categorization_setup, count(categorization_setup) as pocet
											from target_data.cm_ldsity2target2categorization_setup
											where ldsity2target_variable in (select unnest($2))
											and spc2classification_rule is null
											group by categorization_setup) as t
											where t.pocet = array_length($2,1)
											)
										and adc2classification_rule in
										(
										-- category non-sortable for ADC
										select adc2classification_rule from
										target_data.cm_ldsity2target2categorization_setup
										where categorization_setup in 
										(select distinct t1.categorization_setup from
										(select a.*, b.* from
										(select unnest(spc2classification_rule) as id_cm_spc, categorization_setup
										from target_data.cm_ldsity2target2categorization_setup
										where ldsity2target_variable in (select unnest($2))
										) as a inner join target_data.cm_spc2classification_rule as b
										on a.id_cm_spc = b.id) as t1
										where t1.classification_rule = ''NOT EXISTS'')
										and adc2classification_rule is not null
										)
									)
								)
			,w_result_1 as	(
							select gid as plot, value_res as value, available_datasets from w_result union --all
							select gid as plot, value, available_datasets from w_not_exists
							)
			');
		
			_query_res_big := _query_res_big || _string_var_100 || _string_var_200 || _string_result_1;
		
		end if;
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
			
		-----------------------------------------------------------------------
		-----------------------------------------------------------------------
		_string_check_insert := replace(replace(concat
		(',w_tlv as		(
						select
								tlv.id,
								tlv.plot,
								tlv.available_datasets,
								tlv.value,
								coalesce(w_result_1.plot,tlv.plot) as plot_upr,
								coalesce(w_result_1.available_datasets,tlv.available_datasets) as available_datasets_upr,
								case
									when tlv.value is     null and w_result_1.value is not null then w_result_1.value
									when tlv.value is not null and tlv.value is distinct from 0.0 and w_result_1.value is null then 0.0
									when tlv.value is not null and tlv.value = 0.0 and w_result_1.value is null then null::double precision
									else w_result_1.value
								end
									as value_upr,
								case
									when tlv.value is     null and w_result_1.value is not null then false
									when tlv.value is not null and tlv.value is distinct from 0.0 and w_result_1.value is null then false
									when tlv.value is not null and tlv.value = 0.0 and w_result_1.value is null then null::boolean									
									else
										case
											when tlv.value = 0.0 and w_result_1.value = 0.0 then true
											when tlv.value = 0.0 and w_result_1.value is distinct from 0.0
													then
														case
															when (abs(1 - (tlv.value / w_result_1.value)) * 100.0) <= ',_threshold,' then true
															else false
														end
											when tlv.value is distinct from 0.0 and w_result_1.value = 0.0
													then
														case
															when (abs(1 - (w_result_1.value / tlv.value)) * 100.0) <= ',_threshold,'	then true
															else false
														end
											else
												case
													when (abs(1 - (tlv.value / w_result_1.value)) * 100.0) <= ',_threshold,' then true
													else false
												end
										end									
								end
									as value_identic
						from
							(
							select * from target_data.t_ldsity_values
							where available_datasets in
								(
								select
									a.id
								from
									(
									select * from target_data.t_available_datasets
									where categorization_setup in (select unnest($1))
									) as a
								inner
								join
									(
									select * from sdesign.cm_refyearset2panel_mapping
									where id in (select unnest(array[',_refyearset2panel_mapping,']))
									) as b
								on
									a.panel = b.panel and a.reference_year_set = b.reference_year_set
								)
							and is_latest = true
							) as tlv
						full outer join w_result_1
						on (tlv.plot = w_result_1.plot and tlv.available_datasets = w_result_1.available_datasets)
						)
		,w_update as	(
						update target_data.t_ldsity_values set is_latest = false where id in
						(select id from w_tlv where value_identic = false and id is not null)
						returning t_ldsity_values.plot, t_ldsity_values.available_datasets
						)
		insert into target_data.t_ldsity_values(plot,available_datasets,value,creation_time,is_latest,ext_version)
		select
				w_tlv.plot_upr as plot,
				w_tlv.available_datasets_upr as available_datasets,
				w_tlv.value_upr as value,
				now() as creation_time,
				true as is_latest,
				''',_ext_version_text,''' as ext_version
		from
				w_tlv
				left join w_update on w_tlv.plot_upr = w_update.plot and w_tlv.available_datasets_upr = w_update.available_datasets
		where
				w_tlv.value_identic = false
		order
				by w_tlv.plot_upr, w_tlv.available_datasets_upr;
		'),'{',''),'}','');	
	
	_query_res_big := _query_res_big || _string_check_insert;

	---------------------------------------------
	---------------------------------------------
	select coalesce(max(id),0) from target_data.t_ldsity_values into _max_id_tlv;
	---------------------------------------------
	---------------------------------------------
	execute ''||_query_res_big||'' using _categorization_setups, _array_id;
	---------------------------------------------
	---------------------------------------------
	_res := concat('The ',(select count(*) from target_data.t_ldsity_values where id > _max_id_tlv),' new local densities were prepared for ',(select count(t.plot) from (select distinct plot from target_data.t_ldsity_values where id > _max_id_tlv) as t),' plots.');
	---------------------------------------------
	---------------------------------------------
	return _res;
	---------------------------------------------
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_save_ldsity_values_internal(integer[], integer[], double precision) IS
'The function for the specified list of input arguments inserts data into the t_available_datasets table and inserts data into the t_ldsity_values table (aggregated local density at the plot level).';

grant execute on function target_data.fn_save_ldsity_values_internal(integer[], integer[], double precision) to public;

-- </function>

-- <function name="fn_etl_export_variable_hierarchy" schema="target_data" src="functions/etl/fn_etl_export_variable_hierarchy.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_variable_hierarchy
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_variable_hierarchy(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_variable_hierarchy
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_categorization_setups		integer[];
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_variable_hierarchy: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_variable_hierarchy: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w1 as	(
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(array[_refyearset2panel_mapping]))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select available_datasets from w3)
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_etl_export_variable_hierarchy: Internal argument _categorization_setups must not by NULL!';
		end if;

		-- supplement of missing categories
		_categorization_setups := target_data.fn_etl_supplement_categorization_setups(_categorization_setups);

		-------------------------------------------------------------
		refresh materialized view target_data.v_variable_hierarchy_internal;
		-------------------------------------------------------------
		
		with
		w1 as	(
				select
						id,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in	(select unnest(_categorization_setups))
				)
		,w2 as	(
				select
						w1.*,
						cltv.ldsity_object_type as core_or_division
				from
						w1
						inner join target_data.cm_ldsity2target_variable as cltv on w1.ldsity2target_variable = cltv.id
				)
		,w3 as	(				
				select
						w2.*,
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w2a.id_spt_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w2a.id_spc_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w2a.id_adt_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w2a.id_adc_orig from w2 as w2a where w2a.categorization_setup = w2.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adc
				from
						w2
				)
		,w4 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w3 order by categorization_setup
				)
		,w5 as	(	
				select
						node as variable_superior,
						panel, reference_year_set,
						unnest(edges) as variable
				from
						target_data.v_variable_hierarchy
				where
						node in (select unnest(_categorization_setups))
				)	
		,w6 as	(
				select
						_etl_id as etl_id_target_variable,
						w5.variable,
						w5.panel, w5.reference_year_set,
						w5.variable_superior,
						w4b.id_spt,
						w4a.id_spt as id_spt_sup,
						w4b.id_spc,
						w4a.id_spc as id_spc_sup,
						w4b.id_adt,
						w4a.id_adt as id_adt_sup,
						w4b.id_adc,
						w4a.id_adc as id_adc_sup
				from		w5
				left join	w4 as w4a	on w5.variable_superior = w4a.categorization_setup
				left join	w4 as w4b	on w5.variable = w4b.categorization_setup
				order by	w5.variable_superior, w5.variable
				)
		,w7 as	(
				select
						w6.*,
						tesp.id 	as id_sub_population_podrizene,
						tesp_sup.id as id_sub_population_nadrizene,
						tead.id 	as id_area_domain_podrizene,
						tead_sup.id	as id_area_domain_nadrizene
				from
						w6
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(w6.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp_sup
				on target_data.fn_etl_array_compare(w6.id_spt_sup,tesp_sup.sub_population)		
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(w6.id_adt,tead.area_domain)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead_sup
				on target_data.fn_etl_array_compare(w6.id_adt_sup,tead_sup.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						tespc.etl_id		as etl_id_spc,
						tespc_sup.etl_id	as etl_id_spc_sup,
						teadc.etl_id		as etl_id_adc,
						teadc_sup.etl_id	as etl_id_adc_sup
				from
						w7
				
				left join	target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population_podrizene = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc,tespc.sub_population_category)		
		
				left join	target_data.t_etl_sub_population_category as tespc_sup
				on  w7.id_sub_population_nadrizene = tespc_sup.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc_sup,tespc_sup.sub_population_category)			
			
				left join	target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain_podrizene = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc,teadc.area_domain_category)
		
				left join	target_data.t_etl_area_domain_category as teadc_sup
				on  w7.id_area_domain_nadrizene = teadc_sup.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc_sup,teadc_sup.area_domain_category)
				)
		,w9 as	(
				select
						etl_id_target_variable							as etl_id_tv,
						panel, reference_year_set,
						case when etl_id_spc is null then 0 else etl_id_spc end			as etl_id_spc,
						case when etl_id_spc_sup is null then 0 else etl_id_spc_sup end 	as etl_id_spc_sup,
						case when etl_id_adc is null then 0 else etl_id_adc end			as etl_id_adc,
						case when etl_id_adc_sup is null then 0 else etl_id_adc_sup end		as etl_id_adc_sup
				from
						w8
				)
		select
			json_agg(json_build_object(
				'target_variable',			w9.etl_id_tv,
				'country',					(select c_country.label
										from sdesign.c_country
										inner join sdesign.t_strata_set on (t_strata_set.country = c_country.id)
										inner join sdesign.t_stratum on (t_stratum.strata_set = t_strata_set.id)
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'strata_set',					(select t_strata_set.strata_set
										from sdesign.t_strata_set
										inner join sdesign.t_stratum on (t_stratum.strata_set = t_strata_set.id)
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'stratum',					(select t_stratum.stratum
										from sdesign.t_stratum
										inner join sdesign.t_panel on (t_panel.stratum = t_stratum.id)
										where t_panel.id = w9.panel),
				'reference_year_set',				(select reference_year_set from sdesign.t_reference_year_set where t_reference_year_set.id = w9.reference_year_set),
				'panel',					(select panel from sdesign.t_panel where t_panel.id = w9.panel),
				'sub_population_category',		w9.etl_id_spc,
				'sub_population_category_superior',	w9.etl_id_spc_sup,
				'area_domain_category',			w9.etl_id_adc,
				'area_domain_category_superior',	w9.etl_id_adc_sup)
				order by w9.etl_id_tv, w9.panel, w9.reference_year_set, w9.etl_id_spc, w9.etl_id_spc_sup, w9.etl_id_adc, w9.etl_id_adc_sup
			)
		into _res
		from w9;
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_variable_hierarchy(integer[], integer) is
'Function returns records for ETL t_variable_hierarchy table.';

grant execute on function target_data.fn_etl_export_variable_hierarchy(integer[], integer) to public;

-- </function>
