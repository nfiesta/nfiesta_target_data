--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(integer, character varying, boolean) CASCADE;
DROP FUNCTION IF EXISTS target_data.fn_etl_get_panel_refyearset_combinations(integer,integer[]) CASCADE;




-- <function name="fn_etl_get_target_variable" schema="target_data" src="functions/etl/fn_etl_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) CASCADE;

create or replace function target_data.fn_etl_get_target_variable
(
	_export_connection	integer,
	_national_language	character varying(2) default 'en'::character varying(2),
	_etl				boolean default null::boolean,
	_target_variable	integer default null::integer
)
returns table
(
	id							integer,
	label						varchar,
	id_etl_target_variable		integer,
	check_target_variable		boolean,
	refyearset2panel_mapping	integer[],
	metadata					json
)
as
$$
declare
		_cond_1			text;
		_cond_2			text;
		_set_column		text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable: Input argument _export_connection must not be null!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_get_target_variable: Input argument _national_language must not be null!';
		end if;

		if _etl is null
		then
			_cond_1 := 'TRUE';
		else
			if _etl = true
			then
				_cond_1 := 'w2.check_target_variable = TRUE';
			else
				_cond_1 := 'w2.check_target_variable = FALSE';
			end if;
		end if;

		if _national_language = 'en'
		then
			_set_column := 'w1.label_en';
		else
			_set_column := 'w1.label';
		end if;

		if _target_variable is null
		then
			_cond_2 := 'TRUE';
		else
			_cond_2 := 'ctv.id = $3';
		end if;

		return query execute
		'
		with
		w1 as	(
				select 
						ctv.*,
						(target_data.fn_etl_check_target_variable($1,ctv.id)) as res
				from 
						target_data.c_target_variable as ctv
				where
						'|| _cond_2 ||'
				order
						by ctv.id
				)
		,w2 as	(
				select
						w1.id,
						'|| _set_column ||' as label,
						(w1.res).id_etl_target_variable,
						(w1.res).check_target_variable,
						(w1.res).refyearset2panel_mapping
				from
						w1
				)
		,w3 as	(
				select
						w2.*,
						target_data.fn_etl_get_target_variable_metadata(w2.id,$2) as metadata
				from
						w2 where '|| _cond_1 ||'
				)
		select
				w3.id,
				w3.label,
				w3.id_etl_target_variable,
				w3.check_target_variable,
				w3.refyearset2panel_mapping,
				w3.metadata
		from
				w3 order by w3.id;
		'
		using _export_connection, _national_language, _target_variable;

end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) IS
'The function returs target variables and informations for ETL. If input argument _etl = TRUE then function
returns target variables that was ETL and all theirs datas ar current. If input argument _etl = FALSE
then function retursn target variables that was not ETL yet or returns target variables that was ETL
and exists newer datas for ETL. If input argument _etl IS NULL (_etl = true or false) then function
returns both cases.';

grant execute on function target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) to public;
-- </function>



-- <function name="fn_etl_get_panel_refyearset_combinations" schema="target_data" src="functions/etl/fn_etl_get_panel_refyearset_combinations.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_panel_refyearset_combinations
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_panel_refyearset_combinations(integer,integer[],integer[]) CASCADE;

create or replace function target_data.fn_etl_get_panel_refyearset_combinations
(
	_id_t_etl_target_variable			integer,
	_refyearset2panel_mapping			integer[],
	_refyearset2panel_mapping_module	integer[] default null::integer[]
)
returns table
(
	refyearset2panel		integer,
	panel					varchar,	
	reference_year_set		varchar,
	passed_by_module		boolean
)
as
$$
declare
		_res_panels_and_reference_year_sets		json;
		_country_array							integer[];
		_res_country							varchar;
		_target_variable						integer;
		_etl_id									integer;
		_categorization_setups					integer[];
		_res									json;
		_check_rys2pm							integer;
begin		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 01: fn_etl_get_panel_refyearset_combinations: Input argument _id_t_etl_target_variable must not be NULL!';
		end if;

		select
				tetv.target_variable
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_target_variable;

		if _target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_panel_refyearset_combinations: For input argument _id_t_etl_target_variable = % not exists any target_variable in table t_etl_target_variable!',_id_t_etl_target_variable;
		end if;

		if _refyearset2panel_mapping_module is not null and _refyearset2panel_mapping is null
		then
			raise exception 'Error 03: fn_etl_get_panel_refyearset_combinations: If input argument _refyearset2panel_mapping_module is not null then input argument _refyearset2panel_mapping must be not null!';
		end if;

		if _refyearset2panel_mapping_module is not null
		then
			with
			w1 as	(select unnest(_refyearset2panel_mapping) as rys2pm)
			,w2 as	(select unnest(_refyearset2panel_mapping_module) as rys2pm)
			,w3 as	(
					select w2.rys2pm from w2 except
					select w1.rys2pm from w1
					)
			select count(w3.rys2pm) from w3
			into
				_check_rys2pm;

			if _check_rys2pm is distinct from 0
			then
				raise exception 'Error 04: fn_etl_get_panel_refyearset_combinations: Some of values in input argument _refyearset2panel_mapping_module is not contains in input argument _refyearset2panel_mapping!';
			end if;
		end if;

		if _refyearset2panel_mapping is null and _refyearset2panel_mapping_module is not null
		then
			raise exception 'Error 05: fn_etl_get_panel_refyearset_combinations: If input argument _refyearset2panel_mapping is null then input argument _refyearset2panel_mapping_module must be null!';
		end if;


		if _refyearset2panel_mapping is null
		then
			return query
			with
			w1 as	(
					select tad.* from target_data.t_available_datasets as tad where tad.categorization_setup in
						(
						select distinct cmlcs.categorization_setup
						from target_data.cm_ldsity2target2categorization_setup as cmlcs
						where cmlcs.ldsity2target_variable in
							(
							select cmltv.id from target_data.cm_ldsity2target_variable as cmltv
							where cmltv.target_variable = _target_variable
							)
						)
					)
			,w2 as	(
					select distinct tlv.available_datasets from target_data.t_ldsity_values as tlv
					where tlv.available_datasets in (select w1.id from w1)
					and is_latest = true
					)
			,w3 as	(
					select distinct tad1.panel, tad1.reference_year_set
					from target_data.t_available_datasets as tad1 where tad1.id in
					(select w2.available_datasets from w2)
					)
			,w4 as	(
					select
							w3.panel as panel_id,
							w3.reference_year_set as reference_year_set_id,
							crpm.id as refyearset2panel,
							tp.panel,
							trys.reference_year_set from w3
					inner join sdesign.cm_refyearset2panel_mapping as crpm on w3.panel = crpm.panel and w3.reference_year_set = crpm.reference_year_set
					inner join sdesign.t_panel as tp on w3.panel = tp.id
					inner join sdesign.t_reference_year_set as trys on w3.reference_year_set = trys.id
					)
			select
					w4.refyearset2panel,
					w4.panel,
					w4.reference_year_set,
					false as passed_by_module
			from
					w4 order by w4.refyearset2panel;
		else
			return query
			with
			w1 as	(
					select cm.* from sdesign.cm_refyearset2panel_mapping as cm
					where cm.id in (select unnest(_refyearset2panel_mapping))
					)
			,w2 as	(
					select
							w1.id as refyearset2panel,
							w1.panel as panel_id,
							w1.reference_year_set as reference_year_set_id,
							tp.panel,
							trys.reference_year_set
					from
							w1
							inner join sdesign.t_panel as tp on w1.panel = tp.id
							inner join sdesign.t_reference_year_set as trys on w1.reference_year_set = trys.id
					)
			,w3 as	(
					select unnest(_refyearset2panel_mapping_module) as rys2pm
					)
			select
					w2.refyearset2panel,
					w2.panel,
					w2.reference_year_set,
					case when w3.rys2pm is not null then true else false end as passed_by_module
			from
					w2
					left join w3 on w2.refyearset2panel = w3.rys2pm
			order
					by w2.refyearset2panel;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_panel_refyearset_combinations(integer, integer[], integer[]) is
'Function returns list of combinations of panels and reference year sets for given target variable that are available for ETL.';

grant execute on function target_data.fn_etl_get_panel_refyearset_combinations(integer, integer[], integer[]) to public;
-- </function>



-- <function name="fn_etl_get_variables" schema="target_data" src="functions/etl/fn_etl_get_variables.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_variables
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_variables(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_get_variables
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer
)
returns json
as
$$
declare
		_country_array							integer[];
		_res_country							varchar;
		_export_connection						integer;
		_target_variable						integer;
		_etl_id									integer;
		_res									json;

		_core_and_division						boolean;
begin
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_get_variables: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_get_variables: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;

		select array_agg(tss.country order by tss.country) from sdesign.t_strata_set as tss where tss.id in
		(select strata_set from sdesign.t_stratum where id in
		(select stratum from sdesign.t_panel
		where id in ((select panel from sdesign.cm_refyearset2panel_mapping where id in (select unnest(array[_refyearset2panel_mapping]))))))
		into _country_array;
	
		if array_length(_country_array,1) is distinct from 1
		then
			raise exception 'Error 03: fn_etl_get_variables: Input argument _refyearset2panel_mapping contains values for two or more different countries!';
		end if;
	
		select label from sdesign.c_country where id = _country_array[1]
		into _res_country;
	
		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;
	
		with
		w_etl_ad as		(
						select * from target_data.t_etl_area_domain	where export_connection = _export_connection
						)
		,w_etl_adc as	(
						select * from target_data.t_etl_area_domain_category where etl_area_domain in (select id from w_etl_ad)
						)
		,w_etl_sp as	(
						select * from target_data.t_etl_sub_population where export_connection = _export_connection
						)
		,w_etl_spc as	(
						select * from target_data.t_etl_sub_population_category where etl_sub_population in (select id from w_etl_sp)
						)		
		,w1 as	(-- complete list of categorization setups for target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where cm.target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups for target variable that is reduced by
				 -- combination of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set
				)
		,w3 as	(-- list of IDs of available datasets from t_ldsity_values table
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		,w4 as	(
				select w2.* from w2 where w2.id in (select w3.available_datasets from w3)
				)
		,w5 as	(
				select
						w4.panel,
						w4.reference_year_set,
						array_agg(w4.categorization_setup order by w4.categorization_setup) as categorization_setup
				from
						w4 group by w4.panel, w4.reference_year_set
				)
		,w6 as	(
				select
						w5.*,
						target_data.fn_etl_supplement_categorization_setups(w5.categorization_setup) as categorization_setup_supplement
				from
						w5
				)
		,w7 as	(
				select
						w6.panel,
						w6.reference_year_set,
						unnest(w6.categorization_setup_supplement) as categorization_setup_supplement
				from
						w6
				)
		,w8 as	(
				select
					id,
					ldsity2target_variable,
					categorization_setup,
					false as core_and_division,				
					(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).id_type as id_spt_orig,
					(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).id_type as id_adt_orig,
					(target_data.fn_get_category4classification_rule_id('spc', t.spc2classification_rule)).id_category as id_spc_orig,
					(target_data.fn_get_category4classification_rule_id('adc', t.adc2classification_rule)).id_category as id_adc_orig,
					(target_data.fn_get_category_type4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_spt_orig,
					(target_data.fn_get_category_type4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_adt_orig,
					(target_data.fn_get_category4classification_rule_id('spc', t.spc2classification_rule)).label_en as label_spc_orig,
					(target_data.fn_get_category4classification_rule_id('adc', t.adc2classification_rule)).label_en as label_adc_orig
				from
					target_data.cm_ldsity2target2categorization_setup as t
				where
					ldsity2target_variable
				in
						(
						select id from target_data.cm_ldsity2target_variable
						where target_variable = _target_variable
						)
				and categorization_setup in (select distinct w7.categorization_setup_supplement from w7)
				)		
		,w9 as	(
				select
						w7.*,
						w8.*,
						cltv.ldsity_object_type as core_or_division
				from
						w7
						inner join w8 on w7.categorization_setup_supplement = w8.categorization_setup
						inner join target_data.cm_ldsity2target_variable as cltv on w8.ldsity2target_variable = cltv.id
				)
		,w10 as	(				
				select
						w9.*,
						-------------
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w2a.id_spt_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w2a.id_spc_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w2a.id_adt_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w2a.id_adc_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 100)
									end
						end as id_adc,
						-------------
						case
							when core_and_division = false
								then label_spt_orig
								else
									case
										when core_or_division = 200
										then label_spt_orig
										else (select distinct w2a.label_spt_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 200)
									end
						end as label_spt,
						
						case
							when core_and_division = false
								then label_spc_orig
								else
									case
										when core_or_division = 200
										then label_spc_orig
										else (select distinct w2a.label_spc_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 200)
									end
						end as label_spc,
						
						case
							when core_and_division = false
								then label_adt_orig
								else
									case
										when core_or_division = 100
										then label_adt_orig
										else (select distinct w2a.label_adt_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 100)
									end
						end as label_adt,
						
						case
							when core_and_division = false
								then label_adc_orig
								else
									case
										when core_or_division = 100
										then label_adc_orig
										else (select distinct w2a.label_adc_orig from w9 as w2a where w2a.categorization_setup = w9.categorization_setup and w2a.core_or_division = 100)
									end
						end as label_adc						
				from
						w9
				)
		,w11 as	(
				select distinct panel, reference_year_set, categorization_setup, id_spt, id_adt, id_spc, id_adc, label_spt, label_adt, label_spc, label_adc from w10 order by categorization_setup
				)
		,w12 as	(
				select
						w11.*,
						w_etl_ad.etl_id as etl_id_ad,
						w_etl_sp.etl_id as etl_id_sp,
						w_etl_adc.etl_id as etl_id_adc,
						w_etl_spc.etl_id as etl_id_spc,
						tp.panel as panel__label,
						trys.reference_year_set as reference_year_set__label
				from
							w11
				left join	w_etl_ad	on w11.id_adt = w_etl_ad.area_domain
				left join	w_etl_sp	on w11.id_spt= w_etl_sp.sub_population
				left join 	w_etl_adc	on w11.id_adc = w_etl_adc.area_domain_category
				left join	w_etl_spc	on w11.id_spc = w_etl_spc.sub_population_category
				
				inner join sdesign.t_panel as tp on w11.panel = tp.id
				inner join sdesign.t_reference_year_set as trys on w11.reference_year_set = trys.id 
				)
		,w13 as	(
				select
						w12.panel__label,
						w12.reference_year_set__label,
						case when label_spt is null then 'null'::varchar else label_spt end as variables_sp,
						case when label_adt is null then 'null'::varchar else label_adt end as variables_ad,
						case when label_spc is null then 'null'::varchar else label_spc end as variables_spc,
						case when label_adc is null then 'null'::varchar else label_adc end as variables_adc,					
						case when etl_id_sp is null then 0 else etl_id_sp end as etl_id_sp,
						case when etl_id_spc is null then 0 else etl_id_spc end as etl_id_spc,
						case when etl_id_ad is null then 0 else etl_id_ad end as etl_id_ad,
						case when etl_id_adc is null then 0 else etl_id_adc end as etl_id_adc
				from
						w12
				)
		select
				json_build_object
				(
				'country', _res_country,
				'target_variable', _etl_id,
				'variables',json_agg(json_build_object(
					'panel', w13.panel__label,
					'reference_year_set', w13.reference_year_set__label,
					'sub_population',  w13.variables_sp,
					'sub_population_category', w13.variables_spc,
					'area_domain',  w13.variables_ad,
					'area_domain_category', w13.variables_adc,
					'sub_population_etl_id', w13.etl_id_sp,
					'sub_population_category_etl_id', w13.etl_id_spc,
					'area_domain_etl_id', w13.etl_id_ad,
					'area_domain_category_etl_id', w13.etl_id_adc))
				)
		from w13
		into _res;

		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_variables(integer[], integer) is
'Function returns variables for given input arguments.';

grant execute on function target_data.fn_etl_get_variables(integer[], integer) to public;
-- </function>



-- <function name="fn_etl_get_panel_refyearset_combinations_false" schema="target_data" src="functions/etl/fn_etl_get_panel_refyearset_combinations_false.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_panel_refyearset_combinations_false
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_panel_refyearset_combinations_false(JSON, integer[]) CASCADE;

create or replace function target_data.fn_etl_get_panel_refyearset_combinations_false
(
	_variables					JSON,
	_refyearset2panel_mapping	integer[]
)
returns table
(
	refyearset2panel		integer,
	panel					varchar,	
	reference_year_set		varchar,
	useable4etl				boolean
)
as
$$
declare
begin		
		if _variables is null
		then
			raise exception 'Error 01: fn_etl_get_panel_refyearset_combinations_false: Input argument _variables must not be NULL!';
		end if;

		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 02: fn_etl_get_panel_refyearset_combinations_false: Input argument _refyearset2panel_mapping must not be NULL!';
		end if;

		return query
		with
		w1 as	(
				select cm.* from sdesign.cm_refyearset2panel_mapping as cm
				where cm.id in (select unnest(_refyearset2panel_mapping))
				)
		,w2 as	(
				select
						w1.id as refyearset2panel,
						w1.panel as panel_id,
						w1.reference_year_set as reference_year_set_id,
						tp.panel,
						trys.reference_year_set
				from
						w1
						inner join sdesign.t_panel as tp on w1.panel = tp.id
						inner join sdesign.t_reference_year_set as trys on w1.reference_year_set = trys.id
				)
		,w3 as	(
				select json_array_elements(_variables) as s
				)
		,w4 as	(
				select
						(s->>'panel')::varchar					as panel,
						(s->>'reference_year_set')::varchar		as reference_year_set
				from w3
				)
		,w5 as	(
				select distinct w4.panel, w4.reference_year_set from w4
				)
		select
				w2.refyearset2panel,
				w2.panel,
				w2.reference_year_set,
				case when w5.panel is not null and w5.reference_year_set is not null then false else true end as useable4etl
		from
				w2
				left join w5 on w2.panel = w5.panel and w2.reference_year_set = w5.reference_year_set
		order
				by w2.refyearset2panel;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_panel_refyearset_combinations_false(JSON, integer[]) is
'Function returns list of combinations of panels and reference year sets that user wants to for ETL and is added information that given combination of panel a reference year set is useable to ETL.';

grant execute on function target_data.fn_etl_get_panel_refyearset_combinations_false(JSON, integer[]) to public;
-- </function>



-- <function name="fn_etl_get_variables_false" schema="target_data" src="functions/etl/fn_etl_get_variables_false.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_variables_false
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_variables_false(JSON, varchar, varchar, character varying) CASCADE;

create or replace function target_data.fn_etl_get_variables_false
(
	_variables				JSON,
	_panel					varchar,
	_reference_year_set		varchar,
	_national_language		character varying(2) default 'en'::character varying(2)
)
returns table
(
	sub_population				varchar,
	sub_population_category		varchar,	
	area_domain					varchar,
	area_domain_category		varchar
)
as
$$
declare
begin		
		if _variables is null
		then
			raise exception 'Error 01: fn_etl_get_variables_false: Input argument _variables must not be NULL!';
		end if;

		if _panel is null
		then
			raise exception 'Error 02: fn_etl_get_variables_false: Input argument _panel must not be NULL!';
		end if;

		if _reference_year_set is null
		then
			raise exception 'Error 03: fn_etl_get_variables_false: Input argument _reference_year_set must not be NULL!';
		end if;		

		if _national_language = 'en'
		then
			return query
			with
				w1 as	(
						select json_array_elements(_variables) as s
						)
				,w2 as	(
						select
								(s->>'panel')::varchar							as panel,
								(s->>'reference_year_set')::varchar				as reference_year_set,
								(s->>'sub_population_en')::varchar				as sub_population,
								(s->>'sub_population_category_en')::varchar		as sub_population_category,
								(s->>'area_domain_en')::varchar					as area_domain,
								(s->>'area_domain_category_en')::varchar		as area_domain_category					
						from w1
						)
				,w3 as	(
						select w2.* from w2
						where w2.panel = _panel
						and w2.reference_year_set = _reference_year_set
						)
				select
						w3.sub_population,
						w3.sub_population_category,
						w3.area_domain,
						w3.area_domain_category
				from
						w3;
		else
			return query
			with
				w1 as	(
						select json_array_elements(_variables) as s
						)
				,w2 as	(
						select
								(s->>'panel')::varchar							as panel,
								(s->>'reference_year_set')::varchar				as reference_year_set,
								(s->>'sub_population')::varchar					as sub_population,
								(s->>'sub_population_category')::varchar		as sub_population_category,
								(s->>'area_domain')::varchar					as area_domain,
								(s->>'area_domain_category')::varchar			as area_domain_category						
						from w1
						)
				,w3 as	(
						select w2.* from w2
						where w2.panel = _panel
						and w2.reference_year_set = _reference_year_set
						)
				select
						w3.sub_population,
						w3.sub_population_category,
						w3.area_domain,
						w3.area_domain_category
				from
						w3;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_variables_false(JSON, varchar, varchar, character varying) is
'Function returns list of attribute variables that must be still implemented for given combination of panel and reference year set for selected target variable.';

grant execute on function target_data.fn_etl_get_variables_false(JSON, varchar, varchar, character varying) to public;
-- </function>

