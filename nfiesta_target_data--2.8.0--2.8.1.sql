--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



-- <function name="fn_etl_get_target_variable" schema="target_data" src="functions/etl/fn_etl_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) CASCADE;

create or replace function target_data.fn_etl_get_target_variable
(
	_export_connection	integer,
	_national_language	character varying(2) default 'en'::character varying(2),
	_etl				boolean default null::boolean,
	_target_variable	integer default null::integer
)
returns table
(
	id								integer,
	label							varchar,
	description						text,
	label_en						varchar,
	description_en					text,
	id_etl_target_variable			integer,
	check_target_variable			boolean,
	refyearset2panel_mapping		integer[],
	check_atomic_area_domains		boolean,
	check_atomic_sub_populations	boolean,
	metadata						json
)
as
$$
declare
		_check_atomic_ad			integer;
		_check_atomic_sp			integer;
		_check_atomic				integer;
		_cond_1						text;
		_cond_2						text;
		_target_variable4etl		integer[];
		_set_column_label			text;
		_set_column_label_en		text;
		_set_column_description		text;
		_set_column_description_en	text;
begin
		if _export_connection is null
		then
			raise exception 'Error 01: fn_etl_get_target_variable: Input argument _export_connection must not be null!';
		end if;

		if _national_language is null
		then
			raise exception 'Error 02: fn_etl_get_target_variable: Input argument _national_language must not be null!';
		end if;

		-------------------------------------------------------------
		with
		w1 as	(
				select unnest(tead.area_domain) as area_domain from target_data.t_etl_area_domain as tead
				where tead.export_connection = _export_connection
				)
		,w2 as	(
				select tead2.area_domain[1] from target_data.t_etl_area_domain as tead2
				where tead2.export_connection = _export_connection
				and array_length(tead2.area_domain,1) = 1
				)
		,w3 as	(
				select w1.area_domain from w1 except
				select w2.area_domain from w2
				)
		select count(w3.*) from w3
		into _check_atomic_ad;
		-------------------------------------------------------------
		with
		w1 as	(
				select unnest(tesp.sub_population) as sub_population from target_data.t_etl_sub_population as tesp
				where tesp.export_connection = _export_connection
				)
		,w2 as	(
				select tesp2.sub_population[1] from target_data.t_etl_sub_population as tesp2
				where tesp2.export_connection = _export_connection
				and array_length(tesp2.sub_population,1) = 1
				)
		,w3 as	(
				select w1.sub_population from w1 except
				select w2.sub_population from w2
				)
		select count(w3.*) from w3
		into _check_atomic_sp;	
		-------------------------------------------------------------
		_check_atomic := _check_atomic_ad + _check_atomic_sp;
		-------------------------------------------------------------
		if _etl is null
		then
			_cond_1 := 'TRUE';
		else
			if _etl = true
			then
				if _check_atomic = 0
				then
					_cond_1 := 'w2a.check_target_variable = TRUE';
				else
					_cond_1 := 'w16.check_target_variable = TRUE';
				end if;
			else
				if _check_atomic = 0
				then
					_cond_1 := 'w2a.check_target_variable = FALSE';
				else
					_cond_1 := 'w16.check_target_variable = FALSE';
				end if;
			end if;
		end if;
		-------------------------------------------------------------
		if _national_language = 'en'
		then
			_set_column_label := 'w1a.label_en';
			_set_column_label_en := 'w1a.label_en';
			_set_column_description := 'w1a.description_en';
			_set_column_description_en := 'w1a.description_en';			
		else
			_set_column_label := 'w1a.label';
			_set_column_label_en := 'w1a.label_en';
			_set_column_description := 'w1a.description';
			_set_column_description_en := 'w1a.description_en';
		end if;
		-------------------------------------------------------------
		if _target_variable is null
		then
			_cond_2 := 'TRUE';
		else
			_cond_2 := 'ctv.id = $3';
		end if;
		-------------------------------------------------------------
		with
		w1 as	(
				select
						cltv.id as id_ldsity2target_variable,
						cltv.target_variable
				from
						target_data.cm_ldsity2target_variable cltv
				where
						target_variable in (select ctv.id from target_data.c_target_variable as ctv)		
				)
		,w2 as	(
				select
						w1.*,
						cltcs.*,
						tad.id as id_available_datasets
				from
						w1
						inner join target_data.cm_ldsity2target2categorization_setup as cltcs
						on w1.id_ldsity2target_variable = cltcs.ldsity2target_variable
						
						inner join target_data.t_available_datasets as tad
						on cltcs.categorization_setup = tad.categorization_setup
				)
		,w3 as	(
				select distinct w2.target_variable from w2
				where w2.id_available_datasets in
					(select distinct tlv.available_datasets from target_data.t_ldsity_values as tlv
					where tlv.available_datasets in (select distinct w2.id_available_datasets from w2)
					and tlv.is_latest = true)
				)
		select array_agg(w3.target_variable order by w3.target_variable) from w3
		into _target_variable4etl;
		-------------------------------------------------------------
		if _check_atomic = 0
		then
			return query execute
			'
			with
			w1a as	(
					select 
							ctv.*,
							(target_data.fn_etl_check_target_variable($1,ctv.id)) as res
					from 
							target_data.c_target_variable as ctv
					where
							ctv.id in (select unnest($4))
					and
							'|| _cond_2 ||'
					order
							by ctv.id
					)
			,w2a as	(
					select
							w1a.id,
							'|| _set_column_label ||' as label,
							'|| _set_column_description ||' as description,
							'|| _set_column_label_en ||' as label_en,
							'|| _set_column_description_en ||' as description_en,
							(w1a.res).id_etl_target_variable,
							(w1a.res).check_target_variable,
							(w1a.res).refyearset2panel_mapping							
					from
							w1a
					)
			,w3a as	(
					select
							w2a.*,
							target_data.fn_etl_get_target_variable_metadata(w2a.id,$2) as metadata
					from
							w2a where '|| _cond_1 ||'
					)	
			select
					w3a.id,
					w3a.label,
					w3a.description,
					w3a.label_en,
					w3a.description_en,
					w3a.id_etl_target_variable,
					w3a.check_target_variable,
					w3a.refyearset2panel_mapping,
					null::boolean as check_atomic_area_domains,
					null::boolean as check_atomic_sub_populations,
					w3a.metadata
			from
					w3a order by w3a.id;			
			'
			using _export_connection, _national_language, _target_variable, _target_variable4etl;
		else
			return query execute
			'
			with
			w1a as	(
					select 
							ctv.*,
							(target_data.fn_etl_check_target_variable($1,ctv.id)) as res
					from 
							target_data.c_target_variable as ctv
					where
							ctv.id in (select unnest($4))
					and
							'|| _cond_2 ||'
					order
							by ctv.id
					)
			,w2a as	(
					select
							w1a.id,
							'|| _set_column_label ||' as label,
							'|| _set_column_description ||' as description,
							'|| _set_column_label_en ||' as label_en,
							'|| _set_column_description_en ||' as description_en,
							(w1a.res).id_etl_target_variable,
							(w1a.res).check_target_variable,
							(w1a.res).refyearset2panel_mapping
					from
							w1a
					)
			,w3a as	(
					select
							w2a.id,
							w2a.label,
							w2a.description,
							w2a.label_en,
							w2a.description_en,
							-----------------------------------
							w2a.id_etl_target_variable,
							w2a.check_target_variable,
							w2a.refyearset2panel_mapping,
							-----------------------------------
							case
								when w2a.id_etl_target_variable is null
								then
									false
								else
									case
										when w2a.check_target_variable = true
										then true
										else false
									end
							end as do_checks4atrribute
							-----------------------------------
					from
							w2a
					)
			-------------------
			-------------------
			,w1 as	(
					select
							t2.etl_target_variable,
							array_agg(t2.refyearset2panel_mapping order by t2.refyearset2panel_mapping) as refyearset2panel_mapping4checks4attribute
					from
							(
							select
									tel.*
							from
									(select * from target_data.t_etl_log where etl_target_variable in (select w3a.id_etl_target_variable from w3a where w3a.do_checks4atrribute = true)) as tel
							inner
							join	(
									select
											etl_target_variable,
											refyearset2panel_mapping,
											max(etl_time) as etl_time_max
									from
											target_data.t_etl_log
									where
											etl_target_variable in (select w3a.id_etl_target_variable from w3a where w3a.do_checks4atrribute = true)
									group
											by etl_target_variable, refyearset2panel_mapping
									) as t1
							on
									tel.etl_target_variable = t1.etl_target_variable
							and		tel.refyearset2panel_mapping = t1.refyearset2panel_mapping
							and		tel.etl_time = t1.etl_time_max
							)
							as t2
					group
							by t2.etl_target_variable
					)
			,w2 as	(
					select
							w1.*,
							tetv.target_variable
					from
							w1
							inner join target_data.t_etl_target_variable as tetv on w1.etl_target_variable = tetv.id
					)
			,w3 as	(
					select
							target_variable,
							unnest(refyearset2panel_mapping4checks4attribute) as refyearset2panel_mapping4checks4attribute
					from
							w2
					)		
			,w4 as	(
					select
							w3.*,
							crpm.panel,
							crpm.reference_year_set
					from
							w3
							inner join sdesign.cm_refyearset2panel_mapping as crpm on w3.refyearset2panel_mapping4checks4attribute = crpm.id
					)
					-----------------
			,w5 as	(
					select
							t1.target_variable,
							cltcs.ldsity2target_variable,
							cltcs.adc2classification_rule,
							cltcs.spc2classification_rule,
							cltcs.categorization_setup,
							case
								when cltcs.adc2classification_rule is null then null::integer[]
								else (select id_type from target_data.fn_get_category_type4classification_rule_id(''adc'', cltcs.adc2classification_rule))
							end as adc_type,
							case
								when cltcs.spc2classification_rule is null then null::integer[]
								else (select id_type from target_data.fn_get_category_type4classification_rule_id(''spc'', cltcs.spc2classification_rule))
							end as spc_type
					from
							(
							select id, target_variable from target_data.cm_ldsity2target_variable
							where target_variable in (select distinct target_variable from w2)
							) as t1
					inner join target_data.cm_ldsity2target2categorization_setup as cltcs
					on t1.id = cltcs.ldsity2target_variable
					)
			,w6 as	(
					select
							w4.target_variable,
							w4.refyearset2panel_mapping4checks4attribute,
							w4.panel,
							w4.reference_year_set,
							w5.ldsity2target_variable,
							w5.adc2classification_rule,
							w5.spc2classification_rule,
							w5.adc_type,
							w5.spc_type,
							tad.id as available_datasets_id
					from
							w4
							inner join w5 on w4.target_variable = w5.target_variable
							inner join target_data.t_available_datasets as tad
							on w4.panel = tad.panel
							and w4.reference_year_set = tad.reference_year_set 
							and w5.categorization_setup = tad.categorization_setup
					)
			,w7 as	(
					select w6.* from w6 where w6.available_datasets_id in
						(
						select distinct tlv.available_datasets from target_data.t_ldsity_values as tlv
						where tlv.available_datasets in (select distinct w6.available_datasets_id from w6)
						and is_latest = true
						)
					)
			,w8 as	(
					select
							t4.target_variable,
							count(t4.adc_type) as adc_type_pocet
					from
							(
							select t3.* from
								(
								select distinct t2.target_variable, t2.adc_type from
								(select t1.target_variable, unnest(t1.adc_type) as adc_type
								from (select distinct target_variable, adc_type from w7 where adc_type is not null) as t1) as t2
								) as t3
							where t3.adc_type not in	(
														select area_domain[1] as area_domain
														from target_data.t_etl_area_domain as tead
														where tead.export_connection = $1
														and array_length(area_domain,1) = 1
														)
							) as t4
					group
							by t4.target_variable
					)
			,w9 as	(
					select
							t4.target_variable,
							count(t4.spc_type) as spc_type_pocet
					from
							(
							select t3.* from
								(
								select distinct t2.target_variable, t2.spc_type from
								(select t1.target_variable, unnest(t1.spc_type) as spc_type
								from (select distinct target_variable, spc_type from w7 where spc_type is not null) as t1) as t2
								) as t3
							where t3.spc_type not in	(
														select sub_population[1] as sub_population
														from target_data.t_etl_sub_population as tesp
														where tesp.export_connection = $1
														and array_length(sub_population,1) = 1
														)
							) as t4
					group
							by t4.target_variable
					)
			,w10 as	(
					select w8.*, tetv.id as id_etl_target_variable from w8
					inner join (select * from target_data.t_etl_target_variable where export_connection = $1) as tetv
					on w8.target_variable = tetv.target_variable
					)
			,w11 as	(
					select w9.*, tetv.id as id_etl_target_variable from w9
					inner join (select * from target_data.t_etl_target_variable where export_connection = $1) as tetv
					on w9.target_variable = tetv.target_variable
					)
			,w12 as	(
					select
							w1.*,
							case when w10.adc_type_pocet is null then 0 else w10.adc_type_pocet end as res_of_check_ad,
							case when w11.spc_type_pocet is null then 0 else w11.spc_type_pocet end as res_of_check_sp
					from
							w1
							left join w10 on w1.etl_target_variable = w10.id_etl_target_variable
							left join w11 on w1.etl_target_variable = w11.id_etl_target_variable
					)
			,w14 as	(
					select
							w3a.*,
							w12.*
					from
							w3a
							left join w12 on w3a.id_etl_target_variable = w12.etl_target_variable
					)
			,w15 as	(
					select
							w14.id,
							w14.label,
							w14.description,
							w14.label_en,
							w14.description_en,
							w14.id_etl_target_variable,
							w14.check_target_variable,
							---------------------------------------------
							case
								when w14.id_etl_target_variable is not null and w14.check_target_variable = true
								then
									case
										when w14.res_of_check_ad = 0 and w14.res_of_check_sp = 0
										then true
										else false
									end							
								else
									w14.check_target_variable
							end
								as check_target_variable4output,
							---------------------------------------------
							case
								when w14.id_etl_target_variable is not null and w14.check_target_variable = true
								then
									w14.refyearset2panel_mapping4checks4attribute	
								else
									w14.refyearset2panel_mapping
							end
								as refyearset2panel_mapping,						
							---------------------------------------------
							case
								when w14.id_etl_target_variable is not null and w14.check_target_variable = true
								then
									case
										when w14.res_of_check_ad = 0
										then true
										else false
									end							
								else
									null::boolean
							end
								as check_atomic_area_domains,
							---------------------------------------------
							case
								when w14.id_etl_target_variable is not null and w14.check_target_variable = true
								then
									case
										when w14.res_of_check_sp = 0
										then true
										else false
									end							
								else
									null::boolean
							end
								as check_atomic_sub_populations
							---------------------------------------------					
					from
							w14
					)
			,w16 as	(
					select
							w15.id,
							w15.label,
							w15.description,
							w15.label_en,
							w15.description_en,
							w15.id_etl_target_variable,
							w15.check_target_variable4output as check_target_variable,
							w15.refyearset2panel_mapping,
							w15.check_atomic_area_domains,
							w15.check_atomic_sub_populations
					from
							w15
					)
			,w17 as	(
					select
							w16.*,
							target_data.fn_etl_get_target_variable_metadata(w16.id,$2) as metadata
					from
							w16 where '|| _cond_1 ||'
					)
			select
					w17.id,
					w17.label,
					w17.description,
					w17.label_en,
					w17.description_en,
					w17.id_etl_target_variable,
					w17.check_target_variable,
					w17.refyearset2panel_mapping,
					w17.check_atomic_area_domains,
					w17.check_atomic_sub_populations,
					w17.metadata
			from
					w17 order by w17.id;
			'
			using _export_connection, _national_language, _target_variable, _target_variable4etl;
		end if;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) IS
'The function returs target variables and informations for ETL. If input argument _etl = TRUE then function
returns target variables that was ETL and all theirs datas are current and all their atomic types are ETL-forged.
If input argument _etl = FALSE then function returns target variables that was not ETL yet or returns target variables
that was ETL and exists newer datas for ETL or returns target variables that was ETL and all theirs datas are current
but all their atomic types were not ETL-forged. If input argument _etl IS NULL (_etl = true or false) then function
returns all cases.';

grant execute on function target_data.fn_etl_get_target_variable(integer, character varying, boolean, integer) to public;
-- </function>



-- <function name="fn_etl_export_ldsity_values" schema="target_data" src="functions/etl/fn_etl_export_ldsity_values.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_etl_export_ldsity_values
--------------------------------------------------------------------------------

-- DROP FUNCTION IF EXISTS target_data.fn_etl_export_ldsity_values(integer[], integer) CASCADE;

create or replace function target_data.fn_etl_export_ldsity_values
(
	_refyearset2panel_mapping	integer[],
	_id_t_etl_target_variable	integer	
)
returns json
as
$$
declare
		_export_connection			integer;
		_target_variable			integer;
		_etl_id						integer;
		_available_datasets			integer[];
		_categorization_setups		integer[];
		_res_available_datasets		json;
		_res_ldsity_values			json;
		_res						json;

		_core_and_division			boolean;
begin	
		if _refyearset2panel_mapping is null
		then
			raise exception 'Error 01: fn_etl_export_ldsity_values: Input argument _refyearset2panel_mapping must not by NULL!';
		end if;
		
		if _id_t_etl_target_variable is null
		then
			raise exception 'Error 02: fn_etl_export_ldsity_values: Input argument _id_t_etl_target_variable must not by NULL!';
		end if;	

		select
				tetv.export_connection,
				tetv.target_variable,
				tetv.etl_id
		from
				target_data.t_etl_target_variable as tetv
		where
				tetv.id = _id_t_etl_target_variable
		into
				_export_connection,
				_target_variable,
				_etl_id;

		if	(
			select
				count(t.ldsity_object_type) = 1
			from
				(select distinct ldsity_object_type from target_data.cm_ldsity2target_variable cltv where target_variable = _target_variable) as t
			)
		then
			_core_and_division := false;
		else
			_core_and_division := true;
		end if;			


		-------------------------------------------------------------------------------------------
		-- LDSITY VALUES => get available data sets and categorization setups FROM t_ldsity_values
		-------------------------------------------------------------------------------------------
		with
		w1 as	(-- all categorization setups for input target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups reduced by combinations of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(-- list of IDs of available datasets from t_ldsity_values table
				select distinct available_datasets from target_data.t_ldsity_values
				where available_datasets in (select w2.id from w2)
				and is_latest = true
				)
		select array_agg(w3.available_datasets order by w3.available_datasets) from w3
		into _available_datasets;
		
		with
		w4 as	(
				select distinct categorization_setup from target_data.t_available_datasets as tad
				where tad.id in (select unnest(_available_datasets))
				)
		select array_agg(w4.categorization_setup order by w4.categorization_setup) from w4
		into _categorization_setups;
	
		if _categorization_setups is null
		then
			raise exception 'Error 03: fn_etl_export_ldsity_values: Internal argument _categorization_setups must not by NULL!';
		end if;
		-------------------------------------------------------------------------------------------


		-------------------------------------------------------------
		-- AVAILABLE DATASETS => get all available data sets
		-------------------------------------------------------------
		with
		w1 as	(-- all categorization setups for input target variable
				select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
				where ldsity2target_variable in
					(select cm.id from target_data.cm_ldsity2target_variable as cm
					where target_variable = _target_variable
					)
				)
		,w2 as	(-- list of categorization setups reduced by combinations of panels and reference year sets
				select a.*
				from
					(
					select tad.* from target_data.t_available_datasets as tad
					where tad.categorization_setup in (select w1.categorization_setup from w1)
					) as a
				inner
				join
					(
					select cmr.* from sdesign.cm_refyearset2panel_mapping as cmr
					where cmr.id in (select unnest(_refyearset2panel_mapping))
					) as b
				on
					a.panel = b.panel and a.reference_year_set = b.reference_year_set				
				)
		,w3 as	(
				select
						w2.id as id_available_datasets,
						w2.panel,
						w2.reference_year_set,
						w2.ldsity_threshold,
						cmltv.id as id_cm_ldsity2target2categorization_setup,
						cmltv.ldsity2target_variable,
						cmltv.adc2classification_rule,
						cmltv.spc2classification_rule,
						cmltv.categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', cmltv.spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', cmltv.spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', cmltv.adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', cmltv.adc2classification_rule)).id_category as id_adc_orig				
				from
						w2
						inner join target_data.cm_ldsity2target2categorization_setup as cmltv
						on w2.categorization_setup = cmltv.categorization_setup
				)
		,w4 as	(
				select
						w3.*,
						cltv.ldsity_object_type as core_or_division
				from
						w3
						inner join target_data.cm_ldsity2target_variable as cltv on w3.ldsity2target_variable = cltv.id
				)
		,w5 as	(				
				select
						w4.*,
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w4a.id_spt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w4a.id_spc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w4a.id_adt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w4a.id_adc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adc
				from
						w4
				)		
		,w6 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w5
				)
		,w7 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from w6 as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(t.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(t.id_adt,tead.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						case when tespc.etl_id is null then 0 else tespc.etl_id end as etl_id_spc,
						case when teadc.etl_id is null then 0 else teadc.etl_id end as etl_id_adc
				from w7
				
				left join target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc,tespc.sub_population_category)
				
				left join target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc,teadc.area_domain_category)
				)
		,w9 as	(
				select distinct t.t_panel__id, t.t_panel__panel, t.t_stratum__id, t.t_cluster_configuration__id, t.t_cluster_configuration__cluster_configuration,
				t.t_stratum__stratum, t.t_strata_set__id, t.t_strata_set__strata_set, t.c_country__id, t.c_country__label,
				t.t_reference_year_set__id, t.t_reference_year_set__reference_year_set, t.t_inventory_campaign__id, t.t_inventory_campaign__inventory
				from
						(
						select		
								t_panel.id as t_panel__id,
								t_panel.panel as t_panel__panel,
								t_cluster.id as t_cluster__id,
								t_cluster.cluster as t_cluster__cluster,
								f_p_plot.gid as f_p_plot__gid,
								f_p_plot.plot as f_p_plot__plot,
								t_cluster_configuration.id as t_cluster_configuration__id,
								t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
								t_stratum.id as t_stratum__id,
								t_stratum.stratum as t_stratum__stratum,
								t_strata_set.id as t_strata_set__id,
								t_strata_set.strata_set as t_strata_set__strata_set,
								c_country.id as c_country__id,
								c_country.label as c_country__label,
								t_reference_year_set.id as t_reference_year_set__id,
								t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
								t_inventory_campaign.id as t_inventory_campaign__id,
								t_inventory_campaign.inventory as t_inventory_campaign__inventory
						from
									sdesign.cm_refyearset2panel_mapping
						inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
						inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
						inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
						inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
						inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
						inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
						inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
						inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
						inner join	sdesign.c_country on t_strata_set.country = c_country.id
						inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
						inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
						where
								cm_refyearset2panel_mapping.id in (select unnest(_refyearset2panel_mapping))
						) as t
				)
		,w10 as	(
				select
						_etl_id as etl_id_tv,
						w9.c_country__label as country,
						w9.t_strata_set__strata_set as strata_set,
						w9.t_stratum__stratum as stratum,
						w9.t_panel__panel as panel,
						w9.t_reference_year_set__reference_year_set as reference_year_set,
						w9.t_cluster_configuration__cluster_configuration as cluster_configuration,
						w9.t_inventory_campaign__inventory as inventory_campaign,
						w8.etl_id_spc,
						w8.etl_id_adc,
						w5.ldsity_threshold
				from
						w5
				inner join	w8 	on w5.categorization_setup = w8.categorization_setup
				inner join	w9	on w5.panel = w9.t_panel__id
								and w5.reference_year_set = w9.t_reference_year_set__id
				)
		,w11 as	(
				select
						distinct
						w10.etl_id_tv,
						w10.country,
						w10.strata_set,
						w10.stratum,
						w10.panel,
						w10.reference_year_set,
						w10.cluster_configuration,
						w10.inventory_campaign,
						w10.etl_id_spc,
						w10.etl_id_adc,
						w10.ldsity_threshold
				from
						w10
				)		
		,w12 as	(
				select
						w11.etl_id_tv,
						w11.country,
						w11.strata_set,
						w11.stratum,
						w11.reference_year_set,
						w11.panel,
						w11.cluster_configuration,
						w11.inventory_campaign,
						w11.etl_id_spc,
						w11.etl_id_adc,
						w11.ldsity_threshold
				from
						w11
				order
				by		w11.country,
						w11.strata_set,
						w11.stratum,
						w11.reference_year_set,
						w11.panel,
						w11.cluster_configuration,
						w11.inventory_campaign,
						w11.etl_id_tv,
						w11.etl_id_spc,
						w11.etl_id_adc 
				)
		select
			json_agg(json_build_object(
				'target_variable',   		w12.etl_id_tv,
				'country',					w12.country,
				'strata_set',				w12.strata_set,
				'stratum',					w12.stratum,
				'reference_year_set',		w12.reference_year_set,
				'panel',					w12.panel,
				'cluster_configuration',	w12.cluster_configuration,
				'inventory_campaign',		w12.inventory_campaign,
				'sub_population_category',	w12.etl_id_spc,
				'area_domain_category',		w12.etl_id_adc,
				'ldsity_threshold',			w12.ldsity_threshold
				))
		from
				w12 into _res_available_datasets;
		-------------------------------------------------------------


		-------------------------------------------------------------
		-- LDSITY VALUES => get ldsity values
		-------------------------------------------------------------
		with
		w1 as	(
				select
						tad.id as id_available_datasets,
						tad.panel,
						tad.reference_year_set,
						tad.categorization_setup
				from
						target_data.t_available_datasets as tad
				where
						tad.id in (select unnest(_available_datasets))
				)
		,w2 as	(
				select t1.*, w1.*
				from target_data.t_ldsity_values as t1
				inner join w1 on t1.available_datasets = w1.id_available_datasets
				where t1.is_latest = true
				)
		,w3 as	(
				select
						id as id_cm_ldsity2target2categorization_setup,
						ldsity2target_variable,
						adc2classification_rule,
						spc2classification_rule,
						categorization_setup,
						_core_and_division as core_and_division,
						(target_data.fn_get_category_type4classification_rule_id('spc', spc2classification_rule)).id_type as id_spt_orig,
						(target_data.fn_get_category4classification_rule_id('spc', spc2classification_rule)).id_category as id_spc_orig,			
						(target_data.fn_get_category_type4classification_rule_id('adc', adc2classification_rule)).id_type as id_adt_orig,
						(target_data.fn_get_category4classification_rule_id('adc', adc2classification_rule)).id_category as id_adc_orig				
				from
						target_data.cm_ldsity2target2categorization_setup
				where
						categorization_setup in (select unnest(_categorization_setups))
				)
		,w4 as	(
				select
						w3.*,
						cltv.ldsity_object_type as core_or_division
				from
						w3
						inner join target_data.cm_ldsity2target_variable as cltv on w3.ldsity2target_variable = cltv.id
				)
		,w5 as	(				
				select
						w4.*,
						case
							when core_and_division = false
								then id_spt_orig
								else
									case
										when core_or_division = 200
										then id_spt_orig
										else (select distinct w4a.id_spt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spt,
						
						case
							when core_and_division = false
								then id_spc_orig
								else
									case
										when core_or_division = 200
										then id_spc_orig
										else (select distinct w4a.id_spc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 200)
									end
						end as id_spc,
						
						case
							when core_and_division = false
								then id_adt_orig
								else
									case
										when core_or_division = 100
										then id_adt_orig
										else (select distinct w4a.id_adt_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adt,
						
						case
							when core_and_division = false
								then id_adc_orig
								else
									case
										when core_or_division = 100
										then id_adc_orig
										else (select distinct w4a.id_adc_orig from w4 as w4a where w4a.categorization_setup = w4.categorization_setup and w4a.core_or_division = 100)
									end
						end as id_adc
				from
						w4
				)
		,w6 as	(
				select distinct categorization_setup, id_spt, id_spc, id_adt, id_adc from w5
				)
		,w7 as	(
				select
						t.*,
						tesp.id as id_sub_population,
						tead.id as id_area_domain
						
				from w6 as t
				
				left join	(
							select * from target_data.t_etl_sub_population
							where export_connection = _export_connection
							) as tesp
				on target_data.fn_etl_array_compare(t.id_spt,tesp.sub_population)
				
				left join	(
							select * from target_data.t_etl_area_domain
							where export_connection = _export_connection
							) as tead
				on target_data.fn_etl_array_compare(t.id_adt,tead.area_domain)
				)
		,w8 as	(
				select
						w7.*,
						case when tespc.etl_id is null then 0 else tespc.etl_id end as etl_id_spc,
						case when teadc.etl_id is null then 0 else teadc.etl_id end as etl_id_adc
				from w7
				
				left join target_data.t_etl_sub_population_category as tespc
				on  w7.id_sub_population = tespc.etl_sub_population
				and target_data.fn_etl_array_compare(w7.id_spc,tespc.sub_population_category)
				
				left join target_data.t_etl_area_domain_category as teadc
				on  w7.id_area_domain = teadc.etl_area_domain
				and target_data.fn_etl_array_compare(w7.id_adc,teadc.area_domain_category)
				)
		,w9 as	(
				select		
						t_panel.id as t_panel__id,
						t_panel.panel as t_panel__panel,
						t_cluster.id as t_cluster__id,
						t_cluster.cluster as t_cluster__cluster,
						f_p_plot.gid as f_p_plot__gid,
						f_p_plot.plot as f_p_plot__plot,
						t_cluster_configuration.id as t_cluster_configuration__id,
						t_cluster_configuration.cluster_configuration as t_cluster_configuration__cluster_configuration,
						t_stratum.id as t_stratum__id,
						t_stratum.stratum as t_stratum__stratum,
						t_strata_set.id as t_strata_set__id,
						t_strata_set.strata_set as t_strata_set__strata_set,
						c_country.id as c_country__id,
						c_country.label as c_country__label,
						t_reference_year_set.id as t_reference_year_set__id,
						t_reference_year_set.reference_year_set as t_reference_year_set__reference_year_set,
						t_inventory_campaign.id as t_inventory_campaign__id,
						t_inventory_campaign.inventory as t_inventory_campaign__inventory
				from
							sdesign.cm_refyearset2panel_mapping
				inner join	sdesign.t_panel on t_panel.id = cm_refyearset2panel_mapping.panel
				inner join	sdesign.cm_cluster2panel_mapping on cm_cluster2panel_mapping.panel = t_panel.id
				inner join	sdesign.t_cluster on t_cluster.id = cm_cluster2panel_mapping."cluster"
				inner join	sdesign.f_p_plot on f_p_plot."cluster" = t_cluster.id
				inner join	sdesign.cm_plot2cluster_config_mapping on f_p_plot.gid = cm_plot2cluster_config_mapping.plot
				inner join	sdesign.t_cluster_configuration on (t_panel.cluster_configuration = t_cluster_configuration.id and cm_plot2cluster_config_mapping.cluster_configuration = t_cluster_configuration.id)
				inner join	sdesign.t_stratum on t_panel.stratum = t_stratum.id
				inner join	sdesign.t_strata_set on t_stratum.strata_set = t_strata_set.id
				inner join	sdesign.c_country on t_strata_set.country = c_country.id
				inner join	sdesign.t_reference_year_set on cm_refyearset2panel_mapping.reference_year_set = t_reference_year_set.id
				inner join	sdesign.t_inventory_campaign on t_reference_year_set.inventory_campaign = t_inventory_campaign.id
				where
						cm_refyearset2panel_mapping.id in (select unnest(_refyearset2panel_mapping))
				)
		,w10 as	(
				select
						_etl_id as etl_id_tv,
						w9.c_country__label as country,
						w9.t_strata_set__strata_set as strata_set,
						w9.t_stratum__stratum as stratum,
						w9.t_panel__panel as panel,
						w9.t_reference_year_set__reference_year_set as reference_year_set,
						w9.t_cluster__cluster as cluster,
						w9.t_cluster_configuration__cluster_configuration as cluster_configuration,
						w9.t_inventory_campaign__inventory as inventory_campaign,
						w9.f_p_plot__plot as plot,
						w8.etl_id_spc,
						w8.etl_id_adc,
						w2.value
				from
						w2
				inner join	w8 	on w2.categorization_setup = w8.categorization_setup
				inner join	w9	on w2.plot = w9.f_p_plot__gid
								and w2.panel = w9.t_panel__id
								and w2.reference_year_set = w9.t_reference_year_set__id
				)
		,w11 as	(
				select
						w10.etl_id_tv,
						w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster,
						w10.cluster_configuration,
						w10.inventory_campaign,						
						w10.plot,
						w10.etl_id_spc,
						w10.etl_id_adc,
						w10.value
				from
						w10
				order
				by		w10.country,
						w10.strata_set,
						w10.stratum,
						w10.reference_year_set,
						w10.panel,
						w10.cluster,
						w10.cluster_configuration,
						w10.inventory_campaign,
						w10.etl_id_tv,
						w10.plot,
						w10.etl_id_spc,
						w10.etl_id_adc 
				)
		select
			json_agg(json_build_object(
				'target_variable',   		w11.etl_id_tv,
				'country',					w11.country,
				'strata_set',				w11.strata_set,
				'stratum',					w11.stratum,
				'reference_year_set',		w11.reference_year_set,
				'panel',					w11.panel,
				'cluster',					w11.cluster,
				'cluster_configuration',	w11.cluster_configuration,
				'inventory_campaign',		w11.inventory_campaign,
				'plot',						w11.plot,
				'sub_population_category',	w11.etl_id_spc,
				'area_domain_category',		w11.etl_id_adc,
				'value',					w11.value))
		from
				w11 into _res_ldsity_values;
		-------------------------------------------------------------


		-------------------------------------------------------------
		-- JOIN elements
		-------------------------------------------------------------
		select json_build_object
			(
				'available_datasets',_res_available_datasets,
				'ldsity_values',_res_ldsity_values
			)
		into _res;
		-------------------------------------------------------------
			
		return _res;
end;
$$
language plpgsql
volatile
cost 100
security invoker;

comment on function target_data.fn_etl_export_ldsity_values(integer[], integer) is
'Function returns records for ETL t_target_data and t_available_datasets table.';

grant execute on function target_data.fn_etl_export_ldsity_values(integer[], integer) to public;
-- </function>