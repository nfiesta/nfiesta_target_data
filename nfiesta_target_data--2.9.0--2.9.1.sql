--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

drop trigger trg__available_datasets__ins ON target_data.t_available_datasets;
drop trigger trg__available_datasets__upd ON target_data.t_available_datasets;

-- <function name="fn_check_t_target_data" schema="target_data" src="functions/fn_check_t_ldsity_values.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_check_t_ldsity_values
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_check_t_ldsity_values();

CREATE OR REPLACE FUNCTION target_data.fn_check_t_ldsity_values() RETURNS TRIGGER AS $src$
    DECLARE
		_cs int[];
		_vars int[];
		_plots int[];
		_refyearsets int[];
		_available_datasets int[];
		_errp json;
    BEGIN
        IF (TG_OP = 'DELETE' or TG_OP = 'UPDATE' or TG_OP = 'INSERT') THEN
			IF (TG_TABLE_NAME = 't_ldsity_values') THEN
				with w_vars as (
						select array_prepend(node, edges) as vs 
					from trans_table
					inner join target_data.t_available_datasets on trans_table.available_datasets = t_available_datasets.id
					inner join target_data.v_variable_hierarchy 
						on ((t_available_datasets.categorization_setup = v_variable_hierarchy.node 
							or t_available_datasets.categorization_setup = any (v_variable_hierarchy.edges))
						and (t_available_datasets.panel = v_variable_hierarchy.panel
							and t_available_datasets.reference_year_set = v_variable_hierarchy.reference_year_set)
					)
				)
				, w_vars_un as (
						select unnest(vs) as v from w_vars
				)
				select array_agg(distinct v) into _vars from w_vars_un;
				select array_agg(distinct categorization_setup) into _cs from trans_table inner join target_data.t_available_datasets on trans_table.available_datasets = t_available_datasets.id;
				select array_agg(distinct plot) into _plots from trans_table;
				select array_agg(distinct reference_year_set) into _refyearsets from target_data.t_available_datasets where id in (select distinct available_datasets from trans_table);
				select array_agg(distinct available_datasets) into _available_datasets from trans_table;
			ELSIF (TG_TABLE_NAME = 't_available_datasets') THEN
                                with w_vars as (
                                                select array_prepend(node, edges) as vs
                                        from trans_table
                                        inner join target_data.v_variable_hierarchy
                                                on ((trans_table.categorization_setup = v_variable_hierarchy.node
                                                        or trans_table.categorization_setup = any (v_variable_hierarchy.edges))
						and (trans_table.panel = v_variable_hierarchy.panel
							and trans_table.reference_year_set = v_variable_hierarchy.reference_year_set)
					)
                                )
                                , w_vars_un as (
                                                select unnest(vs) as v from w_vars
                                )
                                select array_agg(distinct v) into _vars from w_vars_un;
				select array_agg(distinct categorization_setup) into _cs from trans_table;
				select array_agg(distinct f_p_plot.gid) into _plots
					from trans_table
					inner join sdesign.t_panel on (trans_table.panel = t_panel.id)
					inner join sdesign.cm_cluster2panel_mapping on (t_panel.id = cm_cluster2panel_mapping.panel)
					inner join sdesign.t_cluster on (cm_cluster2panel_mapping.cluster = t_cluster.id)
					inner join sdesign.f_p_plot on (t_cluster.id = f_p_plot.cluster);
				select array_agg(distinct reference_year_set) into _refyearsets from target_data.t_available_datasets where id in (select distinct id from trans_table);
				select array_agg(distinct id) into _available_datasets from trans_table;
			ELSE
				RAISE EXCEPTION 'fn_check_t_ldsity_values -- % -- table name not known: %', TG_OP, TG_TABLE_NAME;
			END IF;

			if _vars is not null
			then
				--raise notice '%		fn_check_t_ldsity_values -- UPDATE CHECK START -- select target_data.fn_add_plot_target_attr(%, %, %, %, %);', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague', _vars, _plots, _refyearsets, 1e-6, true;
				with w_err as (
					select target_data.fn_add_plot_target_attr(_vars, _plots, _refyearsets, 1e-6, true) as fn_err
				)
				, w_err_t as (
					select (fn_err).* from w_err
				)
				, w_err_json as (
					select 
						json_build_object(
							'plot'					, plot,
							'reference_year_set'	, reference_year_set,
							'variable'				, variable,
							'ldsity'				, ldsity,
							'ldsity_sum'			, ldsity_sum,
							'variables_def'			, variables_def,
							'variables_found'		, variables_found,
							'diff'					, diff
						) as errj
					from w_err_t
				)
				select json_agg(errj) into _errp from w_err_json;
				--raise notice '%		fn_check_t_ldsity_values -- UPDATE CHECK STOP', clock_timestamp()::timestamp with time zone at time zone 'Europe/Prague';
				IF 
					(json_array_length(_errp) > 0) 
					THEN RAISE EXCEPTION 'fn_check_t_ldsity_values -- % -- % -- plot level local densities are not additive:
						checked categorization setups: %, variables in additivity set: %, checked plots: %, 
						found err plots: 
						%', TG_TABLE_NAME, TG_OP, _cs, _vars, _plots, jsonb_pretty(_errp::jsonb);
				END IF;
			else
				RAISE WARNING 'fn_check_t_ldsity_values -- % -- % -- additivity check was skiped: 
						* 0 rows edited (next line displays NULL)
						* corresponding variable hierarchy not found (next line displays array)
						trans_table available_datasets: %', TG_TABLE_NAME, TG_OP, _available_datasets;
			end if;
	ELSE
		RAISE EXCEPTION 'fn_check_t_ldsity_values -- trigger operation not known: %', TG_OP;
	END IF;
        RETURN NULL; -- result is ignored since this is an AFTER trigger
    END;
$src$ LANGUAGE plpgsql;

comment on function target_data.fn_check_t_ldsity_values IS 'Trigger function for checking additivity. Launched by triggers on either t_ldsity_values or t_available_datasets. Triggers on insert and update on t_available_datasets dropped due to performance problem. Will be replaced by more advanced concept of additivity checks in https://gitlab.com/nfiesta/nfiesta_pg/-/issues/133.';

drop trigger if exists trg__ldsity_values__ins ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__ins
    AFTER INSERT ON target_data.t_ldsity_values
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__ldsity_values__upd ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__upd
    AFTER UPDATE ON target_data.t_ldsity_values
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__ldsity_values__del ON target_data.t_ldsity_values;
CREATE TRIGGER trg__ldsity_values__del
    AFTER DELETE ON target_data.t_ldsity_values
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

/*
triggers dropped due to prefromance problem
drop trigger if exists trg__available_datasets__ins ON target_data.t_available_datasets;
CREATE TRIGGER trg__available_datasets__ins
    AFTER INSERT ON target_data.t_available_datasets
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

drop trigger if exists trg__available_datasets__upd ON target_data.t_available_datasets;
CREATE TRIGGER trg__available_datasets__upd
    AFTER UPDATE ON target_data.t_available_datasets
    REFERENCING NEW TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();
*/

drop trigger if exists trg__available_datasets__del ON target_data.t_available_datasets;
CREATE TRIGGER trg__available_datasets__del
    AFTER DELETE ON target_data.t_available_datasets
    REFERENCING OLD TABLE AS trans_table
    FOR EACH STATEMENT EXECUTE FUNCTION target_data.fn_check_t_ldsity_values();

-- </function>
