--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--



drop trigger if exists trg_t_available_datasets_after_update ON target_data.t_available_datasets;
drop trigger if exists trg_t_available_datasets_after_insert ON target_data.t_available_datasets;
drop function if exists target_data.fn_trg_check_t_available_datasets();



-- <function name="fn_trg_check_t_available_datasets" schema="target_data" src="functions/fn_trg_check_t_available_datasets.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_t_available_datasets
---------------------------------------------------------------------------------------------------

-- drop function target_data.fn_trg_check_t_available_datasets();

create or replace function target_data.fn_trg_check_t_available_datasets()
returns trigger as 
$$
declare
	_target_variable_check	integer;
	_target_variable		integer;
	_categorization_setups	integer[];
	_check_1				integer;
	_check_2				integer;
begin
	if (TG_OP = 'INSERT') or (TG_OP = 'UPDATE')
	then
		if (select count(*) > 0 from trans_table)
		then
			select count(t.*) from
			(
			select distinct target_variable from target_data.cm_ldsity2target_variable where id in
			(select ldsity2target_variable from target_data.cm_ldsity2target2categorization_setup
			where categorization_setup in (select categorization_setup from trans_table))
			) as t
			into _target_variable_check;
			---------------------------------------------------------
			if _target_variable_check is distinct from 1
			then
				raise exception 'Error: 01: fn_trg_check_t_available_datasets: Check is activated for more (count = %) target variables than one!',_target_variable_check;
			end if;
			---------------------------------------------------------
			select distinct target_variable from target_data.cm_ldsity2target_variable where id in
			(select ldsity2target_variable from target_data.cm_ldsity2target2categorization_setup
			where categorization_setup in (select categorization_setup from trans_table))
			into _target_variable;

			with
			w1 as	(
					select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup
					where ldsity2target_variable in (select id from target_data.cm_ldsity2target_variable where target_variable = _target_variable)
					)
			select array_agg(w1.categorization_setup) from w1
			into _categorization_setups;
			---------------------------------------------------------
			-- check unique ldsity_threshold --
			with
			w1 as	(
					select
							distinct panel,	reference_year_set,	ldsity_threshold
					from
							target_data.t_available_datasets
					where
							categorization_setup in (select unnest(_categorization_setups))
					)
			,w2 as	(
					select
							w1.panel,
							w1.reference_year_set,
							count(w1.ldsity_threshold) as ldsity_threshold_count
					from
							w1
					group
							by w1.panel, w1.reference_year_set
					)
			select count(w2.*) from w2 where w2.ldsity_threshold_count > 1
			into _check_1;

			if _check_1 > 0
			then
				raise exception 'Error: 02: fn_trg_check_t_available_datasets: For target_variable = % not exists the equal value ldsity_threshold for some combination of panel and reference year set in t_available_datasets table!', _target_variable;
			end if;
			---------------------------------------------------------
			-- check unique last_change --
			with
			w1 as	(
					select
							distinct panel,	reference_year_set,	last_change
					from
							target_data.t_available_datasets
					where
							categorization_setup in (select unnest(_categorization_setups))
					)
			,w2 as	(
					select
							w1.panel,
							w1.reference_year_set,
							count(w1.last_change) as last_change_count
					from
							w1
					group
							by w1.panel, w1.reference_year_set
					)
			select count(w2.*) from w2 where w2.last_change_count > 1
			into _check_2;

			if _check_2 > 0
			then
				raise exception 'Error: 03: fn_trg_check_t_available_datasets: For target_variable = % not exists the equal value last_change for some combination of panel and reference year set in t_available_datasets table!', _target_variable;
			end if;
			---------------------------------------------------------
		end if;
	end if;

	return null;
end;
$$
language plpgsql;

comment on function target_data.fn_trg_check_t_available_datasets() is
'This trigger function controls that for combination of panel, reference_year_set and target_variable exists the equal value ldsity_threshold and exists the equal value last_change in t_available_datasets table';

grant execute on function target_data.fn_trg_check_t_available_datasets() to public;
-- </function>


--target_data.t_available_datasets
drop trigger if exists trg_t_available_datasets_after_update ON target_data.t_available_datasets;
create trigger trg_t_available_datasets_after_update
after update on target_data.t_available_datasets
referencing new table as trans_table
for each statement execute procedure target_data.fn_trg_check_t_available_datasets();

drop trigger if exists trg_t_available_datasets_after_insert ON target_data.t_available_datasets;
create trigger trg_t_available_datasets_after_insert
after insert on target_data.t_available_datasets
referencing new table as trans_table
for each statement execute procedure target_data.fn_trg_check_t_available_datasets();
