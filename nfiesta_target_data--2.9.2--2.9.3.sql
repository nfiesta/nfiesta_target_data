--
-- Copyright 2023 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--


-- <function name="fn_get_target_variable" schema="target_data" src="functions/fn_get_target_variable.sql">
--
-- Copyright 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

--------------------------------------------------------------------------------
-- fn_get_target_variable
--------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_get_target_variable(integer) CASCADE;

CREATE OR REPLACE FUNCTION target_data.fn_get_target_variable(_ldsity_object_group integer DEFAULT NULL::integer)
RETURNS TABLE (
id			integer[],
ldsity_object_group	integer,
label			character varying(200),
description		text,
label_en		character varying(200),
description_en		text,
state_or_change		integer,
soc_label		character varying(200),
soc_description		text,
areal_or_population	integer
)
AS
$$
BEGIN
	IF _ldsity_object_group IS NULL
	THEN
		RETURN QUERY
		WITH 
		w_objects AS (
			SELECT t2.ldsity_object_group, array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			GROUP BY t2.ldsity_object_group
			),
		w_core AS (
			SELECT 	t5.target_variable, 
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = false) AS ldsity_objects_pozit,
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = true) AS ldsity_objects_negat
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		),
		w_target_variable AS (
			SELECT 	t2.ldsity_object_group, t1.target_variable, t3.label, t3.description, t3.label_en, t3.description_en, t3.state_or_change, 
				t4.label AS soc_label, t4.description AS soc_description
			FROM w_core AS t1
			INNER JOIN w_objects AS t2
			ON t2.ldsity_objects = t1.ldsity_objects_pozit AND CASE WHEN t1.ldsity_objects_negat IS NOT NULL THEN t2.ldsity_objects = t1.ldsity_objects_negat ELSE true END
			INNER JOIN target_data.c_target_variable AS t3
			ON t1.target_variable = t3.id
			INNER JOIN target_data.c_state_or_change AS t4
			ON t3.state_or_change = t4.id
		),
		w_agg_200 AS (
			SELECT 	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, 
				t2.version,
				t5.areal_or_population,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = false) AS ldsity_pozit,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = true) AS ldsity_negat,
				count(*) OVER (PARTITION BY t1.target_variable) AS no_of_contrib
			FROM w_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.target_variable = t2.target_variable AND t2.ldsity_object_type = 100
			INNER JOIN target_data.c_ldsity AS t4
			ON t2.ldsity = t4.id
			INNER JOIN target_data.c_ldsity_object AS t5
			ON t4.ldsity_object = t5.id
			LEFT JOIN target_data.cm_ldsity2target_variable AS t3
			ON t1.target_variable = t3.target_variable AND t3.ldsity_object_type = 200 AND t2.use_negative = t3.use_negative
			GROUP BY t1.target_variable, t1.ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, t2.version, t5.areal_or_population
		), w_agg_id AS (
			-- query with one contribution which can be divided by other contributions
			(SELECT array_agg(t1.target_variable) AS ids, t1.ldsity_object_group,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib = 1
			GROUP BY t1.ldsity, t1.ldsity_object_group, 
				t1.area_domain_category, t1.sub_population_category,
				t1.state_or_change, t1.soc_label, t1.soc_description, t1.areal_or_population,
				t1.ldsity_object_type, CASE WHEN t1.state_or_change != 200 THEN t1.version END
			)
			UNION ALL
			-- query with more than one contribution, can not be divided
			(SELECT array_agg(DISTINCT t1.target_variable) AS ids, t1.ldsity_object_group, 
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib > 1
			GROUP BY t1.ldsity_object_group, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type, CASE WHEN t1.state_or_change != 200 THEN t1.version END
			)
		)
		SELECT ids AS id, t1.ldsity_object_group, t1.label[1], t1.description[1], t1.label_en[1], t1.description_en[1],
			t1.state_or_change, t1.soc_label, t1.soc_description,
			t1.areal_or_population[1]
		FROM w_agg_id AS t1;
	ELSE
		IF NOT EXISTS (SELECT * FROM target_data.c_ldsity_object_group AS t1 WHERE t1.id = _ldsity_object_group)
		THEN RAISE EXCEPTION 'Given ldsity object group does not exist in table c_ldsity_object_group (%)', _ldsity_object_group;
		END IF;

		RETURN QUERY
		WITH 
		w_objects AS (
			SELECT t2.ldsity_object_group, array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects
			FROM target_data.c_ldsity_object_group AS t1
			INNER JOIN target_data.cm_ld_object2ld_object_group AS t2
			ON t1.id = t2.ldsity_object_group
			WHERE t1.id = _ldsity_object_group
			GROUP BY t2.ldsity_object_group
			),
		w_core AS (
			SELECT 	t5.target_variable,
 				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = false) AS ldsity_objects_pozit,
				array_agg(t4.ldsity_object ORDER BY ldsity_object) FILTER (WHERE t5.use_negative = true) AS ldsity_objects_negat
			FROM target_data.c_ldsity AS t4
			INNER JOIN target_data.cm_ldsity2target_variable AS t5
			ON t4.id = t5.ldsity
			WHERE t5.ldsity_object_type = 100
			GROUP BY target_variable
		),
		w_target_variable AS (
			SELECT 	t2.ldsity_object_group, t1.target_variable, t3.label, t3.description, t3.label_en, t3.description_en, t3.state_or_change, 
				t4.label AS soc_label, t4.description AS soc_description
			FROM w_core AS t1
			INNER JOIN w_objects AS t2
			ON t2.ldsity_objects = t1.ldsity_objects_pozit AND CASE WHEN t1.ldsity_objects_negat IS NOT NULL THEN t2.ldsity_objects = t1.ldsity_objects_negat ELSE true END
			INNER JOIN target_data.c_target_variable AS t3
			ON t1.target_variable = t3.id
			INNER JOIN target_data.c_state_or_change AS t4
			ON t3.state_or_change = t4.id
		),
		w_agg_200 AS (
			SELECT 	t1.ldsity_object_group, t1.target_variable, t1.label, t1.description, t1.label_en, t1.description_en,
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category,
				t2.version,
				t5.areal_or_population,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = false) AS ldsity_pozit,
				array_agg(t3.ldsity ORDER BY t3.ldsity) FILTER (WHERE t3.ldsity IS NOT NULL AND t3.use_negative = true) AS ldsity_negat,
				count(*) OVER (PARTITION BY t1.target_variable) AS no_of_contrib
			FROM w_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.target_variable = t2.target_variable AND t2.ldsity_object_type = 100
			INNER JOIN target_data.c_ldsity AS t4
			ON t2.ldsity = t4.id
			INNER JOIN target_data.c_ldsity_object AS t5
			ON t4.ldsity_object = t5.id
			LEFT JOIN target_data.cm_ldsity2target_variable AS t3
			ON t1.target_variable = t3.target_variable AND t3.ldsity_object_type = 200 AND t2.use_negative = t3.use_negative
			GROUP BY t1.target_variable, t1.ldsity_object_group, t1.label, t1.description, t1.label_en, t1.description_en, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t2.ldsity_object_type, t2.ldsity, t2.area_domain_category, t2.sub_population_category, t2.version, t5.areal_or_population
		), w_agg_id AS (
			-- query with one contribution which can be divided by other contributions
			(SELECT array_agg(t1.target_variable) AS ids, t1.ldsity_object_group,
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description
			FROM w_agg_200 AS t1
			WHERE no_of_contrib = 1
			GROUP BY t1.ldsity, t1.ldsity_object_group, 
				t1.area_domain_category, t1.sub_population_category,
				t1.state_or_change, t1.soc_label, t1.soc_description, t1.areal_or_population,
				t1.ldsity_object_type, CASE WHEN t1.state_or_change != 200 THEN t1.version END
			)
			UNION ALL
			-- query with more than one contribution, can not be divided
			(SELECT array_agg(DISTINCT t1.target_variable) AS ids, t1.ldsity_object_group, 
				array_agg(t1.label ORDER BY t1.target_variable) AS label, 
				array_agg(t1.description ORDER BY t1.target_variable) AS description, 
				array_agg(t1.label_en ORDER BY t1.target_variable) AS label_en, 
				array_agg(t1.description_en ORDER BY t1.target_variable) AS description_en,
				array_agg(t1.areal_or_population ORDER BY t1.target_variable) AS areal_or_population,
				t1.state_or_change, t1.soc_label, t1.soc_description 
			FROM w_agg_200 AS t1
			WHERE no_of_contrib > 1
			GROUP BY t1.ldsity_object_group, 
				t1.state_or_change, t1.soc_label, t1.soc_description,
				t1.ldsity_object_type, CASE WHEN t1.state_or_change != 200 THEN t1.version END
			)
		)
		SELECT ids AS id, t1.ldsity_object_group, t1.label[1], t1.description[1], t1.label_en[1], t1.description_en[1],
			t1.state_or_change, t1.soc_label, t1.soc_description,
			t1.areal_or_population[1]
		FROM w_agg_id AS t1;
	END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE
COST 100
SECURITY INVOKER;

COMMENT ON FUNCTION target_data.fn_get_target_variable(integer) IS
'Function returns records from c_target_variable table, optionally for given ldsity_object_group.';

GRANT EXECUTE ON FUNCTION target_data.fn_get_target_variable(integer) TO public;

-- </function>

-- <function name="fn_trg_check_cm_ldsity2target_variable" schema="target_data" src="functions/fn_trg_check_cm_ldsity2target_variable.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

---------------------------------------------------------------------------------------------------
-- fn_trg_check_cm_ldsity2target_variable
---------------------------------------------------------------------------------------------------

-- DROP FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable();

CREATE OR REPLACE FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() 
RETURNS TRIGGER AS 
$$
DECLARE
_test1		boolean;
_test2		boolean;
_test3		boolean;
_test4		boolean;
_variables 	integer[];
_use_negative	boolean[];
_100100		integer[];
_100200		integer[];
_200100		integer[];
_200200		integer[];
_total_eq	integer;
_id		integer;
_pozitive_objects integer[];
_negative_objects integer[];
BEGIN

	-- check if already exists
	WITH w_new AS (
		SELECT  t1.target_variable,
			array_agg(t1.ldsity ORDER BY ldsity, use_negative) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY ldsity, use_negative) AS ldsity_object_type, 
			array_agg(t1.use_negative ORDER BY ldsity, use_negative) AS use_negative, 
			array_agg(t1.version ORDER BY ldsity, use_negative) AS version,
			t1.area_domain_category AS adc,
			t1.sub_population_category AS spc
		FROM new_table AS t1
		GROUP BY target_variable, t1.area_domain_category, t1.sub_population_category
	), w_existing AS (
		SELECT  t1.target_variable,
			array_agg(t1.ldsity ORDER BY ldsity, use_negative) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY ldsity, use_negative) AS ldsity_object_type, 
			array_agg(t1.use_negative ORDER BY ldsity, use_negative) AS use_negative, 
			array_agg(t1.version ORDER BY ldsity, use_negative) AS version,
			t1.area_domain_category AS adc,
			t1.sub_population_category AS spc
		FROM (SELECT * FROM target_data.cm_ldsity2target_variable EXCEPT SELECT * FROM new_table) AS t1
		GROUP BY target_variable, t1.area_domain_category, t1.sub_population_category
	)
	, w_comp AS (
		SELECT t2.target_variable
		FROM w_new AS t1
		INNER JOIN w_existing AS t2
		ON 	coalesce(t1.ldsity,array[0]) = coalesce(t2.ldsity,array[0]) AND
		 	coalesce(t1.ldsity_object_type,array[0]) = coalesce(t2.ldsity_object_type,array[0]) AND
		 	coalesce(t1.use_negative,array[NULL::boolean]) = coalesce(t2.use_negative,array[NULL::boolean]) AND
		 	coalesce(t1.version,array[0]) = coalesce(t2.version,array[0]) AND
		 	coalesce(t1.adc,array[0]) = coalesce(t2.adc,array[0]) AND
		 	coalesce(t1.spc,array[0]) = coalesce(t2.spc,array[0])
	)
	SELECT count(*) FROM w_comp
	INTO _total_eq;

--raise notice '%', _total_eq;

	--_id := (SELECT max(id) 
	--	FROM 	(SELECT * FROM target_data.cm_ldsity2target_variable EXCEPT SELECT * FROM new_table) AS t1);

	IF _total_eq IS NOT NULL AND _total_eq > 0
	THEN 
		--PERFORM pg_catalog.setval('target_data.cm_ldsity2target_variable_id_seq', _id, true);
		RAISE EXCEPTION 'Some of inserted/udated target variables already exists in table target_data.cm_ldsity2target_variable. This means that given combination of ldsitys is already assigned to some target variable.';
	END IF;

	-- after insert on cm_ldsity2target_variable
	WITH w_new AS (
		SELECT 	
			t2.id, t2.label, t2.description, t2.label_en, t2.description_en,
			array_agg(t1.ldsity ORDER BY t1.ldsity) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY t1.ldsity) AS ldsity_object_type,
			array_agg(t1.use_negative ORDER BY t1.ldsity) AS use_negative
		FROM 
			new_table AS t1
		INNER JOIN
			target_data.c_target_variable AS t2
		ON t1.target_variable = t2.id
		WHERE t1.ldsity_object_type = 100
		GROUP BY t2.id, t2.label, t2.description, t2.label_en, t2.description
	),
	w_existing AS (
		SELECT 	
			t2.id, t2.label, t2.description, t2.label_en, t2.description_en,
			array_agg(t1.ldsity ORDER BY t1.ldsity) AS ldsity, 
			array_agg(t1.ldsity_object_type ORDER BY t1.ldsity) AS ldsity_object_type,
			array_agg(t1.use_negative ORDER BY t1.ldsity) AS use_negative
		FROM 
			(SELECT * FROM target_data.cm_ldsity2target_variable EXCEPT SELECT * FROM new_table) AS t1
		INNER JOIN
			target_data.c_target_variable AS t2
		ON t1.target_variable = t2.id
		WHERE t1.ldsity_object_type = 100
		GROUP BY t2.id, t2.label, t2.description, t2.label_en, t2.description
	)
	SELECT
		t1.label = t2.label AS label_test, t1.description = t2.description AS desc_test,
		t1.label_en = t2.label_en AS label_en_test, t1.description_en = t2.description_en AS desc_en_test
	FROM
		w_new AS t1
	INNER JOIN
		w_existing AS t2
	ON t1.ldsity = t2.ldsity AND t1.use_negative = t2.use_negative
	INTO _test1, _test2, _test3, _test4;

	-- if the join on the same ldsity with ldsity_object_type = 100 results in not null output
	-- it means, there already exists target variable with given 100 ldsitys (both constrained and non-constrained)

	IF _test1 IS NOT NULL
	THEN
		-- then we can test the equality
		IF _test1 = false OR _test2 = false OR _test3 = false OR _test4 = false
		THEN
			RAISE EXCEPTION 'Newly inserted target variable has to respect the label (%) and description (%), label_en (%) and description_en (%) respectively, of existing target variables with the same local density contributions (of ldsity_object_type = 100).', _test1, _test2, _test3, _test4;
		END IF;
	END IF; 

	-- check if correct combination of ldsity_object_types is choosen
	WITH 
	w_tv AS (SELECT distinct target_variable, use_negative
		FROM new_table
	),
	w_type AS (
		SELECT t1.target_variable, t1.ldsity_object_type, t3.areal_or_population, t1.use_negative, count(*) AS total
		FROM new_table AS t1
		INNER JOIN target_data.c_ldsity AS t2
		ON t1.ldsity = t2.id
		INNER JOIN target_data.c_ldsity_object AS t3
		ON t2.ldsity_object = t3.id
		GROUP BY t1.target_variable, t1.ldsity_object_type, t3.areal_or_population, t1.use_negative
	),
	w_total AS (
		SELECT 	t1.target_variable AS target_variable, t1.use_negative,
			coalesce(t2.total,0) AS total_100100, coalesce(t3.total,0) AS total_100200,
			coalesce(t4.total,0) AS total_200100, coalesce(t5.total,0) AS total_200200
		FROM
			w_tv AS t1
		LEFT JOIN w_type AS t2
		ON 	t1.target_variable = t2.target_variable AND t1.use_negative = t2.use_negative
			AND t2.ldsity_object_type = 100 AND t2.areal_or_population = 100
		LEFT JOIN w_type AS t3
		ON 	t1.target_variable = t3.target_variable AND t1.use_negative = t3.use_negative
			AND t3.ldsity_object_type = 100 AND t3.areal_or_population = 200
		LEFT JOIN w_type AS t4
		ON 	t1.target_variable = t4.target_variable AND t1.use_negative = t4.use_negative
			AND t4.ldsity_object_type = 200 AND t4.areal_or_population = 100
		LEFT JOIN w_type AS t5
		ON 	t1.target_variable = t5.target_variable AND t1.use_negative = t5.use_negative
			AND t5.ldsity_object_type = 200 AND t5.areal_or_population = 200
			  
	), w_result AS (
		SELECT
			target_variable, use_negative, total_100100, total_100200, total_200100, total_200200,
			CASE
			WHEN total_100100 = 1 AND total_100200 = 0 AND total_200100 = 0 AND total_200200 = 0 THEN true	-- only one areal
			WHEN total_100100 = 0 AND total_100200 >= 1 AND total_200100 = 0 AND total_200200 = 0 THEN true -- one or more populational
			WHEN total_100100 = 1 AND total_100200 = 0 AND total_200100 = 0 AND total_200200 >= 1 THEN true -- one areal divided by one or more populational
			ELSE false -- everything else does not make sense
			END AS validity	
		FROM w_total
	)
	SELECT 	
		array_agg(total_100100 ORDER BY target_variable, use_negative),
		array_agg(total_100200 ORDER BY target_variable, use_negative),
		array_agg(total_200100 ORDER BY target_variable, use_negative),
		array_agg(total_200200 ORDER BY target_variable, use_negative),
		array_agg(use_negative ORDER BY target_variable, use_negative),
		array_agg(target_variable ORDER BY target_variable, use_negative)
	FROM w_result
	WHERE validity = false
	INTO _100100,_100200,_200100,_200200,_use_negative,_variables;

--raise notice '%, %, %, %', _100100,_100200,_200100,_200200;

	IF _variables IS NOT NULL
	THEN
		RAISE EXCEPTION 'Some of the inserted target variables (%) (can be only pozitive or negative part of the variable) has violited ldsity_object_type and areal/population combination constraints. (%, %, %, %, %)', _variables, _100100, _100200, _200100, _200200, _use_negative;
	END IF;


		WITH w AS (
			SELECT 	array_agg(t2.ldsity_object ORDER BY t2.ldsity_object) AS ldsity_objects,
				t1.use_negative
			FROM new_table AS t1
			INNER JOIN target_data.c_ldsity AS t2
			ON t1.ldsity = t2.id
			GROUP BY t1.use_negative
		)
		SELECT t1.ldsity_objects, t2.ldsity_objects
		FROM w AS t1, w AS t2
		WHERE t1.use_negative = false AND t2.use_negative = true
		INTO _pozitive_objects, _negative_objects;

		IF _negative_objects IS NOT NULL AND _pozitive_objects != _negative_objects
		THEN
			RAISE EXCEPTION 'The list of ldsity objects for negative ldsity contributions must be the same as for the pozitive ones.';
		END IF;

	RETURN NULL;
END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() IS
'This trigger function controls the newly inserted records into c_target_variable table.';

GRANT EXECUTE ON FUNCTION target_data.fn_trg_check_cm_ldsity2target_variable() TO public;


-- </function>

