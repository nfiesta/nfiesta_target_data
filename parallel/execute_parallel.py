#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
sys.path.append('./')
from nfiesta_parallel import run_parallel

sql = '''
SELECT format($$SELECT target_data.fn_save_ldsity_values(array[%s], 28);$$,
	refyearset2panel)
FROM target_data.t_panel_refyearset_group
WHERE panel_refyearset_group = 15;'''

run_parallel(_sql=sql, _processes=10, _verbose=True)
