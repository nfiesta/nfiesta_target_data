#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import datetime
import psycopg2
import psycopg2.extras
from multiprocessing import Pool

def single_query(c, m, _sql, verbose=True):
    try:
        #connstr = 'host=bran-pga port=5432 dbname=nfi_analytical user=xxx' # for network authentication (password can be stored in ~/.pgpass)
        connstr = 'dbname=nfi_analytical port=5432' # for local linux based authentication (without password)
        conn = psycopg2.connect(connstr)
    except psycopg2.DatabaseError as e:
        print("nfiesta_parallel.single_query(), not possible to connect DB")
        print(e)
        return 1

    conn.set_session(isolation_level=None, readonly=None, deferrable=None, autocommit=True) # to not execute in transaction
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cmd = '%s' % (_sql)

    try:
        #print("%s\tnfiesta_parallel.py processing command: %s" % (datetime.datetime.now(), cmd))
        cur.execute(cmd)
        #for notice in conn.notices: print(notice)
        #del conn.notices[:]
    except psycopg2.Error as e:
        print("nfiesta_parallel.single_query(), SQL error:")
        print(e)
        return 2
    if cur.description != None:
        path_row_list = cur.fetchall()
        if verbose:
            print('%s\t%s/%s\tnfiesta_parallel.py result not None: %s' % (datetime.datetime.now(), c, m, path_row_list))
        else:
            print('%s\tnfiesta_parallel.py result not None' % (datetime.datetime.now()))
    else:
        path_row_list = None
        if verbose:
            print('%s\t%s/%s\tnfiesta_parallel.py result None: %s' % (datetime.datetime.now(), c, m, path_row_list))
        else:
            print('%s\tnfiesta_parallel.py result None' % (datetime.datetime.now()))
        #print('%s\tnfiesta_parallel.py cur.description: %s' % (datetime.datetime.now(), cur.description))
        #path_row_list = None
    cur.close()
    conn.close()
    return path_row_list

def run_parallel(_sql, _processes, _verbose=True):
    _qrsl = single_query(1, 1, _sql, verbose=_verbose)
    _qrs = [[c+1, len(_qrsl), q[0]] for c, q in enumerate(_qrsl)]
    with Pool(_processes) as p:
        res_multi = p.starmap(single_query, _qrs, chunksize=1)
    print(res_multi)

