--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.

\set VERBOSITY terse

-- 1. update => attribute additivity violation simulation
update nfi_analytical.f_p_stems
set
	con_dec = (select id from nfi_analytical.c_con_dec where label = 'coniferous') -- orig value is 200
where
	plots = 6309
and
	dead_stem = (select id from nfi_analytical.c_dead_stem where label = 'live stem')
and
	species in (select id from nfi_analytical.c_species where label = 'olse lepkava');
	
-- aggregated ldsity values => count of merchantable wood stems => attribute additivity violation
select * from target_data.fn_save_ldsity_values
(
	(select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
	where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
	and reference_year_set in (select id from sdesign.t_reference_year_set where label = 'NFI2 (2011-2015)')),
	(select id from target_data.c_target_variable where label_en = 'number of merch stems')
);

-- check a count of merchantable wood stems in database
with w AS (select id, plot, available_datasets, round(value::numeric,8) as value, is_latest
from target_data.t_ldsity_values
where available_datasets in
(
select id from target_data.t_available_datasets
	where categorization_setup in (select unnest(
		(select array_agg(t.categorization_setup order by t.categorization_setup) from
			(select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup where ldsity2target_variable in
			(select id from target_data.cm_ldsity2target_variable where target_variable =
			(select id from target_data.c_target_variable where label_en = 'number of merch stems'))) as t)))
	and panel in (select unnest((select array_agg(id order by id) from sdesign.t_panel
			  	  where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))))
	and reference_year_set in (select unnest((select array_agg(id order by id) from sdesign.t_reference_year_set
							   where label = 'NFI2 (2011-2015)')))
) order by id
) select sum(value), count(*) from w;


-- 2. back update
update nfi_analytical.f_p_stems
set
	con_dec = (select id from nfi_analytical.c_con_dec where label = 'decidious')
where
	plots = 6309
and
	dead_stem = (select id from nfi_analytical.c_dead_stem where label = 'live stem')
and
	species in (select id from nfi_analytical.c_species where label = 'olse lepkava');

-- aggregated ldsity values => count of merchantable wood stems => end by note: The 0 new local densities were prepared for 0 plots.
select * from target_data.fn_save_ldsity_values
(
	(select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
	where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
	and reference_year_set in (select id from sdesign.t_reference_year_set where label = 'NFI2 (2011-2015)')),
	(select id from target_data.c_target_variable where label_en = 'number of merch stems')
);

-- check a count of merchantable wood stems in database
with w AS (select id, plot, available_datasets, round(value::numeric,8) as value, is_latest
from target_data.t_ldsity_values
where available_datasets in
(
select id from target_data.t_available_datasets
	where categorization_setup in (select unnest(
		(select array_agg(t.categorization_setup order by t.categorization_setup) from
			(select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup where ldsity2target_variable in
			(select id from target_data.cm_ldsity2target_variable where target_variable =
			(select id from target_data.c_target_variable where label_en = 'number of merch stems'))) as t)))
	and panel in (select unnest((select array_agg(id order by id) from sdesign.t_panel
			  	  where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))))
	and reference_year_set in (select unnest((select array_agg(id order by id) from sdesign.t_reference_year_set
							   where label = 'NFI2 (2011-2015)')))
) order by id
) select sum(value), count(*) from w;


