--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.

vacuum analyze;

with w_fnres as (
	select target_data.fn_add_plot_target_attr((select array_agg(id order by id) from target_data.t_categorization_setup)) as res
)
select plot, reference_year_set,
	variable, round(ldsity::numeric, 10) as ldsity,
	variables_def, variables_found, round(ldsity_sum::numeric, 10) as ldsity_sum,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where diff > 0.0001 or diff is null
order by plot, reference_year_set, variable, variables_def, variables_found;

alter table target_data.t_ldsity_values disable trigger trg__ldsity_values__ins;
alter table target_data.t_ldsity_values disable trigger trg__ldsity_values__upd;

update target_data.t_ldsity_values SET value = value + 0.13
where plot = 8155 and available_datasets = (select id from target_data.t_available_datasets 
                                                where panel = 3 and reference_year_set = 3 and categorization_setup = 106);

update target_data.t_ldsity_values SET value = value + 0.13
where plot = 8158 and available_datasets = (select id from target_data.t_available_datasets 
                                                where panel = 3 and reference_year_set = 3 and categorization_setup = 108);

alter table target_data.t_ldsity_values enable trigger trg__ldsity_values__ins;
alter table target_data.t_ldsity_values enable trigger trg__ldsity_values__upd;

with w_fnres as (
	select target_data.fn_add_plot_target_attr((select array_agg(id order by id) from target_data.t_categorization_setup)) as res
)
select plot, reference_year_set,
	variable, round(ldsity::numeric, 10) as ldsity,
	variables_def, variables_found, round(ldsity_sum::numeric, 10) as ldsity_sum,
	round(diff::numeric, 10) as diff
from (select (res).* from w_fnres) as foo
where diff > 0.0001 or diff is null
order by plot, reference_year_set, variable, variables_def, variables_found;

----------------additivity broken by introduction of all-zero local density (no insert in t_target_data) in t_available_dataset
/*
--select * from nfiesta_test.c_area_domain_category;
insert into nfiesta_test.c_area_domain_category (id, area_domain, label, description)
values (9, 3,'non-forest & non-LČR & TEST A','non-forest & non-LČR & TEST A'),
(10, 3,'non-forest & non-LČR & TEST B','non-forest & non-LČR & TEST B');

--select * from nfiesta_test.t_variable;
insert into nfiesta_test.t_variable (id, target_variable, area_domain_category)
values
(16, 1,9),
(17, 1,10);


--select * from nfiesta_test.t_variable_hierarchy;
insert into nfiesta_test.t_variable_hierarchy (id,variable,variable_superior)
values
(13, 16, 10),
(14, 17, 10);

--select * from nfiesta_test.t_available_datasets order by variable;
insert into nfiesta_test.t_available_datasets (panel, reference_year_set, variable)
values (3,3,16);
*/
