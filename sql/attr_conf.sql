--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------;
-- regression tests for attribute configurations
--------------------------------------------------------------------;
-- area of forests [unitary output]
-- GET before SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
),
w_fn AS (
	SELECT t2.target_variable
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,100) AS t2
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM w_fn AS t1
	INNER JOIN w_all AS t2
	ON t1.target_variable = t2.target_variable
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, NULL::integer[], NULL::integer[][], NULL::integer[], NULL::integer[][])
FROM w_agg AS t1;
-- SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
),
w_fn AS (
	SELECT t2.target_variable
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,100) AS t2
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM w_fn AS t1
	INNER JOIN w_all AS t2
	ON t1.target_variable = t2.target_variable
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_categorization_setup (t1.target_variable, t1.cm_ids, NULL::integer[], NULL::integer[][], NULL::integer[], NULL::integer[][])
FROM w_agg AS t1;
-- GET after SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
),
w_fn AS (
	SELECT t2.target_variable
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,100) AS t2
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM w_fn AS t1
	INNER JOIN w_all AS t2
	ON t1.target_variable = t2.target_variable
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, NULL::integer[], NULL::integer[][], NULL::integer[], NULL::integer[][])
FROM w_agg AS t1;
-- GET before SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
),
w_fn AS (
	SELECT t2.target_variable
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,100) AS t2
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM w_fn AS t1
	INNER JOIN w_all AS t2
	ON t1.target_variable = t2.target_variable
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup
(
	t1.target_variable,
	t1.cm_ids,
	array[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')]],
	NULL::integer[],
	NULL::integer[][]
)
FROM w_agg AS t1;
-- SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
),
w_fn AS (
	SELECT t2.target_variable
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,100) AS t2
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM w_fn AS t1
	INNER JOIN w_all AS t2
	ON t1.target_variable = t2.target_variable
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_categorization_setup
(
	t1.target_variable,
	t1.cm_ids,
	array[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')]],
	NULL::integer[],
	NULL::integer[][]
)
FROM w_agg AS t1;
-- GET after SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
),
w_fn AS (
	SELECT t2.target_variable
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,100) AS t2
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM w_fn AS t1
	INNER JOIN w_all AS t2
	ON t1.target_variable = t2.target_variable
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup
(
	t1.target_variable,
	t1.cm_ids,
	array[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')]],
	NULL::integer[],
	NULL::integer[][]
)
FROM w_agg AS t1;



-- count of merchantable wood stems [conifers/broadleaves and group of 14 woody species]
-- GET before SAVE
select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where label_en = 'number of merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch stems')),
	null::integer[],
	null::integer[][],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')]]
);
-- SAVE
select * from target_data.fn_save_categorization_setup
(
	(select id from target_data.c_target_variable where label_en = 'number of merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch stems')),
	null::integer[],
	null::integer[][],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')]]
);
-- GET after SAVE
select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where label_en = 'number of merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch stems')),
	null::integer[],
	null::integer[][],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')]]
);
-- GET before SAVE
select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where label_en = 'number of merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch stems')),
	array[300],
	array[array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')]]
);
-- SAVE
select * from target_data.fn_save_categorization_setup
(
	(select id from target_data.c_target_variable where label_en = 'number of merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch stems')),
	array[300],
	array[array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')]]
);
-- GET after SAVE
select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where label_en = 'number of merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch stems')),
	array[300],
	array[array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')]]
);



-- count of merchantable and non-merchantable wood stems [conifers/broadleaves and group of 14 woody species]
-- GET before SAVE
select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems')),
	null::integer[],
	null::integer[][],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);
-- SAVE
select * from target_data.fn_save_categorization_setup
(
	(select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems')),
	null::integer[],
	null::integer[][],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);
-- GET after SAVE
select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems')),
	null::integer[],
	null::integer[][],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);
-- GET before SAVE
select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems')),
	array[300],
	array[array[100],array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);
-- SAVE
select * from target_data.fn_save_categorization_setup
(
	(select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems')),
	array[300],
	array[array[100],array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);
-- GET after SAVE
select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems')),
	array[300],
	array[array[100],array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);



-- area of forests according to crown projections of merchantable wood stems [sortable/non-sortable, conifers/broadleaves and group of 14 woody species]
-- GET before SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, 
	null::integer[],
	null::integer[][],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;
-- SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_categorization_setup (t1.target_variable, t1.cm_ids, 
	null::integer[],
	null::integer[][],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;
-- GET after SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, 
	null::integer[],
	null::integer[][],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;
-- GET before SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[null::int]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;
-- SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[null::int]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;
-- GET after SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[null::int]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;



-- area of forests according to crown projections of merchantable and non-merchantable wood stems [sortable/non-sortable, conifers/broadleaves and group of 14 woody species]
-- GET before SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems','crown projections of non-merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, 
	null::integer[],
	null::integer[][],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;
-- SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems','crown projections of non-merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_categorization_setup (t1.target_variable, t1.cm_ids, 
	null::integer[],
	null::integer[][],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;
-- GET after SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems','crown projections of non-merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, 
	null::integer[],
	null::integer[][],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;
-- GET before SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems','crown projections of non-merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[null::int],
			array[null::int]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
			
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;
-- SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems','crown projections of non-merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[null::int],
			array[null::int]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
			
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;
-- GET after SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems','crown projections of non-merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[null::int],
			array[null::int]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
			
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;



-- change of area of forests [unitary output]
-- not divided by any
-- GET before SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area change'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
),
w_fn AS (
	SELECT t2.target_variable
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,100) AS t2
), w_agg AS (
	SELECT t1.target_variable, array_agg(distinct t2.id ORDER BY t2.id) AS cm_ids
	FROM w_fn AS t1
	INNER JOIN w_all AS t2
	ON t1.target_variable = t2.target_variable
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup
(
	t1.target_variable,
	t1.cm_ids,
	NULL::integer[],
	NULL::integer[][],
	NULL::integer[],
	NULL::integer[][]
)
FROM w_agg AS t1;
-- SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area change'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
),
w_fn AS (
	SELECT t2.target_variable
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,100) AS t2
), w_agg AS (
	SELECT t1.target_variable, array_agg(distinct t2.id ORDER BY t2.id) AS cm_ids
	FROM w_fn AS t1
	INNER JOIN w_all AS t2
	ON t1.target_variable = t2.target_variable
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_categorization_setup
(
	t1.target_variable,
	t1.cm_ids,
	NULL::integer[],
	NULL::integer[][],
	NULL::integer[],
	NULL::integer[][]
)
FROM w_agg AS t1;
-- GET after SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area change'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
),
w_fn AS (
	SELECT t2.target_variable
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,100) AS t2
), w_agg AS (
	SELECT t1.target_variable, array_agg(distinct t2.id ORDER BY t2.id) AS cm_ids
	FROM w_fn AS t1
	INNER JOIN w_all AS t2
	ON t1.target_variable = t2.target_variable
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup
(
	t1.target_variable,
	t1.cm_ids,
	NULL::integer[],
	NULL::integer[][],
	NULL::integer[],
	NULL::integer[][]
)
FROM w_agg AS t1;



-- not divided by sub_pop, only by area
-- GET before SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area change'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
),
w_fn AS (
	SELECT t2.target_variable
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,100) AS t2
), w_agg AS (
	SELECT t1.target_variable, array_agg(distinct t2.id ORDER BY t2.id) AS cm_ids
	FROM w_fn AS t1
	INNER JOIN w_all AS t2
	ON t1.target_variable = t2.target_variable
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup
(
	t1.target_variable,
	t1.cm_ids,
	array[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array[	array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
		array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')]
		],
	NULL::integer[],
	NULL::integer[][]
)
FROM w_agg AS t1;
-- SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area change'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
),
w_fn AS (
	SELECT t2.target_variable
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,100) AS t2
), w_agg AS (
	SELECT t1.target_variable, array_agg(distinct t2.id ORDER BY t2.id) AS cm_ids
	FROM w_fn AS t1
	INNER JOIN w_all AS t2
	ON t1.target_variable = t2.target_variable
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_categorization_setup
(
	t1.target_variable,
	t1.cm_ids,
	array[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array[	array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
		array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')]
		],
	NULL::integer[],
	NULL::integer[][]
)
FROM w_agg AS t1;
-- GET after SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area change'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
),
w_fn AS (
	SELECT t2.target_variable
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,100) AS t2
), w_agg AS (
	SELECT t1.target_variable, array_agg(distinct t2.id ORDER BY t2.id) AS cm_ids
	FROM w_fn AS t1
	INNER JOIN w_all AS t2
	ON t1.target_variable = t2.target_variable
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup
(
	t1.target_variable,
	t1.cm_ids,
	array[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array[	array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
		array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')]
		],
	NULL::integer[],
	NULL::integer[][]
)
FROM w_agg AS t1;



-- sub_population separated
-- GET before SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type, t2.use_negative
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area change'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en, t2.use_negative 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY use_negative, label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en, t2.use_negative
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array[
			'crown projections of merchantable wood stems',
			'crown projections of non-merchantable wood stems',
			'crown projections of merchantable wood stems',
			'crown projections of non-merchantable wood stems'
			]::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(distinct t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT DISTINCT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity AND
		t1.use_negative = t2.use_negative
	GROUP BY t1.target_variable
)
select target_data.fn_get_categorization_setup
	(
	t1.target_variable, t1.cm_ids,
	null::integer[],
	null::integer[][],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
			
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')
		  			],
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')
		  			]
		  	]
	)
FROM w_agg AS t1;
-- SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type, t2.use_negative
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area change'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en, t2.use_negative 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY use_negative, label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en, t2.use_negative
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array[
			'crown projections of merchantable wood stems',
			'crown projections of non-merchantable wood stems',
			'crown projections of merchantable wood stems',
			'crown projections of non-merchantable wood stems'
			]::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(distinct t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT DISTINCT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity AND
		t1.use_negative = t2.use_negative
	GROUP BY t1.target_variable
)
select target_data.fn_save_categorization_setup
	(
	t1.target_variable, t1.cm_ids,
	null::integer[],
	null::integer[][],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
			
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')
		  			],
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')
		  			]
		  	]
	)
FROM w_agg AS t1;
-- GET after SAVE
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type, t2.use_negative
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area change'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en, t2.use_negative 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY use_negative, label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en, t2.use_negative
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array[
			'crown projections of merchantable wood stems',
			'crown projections of non-merchantable wood stems',
			'crown projections of merchantable wood stems',
			'crown projections of non-merchantable wood stems'
			]::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(distinct t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT DISTINCT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity AND
		t1.use_negative = t2.use_negative
	GROUP BY t1.target_variable
)
select target_data.fn_get_categorization_setup
	(
	t1.target_variable, t1.cm_ids,
	null::integer[],
	null::integer[][],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
			
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')
		  			],
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')
		  			]
		  	]
	)
FROM w_agg AS t1;
--------------------------------------------------------------------;
--------------------------------------------------------------------;
-- create constrained target variable - sorting only by conifers
--------------------------------------------------------------------;
--------------------------------------------------------------------;
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
	LEFT JOIN LATERAL
		target_data.fn_get_sub_population_category(t2.target_variable, t2.id) AS t3
	ON true
	WHERE t3.id IS NULL OR t3.restriction = false
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems','crown projections of non-merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT target_variable, ldsity, label_en FROM w_100 UNION ALL 
		SELECT target_variable, ldsity, label_en FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_constrained_target_variable (
	t1.target_variable, t1.cm_ids,
	NULL::integer[][],
	NULL::integer[][],
	array	[
			array[null::int],
			array[(select id from target_data.c_sub_population_category where label_en = 'conifers')],
			array[(select id from target_data.c_sub_population_category where label_en = 'conifers')]
			],
			
	array	[
			array[null::int],
		  	array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  	array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]
		  	])
FROM w_agg AS t1;

-- merch and non/merch - constrain on area sfmpg
select * from target_data.fn_save_constrained_target_variable
(
	(select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems'),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable 
		where target_variable = (select id from target_data.c_target_variable where label_en = 'number of merch and non-merch stems')
	),
	array	[
		array[(select id from target_data.c_area_domain_category where label_en = 'SFMPG forest')],
		array[(select id from target_data.c_area_domain_category where label_en = 'SFMPG forest')]
		],
	array	[
		array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
		array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')]
		],
	null::integer[][],
	null::integer[][]
);

-- constrain the core ldsity
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
	LEFT JOIN LATERAL
		target_data.fn_get_sub_population_category(t2.target_variable, t2.id) AS t3
	ON true
	WHERE t3.id IS NULL OR t3.restriction = false
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems','crown projections of non-merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_constrained_target_variable (
	t1.target_variable, t1.cm_ids,
	array	[
			array[(select id from target_data.c_area_domain_category where label_en = 'SFMPG forest')],
			array[null::int],
			array[null::int]
			],
			
	array	[
		  	array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[null::int],
			array[null::int]
		  	],
	NULL::integer[][],
	NULL::integer[][]
	)
FROM w_agg AS t1;

-- test on fn_get_target_variable
select * from target_data.fn_get_target_variable();
select * from target_data.fn_get_ldsity(array[1,4,5,8]);
select * from target_data.fn_get_ldsity(array[1,4,5,9]);
select * from target_data.fn_get_ldsity(array[1,4,5,10]);

--------------------------------------------------------------------;
-- check attribute configurations
--------------------------------------------------------------------;
select * from target_data.t_categorization_setup order by id;
--------------------------------------------------------------------;
WITH w_adc AS (
	SELECT
		t1.ldsity2target_variable,
		t1.categorization_setup,
		--t2.adc2classification_rule,
		--t2.id,
		--t3.area_domain_category,
		array_agg(t4.label_en ORDER BY t2.id) FILTER (WHERE label_en IS NOT NULL) AS adc_label_en
	FROM 
		target_data.cm_ldsity2target2categorization_setup AS t1
	LEFT JOIN
		unnest(t1.adc2classification_rule) WITH ORDINALITY AS t2(adc2classification_rule, id)
	ON true
	LEFT JOIN
		target_data.cm_adc2classification_rule AS t3
	ON t2.adc2classification_rule = t3.id
	LEFT JOIN
		target_data.c_area_domain_category AS t4
	ON t3.area_domain_category = t4.id
	GROUP BY t1.ldsity2target_variable,
		t1.categorization_setup
),
w_spc AS (
	SELECT
		t1.ldsity2target_variable,
		t1.categorization_setup,
		--t2.spc2classification_rule,
		--t2.id,
		--t3.sub_population_category,
		array_agg(t4.label_en ORDER BY t2.id) FILTER (WHERE label_en IS NOT NULL) AS spc_label_en
	FROM 
		target_data.cm_ldsity2target2categorization_setup AS t1
	LEFT JOIN
		unnest(t1.spc2classification_rule) WITH ORDINALITY AS t2(spc2classification_rule, id)
	ON true
	LEFT JOIN
		target_data.cm_spc2classification_rule AS t3
	ON t2.spc2classification_rule = t3.id
	LEFT JOIN
		target_data.c_sub_population_category AS t4
	ON t3.sub_population_category = t4.id
	GROUP BY t1.ldsity2target_variable,
		t1.categorization_setup
)
SELECT  t1.ldsity2target_variable,
	t4.label_en AS target_variable,
	t5.label_en AS ldsity,
	t1.categorization_setup,
	t1.adc_label_en, t2.spc_label_en
FROM w_adc AS t1
INNER JOIN
	w_spc AS t2
ON t1.ldsity2target_variable = t2.ldsity2target_variable AND
	t1.categorization_setup = t2.categorization_setup
INNER JOIN
	target_data.cm_ldsity2target_variable AS t3
ON t1.ldsity2target_variable = t3.id
INNER JOIN target_data.c_target_variable AS t4
ON t3.target_variable = t4.id
INNER JOIN target_data.c_ldsity AS t5
ON t3.ldsity = t5.id
ORDER BY t4.label_en, t5.label_en, t1.adc_label_en, t2.spc_label_en;

--------------------------------------------------------------------;