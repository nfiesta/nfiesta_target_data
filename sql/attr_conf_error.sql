--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------;
-- regression test of violate of the uniqueness for attribute configurations
---------------------------------------------------------------------------;
WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type, t2.area_domain_category, t2.sub_population_category 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area' AND t2.ldsity_object_type = 100 AND t2.area_domain_category IS NULL
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en, t3.label_en AS spc, t3.restriction
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
	LEFT JOIN LATERAL
		target_data.fn_get_sub_population_category(t2.target_variable, t2.id) AS t3
	ON true
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels, array_agg(spc) FILTER (WHERE spc IS NOT NULL) AS spc_restr
	FROM 
		(SELECT target_variable, label_en, array_agg(spc ORDER BY spc) FILTER (WHERE spc IS NOT NULL AND restriction = true) AS spc
		FROM w_fn
		GROUP BY target_variable, label_en
		) AS t1
	GROUP BY target_variable
), w_fn_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en, t3.label_en AS adc, t3.restriction
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	LEFT JOIN LATERAL
		target_data.fn_get_area_domain_category(t2.target_variable, t2.id) AS t3
	ON true
	WHERE t1.labels = array['crown projections of merchantable wood stems','crown projections of non-merchantable wood stems']::varchar[] AND
		t1.spc_restr IS NULL --= array[array['conifers'],array['conifers']]::varchar[][]
), w_100 AS (
	SELECT target_variable, ldsity, label_en, array_agg(adc ORDER BY adc) FILTER (WHERE adc IS NOT NULL AND restriction = true) AS adc
	FROM w_fn_100
	GROUP BY target_variable, ldsity, label_en
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT target_variable, ldsity, label_en FROM w_100 WHERE adc IS NULL UNION ALL 
		SELECT distinct target_variable, ldsity, label_en FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100 WHERE adc IS NULL)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
insert into target_data.cm_ldsity2target2categorization_setup(ldsity2target_variable,adc2classification_rule,spc2classification_rule,categorization_setup)
select
		cltcs.ldsity2target_variable,
		cltcs.adc2classification_rule,
		cltcs.spc2classification_rule,
		cltcs.categorization_setup
from
		target_data.cm_ldsity2target2categorization_setup as cltcs
where
		cltcs.ldsity2target_variable in (select unnest(w_agg.cm_ids) from w_agg)
order
		by cltcs.categorization_setup;
---------------------------------------------------------------------------;
-- regression tests for not valid input arguments => fn_get_categorization_setup
---------------------------------------------------------------------------;
select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where id = 3),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where id = 3)),
	array[300],
	array[array[100],array[null::int]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);


select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where id = 3),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where id = 3)),
	array[300],
	array[array[100],array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[null::int,(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);


select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where id = 3),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where id = 3)),
	array[300,300],
	array[array[100,100],array[100,null::int]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);


select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where id = 3),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where id = 3)),
	array[300],
	array[array[100],array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[null::int]]
);


WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[100]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;


WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area'),300],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot'),100],
			array[null::int,100]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;


WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[null::int]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable')
			],
	array	[
			array[100],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;


WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_get_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[null::int]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,100],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;


select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where id = 3),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where id = 3)),
	array[300],
	array[array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);


select * from target_data.fn_get_categorization_setup
(
	(select id from target_data.c_target_variable where id = 3),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where id = 3)),
	array[300],
	array[array[100],array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')]]
);
---------------------------------------------------------------------------;
-- regression tests for not valid input arguments => fn_save_categorization_setup
---------------------------------------------------------------------------;
select * from target_data.fn_save_categorization_setup
(
	(select id from target_data.c_target_variable where id = 3),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where id = 3)),
	array[300],
	array[array[100],array[null::int]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);


select * from target_data.fn_save_categorization_setup
(
	(select id from target_data.c_target_variable where id = 3),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where id = 3)),
	array[300],
	array[array[100],array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[null::int,(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);


select * from target_data.fn_save_categorization_setup
(
	(select id from target_data.c_target_variable where id = 3),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where id = 3)),
	array[300,300],
	array[array[100,100],array[100,null::int]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);


select * from target_data.fn_save_categorization_setup
(
	(select id from target_data.c_target_variable where id = 3),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where id = 3)),
	array[300],
	array[array[100],array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[null::int]]
);


WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[100]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;


WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area'),300],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot'),100],
			array[null::int,100]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,null::int],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;


WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[null::int]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable')
			],
	array	[
			array[100],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;


WITH w_all AS (
	SELECT t1.id AS target_variable, t2.id, t2.ldsity, t2.ldsity_object_type 
	FROM target_data.c_target_variable AS t1
	INNER JOIN target_data.cm_ldsity2target_variable AS t2
	ON t1.id = t2.target_variable
	WHERE t1.label_en = 'forest area'
),
w_tv AS (
	SELECT array_agg(DISTINCT t1.target_variable ORDER BY t1.target_variable) AS variables
	FROM w_all AS t1 where t1.target_variable not in (10)
), w_fn AS (
	SELECT t2.target_variable, t2.id AS ldsity, t2.label_en 
	FROM 	w_tv AS t1,
		target_data.fn_get_ldsity(t1.variables,200) AS t2
), w_200 AS (
	SELECT target_variable, array_agg(label_en ORDER BY label_en) AS labels
	FROM w_fn
	GROUP BY target_variable
), w_100 AS (
	SELECT
		t2.target_variable, t2.id AS ldsity, t2.label_en
	FROM
		w_200 AS t1,
		target_data.fn_get_ldsity(array[t1.target_variable], 100) AS t2
	WHERE t1.labels = array['crown projections of merchantable wood stems']::varchar[]
), w_agg AS (
	SELECT t1.target_variable, array_agg(t2.id ORDER BY t2.id) AS cm_ids
	FROM (	SELECT * FROM w_100 UNION ALL 
		SELECT * FROM w_fn WHERE target_variable = (SELECT target_variable FROM w_100)
		) AS t1
	INNER JOIN w_all AS t2
	ON 	t1.target_variable = t2.target_variable AND
		t1.ldsity = t2.ldsity
	GROUP BY t1.target_variable
)
SELECT target_data.fn_save_categorization_setup (t1.target_variable, t1.cm_ids, 
	array	[(select id from target_data.c_area_domain where label_en = 'land register according to SFMPG area')],
	array	[
			array[(select id from target_data.c_ldsity_object where label_en = 'the center of the inventory plot')],
			array[null::int]
			],
	array	[
			(select id from target_data.c_sub_population where label_en = 'sortable/non-sortable'),
			(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),
			(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')
			],
	array	[
			array[null::int,null::int,100],
		  	array	[
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),
		  			(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')
		  			]
		  	])
FROM w_agg AS t1;


select * from target_data.fn_save_categorization_setup
(
	(select id from target_data.c_target_variable where id = 3),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where id = 3)),
	array[300],
	array[array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')],
		  array[(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'non-merchantable wood stems')]]
);

select * from target_data.fn_save_categorization_setup
(
	(select id from target_data.c_target_variable where id = 3),
	(select array_agg(id order by id) from target_data.cm_ldsity2target_variable where target_variable = (select id from target_data.c_target_variable where id = 3)),
	array[300],
	array[array[100],array[100]],
	array[(select id from target_data.c_sub_population where label_en = 'conifers/broadleaves'),(select id from target_data.c_sub_population where label_en = 'group of 14 woody species')],
	array[array[(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems'),(select id from target_data.c_ldsity_object where label_en = 'merchantable wood stems')]]
);
---------------------------------------------------------------------------;