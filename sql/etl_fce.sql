--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------

-- get_EXPORT_CONNECTIONS
select id, host, dbname, port, comment from target_data.fn_etl_get_export_connections() order by id;


-- save_EXPORT_CONNECTION
select * from target_data.fn_etl_save_export_connection
(
	'bran-nfiesta'::varchar,
	'nfi_esta'::varchar,
	5432::integer,
	'host=bran-nfiesta port=5432 dbname=nfi_esta'::text
);


-- get_EXPORT_CONNECTIONS
select id, host, dbname, port, comment from target_data.fn_etl_get_export_connections() order by id;


-- get_TARGET_VARIABLE
select id, label, description, label_en, description_en, id_etl_target_variable, check_target_variable, refyearset2panel_mapping, check_atomic_area_domains, check_atomic_sub_populations, metadata
from target_data.fn_etl_get_target_variable
(
	(	-- export_connection
		select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432
	),	
	'cs',
	null::boolean
) order by id;

select 	id, label, description, label_en, description_en, id_etl_target_variable, check_target_variable, refyearset2panel_mapping, check_atomic_area_domains, check_atomic_sub_populations, metadata
from target_data.fn_etl_get_target_variable
(
	(	-- export_connection
		select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432
	),	
	'en',
	null::boolean
) order by id;

-- get_TARGET_VARIABLE
-- 4th argument is not null
select 	id, label, description, label_en, description_en, id_etl_target_variable, check_target_variable, refyearset2panel_mapping, check_atomic_area_domains, check_atomic_sub_populations, metadata
from target_data.fn_etl_get_target_variable
(
	(	-- export_connection
		select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432
	),	
	'cs',
	null::boolean,
	3
) order by id;


-- check_TARGET_VARIABLE
select * from target_data.fn_etl_check_target_variable
(
	(	-- export_connection
		select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432
	),
	(	-- target_variable
		select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1

	)
);


-- get_TARGET_VARIABLE_METADATA
with
w as	(
		select target_data.fn_etl_get_target_variable_metadata
				(
					(select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
),
					'cs'
				) as res
		)
select jsonb_pretty(w.res::jsonb) as res from w;


-- save_TARGET_VARIABLE
select * from target_data.fn_etl_save_target_variable
	(
	(select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432),
	(select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
),
	7
	);

-- test unique
-- etl_id
select * from target_data.fn_etl_save_target_variable
	(
	(select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432),
	(select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is not null) as t1
),
	7
	);

-- target_variable
select * from target_data.fn_etl_save_target_variable
	(
	(select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432),
	(select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
),
	8
	);

-- check_t_etl_target_variable
select id, export_connection, target_variable, etl_id from target_data.t_etl_target_variable order by id;


-- check_TARGET_VARIABLE
select * from target_data.fn_etl_check_target_variable
(
	(	-- export_connection
		select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432
	),
	(	-- target_variable
		select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1

	)
);


-- get_PANEL_REFYEARSET_COMBINATIONS
-- 2nd an 3rd argument is null
select refyearset2panel, panel, reference_year_set, passed_by_module from target_data.fn_etl_get_panel_refyearset_combinations
(
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	),
	null::integer[],
	null::integer[]
)
order by refyearset2panel;

-- 3rd argument is null
select refyearset2panel, panel, reference_year_set, passed_by_module from target_data.fn_etl_get_panel_refyearset_combinations
(
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	),
	array[5,9],
	null::integer[]
)
order by refyearset2panel;

-- 3rd argument is not null
select refyearset2panel, panel, reference_year_set, passed_by_module from target_data.fn_etl_get_panel_refyearset_combinations
(
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	),
	array[5,9,13],
	array[5,13]
)
order by refyearset2panel;


-- check_AREA_DOMAIN
select * from target_data.fn_etl_check_area_domain
(
	(	-- refyearset2panel_mapping
		select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
		where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
		and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
	),			
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	)
);


-- save_AREA_DOMAIN => I. domain
select * from target_data.fn_etl_save_area_domain
(
	(	-- export_connection
		select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432
	),
	(	-- area_domain
		select area_domain from target_data.fn_etl_check_area_domain
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
	),
		-- etl_id
		4
);


-- check_AREA_DOMAIN
select * from target_data.fn_etl_check_area_domain
(
	(	-- refyearset2panel_mapping
		select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
		where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
		and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
	),			
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	)
);


-- check_AREA_DOMAIN_CATEGORY
select * from target_data.fn_etl_check_area_domain_category
(
	(	-- refyearset2panel_mapping
		select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
		where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
		and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
	),			
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	)
);


-- check_AREA_DOMAIN_CATEGORY [with input argument id_t_etl_ad]
select * from target_data.fn_etl_check_area_domain_category
(
	(	-- refyearset2panel_mapping
		select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
		where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
		and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
	),			
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	),
	1
);


-- save_AREA_DOMAIN_CATEGORY => I. category
select * from target_data.fn_etl_save_area_domain_category
(
	(	-- area_domain_category
		select area_domain_category from target_data.fn_etl_check_area_domain_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'SFMPG forest'
	),
		-- etl_id
		8,
	(
		-- etl_area_domain
		select id_t_etl_ad from target_data.fn_etl_check_area_domain_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'SFMPG forest'
	)
);


-- save_AREA_DOMAIN_CATEGORY => II. category
select * from target_data.fn_etl_save_area_domain_category
(
	(	-- area_domain_category
		select area_domain_category from target_data.fn_etl_check_area_domain_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'SFMPG non-forest'
	),
		-- etl_id
		9,
	(
		-- etl_area_domain
		select id_t_etl_ad from target_data.fn_etl_check_area_domain_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'SFMPG non-forest'
	)
);


-- check_AREA_DOMAIN_CATEGORY
select * from target_data.fn_etl_check_area_domain_category
(
	(	-- refyearset2panel_mapping
		select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
		where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
		and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
	),			
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	)
);


-- check_SUB_POPULATION
select * from target_data.fn_etl_check_sub_population
(
	(	-- refyearset2panel_mapping
		select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
		where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
		and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
	),			
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	)
);


-- save_SUB_POPULATION => I. population
select * from target_data.fn_etl_save_sub_population
(
	(	-- export_connection
		select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432
	),
	(	-- sub_population
		select sub_population from target_data.fn_etl_check_sub_population
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'group of 14 woody species'
	),
		-- etl_id
		5
);


-- save_SUB_POPULATION => II. population
select * from target_data.fn_etl_save_sub_population
(
	(	-- export_connection
		select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432
	),
	(	-- sub_population
		select sub_population from target_data.fn_etl_check_sub_population
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers/broadleaves'
	),
		-- etl_id
		6
);


-- save_SUB_POPULATION => III. population
select * from target_data.fn_etl_save_sub_population
(
	(	-- export_connection
		select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432
	),
	(	-- sub_population
		select sub_population from target_data.fn_etl_check_sub_population
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers/broadleaves;group of 14 woody species'
	),
		-- etl_id
		7
);


-- check_SUB_POPULATION
select * from target_data.fn_etl_check_sub_population
(
	(	-- refyearset2panel_mapping
		select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
		where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
		and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
	),			
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	)
);


-- check_SUB_POPULATION_CATEGORY
select * from target_data.fn_etl_check_sub_population_category
(
	(	-- refyearset2panel_mapping
		select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
		where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
		and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
	),			
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	)
);


-- check_SUB_POPULATION_CATEGORY [with input argument id_t_etl_sp]
select * from target_data.fn_etl_check_sub_population_category
(
	(	-- refyearset2panel_mapping
		select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
		where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
		and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
	),			
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	),
	1
);


-- save_SUB_POPULATION_CATEGORY => I. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'Norway spruce'
	),
		-- etl_id
		21,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'Norway spruce'
	)
);


-- save_SUB_POPULATION_CATEGORY => II. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'European silver fir'
	),
		-- etl_id
		22,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'European silver fir'
	)
);


-- save_SUB_POPULATION_CATEGORY => III. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'Scots pine'
	),
		-- etl_id
		23,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'Scots pine'
	)
);


-- save_SUB_POPULATION_CATEGORY => IV. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'European larch'
	),
		-- etl_id
		24,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'European larch'
	)
);


-- save_SUB_POPULATION_CATEGORY => V. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'other coniferous'
	),
		-- etl_id
		25,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'other coniferous'
	)
);


-- save_SUB_POPULATION_CATEGORY => VI. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'European beech'
	),
		-- etl_id
		26,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'European beech'
	)
);


-- save_SUB_POPULATION_CATEGORY => VII. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'oak species'
	),
		-- etl_id
		27,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'oak species'
	)
);


-- save_SUB_POPULATION_CATEGORY => VIII. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'European hornbeam'
	),
		-- etl_id
		28,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'European hornbeam'
	)
);


-- save_SUB_POPULATION_CATEGORY => IX. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'maple species'
	),
		-- etl_id
		29,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'maple species'
	)
);


-- save_SUB_POPULATION_CATEGORY => X. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'ash species'
	),
		-- etl_id
		30,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'ash species'
	)
);


-- save_SUB_POPULATION_CATEGORY => XI. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'alder species'
	),
		-- etl_id
		31,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'alder species'
	)
);


-- save_SUB_POPULATION_CATEGORY => XII. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'birch species'
	),
		-- etl_id
		32,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'birch species'
	)
);


-- save_SUB_POPULATION_CATEGORY => XIII. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'other hardwood broad'
	),
		-- etl_id
		33,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'other hardwood broad'
	)
);


-- save_SUB_POPULATION_CATEGORY => XIV. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'other softwood broad'
	),
		-- etl_id
		34,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'other softwood broad'
	)
);


-- save_SUB_POPULATION_CATEGORY => XV. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers'
	),
		-- etl_id
		35,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers'
	)
);


-- save_SUB_POPULATION_CATEGORY => XVI. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers;Norway spruce'
	),
		-- etl_id
		36,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers;Norway spruce'
	)
);


-- save_SUB_POPULATION_CATEGORY => XVII. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers;European silver fir'
	),
		-- etl_id
		37,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers;European silver fir'
	)
);


-- save_SUB_POPULATION_CATEGORY => XVIII. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers;Scots pine'
	),
		-- etl_id
		38,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers;Scots pine'
	)
);


-- save_SUB_POPULATION_CATEGORY => XIX. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers;European larch'
	),
		-- etl_id
		39,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers;European larch'
	)
);


-- save_SUB_POPULATION_CATEGORY => XX. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers;other coniferous'
	),
		-- etl_id
		40,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'conifers;other coniferous'
	)
);


-- save_SUB_POPULATION_CATEGORY => XXI. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves'
	),
		-- etl_id
		41,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves'
	)
);


-- save_SUB_POPULATION_CATEGORY => XXII. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;European beech'
	),
		-- etl_id
		42,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;European beech'
	)
);


-- save_SUB_POPULATION_CATEGORY => XXIII. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;oak species'
	),
		-- etl_id
		43,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;oak species'
	)
);


-- save_SUB_POPULATION_CATEGORY => XXIV. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;European hornbeam'
	),
		-- etl_id
		44,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;European hornbeam'
	)
);


-- save_SUB_POPULATION_CATEGORY => XXV. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;maple species'
	),
		-- etl_id
		45,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;maple species'
	)
);


-- save_SUB_POPULATION_CATEGORY => XXVI. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;ash species'
	),
		-- etl_id
		46,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;ash species'
	)
);


-- save_SUB_POPULATION_CATEGORY => XXVII. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;alder species'
	),
		-- etl_id
		47,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;alder species'
	)
);


-- save_SUB_POPULATION_CATEGORY => XXVIII. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;birch species'
	),
		-- etl_id
		48,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;birch species'
	)
);


-- save_SUB_POPULATION_CATEGORY => XXIX. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;other hardwood broad'
	),
		-- etl_id
		49,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;other hardwood broad'
	)
);


-- save_SUB_POPULATION_CATEGORY => XXXX. category
select * from target_data.fn_etl_save_sub_population_category
(
	(	-- sub_population_category
		select sub_population_category from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;other softwood broad'
	),
		-- etl_id
		50,
	(
		-- etl_sub_population
		select id_t_etl_sp from target_data.fn_etl_check_sub_population_category
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
					and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			)
				where label_en = 'broadleaves;other softwood broad'
	)
);


-- check_SUB_POPULATION_CATEGORY
select * from target_data.fn_etl_check_sub_population_category
(
	(	-- refyearset2panel_mapping
		select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
		where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
		and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
	),			
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	)
);


-- get_VARIABLES
with
w as	(
		select target_data.fn_etl_get_variables
			(
				(	-- refyearset2panel_mapping
					select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
				),			
				(	-- etl_target_variable
					select id from target_data.t_etl_target_variable
					where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
			 		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
				)
			) as res
		)
select jsonb_pretty(w.res::jsonb) as res from w;


-- get_PANEL_REFYEARSET_COMBINATIONS_FALSE
select * from target_data.fn_etl_get_panel_refyearset_combinations_false
(
	(	-- JSON
		select json_agg(json_build_object('panel',t.panel,'reference_year_set',t.reference_year_set))
		from
				(
				select
						tp.panel,
						trys.reference_year_set
				from
					(
					select id, panel, reference_year_set from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
					) as t
				inner join sdesign.t_panel as tp on t.panel = tp.id
				inner join sdesign.t_reference_year_set as trys on t.reference_year_set = trys.id
				) as t
	),
	(	-- refyearset2panel_mapping
		select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
		where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
		and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
	)
) order by refyearset2panel;


-- get_VARIABLES_FALSE [CS]
select * from target_data.fn_etl_get_variables_false
(
	(	-- JSON
		select json_agg(json_build_object
							(
							'panel',t.panel,
							'reference_year_set',t.reference_year_set,
							'sub_population',t.sub_population,
							'sub_population_category',t.sub_population_category,
							'area_domain',t.area_domain,
							'area_domain_category',t.area_domain_category,
							'sub_population_en',t.sub_population_en,
							'sub_population_category_en',t.sub_population_category_en,
							'area_domain_en',t.area_domain_en,
							'area_domain_category_en',t.area_domain_category_en							
							)
						)
		from
				(
				select
						tp.panel,
						trys.reference_year_set,
						tt.sub_population,
						tt.sub_population_category,
						tt.area_domain,
						tt.area_domain_category,
						tt.sub_population_en,
						tt.sub_population_category_en,
						tt.area_domain_en,
						tt.area_domain_category_en
				from
					(
					select id, panel, reference_year_set from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
					) as t
				inner join sdesign.t_panel as tp on t.panel = tp.id
				inner join sdesign.t_reference_year_set as trys on t.reference_year_set = trys.id
				
				inner join
							(
							select
									'NFRD11- 1plot, s2a'	as panel,
									'2011 - 2015' as reference_year_set,
									'null' as sub_population,
									'null' as sub_population_category,
									'území SLHPO' as area_domain,
									'v území SLHPO' as area_domain_category,
									'null' as sub_population_en,
									'null' as sub_population_category_en,
									'SFMPG area' as area_domain_en,
									'in SFMPG area' as area_domain_category_en
							union all
							select
									'NFRD11- 1plot, s2a'	as panel,
									'2011 - 2015' as reference_year_set,
									'null' as sub_population,
									'null' as sub_population_category,
									'území SLHPO' as area_domain,
									'mimo území SLHPO' as area_domain_category,
									'null' as sub_population_en,
									'null' as sub_population_category_en,
									'SFMPG area' as area_domain_en,
									'outside SFMPG area' as area_domain_category_en
							-------------------------------------------------------
							union all
							select
									'NFRD14- 1plot, s2a'	as panel,
									'2011 - 2015' as reference_year_set,
									'null' as sub_population,
									'null' as sub_population_category,
									'území SLHPO' as area_domain,
									'v území SLHPO' as area_domain_category,
									'null' as sub_population_en,
									'null' as sub_population_category_en,
									'SFMPG area' as area_domain_en,
									'in SFMPG area' as area_domain_category_en
							union all
							select
									'NFRD14- 1plot, s2a'	as panel,
									'2011 - 2015' as reference_year_set,
									'null' as sub_population,
									'null' as sub_population_category,
									'území SLHPO' as area_domain,
									'mimo území SLHPO' as area_domain_category,
									'null' as sub_population_en,
									'null' as sub_population_category_en,
									'SFMPG area' as area_domain_en,
									'outside SFMPG area' as area_domain_category_en
							) as tt
							on tp.panel = tt.panel and trys.reference_year_set = tt.reference_year_set
				
				) as t
	),
	'NFRD11- 1plot, s2a'::varchar,
	'2011 - 2015'::varchar,
	'cs'::character varying
);


-- get_VARIABLES_FALSE [EN]
select * from target_data.fn_etl_get_variables_false
(
	(	-- JSON
		select json_agg(json_build_object
							(
							'panel',t.panel,
							'reference_year_set',t.reference_year_set,
							'sub_population',t.sub_population,
							'sub_population_category',t.sub_population_category,
							'area_domain',t.area_domain,
							'area_domain_category',t.area_domain_category,
							'sub_population_en',t.sub_population_en,
							'sub_population_category_en',t.sub_population_category_en,
							'area_domain_en',t.area_domain_en,
							'area_domain_category_en',t.area_domain_category_en							
							)
						)
		from
				(
				select
						tp.panel,
						trys.reference_year_set,
						tt.sub_population,
						tt.sub_population_category,
						tt.area_domain,
						tt.area_domain_category,
						tt.sub_population_en,
						tt.sub_population_category_en,
						tt.area_domain_en,
						tt.area_domain_category_en
				from
					(
					select id, panel, reference_year_set from sdesign.cm_refyearset2panel_mapping
					where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD14- 1plot, s2a'))
					and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
					) as t
				inner join sdesign.t_panel as tp on t.panel = tp.id
				inner join sdesign.t_reference_year_set as trys on t.reference_year_set = trys.id
				
				inner join
							(
							select
									'NFRD11- 1plot, s2a'	as panel,
									'2011 - 2015' as reference_year_set,
									'null' as sub_population,
									'null' as sub_population_category,
									'území SLHPO' as area_domain,
									'v území SLHPO' as area_domain_category,
									'null' as sub_population_en,
									'null' as sub_population_category_en,
									'SFMPG area' as area_domain_en,
									'in SFMPG area' as area_domain_category_en
							union all
							select
									'NFRD11- 1plot, s2a'	as panel,
									'2011 - 2015' as reference_year_set,
									'null' as sub_population,
									'null' as sub_population_category,
									'území SLHPO' as area_domain,
									'mimo území SLHPO' as area_domain_category,
									'null' as sub_population_en,
									'null' as sub_population_category_en,
									'SFMPG area' as area_domain_en,
									'outside SFMPG area' as area_domain_category_en
							-------------------------------------------------------
							union all
							select
									'NFRD14- 1plot, s2a'	as panel,
									'2011 - 2015' as reference_year_set,
									'null' as sub_population,
									'null' as sub_population_category,
									'území SLHPO' as area_domain,
									'v území SLHPO' as area_domain_category,
									'null' as sub_population_en,
									'null' as sub_population_category_en,
									'SFMPG area' as area_domain_en,
									'in SFMPG area' as area_domain_category_en
							union all
							select
									'NFRD14- 1plot, s2a'	as panel,
									'2011 - 2015' as reference_year_set,
									'null' as sub_population,
									'null' as sub_population_category,
									'území SLHPO' as area_domain,
									'mimo území SLHPO' as area_domain_category,
									'null' as sub_population_en,
									'null' as sub_population_category_en,
									'SFMPG area' as area_domain_en,
									'outside SFMPG area' as area_domain_category_en
							) as tt
							on tp.panel = tt.panel and trys.reference_year_set = tt.reference_year_set
				
				) as t
	),
	'NFRD11- 1plot, s2a'::varchar,
	'2011 - 2015'::varchar,
	'en'::character varying
);


-- get_ETL_ID4TARGET_VARIABLE
select * from target_data.fn_etl_get_etl_id4target_variable(1);


-- export_VARIABLE
with
w as	(
		select target_data.fn_etl_export_variable
		(
			(	-- refyearset2panel_mapping
				select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
				where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
				and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
			),			
			(	-- etl_target_variable
				select id from target_data.t_etl_target_variable
				where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
				and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
			)
		) as res
		)
select jsonb_pretty(res::jsonb) as res from w; 


-- export_VARIABLE_HIERARCHY
with
w as	(
		select target_data.fn_etl_export_variable_hierarchy
		(
			(	-- refyearset2panel_mapping
				select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
				where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
				and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
			),			
			(	-- etl_target_variable
				select id from target_data.t_etl_target_variable
				where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
				and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
			)		
		) as res
		)
select jsonb_pretty(res::jsonb) as res from w;


-- export_LDSITY_VALUES
with
w as	(
		select target_data.fn_etl_export_ldsity_values
		(
			(	-- refyearset2panel_mapping
				select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
				where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
				and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
			),			
			(	-- etl_target_variable
				select id from target_data.t_etl_target_variable
				where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
				and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
			)
		) as res
		)
select jsonb_pretty(res::jsonb) as res from w;


-- save_LOG
select * from target_data.fn_etl_save_log
(
	(	-- refyearset2panel_mapping
		select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
		where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
		and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
	),			
	(	-- etl_target_variable
		select id from target_data.t_etl_target_variable
		where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
		and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
	)		
);


-- check_T_ETL_LOG
select id, etl_target_variable, refyearset2panel_mapping from target_data.t_etl_log order by id;


-- check_TARGET_VARIABLE
select * from target_data.fn_etl_check_target_variable
(
	(	-- export_connection
		select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432
	),
	(	-- target_variable
		select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1

	)
);


-- get_TARGET_VARIABLE_METADATAS
with
w as	(
		select target_data.fn_etl_get_target_variable_metadatas(array[1],'cs') as res
		)
select jsonb_pretty(w.res::jsonb) as res from w;


-- save_EXPORT_CONNECTION
select * from target_data.fn_etl_save_export_connection
(
	'bran-nfiesta'::varchar,
	'nfi_esta'::varchar,
	5433::integer,
	'host=bran-nfiesta dbname=nfi_esta port=5433'::text
);


-- update_EXPORT_CONNECTION
select * from target_data.fn_etl_update_export_connection
(
	2,
	'host=bran-nfiesta_test dbname=nfi_esta_test port=5434'::text
);


-- get_EXPORT_CONNECTIONS
select id, host, dbname, port, comment from target_data.fn_etl_get_export_connections() order by id;


-- try_delete_EXPORT_CONNECTION
select * from target_data.fn_etl_try_delete_export_connection(2);


-- delete_EXPORT_CONNECTION
select * from target_data.fn_etl_delete_export_connection(2);


-- get_EXPORT_CONNECTIONS
select id, host, dbname, port, comment from target_data.fn_etl_get_export_connections() order by id;


-- get_ETL_IDS_OF_AREA_DOMAIN
select * from target_data.fn_etl_get_etl_ids_of_area_domain(1);


-- get_ETL_IDS_OF_SUB_POPULATION
select * from target_data.fn_etl_get_etl_ids_of_sub_population(1);


-- get_ETL_IDS_OF_TARGET_VARIABLE
select * from target_data.fn_etl_get_etl_ids_of_target_variable(1);


-- EXPORT_AREA_DOMAIN_CATEGORY_MAPPING
with
w1 as	(
		select target_data.fn_etl_export_area_domain_category_mapping
		(
			(	-- refyearset2panel_mapping
				select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
				where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
				and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
			),			
			(	-- etl_target_variable
				select id from target_data.t_etl_target_variable
				where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
				and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
			)
		) as res
		)
,w2 as	(
		select json_array_elements(w1.res) as s from w1
		)
,w3 as	(
		select
				(s->>'area_domain_category_non_atomic')::integer	as area_domain_category_non_atomic,
				(s->>'area_domain_category_atomic')::integer		as area_domain_category_atomic
		from w2
		)
select
		w3.area_domain_category_non_atomic,
		w3.area_domain_category_atomic
from
		w3 order by w3.area_domain_category_non_atomic, w3.area_domain_category_atomic;


-- EXPORT_SUB_POPULATION_CATEGORY_MAPPING
with
w1 as	(
		select target_data.fn_etl_export_sub_population_category_mapping
		(
			(	-- refyearset2panel_mapping
				select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
				where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
				and reference_year_set in (select id from sdesign.t_reference_year_set where reference_year_set = '2011 - 2015')
			),			
			(	-- etl_target_variable
				select id from target_data.t_etl_target_variable
				where export_connection = (select id from target_data.t_export_connection where host = 'bran-nfiesta' and dbname = 'nfi_esta' and port = 5432)
				and target_variable = (select t1.target_variable 
	from 	(select distinct t1.id as target_variable
		from target_data.c_target_variable as t1
		inner join target_data.cm_ldsity2target_variable as t2
		on t1.id = t2.target_variable
		where label = 'počet kmenů hroubí a nehroubí' and t2.area_domain_category is null) as t1
) and etl_id = 7
			)
		) as res
		)
,w2 as	(
		select json_array_elements(w1.res) as s from w1
		)
,w3 as	(
		select
				(s->>'sub_population_category_non_atomic')::integer	as sub_population_category_non_atomic,
				(s->>'sub_population_category_atomic')::integer		as sub_population_category_atomic
		from w2
		)
select
		w3.sub_population_category_non_atomic,
		w3.sub_population_category_atomic
from
		w3 order by w3.sub_population_category_non_atomic, w3.sub_population_category_atomic;


-- get_area_domains4update
select jsonb_pretty(fn_etl_get_area_domains4update::jsonb) from target_data.fn_etl_get_area_domains4update(1);


-- get_list_of_area_domains4update
select * from target_data.fn_etl_get_list_of_area_domains4update(1,array[4]) order by etl_area_domain;


-- get_area_domain_categories4update
select jsonb_pretty(fn_etl_get_area_domain_categories4update::jsonb) from target_data.fn_etl_get_area_domain_categories4update(1,4);


-- get_sub_populations4update
select jsonb_pretty(fn_etl_get_sub_populations4update::jsonb) from target_data.fn_etl_get_sub_populations4update(1);


-- get_list_of_sub_populations4update
select * from target_data.fn_etl_get_list_of_sub_populations4update(1,array[5,6]) order by etl_sub_population;


-- get_sub_population_categories4update
select jsonb_pretty(fn_etl_get_sub_population_categories4update::jsonb) from target_data.fn_etl_get_sub_population_categories4update(1,5);


-- get_area_domain_categories4area_domains
select jsonb_pretty(fn_etl_get_area_domain_categories4area_domains::jsonb) from target_data.fn_etl_get_area_domain_categories4area_domains(1);


-- get_sub_population_categories4sub_populations
select jsonb_pretty(fn_etl_get_sub_population_categories4sub_populations::jsonb) from target_data.fn_etl_get_sub_population_categories4sub_populations(1);


-- get_area_domain_json
with w as (select * from target_data.fn_etl_get_area_domain_json(array[5],1)) select jsonb_pretty(w.fn_etl_get_area_domain_json::jsonb) from w;
with w as (select * from target_data.fn_etl_get_area_domain_json(array[5],1,true)) select jsonb_pretty(w.fn_etl_get_area_domain_json::jsonb) from w;
with w as (select * from target_data.fn_etl_get_area_domain_json(array[5],1,false)) select jsonb_pretty(w.fn_etl_get_area_domain_json::jsonb) from w;


-- get_sub_population_json
with w as (select * from target_data.fn_etl_get_sub_population_json(array[5],1)) select jsonb_pretty(w.fn_etl_get_sub_population_json::jsonb) from w;
with w as (select * from target_data.fn_etl_get_sub_population_json(array[5],1,true)) select jsonb_pretty(w.fn_etl_get_sub_population_json::jsonb) from w;
with w as (select * from target_data.fn_etl_get_sub_population_json(array[5],1,false)) select jsonb_pretty(w.fn_etl_get_sub_population_json::jsonb) from w;


-- get_area_domain
select * from target_data.fn_etl_get_area_domain(array[5],1,'[{"id" : 1, "auto_transfer" : false, "auto_pair" : true}]'::json) order by id;


-- get_area_domain_category
select * from target_data.fn_etl_get_area_domain_category(array[5],1,array[300]) order by id;


-- get_sub_population
select * from target_data.fn_etl_get_sub_population(array[5],1,'[{"id" : 1, "auto_transfer" : false, "auto_pair" : true}]'::json) order by id;


-- get_sub_population_category
select * from target_data.fn_etl_get_sub_population_category(array[5],1,array[401]) order by id;
select * from target_data.fn_etl_get_sub_population_category(array[5],1,array[300]) order by id;
select * from target_data.fn_etl_get_sub_population_category(array[5],1,array[401,300]) order by id;


-- get_area_domain_category_json
with w as (select * from target_data.fn_etl_get_area_domain_category_json(array[5],1,array[300])) select jsonb_pretty(w.fn_etl_get_area_domain_category_json::jsonb) from w;


-- get_sub_population_category_json
with w as (select * from target_data.fn_etl_get_sub_population_category_json(array[5],1,array[401,300])) select jsonb_pretty(w.fn_etl_get_sub_population_category_json::jsonb) from w;
