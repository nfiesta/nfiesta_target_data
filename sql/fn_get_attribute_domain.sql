select 
	category[1] as attr_1,
	category[2] as attr_2 
from target_data.fn_get_attribute_domain('sp'::varchar, ARRAY[(SELECT id FROM target_data.c_sub_population WHERE label_en = 'dead stem/live stem'),
							(SELECT id FROM target_data.c_sub_population WHERE label_en = 'conifers/broadleaves')]);

select 
	category[1] as attr_1,
	category[2] as attr_2, 
	category[3] as attr_3 
from target_data.fn_get_attribute_domain('sp'::varchar, ARRAY[(SELECT id FROM target_data.c_sub_population WHERE label_en = 'dead stem/live stem'),
							(SELECT id FROM target_data.c_sub_population WHERE label_en = 'conifers/broadleaves'),
							(SELECT id FROM target_data.c_sub_population WHERE label_en = 'group of 14 woody species')]);

select 
	category[1] as attr_1,
	category[2] as attr_2 
from target_data.fn_get_attribute_domain('ad'::varchar, ARRAY[(SELECT id FROM target_data.c_area_domain WHERE label_en = 'entire area of geographical domain'),
								(SELECT id FROM target_data.c_area_domain WHERE label_en = 'land category according to FAO FRA')]);

select 
	category[1] as attr_1,
	category[2] as attr_2, 
	category[3] as attr_3 
from target_data.fn_get_attribute_domain('ad'::varchar, ARRAY[(SELECT id FROM target_data.c_area_domain WHERE label_en = 'entire area of geographical domain'),
								(SELECT id FROM target_data.c_area_domain WHERE label_en = 'land category according to FAO FRA'),
								(SELECT id FROM target_data.c_area_domain WHERE label_en = 'land category according to FAO FRA')]);
