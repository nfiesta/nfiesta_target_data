--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

with w_util as (
	select target_data.fn_system_utilization() as system_utilization
) 
select 
	(system_utilization->'load'->>'1_min')::float between 0.0 and (system_utilization->>'cpu_count')::float as "1_min_load_present",
	(system_utilization->'load'->>'5_min')::float between 0.0 and (system_utilization->>'cpu_count')::float as "5_min_load_present",
	(system_utilization->'load'->>'15_min')::float between 0.0 and (system_utilization->>'cpu_count')::float as "15_min_load_present",
	(system_utilization->>'cpu_count')::int > 0 as "cpu_count_present",
	(system_utilization->>'connection_limit')::int = -1 or  (system_utilization->>'connection_limit')::int > 0 as "connection_limit_present"
from w_util;
/*
    "load": {
        "1_min": 0.0,
        "5_min": 0.02,
        "15_min": 0.0
    },
    "cpu_count": 8,
    "connection_limit": -1
*/
