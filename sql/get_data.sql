--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------

-- c_ldsity_object_group
SELECT * FROM target_data.fn_get_ldsity_object_group() ORDER BY id;
SELECT * FROM target_data.fn_get_ldsity_object_group(1);

-- c_ldsity_object
SELECT * FROM target_data.fn_get_ldsity_object() ORDER BY id;
SELECT * FROM target_data.fn_get_ldsity_object4ld_object_group() ORDER BY id;
SELECT * FROM target_data.fn_get_ldsity_object4ld_object_group(1);
SELECT * FROM target_data.fn_get_state_or_change() ORDER BY id;
SELECT * FROM target_data.fn_get_ldsity_object4adsp(400,100);
SELECT * FROM target_data.fn_get_ldsity_object4adsp(400,200);
SELECT * FROM target_data.fn_get_ldsity_object4ld_object_adsp(400,100,100);
SELECT * FROM target_data.fn_get_ldsity_object4ld_object_adsp(400,200,100);
SELECT * FROM target_data.fn_get_ldsity_objects4target_variable(3);

-- c_target_variable
SELECT * FROM target_data.fn_get_target_variable() ORDER BY id;
SELECT * FROM target_data.fn_get_target_variable(1) ORDER BY id;

-- c_ldsity
SELECT * FROM target_data.fn_get_ldsity() ORDER BY id;
SELECT * FROM target_data.fn_get_ldsity(array[1,4,5],100) ORDER BY id;
SELECT * FROM target_data.fn_get_ldsity(array[1,4,5],200) ORDER BY id;
SELECT * FROM target_data.fn_get_ldsity4object() ORDER BY id;
SELECT * FROM target_data.fn_get_ldsity4object(200) ORDER BY id;
SELECT * FROM target_data.fn_get_ldsity4object(200,200) ORDER BY id;
SELECT * FROM target_data.fn_get_ldsity_subpop4object(100);
SELECT * FROM target_data.fn_get_ldsity_subpop4object(200);
SELECT * FROM target_data.fn_get_ldsity_subpop4object(300);

SELECT * FROM target_data.fn_get_ldsity_object_type() ORDER BY id;
SELECT * FROM target_data.fn_get_areal_or_population() ORDER BY id;

-- c_version
SELECT * FROM target_data.fn_get_version(NULL::int,NULL::int) ORDER BY id;
SELECT * FROM target_data.fn_get_version(1,NULL::int) ORDER BY id;
SELECT * FROM target_data.fn_get_version(1,1) ORDER BY id;
SELECT * FROM target_data.fn_get_version(NULL::int,1) ORDER BY id;
SELECT * FROM target_data.fn_get_version(1,1,false) ORDER BY id;
SELECT * FROM target_data.fn_get_version(1,1,true) ORDER BY id;
SELECT * FROM target_data.fn_get_version(NULL,1,true) ORDER BY id;

-- c_unit_of_measure
SELECT * FROM target_data.fn_get_unit_of_measure() ORDER BY id;
SELECT * FROM target_data.fn_get_unit_of_measure(1) ORDER BY id;
SELECT * FROM target_data.fn_get_unit_of_measure4ld_object_group(1) ORDER BY id;

-- c_definition_variant
SELECT * FROM target_data.fn_get_definition_variant() ORDER BY id;
SELECT * FROM target_data.fn_get_definition_variant(1) ORDER BY id;
SELECT * FROM target_data.fn_get_definition_variant4ldsity() ORDER BY id;
SELECT * FROM target_data.fn_get_definition_variant4ldsity(4) ORDER BY id;
-- c_area_domain
SELECT * FROM target_data.fn_get_area_domain(NULL::int, 2) ORDER BY id;
SELECT * FROM target_data.fn_get_area_domain(NULL::int, 6) ORDER BY id;
SELECT * FROM target_data.fn_get_area_domain(NULL::int, 13) ORDER BY id;

-- c_area_domain_category
SELECT * FROM target_data.fn_get_area_domain_category(NULL::int, NULL::int) ORDER BY id;
SELECT * FROM target_data.fn_get_area_domain_category(1) ORDER BY id;
SELECT * FROM target_data.fn_get_area_domain_category(1,1) ORDER BY id;
SELECT * FROM target_data.fn_get_area_domain_category(NULL::int,1) ORDER BY id;
SELECT * FROM target_data.fn_get_area_domain_category(7,1,true) ORDER BY id;

-- c_sub_population_category
SELECT * FROM target_data.fn_get_sub_population_category(NULL::int,NULL::int) ORDER BY id;
SELECT * FROM target_data.fn_get_sub_population_category(2,NULL::int) ORDER BY id;
SELECT * FROM target_data.fn_get_sub_population_category(2,2) ORDER BY id;
SELECT * FROM target_data.fn_get_sub_population_category(NULL::int,2) ORDER BY id;
SELECT * FROM target_data.fn_get_sub_population_category(7,2,true) ORDER BY id;

-- hierarchy
SELECT * FROM target_data.fn_get_hierarchy(100,100,200);
SELECT * FROM target_data.fn_get_hierarchy(200,400,200);
