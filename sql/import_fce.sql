--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------

begin;

-- get_AREAL_OR_POPULATIONS
select * from target_data.fn_import_get_areal_or_populations(1);
select * from target_data.fn_import_get_areal_or_populations
(
		1,
		(select array_agg(id) from target_data.c_areal_or_population where label = 'area domain')
);

-- check_AREAL_OR_POPULATION
select * from target_data.fn_import_check_areal_or_population(1,'population'::varchar);

-- get_LDSITY_OBJECTS
select * from target_data.fn_import_get_ldsity_objects(100);
select * from target_data.fn_import_get_ldsity_objects(100,array[200,300]);

-- check_LDSITY_OBJECT
select * from target_data.fn_import_check_ldsity_object(100, 'merchantable wood stems'::varchar);

-- save_LDSITY_OBJECT
select pg_catalog.setval('target_data.c_ldsity_object_id_seq', (select max(id) from target_data.c_ldsity_object), true);
select * from target_data.fn_import_save_ldsity_object(100,'test'::varchar,'test'::text,'test'::varchar,'test'::text,'nfi_analytical.t_test'::varchar,100,100,'plots'::varchar,'is_latest = true'::text);

-- LDSITY_OBJECT
select * from target_data.c_ldsity_object order by id;

-- get_UNIT_OF_MEASURES
select * from target_data.fn_import_get_unit_of_measures(200);
select * from target_data.fn_import_get_unit_of_measures(200,array[100]);

-- check_UNIT_OF_MEASURE
select * from target_data.fn_import_check_unit_of_measure(300,'m2/ha'::varchar);

-- save_UNIT_OF_MEASURE
select pg_catalog.setval('target_data.c_unit_of_measure_id_seq', (select max(id) from target_data.c_unit_of_measure), true);
select * from target_data.fn_import_save_unit_of_measure(200,'test'::varchar,'test'::text,'test'::varchar,'test'::text);

-- UNIT_OF_MEASURE
select * from target_data.c_unit_of_measure order by id;

-- get_DEFINITION_VARIANTS
select * from target_data.fn_import_get_definition_variants(2);
select * from target_data.fn_import_get_definition_variants(2,array[1]);

-- check_DEFINITION_VARIANT
select * from target_data.fn_import_check_definition_variant(2, 'Generalizovaná lokální hustota s využitím vzorníků druhého stupně.'::varchar);

-- save_DEFINITION_VARIANT
select pg_catalog.setval('target_data.c_definition_variant_id_seq', (select max(id) from target_data.c_definition_variant), true);
select * from target_data.fn_import_save_definition_variant(4,'test'::varchar,'test'::text,'test'::varchar,'test'::text);

-- DEFINITION_VARIANT
select * from target_data.c_definition_variant order by id;

-- get_AREA_DOMAINS
select * from target_data.fn_import_get_area_domains(200);
select * from target_data.fn_import_get_area_domains(200,array[100]);

-- check_AREA_DOMAIN
select * from target_data.fn_import_check_area_domain(2, 'land category according to FAO FRA'::character varying);

-- save_AREA_DOMAIN
select pg_catalog.setval('target_data.c_area_domain_id_seq', (select max(id) from target_data.c_area_domain), true);
select * from target_data.fn_import_save_area_domain(400,'test'::varchar,'test'::text,'test'::varchar,'test'::text);

-- AREA_DOMAIN
select * from target_data.c_area_domain order by id;

-- get_SUB_POPULATIONS
select * from target_data.fn_import_get_sub_populations(100);
select * from target_data.fn_import_get_sub_populations(100,array[100]);

-- check_SUB_POPULATION
select * from target_data.fn_import_check_sub_population(300, 'group of 14 woody species'::character varying);

-- save_SUB_POPULATION
select pg_catalog.setval('target_data.c_sub_population_id_seq', (select max(id) from target_data.c_sub_population), true);
select * from target_data.fn_import_save_sub_population(500,'test'::varchar,'test'::text,'test'::varchar,'test'::text);

-- SUB_POPULATION
select * from target_data.c_sub_population order by id;

-- get_AREA_DOMAIN_CATEGORIES
select * from target_data.fn_import_get_area_domain_categories(201,200);
select * from target_data.fn_import_get_area_domain_categories(201,200,array[201]);

-- check_AREA_DOMAIN_CATEGORY
select * from target_data.fn_import_check_area_domain_category(202,200,'non-forest'::character varying);

-- save_AREA_DOMAIN_CATEGORY
select pg_catalog.setval('target_data.c_area_domain_category_id_seq', (select max(id) from target_data.c_area_domain_category), true);
select * from target_data.fn_import_save_area_domain_category(401,301,'test'::varchar,'test'::text,'test'::varchar,'test'::text);

-- AREA_DOMAIN_CATEGORY
select * from target_data.c_area_domain_category order by id;

-- get_SUB_POPULATION_CATEGORIES
select * from target_data.fn_import_get_sub_population_categories(101,100);
select * from target_data.fn_import_get_sub_population_categories(101,100,array[101]);

-- check_SUB_POPULATION_CATEGORY
select * from target_data.fn_import_check_sub_population_category(101,100,'dead stem'::character varying);

-- save_SUB_POPULATION_CATEGORY
select pg_catalog.setval('target_data.c_sub_population_category_id_seq', (select max(id) from target_data.c_sub_population_category), true);
select * from target_data.fn_import_save_sub_population_category(405,402,'test'::varchar,'test'::text,'test'::varchar,'test'::text);

-- SUB_POPULATION_CATEGORY
select * from target_data.c_sub_population_category order by id;

-- get_ADC_HIERARCHIES
select * from target_data.fn_import_get_adc_hierarchies(1,201,100,true);
select * from target_data.fn_import_get_adc_hierarchies(1,201,100,true,array[2]);

-- save_ADC_HIERARCHY
select * from target_data.fn_import_save_adc_hierarchy(1,101,201,true);
select * from target_data.fn_import_save_adc_hierarchy(1,101,202,true);

-- T_ADC_HIERARCHY
select * from target_data.t_adc_hierarchy order by id;

-- get_SPC_HIERARCHIES
select * from target_data.fn_import_get_spc_hierarchies(1,102,301,true);
select * from target_data.fn_import_get_spc_hierarchies(1,102,301,true,array[17]);

-- save_SPC_HIERARCHY
select * from target_data.fn_import_save_spc_hierarchy(1,403,405,true);

-- T_SPC_HIERARCHY
select * from target_data.t_spc_hierarchy order by id;

-- get_ADC2CLASSIFICATION_RULES
select * from target_data.fn_import_get_adc2classification_rules(1,201,100);
select * from target_data.fn_import_get_adc2classification_rules(1,201,100,array[2]);

-- check_ADC2CLASSIFICATION_RULE
select * from target_data.fn_import_check_adc2classification_rule(3,202,100,'fao_fra_forest IN (200)'::text);

-- save_ADC2CLASSIFICATION_RULE
select * from target_data.fn_import_save_adc2classification_rule(6,303,100,'test'::text,5);

-- CM_ADC2CLASSIFICATION_RULE
select * from target_data.cm_adc2classification_rule order by id;

-- get_SPC2CLASSIFICATION_RULES
select * from target_data.fn_import_get_spc2classification_rules(1,101,200);
select * from target_data.fn_import_get_spc2classification_rules(1,101,200,array[1]);

-- check_SPC2CLASSIFICATION_RULE
select * from target_data.fn_import_check_spc2classification_rule(2,102,200,'dead_stem IN (200)'::text);

-- save_SPC2CLASSIFICATION_RULE
select * from target_data.fn_import_save_spc2classification_rule(43,405,100,'test'::text,5);

-- CM_SPC2CLASSIFICATION_RULE
select * from target_data.cm_spc2classification_rule order by id;

-- get_ADC2CLASSRULE2PANEL_REFYEARSETS
select * from target_data.fn_import_get_adc2classrule2panel_refyearsets(1,1,2);
select * from target_data.fn_import_get_adc2classrule2panel_refyearsets(1,1,2,array[1]);
select * from target_data.fn_import_get_adc2classrule2panel_refyearsets(1,6,5);

-- save_ADC2CLASSRULE2PANEL_REFYEARSET
select * from target_data.fn_import_save_adc2classrule2panel_refyearset(1,6,2);
select * from target_data.fn_import_save_adc2classrule2panel_refyearset(1,6,4);

-- CM_ADC2CLASSRULE2PANEL_REFYEARSET
select * from target_data.cm_adc2classrule2panel_refyearset order by id;

-- get_SPC2CLASSRULE2PANEL_REFYEARSETS
select * from target_data.fn_import_get_spc2classrule2panel_refyearsets(1,1,2);
select * from target_data.fn_import_get_spc2classrule2panel_refyearsets(1,1,2,array[1]);
select * from target_data.fn_import_get_spc2classrule2panel_refyearsets(1,43,5);

-- save_ADC2CLASSRULE2PANEL_REFYEARSET
select * from target_data.fn_import_save_spc2classrule2panel_refyearset(1,43,2);
select * from target_data.fn_import_save_spc2classrule2panel_refyearset(1,43,4);

-- CM_SPC2CLASSRULE2PANEL_REFYEARSET
select * from target_data.cm_spc2classrule2panel_refyearset order by id;

-- get_LDSITIES
select * from target_data.fn_import_get_ldsities(100,1);
select * from target_data.fn_import_get_ldsities(100,1,array[2,3]);
select * from target_data.fn_import_get_ldsities(100,1,array[1,2,3]);

-- check_LDSITY
select * from target_data.fn_import_check_ldsity(1,200,'count of merchantable wood stems');

-- save_LDSITY
select * from target_data.fn_import_save_ldsity(6,'test'::varchar,'test'::text,'test'::varchar,'test'::text,401,'test'::text,301,array[303],null::int[],array[4]);

-- LDSITY
select * from target_data.c_ldsity order by id;

-- get_VERSIONS
select * from target_data.fn_import_get_versions(1);
select * from target_data.fn_import_get_versions(1,array[100]);

-- check_VERSION
select * from target_data.fn_import_check_version(1,'version 1'::varchar);

-- save_VERSION
select * from target_data.fn_import_save_version(1,'test'::varchar,'test'::text,'test'::varchar,'test'::text);

-- VERSION
select * from target_data.c_version order by id;

-- get_LDSITY2PANEL_REFYEARSET_VERSION
select * from target_data.fn_import_get_ldsity2panel_refyearset_versions(1000,5,5,100);
select * from target_data.fn_import_get_ldsity2panel_refyearset_versions(1000,5,5,100,array[1]);

-- save_LDSITY2PANEL_REFYEARSET_VERSION
select * from target_data.fn_import_save_ldsity2panel_refyearset_version(1000,6,5,100);
select * from target_data.fn_import_save_ldsity2panel_refyearset_version(1000,6,9,100);
select * from target_data.fn_import_save_ldsity2panel_refyearset_version(1000,6,13,100);

-- CM_LDSITY2PANEL_REFYEARSET_VERSION
select * from target_data.cm_ldsity2panel_refyearset_version order by id;

commit;