--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------
alter table target_data.t_ldsity_values disable trigger trg__ldsity_values__ins;
alter table target_data.t_ldsity_values disable trigger trg__ldsity_values__upd;

--------------------------------------------------------------------;
-- change of area of forests => test save zero value
--------------------------------------------------------------------;
update nfi_analytical.t_plots set is_latest = false where plot = 14937 and reference_year_set = 4;

insert into nfi_analytical.t_plots(id,plot,reference_year_set,fao_fra_forest,land_register,ld_forest,version,is_latest) values
((select max(id) + 1 from nfi_analytical.t_plots),14937,4,200,200,0,100,true);

select * from target_data.fn_save_ldsity_values
(
	(select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
	where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
	and reference_year_set in (select id from sdesign.t_reference_year_set where label = 'NFI2-3_change (2011-2020)')),
		(SELECT t1.target_variable
		FROM
			(SELECT t1.id AS target_variable, array_agg(t2.id) AS cm_ids, array_agg(t3.label_en ORDER BY t3.label_en) AS labels, count(*) AS total
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			WHERE t1.label = 'změna plochy lesa'
			GROUP BY t1.id) AS t1
		WHERE t1.total = 2
		)
);

select id, plot, available_datasets, value, is_latest
from target_data.t_ldsity_values
where available_datasets in
(
select id from target_data.t_available_datasets
	where categorization_setup in (select unnest(
		(select array_agg(t.categorization_setup order by t.categorization_setup) from
			(select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup where ldsity2target_variable in
			(select id from target_data.cm_ldsity2target_variable where target_variable =
				(SELECT t1.target_variable
				FROM
					(SELECT t1.id AS target_variable, array_agg(t2.id) AS cm_ids, array_agg(t3.label_en ORDER BY t3.label_en) AS labels, count(*) AS total
					FROM target_data.c_target_variable AS t1
					INNER JOIN target_data.cm_ldsity2target_variable AS t2
					ON t1.id = t2.target_variable
					INNER JOIN target_data.c_ldsity AS t3
					ON t2.ldsity = t3.id
					WHERE t1.label = 'změna plochy lesa'
					GROUP BY t1.id) AS t1
				WHERE t1.total = 2
				)
			)) as t)))
	and panel in (select unnest((select array_agg(id order by id) from sdesign.t_panel
			  	  where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))))
	and reference_year_set in (select unnest((select array_agg(id order by id) from sdesign.t_reference_year_set
							   where label = 'NFI2-3_change (2011-2020)')))
) order by id;
--------------------------------------------------------------------;
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- change of area of forests => add new local density
--------------------------------------------------------------------;
update nfi_analytical.t_plots set is_latest = false where plot = 8156 and reference_year_set = 4;

insert into nfi_analytical.t_plots(id,plot,reference_year_set,fao_fra_forest,land_register,ld_forest,version,is_latest) values
((select max(id) + 1 from nfi_analytical.t_plots),8156,4,100,100,1,100,true);

select * from target_data.fn_save_ldsity_values
(
	(select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
	where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
	and reference_year_set in (select id from sdesign.t_reference_year_set where label = 'NFI2-3_change (2011-2020)')),
		(SELECT t1.target_variable
		FROM
			(SELECT t1.id AS target_variable, array_agg(t2.id) AS cm_ids, array_agg(t3.label_en ORDER BY t3.label_en) AS labels, count(*) AS total
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			WHERE t1.label = 'změna plochy lesa'
			GROUP BY t1.id) AS t1
		WHERE t1.total = 2
		)
);

select id, plot, available_datasets, value, is_latest
from target_data.t_ldsity_values
where available_datasets in
(
select id from target_data.t_available_datasets
	where categorization_setup in (select unnest(
		(select array_agg(t.categorization_setup order by t.categorization_setup) from
			(select distinct categorization_setup from target_data.cm_ldsity2target2categorization_setup where ldsity2target_variable in
			(select id from target_data.cm_ldsity2target_variable where target_variable =
				(SELECT t1.target_variable
				FROM
					(SELECT t1.id AS target_variable, array_agg(t2.id) AS cm_ids, array_agg(t3.label_en ORDER BY t3.label_en) AS labels, count(*) AS total
					FROM target_data.c_target_variable AS t1
					INNER JOIN target_data.cm_ldsity2target_variable AS t2
					ON t1.id = t2.target_variable
					INNER JOIN target_data.c_ldsity AS t3
					ON t2.ldsity = t3.id
					WHERE t1.label = 'změna plochy lesa'
					GROUP BY t1.id) AS t1
				WHERE t1.total = 2
				)
		)) as t)))
	and panel in (select unnest((select array_agg(id order by id) from sdesign.t_panel
			  	  where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))))
	and reference_year_set in (select unnest((select array_agg(id order by id) from sdesign.t_reference_year_set
							   where label = 'NFI2-3_change (2011-2020)')))
) order by id;
--------------------------------------------------------------------;
--------------------------------------------------------------------;

alter table target_data.t_ldsity_values enable trigger trg__ldsity_values__ins;
alter table target_data.t_ldsity_values enable trigger trg__ldsity_values__upd;
