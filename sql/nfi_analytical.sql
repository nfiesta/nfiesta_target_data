--
-- Copyright 2020, 2021 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

---------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------;
CREATE SCHEMA nfi_analytical;
---------------------------------------------------------------------------------------------------;

--------------------------------------------------------------------;
-- Table: c_areal_or_population
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_areal_or_population;

CREATE TABLE nfi_analytical.c_areal_or_population
(
  id			integer NOT NULL,
  label			character varying(200) NOT NULL,
  description		text NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_areal_or_population IS '.';
COMMENT ON COLUMN nfi_analytical.c_areal_or_population.id IS '.';
COMMENT ON COLUMN nfi_analytical.c_areal_or_population.label IS '.';
COMMENT ON COLUMN nfi_analytical.c_areal_or_population.description IS '.';

ALTER TABLE nfi_analytical.c_areal_or_population ADD CONSTRAINT pkey__c_areal_or_population_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_areal_or_population_id ON nfi_analytical.c_areal_or_population IS 'Primary key.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_ldsity_object
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_ldsity_object;

CREATE TABLE nfi_analytical.c_ldsity_object
(
  id			integer NOT NULL,
  label			character varying(200) NOT NULL,
  description		text NOT NULL,
  label_en		character varying(200) NOT NULL,
  description_en	text NOT NULL,
  table_name		character varying(200) NOT NULL,
  upper_object		integer,
  areal_or_population	integer NOT NULL,
  column4upper_object character varying(200),
  filter text
);

COMMENT ON TABLE nfi_analytical.c_ldsity_object IS '.';
COMMENT ON COLUMN nfi_analytical.c_ldsity_object.id IS 'Identifier of ldsity object.';
COMMENT ON COLUMN nfi_analytical.c_ldsity_object.label IS 'Label of ldsity object.';
COMMENT ON COLUMN nfi_analytical.c_ldsity_object.description IS 'Description of ldsity object.';
COMMENT ON COLUMN nfi_analytical.c_ldsity_object.label_en IS 'English label of ldsity object.';
COMMENT ON COLUMN nfi_analytical.c_ldsity_object.description_en IS 'English description of ldsity object.';
COMMENT ON COLUMN nfi_analytical.c_ldsity_object.table_name IS '.';
COMMENT ON COLUMN nfi_analytical.c_ldsity_object.upper_object IS '.';
COMMENT ON COLUMN nfi_analytical.c_ldsity_object.areal_or_population IS '.';
COMMENT ON COLUMN nfi_analytical.c_ldsity_object.column4upper_object IS '.';
COMMENT ON COLUMN nfi_analytical.c_ldsity_object.filter IS '.';

ALTER TABLE nfi_analytical.c_ldsity_object ADD CONSTRAINT pkey__c_ldsity_object_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_ldsity_object_id ON nfi_analytical.c_ldsity_object IS 'Primary key.';

ALTER TABLE nfi_analytical.c_ldsity_object
  ADD CONSTRAINT fkey__c_ldsity_object__c_ldsity_object FOREIGN KEY (upper_object)
      REFERENCES nfi_analytical.c_ldsity_object (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__c_ldsity_object__c_ldsity_object ON nfi_analytical.c_ldsity_object IS
'Foreign key to table c_ldsity_object.';

ALTER TABLE nfi_analytical.c_ldsity_object
  ADD CONSTRAINT fkey__c_ldsity_object__c_areal_or_population FOREIGN KEY (areal_or_population)
      REFERENCES nfi_analytical.c_areal_or_population (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__c_ldsity_object__c_areal_or_population ON nfi_analytical.c_ldsity_object IS
'Foreign key to table c_areal_or_population.';

CREATE INDEX fki__c_ldsity_object__c_ldsity_object ON nfi_analytical.c_ldsity_object USING btree (upper_object);
COMMENT ON INDEX nfi_analytical.fki__c_ldsity_object__c_ldsity_object IS
'BTree index on foreign key fkey__c_ldsity_object__c_ldsity_object.';

CREATE INDEX fki__c_ldsity_object__c_areal_or_population ON nfi_analytical.c_ldsity_object USING btree (areal_or_population);
COMMENT ON INDEX nfi_analytical.fki__c_ldsity_object__c_areal_or_population IS
'BTree index on foreign key fkey__c_ldsity_object__c_areal_or_population.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_unit_of_measure
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_unit_of_measure;

CREATE TABLE nfi_analytical.c_unit_of_measure
(
	id		integer NOT NULL,
	label		character varying(200) NOT NULL,
	description		text NOT NULL,
	label_en	character varying(200) NOT NULL,
	description_en	text NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_unit_of_measure IS 'Table of unit of measures.';
COMMENT ON COLUMN nfi_analytical.c_unit_of_measure.id IS 'Identifier of unit of measure.';
COMMENT ON COLUMN nfi_analytical.c_unit_of_measure.label IS 'Label of unit of measure.';
COMMENT ON COLUMN nfi_analytical.c_unit_of_measure.description IS 'Description of unit of measure.';
COMMENT ON COLUMN nfi_analytical.c_unit_of_measure.label IS 'English label of unit of measure.';
COMMENT ON COLUMN nfi_analytical.c_unit_of_measure.description IS 'English description of unit of measure.';

ALTER TABLE nfi_analytical.c_unit_of_measure ADD CONSTRAINT pkey__c_unit_of_measure_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_unit_of_measure_id ON nfi_analytical.c_unit_of_measure IS 'Primary key.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_definition_variant
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_definition_variant;

CREATE TABLE nfi_analytical.c_definition_variant
(
	id		integer NOT NULL,
	label		character varying(200) NOT NULL,
	description		text NOT NULL,
	label_en	character varying(200) NOT NULL,
	description_en		text NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_definition_variant IS 'Table of definition variants.';
COMMENT ON COLUMN nfi_analytical.c_definition_variant.id IS 'Identifier of definition variant.';
COMMENT ON COLUMN nfi_analytical.c_definition_variant.label IS 'Label of definition variant.';
COMMENT ON COLUMN nfi_analytical.c_definition_variant.description IS 'Description of definition variant.';
COMMENT ON COLUMN nfi_analytical.c_definition_variant.label_en IS 'English label of definition variant.';
COMMENT ON COLUMN nfi_analytical.c_definition_variant.description_en IS 'English description of definition variant.';

ALTER TABLE nfi_analytical.c_definition_variant ADD CONSTRAINT pkey__c_definition_variant_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_definition_variant_id ON nfi_analytical.c_definition_variant IS 'Primary key.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_area_domain
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_area_domain;

CREATE TABLE nfi_analytical.c_area_domain
(
	id		integer NOT NULL,
	label		character varying(200) NOT NULL,
	description		text NOT NULL,
	label_en	character varying(200) NOT NULL,
	description_en	text NOT NULL,
	array_order	integer NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_area_domain IS 'Table of area domains, agregates of attribute domain categories detectable on plot center.';
COMMENT ON COLUMN nfi_analytical.c_area_domain.id IS 'Identifier of area domain, primary key.';
COMMENT ON COLUMN nfi_analytical.c_area_domain.label IS 'Label of area domain.';
COMMENT ON COLUMN nfi_analytical.c_area_domain.description IS 'Description of area domain.';
COMMENT ON COLUMN nfi_analytical.c_area_domain.label_en IS 'English label of area domain.';
COMMENT ON COLUMN nfi_analytical.c_area_domain.description_en IS 'English description of area domain.';
COMMENT ON COLUMN nfi_analytical.c_area_domain.array_order IS '.';

ALTER TABLE nfi_analytical.c_area_domain ADD CONSTRAINT pkey__c_area_domain_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_area_domain_id ON nfi_analytical.c_area_domain IS 'Primary key.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_area_domain_category
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_area_domain_category;

CREATE TABLE nfi_analytical.c_area_domain_category
(
	id			integer NOT NULL,
	label			character varying(200) NOT NULL,
	description			text NOT NULL,
	label_en		character varying(200) NOT NULL,
	description_en		text NOT NULL,
	area_domain		integer NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_area_domain_category IS 'Table of area domain categories, attribute domains detectable on plot center.';
COMMENT ON COLUMN nfi_analytical.c_area_domain_category.id IS 'Identifier of area domain category, primary key.';
COMMENT ON COLUMN nfi_analytical.c_area_domain_category.label IS 'Label of area domain.';
COMMENT ON COLUMN nfi_analytical.c_area_domain_category.description IS 'Description of area domain.';
COMMENT ON COLUMN nfi_analytical.c_area_domain_category.label_en IS 'English label of area domain.';
COMMENT ON COLUMN nfi_analytical.c_area_domain_category.description_en IS 'English description of area domain.';
COMMENT ON COLUMN nfi_analytical.c_area_domain_category.area_domain IS 'Identifier of area domain, foreign key to table c_area_domain.';

ALTER TABLE nfi_analytical.c_area_domain_category ADD CONSTRAINT pkey__c_area_domain_category_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_area_domain_category_id ON nfi_analytical.c_area_domain_category IS 'Primary key.';

ALTER TABLE nfi_analytical.c_area_domain_category
  ADD CONSTRAINT fkey__c_area_domain_category__c_area_domain FOREIGN KEY (area_domain)
      REFERENCES nfi_analytical.c_area_domain (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__c_area_domain_category__c_area_domain ON nfi_analytical.c_area_domain_category IS
'Foreign key to table c_area_domain.';

CREATE INDEX fki__c_area_domain_category__c_area_domain ON nfi_analytical.c_area_domain_category USING btree (area_domain);
COMMENT ON INDEX nfi_analytical.fki__c_area_domain_category__c_area_domain IS
'BTree index on foreign key fkey__c_area_domain_category__c_area_domain.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: cm_adc2classification_rule
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.cm_adc2classification_rule;

CREATE TABLE nfi_analytical.cm_adc2classification_rule
(
	id integer NOT NULL,
	area_domain_category integer NOT NULL,
	ldsity_object integer NOT NULL,
	classification_rule	text
);

COMMENT ON TABLE nfi_analytical.cm_adc2classification_rule IS '.';
COMMENT ON COLUMN nfi_analytical.cm_adc2classification_rule.id IS '.';
COMMENT ON COLUMN nfi_analytical.cm_adc2classification_rule.area_domain_category IS '.';
COMMENT ON COLUMN nfi_analytical.cm_adc2classification_rule.ldsity_object IS '.';
COMMENT ON COLUMN nfi_analytical.cm_adc2classification_rule.classification_rule IS '.';

ALTER TABLE nfi_analytical.cm_adc2classification_rule ADD CONSTRAINT pkey__cm_adc2classification_rule_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__cm_adc2classification_rule_id ON nfi_analytical.cm_adc2classification_rule IS 'Primary key.';

ALTER TABLE nfi_analytical.cm_adc2classification_rule
  ADD CONSTRAINT fkey__cm_adc2classification_rule__c_area_domain_category FOREIGN KEY (area_domain_category)
      REFERENCES nfi_analytical.c_area_domain_category (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__cm_adc2classification_rule__c_area_domain_category ON nfi_analytical.cm_adc2classification_rule IS
'Foreign key to table c_area_domain_category.';

ALTER TABLE nfi_analytical.cm_adc2classification_rule
  ADD CONSTRAINT fkey__cm_adc2classification_rule__c_ldsity_object FOREIGN KEY (ldsity_object)
      REFERENCES nfi_analytical.c_ldsity_object (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__cm_adc2classification_rule__c_ldsity_object ON nfi_analytical.cm_adc2classification_rule IS
'Foreign key to table c_ldsity_object.';

CREATE INDEX fki__cm_adc2classification_rule__c_area_domain_category ON nfi_analytical.cm_adc2classification_rule USING btree (area_domain_category);
COMMENT ON INDEX nfi_analytical.fki__cm_adc2classification_rule__c_area_domain_category IS
'BTree index on foreign key fkey__cm_adc2classification_rule__c_area_domain_category.';

-- SEQUENCE
CREATE SEQUENCE nfi_analytical.cm_adc2classification_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE nfi_analytical.cm_adc2classification_rule_id_seq OWNED BY nfi_analytical.cm_adc2classification_rule.id;
ALTER TABLE ONLY nfi_analytical.cm_adc2classification_rule ALTER COLUMN id SET DEFAULT nextval('nfi_analytical.cm_adc2classification_rule_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_sub_population
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_sub_population;

CREATE TABLE nfi_analytical.c_sub_population
(
	id		integer NOT NULL,
	label		character varying(200) NOT NULL,
	description		text NOT NULL,
	label_en	character varying(200) NOT NULL,
	description_en	text NOT NULL,
	array_order	integer NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_sub_population IS 'Table of sub-populations, agregates of attribute domain categories detectable on measured objects (several entities on 1 plot center).';
COMMENT ON COLUMN nfi_analytical.c_sub_population.id IS 'Identifier of sub-population, primary key.';
COMMENT ON COLUMN nfi_analytical.c_sub_population.label IS 'Label of sub-population.';
COMMENT ON COLUMN nfi_analytical.c_sub_population.description IS 'Description of sub-population.';
COMMENT ON COLUMN nfi_analytical.c_sub_population.label_en IS 'English label of sub-population.';
COMMENT ON COLUMN nfi_analytical.c_sub_population.description_en IS 'English description of sub-population.';
COMMENT ON COLUMN nfi_analytical.c_sub_population.array_order IS '.';

ALTER TABLE nfi_analytical.c_sub_population ADD CONSTRAINT pkey__c_sub_population_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_sub_population_id ON nfi_analytical.c_sub_population IS 'Primary key.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_sub_population_category
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_sub_population_category;

CREATE TABLE nfi_analytical.c_sub_population_category
(
	id			integer NOT NULL,
	label			character varying(200) NOT NULL,
	description		text NOT NULL,
	label_en		character varying(200) NOT NULL,
	description_en	text NOT NULL,
	sub_population		integer NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_sub_population_category IS 'Table of sub-population categories, attribute domains detectable on measured objects (several entities on 1 plot center).';
COMMENT ON COLUMN nfi_analytical.c_sub_population_category.id IS 'Identifier of sub-population category, primary key.';
COMMENT ON COLUMN nfi_analytical.c_sub_population_category.label IS 'Label of sub-population category.';
COMMENT ON COLUMN nfi_analytical.c_sub_population_category.description IS 'Description of sub-population category.';
COMMENT ON COLUMN nfi_analytical.c_sub_population_category.label_en IS 'English label of sub-population category.';
COMMENT ON COLUMN nfi_analytical.c_sub_population_category.description_en IS 'English description of sub-population category.';
COMMENT ON COLUMN nfi_analytical.c_sub_population_category.sub_population IS 'Identifier of sub-population, foreign key to table c_sub_population.';

ALTER TABLE nfi_analytical.c_sub_population_category ADD CONSTRAINT pkey__c_sub_population_category_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_sub_population_category_id ON nfi_analytical.c_sub_population_category IS 'Primary key.';

ALTER TABLE nfi_analytical.c_sub_population_category
  ADD CONSTRAINT fkey__c_sub_population_category__c_sub_population FOREIGN KEY (sub_population)
      REFERENCES nfi_analytical.c_sub_population (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__c_sub_population_category__c_sub_population ON nfi_analytical.c_sub_population_category IS
'Foreign key to table c_sub_population.';

CREATE INDEX fki__c_sub_population_category__c_sub_population ON nfi_analytical.c_sub_population_category USING btree (sub_population);
COMMENT ON INDEX nfi_analytical.fki__c_sub_population_category__c_sub_population IS
'BTree index on foreign key fkey__c_sub_population_category__c_sub_population.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: cm_spc2classification_rule
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.cm_spc2classification_rule;

CREATE TABLE nfi_analytical.cm_spc2classification_rule
(
	id integer NOT NULL,
	sub_population_category integer NOT NULL,
	ldsity_object integer NOT NULL,
	classification_rule	text
);

COMMENT ON TABLE nfi_analytical.cm_spc2classification_rule IS '.';
COMMENT ON COLUMN nfi_analytical.cm_spc2classification_rule.id IS '.';
COMMENT ON COLUMN nfi_analytical.cm_spc2classification_rule.sub_population_category IS '.';
COMMENT ON COLUMN nfi_analytical.cm_spc2classification_rule.ldsity_object IS '.';
COMMENT ON COLUMN nfi_analytical.cm_spc2classification_rule.classification_rule IS '.';

ALTER TABLE nfi_analytical.cm_spc2classification_rule ADD CONSTRAINT pkey__cm_spc2classification_rule_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__cm_spc2classification_rule_id ON nfi_analytical.cm_spc2classification_rule IS 'Primary key.';

ALTER TABLE nfi_analytical.cm_spc2classification_rule
  ADD CONSTRAINT fkey__cm_spc2classification_rule__c_sub_population_category FOREIGN KEY (sub_population_category)
      REFERENCES nfi_analytical.c_sub_population_category (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__cm_spc2classification_rule__c_sub_population_category ON nfi_analytical.cm_spc2classification_rule IS
'Foreign key to table c_sub_population_category.';

ALTER TABLE nfi_analytical.cm_spc2classification_rule
  ADD CONSTRAINT fkey__cm_spc2classification_rule__c_ldsity_object FOREIGN KEY (ldsity_object)
      REFERENCES nfi_analytical.c_ldsity_object (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__cm_spc2classification_rule__c_ldsity_object ON nfi_analytical.cm_spc2classification_rule IS
'Foreign key to table c_ldsity_object.';

CREATE INDEX fki__cm_spc2classification_rule__c_sub_population_category ON nfi_analytical.cm_spc2classification_rule USING btree (sub_population_category);
COMMENT ON INDEX nfi_analytical.fki__cm_spc2classification_rule__c_sub_population_category IS
'BTree index on foreign key fkey__cm_spc2classification_rule__c_sub_population_category.';

-- SEQUENCE
CREATE SEQUENCE nfi_analytical.cm_spc2classification_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE nfi_analytical.cm_spc2classification_rule_id_seq OWNED BY nfi_analytical.cm_spc2classification_rule.id;
ALTER TABLE ONLY nfi_analytical.cm_spc2classification_rule ALTER COLUMN id SET DEFAULT nextval('nfi_analytical.cm_spc2classification_rule_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_ldsity
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_ldsity;

CREATE TABLE nfi_analytical.c_ldsity
(
  id				integer NOT NULL,
  label				character varying(200) NOT NULL,
  description		text NOT NULL,
  label_en			character varying(200) NOT NULL,
  description_en	text NOT NULL,
  ldsity_object			integer NOT NULL,
  column_expression		text NOT NULL,
  unit_of_measure		integer,
  area_domain_category		integer[],
  sub_population_category	integer[],
  definition_variant		integer[]
);

COMMENT ON TABLE nfi_analytical.c_ldsity IS '.';
COMMENT ON COLUMN nfi_analytical.c_ldsity.id IS 'Identifier of ldsity, primary key.';
COMMENT ON COLUMN nfi_analytical.c_ldsity.label IS 'Label of ldsity.';
COMMENT ON COLUMN nfi_analytical.c_ldsity.description IS 'Description of ldsity.';
COMMENT ON COLUMN nfi_analytical.c_ldsity.label_en IS 'English label of ldsity.';
COMMENT ON COLUMN nfi_analytical.c_ldsity.description_en IS 'English description of ldsity.';
COMMENT ON COLUMN nfi_analytical.c_ldsity.ldsity_object IS 'Identifier of ldstity object, foreign key to c_ldsity_object.';
COMMENT ON COLUMN nfi_analytical.c_ldsity.column_expression IS '.';
COMMENT ON COLUMN nfi_analytical.c_ldsity.unit_of_measure IS 'Identifier of unit of measure, foreign key to c_unit_of_measure.';
COMMENT ON COLUMN nfi_analytical.c_ldsity.area_domain_category IS 'Array of identifiers of area domain category, array of foreign keys to cm_adc2conversion_string.';
COMMENT ON COLUMN nfi_analytical.c_ldsity.sub_population_category IS 'Array of identifiers of sub-population category, array of foreign keys to cm_spc2conversion_string.';
COMMENT ON COLUMN nfi_analytical.c_ldsity.definition_variant IS 'Array of identifiers of definition variant, array of foreign keys to c_definition_variant.';

ALTER TABLE nfi_analytical.c_ldsity ADD CONSTRAINT pkey__c_ldsity_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_ldsity_id ON nfi_analytical.c_ldsity IS 'Primary key.';

ALTER TABLE nfi_analytical.c_ldsity
  ADD CONSTRAINT fkey__c_ldsity__c_ldsity_object FOREIGN KEY (ldsity_object)
      REFERENCES nfi_analytical.c_ldsity_object (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__c_ldsity__c_ldsity_object ON nfi_analytical.c_ldsity IS
'Foreign key to table c_ldsity_object.';

ALTER TABLE nfi_analytical.c_ldsity
  ADD CONSTRAINT fkey__c_ldsity__c_unit_of_measure FOREIGN KEY (unit_of_measure)
      REFERENCES nfi_analytical.c_unit_of_measure (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__c_ldsity__c_unit_of_measure ON nfi_analytical.c_ldsity IS
'Foreign key to table c_unit_of_measure.';

CREATE INDEX fki__c_ldsity__c_ldsity_object ON nfi_analytical.c_ldsity USING btree (ldsity_object);
COMMENT ON INDEX nfi_analytical.fki__c_ldsity__c_ldsity_object IS
'BTree index on foreign key fkey__c_ldsity__c_ldsity_object.';

CREATE INDEX fki__c_ldsity__c_unit_of_measure ON nfi_analytical.c_ldsity USING btree (unit_of_measure);
COMMENT ON INDEX nfi_analytical.fki__c_ldsity__c_unit_of_measure IS
'BTree index on foreign key fkey__c_ldsity__c_unit_of_measure.';

-- SEQUENCE
CREATE SEQUENCE nfi_analytical.c_ldsity_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE nfi_analytical.c_ldsity_id_seq OWNED BY nfi_analytical.c_ldsity.id;
ALTER TABLE ONLY nfi_analytical.c_ldsity ALTER COLUMN id SET DEFAULT nextval('nfi_analytical.c_ldsity_id_seq'::regclass);
--------------------------------------------------------------------;

--------------------------------------------------------------------;
-- Table: c_version
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_version;

CREATE TABLE nfi_analytical.c_version
(
	id		integer NOT NULL,
	label		character varying(200) NOT NULL,
	description	text NOT NULL,
	label_en character varying(200),
	description_en text NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_version IS 'Table of versions.';
COMMENT ON COLUMN nfi_analytical.c_version.id IS 'Identifier of version.';
COMMENT ON COLUMN nfi_analytical.c_version.label IS 'Label of version.';
COMMENT ON COLUMN nfi_analytical.c_version.description IS 'Description of version.';
COMMENT ON COLUMN nfi_analytical.c_version.label_en IS 'English label of version.';
COMMENT ON COLUMN nfi_analytical.c_version.description_en IS 'English description of version.';

ALTER TABLE nfi_analytical.c_version ADD CONSTRAINT pkey__c_version_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_version_id ON nfi_analytical.c_version IS 'Primary key.';
--------------------------------------------------------------------;

--------------------------------------------------------------------;
-- Table: cm_ldsity2panel_refyearset_version
--------------------------------------------------------------------;

CREATE TABLE nfi_analytical.cm_ldsity2panel_refyearset_version (
id 			serial,
ldsity			integer,
refyearset2panel	integer,
version			integer,
CONSTRAINT pkey__cm_ldsity2panel_refyearset_version__id PRIMARY KEY (id)
);

ALTER TABLE nfi_analytical.cm_ldsity2panel_refyearset_version ADD CONSTRAINT
fkey__cm_ldsity2panel_refyearset_version__c_ldsity FOREIGN KEY (ldsity)
REFERENCES nfi_analytical.c_ldsity(id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE nfi_analytical.cm_ldsity2panel_refyearset_version ADD CONSTRAINT
fkey__cm_ldsity2panel_refyearset_version__cm_ryset2panel FOREIGN KEY (refyearset2panel)
REFERENCES sdesign.cm_refyearset2panel_mapping(id) ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE nfi_analytical.cm_ldsity2panel_refyearset_version ADD CONSTRAINT
fkey__cm_ldsity2panel_refyearset_version__c_version FOREIGN KEY (version)
REFERENCES nfi_analytical.c_version(id) ON UPDATE CASCADE ON DELETE NO ACTION;

CREATE INDEX fki__cm_ldsity2panel_refyearset_version__c_ldsity ON nfi_analytical.cm_ldsity2panel_refyearset_version USING btree (ldsity);
COMMENT ON INDEX nfi_analytical.fki__cm_ldsity2panel_refyearset_version__c_ldsity IS
'BTree index on foreign key fkey__cm_ldsity2panel_refyearset_version__c_ldsity.';

CREATE INDEX fki__cm_ldsity2panel_refyearset_version__cm_ryset2panel ON nfi_analytical.cm_ldsity2panel_refyearset_version USING btree (refyearset2panel);
COMMENT ON INDEX nfi_analytical.fki__cm_ldsity2panel_refyearset_version__cm_ryset2panel IS
'BTree index on foreign key fkey__cm_ldsity2panel_refyearset_version__cm_ryset2panel.';

CREATE INDEX fki__cm_ldsity2panel_refyearset_version__c_version ON nfi_analytical.cm_ldsity2panel_refyearset_version USING btree (version);
COMMENT ON INDEX nfi_analytical.fki__cm_ldsity2panel_refyearset_version__c_version IS
'BTree index on foreign key fkey__cm_ldsity2panel_refyearset_version__c_version.';

COMMENT ON TABLE nfi_analytical.cm_ldsity2panel_refyearset_version IS 'Mapping table between local density contribution, panels and reference year sets and also versions. Its purpose is to model a spatial and time availability of the data according to their versions.';

COMMENT ON COLUMN nfi_analytical.cm_ldsity2panel_refyearset_version.id IS 'Primary key, identificator of the record.';
COMMENT ON COLUMN nfi_analytical.cm_ldsity2panel_refyearset_version.ldsity IS 'Identifier of the local density contribution, foreign key to table c_ldsity.';
COMMENT ON COLUMN nfi_analytical.cm_ldsity2panel_refyearset_version.refyearset2panel IS 'Identifier of the panel and reference year set combination, foreign key to table cm_refyearset2panel_mapping.';
COMMENT ON COLUMN nfi_analytical.cm_ldsity2panel_refyearset_version.version IS 'Identifier of the version, foreign key to table c_version.';

--------------------------------------------------------------------;
-- Table: c_fao_fra_forest
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_fao_fra_forest;

CREATE TABLE nfi_analytical.c_fao_fra_forest
(
	id		integer NOT NULL,
	label		character varying(200) NOT NULL,
	description	text NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_fao_fra_forest IS 'Table of fao fra forest.';
COMMENT ON COLUMN nfi_analytical.c_fao_fra_forest.id IS 'Identifier of fao fra forest.';
COMMENT ON COLUMN nfi_analytical.c_fao_fra_forest.label IS 'Label of fao fra forest.';
COMMENT ON COLUMN nfi_analytical.c_fao_fra_forest.description IS 'Description of fao fra forest.';

ALTER TABLE nfi_analytical.c_fao_fra_forest ADD CONSTRAINT pkey__c_fao_fra_forest_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_fao_fra_forest_id ON nfi_analytical.c_fao_fra_forest IS 'Primary key.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_land_register
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_land_register;

CREATE TABLE nfi_analytical.c_land_register
(
	id		integer NOT NULL,
	label		character varying(200) NOT NULL,
	description	text NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_land_register IS 'Table of land register.';
COMMENT ON COLUMN nfi_analytical.c_land_register.id IS 'Identifier of land register.';
COMMENT ON COLUMN nfi_analytical.c_land_register.label IS 'Label of land register.';
COMMENT ON COLUMN nfi_analytical.c_land_register.description IS 'Description of land register.';

ALTER TABLE nfi_analytical.c_land_register ADD CONSTRAINT pkey__c_land_register_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_land_register_id ON nfi_analytical.c_land_register IS 'Primary key.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_con_dec
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_con_dec;

CREATE TABLE nfi_analytical.c_con_dec
(
	id		integer NOT NULL,
	label		character varying(200) NOT NULL,
	description	text NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_con_dec IS 'Table of conifer or decidious.';
COMMENT ON COLUMN nfi_analytical.c_con_dec.id IS 'Identifier of conifer or decidious.';
COMMENT ON COLUMN nfi_analytical.c_con_dec.label IS 'Label of conifer or decidious.';
COMMENT ON COLUMN nfi_analytical.c_con_dec.description IS 'Description of conifer or decidious.';

ALTER TABLE nfi_analytical.c_con_dec ADD CONSTRAINT pkey__c_con_dec_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_con_dec_id ON nfi_analytical.c_con_dec IS 'Primary key.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_species
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_species;

CREATE TABLE nfi_analytical.c_species
(
	id		integer NOT NULL,
	label		character varying(200) NOT NULL,
	description	text NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_species IS 'Table of species.';
COMMENT ON COLUMN nfi_analytical.c_species.id IS 'Identifier of species.';
COMMENT ON COLUMN nfi_analytical.c_species.label IS 'Label of species.';
COMMENT ON COLUMN nfi_analytical.c_species.description IS 'Description of species.';

ALTER TABLE nfi_analytical.c_species ADD CONSTRAINT pkey__c_species_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_species_id ON nfi_analytical.c_species IS 'Primary key.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_dead_stem
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.c_dead_stem;

CREATE TABLE nfi_analytical.c_dead_stem
(
	id		integer NOT NULL,
	label		character varying(200) NOT NULL,
	description	text NOT NULL
);

COMMENT ON TABLE nfi_analytical.c_dead_stem IS 'Table of dead stem.';
COMMENT ON COLUMN nfi_analytical.c_dead_stem.id IS 'Identifier of dead stem.';
COMMENT ON COLUMN nfi_analytical.c_dead_stem.label IS 'Label of dead stem.';
COMMENT ON COLUMN nfi_analytical.c_dead_stem.description IS 'Description of dead stem.';

ALTER TABLE nfi_analytical.c_dead_stem ADD CONSTRAINT pkey__c_dead_stem_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__c_dead_stem_id ON nfi_analytical.c_dead_stem IS 'Primary key.';
--------------------------------------------------------------------;

--------------------------------------------------------------------;
-- Table: t_plots
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.t_plots;

CREATE TABLE nfi_analytical.t_plots
(
  id integer NOT NULL,
  plot integer,
  reference_year_set integer,
  fao_fra_forest integer NOT NULL,
  land_register integer NOT NULL,
  ld_forest integer NOT NULL,
  version integer NOT NULL,
  is_latest boolean NOT NULL
);

COMMENT ON TABLE nfi_analytical.t_plots IS '.';
COMMENT ON COLUMN nfi_analytical.t_plots.id IS 'Identifier of plot, primary key.';
COMMENT ON COLUMN nfi_analytical.t_plots.plot IS 'Identifier of plot, foreign key to sdesign.f_p_plot.';
COMMENT ON COLUMN nfi_analytical.t_plots.reference_year_set IS 'Identifier of reference year set, foreign key to sdesign.reference_year_set.';
COMMENT ON COLUMN nfi_analytical.t_plots.fao_fra_forest IS 'Identifier of fao fra forest, foreign key to c_fao_fra_forest.';
COMMENT ON COLUMN nfi_analytical.t_plots.land_register IS 'Identifier of land register, foreign key to c_land_register.';
COMMENT ON COLUMN nfi_analytical.t_plots.ld_forest IS '.';
COMMENT ON COLUMN nfi_analytical.t_plots.version IS 'Identifier of version, foreign key to c_version.';
COMMENT ON COLUMN nfi_analytical.t_plots.is_latest IS 'Identifier of latest valid version of record.';

ALTER TABLE nfi_analytical.t_plots ADD CONSTRAINT pkey__t_plots_gid PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__t_plots_gid ON nfi_analytical.t_plots IS 'Primary key.';

ALTER TABLE nfi_analytical.t_plots
  ADD CONSTRAINT fkey__t_plots__t_reference_year_set FOREIGN KEY (reference_year_set)
      REFERENCES sdesign.t_reference_year_set (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_plots__t_reference_year_set ON nfi_analytical.t_plots IS
'Foreign key to table sdesign.t_reference_year_set.';

ALTER TABLE nfi_analytical.t_plots
  ADD CONSTRAINT fkey__t_plots__f_p_plot FOREIGN KEY (plot)
      REFERENCES sdesign.f_p_plot (gid) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_plots__f_p_plot ON nfi_analytical.t_plots IS
'Foreign key to table sdesign.f_p_plot.';

ALTER TABLE nfi_analytical.t_plots
  ADD CONSTRAINT fkey__t_plots__c_fao_fra_forest FOREIGN KEY (fao_fra_forest)
      REFERENCES nfi_analytical.c_fao_fra_forest (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_plots__c_fao_fra_forest ON nfi_analytical.t_plots IS
'Foreign key to table c_fao_fra_forest.';

ALTER TABLE nfi_analytical.t_plots
  ADD CONSTRAINT fkey__t_plots__c_land_register FOREIGN KEY (land_register)
      REFERENCES nfi_analytical.c_land_register (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_plots__c_land_register ON nfi_analytical.t_plots IS
'Foreign key to table c_land_register.';

ALTER TABLE nfi_analytical.t_plots
  ADD CONSTRAINT fkey__t_plots__c_version FOREIGN KEY (version)
      REFERENCES nfi_analytical.c_version (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_plots__c_version ON nfi_analytical.t_plots IS
'Foreign key to table c_version.';

CREATE INDEX fki__t_plots__c_fao_fra_forest ON nfi_analytical.t_plots USING btree (fao_fra_forest);
COMMENT ON INDEX nfi_analytical.fki__t_plots__c_fao_fra_forest IS
'BTree index on foreign key c_fao_fra_forest.';

CREATE INDEX fki__t_plots__c_land_register ON nfi_analytical.t_plots USING btree (land_register);
COMMENT ON INDEX nfi_analytical.fki__t_plots__c_land_register IS
'BTree index on foreign key c_land_register.';

CREATE INDEX fki__t_plots__c_version ON nfi_analytical.t_plots USING btree (version);
COMMENT ON INDEX nfi_analytical.fki__t_plots__c_version IS
'BTree index on foreign key c_version.';

-- SEQUENCE
CREATE SEQUENCE nfi_analytical.t_plots_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE nfi_analytical.t_plots_id_seq OWNED BY nfi_analytical.t_plots.id;
ALTER TABLE ONLY nfi_analytical.t_plots ALTER COLUMN id SET DEFAULT nextval('nfi_analytical.t_plots_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: f_p_stems
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.f_p_stems;

CREATE TABLE nfi_analytical.f_p_stems
(
  gid integer NOT NULL,
  plots integer NOT NULL,
  con_dec integer,
  species integer,
  dead_stem integer,
  izone_ha double precision,
  ld_stem_count_ksha double precision,
  crown_proj_m2 double precision,
  ld_crown_proj_m2ha double precision,
  version integer NOT NULL,
  is_latest boolean NOT NULL
);

COMMENT ON TABLE nfi_analytical.f_p_stems IS '.';
COMMENT ON COLUMN nfi_analytical.f_p_stems.gid IS '.';
COMMENT ON COLUMN nfi_analytical.f_p_stems.plots IS '.';
COMMENT ON COLUMN nfi_analytical.f_p_stems.con_dec IS '.';
COMMENT ON COLUMN nfi_analytical.f_p_stems.izone_ha IS '.';
COMMENT ON COLUMN nfi_analytical.f_p_stems.ld_stem_count_ksha IS '.';
COMMENT ON COLUMN nfi_analytical.f_p_stems.crown_proj_m2 IS '.';
COMMENT ON COLUMN nfi_analytical.f_p_stems.ld_crown_proj_m2ha IS '.';
COMMENT ON COLUMN nfi_analytical.f_p_stems.version IS 'Identifier of version, foreign key to c_version.';
COMMENT ON COLUMN nfi_analytical.f_p_stems.is_latest IS 'Identifier of latest valid version of record.';

ALTER TABLE nfi_analytical.f_p_stems ADD CONSTRAINT pkey__f_p_stems_gid PRIMARY KEY(gid);
COMMENT ON CONSTRAINT pkey__f_p_stems_gid ON nfi_analytical.f_p_stems IS 'Primary key.';

ALTER TABLE nfi_analytical.f_p_stems
  ADD CONSTRAINT fkey__f_p_stems__t_plots FOREIGN KEY (plots)
      REFERENCES nfi_analytical.t_plots (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__f_p_stems__t_plots ON nfi_analytical.f_p_stems IS
'Foreign key to table t_plots.';

ALTER TABLE nfi_analytical.f_p_stems
  ADD CONSTRAINT fkey__f_p_stems__c_con_dec FOREIGN KEY (con_dec)
      REFERENCES nfi_analytical.c_con_dec (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__f_p_stems__c_con_dec ON nfi_analytical.f_p_stems IS
'Foreign key to table c_con_dec.';

ALTER TABLE nfi_analytical.f_p_stems
  ADD CONSTRAINT fkey__f_p_stems__c_species FOREIGN KEY (species)
      REFERENCES nfi_analytical.c_species (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__f_p_stems__c_species ON nfi_analytical.f_p_stems IS
'Foreign key to table c_species.';

ALTER TABLE nfi_analytical.f_p_stems
  ADD CONSTRAINT fkey__f_p_stems__c_dead_stem FOREIGN KEY (dead_stem)
      REFERENCES nfi_analytical.c_dead_stem (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__f_p_stems__c_dead_stem ON nfi_analytical.f_p_stems IS
'Foreign key to table c_dead_stem.';

ALTER TABLE nfi_analytical.f_p_stems
  ADD CONSTRAINT fkey__f_p_stems__c_version FOREIGN KEY (version)
      REFERENCES nfi_analytical.c_version (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__f_p_stems__c_version ON nfi_analytical.f_p_stems IS
'Foreign key to table c_version.';

CREATE INDEX fki__f_p_stems__t_plots ON nfi_analytical.f_p_stems USING btree (plots);
COMMENT ON INDEX nfi_analytical.fki__f_p_stems__t_plots IS
'BTree index on foreign key t_plots.';

CREATE INDEX fki__f_p_stems__c_con_dec ON nfi_analytical.f_p_stems USING btree (con_dec);
COMMENT ON INDEX nfi_analytical.fki__f_p_stems__c_con_dec IS
'BTree index on foreign key c_con_dec.';

CREATE INDEX fki__f_p_stems__c_species ON nfi_analytical.f_p_stems USING btree (species);
COMMENT ON INDEX nfi_analytical.fki__f_p_stems__c_species IS
'BTree index on foreign key c_species.';

CREATE INDEX fki__f_p_stems__c_dead_stem ON nfi_analytical.f_p_stems USING btree (dead_stem);
COMMENT ON INDEX nfi_analytical.fki__f_p_stems__c_dead_stem IS
'BTree index on foreign key c_dead_stem.';

CREATE INDEX fki__f_p_stems__c_version ON nfi_analytical.f_p_stems USING btree (version);
COMMENT ON INDEX nfi_analytical.fki__f_p_stems__c_version IS
'BTree index on foreign key c_version.';

-- SEQUENCE
CREATE SEQUENCE nfi_analytical.f_p_stems_gid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE nfi_analytical.f_p_stems_gid_seq OWNED BY nfi_analytical.f_p_stems.gid;
ALTER TABLE ONLY nfi_analytical.f_p_stems ALTER COLUMN gid SET DEFAULT nextval('nfi_analytical.f_p_stems_gid_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: t_regeneration
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.t_regeneration;

CREATE TABLE nfi_analytical.t_regeneration
(
  id integer NOT NULL,
  plots integer NOT NULL,
  version integer NOT NULL,
  is_latest boolean NOT NULL
);

COMMENT ON TABLE nfi_analytical.t_regeneration IS '.';
COMMENT ON COLUMN nfi_analytical.t_regeneration.id IS '.';
COMMENT ON COLUMN nfi_analytical.t_regeneration.plots IS '.';
COMMENT ON COLUMN nfi_analytical.t_regeneration.version IS 'Identifier of version, foreign key to c_version.';
COMMENT ON COLUMN nfi_analytical.t_regeneration.is_latest IS 'Identifier of latest valid version of record.';

ALTER TABLE nfi_analytical.t_regeneration ADD CONSTRAINT pkey__t_regeneration_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__t_regeneration_id ON nfi_analytical.t_regeneration IS 'Primary key.';

ALTER TABLE nfi_analytical.t_regeneration
  ADD CONSTRAINT fkey__t_regeneration__t_plots FOREIGN KEY (plots)
      REFERENCES nfi_analytical.t_plots (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_regeneration__t_plots ON nfi_analytical.t_regeneration IS
'Foreign key to table t_plots.';

ALTER TABLE nfi_analytical.t_regeneration
  ADD CONSTRAINT fkey__t_regeneration__c_version FOREIGN KEY (version)
      REFERENCES nfi_analytical.c_version (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_regeneration__c_version ON nfi_analytical.t_regeneration IS
'Foreign key to table c_version.';

CREATE INDEX fki__t_regeneration__t_plots ON nfi_analytical.t_regeneration USING btree (plots);
COMMENT ON INDEX nfi_analytical.fki__t_regeneration__t_plots IS
'BTree index on foreign key t_plots.';

CREATE INDEX fki__t_regeneration__c_version ON nfi_analytical.t_regeneration USING btree (version);
COMMENT ON INDEX nfi_analytical.fki__t_regeneration__c_version IS
'BTree index on foreign key c_version.';

-- SEQUENCE
CREATE SEQUENCE nfi_analytical.t_regeneration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE nfi_analytical.t_regeneration_id_seq OWNED BY nfi_analytical.t_regeneration.id;
ALTER TABLE ONLY nfi_analytical.t_regeneration ALTER COLUMN id SET DEFAULT nextval('nfi_analytical.t_regeneration_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: t_regenerations
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.t_regenerations;

CREATE TABLE nfi_analytical.t_regenerations
(
  id integer NOT NULL,
  regeneration integer NOT NULL,
  con_dec integer NOT NULL,
  species integer NOT NULL,
  izone_ha double precision,
  ld_stem_count_ksha double precision,
  crown_proj_m2 double precision,
  ld_crown_proj_m2ha double precision,
  version integer NOT NULL,
  is_latest boolean NOT NULL
);

COMMENT ON TABLE nfi_analytical.t_regenerations IS '.';
COMMENT ON COLUMN nfi_analytical.t_regenerations.id IS '.';
COMMENT ON COLUMN nfi_analytical.t_regenerations.regeneration IS '.';
COMMENT ON COLUMN nfi_analytical.t_regenerations.con_dec IS '.';
COMMENT ON COLUMN nfi_analytical.t_regenerations.species IS '.';
COMMENT ON COLUMN nfi_analytical.t_regenerations.izone_ha IS '.';
COMMENT ON COLUMN nfi_analytical.t_regenerations.ld_stem_count_ksha IS '.';
COMMENT ON COLUMN nfi_analytical.t_regenerations.crown_proj_m2 IS '.';
COMMENT ON COLUMN nfi_analytical.t_regenerations.ld_crown_proj_m2ha IS '.';
COMMENT ON COLUMN nfi_analytical.t_regenerations.version IS 'Identifier of version, foreign key to c_version.';
COMMENT ON COLUMN nfi_analytical.t_regenerations.is_latest IS 'Identifier of latest valid version of record.';

ALTER TABLE nfi_analytical.t_regenerations ADD CONSTRAINT pkey__t_regenerations_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__t_regenerations_id ON nfi_analytical.t_regenerations IS 'Primary key.';

ALTER TABLE nfi_analytical.t_regenerations
  ADD CONSTRAINT fkey__t_regenerations__t_regeneration FOREIGN KEY (regeneration)
      REFERENCES nfi_analytical.t_regeneration (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_regenerations__t_regeneration ON nfi_analytical.t_regenerations IS
'Foreign key to table t_regeneration.';

ALTER TABLE nfi_analytical.t_regenerations
  ADD CONSTRAINT fkey__t_regenerations__c_con_dec FOREIGN KEY (con_dec)
      REFERENCES nfi_analytical.c_con_dec (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_regenerations__c_con_dec ON nfi_analytical.t_regenerations IS
'Foreign key to table c_con_dec.';

ALTER TABLE nfi_analytical.t_regenerations
  ADD CONSTRAINT fkey__t_regenerations__c_species FOREIGN KEY (species)
      REFERENCES nfi_analytical.c_species (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_regenerations__c_species ON nfi_analytical.t_regenerations IS
'Foreign key to table c_species.';

ALTER TABLE nfi_analytical.t_regenerations
  ADD CONSTRAINT fkey__t_regenerations__c_version FOREIGN KEY (version)
      REFERENCES nfi_analytical.c_version (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_regenerations__c_version ON nfi_analytical.t_regenerations IS
'Foreign key to table c_version.';

CREATE INDEX fki__t_regenerations__t_regeneration ON nfi_analytical.t_regenerations USING btree (regeneration);
COMMENT ON INDEX nfi_analytical.fki__t_regenerations__t_regeneration IS
'BTree index on foreign key t_regeneration.';

CREATE INDEX fki__t_regenerations__c_con_dec ON nfi_analytical.t_regenerations USING btree (con_dec);
COMMENT ON INDEX nfi_analytical.fki__t_regenerations__c_con_dec IS
'BTree index on foreign key c_con_dec.';

CREATE INDEX fki__t_regenerations__c_species ON nfi_analytical.t_regenerations USING btree (species);
COMMENT ON INDEX nfi_analytical.fki__t_regenerations__c_species IS
'BTree index on foreign key c_species.';

CREATE INDEX fki__t_regenerations__c_version ON nfi_analytical.t_regenerations USING btree (version);
COMMENT ON INDEX nfi_analytical.fki__t_regenerations__c_version IS
'BTree index on foreign key c_version.';

-- SEQUENCE
CREATE SEQUENCE nfi_analytical.t_regenerations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE nfi_analytical.t_regenerations_id_seq OWNED BY nfi_analytical.t_regenerations.id;
ALTER TABLE ONLY nfi_analytical.t_regenerations ALTER COLUMN id SET DEFAULT nextval('nfi_analytical.t_regenerations_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: t_adc_hierarchy
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.t_adc_hierarchy;

CREATE TABLE nfi_analytical.t_adc_hierarchy
(
  id integer NOT NULL,
  variable_superior integer NOT NULL,
  variable integer NOT NULL,
  dependent boolean NOT NULL
);

COMMENT ON TABLE nfi_analytical.t_adc_hierarchy IS '.';
COMMENT ON COLUMN nfi_analytical.t_adc_hierarchy.id IS '.';
COMMENT ON COLUMN nfi_analytical.t_adc_hierarchy.variable_superior IS '.';
COMMENT ON COLUMN nfi_analytical.t_adc_hierarchy.variable IS '.';
COMMENT ON COLUMN nfi_analytical.t_adc_hierarchy.dependent IS '.';

ALTER TABLE nfi_analytical.t_adc_hierarchy ADD CONSTRAINT pkey__t_adc_hierarchy_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__t_adc_hierarchy_id ON nfi_analytical.t_adc_hierarchy IS 'Primary key.';

ALTER TABLE nfi_analytical.t_adc_hierarchy
  ADD CONSTRAINT fkey__t_adc_hierarchy__c_area_domain_category_1 FOREIGN KEY (variable_superior)
      REFERENCES nfi_analytical.c_area_domain_category (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_adc_hierarchy__c_area_domain_category_1 ON nfi_analytical.t_adc_hierarchy IS
'Foreign key to table c_area_domain_category.';

ALTER TABLE nfi_analytical.t_adc_hierarchy
  ADD CONSTRAINT fkey__t_adc_hierarchy__c_area_domain_category_2 FOREIGN KEY (variable)
      REFERENCES nfi_analytical.c_area_domain_category (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_adc_hierarchy__c_area_domain_category_2 ON nfi_analytical.t_adc_hierarchy IS
'Foreign key to table c_area_domain_category.';

CREATE INDEX fki__t_adc_hierarchy__c_area_domain_category_1 ON nfi_analytical.t_adc_hierarchy USING btree (variable_superior);
COMMENT ON INDEX nfi_analytical.fki__t_adc_hierarchy__c_area_domain_category_1 IS
'BTree index on foreign key fkey__t_adc_hierarchy__c_area_domain_category_1.';

CREATE INDEX fki__t_adc_hierarchy__c_area_domain_category_2 ON nfi_analytical.t_adc_hierarchy USING btree (variable);
COMMENT ON INDEX nfi_analytical.fki__t_adc_hierarchy__c_area_domain_category_2 IS
'BTree index on foreign key fkey__t_adc_hierarchy__c_area_domain_category_2.';

-- SEQUENCE
CREATE SEQUENCE nfi_analytical.t_adc_hierarchy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE nfi_analytical.t_adc_hierarchy_id_seq OWNED BY nfi_analytical.t_adc_hierarchy.id;
ALTER TABLE ONLY nfi_analytical.t_adc_hierarchy ALTER COLUMN id SET DEFAULT nextval('nfi_analytical.t_adc_hierarchy_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: t_spc_hierarchy
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.t_spc_hierarchy;

CREATE TABLE nfi_analytical.t_spc_hierarchy
(
  id integer NOT NULL,
  variable_superior integer NOT NULL,
  variable integer NOT NULL,
  dependent boolean NOT NULL
);

COMMENT ON TABLE nfi_analytical.t_spc_hierarchy IS '.';
COMMENT ON COLUMN nfi_analytical.t_spc_hierarchy.id IS '.';
COMMENT ON COLUMN nfi_analytical.t_spc_hierarchy.variable_superior IS '.';
COMMENT ON COLUMN nfi_analytical.t_spc_hierarchy.variable IS '.';
COMMENT ON COLUMN nfi_analytical.t_spc_hierarchy.dependent IS '.';

ALTER TABLE nfi_analytical.t_spc_hierarchy ADD CONSTRAINT pkey__t_spc_hierarchy_id PRIMARY KEY(id);
COMMENT ON CONSTRAINT pkey__t_spc_hierarchy_id ON nfi_analytical.t_spc_hierarchy IS 'Primary key.';

ALTER TABLE nfi_analytical.t_spc_hierarchy
  ADD CONSTRAINT fkey__t_spc_hierarchy__c_sub_population_category_1 FOREIGN KEY (variable_superior)
      REFERENCES nfi_analytical.c_sub_population_category (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_spc_hierarchy__c_sub_population_category_1 ON nfi_analytical.t_spc_hierarchy IS
'Foreign key to table c_sub_population_category.';

ALTER TABLE nfi_analytical.t_spc_hierarchy
  ADD CONSTRAINT fkey__t_spc_hierarchy__c_sub_population_category_2 FOREIGN KEY (variable)
      REFERENCES nfi_analytical.c_sub_population_category (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION;
COMMENT ON CONSTRAINT fkey__t_spc_hierarchy__c_sub_population_category_2 ON nfi_analytical.t_spc_hierarchy IS
'Foreign key to table c_sub_population_category.';

CREATE INDEX fki__t_spc_hierarchy__c_sub_population_category_1 ON nfi_analytical.t_spc_hierarchy USING btree (variable_superior);
COMMENT ON INDEX nfi_analytical.fki__t_spc_hierarchy__c_sub_population_category_1 IS
'BTree index on foreign key fkey__t_spc_hierarchy__c_sub_population_category_1.';

CREATE INDEX fki__t_spc_hierarchy__c_sub_population_category_2 ON nfi_analytical.t_spc_hierarchy USING btree (variable);
COMMENT ON INDEX nfi_analytical.fki__t_spc_hierarchy__c_sub_population_category_2 IS
'BTree index on foreign key fkey__t_spc_hierarchy__c_sub_population_category_2.';

-- SEQUENCE
CREATE SEQUENCE nfi_analytical.t_spc_hierarchy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE nfi_analytical.t_spc_hierarchy_id_seq OWNED BY nfi_analytical.t_spc_hierarchy.id;
ALTER TABLE ONLY nfi_analytical.t_spc_hierarchy ALTER COLUMN id SET DEFAULT nextval('nfi_analytical.t_spc_hierarchy_id_seq'::regclass);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: cm_adc2classrule2panel_refyearset
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.cm_adc2classrule2panel_refyearset;

CREATE TABLE nfi_analytical.cm_adc2classrule2panel_refyearset(
id				serial,
adc2classification_rule 	integer NOT NULL,
refyearset2panel		integer NOT NULL,
CONSTRAINT pkey__cm_adc2classrule2panel_refyearset__id PRIMARY KEY (id)
);

ALTER TABLE nfi_analytical.cm_adc2classrule2panel_refyearset ADD CONSTRAINT
fkey__cm_adc2classrule2panel_refyearset__adc2classrule FOREIGN KEY (adc2classification_rule)
REFERENCES nfi_analytical.cm_adc2classification_rule (id)
ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE nfi_analytical.cm_adc2classrule2panel_refyearset ADD CONSTRAINT
fkey__cm_adc2classrule2panel_refyearset__refyearset2panel FOREIGN KEY (refyearset2panel)
REFERENCES sdesign.cm_refyearset2panel_mapping (id)
ON UPDATE CASCADE ON DELETE NO ACTION;

CREATE INDEX fki__cm_adc2classrule2panel_refyearset__adc2classrule ON nfi_analytical.cm_adc2classrule2panel_refyearset USING btree(adc2classification_rule);
CREATE INDEX fki__cm_adc2classrule2panel_refyearset__refyearset2panel ON nfi_analytical.cm_adc2classrule2panel_refyearset USING btree(refyearset2panel);

COMMENT ON TABLE nfi_analytical.cm_adc2classrule2panel_refyearset IS 'Mapping table between classification rules and panels/reference year sets. Its purpose is to determine the availability of classification rules for specific panels nad reference year sets.';
COMMENT ON COLUMN nfi_analytical.cm_adc2classrule2panel_refyearset.id IS 'Identifier of the record, primary key.';
COMMENT ON COLUMN nfi_analytical.cm_adc2classrule2panel_refyearset.adc2classification_rule IS 'Identifier of the classification rule, foreign key to table cm_adc2classification_rule.';
COMMENT ON COLUMN nfi_analytical.cm_adc2classrule2panel_refyearset.refyearset2panel IS 'Identifier of the panel and reference year set combination, foreign key to table cm_refyearset2panel_mapping.';
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: cm_spc2classrule2panel_refyearset
--------------------------------------------------------------------;
-- DROP TABLE nfi_analytical.cm_spc2classrule2panel_refyearset;

CREATE TABLE nfi_analytical.cm_spc2classrule2panel_refyearset(
id				serial,
spc2classification_rule 	integer NOT NULL,
refyearset2panel		integer NOT NULL,
CONSTRAINT pkey__cm_spc2classrule2panel_refyearset__id PRIMARY KEY (id)
);

ALTER TABLE nfi_analytical.cm_spc2classrule2panel_refyearset ADD CONSTRAINT
fkey__cm_spc2classrule2panel_refyearset__spc2classrule FOREIGN KEY (spc2classification_rule)
REFERENCES nfi_analytical.cm_spc2classification_rule (id)
ON UPDATE CASCADE ON DELETE NO ACTION;

ALTER TABLE nfi_analytical.cm_spc2classrule2panel_refyearset ADD CONSTRAINT
fkey__cm_spc2classrule2panel_refyearset__refyearset2panel FOREIGN KEY (refyearset2panel)
REFERENCES sdesign.cm_refyearset2panel_mapping (id)
ON UPDATE CASCADE ON DELETE NO ACTION;

CREATE INDEX fki__cm_spc2classrule2panel_refyearset__spc2classrule ON nfi_analytical.cm_spc2classrule2panel_refyearset USING btree(spc2classification_rule);
CREATE INDEX fki__cm_spc2classrule2panel_refyearset__refyearset2panel ON nfi_analytical.cm_spc2classrule2panel_refyearset USING btree(refyearset2panel);

COMMENT ON TABLE nfi_analytical.cm_spc2classrule2panel_refyearset IS 'Mapping table between classification rules and panels/reference year sets. Its purpose is to determine the availability of classification rules for specific panels nad reference year sets.';
COMMENT ON COLUMN nfi_analytical.cm_spc2classrule2panel_refyearset.id IS 'Identifier of the record, primary key.';
COMMENT ON COLUMN nfi_analytical.cm_spc2classrule2panel_refyearset.spc2classification_rule IS 'Identifier of the classification rule, foreign key to table cm_spc2classification_rule.';
COMMENT ON COLUMN nfi_analytical.cm_spc2classrule2panel_refyearset.refyearset2panel IS 'Identifier of the panel and reference year set combination, foreign key to table cm_refyearset2panel_mapping.';
--------------------------------------------------------------------;
