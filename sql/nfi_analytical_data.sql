--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--

---------------------------------------------------------------------------------------------------
--------------------------------------------------------------------;
-- Table: c_areal_or_population
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_areal_or_population(id,label,description) VALUES
(100,'area domain','Area domain.'),
(200,'population','Population.');
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_ldsity_object
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_ldsity_object(id,label,description,label_en,description_en,table_name,upper_object,areal_or_population,column4upper_object,filter) VALUES
(100,'střed inventarizační plochy','střed inventarizační plochy'::text,'the center of the inventory plot','the center of the inventory plot'::text,'nfi_analytical.t_plots',null::integer,100,null::character varying,'is_latest = true'),
(200,'kmeny hroubí','kmeny hroubí'::text,'merchantable wood stems','merchantable wood stems'::text,'nfi_analytical.f_p_stems',100,200,'plots'::character varying,'is_latest = true'),
(300,'lokalita kmenů nehroubí','lokalita kmenů nehroubí'::text,'the location of non-merchantable wood stems','the location of non-merchantable wood stems'::text,'nfi_analytical.t_regeneration',100,100,'plots'::character varying,'is_latest = true'),
(400,'kmeny nehroubí','kmeny nehroubí'::text,'non-merchantable wood stems','non-merchantable wood stems'::text,'nfi_analytical.t_regenerations',300,200,'regeneration'::character varying,'is_latest = true');
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_unit_of_measure
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_unit_of_measure(id,label,description,label_en,description_en) VALUES
(100,'ha','Hektar.','ha','Hectare.'),
(200,'ks','Kus.','pc.','Piece.'),
(300,'m2/ha','Metr čtvereční na hektar.','m2/ha','Square metre per hectare.');
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_version
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_version(id,label,description,label_en,description_en) VALUES
(100,'1. verze','1. verze.','version 1','Version 1.');
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_area_domain
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_area_domain(id,label,description,label_en,description_en,array_order) VALUES
(100,'celá plocha geografické domény','Celá plocha geografické domény.','entire area of geographical domain','The entire area of the geographical domain.',1),
(200,'kategorie pozemku podle FAO FRA','Kategorie pozemku (Les nebo Neles) podle definice FAO FRA.','land category according to FAO FRA','The land category (Forest or Non-Forest) according to the FAO FRA definition.',2),
(300,'zařazení pozemku podle SLHP','Zařazení pozemku (Les nebo Neles) podle SLHP.','land register according to SFMPG area','The land register (Forest or Non-Forest) according to the SFMPG area.',3);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_area_domain_category
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_area_domain_category(id,label,description,label_en,description_en,area_domain) VALUES
(101,'celá plocha geografické domény','Celá plocha geografické domény (celá zájmová oblast).','entire area of geographical domain','The entire area of the geographical domain (the whole area of interest).',100),
(201,'les','Pozemek kategorie Les podle FAO FRA.','forest','The land forest "Forest" according to FAO FRA.',200),
(202,'neles','Pozemek kategorie Neles podle FAO FRA.','non-forest','The land "Non-Forest" according to FAO FRA.',200),
(301,'les podle SLHP','Pozemek kategorie Les podle SLHP.','SFMPG forest','The land forest "Forest" according to SFMPG area.',300),
(302,'neles podle SLHP','Pozemek kategorie Neles podle SLHP.','SFMPG non-forest','The land "Non-Forest" according to SFMPG area.',300);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: cm_adc2classification_rule
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.cm_adc2classification_rule(area_domain_category,ldsity_object,classification_rule) VALUES
(101,100,'plot is not null'),
(201,100,'fao_fra_forest IN (100)'),
(202,100,'fao_fra_forest IN (200)'),
(301,100,'land_register IN (100)'),
(302,100,'land_register IN (200)');
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_sub_population
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_sub_population(id,label,description,label_en,description_en,array_order) VALUES
(100,'mrtvé/živé kmeny','Skupina mrtvých, nebo živých kmenů.','dead stem/live stem','The group of dead or live stems.',1),
(200,'jehličnany/listnáče','Skupina jehličnanů, nebo listnáčů.','conifers/broadleaves','The group of conifers or broadleaves.',2),
(300,'skupina 14 druhů dřevin','Skupina 14 druhů dřevin.','group of 14 woody species','The group of 14 woody species.',3),
(400,'tříditelné/netříditelné','Skupina tříditelných, nebo netříditelných kategorií.','sortable/non-sortable','The group of sortable or non-sortable categories.',4);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_sub_population_category
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_sub_population_category(id,label,description,label_en,description_en,sub_population) VALUES
(101,'souš','souš','dead stem','dead stem',100),
(102,'živý kmen','živý kmen','live stem','live stem',100),
(201,'jehličnany','jehličnany','conifers','conifers',200),
(202,'listnáče','listnáče','broadleaves','broadleaves',200),
(301,'smrk ztepilý','smrk ztepilý','Norway spruce','Norway spruce',300),
(302,'jedle bělokorá','jedle bělokorá','European silver fir','European silver fir',300),
(303,'borovice lesní','borovice lesní','Scots pine','Scots pine',300),
(304,'modřín opadavý','modřín opadavý','European larch','European larch',300),
(305,'ostatní jehličnaté','ostatní jehličnaté','other coniferous','other coniferous',300),
(306,'buk lesní','buk lesní','European beech','European beech',300),
(307,'duby','duby','oak species','oak species',300),
(308,'habr obecný','habr obecný','European hornbeam','European hornbeam',300),
(309,'javory','javory','maple species','maple species',300),
(310,'jasany','jasany','ash species','ash species',300),
(311,'olše','olše','alder species','alder species',300),
(312,'břízy','břízy','birch species','birch species',300),
(313,'ostatní list. tvrdé','ostatní list. tvrdé','other hardwood broad','other hardwood broad',300),
(314,'ostatní list. měkké','ostatní list. měkké','other softwood broad','other softwood broad',300),
(401,'tříditelné','tříditelné','sortable','sortable',400),
(402,'netříditelné','netříditelné','non-sortable','non-sortable',400);
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: cm_spc2classification_rule
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.cm_spc2classification_rule(sub_population_category,ldsity_object,classification_rule) VALUES
(101,200,'dead_stem IN (100)'),
(102,200,'dead_stem IN (200)'),
(201,200,'con_dec IN (100)'),
(202,200,'con_dec IN (200)'),
(301,200,'species IN (8500,18400)'),
(302,200,'species IN (9200,19100)'),
(303,200,'species IN (9900,19800)'),
(304,200,'species IN (10800,20700)'),
(305,200,'species IN (8600,8700,8800,8900,9000,9100,9300,9400,9500,9600,9700,9800,10000,10100,10200,10300,10400,10500,10600,10700,10900,11000,11100,11200,11300,17400,18500,18600,18700,18800,18900,19000,19200,19300,19400,19500,19600,19700,19900,20000,20100,20200,20300,20400,20500,20600,20800,20900,21000,21100,21200,27300)'),
(306,200,'species IN (12200,22100)'),
(307,200,'species IN (11400,11500,1160011700,11800,11900,12000,12100,21300,21400,21500,21600,21700,21800,21900,22000)'),
(308,200,'species IN (12300,22200)'),
(309,200,'species IN (12400,12500,12600,12700,12800,22300,22400,22500,22600,22700)'),
(310,200,'species IN (12900,13000,13100,22800,22900,23000)'),
(311,200,'species IN (15200,15300,25100,25200)'),
(312,200,'species IN (13600,13700,16700,23500,23600,26600)'),
(313,200,'species IN (13200,13300,13400,13500,13800,13900,14000,14100,14200,14300,14400,14500,14600,14700,14800,16600,16800,16900,17000,17100,17200,17300,17500,17600,17700,17900,18000,18100,18200,18300,23100,23200,23300,23400,23700,23800,23900,24000,24100,24200,24300,24400,24500,24600,24700,26500,26700,26800,26900,27000,27100,27200,27400,27500,27600,27800,27900,28000,28100,28200)'),
(314,200,'species IN (14900,15000,15100,15400,15500,15600,15700,15800,15900,16000,16100,16200,16300,16400,16500,17800,24800,24900,25000,25300,25400,25500,25600,25700,25800,25900,26000,26100,26200,26300,26400,27700)'),
(201,400,'con_dec IN (100)'),
(202,400,'con_dec IN (200)'),
(301,400,'species IN (8500,18400)'),
(302,400,'species IN (9200,19100)'),
(303,400,'species IN (9900,19800)'),
(304,400,'species IN (10800,20700)'),
(305,400,'species IN (8600,8700,8800,8900,9000,9100,9300,9400,9500,9600,9700,9800,10000,10100,10200,10300,10400,10500,10600,10700,10900,11000,11100,11200,11300,17400,18500,18600,18700,18800,18900,19000,19200,19300,19400,19500,19600,19700,19900,20000,20100,20200,20300,20400,20500,20600,20800,20900,21000,21100,21200,27300)'),
(306,400,'species IN (12200,22100)'),
(307,400,'species IN (11400,11500,1160011700,11800,11900,12000,12100,21300,21400,21500,21600,21700,21800,21900,22000)'),
(308,400,'species IN (12300,22200)'),
(309,400,'species IN (12400,12500,12600,12700,12800,22300,22400,22500,22600,22700)'),
(310,400,'species IN (12900,13000,13100,22800,22900,23000)'),
(311,400,'species IN (15200,15300,25100,25200)'),
(312,400,'species IN (13600,13700,16700,23500,23600,26600)'),
(313,400,'species IN (13200,13300,13400,13500,13800,13900,14000,14100,14200,14300,14400,14500,14600,14700,14800,16600,16800,16900,17000,17100,17200,17300,17500,17600,17700,17900,18000,18100,18200,18300,23100,23200,23300,23400,23700,23800,23900,24000,24100,24200,24300,24400,24500,24600,24700,26500,26700,26800,26900,27000,27100,27200,27400,27500,27600,27800,27900,28000,28100,28200)'),
(314,400,'species IN (14900,15000,15100,15400,15500,15600,15700,15800,15900,16000,16100,16200,16300,16400,16500,17800,24800,24900,25000,25300,25400,25500,25600,25700,25800,25900,26000,26100,26200,26300,26400,27700)'),
(401,200,'EXISTS'),
(402,200,'NOT EXISTS'),
(401,400,'EXISTS'),
(402,400,'NOT EXISTS');
--------------------------------------------------------------------;
--------------------------------------------------------------------;
-- Table: c_definition_variant
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_definition_variant(id, label, label_en, description, description_en) VALUES
(1,'kalibrovaný model','Model plochy korunových projekcí využívající kalibračního měření na inventarizační ploše.',
'calibrated model', 'Crown projections area model with use of calibration trees on the inventory plot.'),
(2,'generalizovaná lokální hustota', 'Generalizovaná lokální hustota s využitím vzorníků druhého stupně.',
'generalized local density', 'Generalized local density - usage of second step sample stems.'),
(3, 'jednoduchá lokální hustota', 'Jednoduchá lokální hustota (ne generalizovaná)', 'simple local density', 'Simple local density (not generalized)');
--------------------------------------------------------------------;
-- Table: c_ldsity
--------------------------------------------------------------------;
/*
INSERT INTO nfi_analytical.c_panel_refyearset_group(id,label,description,label_en,description_en) VALUES
(100,'sgrid 1','All panels coming from inventory 1 grid which containes rotated clusters (duplexes).', NULL, NULL),
(200,'inventory 2 sgrid 2a','All panels coming from s2a grid, which were firstly measured in the 2nd inventory campaign.', NULL, NULL),
(300,'inventory 3 sgrid 2a, 2b','Panels with both s2a and s2b plots. S2b were measured in the 3rd inventory campaign.', NULL, NULL),
(400,'inventory 3, sgrid 2a','Third inventory, sampling grid s2a.', NULL, NULL),
(500,'inventory 2, 3 sgrid 1, 2','Second and thidr inventory. Panels from sgrid 1, sgrid 2 a and b.', NULL, NULL); -- 100, 200, 300
*/
--------------------------------------------------------------------;

WITH w_panel_refyearset_group AS (
	SELECT 100 AS panel_refyearset_group, t1.id AS refyearset2panel
	FROM sdesign.cm_refyearset2panel_mapping AS t1
	INNER JOIN sdesign.t_panel AS t2
	ON t1.panel = t2.id
	INNER JOIN sdesign.t_reference_year_set AS t3
	ON t1.reference_year_set = t3.id
	WHERE  t2.panel IN ('NFRD12- 2plots','NFRD7- 2plots') AND
		t3.reference_year_set = '2011 - 2014'
	UNION ALL
	SELECT 200, t1.id
	FROM sdesign.cm_refyearset2panel_mapping AS t1
	INNER JOIN sdesign.t_panel AS t2
	ON t1.panel = t2.id
	INNER JOIN sdesign.t_reference_year_set AS t3
	ON t1.reference_year_set = t3.id
	WHERE  t2.panel IN ('NFRD11- 1plot, s2a', 'NFRD13- 1plot, s2a', 'NFRD14- 1plot, s2a') AND
		t3.reference_year_set = '2011 - 2015'
	UNION ALL
	SELECT 300, t1.id
	FROM sdesign.cm_refyearset2panel_mapping AS t1
	INNER JOIN sdesign.t_panel AS t2
	ON t1.panel = t2.id
	INNER JOIN sdesign.t_reference_year_set AS t3
	ON t1.reference_year_set = t3.id
	WHERE  t2.panel IN ('NFRD11- 1plot, s2a', 'NFRD13- 1plot, s2a', 'NFRD14- 1plot, s2a', 'NFRD11- 1plot, s2b', 'NFRD13- 1plot, s2b') AND
		t3.reference_year_set = '2016 - 2020'
	UNION ALL
	SELECT 400, t1.id
	FROM sdesign.cm_refyearset2panel_mapping AS t1
	INNER JOIN sdesign.t_panel AS t2
	ON t1.panel = t2.id
	INNER JOIN sdesign.t_reference_year_set AS t3
	ON t1.reference_year_set = t3.id
	WHERE  t2.panel IN ('NFRD11- 1plot, s2a', 'NFRD13- 1plot, s2a', 'NFRD14- 1plot, s2a') AND
		t3.reference_year_set = '2016 - 2020'
)
, w_data AS (
	SELECT * FROM 
		(VALUES
		('plocha lesa','Plocha lesa.','area of forests','The area of forests.',100,'ld_forest',100,array[1],null::integer[],500, NULL::int[]),
		('počet kmenů hroubí','Počet kmenů hroubí.','count of merchantable wood stems','The count of merchantable wood stems.',200,'ld_stem_count_ksha',200,array[2],array[2],600, NULL::int[]),
		('počet kmenů nehroubí','Počet kmenů nehroubí.','count of non-merchantable wood stems','The count of non-merchantable wood stems.',400,'ld_stem_count_ksha',200,array[2],null::int[],200, NULL::int[]),
		('korunové projekce kmenů hroubí','Korunové projekce kmenů hroubí.','crown projections of merchantable wood stems','Crown projections of merchantable wood stems.',200,'ld_crown_proj_m2ha',300,array[2],array[2],600,array[1,2]),
		('korunové projekce kmenů nehroubí','Korunové projekce kmenů nehroubí.','crown projections of non-merchantable wood stems','Crown projections of non-merchantable wood stems.',400,'ld_crown_proj_m2ha',300,array[2],null::int[],200,array[1,3])
		) AS t(label, description, label_en, description_en, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, panel_refyearset_group, definition_variant)
),
w_ldsity AS (
	INSERT INTO nfi_analytical.c_ldsity(label,description,label_en,description_en,ldsity_object,column_expression,unit_of_measure,area_domain_category,sub_population_category, definition_variant)
	SELECT label, description, label_en, description_en, ldsity_object, column_expression, unit_of_measure, area_domain_category, sub_population_category, definition_variant
	FROM w_data
	RETURNING id, label, description, label_en, description_en, definition_variant
),
w_ldsity_id AS (
	SELECT t1.id, t1.label, t1.description, t1.label_en, t1.description_en, t2.ldsity_object, t2.column_expression, t2.unit_of_measure, t2.area_domain_category, t2.sub_population_category, t2.panel_refyearset_group
	FROM w_ldsity AS t1
	INNER JOIN w_data AS t2
	ON t1.label = t2.label
)
-- Table: cm_ldsity2panel_refyearset_version
, w_all AS (
	SELECT t1.id AS ldsity, t2.refyearset2panel
	FROM 	w_ldsity_id AS t1
	INNER JOIN
		(SELECT panel_refyearset_group, refyearset2panel
		FROM w_panel_refyearset_group
		UNION ALL
		SELECT 500, refyearset2panel
		FROM w_panel_refyearset_group
		WHERE panel_refyearset_group IN (100,200,300)
		UNION ALL
		SELECT 600, refyearset2panel
		FROM w_panel_refyearset_group
		WHERE panel_refyearset_group IN (100,200)
		)AS t2
	ON t1.panel_refyearset_group = t2.panel_refyearset_group
)
INSERT INTO nfi_analytical.cm_ldsity2panel_refyearset_version (ldsity, refyearset2panel, version)
SELECT ldsity, refyearset2panel, 100 AS version
FROM w_all;


--------------------------------------------------------------------;
-- Table: c_fao_fra_forest
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_fao_fra_forest(id,label,description) VALUES
(100,'forest','Forest.'),
(200,'non-forest','Non-Forest.');
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_land_register
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_land_register(id,label,description) VALUES
(100,'forest','Forest.'),
(200,'non-forest','Non-Forest.');
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_con_dec
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_con_dec(id,label,description) VALUES
(100,'coniferous','Coniferous.'),
(200,'decidious','Decidioius.');
--------------------------------------------------------------------;


--------------------------------------------------------------------;
-- Table: c_species
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_species(id,label,description) VALUES
(8500,'smrk ztepily','smrk ztepily'),
(8600,'smrk pichlavy','smrk pichlavy'),
(8700,'smrk cerny','smrk cerny'),
(8800,'smrk sivy','smrk sivy'),
(8900,'smrk omorika','smrk omorika'),
(9000,'smrk Engelmannuv','smrk Engelmannuv'),
(9100,'smrky ostatni','smrky ostatni'),
(9200,'jedle belokora','jedle belokora'),
(9300,'jedle obrovska','jedle obrovska'),
(9400,'jedle ojinena','jedle ojinena'),
(9500,'jedle kavkazska','jedle kavkazska'),
(9600,'jedle vznesena','jedle vznesena'),
(9700,'jedle ostatni','jedle ostatni'),
(9800,'douglaska tisolista','douglaska tisolista'),
(9900,'borovice lesni','borovice lesni'),
(10000,'borovice cerna','borovice cerna'),
(10100,'borovice banksova','borovice banksova'),
(10200,'borovice vejmutovka','borovice vejmutovka'),
(10300,'borovice limba','borovice limba'),
(10400,'borovice pokroucena','borovice pokroucena'),
(10500,'borovice ostatni','borovice ostatni'),
(10600,'borovice klec (kosodrevina)','borovice klec (kosodrevina)'),
(10700,'borovice blatka','borovice blatka'),
(10800,'modrin evropsky','modrin evropsky'),
(10900,'modriny ostatni','modriny ostatni'),
(11000,'tis cerveny','tis cerveny'),
(11100,'jalovec obecny','jalovec obecny'),
(11200,'souse jehlicnate','souse jehlicnate'),
(11300,'ostatni jehlicnate','ostatni jehlicnate'),
(11400,'dub letni','dub letni'),
(11500,'dub letni slavonsky','dub letni slavonsky'),
(11600,'dub zimni','dub zimni'),
(11700,'dub cerveny','dub cerveny'),
(11800,'dub pyrity','dub pyrity'),
(11900,'dub bahenni','dub bahenni'),
(12000,'duby ostatní','duby ostatní'),
(12100,'dub cer','dub cer'),
(12200,'buk lesni','buk lesni'),
(12300,'habr obecny','habr obecny'),
(12400,'javor mlec','javor mlec'),
(12500,'javor klen','javor klen'),
(12600,'javor babyka','javor babyka'),
(12700,'javor jasanolisty','javor jasanolisty'),
(12800,'javory ostatni','javory ostatni'),
(12900,'jasan ztepily','jasan ztepily'),
(13000,'jasan americky','jasan americky'),
(13100,'jasan uzkolisty','jasan uzkolisty'),
(13200,'jilm habrolisty','jilm habrolisty'),
(13300,'jilm horsky','jilm horsky'),
(13400,'jilm vaz','jilm vaz'),
(13500,'trnovnik akat','trnovnik akat'),
(13600,'briza bradavicnata','briza bradavicnata'),
(13700,'briza pyrita','briza pyrita'),
(13800,'jerab ptaci','jerab ptaci'),
(13900,'jerab brek','jerab brek'),
(14000,'jerab muk','jerab muk'),
(14100,'oresak kralovsky','oresak kralovsky'),
(14200,'oresak cerny','oresak cerny'),
(14300,'platan javorolisty','platan javorolisty'),
(14400,'tresen ptaci','tresen ptaci'),
(14500,'stremcha pozdni','stremcha pozdni'),
(14600,'hrusen','hrusen'),
(14700,'jablon','jablon'),
(14800,'ostatni listnate tvrde','ostatni listnate tvrde'),
(14900,'lipa srdcita','lipa srdcita'),
(15000,'lipa velkolista','lipa velkolista'),
(15100,'lipa stribrna','lipa stribrna'),
(15200,'olse lepkava','olse lepkava'),
(15300,'olse seda','olse seda'),
(15400,'topol osika','topol osika'),
(15500,'topol bily','topol bily'),
(15600,'topol cerny','topol cerny'),
(15700,'topoly ostatni neslechtene','topoly ostatni neslechtene'),
(15800,'topoly slechtene','topoly slechtene'),
(15900,'vrba jiva','vrba jiva'),
(16000,'vrba bila, v. krehka','vrba bila, v. krehka'),
(16100,'jirovec madal','jirovec madal'),
(16200,'kastanovnik jedly','kastanovnik jedly'),
(16300,'pajasan zlaznaty','pajasan zlaznaty'),
(16400,'souse listnate','souse listnate'),
(16500,'ostatni listnate mekke','ostatni listnate mekke'),
(16600,'ostatni kere stromoviteho vzrustu','ostatni kere stromoviteho vzrustu'),
(16700,'briza karpatska','briza karpatska'),
(16800,'drin obecny','drin obecny'),
(16900,'svida krvava','svida krvava'),
(17000,'liska obecna','liska obecna'),
(17100,'hlohy sp.','hlohy sp.'),
(17200,'brslen evropsky','brslen evropsky'),
(17300,'krusina olsova','krusina olsova'),
(17400,'jalovec sp. ostatni','jalovec sp. ostatni'),
(17500,'visen turecka, mahalebka','visen turecka, mahalebka'),
(17600,'stremcha obecna','stremcha obecna'),
(17700,'resetlak pocistivy','resetlak pocistivy'),
(17800,'vrba sp., ostatni','vrba sp., ostatni'),
(17900,'bez cerny','bez cerny'),
(18000,'klokoc zpereny','klokoc zpereny'),
(18100,'serik obecny','serik obecny'),
(18200,'kalina obecna','kalina obecna'),
(18300,'trnka obecna','trnka obecna'),
(18400,'smrk ztepily','smrk ztepily'),
(18500,'smrk pichlavy','smrk pichlavy'),
(18600,'smrk cerny','smrk cerny'),
(18700,'smrk sivy','smrk sivy'),
(18800,'smrk omorika','smrk omorika'),
(18900,'smrk Engelmannuv','smrk Engelmannuv'),
(19000,'smrky ostatni','smrky ostatni'),
(19100,'jedle belokora','jedle belokora'),
(19200,'jedle obrovska','jedle obrovska'),
(19300,'jedle ojinena','jedle ojinena'),
(19400,'jedle kavkazska','jedle kavkazska'),
(19500,'jedle vznesena','jedle vznesena'),
(19600,'jedle ostatni','jedle ostatni'),
(19700,'douglaska tisolista','douglaska tisolista'),
(19800,'borovice lesni','borovice lesni'),
(19900,'borovice cerna','borovice cerna'),
(20000,'borovice banksova','borovice banksova'),
(20100,'borovice vejmutovka','borovice vejmutovka'),
(20200,'borovice limba','borovice limba'),
(20300,'borovice pokroucena','borovice pokroucena'),
(20400,'borovice ostatni','borovice ostatni'),
(20500,'borovice klec (kosodrevina)','borovice klec (kosodrevina)'),
(20600,'borovice blatka','borovice blatka'),
(20700,'modrin evropsky','modrin evropsky'),
(20800,'modriny ostatni','modriny ostatni'),
(20900,'tis cerveny','tis cerveny'),
(21000,'jalovec obecny','jalovec obecny'),
(21100,'souse jehlicnate','souse jehlicnate'),
(21200,'ostatni jehlicnate','ostatni jehlicnate'),
(21300,'dub letni','dub letni'),
(21400,'dub letni slavonsky','dub letni slavonsky'),
(21500,'dub zimni','dub zimni'),
(21600,'dub cerveny','dub cerveny'),
(21700,'dub pyrity','dub pyrity'),
(21800,'dub bahenni','dub bahenni'),
(21900,'duby ostatní','duby ostatní'),
(22000,'dub cer','dub cer'),
(22100,'buk lesni','buk lesni'),
(22200,'habr obecny','habr obecny'),
(22300,'javor mlec','javor mlec'),
(22400,'javor klen','javor klen'),
(22500,'javor babyka','javor babyka'),
(22600,'javor jasanolisty','javor jasanolisty'),
(22700,'javory ostatni','javory ostatni'),
(22800,'jasan ztepily','jasan ztepily'),
(22900,'jasan americky','jasan americky'),
(23000,'jasan uzkolisty','jasan uzkolisty'),
(23100,'jilm habrolisty','jilm habrolisty'),
(23200,'jilm horsky','jilm horsky'),
(23300,'jilm vaz','jilm vaz'),
(23400,'trnovnik akat','trnovnik akat'),
(23500,'briza bradavicnata','briza bradavicnata'),
(23600,'briza pyrita','briza pyrita'),
(23700,'jerab ptaci','jerab ptaci'),
(23800,'jerab brek','jerab brek'),
(23900,'jerab muk','jerab muk'),
(24000,'oresak kralovsky','oresak kralovsky'),
(24100,'oresak cerny','oresak cerny'),
(24200,'platan javorolisty','platan javorolisty'),
(24300,'tresen ptaci','tresen ptaci'),
(24400,'stremcha pozdni','stremcha pozdni'),
(24500,'hrusen','hrusen'),
(24600,'jablon','jablon'),
(24700,'ostatni listnate tvrde','ostatni listnate tvrde'),
(24800,'lipa srdcita','lipa srdcita'),
(24900,'lipa velkolista','lipa velkolista'),
(25000,'lipa stribrna','lipa stribrna'),
(25100,'olse lepkava','olse lepkava'),
(25200,'olse seda','olse seda'),
(25300,'topol osika','topol osika'),
(25400,'topol bily','topol bily'),
(25500,'topol cerny','topol cerny'),
(25600,'topoly ostatni neslechtene','topoly ostatni neslechtene'),
(25700,'topoly slechtene','topoly slechtene'),
(25800,'vrba jiva','vrba jiva'),
(25900,'vrba bila, v. krehka','vrba bila, v. krehka'),
(26000,'jirovec madal','jirovec madal'),
(26100,'kastanovnik jedly','kastanovnik jedly'),
(26200,'pajasan zlaznaty','pajasan zlaznaty'),
(26300,'souse listnate','souse listnate'),
(26400,'ostatni listnate mekke','ostatni listnate mekke'),
(26500,'ostatni kere stromoviteho vzrustu','ostatni kere stromoviteho vzrustu'),
(26600,'briza karpatska','briza karpatska'),
(26700,'drin obecny','drin obecny'),
(26800,'svida krvava','svida krvava'),
(26900,'liska obecna','liska obecna'),
(27000,'hlohy sp.','hlohy sp.'),
(27100,'brslen evropsky','brslen evropsky'),
(27200,'krusina olsova','krusina olsova'),
(27300,'jalovec sp. ostatni','jalovec sp. ostatni'),
(27400,'visen turecka, mahalebka','visen turecka, mahalebka'),
(27500,'stremcha obecna','stremcha obecna'),
(27600,'resetlak pocistivy','resetlak pocistivy'),
(27700,'vrba sp., ostatni','vrba sp., ostatni'),
(27800,'bez cerny','bez cerny'),
(27900,'klokoc zpereny','klokoc zpereny'),
(28000,'serik obecny','serik obecny'),
(28100,'kalina obecna','kalina obecna'),
(28200,'trnka obecna','trnka obecna');
--------------------------------------------------------------------;

--------------------------------------------------------------------;
-- Table: c_dead_stem
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.c_dead_stem(id,label,description) VALUES
(100,'dead stem','Dead stem.'),
(200,'live stem','Live stem.');
--------------------------------------------------------------------;

--------------------------------------------------------------------;
-- Table: t_spc_hierarchy
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.t_spc_hierarchy(variable_superior,variable,dependent) VALUES
(401,201,false),
(401,202,false),
(201,301,false),
(201,302,false),
(201,303,false),
(201,304,false),
(201,305,false),
(202,306,false),
(202,307,false),
(202,308,false),
(202,309,false),
(202,310,false),
(202,311,false),
(202,312,false),
(202,313,false),
(202,314,false),
(102,301,true),
(102,302,true),
(102,303,true),
(102,304,true),
(102,305,true),
(102,306,true),
(102,307,true),
(102,308,true),
(102,309,true),
(102,310,true),
(102,311,true),
(102,312,true),
(102,313,true),
(102,314,true);
--------------------------------------------------------------------;

--------------------------------------------------------------------;
-- Table: cm_adc2classrule2panel_refyearset
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.cm_adc2classrule2panel_refyearset(adc2classification_rule, refyearset2panel) VALUES
(1,2),
(2,2),
(3,2),
(4,2),
(5,2);
--------------------------------------------------------------------;

--------------------------------------------------------------------;
-- Table: cm_spc2classrule2panel_refyearset
--------------------------------------------------------------------;
INSERT INTO nfi_analytical.cm_spc2classrule2panel_refyearset(spc2classification_rule, refyearset2panel) VALUES
(1,2),
(2,2),
(3,2),
(4,2),
(5,2),
(6,2),
(7,2),
(8,2),
(9,2),
(10,2),
(11,2),
(12,2),
(13,2),
(14,2),
(15,2),
(16,2),
(17,2),
(18,2),
(19,5),
(20,5),
(21,5),
(22,5),
(23,5),
(24,5),
(25,5),
(26,5),
(27,5),
(28,5),
(29,5),
(30,5),
(31,5),
(32,5),
(33,5),
(34,5);
--------------------------------------------------------------------;
