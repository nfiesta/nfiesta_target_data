--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------
-- this will be replaced by fn_save_pyrgroup:

-- c_panel_refyearset_group

-- inserting test data into target_data.c_panel_refyearset_group, 
-- each panel and reference - yearset combination of the sdesign.cm_refyearset2panel_mapping becomes a group 
INSERT INTO target_data.c_panel_refyearset_group (label, description, label_en, description_en)
SELECT 
	concat(t2.panel, ' / ', t3.reference_year_set) AS label, 
	concat(t2.label, ' / ', t3.label) AS description,
	concat(t2.panel, ' / ', t3.reference_year_set) AS label_en, 
	concat(t2.label, ' / ', t3.label) AS description_en
FROM 
	sdesign.cm_refyearset2panel_mapping AS t1
INNER JOIN
	sdesign.t_panel AS t2
	ON t1.panel = t2.id 
INNER JOIN 
	sdesign.t_reference_year_set AS t3
	ON t1.reference_year_set = t3.id;

-- t_panel_refyearset_group
INSERT INTO target_data.t_panel_refyearset_group(panel_refyearset_group, refyearset2panel)
SELECT 
	t4.id AS panel_refyearset_group,
	t1.id AS refyearset2panel 
FROM 
	sdesign.cm_refyearset2panel_mapping AS t1
INNER JOIN
	sdesign.t_panel AS t2
	ON t1.panel = t2.id 
INNER JOIN 
	sdesign.t_reference_year_set AS t3
	ON t1.reference_year_set = t3.id
INNER JOIN
	target_data.c_panel_refyearset_group AS t4
	ON	t4.label = concat(t2.panel, ' / ', t3.reference_year_set) AND 
		t4.description = concat(t2.label, ' / ', t3.label);

-- get function
SELECT * FROM target_data.fn_get_panel_refyearset_group();
SELECT * FROM target_data.fn_get_panel_refyearset_group(1);

SELECT * FROM target_data.fn_try_pyrgroup_refyearset2panel_mapping(array[1]);

SELECT * FROM target_data.fn_update_pyrgroup('NFRD12- 2plots / 2001 - 2004', 'NFRD12- 2 plots within rotated cluster / NFI1 (2001-2004)',
	'NFRD12- 2plots / 2001 - 2004','NFRD12- 2 plots within rotated cluster / NFI1 (2001-2004)', 1);

SELECT * FROM target_data.c_panel_refyearset_group WHERE id = 1;
 
INSERT INTO target_data.c_panel_refyearset_group(label, description, label_en, description_en)
VALUES ('a','aa','a','aa');

INSERT INTO target_data.t_panel_refyearset_group(panel_refyearset_group, refyearset2panel)
SELECT t2.id, t1.id
FROM (SELECT * FROM sdesign.cm_refyearset2panel_mapping LIMIT 1) AS t1,
	target_data.c_panel_refyearset_group AS t2
WHERE t2.label = 'a';

SELECT target_data.fn_delete_pyrgroup(id)
FROM target_data.c_panel_refyearset_group
WHERE label = 'a';

SELECT * FROM target_data.c_panel_refyearset_group AS t1
INNER JOIN target_data.t_panel_refyearset_group AS t2
ON t1.id = t2.panel_refyearset_group 
WHERE t1.label = 'a';
 
-- test if an exception is raised 
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_groups(NULL);

-- return some groups
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_groups(ARRAY[1,2,3]);  

-------------------------------------------------------------------------;
-- checking of fn_get_eligible_panel_refyearset_combinations_internal
-------------------------------------------------------------------------;
-- checking if the function returns some combinations of panels and reference yearsets for forest area   
WITH
w1 AS	(SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, ARRAY[1,2,3], FALSE))
,w2 AS	(SELECT unnest(w1.fn_get_eligible_panel_refyearset_combinations_internal) AS refyearset2panel_mapping FROM w1)
SELECT array_agg(w2.refyearset2panel_mapping ORDER BY w2.refyearset2panel_mapping) FROM w2;
-- {2,4,5,6,8,9,10,12,13,14}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, ARRAY[1,2,3], TRUE); 
-- no records, state variable has no negative contributions to local density

WITH
w1 AS	(SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(4, ARRAY[109,110,111,112,113,115,116,117,118,119,120,121,122,123,128,129,130,131,132,134,135,136,137,138,139,140,141,142,147,148,149,150,151,153,154,155,156,157,158,159,160,161], FALSE))
,w2 AS	(SELECT unnest(w1.fn_get_eligible_panel_refyearset_combinations_internal) AS refyearset2panel_mapping FROM w1)
SELECT array_agg(w2.refyearset2panel_mapping ORDER BY w2.refyearset2panel_mapping) FROM w2;
-- {2,4,9,13}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(4, ARRAY[109,110,111,112,113,115,116,117,118,119,120,121,122,123,128,129,130,131,132,134,135,136,137,138,139,140,141,142,147,148,149,150,151,153,154,155,156,157,158,159,160,161], TRUE);
-- no records, state variable has no negative contributions to local density

WITH
w1 AS	(SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(5, ARRAY[173,179,192,198,211,217,173,179,192,198,211,217], FALSE))
,w2 AS	(SELECT unnest(w1.fn_get_eligible_panel_refyearset_combinations_internal) AS refyearset2panel_mapping FROM w1)
SELECT array_agg(w2.refyearset2panel_mapping ORDER BY w2.refyearset2panel_mapping) FROM w2;
-- {9,13}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(5, ARRAY[173,179,192,198,211,217,173,179,192,198,211,217], TRUE);
-- no records, state variable has no negative contributions to local density

-- checking if the function returns some combinations of panels and reference yearsets for change of forest area 
WITH
w1 AS	(SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(6, ARRAY[221,222], FALSE))
,w2 AS	(SELECT unnest(w1.fn_get_eligible_panel_refyearset_combinations_internal) AS refyearset2panel_mapping FROM w1)
SELECT array_agg(w2.refyearset2panel_mapping ORDER BY w2.refyearset2panel_mapping) FROM w2;
-- {2,4,5,6,8,9,10,12,13,14}

-- the same set is retrieved for negative contributions
WITH
w1 AS	(SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(6, ARRAY[221,222], TRUE))
,w2 AS	(SELECT unnest(w1.fn_get_eligible_panel_refyearset_combinations_internal) AS refyearset2panel_mapping FROM w1)
SELECT array_agg(w2.refyearset2panel_mapping ORDER BY w2.refyearset2panel_mapping) FROM w2;
-- {2,4,5,6,8,9,10,12,13,14}

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood stems   
WITH
w1 AS	(SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(2, ARRAY[23,24,25,26,27,29,30,31,32,33,34,35,36,37,40,41,42,43,44,46,47,48,49,50,51,52,53,54], FALSE))
,w2 AS	(SELECT unnest(w1.fn_get_eligible_panel_refyearset_combinations_internal) AS refyearset2panel_mapping FROM w1)
SELECT array_agg(w2.refyearset2panel_mapping ORDER BY w2.refyearset2panel_mapping) FROM w2;
-- {2,4,5,9,13}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(2, ARRAY[23,24,25,26,27,29,30,31,32,33,34,35,36,37,40,41,42,43,44,46,47,48,49,50,51,52,53,54], TRUE);
-- no records, state variable has no negative contributions to local density

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood and regeneration stems   
WITH
w1 AS	(SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(3, ARRAY[73,79,90,96,73,79,90,96], FALSE))
,w2 AS	(SELECT unnest(w1.fn_get_eligible_panel_refyearset_combinations_internal) AS refyearset2panel_mapping FROM w1)
SELECT array_agg(w2.refyearset2panel_mapping ORDER BY w2.refyearset2panel_mapping) FROM w2;
--{5,9,13}

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(3, ARRAY[73,79,90,96,73,79,90,96], TRUE);
-- no records, state variable has no negative contributions to local density

-- checking if the function raises an exception for NULL input
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(NULL, ARRAY[1,2,3], TRUE);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, NULL, TRUE);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations_internal(1, ARRAY[1,2,3], NULL);

----------------------------------------------------------------------------;
-- checking of fn_get_eligible_panel_refyearset_combinations -- wraper API
----------------------------------------------------------------------------;
-- checking if the function returns some combinations of panels and reference yearsets for forest area   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(1, ARRAY[2,3]);
-- ten records

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(1, ARRAY[1]);
-- ten records

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(4, ARRAY[109,110,111,112,113,115,116,117,118,119,120,121,122,123,128,129,130,131,132,134,135,136,137,138,139,140,141,142,147,148,149,150,151,153,154,155,156,157,158,159,160,161]);
-- four records

SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(5, ARRAY[163,180,182,199,201,218,163,180,182,199,201,218]);
-- two records

-- checking if the function returns some combinations of panels and reference yearsets for change of forest area 
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(6, ARRAY[221,222]);
-- three records, all coresponding to change referenece yearsets

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(2, ARRAY[23,24,25,26,27,29,30,31,32,33,34,35,36,37,40,41,42,43,44,46,47,48,49,50,51,52,53,54]);
-- five records

-- checking if the function returns some combinations of panels and reference yearsets for number of merchantable wood and regeneration stems   
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(3, ARRAY[73,79,90,96,73,79,90,96]);
-- three records

 -- checking if the function raises an exception for NULL input
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(NULL, ARRAY[73,79,90,96,73,79,90,96]);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(3, NULL);
SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(NULL, NULL);

----------------------------------------------------------------------------;
-- checking of fn_get_eligible_panel_refyearset_combinations -- overloaded function for internal use in fn_save_ldsity_values
----------------------------------------------------------------------------;
  SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(1);
  -- 3 records
  
  SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(2);
  -- 51 records
  
  SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(4);
  -- 57 records

  -- test of null input
  SELECT * FROM target_data.fn_get_eligible_panel_refyearset_combinations(NULL);
  -- exception raised

-------------------------------------------------------------------------;
-- checking of fn_get_refyearset2panel_mapping4group
-------------------------------------------------------------------------;
-- checking if the function returns some combinations of panels and reference yearsets   
SELECT * FROM target_data.fn_get_refyearset2panel_mapping4group(9);
SELECT * FROM target_data.fn_get_refyearset2panel_mapping4group(13);

 -- checking if the function raises an exception for NULL input
SELECT * FROM target_data.fn_get_refyearset2panel_mapping4group(NULL);
