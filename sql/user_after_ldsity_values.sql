--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------

---------------------------------------------------
-- try delete
---------------------------------------------------
--SELECT * FROM target_data.fn_try_delete_area_domain();
SELECT * FROM target_data.fn_try_delete_area_domain((SELECT id FROM target_data.c_area_domain WHERE label_en = 'land register according to SFMPG area'));
SELECT * FROM target_data.fn_try_delete_sub_population((SELECT id FROM target_data.c_sub_population WHERE label_en = 'conifers/broadleaves'));
SELECT * FROM target_data.fn_try_delete_target_variable((SELECT id FROM target_data.c_target_variable WHERE label_en = 'number of merch stems'));

-- delete ldsity_values
SELECT target_data.fn_delete_ldsity_values(target_variable)
FROM 
	(SELECT t1.target_variable
	FROM
		(SELECT t1.id AS target_variable, array_agg(t2.id) AS cm_ids, array_agg(t3.label_en ORDER BY t3.label_en) AS labels, count(*) AS total
		FROM target_data.c_target_variable AS t1
		INNER JOIN target_data.cm_ldsity2target_variable AS t2
		ON t1.id = t2.target_variable
		INNER JOIN target_data.c_ldsity AS t3
		ON t2.ldsity = t3.id
		WHERE t1.label = 'změna plochy lesa'
		GROUP BY t1.id) AS t1
	WHERE t1.total = 2
	) AS t1;

-- delete area domain completely with ldsity values
--SELECT * FROM target_data.fn_delete_area_domain_and_values(300);

-- store deleted ldsity values
select * from target_data.fn_save_ldsity_values
(
	(select array_agg(id order by id) from sdesign.cm_refyearset2panel_mapping
	where panel in (select id from sdesign.t_panel where panel in ('NFRD11- 1plot, s2a','NFRD13- 1plot, s2a','NFRD14- 1plot, s2a'))
	and reference_year_set in (select id from sdesign.t_reference_year_set where label = 'NFI2-3_change (2011-2020)')),
		(SELECT t1.target_variable
		FROM
			(SELECT t1.id AS target_variable, array_agg(t2.id) AS cm_ids, array_agg(t3.label_en ORDER BY t3.label_en) AS labels, count(*) AS total
			FROM target_data.c_target_variable AS t1
			INNER JOIN target_data.cm_ldsity2target_variable AS t2
			ON t1.id = t2.target_variable
			INNER JOIN target_data.c_ldsity AS t3
			ON t2.ldsity = t3.id
			WHERE t1.label = 'změna plochy lesa'
			GROUP BY t1.id) AS t1
		WHERE t1.total = 2
		)
);
