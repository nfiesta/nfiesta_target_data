--
-- Copyright 2021, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--
-- DATA DISCLAIMER
-- Any results produced on the basis of openly published Czech National Forest Inventory (CZNFI) sample data
-- do not reflect the true status or changes within any geographical area of the Czech Republic,
-- at, during or between any time occasion(s).
-- In particular, any such results must not be presented or interpreted as an alternative to any information published by CZNFI,
-- be it a past or future CZNFI publication.
--
---------------------------------------------------------------------------------------------------

--------------------------------------------------------------------;
-- c_ldsity_object_group
--------------------------------------------------------------------;
SELECT target_data.fn_save_ldsity_object_group(label, description, label_en, description_en, ldsity_objects)
FROM (VALUES 
	('kmeny hroubí', 'Kmeny hroubí.', 'merchantable wood stems', 'Merchantable wood stems.', array[200]),
	('kmeny hroubí a nehroubí', 'Kmeny hroubí (nad 7 cm výčetní tloušťky) a nehroubí (do 7 cm výčetní tloušťky).','merchantable and non-merchantable wood stems', 'Merchantable (over 7cm dbh) and non-merchantable (below 7cm dbh) wood stems.', array[200,400]),
	('střed inventarizační plochy', 'Střed inventarizační plochy.', 'centre of the inventory plot', 'The centre of the inventory plot.', array[100])
	) AS t1(label, description, label_en, description_en, ldsity_objects); 
--------------------------------------------------------------------;

--------------------------------------------------------------------;
-- Table: c_target_variable and cm_ldsity2target_variable
--------------------------------------------------------------------;
-- forest area
SELECT target_data.fn_save_target_variable(100, 'plocha lesa'::varchar, 'Plocha lesa.'::text, 'forest area'::varchar, 'Area of forests.'::text, array[t1.id], array[100], array[100])
FROM
	target_data.c_ldsity AS t1
WHERE
	t1.label = 'plocha lesa';

-- trigger test
SELECT target_data.fn_save_target_variable(100, 'plocha lesa'::varchar, 'Plocha lesa.'::text, 'forest area'::varchar, 'Area of forests.'::text, array[t1.id], array[100], array[100])
FROM
	target_data.c_ldsity AS t1
WHERE
	t1.label = 'plocha lesa';

-- number of stems
SELECT target_data.fn_save_target_variable(100, 'počet kmenů hroubí'::varchar, 'Počet kmenů hroubí.'::text, 'number of merch stems'::varchar, 'Number of merchantable wood stems.'::text, array[t1.id], array[100], array[100])
FROM
	target_data.c_ldsity AS t1
WHERE
	t1.label = 'počet kmenů hroubí';

WITH
w1 AS	(
		SELECT * from target_data.c_ldsity WHERE label = 'počet kmenů hroubí' UNION ALL
		SELECT * from target_data.c_ldsity WHERE label = 'počet kmenů nehroubí'
		)
,w2 AS	(
		SELECT array_agg(id ORDER BY id) AS id_ldsity FROM w1
		)
SELECT target_data.fn_save_target_variable(100, 'počet kmenů hroubí a nehroubí'::varchar, 'Počet kmenů hroubí a nehroubí.'::text, 'number of merch and non-merch stems'::varchar, 'Number of merchantable and non-merchantable wood stems.'::text, w2.id_ldsity, array[100,100], array[100,100]) FROM w2;

WITH
w1 AS	(
		SELECT * from target_data.c_ldsity WHERE label = 'plocha lesa' UNION ALL
		SELECT * from target_data.c_ldsity WHERE label = 'korunové projekce kmenů hroubí'
		)
,w2 AS	(
		SELECT array_agg(id ORDER BY id) AS id_ldsity FROM w1
		)
SELECT target_data.fn_save_target_variable(100, 'plocha lesa'::varchar, 'Plocha lesa.'::text, 'forest area'::varchar, 'Area of forests.'::text, w2.id_ldsity, array[100,200], array[100,100]) FROM w2;

WITH
w1 AS	(
		SELECT * from target_data.c_ldsity WHERE label = 'plocha lesa' UNION ALL
		SELECT * from target_data.c_ldsity WHERE label = 'korunové projekce kmenů hroubí' UNION ALL
		SELECT * from target_data.c_ldsity WHERE label = 'korunové projekce kmenů nehroubí'
		)
,w2 AS	(
		SELECT array_agg(id ORDER BY id) AS id_ldsity FROM w1
		)
SELECT target_data.fn_save_target_variable(100, 'plocha lesa'::varchar, 'Plocha lesa.'::text, 'forest area'::varchar, 'Area of forests.'::text, w2.id_ldsity, array[100,200,200], array[100,100,100]) FROM w2;

WITH
w1 AS	(
		SELECT * from target_data.c_ldsity WHERE label = 'plocha lesa'
		)
,w2 AS	(
		SELECT array_agg(id ORDER BY id) AS id_ldsity FROM w1
		)
SELECT target_data.fn_save_change_target_variable('změna plochy lesa'::varchar, 'Změna plochy lesa.'::text, 'forest area change'::varchar, 'Change of forest area.'::text, w2.id_ldsity, array[100], array[100], w2.id_ldsity, array[100], array[100]) FROM w2;

-- forest area change with partitioning by crown projections
WITH
w1 AS	(
		SELECT * from target_data.c_ldsity WHERE label = 'plocha lesa' UNION ALL
		SELECT * from target_data.c_ldsity WHERE label = 'korunové projekce kmenů hroubí' UNION ALL
		SELECT * from target_data.c_ldsity WHERE label = 'korunové projekce kmenů nehroubí'
		)
,w2 AS	(
		SELECT array_agg(id ORDER BY id) AS id_ldsity FROM w1
		)
SELECT target_data.fn_save_change_target_variable('změna plochy lesa'::varchar, 'Změna plochy lesa.'::text, 'forest area change'::varchar, 'Change of forest area.'::text, w2.id_ldsity, array[100,200,200], array[100,100,100], w2.id_ldsity, array[100,200,200], array[100,100,100]) FROM w2;


-- ldsity_object_group try/delete
SELECT target_data.fn_save_ldsity_object_group(label, description, label_en, description_en, ldsity_objects)
FROM (VALUES 
	('kmeny nehroubí', 'Kmeny nehroubí (do 7 cm výčetní tloušťky).','non-merchantable wood stems', 'Non-merchantable (below 7cm dbh) wood stems.', array[400])
	) AS t1(label, description, label_en, description_en, ldsity_objects);

SELECT target_data.fn_try_delete_ldsity_object_group(id)
FROM target_data.c_ldsity_object_group
WHERE label_en ='merchantable wood stems';

SELECT target_data.fn_try_delete_ldsity_object_group(id)
FROM target_data.c_ldsity_object_group
WHERE label_en ='non-merchantable wood stems';

SELECT target_data.fn_delete_ldsity_object_group(id)
FROM target_data.c_ldsity_object_group
WHERE label_en ='non-merchantable wood stems';

---------------------------------------------------
-- classification rules
---------------------------------------------------
-- ok
SELECT * FROM target_data.fn_check_classification_rule_syntax(100,'land_register=100');
-- false
SELECT * FROM target_data.fn_check_classification_rule_syntax(100,'land_regis');

-- ok
SELECT * FROM target_data.fn_check_classification_rule(2,200,'con_dec=100');
-- invalid syntax - given object (plot center) does not contain con_dec column
SELECT * FROM target_data.fn_check_classification_rule(2,100,'con_dec=100');
-- test on constrained rule
SELECT * FROM target_data.fn_check_classification_rule(2,200,'species IN (8500,18400)',NULL,NULL,array[201]);
SELECT * FROM target_data.fn_check_classification_rule(3,400,'species IN (8500,18400)',NULL,NULL,array[201]);

SELECT * FROM target_data.fn_check_classification_rules(2,200,array['con_dec=100'],1);
SELECT * FROM target_data.fn_check_classification_rules(2,200,array['con_dec=100']);
SELECT * FROM target_data.fn_check_classification_rules(2,200,array['false']);
SELECT * FROM target_data.fn_check_classification_rules(2,200,array['true']);
SELECT * FROM target_data.fn_check_classification_rules(2,200,array['true','con_dec=100']);
SELECT * FROM target_data.fn_check_classification_rules(2,200,array['species=8500','species=8600']);
SELECT * FROM target_data.fn_check_classification_rules(2,200,array['con_dec=100','con_dec=200']);
SELECT * FROM target_data.fn_check_classification_rules(2,200,array['false','con_dec=300']);

--SELECT * FROM target_data.fn_try_delete_area_domain();
SELECT * FROM target_data.fn_try_delete_sub_population((SELECT id FROM target_data.c_sub_population WHERE label_en = 'conifers/broadleaves'));
SELECT * FROM target_data.fn_delete_sub_population((SELECT id FROM target_data.c_sub_population WHERE label_en = 'conifers/broadleaves'));

-- categories and rules
WITH w_panelref AS (
	-- should be 2,4,5,9,13 panelrefs
	SELECT t1.id, t2.result
	FROM 	sdesign.cm_refyearset2panel_mapping AS t1,
		target_data.fn_check_classification_rules(2,200,array['con_dec=100','con_dec=200'], t1.id) AS t2
),
w_agg AS (
	SELECT array_agg(panel_ref) AS panel_ref		-- 2d array has to be build, use_negative false, hence only 1 element, not necessary to build another for use_negative true
	FROM 
		(SELECT array_agg(t1.id ORDER BY t1.id) AS panel_ref
		FROM 
			w_panelref AS t1
		WHERE result = true
		) AS t1
	),
w_hroubi AS (
	SELECT DISTINCT t2.category_id AS id, t2.cat_label_en AS label_en
	FROM w_agg AS t1, 
		target_data.fn_save_categories_and_rules(200, 200, 100, 'jehličnany/listnáče','Skupina jehličnanů nebo listnáčů.','conifers/broadleaves','The group of conifers or broadleaves.', array['jehličnany','listnáče'],array['jehličnaté','listnaté'],array['conifers','broadleaves'], array['conifers','broadleaves'], array['con_dec=100','con_dec=200'], array[false, false], t1.panel_ref) AS t2
), w_rules AS (

		SELECT 'con_dec=100' AS rule, 'conifers' AS label_en
		UNION ALL
		SELECT 'con_dec=200' AS rule, 'broadleaves' AS label_en
), w_ids AS (
		SELECT t1.id, t1.label_en, t2.rule
		FROM w_hroubi AS t1
		INNER JOIN w_rules AS t2
		ON t1.label_en = t2.label_en
), w_agg_rule AS (
		SELECT array_agg(id ORDER BY id) AS ids, array_agg(rule ORDER BY id) AS rules
		FROM w_ids
), w_panelref_n AS (
	-- should be only 5,9,13 panelrefs
	SELECT t1.id, t3.result
	FROM 	sdesign.cm_refyearset2panel_mapping AS t1,
		w_agg_rule AS t2,
		target_data.fn_check_classification_rules(3,400,t2.rules, t1.id) AS t3
),
w_agg_n AS (
	SELECT array_agg(panel_ref) AS panel_ref		-- 2d array hase to be build, use_negative false, hence only 1 element, not necessary to build another for use_negative true
	FROM 
		(SELECT array_agg(t1.id ORDER BY t1.id) AS panel_ref
		FROM 
			w_panelref_n AS t1
		WHERE result = true
		) AS t1
	)
SELECT *
FROM 	w_agg_n AS t1,
	w_agg_rule AS t2,
	target_data.fn_save_rules(400, 200, 100, t2.ids, array['con_dec=100','con_dec=200'], array[false, false], t1.panel_ref) AS t3
;

-- hierarchy
SELECT * FROM target_data.fn_save_hierarchy(200,array[403,403,403,403,403,404,404,404,404,404,404,404,404,404],array[301,302,303,304,305,306,307,308,309,310,311,312,313,314]);

-- test on constrained rule - fictive, just for test of 2 upper categories
-- should fail on non-even length of given arrays
SELECT * FROM target_data.fn_check_classification_rules(2,200,array['species=8500','species=8600'],NULL,NULL,array[array[1]]);
-- ok, must be the same result as nonconstrained, 201 and 202 are not conditions which somehow reduce the number of stems
SELECT * FROM target_data.fn_check_classification_rules(2,200,array['species=8500','species=8600'],NULL,NULL,array[array[1],array[2]]);

-- hierarchy
SELECT * FROM target_data.fn_delete_hierarchy(200,array[401,401],array[1,2]);
SELECT * FROM target_data.fn_save_hierarchy(200,array[401,401],array[403,404]);

-- cm_adc2classrule2panel_refyearset
with
w1 as (select * from target_data.cm_adc2classification_rule order by id),
w2 as (select (target_data.fn_get_available_refyearset2panel4classification_rule_id('adc'::character varying,w1.id)) as res from w1),
w3 as (select (res).classification_rule_id, (res).refyearset2panel from w2)
insert into target_data.cm_adc2classrule2panel_refyearset(adc2classification_rule,refyearset2panel)
select classification_rule_id, refyearset2panel from w3
where refyearset2panel is distinct from 2
order by classification_rule_id, refyearset2panel; 

-- cm_spc2classrule2panel_refyearset
with
w1 as (	select * 
	from target_data.cm_spc2classification_rule 
	where sub_population_category NOT IN (
		select id 
		from target_data.c_sub_population_category 
		where label_en = ANY(array['conifers','broadleaves'])
	) 
	order by id
),
w2 as (select (target_data.fn_get_available_refyearset2panel4classification_rule_id('spc'::character varying,w1.id)) as res from w1),
w3 as (select (res).classification_rule_id, (res).refyearset2panel from w2)
insert into target_data.cm_spc2classrule2panel_refyearset(spc2classification_rule,refyearset2panel)
select classification_rule_id, refyearset2panel from w3
where (classification_rule_id <= 18 and refyearset2panel is distinct from 2)								-- these datas are inserted using nfi_analytical_data.sql
or (classification_rule_id >= 19 and classification_rule_id <= 34 and refyearset2panel is distinct from 5)	-- these datas are inserted using nfi_analytical_data.sql
or (classification_rule_id >= 35)
order by classification_rule_id, refyearset2panel;

-- ukey test fail
INSERT INTO target_data.c_sub_population_category(label, description, sub_population, label_en, description_en) 
VALUES ('jehličnany','jehličnaté',401,'conifers','conifers');

-- check cm_adc2classrule2panel_refyearset
select id, adc2classification_rule, refyearset2panel from target_data.cm_adc2classrule2panel_refyearset order by id;

-- check cm_spc2classrule2panel_refyearset
select id, spc2classification_rule, refyearset2panel from target_data.cm_spc2classrule2panel_refyearset order by id;

-- test stored rules
SELECT *
FROM 	sdesign.cm_refyearset2panel_mapping AS t1,
	target_data.fn_check_stored_classification_rules(
	(SELECT id FROM target_data.c_ldsity WHERE label_en = 'count of merchantable wood stems'), -- ldsity
	(SELECT id FROM target_data.c_ldsity_object WHERE label_en = 'merchantable wood stems'), -- ldsity_object
	NULL::int, -- area_domain
	(SELECT id FROM target_data.c_sub_population WHERE label_en = 'conifers/broadleaves'),	-- sub_population
	t1.id, -- panelref
	false -- use_negative
) AS t2;


-------------------------
-- state
SELECT * FROM target_data.fn_get_classification_type(2);
-- change
SELECT * FROM target_data.fn_get_classification_type(6);
-- error
SELECT * FROM target_data.fn_get_classification_type(13);

----------------------------
-- updates
----------------------------
-- test
SELECT target_data.fn_save_ldsity_object(400, 'kmeny nehroubí - test', 'kmeny nehroubí - test', 'non-merchantable wood stems - test', 'non-merchantable wood stems - test');
-- state back
SELECT target_data.fn_save_ldsity_object(400, 'kmeny nehroubí', 'kmeny nehroubí', 'non-merchantable wood stems', 'non-merchantable wood stems');

-- test
SELECT target_data.fn_save_ldsity(1, 'plocha lesa - test', 'Plocha lesa. - test', 'area of forests - test', 'The area of forests. - test');
-- state back
SELECT target_data.fn_save_ldsity(1, 'plocha lesa', 'Plocha lesa.', 'area of forests', 'The area of forests.');

-- test
	-- fail
SELECT target_data.fn_save_target_variable (100, 'plocha lesa - test', 'Plocha lesa. - test', 'forest area - test', 'The area of forests. - test', NULL, NULL, NULL, array[1,4]);
	-- ok
SELECT target_data.fn_save_target_variable (100, 'plocha lesa - test', 'Plocha lesa. - test', 'forest area - test', 'The area of forests. - test', NULL, NULL, NULL, array[1,4,5]);
	-- state back
SELECT target_data.fn_save_target_variable (100, 'plocha lesa', 'Plocha lesa.', 'forest area', 'Area of forests.', NULL, NULL, NULL, array[1,4,5]);
----------------------------
